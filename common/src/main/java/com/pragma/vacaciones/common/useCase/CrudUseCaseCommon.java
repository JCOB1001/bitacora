package com.pragma.vacaciones.common.useCase;

import com.pragma.vacaciones.common.persistencia.Modelo;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class CrudUseCaseCommon<REPOSITORIO extends CrudRepositorioBase<ID, ENTIDAD>, ENTIDAD extends Modelo<ID>, ID> {

    private final REPOSITORIO repositorio;

    public ENTIDAD guardar(ENTIDAD entidad) {
        return repositorio.guardar(entidad);
    }

    public Stream<ENTIDAD> guardarTodo(Iterable<ENTIDAD> entidad){ return repositorio.guardar(entidad);}

    public void actualizar(ENTIDAD entidad) {
        repositorio.actualizar(entidad);
    }

    public void actualizarTodo(Iterable<ENTIDAD> entidad){ repositorio.actualizar(entidad);}

    public ENTIDAD obtenerPorId(ID id) {
        return repositorio.buscarPorId(id);
    }

    public Stream<ENTIDAD> obtenerPorIdLista(Iterable<ID> id){ return repositorio.buscarPorId(id);}

    public Stream<ENTIDAD> obtenerTodos() {
        return repositorio.buscarTodo();
    }

    public void eliminarPorId(ID id) {
        repositorio.eliminarPorId(id);
    }
}
