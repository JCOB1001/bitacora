package com.pragma.vacaciones.common.servicio;


import com.pragma.vacaciones.common.filtro.FiltroBase;
import com.pragma.vacaciones.common.persistencia.Modelo;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;


public interface CrudRepositorioBaseFiltro<ID, MODELO extends Modelo<ID>, FILTRO extends FiltroBase, RESPUESTA extends ObjetoRespuesta> {

    MODELO guardar(MODELO modelo);

    Stream<MODELO> guardar(Iterable<MODELO> modelo);

    void actualizar(MODELO modelo);

    void actualizar(Iterable<MODELO> modelo);

    void actualizar(ID id, MODELO modelo);

    void eliminarPorId(ID id);

    void eliminarPorId(Iterable<ID> id);

    void delete(MODELO modelo);

    MODELO buscarPorId(ID id);

    Stream<MODELO> buscarPorId(Iterable<ID> id);

    RESPUESTA buscarTodo(FILTRO filtro);

}
