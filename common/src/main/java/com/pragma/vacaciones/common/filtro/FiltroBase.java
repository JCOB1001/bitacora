package com.pragma.vacaciones.common.filtro;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FiltroBase {

    private Integer numeroPagina;
    private Integer totalElementos;

}
