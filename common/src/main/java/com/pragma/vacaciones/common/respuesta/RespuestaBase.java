package com.pragma.vacaciones.common.respuesta;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RespuestaBase implements Serializable {

    private String mensaje;

    public static RespuestaBase ok(String mensaje) {
        return new RespuestaBase(mensaje);
    }

}
