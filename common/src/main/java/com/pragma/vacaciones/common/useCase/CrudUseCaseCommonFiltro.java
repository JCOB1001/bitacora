package com.pragma.vacaciones.common.useCase;

import com.pragma.vacaciones.common.filtro.FiltroBase;
import com.pragma.vacaciones.common.persistencia.Modelo;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBaseFiltro;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class CrudUseCaseCommonFiltro<REPOSITORIO
        extends CrudRepositorioBaseFiltro<ID, ENTIDAD, FILTRO, RESPUESTA>, ID, ENTIDAD extends Modelo<ID>, FILTRO extends FiltroBase, RESPUESTA extends ObjetoRespuesta> {

    private final REPOSITORIO repositorio;

    public ENTIDAD guardar(ENTIDAD entidad) {
        return repositorio.guardar(entidad);
    }

    public void actualizar(ENTIDAD entidad) {
        repositorio.actualizar(entidad);
    }

    public ENTIDAD obtenerPorId(ID id) {
        return repositorio.buscarPorId(id);
    }

    public RESPUESTA obtenerTodos(FILTRO filtro) {
        return repositorio.buscarTodo(filtro);
    }

    public void eliminarPorId(ID id) {
        repositorio.eliminarPorId(id);
    }
}
