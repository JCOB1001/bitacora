package com.pragma.vacaciones.common.persistencia;

public interface IdEntidad<ID> {

    public interface Atributos {
        String ID = "id";
    }

    ID getId();

    void setId(ID id);
}
