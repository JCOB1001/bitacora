--Script para añadir el campo Indices a la tabla conocimientos_usuario

--Para desarrollo
ALTER TABLE perfilesdev.usuario_conocimiento ADD indice INT DEFAULT 0 NULL;

--Para produccion
ALTER TABLE perfilesprod.usuario_conocimiento ADD indice INT DEFAULT 0 NULL;
