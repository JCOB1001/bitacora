
##NUEVAS COLUMNAS
ALTER TABLE perfilesprod.usuario
ADD fecha_inicio_contrato date;
ALTER TABLE perfilesprod.usuario
ADD tipo_contrato_id varchar(255);

alter table perfilesprod.usuario
add constraint fk_usuario_tipo_contrato_tipo_contrato_id
foreign key (tipo_contrato_id) references perfilesprod.tipo_contrato

##MIGRAR CONTRATOS A USUARIO
UPDATE perfilesprod.usuario u,
       perfilesprod.contrato_historico c
SET    u.tipo_contrato_id = c.tipo_contrato_id,
       u.fecha_inicio_contrato = c.fecha_inicio
WHERE  u.id = c.usuario_id
and    c.estado = 1