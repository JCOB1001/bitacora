--Script para dias_celebracion spring15

create table perfilesprod.dias_celebracion (id varchar(255) not null,
                                            nombre varchar(255),
                                            primary key (id));

create table perfilesprod.dias_no_celebracion_usuario (id varchar(255) not null,
                                          dia_celebracion_id varchar(255),
                                          user_id varchar(255),
                                          primary key (id));

INSERT INTO perfilesprod.dias_celebracion(id,nombre) VALUES
(UUID(),'CUMPLEAÑOS'),
(UUID(),'DÍA DE LA MADRE'),
(UUID(),'DÍA DEL PADRE'),
(UUID(),'DÍA DEL NIÑO'),
(UUID(),'DÍA DE LA MUJER'),
(UUID(),'DÍA DEL HOMBRE'),
(UUID(),'ANIVERSARIO EN PRAGMA'),
(UUID(),'GRADO DEL PRAGMÁTICO'),
(UUID(),'NAVIDAD');



