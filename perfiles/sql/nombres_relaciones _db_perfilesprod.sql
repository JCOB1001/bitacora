--PROD

--fotografia
ALTER TABLE perfilesprod.fotografia
DROP FOREIGN KEY fotografia_ibfk_1;

ALTER TABLE perfilesprod.fotografia
CHANGE id_usuario usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.fotografia
add constraint fk_fotografia_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--usuario_entity_capacidad
ALTER TABLE perfilesprod.usuario_entity_capacidad
DROP FOREIGN KEY usuario_entity_capacidad_ibfk_1;

ALTER TABLE perfilesprod.usuario_entity_capacidad
CHANGE usuario_entity_id usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL;

alter table perfilesprod.usuario_entity_capacidad
add constraint fk_usuario_entity_capacidad_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--historico_cambios_pragmadex
ALTER TABLE perfilesprod.historico_cambios_pragmadex
CHANGE id_usuario_modifica usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL
COMMENT 'Usuario que modifica';

alter table perfilesprod.historico_cambios_pragmadex
add constraint fk_historico_cambios_pragmadex_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--intereses
ALTER TABLE perfilesprod.intereses
DROP CONSTRAINT UKdmrcs7ayxgm0yftmhmxw82e0d;
ALTER TABLE perfilesprod.intereses
DROP CONSTRAINT UKsve7ijm0rn65cuafx1vfpe6m6;

ALTER TABLE perfilesprod.intereses
CHANGE user_id usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.intereses
add constraint fk_intereses_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

alter table perfilesprod.intereses
add constraint uq_usuario_id_nombre
unique (usuario_id, nombre);

--usuario_conocimiento
ALTER TABLE perfilesprod.usuario_conocimiento
DROP CONSTRAINT UKe0dbipaenok8sti30iq1c7g3u;

ALTER TABLE perfilesprod.usuario_conocimiento
CHANGE user_id usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.usuario_conocimiento
add constraint fk_usuario_conocimiento_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

alter table perfilesprod.usuario_conocimiento
add constraint uq_usuario_id_skill_name
unique (usuario_id, skill_name);

--contrato_historico
ALTER TABLE perfilesprod.contrato_historico
CHANGE id_usuario usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;
ALTER TABLE perfilesprod.contrato_historico
CHANGE id_tipocontrato tipo_contrato_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.contrato_historico
add constraint fk_contrato_historico_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;
alter table perfilesprod.contrato_historico
add constraint fk_contrato_historico_tipo_contrato_tipo_contrato_id
foreign key (tipo_contrato_id) REFERENCES perfilesprod.tipo_contrato(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--estudio
ALTER TABLE perfilesprod.estudio
CHANGE id_usuario usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.estudio
add constraint fk_estudio_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--informacion_nomina
ALTER TABLE perfilesprod.informacion_nomina
CHANGE id_usuario usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.informacion_nomina
add constraint fk_informacion_nomina_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--experiencia_laboral_pragma
ALTER TABLE perfilesprod.experiencia_laboral_pragma
CHANGE id_usuario usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.experiencia_laboral_pragma
add constraint fk_experiencia_laboral_pragma_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--experiencia_laboral_externa
ALTER TABLE perfilesprod.experiencia_laboral_externa
CHANGE id_usuario usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.experiencia_laboral_externa
add constraint fk_experiencia_laboral_externa_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--usuario_idiomas
ALTER TABLE perfilesprod.usuario_idiomas
DROP CONSTRAINT UKde59lrav471k32hxjtu2fblmv;

ALTER TABLE perfilesprod.usuario_idiomas
CHANGE user_id usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;
ALTER TABLE perfilesprod.usuario_idiomas
CHANGE idioma_id idiomas_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.usuario_idiomas
add constraint uq_usuario_id_idiomas_id
unique (usuario_id, idiomas_id);

alter table perfilesprod.usuario_idiomas
add constraint fk_usuario_idiomas_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;
alter table perfilesprod.usuario_idiomas
add constraint fk_usuario_idiomas_idiomas_idiomas_id
foreign key (idiomas_id) REFERENCES perfilesprod.idiomas(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--informacion_laboral
alter table perfilesprod.informacion_laboral
add constraint fk_informacion_laboral_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;


--dias_no_celebracion_usuario
ALTER TABLE perfilesprod.dias_no_celebracion_usuario
DROP FOREIGN KEY FK5tpeyfg4ydl0s8wuiqi0f231c;

ALTER TABLE perfilesprod.dias_no_celebracion_usuario
CHANGE user_id usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.dias_no_celebracion_usuario
add constraint fk_dncu_dias_celebracion_dia_celebracion_id
foreign key (dia_celebracion_id) REFERENCES perfilesprod.dias_celebracion(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;
alter table perfilesprod.dias_no_celebracion_usuario
add constraint fk_dias_no_celebracion_usuario_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--informacion_familiar
ALTER TABLE perfilesprod.informacion_familiar
DROP FOREIGN KEY FK7m0ghdusfve8eorp4mureikja;

ALTER TABLE perfilesprod.informacion_familiar
CHANGE id_parentesco parentesco_id bigint NULL;

ALTER TABLE perfilesprod.informacion_familiar
CHANGE id_usuario usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.informacion_familiar
add constraint fk_informacion_familiar_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;
alter table perfilesprod.informacion_familiar
add constraint fk_informacion_familiar_parentesco_parentesco_id
foreign key (parentesco_id) REFERENCES perfilesprod.parentesco(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

--otros_estudios
ALTER TABLE perfilesprod.otros_estudios
CHANGE id_usuario usuario_id varchar(255)
CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;

alter table perfilesprod.otros_estudios
add constraint fk_otros_estudios_usuario_usuario_id
foreign key (usuario_id) REFERENCES perfilesprod.usuario(id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE perfilesprod.barra_progreso
DROP CONSTRAINT UK6m1mj90ikuep9egk84ysftceo;
alter table perfilesprod.barra_progreso
add constraint uq_nombre_campo unique (nombre_campo)

ALTER TABLE perfilesprod.conocimientos
DROP CONSTRAINT UKda197rf6lumw7d7resg0gvs0r;
alter table cperfilesprod.onocimientos
add constraint uq_nombre unique (nombre)

ALTER TABLE perfilesprod.interes
DROP CONSTRAINT UKhtav6dv54dhf7g115p428y629;
alter table perfilesprod.interes
add constraint uq_interes_nombre unique (nombre)

ALTER TABLE perfilesprod.usuario
DROP CONSTRAINT UK_chhuyxo6lvac9v1hnw6faqua9;
alter table perfilesprod.usuario
add constraint uq_correo_empresarial unique (correo_empresarial)