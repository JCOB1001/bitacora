--Script para IDIOMAS

CREATE TABLE perfilesprod.usuario_idiomas(

          `id`                  varchar(255) NOT NULL,
          `idioma_id`           varchar(255) NOT NULL,
          `user_id`             varchar(255) NOT NULL,
          `nivel`               int NOT NULL,
          `prueba_suficiencia`  bit(1) DEFAULT FALSE,
          `nombre_prueba`       varchar(255) DEFAULT '',
          `puntaje`             double DEFAULT 0,
          `url_certificado`     varchar(255) DEFAULT '',

      PRIMARY KEY (`id`),
      UNIQUE KEY `UKde59lrav471k32hxjtu2fblmv` (`user_id`,`idioma_id`)
    );

CREATE TABLE perfilesprod.idiomas(

          `id`                  varchar(255) NOT NULL,
          `nombre`              varchar(255) DEFAULT NULL,

    PRIMARY KEY (`id`)
)

INSERT INTO perfilesprod.idiomas (id,nombre) VALUES
          ('1','Ingles'),
          ('2','Español'),
          ('3','Italiano'),
          ('4','Frances'),
          ('5','Aleman'),
          ('6','Arabe'),
          ('7','Chino'),
          ('1','Japones');

INSERT INTO perfilesprod.barra_progreso (id,nombre_campo,valor)
VALUES ('35','idiomas',3.030);

UPDATE perfilesprod.barra_progreso SET valor  = 3.030 WHERE valor = 3.125;

