--dev
INSERT INTO perfilesdev.tipo_cambio (id,nombre_cambio)
	VALUES ('01','TERMINO DE CONTRATO');
INSERT INTO perfilesdev.tipo_cambio (id,nombre_cambio)
	VALUES ('02','CAMBIO DE CONTRATO');
INSERT INTO perfilesdev.tipo_cambio (id,nombre_cambio)
	VALUES ('03','CAMBIO DE SALARIO');
--
INSERT INTO perfilesdev.tipo_contrato (id, descripcion, vacaciones)
    VALUES('16', 'Termino de contrato', 0);

--prod
INSERT INTO perfilesprod.tipo_cambio (id,nombre_cambio)
	VALUES ('01','TERMINO DE CONTRATO');
INSERT INTO perfilesprod.tipo_cambio (id,nombre_cambio)
	VALUES ('02','CAMBIO DE CONTRATO');
INSERT INTO perfilesprod.tipo_cambio (id,nombre_cambio)
	VALUES ('03','CAMBIO DE SALARIO');

INSERT INTO perfilesprod.tipo_contrato (id, descripcion, vacaciones)
    VALUES('16', 'Termino de contrato', 0);