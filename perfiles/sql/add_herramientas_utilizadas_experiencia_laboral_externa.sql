--Script para añadir el campo herramientas_utilizadas a la tabla experiencia_laboral_externa

--Para desarrollo
ALTER TABLE perfilesdev.experiencia_laboral_externa ADD herramientas_utilizadas TEXT NULL;

--Para produccion
ALTER TABLE perfilesprod.experiencia_laboral_externa ADD herramientas_utilizadas TEXT NULL;