INSERT INTO experiencia_laboral_externa_detalle  (
ID,
ROL,
LOGROS,
HERRAMIENTAS_UTILIZADAS,
DESCRIPCION,
experiencia_laboral_externa_id
) SELECT UUID(), exp.ROL, exp.LOGROS, exp.HERRAMIENTAS_UTILIZADAS, exp.DESCRIPCION, exp.ID FROM experiencia_laboral_externa exp;

UPDATE experiencia_laboral_externa_detalle SET IDIOMAS_ID = 2;