
INSERT INTO parentesco VALUES
(1, 'Madre'),
(2, 'Padre'),
(3, 'Cónyuge'),
(4, 'Hermano(a)'),
(5, 'Hijo(a)');

INSERT INTO tipo_contrato
(id, descripcion, vacaciones)
VALUES('1', 'Contrato de aprendizaje', 0),
('10', 'Contratado por externo', 0),
('11', 'Convenio Formativo Laboral Pro', 1),
('12', 'Término indefinido Perú', 1),
('13', 'Término indefinido Panamá', 1),
('14', 'Término indefinido EU', 1),
('15', 'Término indefinido Guatemala', 1),
('16', 'Termino de contrato', 0),
('2', 'Término indefinido', 1),
('3', 'Prestación de servicios', 0),
('4', 'Obra labor', 0),
('5', 'Aprendizaje', 1),
('6', 'término fijo', 1),
('8', 'patrocinio aprendizaje', 1),
('9', 'Cesión de contrato', 0),
('99', 'Retiro-prueba', 0);


INSERT INTO usuario (id, correo_empresarial, identificacion, nombres, apellidos, id_vicepresidencia, cliente,
                    fecha_Ingreso, dias_Ganados, dias_Tomados,
                    google_id, correo_personal, telefono_fijo, telefono_celular,
                    id_lugar_nacimiento, id_lugar_residencia, id_nacionalidad,
                    fecha_nacimiento, direccion, nombre_contacto_emergencia,
                    parentesco_contacto_emergencia, telefono_contacto_emergencia,
                    tarjeta_profesional, perfil_profesional, conocimientos_tecnicos,
                    talla_camisa, estado_civil, genero, grupo_sanguineo, estado, id_tipo_documento,
                    id_profesion, cargo, fecha_ultima_actualizacion, fecha_creacion, aceptacion_de_terminos,
                    estrato,estado_profesion,semestre,tipo_contrato_id,fecha_inicio_contrato,empresa, proyectosEEUU, visa, total_anos_experiencia_laboral)
VALUES
('9876554','pablo.munozm@pragma.com.co','123456789','Pablo Camilo', 'Munoz Arango', 1, 'Grupo Familia', '2021-07-11', 120, 10,
'1234-4321', 'pablo.munozm@correo.com','3123456','3003121234','283','283','47','1991-07-11','calle 1 # 2 - 1',
'Señora Arango', 'Madre','3213214321','12t', 'Programar en java','Java','XL','SOLTERO_A','MASCULINO','O_POSITIVO','ACTIVO','1','32',
 'Desarrollador' ,'2021-07-11','2020-07-11',false,1,'ESTUDIANDO','1','1','2022-03-11','Pragma', true, true, 10),
('098644','manoloM@pragma.com.co','123456','Manolo', 'Leon', 1, 'Grupo Familia', '2021-07-11', 120, 10,
'113188858824715279245','manoloM@correo.com','3223456','3103121234','283','283','47','1996-07-11','calle 43 # 2 - 1', 'Señor Leon', 'Padre','3013214321','12trd',
'Programar en angular','Angular','XL','SOLTERO_A','MASCULINO','O_POSITIVO','ACTIVO','1','32', 'Desarrollador', '2021-07-11','2020-07-11',false,2,'ESTUDIANDO','1','1','2022-03-11','Pragma', true, true, 10),
('879056','shrekk@pragma.com.co','123456','Shrekk', 'Muñóz', 1, 'Grupo Familia', '2021-07-11', 120, 10,
'google_id', 'shrekk@correo.com','3323456','3103121235','281','284','47','1997-07-11','calle 43 # 22 - 1', 'Señor Muñóz', 'Padre','3033214321','134trd',
'Programar en NodeJs','NodeJs','XL','SOLTERO_A','MASCULINO','O_POSITIVO','ACTIVO','1','32', 'Desarrollador', '2021-07-11','2020-07-11',false,3,'ESTUDIANDO','1','1','2022-03-11','Pragma', true, true, 10),
('4798034','bob@pragma.com.co','123456','Bob', 'Torres', 1, 'Grupo Familia', '2021-07-11', 120, 10,
'google_id', 'joseT@correo.com','3323457','3113121235','281','282','47','1992-07-11','calle 43 # 22 - 21', 'Señor Torres', 'Padre','3035214321','224trd',
'Programar en Reac','Reac','XL','SOLTERO_A','MASCULINO','O_POSITIVO','ACTIVO','1','32', 'Desarrollador', '2021-07-11','2020-07-11',false,4,'ESTUDIANDO','1','1','2022-03-11','Pragma', true, true, 10),
('93234567893','hans.cupaban@pragma.com.co','199456','Hans', 'Cupaban', 1, 'Grupo Familia', '2021-07-11', 120, 10,
'google_id', 'hans.cupaban@correo.com','3323457','3113121235','281','282','47','1993-07-11','calle 43 # 22 - 21', 'Señor Cupaban', 'Padre','3035214321','822234trd',
'Programar en Reac','Reac','XL','SOLTERO_A','MASCULINO','O_POSITIVO','ACTIVO','1','32', 'Desarrollador', '2021-07-11','2020-07-11',false,5,'ESTUDIANDO','1','1','2022-03-11','Pragma', true, true, 10),
('93234567894','karen.beltran@pragma.com.co','123456','Karen Brigid', 'Beltran Vera', 1, 'Grupo Familia', '2021-07-11', 120, 10,
'google_id', 'brigid2427@correo.com','3323457','3113121235','281','282','47','1993-07-11','calle 43 # 22 - 21', 'Señor Beltran', 'Padre','3035214321','822234trd',
'Programar en Reac','Reac','XL','SOLTERO_A','FEMENINO','O_POSITIVO','ACTIVO','1','32', 'Desarrollador', '2021-07-11','2020-07-11', false,6,'ESTUDIANDO','1','1','2022-03-11','Pragma', true, true, 10),
('93234567895','charlie.solis@pragma.com.co','123456','Charlie', 'Solis Núñez', 1, 'Grupo Familia', '2019-08-28', 120, 10,
'google_id', 'charliesolis@correo.com','3323457','3113121235','281','282','47','1993-07-11','calle 43 # 22 - 21', 'Señor Solis', 'Padre','3035214321','822234trd',
'Programar en Python','Python','S','SOLTERO_A','MASCULINO','A_POSITIVO','INACTIVO','1','32', 'Desarrollador', '2021-07-11','2020-07-11', false,4,'ESTUDIANDO','10','1','2019-08-28','Pragma', true, true, 10);

INSERT INTO usuario (id, correo_empresarial, identificacion, nombres, apellidos, id_vicepresidencia, cliente, fecha_Ingreso, dias_Ganados, dias_Tomados, fecha_Creacion,aceptacion_de_terminos,estrato, semestre, estado_profesion,tipo_contrato_id,fecha_inicio_contrato,empresa)
VALUES
('111839089495784854974','cristianc.munozm@pragma.com.co','123456','Cristian Camilo', 'Munoz Arango', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('107549172527795691058','bryan.rodas@pragma.com.co','123456','Bryan', 'Rodas Collazos', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854972','cristina.quintero@pragma.co','123456','Cristina', 'Quintero Noreña', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854971','diego.orozco@pragma.com.co','123456','Diego Andres', 'Orozco Perez', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854975','kelly.gomez@pragma.com.co','123456','Kelly Julieth', 'Gomez Parra', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854976','paola.trujillo@pragma.com.co','123456','Paola Andrea', 'Trujillo Betancur', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854977','david.tolosa@pragma.com.co','123456','David Alejandro', 'Tolosa Zabala', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('104437232779120964108','sebastian.vega@pragma.com.co','123456','Brayan Sebastian', 'Vega Garcia', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854979','carlos.pelaez@pragma.com.co','123456','Carlos', 'Peláez Parra', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('106646756784347397645','carlos.lopez@pragma.com.co','123456','Carlos Alberto', 'Lopez Mazo', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('110997072129088714689','carlos.murillo@pragma.com.co','123456','Carlos Alberto', 'Murillo Mendoza', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854982','dana.chaparro@pragma.com.co','123456','Dana Paulina', 'Chaparro Vera', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('112207479951484153746','daniel.pena@pragma.com.co','123456','Daniel Fernando', 'Peña Pinzón', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('104476776446278724912','jenny.escobar@pragma.com.co','123456','Jenny Carolina', 'Escobar Sosa', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854985','jorge.perez@pragma.com.co','123456','Jorge Omar', 'Perez Peñaranda', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854986','jose.hernandez@pragma.com.co','123456','José Andrés', 'Hernández Flórez', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854987','jose.losada@pragma.com.co','123456','Jose', 'Losada Jiménez', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111417031252920999248','juan.arrieta@pragma.com.co','123456','Juan Camilo', 'Arrieta Acosta', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('103562812116469158343','juan.fuentes@pragma.com.co','123456','Juan Andrés', 'Fuentes Laguado', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('117119995080232990145','kevin.echeverri@pragma.com.co','123456','Kevin Orlando', 'Echeverri Loboa', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('111839089495784854991','pedro.lopez@pragma.com.co','123456','Pedro Jose', 'López Suarez', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('109833729345948461593','rafael.tigreros@pragma.com.co','123456','Rafael', 'Tigreros Colmenares', '1', 'Grupo Familia', '2021-07-11', 50, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('01234567891','miguelM@pragma.com.co','123456','Miguel', 'Leon', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('01234567892','marcosM@pragma.com.co','123456','Marcos', 'Muñóz', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('01234567893','joseT@pragma.com.co','123456','Jose', 'Torres', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('01234567894','danielaR@pragma.com.co','123456','Daniela', 'Rojas', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('01234567895','carlosJ@pragma.com.co','123456','Carlos', 'Julio', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('01234567896','kevinR@pragma.com.co','123456','Kevin', 'Rodas', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('01234567897','luisC@pragma.com.co','123456','Luis', 'Cardona', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('01234567898','maryuriS@pragma.com.co','123456','Maryuri', 'Santos', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('01234567899','bryanM@pragma.com.co','123456','Bryan', 'Muñóz', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('012345678910','camilaP@pragma.com.co','123456','Camila', 'Peréz', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma'),
('012345678911','luis.forero@pragma.com.co','123456','Luis', 'Forero', '1', 'Grupo Familia', '2021-07-11', 120, 10,'2020-07-11',false,1,'1','ESTUDIANDO','1','2022-03-11','Pragma');

INSERT INTO barra_progreso (id, nombre_campo, valor )
VALUES
('9876554', 'identificacion', 3.03),
('2', 'nombres', 3.03),
('3', 'apellidos', 3.03),
('4', 'fechaIngreso', 3.03),
('5', 'correoPersonal', 3.03),
('6', 'telefonoFijo', 0.0),
('7', 'telefonoCelular', 0.0),
('8', 'idNacionalidad', 3.03),
('9', 'idLugarNacimiento', 3.03),
('10', 'idLugarRecidencia', 3.03),
('11', 'fechaNacimiento', 3.03),
('12', 'direccion', 3.03),
('13', 'nombreContactoEmergencia', 3.03),
('14', 'parentescoContactoEmergencia', 3.03),
('15', 'telefonoContactoEmergencia', 3.03),
('16', 'tarjetaProfesional', 3.03),
('17', 'perfilProfesional', 3.03),
('18', 'conocimientosTecnicos', 3.03),
('19', 'tallaCamisa', 3.03),
('20', 'estadoCivil', 3.03),
('21', 'grupoSanguineo', 3.03),
('22', 'idTipoDocumento', 3.03),
('23', 'idProfesion', 3.03),
('24', 'cargo', 3.03),
('25', 'idVicepresidencia', 3.03),
('26', 'capacidad', 3.03),
('27', 'fotografias', 3.03),
('28', 'experienciaLaboralInterna', 3.03),
('29', 'experienciaLaboralExterna', 3.03),
('30', 'estudios', 3.03),
('31', 'otrosEstudios', 3.03),
('32', 'intereses', 3.03),
('33', 'conocimientos', 3.03),
('34', 'correoEmpresarial', 3.03),
('35', 'idiomas', 3.03);

INSERT INTO capacidad VALUES
(1, 'Backend'),
(2, 'Frontend'),
(3, 'Agilismo'),
(4, 'UX'),
(5, 'UI');

INSERT INTO informacion_familiar (id,nombres,apellidos,genero,fecha_Nacimiento,usuario_id,parentesco_id,dependiente)
VALUES
('1','Eduard Jose','Cantillo Rodriguez','FEMENINO','2018-02-27','098644',4,true),
('2','Juan Jose','Cantillo Rodriguez','MASCULINO','2018-02-27','098644',4,false),
('3','Alvis Jose','Cantillo Rodriguez','MASCULINO','2018-02-27','93234567893',4,false),
('4','Cristian Jose','Cantillo Rodriguez','MASCULINO','2018-02-27','93234567893',4,true);

INSERT INTO fotografia (usuario_id, id, contenido, fecha_creacion, tipo_fotografia )
VALUES ('9876554','1', 'https://cn.i.cdn.ti-platform.com/cnemea/content/565/showpage/courage-the-cowardy-dog/uk/showicon_courage.png','2020-07-11','REDONDA'),
 ('9876554','2', 'https://i.pinimg.com/originals/08/c1/9a/08c19aa1733751ad1f28ad72ab569093.jpg','2020-07-11','CUADRADO' );

 INSERT INTO experiencia_laboral_pragma(id,usuario_id, proyecto, rol, fecha_de_ingreso, fecha_de_finalizacion, tiempo_de_experiencia, logros) VALUES
     ('1','01234567891','Ecosistemas','Auxiliar de desarrollo','2018-06-19','2021-04-18',25,'terminar todo'),
     ('2','01234567891','Vacaciones','Lider de equipo','2018-06-19','2021-03-18',24,'terminar todo'),
     ('3','01234567891','Bitacora','Arquitecto','2018-06-19','2021-02-18',23,'terminar todo'),
     ('4','01234567892','Vacaciones','Ingeniero de proyectos','2018-06-19','2021-01-18',22,'terminar todo'),
     ('5','01234567892','Ecosistemas','Desarrollador backend','2018-06-19','2020-12-18',21,'terminar todo');


 INSERT INTO experiencia_laboral_externa(id, usuario_id, empresa, rol, fecha_de_ingreso, fecha_de_finalizacion, tiempo_de_experiencia, logros) VALUES
     ('2','9876554','freelncer','backend','2018-06-19','2021-04-18',25,'terminar todo'),
     ('1','9876554','freelncer','backend','2018-06-19','2021-04-18',25,'terminar todo'),
     ('3','9876554','freelncer','backend','2018-06-19','2021-04-18',25,'terminar todo'),
     ('4','012345678911','freelncer','backend','2018-06-19','2021-04-18',25,'terminar todo'),
     ('5','012345678910','freelncer','backend','2018-06-19','2021-04-18',25,'terminar todo');



 INSERT INTO estudio (id, usuario_id, nivel, titulo_obtenido, institucion, anio_finalizacion)  VALUES
 ('1','9876554','MAESTRIA','Ingenieria','ufps','2022'),
 ('2','9876554','MAESTRIA','Ingenieria','ufps','2022'),
 ('3','9876554','MAESTRIA','Ingenieria','ufps','2022'),
 ('4','9876554','MAESTRIA','Ingenieria','ufps','2022'),
 ('5','9876554','MAESTRIA','Ingenieria','ufps','2022');

 INSERT INTO otros_estudios(id, usuario_id, fecha_de_expiracion, horas, titulo, anio, instituto, url_certificado) VALUES
    ('1', '01234567891', '2021-06-19',22, 'Dev', '2020', 'HOUSE', 'https://d2y95fubzeqro6.cloudfront.net/general/certicados/d779d856-ddbf-4b47-b4bc-059e22a4bb8c.png'),
    ('2', '01234567891', '2021-09-19',22, 'JAVA', '2020', 'HOUSE', 'https://d2y95fubzeqro6.cloudfront.net/general/certicados/d779d856-ddbf-4b47-b4bc-059e22a4bb8c.png'),
    ('3', '01234567891', '2021-08-19',22, 'RUBy', '2021', 'HOUSE', 'https://d2y95fubzeqro6.cloudfront.net/general/certicados/d779d856-ddbf-4b47-b4bc-059e22a4bb8c.png');

INSERT INTO intereses (id,usuario_id,nombre) VALUES
    ('1','111839089495784854974','FUTBOL'),
    ('2','111839089495784854974','MICRO'),
    ('3','111839089495784854974','MOVIE'),
    ('4','111839089495784854974','PESCA');

INSERT INTO conocimientos (id,nombre) VALUES
    ('1','Angular'),
    ('2','AWS'),
    ('3','SQL'),
    ('4','Java');

INSERT INTO usuario_conocimiento (id,usuario_id,skill_name, rate,indice) VALUES
    ('1','9876554','Angular',3,0),
    ('2','111839089495784854974','AWS',3,0),
    ('3','111839089495784854974','SQL',3,0),
    ('4','111839089495784854974','Java',3,0),
    ('5','111839089495784854974','React',4,0),
    ('6','01234567891','React',3,0),
    ('7','01234567892','React',3,0),
    ('8','01234567893','React',3,0),
    ('9','9876554','SQL',2,0);

INSERT INTO interes (id,nombre) VALUES
('1','Artes Marciales'),
('2','Ciclismo'),
('3','Boxeo');

INSERT INTO idiomas (id, nombre) VALUES
('1', 'Ingles'),
('2', 'Español'),
('3', 'Italiano'),
('4', 'Frances'),
('5', 'Aleman'),
('6', 'Arabe'),
('7', 'Hindi');

INSERT INTO usuario_idiomas (id, usuario_id, idiomas_id, nivel,prueba_suficiencia,nombre_prueba,puntaje,url_certificado) VALUES
('1', '098644', '1', 4,true,'ECAES',25,'google.com'),
('2', '098644', '2', 2,true,'ECAES',25,'google.com'),
('3', '9876554', '3', 1,true,'ECAES',25,'google.com'),
('4', '9876554', '4', 3,false,null,null,null);

INSERT INTO informacion_laboral (id, usuario_id, tipo_contrato, fecha_inicio_contrato_actual, referido, cliente, proyecto, fecha_inicio_relacion, estado_periodo_prueba, evaluador_periodo_prueba)
VALUES
('1', '098644', 'indefinido', '2020-12-09', 'jose', 'nuevo', 'nuevo', '2020-12-09', 'REALIZADO', 'jose'),
('2', '9876554', 'indefinido', '2021-12-09', 'pepito', 'nuevo', 'nuevo', '2021-12-09', 'REALIZADO', 'pepito'),
('3', '9876554', 'indefinido', '2021-12-09', 'pepito', 'nuevo', 'nuevo', '2021-12-09', 'REALIZADO', 'pepito');

INSERT INTO informacion_nomina(id, usuario_id, cargo_nomina, salario_basico_ordinario, salario_basico_integral, no_constitutivos_salario,moneda) VALUES
('1','098644','TTTT',500,600,700,'SOLES'),
('2','9876554','AAA',1000,2000,3000,'DOLARES');

INSERT INTO contrato_historico (id, usuario_id, fecha_inicio, fecha_fin, tipo_contrato_id, estado) VALUES
('1','9876554', '2020-12-09', '2021-02-12', '1', '0'),
('3','9876554', '2021-02-12', '2021-02-12', '1', '1'),
('2','098644', '2020-12-09', '2021-02-12', '3', '1');

INSERT INTO dias_celebracion(id,nombre) VALUES
('1','CUMPLEAÑOS'),
('2','NAVIDAD'),
('3','TIEMPO EN PRAGMA');

INSERT INTO dias_no_celebracion_usuario(id,usuario_id,dia_celebracion_id) VALUES

(UUID(),'93234567893','1'),
(UUID(),'9876554','1'),
(UUID(),'93234567893','3');

INSERT INTO tipo_cambio
(id, nombre_cambio)
VALUES('21', 'pruebita')
,('01', 'TERMINO DE CONTRATO'),
('02', 'CAMBIO DE CONTRATO'),
('03', 'CAMBIO DE SALARIO');


INSERT INTO historico_de_cambios
(id, usuario_id, detalle, fecha_hito, tipo_cambio_id)
VALUES('517ae736-9bbb-49ad-990f-1c1fb920121f','9876554', 'auxiliar', NULL, '21');

INSERT INTO usuario_detalle (id,cargo,conocimientos_tecnicos,perfil_profesional,idiomas_id,usuario_id)
values('98765','ingeniero','java,python,spring','hola, soy juanma y soy el mejor en todo lo que hago','2','9876554'),
('98764','engineer','java,python,spring','hi, i am juanma and i am the best at everything i do','1','9876554');

INSERT INTO  estudio_detalle (id,nivel,titulo_obtenido,idiomas_id,estudio_id)
values('34567','PROFESIONAL','ingeniero de sistemas','2','2'),
('34568','PROFESSIONAL','System engineer','1','1');

INSERT INTO experiencia_laboral_externa_detalle (id,descripcion,herramientas_utilizadas,logros,rol,idiomas_id,experiencia_laboral_externa_id)
values('3456','trabaje como desarrollador devops para bancolombia','PYTHON numero 3','terminar todo','backend','2','1'),
('3457','work as devops developer for Bancolombia','PYTHON number 4','finish everything','backend','1','2');

INSERT INTO experiencia_laboral_pragma_detalle (id,descripcion,herramientas_utilizadas,logros,proyecto,rol,idiomas_id,experiencia_laboral_pragma_id)
values('3456','trabaje como desarrollador devops','PYTHON numero 3','terminar todo','ecosistema','backend','2','1'),
('3457','work as devops developer','PYTHON number 4','finish everything','ecosystem','backend','1','2');

INSERT INTO  otros_estudios_detalle (id,etiquetas,instituto,titulo,idiomas_id,otros_estudios_id)
values('1','DEVOPS','COURSERA','ingeniero de sistemas','2','1'),
('2','DEVOPS','COURSERA','System engineer','1','1');
