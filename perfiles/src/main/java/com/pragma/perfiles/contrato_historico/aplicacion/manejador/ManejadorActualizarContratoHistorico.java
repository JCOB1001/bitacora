package com.pragma.perfiles.contrato_historico.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ContratoHistoricoNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.contrato_historico.aplicacion.comando.ActualizarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.aplicacion.fabrica.FabricaContratoHistorico;
import com.pragma.perfiles.contrato_historico.dominio.UseCase.ContratoHistoricoCrudUseCase;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorActualizarContratoHistorico {

    private final ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase;
    private final FabricaContratoHistorico fabricaContratoHistorico;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public void ejecutar(String idContratoHistorico ,ActualizarContratoHistoricoComando actualizarContratoHistoricoComando){

        ContratoHistorico contratoHistorico = contratoHistoricoCrudUseCase.obtenerPorId(idContratoHistorico);
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(actualizarContratoHistoricoComando.getIdUsuario());

        if(usuario == null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+ actualizarContratoHistoricoComando.getIdUsuario()+" no existe");
        }

        if(contratoHistorico == null){
            throw new ContratoHistoricoNoEncontrado("Este contrato historico no existe en la Base de Datos. ");
        }

        if(!contratoHistorico.getIdUsuario().equals(actualizarContratoHistoricoComando.getIdUsuario())){
            throw new ContratoHistoricoNoEncontrado("El id del usuario enviado no coincide con los registros de la base de dato ");
        }

        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.guardar(usuario);
        ContratoHistorico contratoHistoricoNuevo = fabricaContratoHistorico.actualizarContratoHistorico(actualizarContratoHistoricoComando);
        contratoHistoricoNuevo.setId(idContratoHistorico);
        contratoHistoricoCrudUseCase.actualizar(contratoHistoricoNuevo);
    }

}
