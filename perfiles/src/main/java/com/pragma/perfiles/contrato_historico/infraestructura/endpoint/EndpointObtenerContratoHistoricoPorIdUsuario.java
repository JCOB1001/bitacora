package com.pragma.perfiles.contrato_historico.infraestructura.endpoint;

import com.pragma.perfiles.contrato_historico.aplicacion.manejador.ManejadorObtenerContratoHistoricoPorIdUsuario;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("contrato-historico")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerContratoHistoricoPorIdUsuario {

    private final ManejadorObtenerContratoHistoricoPorIdUsuario manejadorObtenerContratoHistoricoPorIdUsuario;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<Stream<ContratoHistorico>> ejecutarObtenerContratoHistoricoPorIdUsuario(@NotNull @PathVariable String idUsuario){
        return manejadorObtenerContratoHistoricoPorIdUsuario.ejecutar(idUsuario);
    }
}
