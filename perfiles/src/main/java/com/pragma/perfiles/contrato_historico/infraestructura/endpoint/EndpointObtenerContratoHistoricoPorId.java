package com.pragma.perfiles.contrato_historico.infraestructura.endpoint;

import com.pragma.perfiles.contrato_historico.aplicacion.manejador.ManejadorObtenerContratoHistoricoPorId;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("contrato-historico")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerContratoHistoricoPorId {
    private final ManejadorObtenerContratoHistoricoPorId manejadorObtenerContratoHistoricoPorId;

    @GetMapping("/obtener/{idContratoHistorico}")
    public ObjetoRespuesta<ContratoHistorico> obtenerPoId(@NotNull @PathVariable String idContratoHistorico){

        ObjetoRespuesta<ContratoHistorico> contratoHistoricoObjetoRespuesta= manejadorObtenerContratoHistoricoPorId.ejecutar(idContratoHistorico);
        return new ObjetoRespuesta<>(HttpStatus.OK,contratoHistoricoObjetoRespuesta.getDato());
    }
}
