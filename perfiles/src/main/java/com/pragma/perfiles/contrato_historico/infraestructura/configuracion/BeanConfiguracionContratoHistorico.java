package com.pragma.perfiles.contrato_historico.infraestructura.configuracion;

import com.pragma.perfiles.contrato_historico.aplicacion.fabrica.FabricaContratoHistorico;
import com.pragma.perfiles.contrato_historico.aplicacion.fabrica.impl.ContratoHistoricoMapperActualizarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.aplicacion.fabrica.impl.ContratoHistoricoMapperGuardarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.aplicacion.manejador.ManejadorActualizarContratoHistorico;
import com.pragma.perfiles.contrato_historico.aplicacion.manejador.ManejadorEliminarContratoHistorico;
import com.pragma.perfiles.contrato_historico.aplicacion.manejador.ManejadorGuardarContratoHistorico;
import com.pragma.perfiles.contrato_historico.aplicacion.manejador.ManejadorObtenerContratoHistoricoPorId;
import com.pragma.perfiles.contrato_historico.aplicacion.manejador.ManejadorObtenerContratoHistoricoPorIdUsuario;
import com.pragma.perfiles.contrato_historico.dominio.UseCase.ContratoHistoricoCrudUseCase;
import com.pragma.perfiles.contrato_historico.dominio.repositorio.ContratoHistoricoRepositorio;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguracionContratoHistorico {

    @Bean
    public ManejadorActualizarContratoHistorico manejadorActualizarContratoHistorico(
            ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase,
            FabricaContratoHistorico fabricaContratoHistorico,
            UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorActualizarContratoHistorico(contratoHistoricoCrudUseCase,
                fabricaContratoHistorico,
                usuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerContratoHistoricoPorId manejadorObtenerContratoHistoricoPorId(
            ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase){
       return new ManejadorObtenerContratoHistoricoPorId(contratoHistoricoCrudUseCase);
    }

    @Bean
    public ManejadorObtenerContratoHistoricoPorIdUsuario manejadorObtenerContratoHistoricoPorIdUsuario(
            UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerContratoHistoricoPorIdUsuario(
                usuarioCrudUseCase);
    }

    @Bean
    public ManejadorEliminarContratoHistorico manejadorEliminarContratoHistorico(ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase,
                                                                                 UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorEliminarContratoHistorico(contratoHistoricoCrudUseCase,usuarioCrudUseCase);
    }

    @Bean
    public ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase(
            ContratoHistoricoRepositorio contratoHistoricoRepositorio){
        return new ContratoHistoricoCrudUseCase(contratoHistoricoRepositorio);
    }

    @Bean
    public FabricaContratoHistorico fabricaContratoHistorico(
            ContratoHistoricoMapperActualizarContratoHistoricoComando contratoHistoricoMapperActualizarContratoHistoricoComando,
            ContratoHistoricoMapperGuardarContratoHistoricoComando contratoHistoricoMapperGuardarContratoHistoricoComando){

        return new FabricaContratoHistorico(contratoHistoricoMapperActualizarContratoHistoricoComando,
                contratoHistoricoMapperGuardarContratoHistoricoComando);

    }
    @Bean
    public ManejadorGuardarContratoHistorico manejadorGuardarContratoHistorico(ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase,
                                                                               FabricaContratoHistorico fabricaContratoHistorico,
                                                                               UsuarioCrudUseCase usuarioCrudUseCase){

        return new ManejadorGuardarContratoHistorico(contratoHistoricoCrudUseCase,fabricaContratoHistorico,
                usuarioCrudUseCase);
    }
}
