package com.pragma.perfiles.contrato_historico.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerContratoHistoricoPorIdUsuario {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Stream<ContratoHistorico>> ejecutar(String idUsuario){
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }
        ContratoHistorico contrato = new ContratoHistorico(null,
                usuario.getId(),
                usuario.getFechaInicioContrato(),
                null,
                usuario.getTipoContratoId(),
                ContratoHistoricoEstado.ACTIVO);

        Stream<ContratoHistorico> contratoHistoricoStream = Stream.of(contrato);
        return new ObjetoRespuesta<>(HttpStatus.OK, contratoHistoricoStream);
    }
}
