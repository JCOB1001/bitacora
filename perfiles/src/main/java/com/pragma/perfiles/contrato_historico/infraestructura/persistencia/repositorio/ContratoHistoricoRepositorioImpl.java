package com.pragma.perfiles.contrato_historico.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import com.pragma.perfiles.contrato_historico.dominio.repositorio.ContratoHistoricoRepositorio;
import com.pragma.perfiles.contrato_historico.infraestructura.persistencia.builder.ContratoHistoricoMapperContratoHistoricoEntity;
import com.pragma.perfiles.contrato_historico.infraestructura.persistencia.dao.ContratoHistoricoDao;
import com.pragma.perfiles.contrato_historico.infraestructura.persistencia.entidad.ContratoHistoricoEntity;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class ContratoHistoricoRepositorioImpl extends CrudRepositorioBaseImpl<String, ContratoHistorico, ContratoHistoricoEntity> implements ContratoHistoricoRepositorio {

    private final ContratoHistoricoDao contratoHistoricoDao;
    private final ContratoHistoricoMapperContratoHistoricoEntity contratoHistoricoMapperContratoHistoricoEntity;


    @Override
    protected ContratoHistoricoDao obtenerRepositorio() { return contratoHistoricoDao; }

    @Override
    protected ContratoHistoricoMapperContratoHistoricoEntity obtenerConversionBase() {
        return contratoHistoricoMapperContratoHistoricoEntity;
    }

    @Override
    public Stream<ContratoHistorico> findByUsuarioId(String usuarioId) {
        return contratoHistoricoMapperContratoHistoricoEntity.rightToLeft(
                contratoHistoricoDao.findByUsuarioId(usuarioId)
        );
    }

    @Override
    public ContratoHistorico findByUsuarioAndEstado(String usuarioId, ContratoHistoricoEstado estado) {
        return contratoHistoricoMapperContratoHistoricoEntity.rightToLeft(
                contratoHistoricoDao.findByUsuarioIdAndEstado(usuarioId, estado)
        );
    }
}
