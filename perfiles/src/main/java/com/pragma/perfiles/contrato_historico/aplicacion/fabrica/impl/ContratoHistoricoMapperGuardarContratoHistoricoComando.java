package com.pragma.perfiles.contrato_historico.aplicacion.fabrica.impl;

import com.pragma.perfiles.contrato_historico.aplicacion.comando.GuardarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ContratoHistoricoMapperGuardarContratoHistoricoComando extends ConvertidorBase<ContratoHistorico, GuardarContratoHistoricoComando> {
}
