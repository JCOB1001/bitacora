package com.pragma.perfiles.contrato_historico.aplicacion.fabrica;

import com.pragma.perfiles.contrato_historico.aplicacion.comando.ActualizarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.aplicacion.comando.GuardarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.aplicacion.fabrica.impl.ContratoHistoricoMapperActualizarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.aplicacion.fabrica.impl.ContratoHistoricoMapperGuardarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaContratoHistorico {

    private final ContratoHistoricoMapperActualizarContratoHistoricoComando contratoHistoricoMapperActualizarContratoHistorico;
    private final ContratoHistoricoMapperGuardarContratoHistoricoComando contratoHistoricoMapperGuardarContratoHistorico;

    public ContratoHistorico guardarContratoHistorico(GuardarContratoHistoricoComando guardarContratoHistoricoComando){
        return contratoHistoricoMapperGuardarContratoHistorico.rightToLeft(guardarContratoHistoricoComando);
    }

    public ContratoHistorico actualizarContratoHistorico(ActualizarContratoHistoricoComando actualizarContratoHistoricoComando){
        return contratoHistoricoMapperActualizarContratoHistorico.rightToLeft(actualizarContratoHistoricoComando);
    }

}
