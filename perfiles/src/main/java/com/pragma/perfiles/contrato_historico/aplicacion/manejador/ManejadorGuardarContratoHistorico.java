package com.pragma.perfiles.contrato_historico.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.contrato_historico.aplicacion.comando.GuardarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.aplicacion.fabrica.FabricaContratoHistorico;
import com.pragma.perfiles.contrato_historico.dominio.UseCase.ContratoHistoricoCrudUseCase;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorGuardarContratoHistorico {

    private final ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase;
    private final FabricaContratoHistorico fabricaContratoHistorico;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public void ejecutar(GuardarContratoHistoricoComando guardarContratoHistoricoComando){
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(guardarContratoHistoricoComando.getIdUsuario());

        if(usuario == null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+ guardarContratoHistoricoComando.getIdUsuario()+" no existe");
        }
        ContratoHistorico contratoHistoricoNuevo = fabricaContratoHistorico.guardarContratoHistorico(guardarContratoHistoricoComando);
        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.actualizar(usuario);
        contratoHistoricoCrudUseCase.guardar(contratoHistoricoNuevo);
    }
}
