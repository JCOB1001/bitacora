package com.pragma.perfiles.contrato_historico.dominio.repositorio;

import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;

public interface ContratoHistoricoRepositorio extends CrudRepositorioBase<String, ContratoHistorico> {
    Stream<ContratoHistorico> findByUsuarioId(String usuarioId);
    ContratoHistorico findByUsuarioAndEstado(String usuarioId, ContratoHistoricoEstado estado);
}
