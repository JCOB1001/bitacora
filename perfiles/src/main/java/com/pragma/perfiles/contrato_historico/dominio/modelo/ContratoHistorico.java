package com.pragma.perfiles.contrato_historico.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ContratoHistorico extends Modelo<String> {

    private String idUsuario;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private String idTipoContrato;
    private ContratoHistoricoEstado estado;

    public ContratoHistorico() { super(null); }

    public ContratoHistorico(String id, String idUsuario, LocalDate fechaInicio, LocalDate fechaFin, String idTipoContrato, ContratoHistoricoEstado estado) {
        super(id);
        this.idUsuario = idUsuario;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.idTipoContrato = idTipoContrato;
        this.estado = estado;
    }
}
