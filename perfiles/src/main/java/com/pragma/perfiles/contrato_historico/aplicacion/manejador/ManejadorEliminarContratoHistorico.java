package com.pragma.perfiles.contrato_historico.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ContratoHistoricoNoEncontrado;
import com.pragma.perfiles.contrato_historico.dominio.UseCase.ContratoHistoricoCrudUseCase;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorEliminarContratoHistorico {

    private final ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public void ejecutar(String idContratoHistorico){
        ContratoHistorico contratoHistorico = contratoHistoricoCrudUseCase.obtenerPorId(idContratoHistorico);

        if(contratoHistorico == null){
            throw new ContratoHistoricoNoEncontrado("Esta contrato historico no existe");
        }

        Usuario usuario=usuarioCrudUseCase.obtenerPorId(contratoHistorico.getIdUsuario());
        if(usuario!=null){
            usuario.setFechaUltimaActualizacion(LocalDate.now());
            usuarioCrudUseCase.actualizar(usuario);
        }
        contratoHistoricoCrudUseCase.eliminarPorId(idContratoHistorico);
    }
}
