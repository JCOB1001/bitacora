package com.pragma.perfiles.contrato_historico.infraestructura.endpoint;

import com.pragma.perfiles.contrato_historico.aplicacion.manejador.ManejadorEliminarContratoHistorico;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("contrato-historico")
@RequiredArgsConstructor
@Validated
public class EndpointEliminarContratoHistorico {
    private final ManejadorEliminarContratoHistorico manejadorEliminarContratoHistorico;

    @DeleteMapping("/{idContratoHistorico}")
    public RespuestaBase ejecutarEliminarContratoHistorico(@NotNull @PathVariable String idContratoHistorico){
        manejadorEliminarContratoHistorico.ejecutar(idContratoHistorico);
        return RespuestaBase.ok("Borrado Exitosamente");
    }
}
