package com.pragma.perfiles.contrato_historico.aplicacion.comando;

import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuardarContratoHistoricoComando {

    private String idUsuario;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private String idTipoContrato;
    private ContratoHistoricoEstado estado;

}
