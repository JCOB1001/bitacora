package com.pragma.perfiles.contrato_historico.infraestructura.persistencia.entidad;

import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.entidad.TipoContratoEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "contrato_historico")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContratoHistoricoEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos {
        String USUARIO = "usuario";
        String FECHA_INICIO = "fechaInicio";
        String FECHA_FIN = "fechaFin";
        String TIPOCONTRATO = "tipoContrato";
        String ESTADO = "estado";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_contrato_historico_usuario_usuario_id"))
    private UsuarioEntity usuario;

    @Column(name = "fecha_inicio")
    private LocalDate fechaInicio;

    @Column(name = "fecha_fin")
    private LocalDate fechaFin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipo_contrato_id", foreignKey = @ForeignKey(name = "fk_contrato_historico_tipo_contrato_tipo_contrato_id"))
    private TipoContratoEntity tipoContrato;

    @Column(name = "estado")
    @Enumerated(value = EnumType.ORDINAL)
    private ContratoHistoricoEstado estado;

}
