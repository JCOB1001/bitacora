package com.pragma.perfiles.contrato_historico.dominio.UseCase;

import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import com.pragma.perfiles.contrato_historico.dominio.repositorio.ContratoHistoricoRepositorio;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class ContratoHistoricoCrudUseCase  extends CrudUseCaseCommon<ContratoHistoricoRepositorio, ContratoHistorico, String> {

    private final ContratoHistoricoRepositorio contratoHistoricoRepositorio;

    public ContratoHistoricoCrudUseCase(ContratoHistoricoRepositorio contratoHistoricoRepositorio) {
        super(contratoHistoricoRepositorio);
        this.contratoHistoricoRepositorio = contratoHistoricoRepositorio;
    }

    public Stream<ContratoHistorico> findByUsuarioId(String usuarioId){
        return contratoHistoricoRepositorio.findByUsuarioId(usuarioId);
    }

    public ContratoHistorico findByUsuarioIdAndEstado(String usuarioId, ContratoHistoricoEstado estado){
        return contratoHistoricoRepositorio.findByUsuarioAndEstado(usuarioId, estado);
    }
}
