package com.pragma.perfiles.contrato_historico.infraestructura.endpoint;

import com.pragma.perfiles.contrato_historico.aplicacion.comando.GuardarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.aplicacion.manejador.ManejadorGuardarContratoHistorico;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("contrato-historico")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarContratoHistorico {

    private final ManejadorGuardarContratoHistorico manejadorGuardarContratoHistorico;

    @PostMapping
    public RespuestaBase ejecutarGuardarInformacionLaboralPragma(@NotNull @RequestBody GuardarContratoHistoricoComando guardarContratoHistoricoComando){
        manejadorGuardarContratoHistorico.ejecutar(guardarContratoHistoricoComando);
        return RespuestaBase.ok("Guardado Exitosamente");
    }
}
