package com.pragma.perfiles.contrato_historico.infraestructura.endpoint;

import com.pragma.perfiles.contrato_historico.aplicacion.comando.ActualizarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.aplicacion.manejador.ManejadorActualizarContratoHistorico;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("contrato-historico")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarContratoHistorico {
    private final ManejadorActualizarContratoHistorico manejadorActualizarContratoHistorico;

    @PutMapping("/{idContratoHistorico}")
    public RespuestaBase ejecutarActualizarExperienciaLaboralPragma(@NotNull @PathVariable String idContratoHistorico,
                                                                    @NotNull @RequestBody ActualizarContratoHistoricoComando actualizarContratoHistoricoComando){
        manejadorActualizarContratoHistorico.ejecutar(idContratoHistorico, actualizarContratoHistoricoComando);
        return RespuestaBase.ok("Actualizado Exitosamente");
    }
}
