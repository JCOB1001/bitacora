package com.pragma.perfiles.contrato_historico.infraestructura.persistencia.builder;

import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.contrato_historico.infraestructura.persistencia.entidad.ContratoHistoricoEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface ContratoHistoricoMapperContratoHistoricoEntity extends ConvertidorBase<ContratoHistorico, ContratoHistoricoEntity> {

    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "idUsuario", target = "usuario.id"),
            @Mapping(source = "idTipoContrato", target = "tipoContrato.id")
    })
    ContratoHistoricoEntity leftToRight(ContratoHistorico contratoHistorico);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "usuario.id", target = "idUsuario"),
            @Mapping(source = "tipoContrato.id", target = "idTipoContrato")
    })
    ContratoHistorico rightToLeft(ContratoHistoricoEntity contratoHistoricoEntity);

}
