package com.pragma.perfiles.contrato_historico.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ContratoHistoricoNoEncontrado;
import com.pragma.perfiles.contrato_historico.dominio.UseCase.ContratoHistoricoCrudUseCase;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerContratoHistoricoPorId {
    private final ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase;

    public ObjetoRespuesta<ContratoHistorico> ejecutar(String idContratoHistorico){
        ContratoHistorico contratoHistorico = contratoHistoricoCrudUseCase.obtenerPorId(idContratoHistorico);

        if(contratoHistorico == null){
            throw new ContratoHistoricoNoEncontrado("El contrato historico no se encuentra registrado");
        }
        return new ObjetoRespuesta<>(contratoHistorico);
    }
}
