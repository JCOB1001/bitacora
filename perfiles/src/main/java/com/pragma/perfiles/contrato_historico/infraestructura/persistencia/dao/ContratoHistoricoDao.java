package com.pragma.perfiles.contrato_historico.infraestructura.persistencia.dao;

import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import com.pragma.perfiles.contrato_historico.infraestructura.persistencia.entidad.ContratoHistoricoEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContratoHistoricoDao extends JpaRepository<ContratoHistoricoEntity, String> {

    Iterable<ContratoHistoricoEntity> findByUsuarioId(String usuarioId);

    ContratoHistoricoEntity findByUsuarioIdAndEstado(String usuarioId, ContratoHistoricoEstado estado);
}
