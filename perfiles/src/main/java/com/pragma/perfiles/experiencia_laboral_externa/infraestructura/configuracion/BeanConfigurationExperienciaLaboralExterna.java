package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.configuracion;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.barra_progreso.dominio.useCase.PesoValoresInfoUsuarioCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.fabrica.FabricaExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.fabrica.impl.GuardarExperienciaLaboralExternaComandoMapper;
import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.fabrica.impl.ExperienciaLaboralExternaMapperActualizarExperienciaLaboralExternaComando;
import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador.*;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.repositorio.ExperienciaLaboralExternaRepositorio;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaDetalleCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationExperienciaLaboralExterna {

    @Bean
    public ManejadorGuardarExperienciaLaboralExterna ManejadorGuardarExperienciaLaboralExterna(ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase,
                                                                                               FabricaExperienciaLaboralExterna fabricaInformacionLaboralExterna,
                                                                                               UsuarioCrudUseCase usuarioCrudUseCase, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso,
                                                                                               ExperienciaLaboralExternaDetalleCrudUseCase experienciaLaboralExternaDetalleCrudUseCase){
        return new ManejadorGuardarExperienciaLaboralExterna(experienciaLaboralExternaCrudUseCase,
                fabricaInformacionLaboralExterna,
                usuarioCrudUseCase, manejadorCalcularValorBarraProgreso,
                experienciaLaboralExternaDetalleCrudUseCase);
    }

    @Bean
    public ManejadorActualizarExperinciaLaboralExterna actualizarExperinciaLaboralExterna(ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase,
                                                                                          FabricaExperienciaLaboralExterna fabricaInformacionLaboralExterna,
                                                                                          UsuarioCrudUseCase usuarioCrudUseCase, ExperienciaLaboralExternaDetalleCrudUseCase experienciaLaboralExternaDetalleCrudUseCase){
        return new ManejadorActualizarExperinciaLaboralExterna(experienciaLaboralExternaCrudUseCase,
                fabricaInformacionLaboralExterna,
                usuarioCrudUseCase,experienciaLaboralExternaDetalleCrudUseCase);
    }

    @Bean
    public ManejadorObtenerExperienciaLaboralExternaPorId manejadorObtenerExperienciaLaboralExternaPorId(ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase){
        return new ManejadorObtenerExperienciaLaboralExternaPorId(experienciaLaboralExternaCrudUseCase);
    }

    @Bean
    public ManejadorObtenerExperienciaLaboralExternaPorIdUsuario manejadorObtenerExperienciaLaboralExternaPorIdUsuario( ExperienciaLaboralExternaCrudUseCase
                                                                                                                                    experienciaLaboralExternaCrudUseCase,
                                                                                                                        UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerExperienciaLaboralExternaPorIdUsuario(
                experienciaLaboralExternaCrudUseCase,
                usuarioCrudUseCase
        );
    }

    @Bean
    public ManejadorObtenerExperienciaLaboralExternaPorIdUsuarioOrdenado manejadorObtenerExperienciaLaboralExternaPorIdUsuarioOrdenado( ExperienciaLaboralExternaCrudUseCase
                                                                                                                                experienciaLaboralExternaCrudUseCase,
                                                                                                                        UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerExperienciaLaboralExternaPorIdUsuarioOrdenado(
                experienciaLaboralExternaCrudUseCase,
                usuarioCrudUseCase
        );
    }

    @Bean
    public ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase(
            ExperienciaLaboralExternaRepositorio experienciaLaboralExternaRepositorio){
        return new ExperienciaLaboralExternaCrudUseCase(experienciaLaboralExternaRepositorio);
    }

    @Bean
    public FabricaExperienciaLaboralExterna fabricaInformacionLaboralExterna(GuardarExperienciaLaboralExternaComandoMapper guardarExperienciaLaboralExternaComandoMapper,
                                                                             ExperienciaLaboralExternaMapperActualizarExperienciaLaboralExternaComando
                                                                                     experienciaLaboralExternaMapperActualizarExperienciaLaboralExternaComando){
        return new FabricaExperienciaLaboralExterna(guardarExperienciaLaboralExternaComandoMapper,
                experienciaLaboralExternaMapperActualizarExperienciaLaboralExternaComando);
    }

    @Bean
    public ManejadorEliminarExperienciaLaboralExterna manejadorEliminarExperienciaLaboralExterna(ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase, UsuarioCrudUseCase usuarioCrudUseCase, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso){
        return new ManejadorEliminarExperienciaLaboralExterna(experienciaLaboralExternaCrudUseCase, usuarioCrudUseCase, manejadorCalcularValorBarraProgreso);
    }


}
