package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.comando.GuardarExperienciaLaboralExternaComando;
import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador.ManejadorGuardarExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(TextosComunes.RUTA_CONTROLADOR)
@RequiredArgsConstructor
@Validated
public class EndpointGuardarExperienciaLaboralExterna {

    private final ManejadorGuardarExperienciaLaboralExterna manejadorGuardarExperienciaLaboralExterna;

    @PostMapping
    public RespuestaBase ejecutarGuardarExperienciaLaboralExterna(@NotNull @RequestBody GuardarExperienciaLaboralExternaComando guardarExperienciaLaboralExternaComando){
        manejadorGuardarExperienciaLaboralExterna.ejecutar(guardarExperienciaLaboralExternaComando);
        return RespuestaBase.ok("Guardado Exitoso");
    }


}
