package com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.dominio.useCase.PesoValoresInfoUsuarioCrudUseCase;
import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.comun.infraestructura.error.ExperienciaLaboralNoEncontrada;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioDetalleCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.comando.ActualizarExperienciaLaboralExternaComando;
import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.fabrica.FabricaExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExternaDetalle;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaDetalleCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ManejadorActualizarExperinciaLaboralExterna {


  private final ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase;
  private final FabricaExperienciaLaboralExterna fabricaExperienciaLaboralExterna;
  private final UsuarioCrudUseCase usuarioCrudUseCase;
  private final ExperienciaLaboralExternaDetalleCrudUseCase experienciaLaboralExternaDetalleCrudUseCase;


    public ObjetoRespuesta<String> ejecutar(String idExperienciaLaboralExterna, ActualizarExperienciaLaboralExternaComando actualizarExperienciaLaboralExternaComando){
        String idIdioma = HttpRequestContextHolder.getIdioma();
        ExperienciaLaboralExterna experienciaLaboralExterna = experienciaLaboralExternaCrudUseCase.obtenerPorId(idExperienciaLaboralExterna);
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(actualizarExperienciaLaboralExternaComando.getIdUsuario());
        ExperienciaLaboralExternaDetalle experienciaLaboralExternaDetalle = experienciaLaboralExternaDetalleCrudUseCase.findByExperienciaLaboralExternaIdAndIdiomaId(idExperienciaLaboralExterna,idIdioma);

        if(experienciaLaboralExterna==null){
            throw new ExperienciaLaboralNoEncontrada("La experiencia laboral externa con el id: "+ idExperienciaLaboralExterna+" no fue encontrada");
        }

        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+actualizarExperienciaLaboralExternaComando.getIdUsuario()+" no existe");
        }

        if(!experienciaLaboralExterna.getIdUsuario().equals(actualizarExperienciaLaboralExternaComando.getIdUsuario())){
            throw new ExperienciaLaboralNoEncontrada("El id del usuario enviado no coincide con los registros de la base de dato ");
        }

        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.actualizar(usuario);
        ExperienciaLaboralExterna experienciaLaboralExternaNueva= fabricaExperienciaLaboralExterna.actualizarExperienciaLaboralExterna(actualizarExperienciaLaboralExternaComando);
        experienciaLaboralExternaNueva.setId(idExperienciaLaboralExterna);
        if(experienciaLaboralExternaDetalle==null){
            experienciaLaboralExternaDetalle = new ExperienciaLaboralExternaDetalle(null,experienciaLaboralExternaNueva.getId(),idIdioma,experienciaLaboralExternaNueva.getRol(),experienciaLaboralExternaNueva.getLogros(),
                    experienciaLaboralExternaNueva.getHerramientasUtilizadas(),experienciaLaboralExternaNueva.getDescripcion());
            experienciaLaboralExternaDetalleCrudUseCase.guardar(experienciaLaboralExternaDetalle);
        }else{
            experienciaLaboralExternaDetalle.setDescripcion(experienciaLaboralExternaNueva.getDescripcion());
            experienciaLaboralExternaDetalle.setHerramientasUtilizadas(experienciaLaboralExternaNueva.getHerramientasUtilizadas());
            experienciaLaboralExternaDetalle.setLogros(experienciaLaboralExternaNueva.getLogros());
            experienciaLaboralExternaDetalle.setRol(experienciaLaboralExternaNueva.getRol());
            experienciaLaboralExternaDetalleCrudUseCase.actualizar(experienciaLaboralExternaDetalle);
        }




        experienciaLaboralExternaCrudUseCase.actualizar(experienciaLaboralExternaNueva);


        return new ObjetoRespuesta<String>("Actualizado exitosamente");
    }

}
