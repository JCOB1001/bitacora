package com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase;

import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.repositorio.ExperienciaLaboralExternaRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class ExperienciaLaboralExternaCrudUseCase extends CrudUseCaseCommon <ExperienciaLaboralExternaRepositorio, ExperienciaLaboralExterna,String> {

    private final ExperienciaLaboralExternaRepositorio experienciaLaboralExternaRepositorio;

    public ExperienciaLaboralExternaCrudUseCase(ExperienciaLaboralExternaRepositorio experienciaLaboralExternaRepositorio){
        super(experienciaLaboralExternaRepositorio);
        this.experienciaLaboralExternaRepositorio=experienciaLaboralExternaRepositorio;
    }

    public Stream<ExperienciaLaboralExterna> findByUsuarioId(String usuarioId){
        return experienciaLaboralExternaRepositorio.findByUsuarioId(usuarioId);
    }

    public Stream<ExperienciaLaboralExterna> findByUsuarioId(String usuarioId, String ordenFecha){
        return experienciaLaboralExternaRepositorio.findByUsuarioId(usuarioId, ordenFecha);
    }

}
