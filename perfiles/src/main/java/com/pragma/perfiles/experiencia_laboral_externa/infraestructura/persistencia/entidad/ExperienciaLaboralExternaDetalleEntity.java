package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "experiencia_laboral_externa_detalle")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExperienciaLaboralExternaDetalleEntity implements IdEntidad<String> {
    public interface ATRIBUTOS extends Atributos{
        String IDIOMA ="idioma";
        String ROL="rol";
        String LOGROS="logros";
        String HERRAMIENTAS_UTILIZADAS = "herramientasUtilizadas";
        String DESCRIPCION = "descripcion";
        String EXPERIENCIALABORALEXTERNA = "experienciaLaboralExterna";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idiomas_id", foreignKey = @ForeignKey(name = "fk_experiencia_laboral_externa_detalle_idiomas_idiomas_id"))
    private IdiomasEntity idioma;
    private String rol;
    private String logros;
    private String herramientasUtilizadas;
    private String descripcion;
    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "experiencia_laboral_externa_id", foreignKey = @ForeignKey(name = "fk_experiencia_laboral_externa_experiencia_laboral_externa_id"))
    private ExperienciaLaboralExternaEntity experienciaLaboralExterna;
}
