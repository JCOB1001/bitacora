package com.pragma.perfiles.experiencia_laboral_externa.aplicacion.fabrica.impl;

import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.comando.GuardarExperienciaLaboralExternaComando;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GuardarExperienciaLaboralExternaComandoMapper extends ConvertidorBase<ExperienciaLaboralExterna, GuardarExperienciaLaboralExternaComando> {
}
