package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador.ManejadorEliminarExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(TextosComunes.RUTA_CONTROLADOR)
@RequiredArgsConstructor
@Validated
public class EndpointEliminarExperienciaLaboralExterna {

    private final ManejadorEliminarExperienciaLaboralExterna manejadorEliminarExperienciaLaboralExterna;

    @DeleteMapping("/{idExperienciaLaboralExterna}")
    public ObjetoRespuesta<String> eliminarExperinciaFamiliarExterna(@PathVariable String idExperienciaLaboralExterna){
        return manejadorEliminarExperienciaLaboralExterna.ejecutar(idExperienciaLaboralExterna);
    }
}
