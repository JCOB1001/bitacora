package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador.ManejadorObtenerExperienciaLaboralExternaPorIdUsuario;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping(TextosComunes.RUTA_CONTROLADOR)
@RequiredArgsConstructor
@Validated
public class EndpointObtenerExperienciaLaboralExternaPorIdUsuario {

    private final ManejadorObtenerExperienciaLaboralExternaPorIdUsuario manejadorObtenerExperienciaLaboralExternaPorIdUsuario;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<Stream<ExperienciaLaboralExterna>> ejecutarObtenerExperienciaLaboralExternaPorIdUsuario(@NotNull @PathVariable String idUsuario){
        return manejadorObtenerExperienciaLaboralExternaPorIdUsuario.ejecutar(idUsuario);
    }

}
