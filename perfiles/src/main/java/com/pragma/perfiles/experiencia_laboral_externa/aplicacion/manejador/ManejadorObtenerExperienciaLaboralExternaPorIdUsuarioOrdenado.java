package com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerExperienciaLaboralExternaPorIdUsuarioOrdenado {

    private final ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Stream<ExperienciaLaboralExterna>> ejecutar(String idUsuario, String ordenFecha){
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }
        Stream<ExperienciaLaboralExterna> experienciaLaboralExternaStream = experienciaLaboralExternaCrudUseCase.findByUsuarioId(usuario.getId(), ordenFecha);
        return new ObjetoRespuesta<>(HttpStatus.OK, experienciaLaboralExternaStream);
    }


}
