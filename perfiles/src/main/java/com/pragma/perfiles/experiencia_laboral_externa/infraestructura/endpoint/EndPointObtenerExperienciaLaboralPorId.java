package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador.ManejadorObtenerExperienciaLaboralExternaPorId;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(TextosComunes.RUTA_CONTROLADOR)
@RequiredArgsConstructor
@Validated
public class EndPointObtenerExperienciaLaboralPorId {

    private final ManejadorObtenerExperienciaLaboralExternaPorId manejadorObtenerExperienciaLaboralExternaPorId;

    @GetMapping("/obtener/{idExperienciaLaboralExterna}")
    public ObjetoRespuesta<ExperienciaLaboralExterna> obtenerPoId(@NotNull @PathVariable String idExperienciaLaboralExterna){

        ObjetoRespuesta<ExperienciaLaboralExterna> informacionFamiliarObjetoRespuesta=manejadorObtenerExperienciaLaboralExternaPorId.ejecutar(idExperienciaLaboralExterna);
        return new ObjetoRespuesta<ExperienciaLaboralExterna>(HttpStatus.OK,informacionFamiliarObjetoRespuesta.getDato());
    }

}
