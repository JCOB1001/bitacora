package com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ExperienciaLaboralNoEncontrada;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerExperienciaLaboralExternaPorId {

    private final ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase;

    public ObjetoRespuesta<ExperienciaLaboralExterna> ejecutar(String idExperienciaLaboralExterna){
        ExperienciaLaboralExterna experienciaLaboralExterna = experienciaLaboralExternaCrudUseCase.obtenerPorId(idExperienciaLaboralExterna);

        if(experienciaLaboralExterna==null){
            throw new ExperienciaLaboralNoEncontrada("La experiencia laboral no se encuentra registrada");
        }

        return new ObjetoRespuesta<ExperienciaLaboralExterna>(experienciaLaboralExterna);
    }
}
