package com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ExperienciaLaboralExternaDetalle extends Modelo<String> {
    private String idExperienciaLaboralExterna;
    private String idIdioma;
    private String rol;;
    private String logros;
    private String herramientasUtilizadas;
    private String descripcion;

    public ExperienciaLaboralExternaDetalle (){ super(null);}

    public ExperienciaLaboralExternaDetalle(String id, String idExperienciaLaboralExterna,String idIdioma, String rol, String logros, String herramientasUtilizadas, String descripcion) {
        super(id);
        this.idExperienciaLaboralExterna = idExperienciaLaboralExterna;
        this.idIdioma = idIdioma;
        this.rol = rol;
        this.logros = logros;
        this.descripcion = descripcion;
        this.herramientasUtilizadas = herramientasUtilizadas;;
    }
}
