package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.configuracion;

import com.pragma.perfiles.estudio.dominio.repositorio.EstudioDetalleRepositorio;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioDetalleCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.repositorio.ExperienciaLaboralExternaDetalleRepositorio;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaDetalleCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationExperienciaLaboralExternaDetalle {
    @Bean
    public ExperienciaLaboralExternaDetalleCrudUseCase experienciaLaboralExternaDetalleCrudUseCase(ExperienciaLaboralExternaDetalleRepositorio experienciaLaboralExternaDetalleRepositorio){
        return  new ExperienciaLaboralExternaDetalleCrudUseCase(experienciaLaboralExternaDetalleRepositorio);
    }
}
