package com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.ExperienciaLaboralNoEncontrada;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorEliminarExperienciaLaboralExterna {

    private final ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;


    public ObjetoRespuesta<String> ejecutar(String idExperienciaLaboral){
        ExperienciaLaboralExterna experienciaLaboralExterna = experienciaLaboralExternaCrudUseCase.obtenerPorId(idExperienciaLaboral);
        if(experienciaLaboralExterna == null){
            throw new ExperienciaLaboralNoEncontrada("Esta experiencia laboral no existe en la base de datos");
        }

        Usuario usuario=usuarioCrudUseCase.obtenerPorId(experienciaLaboralExterna.getIdUsuario());
        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+experienciaLaboralExterna.getIdUsuario()+" no se encuntra registrado");
        }

        usuario.setFechaUltimaActualizacion(LocalDate.now());

        experienciaLaboralExternaCrudUseCase.eliminarPorId(idExperienciaLaboral);

        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuarioCrudUseCase.actualizar(usuario);

        ObjetoRespuesta<String> respuesta=new ObjetoRespuesta<String>(HttpStatus.OK,"Eliminado exitosamente");
        return respuesta;
    }
}
