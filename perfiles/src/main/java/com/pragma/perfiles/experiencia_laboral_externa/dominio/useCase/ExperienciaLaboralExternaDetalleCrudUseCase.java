package com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase;

import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExternaDetalle;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.repositorio.ExperienciaLaboralExternaDetalleRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

public class ExperienciaLaboralExternaDetalleCrudUseCase extends CrudUseCaseCommon<ExperienciaLaboralExternaDetalleRepositorio, ExperienciaLaboralExternaDetalle,String> {
    private final ExperienciaLaboralExternaDetalleRepositorio experienciaLaboralExternaDetalleRepositorio;

    public ExperienciaLaboralExternaDetalleCrudUseCase(ExperienciaLaboralExternaDetalleRepositorio experienciaLaboralExternaDetalleRepositorio){
        super(experienciaLaboralExternaDetalleRepositorio);
        this.experienciaLaboralExternaDetalleRepositorio=experienciaLaboralExternaDetalleRepositorio;
    }

    public ExperienciaLaboralExternaDetalle findByExperienciaLaboralExternaIdAndIdiomaId(String experienciaLaboralExternaDetalleId, String idiomaId) {
        return this.experienciaLaboralExternaDetalleRepositorio.findByExperienciaLaboralExternaIdAndIdiomaId(experienciaLaboralExternaDetalleId,idiomaId);
    }
}
