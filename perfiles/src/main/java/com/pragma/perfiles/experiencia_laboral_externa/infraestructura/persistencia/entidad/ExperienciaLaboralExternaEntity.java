package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "experiencia_laboral_externa")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExperienciaLaboralExternaEntity implements IdEntidad<String> {


    public interface ATRIBUTOS extends Atributos{
        String USUARIO ="usuario";
        String EMPRESA="empresa";
        String ROL="rol";
        String FECHA_DE_INGRESO="fechaDeIngreso";
        String FECHA_DE_FINALIZACION="fechaDeFinalizacion";
        String TIEMPO_DE_EXPERIENCIA="tiempoDeExperiencia";
        String LOGROS="logros";
        String HERRAMIENTAS_UTILIZADAS = "herramientasUtilizadas";
        String DESCRIPCION = "descripcion";
        String EXPERIENCIALABORALEXTERNADETALLE = "experienciaLaboralExternaDetalle";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_experiencia_laboral_externa_usuario_usuario_id"))
    private UsuarioEntity usuario;
    private String empresa;
    private String rol;
    private LocalDate fechaDeIngreso;
    private LocalDate fechaDeFinalizacion;
    private long tiempoDeExperiencia;
    private String logros;
    private String herramientasUtilizadas;
    private String descripcion;
    @JsonManagedReference
    @OneToMany(mappedBy = "experienciaLaboralExterna",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ExperienciaLaboralExternaDetalleEntity> experienciaLaboralExternaDetalle;
}
