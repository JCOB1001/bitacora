package com.pragma.perfiles.experiencia_laboral_externa.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarExperienciaLaboralExternaComando {
    private String idUsuario;
    private String empresa;
    private String rol;
    private LocalDate fechaDeIngreso;
    private LocalDate fechaDeFinalizacion;
    private long tiempoDeExperiencia;
    private String logros;
    private String herramientasUtilizadas;
    private String descripcion;
}
