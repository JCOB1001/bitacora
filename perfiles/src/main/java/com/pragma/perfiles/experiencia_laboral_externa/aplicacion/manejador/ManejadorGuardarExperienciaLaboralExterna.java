package com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.barra_progreso.dominio.useCase.PesoValoresInfoUsuarioCrudUseCase;
import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.comando.GuardarExperienciaLaboralExternaComando;
import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.fabrica.FabricaExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExternaDetalle;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaDetalleCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorGuardarExperienciaLaboralExterna {

    private final ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase;
    private final FabricaExperienciaLaboralExterna fabricaInformacionLaboralExterna;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;
    private final ExperienciaLaboralExternaDetalleCrudUseCase experienciaLaboralExternaDetalleCrudUseCase;

    public void ejecutar(GuardarExperienciaLaboralExternaComando guardarExperienciaLaboralExternaComando){
        String idIdioma = HttpRequestContextHolder.getIdioma();
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(guardarExperienciaLaboralExternaComando.getIdUsuario());
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }
        usuario.setFechaUltimaActualizacion(LocalDate.now());

        ExperienciaLaboralExterna experienciaLaboralExterna = fabricaInformacionLaboralExterna.guardarExperienciaLaboralExterna(guardarExperienciaLaboralExternaComando);
        ExperienciaLaboralExterna experienciaLaboralExternaNuevo = experienciaLaboralExternaCrudUseCase.guardar(experienciaLaboralExterna);

        ExperienciaLaboralExternaDetalle experienciaLaboralExternaDetalle = new ExperienciaLaboralExternaDetalle(null,experienciaLaboralExternaNuevo.getId(),idIdioma,experienciaLaboralExterna.getRol(),experienciaLaboralExterna.getLogros()
                ,experienciaLaboralExterna.getHerramientasUtilizadas(),experienciaLaboralExterna.getDescripcion());
        experienciaLaboralExternaDetalleCrudUseCase.guardar(experienciaLaboralExternaDetalle);

        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuarioCrudUseCase.actualizar(usuario);
    }
}
