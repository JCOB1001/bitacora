package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.builder;

import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExternaDetalle;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad.ExperienciaLaboralExternaDetalleEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface ExperienciaExternaDetalleMapperExperienciaExternaDetalleEntity extends ConvertidorBase<ExperienciaLaboralExternaDetalle, ExperienciaLaboralExternaDetalleEntity> {
    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "idExperienciaLaboralExterna", target = "experienciaLaboralExterna.id"),
            @Mapping(source = "idIdioma", target = "idioma.id")
    })
    ExperienciaLaboralExternaDetalleEntity leftToRight(ExperienciaLaboralExternaDetalle experienciaLaboralExternaDetalle);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "experienciaLaboralExterna.id", target = "idExperienciaLaboralExterna"),
            @Mapping(source = "idioma.id", target = "idIdioma")
    })
    ExperienciaLaboralExternaDetalle rightToLeft(ExperienciaLaboralExternaDetalleEntity experienciaLaboralExternaDetalleEntity);
}
