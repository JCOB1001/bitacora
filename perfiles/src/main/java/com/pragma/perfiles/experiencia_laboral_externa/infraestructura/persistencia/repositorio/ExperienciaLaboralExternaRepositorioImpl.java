package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.comun.infraestructura.error.OrdenNoValido;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.repositorio.ExperienciaLaboralExternaRepositorio;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.builder.ExperienciaLaboralExternaMapperExperienciaLaboralExternaEntity;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.dao.ExperienciaLaboralExternaDao;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad.ExperienciaLaboralExternaEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class ExperienciaLaboralExternaRepositorioImpl extends CrudRepositorioBaseImpl<String, ExperienciaLaboralExterna, ExperienciaLaboralExternaEntity> implements ExperienciaLaboralExternaRepositorio {

    private final ExperienciaLaboralExternaDao experienciaLaboralExternaDao;
    private final ExperienciaLaboralExternaMapperExperienciaLaboralExternaEntity experienciaLaboralExternaMapperExperienciaLaboralExternaEntity;

    @Override
    protected ExperienciaLaboralExternaDao obtenerRepositorio(){
        return experienciaLaboralExternaDao;
    }

    @Override
    protected ExperienciaLaboralExternaMapperExperienciaLaboralExternaEntity obtenerConversionBase(){
        return experienciaLaboralExternaMapperExperienciaLaboralExternaEntity;
    }

    @Override
    public Stream<ExperienciaLaboralExterna> findByUsuarioId(String usuarioId) {
        return experienciaLaboralExternaMapperExperienciaLaboralExternaEntity.rightToLeft(
                experienciaLaboralExternaDao.findByUsuarioId(usuarioId)
        );
    }

    @Override
    public Stream<ExperienciaLaboralExterna> findByUsuarioId(String usuarioId, String ordenFecha) {

        if(ordenFecha.toLowerCase().equals("asc")){
            return experienciaLaboralExternaMapperExperienciaLaboralExternaEntity.rightToLeft(
                    experienciaLaboralExternaDao.findByUsuarioIdAsc(usuarioId)
            );

        }

        else if(ordenFecha.toLowerCase().equals("des")){
            return experienciaLaboralExternaMapperExperienciaLaboralExternaEntity.rightToLeft(
                    experienciaLaboralExternaDao.findByUsuarioIdDesc(usuarioId)
            );

        }
        else{
            throw new OrdenNoValido("No se logró ordenar");
        }

    }


}
