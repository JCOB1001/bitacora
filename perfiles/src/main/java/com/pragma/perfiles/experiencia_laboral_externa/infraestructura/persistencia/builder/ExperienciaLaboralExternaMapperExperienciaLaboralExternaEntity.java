package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.builder;

import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioDetalleEntity;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioEntity;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExternaDetalle;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad.ExperienciaLaboralExternaDetalleEntity;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad.ExperienciaLaboralExternaEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.*;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ExperienciaLaboralExternaMapperExperienciaLaboralExternaEntity extends ConvertidorBase<ExperienciaLaboralExterna, ExperienciaLaboralExternaEntity> {
    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "idUsuario", target = "usuario.id")
    )
    ExperienciaLaboralExternaEntity leftToRight(ExperienciaLaboralExterna experienciaLaboralExterna);

    @Named("rightToLeft")
    @Override
    @Mappings(
            @Mapping(source = "usuario.id", target = "idUsuario")
    )
    ExperienciaLaboralExterna rightToLeft(ExperienciaLaboralExternaEntity experienciaLaboralExternaEntity);

    @AfterMapping
    default void afterRightToLeft(@MappingTarget ExperienciaLaboralExterna experienciaLaboralExterna, ExperienciaLaboralExternaEntity experienciaLaboralExternaEntity) {
        if(experienciaLaboralExternaEntity.getExperienciaLaboralExternaDetalle()!=null){
            List<ExperienciaLaboralExternaDetalleEntity> experienciaLaboralExternaDetalle = experienciaLaboralExternaEntity.getExperienciaLaboralExternaDetalle().stream().filter(item->
                    item.getIdioma().getId().equals(HttpRequestContextHolder.getIdioma())).collect(Collectors.toList());
            if(experienciaLaboralExternaDetalle.isEmpty()) {
                experienciaLaboralExternaDetalle.add(new ExperienciaLaboralExternaDetalleEntity());
            }
                    experienciaLaboralExterna.setDescripcion(experienciaLaboralExternaDetalle.get(0).getDescripcion());
                    experienciaLaboralExterna.setHerramientasUtilizadas(experienciaLaboralExternaDetalle.get(0).getHerramientasUtilizadas());
                    experienciaLaboralExterna.setLogros(experienciaLaboralExternaDetalle.get(0).getLogros());
                    experienciaLaboralExterna.setRol(experienciaLaboralExternaDetalle.get(0).getRol());
        }
    }

}
