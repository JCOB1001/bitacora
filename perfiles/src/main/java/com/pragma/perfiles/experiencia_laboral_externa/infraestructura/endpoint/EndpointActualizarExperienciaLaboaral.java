package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.comando.ActualizarExperienciaLaboralExternaComando;
import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.manejador.ManejadorActualizarExperinciaLaboralExterna;

import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(TextosComunes.RUTA_CONTROLADOR)
@RequiredArgsConstructor
@Validated
public class EndpointActualizarExperienciaLaboaral {

    private final ManejadorActualizarExperinciaLaboralExterna manejadorActualizarExperinciaLaboralExterna;

    @PutMapping("/{idInformacionLaboralExterna}")
    public ObjetoRespuesta<String> actualizarInformacionLaboral(@NotNull @PathVariable String idInformacionLaboralExterna,@NotNull @RequestBody
                                                                ActualizarExperienciaLaboralExternaComando actualizarExperienciaLaboralExternaComando){

        return manejadorActualizarExperinciaLaboralExterna.ejecutar(idInformacionLaboralExterna,actualizarExperienciaLaboralExternaComando);
    }
}
