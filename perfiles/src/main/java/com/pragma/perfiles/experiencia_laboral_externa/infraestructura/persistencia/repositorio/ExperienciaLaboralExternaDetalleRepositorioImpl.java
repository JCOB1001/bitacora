package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExternaDetalle;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.repositorio.ExperienciaLaboralExternaDetalleRepositorio;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.builder.ExperienciaExternaDetalleMapperExperienciaExternaDetalleEntity;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.dao.ExperienciaLaboralExternaDetalleDao;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad.ExperienciaLaboralExternaDetalleEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ExperienciaLaboralExternaDetalleRepositorioImpl extends CrudRepositorioBaseImpl<String, ExperienciaLaboralExternaDetalle, ExperienciaLaboralExternaDetalleEntity> implements ExperienciaLaboralExternaDetalleRepositorio {
    private final ExperienciaLaboralExternaDetalleDao experienciaLaboralExternaDetalleDao;
    private final ExperienciaExternaDetalleMapperExperienciaExternaDetalleEntity experienciaExternaDetalleMapperExperienciaExternaDetalleEntity;

    @Override
    public ExperienciaLaboralExternaDetalleDao obtenerRepositorio(){ return experienciaLaboralExternaDetalleDao; }

    @Override
    protected ExperienciaExternaDetalleMapperExperienciaExternaDetalleEntity obtenerConversionBase(){
        return experienciaExternaDetalleMapperExperienciaExternaDetalleEntity;
    }

    public ExperienciaLaboralExternaDetalle findByExperienciaLaboralExternaIdAndIdiomaId(String experienciaLaboralExternaId, String idiomaId){
        return obtenerConversionBase().rightToLeft(experienciaLaboralExternaDetalleDao.findByExperienciaLaboralExternaIdAndIdiomaId(experienciaLaboralExternaId,idiomaId));
    }
}
