package com.pragma.perfiles.experiencia_laboral_externa.dominio.repositorio;

import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;

public interface ExperienciaLaboralExternaRepositorio extends CrudRepositorioBase <String, ExperienciaLaboralExterna>{

    Stream<ExperienciaLaboralExterna> findByUsuarioId(String usuarioId);
    Stream<ExperienciaLaboralExterna> findByUsuarioId(String usuarioId, String ordenFecha);


}
