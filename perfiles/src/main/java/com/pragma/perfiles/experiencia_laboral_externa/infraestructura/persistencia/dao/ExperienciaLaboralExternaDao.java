package com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.dao;

import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad.ExperienciaLaboralExternaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ExperienciaLaboralExternaDao extends JpaRepository<ExperienciaLaboralExternaEntity,String> {

    Iterable<ExperienciaLaboralExternaEntity> findByUsuarioId(String usuarioId);

    @Query("SELECT ele " +
            "FROM ExperienciaLaboralExternaEntity ele " +
            "WHERE ele.usuario.id LIKE ?1 " +
            "ORDER BY ele.fechaDeFinalizacion asc, ele.fechaDeIngreso asc")
    Iterable<ExperienciaLaboralExternaEntity> findByUsuarioIdAsc(String usuarioId);

    @Query("SELECT ele " +
            "FROM ExperienciaLaboralExternaEntity ele " +
            "WHERE ele.usuario.id LIKE ?1 " +
            "ORDER BY ele.fechaDeFinalizacion desc, ele.fechaDeIngreso desc")
    Iterable<ExperienciaLaboralExternaEntity> findByUsuarioIdDesc(String usuarioId);


}
