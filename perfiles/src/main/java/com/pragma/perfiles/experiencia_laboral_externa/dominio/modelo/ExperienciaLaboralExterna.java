package com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ExperienciaLaboralExterna extends Modelo<String> {

   private String idUsuario;
   private String empresa;
   private String rol;
   private LocalDate fechaDeIngreso;
   private LocalDate fechaDeFinalizacion;
   private long tiempoDeExperiencia;
   private String logros;
    private String herramientasUtilizadas;
    private String descripcion;

    public ExperienciaLaboralExterna (){ super(null);}

    public ExperienciaLaboralExterna(String id, String idUsuario, String empresa, String rol, LocalDate fechaDeIngreso, LocalDate fechaDeFinalizacion, long tiempoDeExperiencia, String logros, String herramientasUtilizadas, String descripcion) {
        super(id);
        this.idUsuario = idUsuario;
        this.empresa = empresa;
        this.rol = rol;
        this.fechaDeIngreso = fechaDeIngreso;
        this.fechaDeFinalizacion = fechaDeFinalizacion;
        this.tiempoDeExperiencia = tiempoDeExperiencia;
        this.logros = logros;
        this.descripcion = descripcion;
        this.herramientasUtilizadas = herramientasUtilizadas;;
    }
}
