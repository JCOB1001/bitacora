package com.pragma.perfiles.experiencia_laboral_externa.dominio.repositorio;

import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExternaDetalle;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface ExperienciaLaboralExternaDetalleRepositorio extends CrudRepositorioBase<String, ExperienciaLaboralExternaDetalle> {
    ExperienciaLaboralExternaDetalle findByExperienciaLaboralExternaIdAndIdiomaId(String experienciaLaboralExternaId, String idiomaId);
}
