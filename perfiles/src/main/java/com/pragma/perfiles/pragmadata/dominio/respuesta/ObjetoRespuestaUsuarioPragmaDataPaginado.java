package com.pragma.perfiles.pragmadata.dominio.respuesta;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.pragmadata.dominio.modelo.PragmaData;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.stream.Stream;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ObjetoRespuestaUsuarioPragmaDataPaginado extends ObjetoRespuesta<Stream<PragmaData>> {

    private Long cantidadPaginas;
    private boolean ultimaPagina;
    private  Integer totalElementos;
    private int status;

    public ObjetoRespuestaUsuarioPragmaDataPaginado(Long cantidadPaginas, boolean ultimaPagina, Integer totalElementos, int status, Stream<PragmaData> dato) {
        super(dato);
        this.cantidadPaginas = cantidadPaginas;
        this.ultimaPagina = ultimaPagina;
        this.totalElementos = totalElementos;
        this.status = status;
    }


}
