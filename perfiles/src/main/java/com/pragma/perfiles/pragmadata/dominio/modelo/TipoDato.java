package com.pragma.perfiles.pragmadata.dominio.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoDato {

    IMAGEN(),
    TEXTO(),
    FECHA(),
    GRUPO_SANGUINEO(),
    TALLA_CAMISETA(),
    ESTADO_CIVIL(),
    ESTADO_PROFESION(),
    NA(),
    ESTADO(),
    BOOLEANO(),
    ARRAY_OBJETO();


}
