package com.pragma.perfiles.pragmadata.aplicacion.manejador;

import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorObtenerConocimientosPorId;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.pragmadata.aplicacion.comando.ConsultaPragmaDataComando;
import com.pragma.perfiles.pragmadata.aplicacion.fabrica.FabricaPragmaData;
import com.pragma.perfiles.pragmadata.dominio.modelo.*;
import com.pragma.perfiles.pragmadata.dominio.respuesta.ObjetoRespuestaExportarPragmaData;
import com.pragma.perfiles.pragmadata.infraestructura.clientefeign.FeignArchivosUtilitario;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RequiredArgsConstructor
public class ManejadorExportarDatosPragmaData {

    private final FabricaPragmaData fabricaPragmaData;

    private final FeignArchivosUtilitario feignArchivosUtilitario;

    private final InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase;

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    private final UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase;

    private final UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase;

    public ObjetoRespuestaExportarPragmaData ejecutar(@NotNull List<ConsultaPragmaDataComando> consultaPragmaDataComando,
                                                      List<PragmaData> usuariosPragmaData) throws IOException {

        List<DatosArchivoComando> informacionArchivo = new ArrayList<>();

        List<String> camposUsuario = consultaPragmaDataComando.stream().map(data -> data.getCampoTabla()).collect(Collectors.toList());

        List<PragmaDataArchivo> usuarios = this.fabricaPragmaData.convertPragmaDatatoPragmaDataArchivo(camposUsuario, usuariosPragmaData);
        Map<String, PragmaDataArchivo> usuariosByCorreoEmpresarial = new HashMap<>();

        usuarios.forEach(pragmaDataArchivo -> usuariosByCorreoEmpresarial.put(pragmaDataArchivo.getCorreoEmpresarial(), pragmaDataArchivo));

        informacionArchivo.add(new DatosArchivoComando("Pragmaticos",usuariosByCorreoEmpresarial.values().stream().collect(Collectors.toList())));

        if (camposUsuario.contains("familiares")) {
            Stream<InformacionFamiliarArchivo> familiares = manejoInformacionFamiliar(usuariosPragmaData.stream());
            informacionArchivo.add(new DatosArchivoComando("Familiares",familiares.collect(Collectors.toList())));
        }

        if (camposUsuario.contains("idiomas")) {
            Stream<InformacionIdiomasArchivo> idiomas = manejoUsuarioIdioma(usuariosPragmaData.stream());
            informacionArchivo.add(new DatosArchivoComando("Idiomas",idiomas.collect(Collectors.toList())));
        }

        if (camposUsuario.contains("conocimientos")) {
            Stream<InformacionConocimientoArchivo> conocimientosTecnicos = manejoUsuarioConocimiento(usuariosPragmaData.stream());
            informacionArchivo.add(new DatosArchivoComando("Conocimiento",conocimientosTecnicos.collect(Collectors.toList())));
        }

        String archivoBase64 = this.feignArchivosUtilitario.convertirV2(new ConvertirArchivoComando("datos-pragmaticos",
                "XLS",
                informacionArchivo)
        ).getBody();

        return new ObjetoRespuestaExportarPragmaData(archivoBase64, 200, "OK");

    }

    private Stream<InformacionFamiliarArchivo>  manejoInformacionFamiliar(Stream<PragmaData> datos) {
        Collection<String> idLista = usuarioCrudUseCase.findByCorreoEmpresarialIn(
                        datos.map(data -> data.getCorreoEmpresarial()).collect(Collectors.toList())
                )
                .map(usuario -> usuario.getId()).collect(Collectors.toList());

        Stream<InformacionFamiliarArchivo> informacionFamiliarLista = informacionFamiliarCrudUseCase.findByUsuarioIn(idLista);

        return informacionFamiliarLista;
    }

    private Stream<InformacionIdiomasArchivo> manejoUsuarioIdioma(Stream<PragmaData> datos) {
        Collection<String> idLista = usuarioCrudUseCase.findByCorreoEmpresarialIn(
                datos.map(data -> data.getCorreoEmpresarial()).collect(Collectors.toList())
            )
            .map(usuario -> usuario.getId()).collect(Collectors.toList());

        return usuarioIdiomasCrudUseCase.findByUsuarioIn(idLista);
    }

    private Stream<InformacionConocimientoArchivo> manejoUsuarioConocimiento(Stream<PragmaData> datos) {
        Collection<String> idLista = usuarioCrudUseCase.findByCorreoEmpresarialIn(
                        datos.map(data -> data.getCorreoEmpresarial()).collect(Collectors.toList())
                )
                .map(usuario -> usuario.getId()).collect(Collectors.toList());
        return usuarioConocimientoCrudUseCase.findByUsuarioIn(idLista);
    }
}
