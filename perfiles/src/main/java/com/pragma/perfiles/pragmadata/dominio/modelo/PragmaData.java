package com.pragma.perfiles.pragmadata.dominio.modelo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.perfil.dominio.modelo.EstadoProfesion;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstadoCivil;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGrupoSanguineo;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioTallaCamisa;
import lombok.*;

import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class PragmaData {

    private String imagenPerfil;
    private String identificacion;
    private String correoEmpresarial;
    private String nombres;
    private String apellidos;
    private String tipoDocumento;
    private String cargo;
    private String profesion;
    private String direccion;
    private String correoPersonal;
    private String telefonoCelular;
    private String telefonoFijo;
    private UsuarioEstadoCivil estadoCivil;
    private String perfilProfesional;
    private String nacionalidad;
    private String paisResidencia;
    private String departamentoResidencia;
    private String ciudadResidencia;
    private String paisNacimiento;
    private String departamentoNacimiento;
    private String ciudadNacimiento;
    private LocalDate fechaNacimiento;
    private UsuarioGrupoSanguineo grupoSanguineo;
    private String vicepresidencia;
    private UsuarioTallaCamisa tallaCamisa;
    private LocalDate fechaUltimaActualizacion;
    private String nombreContactoEmergencia;
    private String telefonoContactoEmergencia;
    private String parentescoContactoEmergencia;
    private String id;
    private EstadoProfesion estadoProfesion;
    private String semestre;
    private String tarjetaProfesional;
    private Integer estrato;
    private String diasNoCelebrables;
    private String familiares;
    private LocalDate fechaIngreso;
    private String idiomas;
    private UsuarioEstado estado;
    private String empresa;
    private String conocimientos;
    private Boolean proyectosEEUU;
    private Boolean visa;
    private String totalAnosExperienciaLaboral;
    private String conocimientosTecnicos;

    public PragmaData(String identificacion, String CorreoEmpresarial, String nombres, String apellidos, String cargo, String direccion,
                      String correoPersonal, String telefonoCelular, String telefonoFijo, UsuarioEstadoCivil estadoCivil,
                      String perfilProfesional, LocalDate fechaNacimiento, UsuarioGrupoSanguineo grupoSanguineo, UsuarioTallaCamisa tallaCamisa,
                      LocalDate fechaUltimaActualizacion, String NombreContactoEmergencia, String TelefonoContactoEmergencia, String ParentescoContactoEmergencia,
                      String id, String ciudadNacimiento, String vicepresidencia, Long profesion, Long tipoDocumento,
                      String ciudadResidencia, String nacionalidad, String diasNoCelebrables, InformacionFamiliar familiares, LocalDate fechaIngreso,
                      Idioma idiomas, UsuarioEstado estado,String empresa, String conocimientos, String conocimientosTecnicos, Boolean visa, Boolean proyectosEEUU, Integer totalAnosExperienciaLaboral) {
        this.identificacion = identificacion;
        this.correoEmpresarial = CorreoEmpresarial;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.tipoDocumento = tipoDocumento == null ? null : tipoDocumento.toString();
        this.cargo = cargo;
        this.profesion = profesion == null ? null : profesion.toString();
        this.direccion = direccion;
        this.correoPersonal = correoPersonal;
        this.telefonoCelular = telefonoCelular;
        this.telefonoFijo = telefonoFijo;
        this.estadoCivil = estadoCivil;
        this.perfilProfesional = perfilProfesional;
        this.nacionalidad = nacionalidad;
        this.ciudadResidencia = ciudadResidencia;
        this.ciudadNacimiento = ciudadNacimiento;
        this.fechaNacimiento = fechaNacimiento;
        this.grupoSanguineo = grupoSanguineo;
        this.vicepresidencia = vicepresidencia;
        this.tallaCamisa = tallaCamisa;
        this.fechaUltimaActualizacion = fechaUltimaActualizacion;
        this.nombreContactoEmergencia = NombreContactoEmergencia;
        this.telefonoContactoEmergencia = TelefonoContactoEmergencia;
        this.parentescoContactoEmergencia = ParentescoContactoEmergencia;
        this.id = id;
        this.estadoProfesion = estadoProfesion;
        this.semestre = semestre;
        this.tarjetaProfesional = tarjetaProfesional;
        this.estrato = estrato;
        this.diasNoCelebrables = diasNoCelebrables;
        this.familiares = null;
        this.fechaIngreso = fechaIngreso;
        this.idiomas = null;
        this.estado = estado;
        this.empresa = empresa;
        this.conocimientos = conocimientos;
        this.visa = visa;
        this.proyectosEEUU = proyectosEEUU;
        this.conocimientosTecnicos = conocimientosTecnicos;
        this.totalAnosExperienciaLaboral = Objects.equals(totalAnosExperienciaLaboral, null) ? "0" : totalAnosExperienciaLaboral.toString();
    }
}
