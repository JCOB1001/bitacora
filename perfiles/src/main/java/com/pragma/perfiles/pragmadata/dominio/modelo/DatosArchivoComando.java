package com.pragma.perfiles.pragmadata.dominio.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatosArchivoComando {
    private String nombreHoja;
    private List<Object> datos;
}
