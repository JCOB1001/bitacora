package com.pragma.perfiles.pragmadata.aplicacion.fabrica;

import com.pragma.perfiles.pragmadata.aplicacion.fabrica.impl.PragmaDataMapperPragmaDataArchivo;
import com.pragma.perfiles.pragmadata.dominio.modelo.PragmaData;
import com.pragma.perfiles.pragmadata.dominio.modelo.PragmaDataArchivo;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Field;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class FabricaPragmaData {

    private final PragmaDataMapperPragmaDataArchivo pragmaDataMapperPragmaDataArchivo;

    private PragmaDataArchivo converterActual (PragmaData pd){
        if ( pd == null ) {
            return null;
        }

        PragmaDataArchivo pragmaDataArchivo = new PragmaDataArchivo();

        pragmaDataArchivo.setImagenPerfil( pd.getImagenPerfil() );
        pragmaDataArchivo.setIdentificacion( pd.getIdentificacion() );
        pragmaDataArchivo.setCorreoEmpresarial( pd.getCorreoEmpresarial() );
        pragmaDataArchivo.setNombres( pd.getNombres() );
        pragmaDataArchivo.setApellidos( pd.getApellidos() );
        pragmaDataArchivo.setTipoDocumento( pd.getTipoDocumento() );
        pragmaDataArchivo.setCargo( pd.getCargo() );
        pragmaDataArchivo.setProfesion( pd.getProfesion() );
        pragmaDataArchivo.setDireccion( pd.getDireccion() );
        pragmaDataArchivo.setCorreoPersonal( pd.getCorreoPersonal() );
        pragmaDataArchivo.setTelefonoCelular( pd.getTelefonoCelular() );
        pragmaDataArchivo.setTelefonoFijo( pd.getTelefonoFijo() );
        if ( pd.getEstadoCivil() != null ) {
            pragmaDataArchivo.setEstadoCivil( pd.getEstadoCivil().name() );
        }
        pragmaDataArchivo.setPerfilProfesional( pd.getPerfilProfesional() );
        pragmaDataArchivo.setNacionalidad( pd.getNacionalidad() );
        pragmaDataArchivo.setPaisResidencia( pd.getPaisResidencia() );
        pragmaDataArchivo.setDepartamentoResidencia( pd.getDepartamentoResidencia() );
        pragmaDataArchivo.setCiudadResidencia( pd.getCiudadResidencia() );
        pragmaDataArchivo.setPaisNacimiento( pd.getPaisNacimiento() );
        pragmaDataArchivo.setDepartamentoNacimiento( pd.getDepartamentoNacimiento() );
        pragmaDataArchivo.setCiudadNacimiento( pd.getCiudadNacimiento() );
        if ( pd.getFechaNacimiento() != null ) {
            pragmaDataArchivo.setFechaNacimiento( DateTimeFormatter.ISO_LOCAL_DATE.format( pd.getFechaNacimiento() ) );
        }
        if ( pd.getGrupoSanguineo() != null ) {
            pragmaDataArchivo.setGrupoSanguineo( pd.getGrupoSanguineo().name() );
        }
        pragmaDataArchivo.setVicepresidencia( pd.getVicepresidencia() );
        if ( pd.getTallaCamisa() != null ) {
            pragmaDataArchivo.setTallaCamisa( pd.getTallaCamisa().name() );
        }
        if ( pd.getFechaUltimaActualizacion() != null ) {
            pragmaDataArchivo.setFechaUltimaActualizacion( DateTimeFormatter.ISO_LOCAL_DATE.format( pd.getFechaUltimaActualizacion() ) );
        }
        pragmaDataArchivo.setNombreContactoEmergencia( pd.getNombreContactoEmergencia() );
        pragmaDataArchivo.setTelefonoContactoEmergencia( pd.getTelefonoContactoEmergencia() );
        pragmaDataArchivo.setParentescoContactoEmergencia( pd.getParentescoContactoEmergencia() );
        pragmaDataArchivo.setId( pd.getId() );
        if ( pd.getEstadoProfesion() != null ) {
            pragmaDataArchivo.setEstadoProfesion( pd.getEstadoProfesion().name() );
        }
        pragmaDataArchivo.setSemestre( pd.getSemestre() );
        pragmaDataArchivo.setTarjetaProfesional( pd.getTarjetaProfesional() );
        pragmaDataArchivo.setEstrato( pd.getEstrato() );
        pragmaDataArchivo.setDiasNoCelebrables( pd.getDiasNoCelebrables() );
        if ( pd.getFechaIngreso() != null ) {
            pragmaDataArchivo.setFechaIngreso( DateTimeFormatter.ISO_LOCAL_DATE.format( pd.getFechaIngreso() ) );
        }
        pragmaDataArchivo.setIdiomas( pd.getIdiomas() );
        if ( pd.getEstado() != null ) {
            pragmaDataArchivo.setEstado( pd.getEstado().name() );
        }
        pragmaDataArchivo.setEmpresa( pd.getEmpresa() );
        pragmaDataArchivo.setConocimientos( pd.getConocimientos() );
        if(pd.getVisa()!=null){
            pragmaDataArchivo.setVisa( pd.getVisa()==true ? "SI": "NO" );
        }
        if(pd.getProyectosEEUU()!=null){
            pragmaDataArchivo.setProyectosEEUU( pd.getProyectosEEUU()==true ? "SI": "NO" );
        }
        pragmaDataArchivo.setTotalAnosExperienciaLaboral( pd.getTotalAnosExperienciaLaboral() );
        pragmaDataArchivo.setConocimientosTecnicos( pd.getConocimientosTecnicos() );

        return pragmaDataArchivo;
    }
    public Stream<PragmaDataArchivo> pragmaDataToPragmaDataArchivo(Stream<PragmaData> pragmaData){
        ArrayList<PragmaData> pd = new ArrayList<>(pragmaData.collect(Collectors.toList()));
        ArrayList<PragmaDataArchivo> pda = new ArrayList<>();
        for (PragmaData p : pd){
            pda.add(converterActual(p));
        }
        return pda.stream();
        //return pragmaDataMapperPragmaDataArchivo.leftToRight(pragmaData.collect(Collectors.toList()));
    }

    public List<PragmaDataArchivo> convertPragmaDatatoPragmaDataArchivo(List<String> camposUsuario, List<PragmaData> pragmaData){
        List<String> camposLista = camposUsuario.stream().filter( campo -> !((campo.equals("familiares")) || campo.equals("idiomas") || campo.equals("conocimientos"))).collect(Collectors.toList());

        List<PragmaDataArchivo> usuariosPragmaDataArchivo = pragmaDataToPragmaDataArchivo(pragmaData.stream()).map(tem -> {
            PragmaDataArchivo pragmadataArchivoTemp = new PragmaDataArchivo();
            for (String campos : camposLista) {
                try {
                    Field field = tem.getClass().getDeclaredField(campos);
                    field.setAccessible(true);
                    if(field.get(tem) != null){
                        if(field.getType().getSimpleName().equals("String")){
                            Object value =  field.get(tem);
                            String valueCadena = (String) value;
                            field.set(pragmadataArchivoTemp, valueCadena.replaceAll(",","").replaceAll("\n"," ").replaceAll("\r",""));
                        }else{
                            field.set(pragmadataArchivoTemp, field.get(tem));
                        }
                    }
                    else{
                        if(field.getType().getSimpleName().equals("Estrato")){
                            field.set(pragmadataArchivoTemp, 0);
                        }
                        field.set(pragmadataArchivoTemp, "");
                    }

                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            return pragmadataArchivoTemp;
        }).collect(Collectors.toList());

        return usuariosPragmaDataArchivo;
    }

}
