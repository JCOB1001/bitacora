package com.pragma.perfiles.pragmadata.dominio.modelo;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ConvertirArchivoComando {

    private String nombreArchivo;
    private String extencionArchivo;
    private List<DatosArchivoComando> contenido;

}

