package com.pragma.perfiles.pragmadata.dominio.modelo;

import lombok.Data;

@Data
public class InformacionIdiomasArchivo {
    private String correoEmpresarial;
    private String idioma;
    private int nivel;
    private String nombrePrueba;
    private String puntaje;
}
