package com.pragma.perfiles.pragmadata.aplicacion.manejador;

import com.pragma.perfiles.pragmadata.dominio.modelo.Campos;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerCampos {

    public ObjetoRespuesta<Stream<Campos>> ejecutar() {
        List<Campos> campos = Arrays.asList(Campos.values());
        return new ObjetoRespuesta<Stream<Campos>>(HttpStatus.OK, campos.stream());
    }
}
