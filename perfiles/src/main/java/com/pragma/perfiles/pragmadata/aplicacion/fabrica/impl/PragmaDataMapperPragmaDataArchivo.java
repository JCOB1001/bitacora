package com.pragma.perfiles.pragmadata.aplicacion.fabrica.impl;

import com.pragma.perfiles.pragmadata.dominio.modelo.PragmaData;
import com.pragma.perfiles.pragmadata.dominio.modelo.PragmaDataArchivo;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

import java.time.format.DateTimeFormatter;

@Mapper(componentModel = "spring")
public interface PragmaDataMapperPragmaDataArchivo extends ConvertidorBase<PragmaData, PragmaDataArchivo> {

}
