package com.pragma.perfiles.pragmadata.dominio.modelo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InformacionConocimientoArchivo {

    private String correoEmpresarial;
    private String skill_name;
    private int rate;
}
