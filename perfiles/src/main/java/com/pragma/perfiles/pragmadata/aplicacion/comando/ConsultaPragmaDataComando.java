package com.pragma.perfiles.pragmadata.aplicacion.comando;

import com.pragma.perfiles.pragmadata.dominio.modelo.TipoDato;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConsultaPragmaDataComando {

        String campoTabla;
        String tabla;
        boolean obligatorio;
        boolean filtrable;
        TipoDato tipoDato;
        List<String> filtros;

}
