package com.pragma.perfiles.pragmadata.dominio.modelo;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.Genero;
import lombok.Data;

import java.time.LocalDate;

@Data
public class InformacionFamiliarArchivo {

    private String nombres;
    private String apellidos;
    private Genero genero;
    private LocalDate fechaNacimiento;
    private String correoEmpresarial;
    private String parentesco;
    private String dependiente;

}
