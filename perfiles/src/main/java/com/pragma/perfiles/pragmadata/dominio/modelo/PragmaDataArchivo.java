package com.pragma.perfiles.pragmadata.dominio.modelo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PragmaDataArchivo {
    private String imagenPerfil;
    private String identificacion;
    private String correoEmpresarial;
    private String nombres;
    private String apellidos;
    private String tipoDocumento;
    private String cargo;
    private String profesion;
    private String direccion;
    private String correoPersonal;
    private String telefonoCelular;
    private String telefonoFijo;
    private String estadoCivil;
    private String perfilProfesional;
    private String nacionalidad;
    private String paisResidencia;
    private String departamentoResidencia;
    private String ciudadResidencia;
    private String paisNacimiento;
    private String departamentoNacimiento;
    private String ciudadNacimiento;
    private String fechaNacimiento;
    private String grupoSanguineo;
    private String vicepresidencia;
    private String tallaCamisa;
    private String fechaUltimaActualizacion;
    private String nombreContactoEmergencia;
    private String telefonoContactoEmergencia;
    private String parentescoContactoEmergencia;
    private String id;
    private String estadoProfesion;
    private String semestre;
    private String tarjetaProfesional;
    private Integer estrato;
    private String diasNoCelebrables;
    private String familiaresDependientes;
    private String fechaIngreso;
    private String idiomas;
    private String estado;
    private String empresa;
    private String conocimientos;
    private String proyectosEEUU;
    private String visa;
    private String totalAnosExperienciaLaboral;
    private String conocimientosTecnicos;
}
