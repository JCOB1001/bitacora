package com.pragma.perfiles.pragmadata.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class Profesion extends Modelo<Long> {

    private String nombre;

    public Profesion(Long id, String nombre){
        super(id);
        this.nombre = nombre;
    }
}
