package com.pragma.perfiles.pragmadata.dominio.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UbicacionGeograficaBasica {
    private Long id;
    private String nombre;
    private String idPadreUbicacionGeografica;
}
