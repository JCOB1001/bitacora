package com.pragma.perfiles.pragmadata.dominio.respuesta;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ObjetoRespuestaExportarPragmaData {

    private String file;
    private int status;
    private String message;
}
