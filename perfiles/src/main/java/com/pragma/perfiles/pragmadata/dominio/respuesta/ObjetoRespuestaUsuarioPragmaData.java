package com.pragma.perfiles.pragmadata.dominio.respuesta;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.Getter;

import java.util.stream.Stream;

@Getter
public class ObjetoRespuestaUsuarioPragmaData extends ObjetoRespuesta<Stream<UsuarioEntity>> {

    private Long cantidadPaginas;
    private boolean ultimaPagina;
    private  Integer totalElementos;

    public ObjetoRespuestaUsuarioPragmaData(Stream<UsuarioEntity> dato, Long cantidadPaginas, boolean ultimaPagina, Integer totalElementos) {
        super(dato);
        this.cantidadPaginas = cantidadPaginas;
        this.ultimaPagina = ultimaPagina;
        this.totalElementos = totalElementos;
    }

}
