package com.pragma.perfiles.pragmadata.infraestructura.endpoints;

import com.pragma.perfiles.pragmadata.aplicacion.manejador.ManejadorObtenerCampos;
import com.pragma.perfiles.pragmadata.dominio.modelo.Campos;
import com.pragma.perfiles.pragmadex.dominio.filtros.FiltroNombreCorreo;
import com.pragma.perfiles.pragmadex.dominio.modelo.PragmaDexPaginado;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@Validated
@RestController
@RequestMapping("pragma-data")
@RequiredArgsConstructor
public class EndpointObtenerCampos {

    private final ManejadorObtenerCampos manejadorObtenerCampos;

    @GetMapping("/campos")
    public ObjetoRespuesta<Stream<Campos>> obtenerCampos(){
        return manejadorObtenerCampos.ejecutar();
    }
}
