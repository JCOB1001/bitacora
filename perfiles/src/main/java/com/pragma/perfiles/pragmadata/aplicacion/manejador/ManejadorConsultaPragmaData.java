package com.pragma.perfiles.pragmadata.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.SolicitudIncorrecta;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiaNoCelebrablePragmaData;
import com.pragma.perfiles.dias_celebracion.dominio.repositorio.DiasNoCelebrablesUsuarioRepositorio;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorio;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.pragmadata.aplicacion.comando.ConsultaPragmaDataComando;
import com.pragma.perfiles.pragmadata.dominio.modelo.*;
import com.pragma.perfiles.pragmadata.dominio.respuesta.ObjetoRespuestaUsuarioPragmaDataPaginado;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import feign.FeignException.Unauthorized;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
@Slf4j
@RequiredArgsConstructor
public class ManejadorConsultaPragmaData {

    private final UsuarioRepositorio usuarioRepositorio;

    private final DiasNoCelebrablesUsuarioRepositorio diasNoCelebrablesUsuarioRepositorio;

    private final ConversionService conversionService;

    public ObjetoRespuesta<Stream<PragmaData>> ejecutar(List<ConsultaPragmaDataComando> consultaPragmaDataComando,
                                                        Integer numeroElementos, Integer numeroPagina) {

        ObjetoRespuestaUsuarioPragmaDataPaginado pragmaDataPaginado;

        try {
            List<String> camposUsuario = consultaPragmaDataComando.stream()
                    .map(ConsultaPragmaDataComando::getCampoTabla).collect(Collectors.toList());
            Map<String, Integer> mapCamposSolicitados = camposUsuario.stream().collect(Collectors.toMap(s -> s, k -> 0));
            List<ConsultaPragmaDataComando> filtros = consultaPragmaDataComando.stream()
                    .filter(s -> s.isFiltrable() &&
                            s.getFiltros() != null &&
                            !s.getFiltros().isEmpty() &&
                            s.getTabla().equals("UsuarioEntity")).collect(Collectors.toList());
            ObjetoRespuestaUsuario usuariosConsultados = usuarioRepositorio.buscarUsuariosPorFiltros(filtros, numeroElementos, numeroPagina);
            List<Usuario> usuarioPragmaDataRespuesta = ((Stream<Usuario>) usuariosConsultados.getDato()).collect(Collectors.toList());
            List<PragmaData> usuariosPragmaDataConverter = usuarioPragmaDataRespuesta
                    .stream().map(usuario -> {
                        PragmaData usuarioPragmaData = conversionService.convert(usuario, PragmaData.class);

                        return usuarioPragmaData;
                    })
                    .collect(Collectors.toList());

            if (mapCamposSolicitados.get("diasNoCelebrables") != null) {
                usuariosPragmaDataConverter = manejoDiasNoCelebrables(usuariosPragmaDataConverter);
            }

            List<PragmaData> usuariosPragmaData = usuariosPragmaDataConverter.stream().map(tem -> {
                PragmaData pragmadataTemp = new PragmaData();
                for (String campos : camposUsuario) {
                    try {
                        Field field = tem.getClass().getDeclaredField(campos);
                        field.setAccessible(true);
                        field.set(pragmadataTemp, field.get(tem));
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                return pragmadataTemp;
            }).collect(Collectors.toList());



            if (mapCamposSolicitados.get("vicepresidencia") != null) {
                usuariosPragmaData = manejoVicepresidencias(usuariosPragmaData);
            }

            if (mapCamposSolicitados.get("tipoDocumento") != null) {
                usuariosPragmaData = manejoTipoDocumento(usuariosPragmaData);
            }

            if (mapCamposSolicitados.get("profesion") != null) {
                usuariosPragmaData = manejoProfesion(usuariosPragmaData);
            }
            if (mapCamposSolicitados.get("nacionalidad") != null ){
                usuariosPragmaData = manejoNacionalidad(usuariosPragmaData);
            }
            if (mapCamposSolicitados.get("paisResidencia") != null
                    || mapCamposSolicitados.get("departametoResidencia") != null || mapCamposSolicitados.get("paisNacimiento") != null
                    || mapCamposSolicitados.get("ciudadResidencia") != null || mapCamposSolicitados.get("departamentoNacimiento") != null
                    || mapCamposSolicitados.get("ciudadNacimiento") != null) {
                usuariosPragmaData = manejoUbicacionGeografica(usuariosPragmaData);
            }

            pragmaDataPaginado =  new ObjetoRespuestaUsuarioPragmaDataPaginado(
                    usuariosConsultados.getCantidadPaginas(),
                    usuariosConsultados.isUltimaPagina(),
                    usuariosConsultados.getTotalElementos(),
                    HttpStatus.OK.value(),
                    usuariosPragmaData.stream());

        } catch (SolicitudIncorrecta e) {
            pragmaDataPaginado = new ObjetoRespuestaUsuarioPragmaDataPaginado();
            pragmaDataPaginado.setMensaje(e.getMessage());
            pragmaDataPaginado.setStatus(HttpStatus.BAD_REQUEST.value());
        } catch (Unauthorized e){
            pragmaDataPaginado = new ObjetoRespuestaUsuarioPragmaDataPaginado();
            pragmaDataPaginado.setMensaje("Problema al autenticar, revise el token");
            pragmaDataPaginado.setStatus(HttpStatus.UNAUTHORIZED.value());
        } catch (Exception e){
            log.error(e.getMessage());
            pragmaDataPaginado = new ObjetoRespuestaUsuarioPragmaDataPaginado();
            pragmaDataPaginado.setMensaje("Error desconocido, contactar al administrador");
            pragmaDataPaginado.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        return pragmaDataPaginado;
    }

    private List<PragmaData> manejoVicepresidencias(List<PragmaData> usuariosPragmaData) {
        List<Vicepresidencia> vicepresidencias = usuarioRepositorio.obtenerVicepresidencias();

        List<PragmaData> usuariosConVicepresindencia = usuariosPragmaData.stream().filter(temp -> {

            for(Vicepresidencia vicepresidencia : vicepresidencias) {
                if (vicepresidencia.getId().equals(temp.getVicepresidencia())) {
                    temp.setVicepresidencia(vicepresidencia.getNombre());
                }
            }

            return true;
        }).collect(Collectors.toList());

        return usuariosConVicepresindencia;
    }

    private List<PragmaData> manejoTipoDocumento(List<PragmaData> usuariosPragmaData) {
        List<TipoDocumento> tiposDocumentos = usuarioRepositorio.obtenerTiposDeDocumentos();

        List<PragmaData> usuariosConTipoDoc = usuariosPragmaData.stream().filter(temp -> {

            for(TipoDocumento tipoDoc : tiposDocumentos) {
                if (temp.getTipoDocumento() != null && tipoDoc.getId().equals(Long.valueOf(temp.getTipoDocumento()))) {
                    temp.setTipoDocumento(tipoDoc.getNombre());
                    break;
                }
            }

            return true;
        }).collect(Collectors.toList());

        return usuariosConTipoDoc;
    }

    private List<PragmaData> manejoProfesion(List<PragmaData> usuariosPragmaData) {

        List<String> profesionIds = usuariosPragmaData
                .stream()
                .map(PragmaData::getProfesion)
                .collect(Collectors.toList());

        List<Profesion> profesiones = usuarioRepositorio.obtenerProfesionPorListaId(profesionIds);

        List<PragmaData> usuariosConProfesion = usuariosPragmaData.stream().filter(temp -> {

            for(Profesion profesion : profesiones) {
                if (temp.getProfesion() != null && profesion.getId().equals(Long.valueOf(temp.getProfesion()))) {
                    temp.setProfesion(profesion.getNombre());
                    break;
                }
            }

            return true;
        }).collect(Collectors.toList());

        return usuariosConProfesion;
    }

    private List<PragmaData> manejoNacionalidad(List<PragmaData> usuariosPragmaData) {

        List<String> idNacionalidad = usuariosPragmaData.stream().map(PragmaData::getNacionalidad).distinct().collect(Collectors.toList());

        boolean isPadre = true;
        String nivel = "0";
        List<UbicacionGeograficaBasica> ubicaciones = usuarioRepositorio.obtenerUbicacionesGeograficasLista(idNacionalidad,isPadre,nivel);

        List<PragmaData> usuariosConUbicacion = usuariosPragmaData.stream()
                .peek(usuarioTemp -> {
                    Optional<String> nombreNacionalidad = ubicaciones.stream()
                    .filter(u -> {
                        if(usuarioTemp.getNacionalidad() != null){
                            return usuarioTemp.getNacionalidad().equalsIgnoreCase(u.getId() + "");
                        }
                        return false;
                    })
                    .findFirst()
                    .map(UbicacionGeograficaBasica::getNombre);
                    nombreNacionalidad.ifPresentOrElse(
                            usuarioTemp::setNacionalidad,
                            ()->{
                                usuarioTemp.setNacionalidad("");
                            }
                    );

        }).collect(Collectors.toList());

        return usuariosConUbicacion;
    }

    private List<PragmaData> manejoUbicacionGeografica(List<PragmaData> usuariosPragmaData) {

        List<List<String>> idUbicaciones = usuariosPragmaData
                .stream()
                .map(temp-> {
                    List<String> listaUbicaciones = new ArrayList<>();
                    listaUbicaciones.add(temp.getCiudadNacimiento());
                    listaUbicaciones.add(temp.getCiudadResidencia());
                    listaUbicaciones.add(temp.getDepartamentoNacimiento());
                    listaUbicaciones.add(temp.getPaisNacimiento());
                    listaUbicaciones.add(temp.getCiudadResidencia());
                    listaUbicaciones.add(temp.getPaisResidencia());
                    return listaUbicaciones;
                }).collect(Collectors.toList());

        List<String> ubicacionesConsulta = idUbicaciones.stream()
                .flatMap(List::stream).filter(s -> s != null)
                .distinct()
                .collect(Collectors.toList());

        List<UbicacionGeograficaCompleta> ubicaciones = usuarioRepositorio.obtenerUbicacionesGeograficas(ubicacionesConsulta);

        Map<String, UbicacionGeograficaCompleta> ubicacionesMapa = ubicaciones.stream()
                .collect(Collectors
                        .toMap(UbicacionGeograficaCompleta::getIdUbicacion,
                                ubicacionGeograficaCompleta -> ubicacionGeograficaCompleta));

        List<PragmaData> usuariosConUbicacion = usuariosPragmaData.stream().filter(temp -> {
            if(temp.getCiudadResidencia() != null && ubicacionesMapa.get(temp.getCiudadResidencia()) != null) {
                temp.setCiudadResidencia(ubicacionesMapa.get(temp.getCiudadResidencia()).getCiudad());
            }

            if(temp.getDepartamentoResidencia() != null && ubicacionesMapa.get(temp.getDepartamentoResidencia()) != null) {
                temp.setDepartamentoResidencia(ubicacionesMapa.get(temp.getDepartamentoResidencia()).getDepartamento());
            }

            if(temp.getPaisResidencia() != null && ubicacionesMapa.get(temp.getPaisResidencia()) != null) {
                temp.setPaisResidencia(ubicacionesMapa.get(temp.getPaisResidencia()).getPais());
            }

            if(temp.getCiudadNacimiento() != null && ubicacionesMapa.get(temp.getCiudadNacimiento()) != null ) {
                temp.setCiudadNacimiento(ubicacionesMapa.get(temp.getCiudadNacimiento()).getCiudad());
            }

            if(temp.getDepartamentoNacimiento() != null && ubicacionesMapa.get(temp.getDepartamentoNacimiento()) != null) {
                temp.setDepartamentoNacimiento(ubicacionesMapa.get(temp.getDepartamentoNacimiento()).getDepartamento());
            }

            if(temp.getPaisNacimiento() != null && ubicacionesMapa.get(temp.getPaisNacimiento()) != null) {
                temp.setPaisNacimiento(ubicacionesMapa.get(temp.getPaisNacimiento()).getPais());
            }
            return true;
        }).collect(Collectors.toList());

        return usuariosConUbicacion;
    }

    private List<PragmaData> manejoDiasNoCelebrables(List<PragmaData> usuariosPragmaData){
        List<String> ids = usuariosPragmaData.stream().map(PragmaData::getId).collect(Collectors.toList());
        Map<String, List<DiaNoCelebrablePragmaData>> diasNoCelebrables = diasNoCelebrablesUsuarioRepositorio.obtenerDiasNoCelebrablesListaUsuarios(ids);
        usuariosPragmaData.stream().forEach(pragmaData -> {
            if(diasNoCelebrables.containsKey(pragmaData.getId())){
                List<String> diasNoCelebrablesUsario = diasNoCelebrables.get(pragmaData.getId())
                        .stream().map(diaNoCelebrablePragmaData -> diaNoCelebrablePragmaData.getNombre())
                        .collect(Collectors.toList());
                String diasConcatenados = StringUtils.join(diasNoCelebrablesUsario, '/');
                pragmaData.setDiasNoCelebrables(diasConcatenados);
            }
        });
        return usuariosPragmaData;
    }
}
