package com.pragma.perfiles.pragmadata.dominio.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UbicacionGeograficaCompleta {

    private String idUbicacion;
    private String ciudad;
    private String departamento;
    private String pais;

}
