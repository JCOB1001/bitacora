package com.pragma.perfiles.pragmadata.infraestructura.clientefeign;

import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignClientConfiguration;
import com.pragma.perfiles.pragmadata.dominio.modelo.*;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

@FeignClient(name = "FeignAdministrarCatalogo", url = "${feign.modulo.administrarCatalogo}", configuration = {FeignClientConfiguration.class})
public interface FeignAdministrarCatalogo {

    @GetMapping("vicepresidencia/listar")
    public ObjetoRespuesta<List<Vicepresidencia>> ejecutarObtenerVicepresidencias();

    @GetMapping("tipo-documento")
    public ObjetoRespuesta<List<TipoDocumento>> ejecutarObtenerTipoDocumentos();

    @PostMapping("profesion/lista")
    public ObjetoRespuesta<List<Profesion>> ejecutarObtenerProfesionPorListaId(@NotNull @RequestBody List<String> idProfesion);

    @PostMapping("ubicaciones-geograficas/completa")
    public ObjetoRespuesta<List<UbicacionGeograficaCompleta>> ejecutarObtenerUbicacionesGeograficas(@NotNull @RequestBody List<String> ids);

    @PostMapping("ubicaciones-geograficas/lista")
    public ObjetoRespuesta<List<UbicacionGeograficaBasica>> ejecutarObtenerUbicacionesGeograficasLista(@NotNull @RequestBody List<String> ids, @NotNull @RequestParam("padre")  boolean padre, @RequestParam("nivel")  String nivel);
}
