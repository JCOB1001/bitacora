package com.pragma.perfiles.pragmadata.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class TipoDocumento extends Modelo<Long> {

    private String nombre;
    private String abreviatura;

    public TipoDocumento(Long id, String nombre, String abreviatura) {
        super(id);
        this.nombre = nombre;
        this.abreviatura = abreviatura;
    }
}
