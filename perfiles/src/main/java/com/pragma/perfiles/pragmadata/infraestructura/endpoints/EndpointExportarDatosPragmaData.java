package com.pragma.perfiles.pragmadata.infraestructura.endpoints;

import com.pragma.perfiles.pragmadata.aplicacion.manejador.ManejadorConsultaPragmaData;
import com.pragma.perfiles.pragmadata.aplicacion.manejador.ManejadorExportarDatosPragmaData;
import com.pragma.perfiles.pragmadata.aplicacion.comando.ConsultaPragmaDataComando;
import com.pragma.perfiles.pragmadata.dominio.respuesta.ObjetoRespuestaExportarPragmaData;
import com.pragma.perfiles.pragmadata.dominio.respuesta.ObjetoRespuestaUsuarioPragmaDataPaginado;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Validated
@RestController
@RequestMapping("pragma-data")
@RequiredArgsConstructor
public class EndpointExportarDatosPragmaData {

    private final ManejadorConsultaPragmaData manejadorConsultarPragmaData;
    private final ManejadorExportarDatosPragmaData manejadorExportarDatosPragmaData;

    @PostMapping("/exportar")
    public ResponseEntity<ObjetoRespuestaExportarPragmaData> exportarDatosPragmaData(@NotNull @RequestBody List<ConsultaPragmaDataComando> consultaPragmaDataComando,
                                                                                     @RequestParam Integer numeroElementos,
                                                                                     @RequestParam Integer numeroPagina) {

        ObjetoRespuestaUsuarioPragmaDataPaginado objetoRespuesta = (ObjetoRespuestaUsuarioPragmaDataPaginado) manejadorConsultarPragmaData.ejecutar(consultaPragmaDataComando, numeroElementos, numeroPagina);
        try {
            if (objetoRespuesta.getStatus() == 401) {
                return new ResponseEntity<ObjetoRespuestaExportarPragmaData>(new ObjetoRespuestaExportarPragmaData(null, 401, "Su token ha expirado"), HttpStatus.UNAUTHORIZED);
            }
            return new ResponseEntity<ObjetoRespuestaExportarPragmaData>(manejadorExportarDatosPragmaData.ejecutar(consultaPragmaDataComando,
                    objetoRespuesta.getDato().collect(Collectors.toList())),
                    HttpStatus.OK);
        } catch (IOException e) {
            ObjetoRespuestaExportarPragmaData objetoRespuestaExportarPragmaData = new ObjetoRespuestaExportarPragmaData(null, 500, "Ocurrió un error");
            e.printStackTrace();
            return new ResponseEntity<ObjetoRespuestaExportarPragmaData>(objetoRespuestaExportarPragmaData, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
