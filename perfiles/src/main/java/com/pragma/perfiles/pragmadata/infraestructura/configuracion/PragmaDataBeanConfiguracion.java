package com.pragma.perfiles.pragmadata.infraestructura.configuracion;

import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorObtenerConocimientosPorId;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import com.pragma.perfiles.perfil.aplicacion.fabrica.impl.UsuarioMapperInformacionConocimientoArchivo;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorCorreo;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.pragmadata.aplicacion.fabrica.FabricaInformacionFamiliarArchivo;
import com.pragma.perfiles.pragmadata.aplicacion.fabrica.FabricaPragmaData;
import com.pragma.perfiles.pragmadata.aplicacion.fabrica.impl.InformacionFamiliarMapperInformacionFamiliarArchivo;
import com.pragma.perfiles.pragmadata.aplicacion.manejador.ManejadorExportarDatosPragmaData;
import com.pragma.perfiles.pragmadata.infraestructura.clientefeign.FeignArchivosUtilitario;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PragmaDataBeanConfiguracion {

    @Bean
    public ManejadorExportarDatosPragmaData manejadorExportarDatosPragmaData(FabricaPragmaData fabricaPragmaData, FeignArchivosUtilitario feignArchivosUtilitario, InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase, UsuarioCrudUseCase usuarioCrudUseCase, UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase, UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase){
        return new ManejadorExportarDatosPragmaData(fabricaPragmaData,feignArchivosUtilitario, informacionFamiliarCrudUseCase,usuarioCrudUseCase,usuarioIdiomasCrudUseCase, usuarioConocimientoCrudUseCase);
    }

    @Bean
    public FabricaInformacionFamiliarArchivo informacionFamiliarArchivo(InformacionFamiliarMapperInformacionFamiliarArchivo informacionFamiliarMapperInformacionFamiliarArchivo){
        return new FabricaInformacionFamiliarArchivo(informacionFamiliarMapperInformacionFamiliarArchivo);
    }
}
