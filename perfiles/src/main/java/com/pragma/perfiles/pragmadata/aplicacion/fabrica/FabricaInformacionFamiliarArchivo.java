package com.pragma.perfiles.pragmadata.aplicacion.fabrica;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.entidad.InformacionFamiliarEntity;
import com.pragma.perfiles.pragmadata.aplicacion.fabrica.impl.InformacionFamiliarMapperInformacionFamiliarArchivo;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionFamiliarArchivo;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class FabricaInformacionFamiliarArchivo {

    private final InformacionFamiliarMapperInformacionFamiliarArchivo informacionFamiliarMapperInformacionFamiliarArchivo;

    public Stream<InformacionFamiliarArchivo> informacionFamiliarPorInformacionFamiliarArchivo(Iterable<InformacionFamiliarEntity> informacionFamiliar){
        return informacionFamiliarMapperInformacionFamiliarArchivo.leftToRight(informacionFamiliar);
    }
}
