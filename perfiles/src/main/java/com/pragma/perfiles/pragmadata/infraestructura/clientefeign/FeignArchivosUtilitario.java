package com.pragma.perfiles.pragmadata.infraestructura.clientefeign;

import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignClientConfiguration;
import com.pragma.perfiles.pragmadata.dominio.modelo.ConvertirArchivoComando;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;

@FeignClient(name = "FeignArchivosUtilitario", url = "${feign.modulo.archivosUtilitario}", configuration = {FeignClientConfiguration.class})
public interface FeignArchivosUtilitario {

    @PostMapping("/convertir")
    ResponseEntity<String> convertir(@RequestBody ConvertirArchivoComando request) throws IOException;

    @PostMapping("/v2/convertir")
    ResponseEntity<String> convertirV2(@RequestBody ConvertirArchivoComando request) throws IOException;
}
