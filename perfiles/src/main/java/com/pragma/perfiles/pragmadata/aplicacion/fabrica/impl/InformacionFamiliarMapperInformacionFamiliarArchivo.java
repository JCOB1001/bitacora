package com.pragma.perfiles.pragmadata.aplicacion.fabrica.impl;

import com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.entidad.InformacionFamiliarEntity;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionFamiliarArchivo;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MapperConfig;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring")
@MapperConfig(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface InformacionFamiliarMapperInformacionFamiliarArchivo extends ConvertidorBase<InformacionFamiliarEntity, InformacionFamiliarArchivo> {

    @AfterMapping
    default void setDependiente(@MappingTarget InformacionFamiliarArchivo informacionFamiliarArchivo,
                                InformacionFamiliarEntity informacionFamiliarEntity) {
        String dependiente = informacionFamiliarEntity.isDependiente() ? "Si" : "No";
        informacionFamiliarArchivo.setDependiente(dependiente);
    }

    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "usuario.correoEmpresarial", target = "correoEmpresarial"),
            @Mapping(source = "parentescoEntity.nombre", target = "parentesco")
    })
    InformacionFamiliarArchivo leftToRight(InformacionFamiliarEntity informacionFamiliarEntity);

    @Named("rightToLeft")
    @Override
    InformacionFamiliarEntity rightToLeft(InformacionFamiliarArchivo informacionFamiliarArchivo);


}
