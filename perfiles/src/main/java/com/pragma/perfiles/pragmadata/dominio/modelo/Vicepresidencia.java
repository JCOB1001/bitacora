package com.pragma.perfiles.pragmadata.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class Vicepresidencia extends Modelo<String> {

    private String nombre;
    private String codigoInterno;

    public Vicepresidencia(String id, String nombre, String codigoInterno) {
        super(id);
        this.nombre = nombre;
        this.codigoInterno = codigoInterno;
    }
}
