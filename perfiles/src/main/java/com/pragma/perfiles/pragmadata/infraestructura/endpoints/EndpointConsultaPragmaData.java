package com.pragma.perfiles.pragmadata.infraestructura.endpoints;

import com.pragma.perfiles.pragmadata.aplicacion.manejador.ManejadorConsultaPragmaData;
import com.pragma.perfiles.pragmadata.aplicacion.comando.ConsultaPragmaDataComando;
import com.pragma.perfiles.pragmadata.dominio.modelo.PragmaData;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Stream;

@Validated
@RestController
@RequestMapping("pragma-data")
@RequiredArgsConstructor
public class EndpointConsultaPragmaData {

    private final ManejadorConsultaPragmaData manejadorConsultarPrgamaData;

    @PostMapping
    public ObjetoRespuesta<Stream<PragmaData>> consultarPragmaData(@NotNull @RequestBody List<ConsultaPragmaDataComando> consultaPragmaDataComando,
                                                                   @RequestParam Integer numeroElementos,
                                                                   @RequestParam Integer numeroPagina){
        return manejadorConsultarPrgamaData.ejecutar(consultaPragmaDataComando, numeroElementos, numeroPagina);
    }
}
