package com.pragma.perfiles.pragmadata.aplicacion.converter;

import com.pragma.perfiles.perfil.dominio.modelo.TipoFotografia;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.pragmadata.dominio.modelo.PragmaData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class UsuarioToPragmaDataConverter implements Converter<Usuario, PragmaData> {

    @Override
    public PragmaData convert(Usuario usuario) {
        PragmaData usuarioPragmaData = new PragmaData();
        usuarioPragmaData.setTipoDocumento(usuario.getIdTipoDocumento() == null ? null : usuario.getIdTipoDocumento().toString());
        usuarioPragmaData.setVicepresidencia(usuario.getIdVicepresidencia());
        usuarioPragmaData.setNombres(usuario.getNombres());
        usuarioPragmaData.setApellidos(usuario.getApellidos());
        usuarioPragmaData.setId(usuario.getId());
        usuarioPragmaData.setCorreoEmpresarial(usuario.getCorreoEmpresarial());
        usuarioPragmaData.setTelefonoCelular(usuario.getTelefonoCelular());
        usuarioPragmaData.setNombreContactoEmergencia(usuario.getNombreContactoEmergencia());
        usuarioPragmaData.setTelefonoContactoEmergencia(usuario.getTelefonoContactoEmergencia());
        usuarioPragmaData.setParentescoContactoEmergencia(usuario.getParentescoContactoEmergencia());
        usuarioPragmaData.setCorreoPersonal(usuario.getCorreoPersonal());
        usuarioPragmaData.setDireccion(usuario.getDireccion());
        usuarioPragmaData.setEstadoCivil(usuario.getEstadoCivil());
        usuarioPragmaData.setFechaUltimaActualizacion(usuario.getFechaUltimaActualizacion());
        usuarioPragmaData.setTelefonoFijo(usuario.getTelefonoFijo());
        usuarioPragmaData.setTallaCamisa(usuario.getTallaCamisa());
        usuarioPragmaData.setGrupoSanguineo(usuario.getGrupoSanguineo());
        usuarioPragmaData.setCargo(usuario.getCargo());
        usuarioPragmaData.setProfesion(usuario.getIdProfesion() == null ? null : usuario.getIdProfesion().toString());
        usuarioPragmaData.setPerfilProfesional(usuario.getPerfilProfesional());
        usuarioPragmaData.setNacionalidad(usuario.getIdNacionalidad());
        usuarioPragmaData.setIdentificacion(usuario.getIdentificacion());
        usuarioPragmaData.setCiudadNacimiento(usuario.getIdLugarNacimiento());
        usuarioPragmaData.setCiudadResidencia(usuario.getIdLugarRecidencia());
        usuarioPragmaData.setFechaNacimiento(usuario.getFechaNacimiento());
        usuarioPragmaData.setPaisNacimiento(usuario.getIdLugarNacimiento());
        usuarioPragmaData.setDepartamentoNacimiento(usuario.getIdLugarNacimiento());
        usuarioPragmaData.setPaisResidencia(usuario.getIdLugarRecidencia());
        usuarioPragmaData.setDepartamentoResidencia(usuario.getIdLugarRecidencia());
        usuarioPragmaData.setEstadoProfesion(usuario.getEstadoProfesion());
        usuarioPragmaData.setSemestre(usuario.getSemestre());
        usuarioPragmaData.setTarjetaProfesional(usuario.getTarjetaProfesional());
        usuarioPragmaData.setEstrato(usuario.getEstrato());
        usuarioPragmaData.setEmpresa(usuario.getEmpresa());
        usuarioPragmaData.setFechaIngreso(usuario.getFechaIngreso());
        usuarioPragmaData.setEstado(usuario.getEstado());
        usuarioPragmaData.setProyectosEEUU(usuario.isProyectosEEUU());
        usuarioPragmaData.setVisa(usuario.isVisa());
        usuarioPragmaData.setTotalAnosExperienciaLaboral(String.valueOf(usuario.getTotalAnosExperienciaLaboral()));
        usuarioPragmaData.setConocimientosTecnicos(usuario.getConocimientosTecnicos());
        if( usuario.obtenerFotografiaPorTipo(TipoFotografia.REDONDA) != null){
            usuarioPragmaData.setImagenPerfil(usuario.obtenerFotografiaPorTipo(TipoFotografia.REDONDA).getContenido());
        }else if (usuario.obtenerFotografiaPorTipo(TipoFotografia.CUADRADO) != null){
            usuarioPragmaData.setImagenPerfil(usuario.obtenerFotografiaPorTipo(TipoFotografia.CUADRADO).getContenido());
        }

        return usuarioPragmaData;
    }
}
