package com.pragma.perfiles.pragmadata.dominio.modelo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Campos {
    IMAGEN_PERFIL("1", "imagenPerfil", "Imagen perfil", "FotografiaEntity", "Datos laborales", false,true ,TipoDato.IMAGEN, null),
    CORREO_PRAGMA("3", "correoEmpresarial", "Correo pragma", "UsuarioEntity", "Datos laborales", true,true, TipoDato.TEXTO, null),
    NUMERO_DOCUMENTO("2", "identificacion", "Numero de documento", "UsuarioEntity", "Información personal", true,true, TipoDato.TEXTO, null),
    NOMBRE("4", "nombres", "Nombres", "UsuarioEntity", "Información personal", true,true, TipoDato.TEXTO, null),
    APELLIDO("5", "apellidos", "Apellidos", "UsuarioEntity", "Información personal", true,true, TipoDato.TEXTO, null),
    TIPO_DOCUMENTO("6", "tipoDocumento", "Tipo de documento", "admin", "Información personal", false,false, TipoDato.NA, null),
    ROL("7", "cargo", "Rol", "UsuarioEntity", "Datos laborales", true,false, TipoDato.TEXTO, null),
    PROFESION("8", "profesion", "Profesión", "admin", "Datos laborales", false, false, TipoDato.NA, null),
    DIRECCION("9", "direccion", "Dirección", "UsuarioEntity", "Información personal", true,false, TipoDato.TEXTO, null),
    CORREO_PERSONAL("10", "correoPersonal", "Correo personal", "UsuarioEntity", "Información personal", true,false, TipoDato.TEXTO, null),
    TELEFONO_CELULAR("11", "telefonoCelular", "Teléfono celular", "UsuarioEntity", "Información personal", true,false, TipoDato.TEXTO, null),
    TELEFONO_FIJO("12", "telefonoFijo", "Teléfono fijo", "UsuarioEntity", "Información personal", true,false, TipoDato.TEXTO, null),
    ESTADO_CIVIL("13", "estadoCivil", "Estado civil", "UsuarioEntity", "Información personal", true,false, TipoDato.ESTADO_CIVIL, null),
    PERFIL_PROFESIONAL("14", "perfilProfesional", "Perfil profesional", "UsuarioEntity", "Datos laborales", true,false, TipoDato.TEXTO, null),
    NACIONALIDAD("15", "nacionalidad", "Nacionalidad", "admin", "Información personal", false,false, TipoDato.NA, null),
    PAIS_RESIDENCIA("16", "paisResidencia", "País de residencia", "admin", "Ubicación", false,false, TipoDato.NA, null),
    DEPARTAMENTO_RESIDENCIA("17", "departamentoResidencia", "Departamento de residencia", "admin", "Ubicación", false,false,TipoDato.NA, null),
    CIUDAD_RESIDENCIA("18", "ciudadResidencia", "Ciudad de residencia", "admin", "Ubicación", false,false,TipoDato.NA, null),
    PAIS_NACIMIENTO("19", "paisNacimiento", "País de nacimiento", "admin", "Ubicación", false,false,TipoDato.NA, null),
    DEPARTAMENTO_NACIMIENTO("20", "departamentoNacimiento", "Departamento de nacimiento", "admin", "Ubicación", false,false,TipoDato.NA, null),
    CIUDAD_NACIMIENTO("21", "ciudadNacimiento", "Ciudad de nacimiento", "admin", "Ubicación", false,false,TipoDato.NA, null),
    FECHA_NACIMIENTO("22", "fechaNacimiento", "Fecha de nacimiento", "UsuarioEntity", "Información personal", true, false, TipoDato.FECHA, null),
    SANGRE_RH("23", "grupoSanguineo","Grupo sanguíneo y Rh", "UsuarioEntity", "Información personal", true,false, TipoDato.GRUPO_SANGUINEO, null),
    VICEPRESIDENCIA("24", "vicepresidencia", "Vicepresidencia", "admin", "Datos laborales", false,false, TipoDato.NA, null),
    TALLA_CAMISA("25", "tallaCamisa", "Talla de camisa", "UsuarioEntity", "Información personal", true,false, TipoDato.TALLA_CAMISETA, null),
    ULTIMA_ACTUALIZACION("26", "fechaUltimaActualizacion", "Última actualización", "UsuarioEntity", "Datos laborales", true,false, TipoDato.FECHA, null),
    CONTACTO_EMERGENCIA("27", "nombreContactoEmergencia", "Contacto de emegencia", "UsuarioEntity", "Contacto emergencia", true,false, TipoDato.TEXTO, null),
    NUMERO_EMERGENCIA("28", "telefonoContactoEmergencia", "Número de emergencia", "UsuarioEntity", "Contacto emergencia", true,false, TipoDato.TEXTO, null),
    PARENTESCO_EMERGENCIA("29", "parentescoContactoEmergencia", "Parentesco emergencia", "UsuarioEntity", "Contacto emergencia", true,false, TipoDato.TEXTO, null),
    ID_USUARIO("30", "id", "Id de usuario", "UsuarioEntity", "Datos laborales", true, false,TipoDato.TEXTO, null),
    ESTADO_PROFESION("31", "estadoProfesion", "Estado Profesion", "UsuarioEntity", "Información personal", true, false, TipoDato.ESTADO_PROFESION, null),
    SEMESTRE("32", "semestre", "Semestre", "UsuarioEntity", "Información personal", true, false, TipoDato.TEXTO, null),
    TARJETA_PROFESIONAL("33", "tarjetaProfesional", "Tarjeta Profesional", "UsuarioEntity", "Información personal", true, false, TipoDato.TEXTO, null),
    ESTRATO("34", "estrato", "Estrato", "UsuarioEntity", "Información personal", true, false, TipoDato.TEXTO, null),
    DIAS_NO_CELEBRABLES("35", "diasNoCelebrables", "Días no celebrables", "dias_no_celebracion_usuario", "Información personal", false, false, TipoDato.TEXTO, null),
    FAMILIAR_DEPENDIENTE("36", "familiares", "Familiares", "informacion_familiar", "Información personal", false, false, TipoDato.ARRAY_OBJETO, "/informacion-familiar/correo/"),
    FECHA_INGRESO("37", "fechaIngreso", "Fecha de ingreso a Pragma", "UsuarioEntity", "Información personal", true, false, TipoDato.FECHA, null),
    IDIOMAS("38","idiomas","Idiomas","usuario_idiomas","Idiomas",false,false,TipoDato.ARRAY_OBJETO, "/idiomas/correo/"),
    ESTADO("39","estado","Estado","UsuarioEntity","Información personal", true, false, TipoDato.ESTADO, null),
    EMPRESA("40", "empresa", "Empresa", "UsuarioEntity", "Datos laborales", true,false, TipoDato.TEXTO, null),
    CONOCIMIENTO("41","conocimientos","Conocimientos","UsuarioConocimientoEntity","Información personal",false,false,TipoDato.ARRAY_OBJETO, "/conocimientos/correo/"),
    PROYECTOS_EEUU("42", "proyectosEEUU", "Proyectos de EEUU", "UsuarioEntity", "Informacion laboral", true,false, TipoDato.BOOLEANO, null),
    VISA("43", "visa", "Tiene visa", "UsuarioEntity", "Informacion laboral", true,false, TipoDato.BOOLEANO, null),
    ANOS_EXPERIENCIA("44", "totalAnosExperienciaLaboral", "Total años de experiencia laboral", "UsuarioEntity", "Informacion laboral", true,false, TipoDato.TEXTO, null),
    CONOCIMIENTOS_TECNICOS("45", "conocimientosTecnicos", "Conocimientos técnicos", "UsuarioEntity", "Informacion laboral", true,false, TipoDato.TEXTO, null),
    ;

    private String id;
    private String campoTabla;
    private String texto;
    private String tabla;
    private String categoria;
    private boolean filtrable;
    private boolean obligatorio;
    private TipoDato tipoDato;
    private String nuevaHoja;

}
