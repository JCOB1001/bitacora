package com.pragma.perfiles.estudio.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.estudio.aplicacion.comando.GuardarEstudioComando;
import com.pragma.perfiles.estudio.aplicacion.fabrica.FabricaEstudio;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioDetalleCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorGuardarEstudio {

    private final FabricaEstudio fabricaEstudio;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final EstudioCrudUseCase estudioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;
    private final EstudioDetalleCrudUseCase estudioDetalleCrudUseCase;

    public ObjetoRespuesta<Estudio> ejecutar(GuardarEstudioComando guardarEstudioComando){
        String idIdioma = HttpRequestContextHolder.getIdioma();
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(guardarEstudioComando.getIdUsuario());

        if(usuario==null){
          throw new UsuarioNoEncontrado("El usuario con el ID "+guardarEstudioComando.getIdUsuario()+" no se encuentra");
        }

        Estudio estudio=fabricaEstudio.guardarEstudio(guardarEstudioComando);
        Estudio nuevoEstudio = estudioCrudUseCase.guardar(estudio);
        EstudioDetalle estudioDetalle = new EstudioDetalle(null,nuevoEstudio.getNivel(),nuevoEstudio.getTituloObtenido(),nuevoEstudio.getId(),idIdioma,
                nuevoEstudio.getInstitucion());
        estudioDetalleCrudUseCase.guardar(estudioDetalle);

        double valorPorcentajeTotal = manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(guardarEstudioComando.getIdUsuario());
        usuario.setBarraProgreso(valorPorcentajeTotal);
        usuarioCrudUseCase.actualizar(usuario);

        return  new ObjetoRespuesta<Estudio>(nuevoEstudio);
    }
}
