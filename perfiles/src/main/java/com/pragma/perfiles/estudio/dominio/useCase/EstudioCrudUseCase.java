package com.pragma.perfiles.estudio.dominio.useCase;

import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.repositorio.EstudioRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class EstudioCrudUseCase extends CrudUseCaseCommon<EstudioRepositorio, Estudio, String>{
    private final EstudioRepositorio estudioRepositorio;

    public EstudioCrudUseCase(EstudioRepositorio estudioRepositorio){

        super(estudioRepositorio);
        this.estudioRepositorio=estudioRepositorio;
    }

    public Stream<Estudio> findByUsuarioId(String usuarioId){
        return this.estudioRepositorio.findByUsuarioId(usuarioId);
    }
}
