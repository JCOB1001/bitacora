package com.pragma.perfiles.estudio.infraestrctura.persistencia.repositorio;

import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.estudio.dominio.repositorio.EstudioDetalleRepositorio;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.builder.EstudioDetalleMapperEstudioDetalleEntity;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.dao.EstudioDetalleDao;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioDetalleEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
@Log4j2
public class EstudioDetalleRepositorioImpl extends CrudRepositorioBaseImpl<String, EstudioDetalle, EstudioDetalleEntity> implements EstudioDetalleRepositorio {
    private final EstudioDetalleDao estudioDetalleDao;

    private final EstudioDetalleMapperEstudioDetalleEntity estudioDetalleMapperEstudioDetalleEntity;

    @Override
    public EstudioDetalleDao obtenerRepositorio(){ return estudioDetalleDao; }

    @Override
    protected EstudioDetalleMapperEstudioDetalleEntity obtenerConversionBase(){
        return estudioDetalleMapperEstudioDetalleEntity;
    }

    public EstudioDetalle findByEstudioIdAndIdiomaId(String estudioId,String idiomaId){
        return obtenerConversionBase().rightToLeft(estudioDetalleDao.findByEstudioIdAndIdiomaId(estudioId,idiomaId));
    }
}
