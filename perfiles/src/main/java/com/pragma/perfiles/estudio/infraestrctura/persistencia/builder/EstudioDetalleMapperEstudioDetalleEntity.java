package com.pragma.perfiles.estudio.infraestrctura.persistencia.builder;

import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioDetalleEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface EstudioDetalleMapperEstudioDetalleEntity extends ConvertidorBase<EstudioDetalle, EstudioDetalleEntity> {

    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "idEstudio", target = "estudio.id"),
            @Mapping(source = "idIdioma", target = "idioma.id")
})
    EstudioDetalleEntity leftToRight(EstudioDetalle estudioDetalle);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "estudio.id", target = "idEstudio"),
            @Mapping(source = "idioma.id", target = "idIdioma")
    })
    EstudioDetalle rightToLeft(EstudioDetalleEntity estudioDetalleEntity);
}
