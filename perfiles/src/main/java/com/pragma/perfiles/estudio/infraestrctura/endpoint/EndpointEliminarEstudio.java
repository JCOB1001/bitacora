package com.pragma.perfiles.estudio.infraestrctura.endpoint;

import com.pragma.perfiles.estudio.aplicacion.manejador.ManejadorEliminarEstudio;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("info-academica-estudio")
@RequiredArgsConstructor
@Valid
public class EndpointEliminarEstudio {

    private final ManejadorEliminarEstudio manejadorEliminarEstudio;

    @DeleteMapping("/{idEstudio}/{idUsuario}")
    public ObjetoRespuesta<String> eliminarEstudio(@NotNull @PathVariable String idEstudio, @NotNull @PathVariable String idUsuario){

        return manejadorEliminarEstudio.ejecutar(idUsuario,idEstudio);
    }
}
