package com.pragma.perfiles.estudio.aplicacion.fabrica.impl;

import com.pragma.perfiles.estudio.aplicacion.comando.ActualizarEstudioComando;
import com.pragma.perfiles.estudio.aplicacion.comando.GuardarEstudioComando;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EstudioMapperActualizarEstudio extends ConvertidorBase<Estudio, ActualizarEstudioComando> {

}
