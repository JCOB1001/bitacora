package com.pragma.perfiles.estudio.dominio.modelo;
import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EstudioDetalle extends  Modelo<String>{

    private String idEstudio;
    private String idIdioma;
    private Tipo nivel;
    private String tituloObtenido;

    private String instituto;


    public EstudioDetalle(String id, Tipo nivel, String tituloObtenido, String idEstudio, String idIdioma, String instituto) {
        super(id);
        this.nivel = nivel;
        this.tituloObtenido = tituloObtenido;
        this.idEstudio=idEstudio;
        this.idIdioma=idIdioma;
        this.instituto = instituto;
    }
}
