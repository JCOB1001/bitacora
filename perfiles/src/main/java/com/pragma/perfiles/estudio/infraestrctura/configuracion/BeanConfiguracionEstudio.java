package com.pragma.perfiles.estudio.infraestrctura.configuracion;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.estudio.aplicacion.fabrica.FabricaEstudio;
import com.pragma.perfiles.estudio.aplicacion.fabrica.impl.EstudioMapperActualizarEstudio;
import com.pragma.perfiles.estudio.aplicacion.fabrica.impl.EstudioMapperGuardarEstudio;
import com.pragma.perfiles.estudio.aplicacion.manejador.ManejadorActualizarEstudio;
import com.pragma.perfiles.estudio.aplicacion.manejador.ManejadorEliminarEstudio;
import com.pragma.perfiles.estudio.aplicacion.manejador.ManejadorGuardarEstudio;
import com.pragma.perfiles.estudio.aplicacion.manejador.ManejadorObtenerEstudioPorIdUsuario;
import com.pragma.perfiles.estudio.dominio.repositorio.EstudioRepositorio;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioDetalleCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class BeanConfiguracionEstudio {

    @Bean
    public EstudioCrudUseCase estudioCrudUseCase(EstudioRepositorio estudioRepositorio){
       return  new EstudioCrudUseCase(estudioRepositorio);
    }

    @Bean
    public FabricaEstudio fabricaEstudio(EstudioMapperGuardarEstudio estudioMapperGuardarEstudio, EstudioMapperActualizarEstudio estudioMapperActualizarEstudio){
        return new FabricaEstudio(estudioMapperGuardarEstudio, estudioMapperActualizarEstudio);
    }

    @Bean
    public ManejadorGuardarEstudio manejadorGuardarEstudio(FabricaEstudio fabricaEstudio, EstudioCrudUseCase estudioCrudUseCase, UsuarioCrudUseCase usuarioCrudUseCase,ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso,EstudioDetalleCrudUseCase estudioDetalleCrudUseCase){
        return new ManejadorGuardarEstudio(fabricaEstudio,usuarioCrudUseCase,estudioCrudUseCase,manejadorCalcularValorBarraProgreso,estudioDetalleCrudUseCase);
    }

    @Bean
    public ManejadorActualizarEstudio manejadorActualizarEstudio(FabricaEstudio fabricaEstudio, UsuarioCrudUseCase usuarioCrudUseCase, EstudioCrudUseCase estudioCrudUseCase, EstudioDetalleCrudUseCase estudioDetalleCrudUseCase){
        return new ManejadorActualizarEstudio(fabricaEstudio,usuarioCrudUseCase,estudioCrudUseCase, estudioDetalleCrudUseCase);
    }

    @Bean
    public ManejadorObtenerEstudioPorIdUsuario manejadorObtenerEstudioPorIdUsuario(EstudioCrudUseCase estudioCrudUseCase,
                                                                                   UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerEstudioPorIdUsuario(estudioCrudUseCase,usuarioCrudUseCase);
    }

    @Bean
    public ManejadorEliminarEstudio manejadorEliminarEstudio(UsuarioCrudUseCase usuarioCrudUseCase, EstudioCrudUseCase estudioCrudUseCase, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso){
        return new ManejadorEliminarEstudio(usuarioCrudUseCase,estudioCrudUseCase,manejadorCalcularValorBarraProgreso);
    }


}
