package com.pragma.perfiles.estudio.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.comun.infraestructura.error.EstudioNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.estudio.aplicacion.comando.ActualizarEstudioComando;
import com.pragma.perfiles.estudio.aplicacion.comando.GuardarEstudioComando;
import com.pragma.perfiles.estudio.aplicacion.fabrica.FabricaEstudio;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioDetalleCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorActualizarEstudio {

    private final FabricaEstudio fabricaEstudio;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final EstudioCrudUseCase estudioCrudUseCase;
    private final EstudioDetalleCrudUseCase estudioDetalleCrudUseCase;

    public ObjetoRespuesta<String> ejecutar(String idInformacionAcademicaEstudio,ActualizarEstudioComando actualizarEstudioComando){
        String idIdioma = HttpRequestContextHolder.getIdioma();
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(actualizarEstudioComando.getIdUsuario());
        Estudio estudio = estudioCrudUseCase.obtenerPorId(idInformacionAcademicaEstudio);
        EstudioDetalle estudioDetalle = estudioDetalleCrudUseCase.findByEstudioIdAndIdiomaId(idInformacionAcademicaEstudio,idIdioma);
        if (estudio==null){
            throw new EstudioNoEncontrado("La informacion Academica con el id: "+idInformacionAcademicaEstudio+" no fue encontrada");
        }

        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID "+actualizarEstudioComando.getIdUsuario()+" no se encuentra registrado");
        }

        if(!estudio.getIdUsuario().equals(actualizarEstudioComando.getIdUsuario())){
            throw new EstudioNoEncontrado("El id de la informacion academica no coincide con la informacion del usuario");
        }
        Estudio estudioNuevo=fabricaEstudio.ActualizarEstudio(actualizarEstudioComando);
        estudioNuevo.setId(idInformacionAcademicaEstudio);

        estudioCrudUseCase.actualizar(estudioNuevo);
        if(estudioDetalle==null){
            estudioDetalle=new EstudioDetalle(null,estudioNuevo.getNivel(),estudioNuevo.getTituloObtenido(),estudio.getId(),idIdioma, estudioNuevo.getInstitucion());
            estudioDetalleCrudUseCase.guardar(estudioDetalle);
        }else{
            estudioDetalle.setNivel(estudioNuevo.getNivel());
            estudioDetalle.setTituloObtenido(estudioNuevo.getTituloObtenido());
            estudioDetalle.setInstituto(estudioNuevo.getInstitucion());
            estudioDetalleCrudUseCase.actualizar(estudioDetalle);
        }

        return  new ObjetoRespuesta<String>("success");
    }

}
