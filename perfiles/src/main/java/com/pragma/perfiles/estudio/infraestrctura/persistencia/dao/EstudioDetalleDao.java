package com.pragma.perfiles.estudio.infraestrctura.persistencia.dao;

import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioDetalleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstudioDetalleDao extends JpaRepository<EstudioDetalleEntity,String> {

    EstudioDetalleEntity findByEstudioIdAndIdiomaId(String usuarioId,String idiomaId);
}
