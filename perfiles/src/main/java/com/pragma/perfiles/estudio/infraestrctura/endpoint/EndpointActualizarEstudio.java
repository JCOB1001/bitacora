package com.pragma.perfiles.estudio.infraestrctura.endpoint;

import com.pragma.perfiles.estudio.aplicacion.comando.ActualizarEstudioComando;
import com.pragma.perfiles.estudio.aplicacion.manejador.ManejadorActualizarEstudio;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("info-academica-estudio")
@RequiredArgsConstructor
@Valid
public class EndpointActualizarEstudio {

    private final ManejadorActualizarEstudio manejadorActualizarEstudio;

    @PutMapping("/{idInformacionAcademicaEstudio}")
    public ObjetoRespuesta<String> guardarEstudio(@NotNull @PathVariable String idInformacionAcademicaEstudio, @NotNull @RequestBody ActualizarEstudioComando actualizarEstudioComando){

        return manejadorActualizarEstudio.ejecutar(idInformacionAcademicaEstudio, actualizarEstudioComando);
    }

}
