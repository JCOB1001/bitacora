package com.pragma.perfiles.estudio.infraestrctura.endpoint;

import com.pragma.perfiles.estudio.aplicacion.manejador.ManejadorObtenerEstudioPorIdUsuario;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.stream.Stream;

@RestController
@RequestMapping("info-academica-estudio")
@RequiredArgsConstructor
@Valid
public class EndpointObtenerEstudiosPorIdUsuarios {

    private final ManejadorObtenerEstudioPorIdUsuario manejadorObtenerEstudioPorIdUsuario;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<Stream<Estudio>> obtenerEstudioPorIdUsuario(@PathVariable String idUsuario){
      return   manejadorObtenerEstudioPorIdUsuario.ejecutar(idUsuario);
    }
}
