package com.pragma.perfiles.estudio.infraestrctura.persistencia.repositorio;

import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.repositorio.EstudioRepositorio;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.builder.EstudioMapperEstudioEntity;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.dao.EstudioDao;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class EstudioRepositorioImple extends CrudRepositorioBaseImpl<String, Estudio, EstudioEntity> implements EstudioRepositorio {

    private final EstudioDao estudioDao;
    private final EstudioMapperEstudioEntity estudioMapperEstudioEntity;
    @Override
    public EstudioDao obtenerRepositorio(){ return estudioDao; }

    @Override
    protected EstudioMapperEstudioEntity obtenerConversionBase(){
        return estudioMapperEstudioEntity;
    }

    @Override
    public Stream<Estudio> findByUsuarioId(String usuarioId) {
        return estudioMapperEstudioEntity.rightToLeft(
                estudioDao.findByUsuarioId(usuarioId)
        );
    }

}
