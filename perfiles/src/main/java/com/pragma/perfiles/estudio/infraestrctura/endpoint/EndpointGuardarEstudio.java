package com.pragma.perfiles.estudio.infraestrctura.endpoint;

import com.pragma.perfiles.estudio.aplicacion.comando.GuardarEstudioComando;
import com.pragma.perfiles.estudio.aplicacion.manejador.ManejadorGuardarEstudio;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("info-academica-estudio")
@RequiredArgsConstructor
@Valid
public class EndpointGuardarEstudio {

    private final ManejadorGuardarEstudio manejadorGuardarEstudio;

    @PostMapping
    public ObjetoRespuesta<Estudio> guardarEstudio(@NotNull @RequestBody GuardarEstudioComando guardarEstudioComando){

        return manejadorGuardarEstudio.ejecutar(guardarEstudioComando);
    }

}
