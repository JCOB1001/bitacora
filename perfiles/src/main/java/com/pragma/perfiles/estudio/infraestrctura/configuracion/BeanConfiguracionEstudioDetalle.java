package com.pragma.perfiles.estudio.infraestrctura.configuracion;

import com.pragma.perfiles.estudio.dominio.repositorio.EstudioDetalleRepositorio;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioDetalleCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class BeanConfiguracionEstudioDetalle {

    @Bean
    public EstudioDetalleCrudUseCase estudioDetalleCrudUseCase(EstudioDetalleRepositorio estudioDetalleRepositorio){
        return  new EstudioDetalleCrudUseCase(estudioDetalleRepositorio);
    }
}
