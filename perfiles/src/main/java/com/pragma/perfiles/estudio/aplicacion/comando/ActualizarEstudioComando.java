package com.pragma.perfiles.estudio.aplicacion.comando;

import com.pragma.perfiles.estudio.dominio.modelo.Tipo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarEstudioComando {

    private Tipo nivel;
    private String tituloObtenido;
    private String idUsuario;
    private String anioFinalizacion;
    private String institucion;
}
