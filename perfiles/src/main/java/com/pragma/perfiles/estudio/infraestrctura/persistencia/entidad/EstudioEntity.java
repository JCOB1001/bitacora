package com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.pragma.perfiles.estudio.dominio.modelo.Tipo;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "estudio")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EstudioEntity implements IdEntidad<String> {

    public interface Atributos extends IdEntidad.Atributos{

        String TIPO="tipo";
        String TITULO_OBTENIDO="tituloObtenido";
        String ANIO_FINALIZACION="anioFinalizacion";
        String INSTITUCION="institucion";
        String USUARIO="usuario";
        String ESTUDIO_DETALLE="estudioDetalle";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    @Enumerated(value = EnumType.STRING)
    private Tipo nivel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_estudio_usuario_usuario_id"))
    private UsuarioEntity usuario;
    private String tituloObtenido;
    private String anioFinalizacion;
    private String institucion;
    @JsonManagedReference
    @OneToMany(mappedBy = "estudio",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<EstudioDetalleEntity> estudioDetalle;

}
