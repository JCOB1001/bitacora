package com.pragma.perfiles.estudio.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Estudio extends Modelo<String> {

    private Tipo nivel;
    private String tituloObtenido;
    private String idUsuario;
    private String anioFinalizacion;
    private String institucion;

    public Estudio(){super(null);}

    public Estudio(String id, Tipo nivel, String tituloObtenido, String idUsuario, String anioFinalizacion, String institucion) {
        super(id);
        this.nivel = nivel;
        this.tituloObtenido = tituloObtenido;
        this.anioFinalizacion = anioFinalizacion;
        this.institucion = institucion;
        this.idUsuario=idUsuario;
    }
}
