package com.pragma.perfiles.estudio.aplicacion.fabrica.impl;

import com.pragma.perfiles.estudio.aplicacion.comando.GuardarEstudioComando;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EstudioMapperGuardarEstudio extends ConvertidorBase<Estudio, GuardarEstudioComando> {
}
