package com.pragma.perfiles.estudio.infraestrctura.persistencia.builder;

import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioDetalleEntity;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.*;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface EstudioMapperEstudioEntity extends ConvertidorBase<Estudio, EstudioEntity> {

    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "idUsuario", target = "usuario.id")
    )
    EstudioEntity leftToRight(Estudio estudio);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "usuario.id", target = "idUsuario")
    })
    Estudio rightToLeft(EstudioEntity estudioEntity);

    @AfterMapping
    default void afterRightToLeft(@MappingTarget Estudio estudio, EstudioEntity estudioEntity) {
        if(estudioEntity.getEstudioDetalle()!=null) {
            List<EstudioDetalleEntity> estudioDetalle = estudioEntity.getEstudioDetalle().stream().filter(item ->
                    item.getIdioma().getId().equals(HttpRequestContextHolder.getIdioma())).collect(Collectors.toList());
            if(estudioDetalle.isEmpty()) {
                estudioDetalle.add(new EstudioDetalleEntity());
            }
                estudio.setNivel(estudioDetalle.get(0).getNivel());
                estudio.setTituloObtenido(estudioDetalle.get(0).getTituloObtenido());
                estudio.setInstitucion(estudioDetalle.get(0).getInstituto());
        }
    }

}
