package com.pragma.perfiles.estudio.dominio.modelo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.pragma.perfiles.comun.infraestructura.error.EnumNoEncontrado;

public enum Tipo {


        TECNICA("Tecnica"),TECNOLOGIA("Tecnologia"),PROFESIONAL("Profesional"),ESPECIALIZACION("Especializacion"),MAESTRIA("Maestria"),DOCTORADO("Doctorado"),
    TECHNIQUE("Technique"),TECHNOLOGY("Technology"),PROFESSIONAL("Professional"),SPECIALIZATION("Specialization"),MASTERDEGREE("MasterDegree"),DOCTORATE("Doctorate");

    private String tipo;

    @JsonValue
    public String getValue() {
        return getTipo();
    }

    @JsonCreator
    public static Tipo fromValue(String value) {
        if (value != null && value.isEmpty()) {
            return null;
        }
        for (Tipo p : values()) {
            if (p.getValue().equals(value)) {
                return p;
            }
        }
        throw new EnumNoEncontrado("Tipo no encontrado" + value);
    }


    private Tipo(String tipo){
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }
}
