package com.pragma.perfiles.estudio.aplicacion.fabrica;

import com.pragma.perfiles.estudio.aplicacion.comando.ActualizarEstudioComando;
import com.pragma.perfiles.estudio.aplicacion.comando.GuardarEstudioComando;
import com.pragma.perfiles.estudio.aplicacion.fabrica.impl.EstudioMapperActualizarEstudio;
import com.pragma.perfiles.estudio.aplicacion.fabrica.impl.EstudioMapperGuardarEstudio;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaEstudio {

    private final EstudioMapperGuardarEstudio estudioMapperGuardarEstudio;
    private final EstudioMapperActualizarEstudio estudioMapperActualizarEstudio;

    public Estudio guardarEstudio(GuardarEstudioComando guardarEstudioComando){
        return  estudioMapperGuardarEstudio.rightToLeft(guardarEstudioComando);
    }

    public Estudio ActualizarEstudio(ActualizarEstudioComando actualizarEstudioComando){
        return estudioMapperActualizarEstudio.rightToLeft(actualizarEstudioComando);
    }

}
