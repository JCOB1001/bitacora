package com.pragma.perfiles.estudio.dominio.repositorio;

import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;

public interface EstudioRepositorio extends CrudRepositorioBase<String, Estudio> {

    Stream<Estudio> findByUsuarioId(String usuarioId);

}
