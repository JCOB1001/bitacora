package com.pragma.perfiles.estudio.dominio.repositorio;

import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;

public interface EstudioDetalleRepositorio extends CrudRepositorioBase<String, EstudioDetalle> {

    EstudioDetalle findByEstudioIdAndIdiomaId(String estudioId, String idiomaId);
}
