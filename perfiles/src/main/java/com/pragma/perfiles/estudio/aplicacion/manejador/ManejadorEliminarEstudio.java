package com.pragma.perfiles.estudio.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.EstudioNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarEstudio {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final EstudioCrudUseCase estudioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public ObjetoRespuesta<String> ejecutar(String idUsuario, String idEstudio){

        Usuario usuario=usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+idUsuario+" no se encuntra registrado");
        }

        Estudio estudio=estudioCrudUseCase.obtenerPorId(idEstudio);

        if (estudio==null){
            throw new EstudioNoEncontrado("No existe ningun Estudio asociado con el ID: "+idEstudio);
        }
        if(!idUsuario.equals(estudio.getIdUsuario())){
                throw new UsuarioNoEncontrado("El id del usuario enviado no coincide con los registros de la base de dato ");
        }

        estudioCrudUseCase.eliminarPorId(idEstudio);


        double valorPorcentajeTotal = manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(idUsuario);
        usuario.setBarraProgreso(valorPorcentajeTotal);
        usuarioCrudUseCase.actualizar(usuario);

        return new ObjetoRespuesta<String>("Success");
    }

}
