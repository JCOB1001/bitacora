package com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.pragma.perfiles.estudio.dominio.modelo.Tipo;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "estudio_detalle")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EstudioDetalleEntity implements IdEntidad<String> {
    public interface Atributos extends IdEntidad.Atributos{

        String TIPO="tipo";
        String TITULO_OBTENIDO="tituloObtenido";
        String IDIOMA="idioma";
        String ESTUDIO= "estudio";
        String INSTITUTO = "instituto";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    @Enumerated(value = EnumType.STRING)
    private Tipo nivel;
    private String instituto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idiomas_id", foreignKey = @ForeignKey(name = "fk_estudio_idiomas_idiomas_id"))
    private IdiomasEntity idioma;
    private String tituloObtenido;
    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "estudio_id", foreignKey = @ForeignKey(name = "fk_estudio_estudio_id"))
    private EstudioEntity estudio;
}
