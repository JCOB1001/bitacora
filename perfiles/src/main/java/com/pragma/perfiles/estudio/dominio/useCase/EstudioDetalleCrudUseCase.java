package com.pragma.perfiles.estudio.dominio.useCase;

import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.estudio.dominio.repositorio.EstudioDetalleRepositorio;
import com.pragma.perfiles.estudio.dominio.repositorio.EstudioRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class EstudioDetalleCrudUseCase extends CrudUseCaseCommon<EstudioDetalleRepositorio, EstudioDetalle, String> {
    private final EstudioDetalleRepositorio estudioDetalleRepositorio;

    public EstudioDetalleCrudUseCase(EstudioDetalleRepositorio estudioDetalleRepositorio){

        super(estudioDetalleRepositorio);
        this.estudioDetalleRepositorio=estudioDetalleRepositorio;
    }

    public EstudioDetalle findByEstudioIdAndIdiomaId(String estudioId, String idiomaId) {
        return this.estudioDetalleRepositorio.findByEstudioIdAndIdiomaId(estudioId,idiomaId);
    }
}
