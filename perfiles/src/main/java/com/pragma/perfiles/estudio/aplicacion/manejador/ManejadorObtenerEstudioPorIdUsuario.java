package com.pragma.perfiles.estudio.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerEstudioPorIdUsuario {

    private final EstudioCrudUseCase estudioCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Stream<Estudio>> ejecutar(String idUsuario){

        Usuario usuario=this.usuarioCrudUseCase.obtenerPorId(idUsuario);

        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+idUsuario+" no se encuentra");
        }
        Stream<Estudio> estudioStream=estudioCrudUseCase.findByUsuarioId(usuario.getId());

        return  new ObjetoRespuesta<Stream<Estudio>>(HttpStatus.OK,estudioStream);

    }
}
