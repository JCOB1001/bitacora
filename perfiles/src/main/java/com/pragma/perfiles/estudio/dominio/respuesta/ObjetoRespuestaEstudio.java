package com.pragma.perfiles.estudio.dominio.respuesta;


import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaEstudio extends ObjetoRespuesta {

    public ObjetoRespuestaEstudio(Stream<Estudio> dato) {
        super(dato);
    }
}
