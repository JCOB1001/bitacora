package com.pragma.perfiles.estudio.infraestrctura.persistencia.dao;

import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstudioDao extends JpaRepository<EstudioEntity,String> {

    Iterable<EstudioEntity> findByUsuarioId(String usuarioId);

}
