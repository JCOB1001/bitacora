package com.pragma.perfiles.inventario.infraestructura.endpoint;

import com.pragma.perfiles.inventario.aplicacion.manejador.ManejadorInventario;
import com.pragma.perfiles.inventario.dominio.respuesta.ElementosRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("inventario")
@RequiredArgsConstructor
@Validated
public class EndpointInventario {

    @Autowired
    private final ManejadorInventario manejadorInventario;

    @GetMapping("/usuario")
    public ElementosRespuesta ejecutarObtenerInventarioPorPersona(@RequestParam(name = "email") String email) {
        return  manejadorInventario.ejecutar(email);
    }
}
