package com.pragma.perfiles.inventario.dominio.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Elemento {

    private String placa;
    private String tipo;
    private String marca;
    private String modelo;
    private String serial;
    private String procesador;
    private String ram;
    private String empresa_propietario;
    private LocalDateTime fecha_compra;
    private String factura;
    private long valor_compra;
    private String estado;
    private String ubicacion;
    private String responsable_administracion;
    private long valor_activo_hoy;
    private String observaciones_activo;
    private String tipo_documento;
    private String documento;
    private String usuario;
    private String email;
    private String ciudad;
    private String departamento;
    private String empresa;
    private LocalDateTime fecha_entrega;
    private String estado_pragmatico;
    private String observaciones_pragmatico;
    private String placa_anterior;
    private String tipo_activo;
    private String comparativo_placa;
    private String comparativo_serial;
}
