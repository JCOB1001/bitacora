package com.pragma.perfiles.inventario.dominio.respuesta;

import com.pragma.perfiles.inventario.dominio.modelo.Elemento;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ElementosRespuesta {

    private List<Elemento> elementos;
}
