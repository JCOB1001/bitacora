package com.pragma.perfiles.inventario.aplicacion.manejador;

import com.pragma.perfiles.inventario.dominio.modelo.Elemento;
import com.pragma.perfiles.inventario.dominio.respuesta.ElementosRespuesta;
import com.pragma.perfiles.inventario.infraestructura.clientfeign.FeignInventario;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;

@Service
@RequiredArgsConstructor
public class ManejadorInventario {

    private final FeignInventario feignInventario;
    public ElementosRespuesta ejecutar(String email) {
        return consultarElementosActivosPorUsuario(email);
    }

    private ElementosRespuesta consultarElementosActivosPorUsuario(String email) {

        Object[] elementosFeing = feignInventario.obtenerInventario(email);
        ArrayList<Elemento> elementos = new ArrayList<>();

        for (int i = 0; i < elementosFeing.length; i++) {
            Elemento elemento = new Elemento();
            LinkedHashMap maps = (LinkedHashMap) elementosFeing[i];
            elemento.setPlaca(String.valueOf(maps.get("placa")));
            elemento.setTipo(String.valueOf(maps.get("tipo")));
            elemento.setMarca(String.valueOf(maps.get("marca")));
            elemento.setModelo(String.valueOf(maps.get("modelo")));
            elemento.setSerial(String.valueOf(maps.get("serial")));
            elemento.setProcesador(String.valueOf(maps.get("procesador")));
            elemento.setRam(String.valueOf(maps.get("ram")));
            elemento.setEmpresa_propietario(String.valueOf(maps.get("empresa_propietario")));
            elemento.setFecha_compra(String.valueOf(maps.get("fecha_compra"))==""?null:
                    getFecha(String.valueOf(maps.get("fecha_compra"))));
            elemento.setFactura(String.valueOf(maps.get("factura")));
            elemento.setValor_compra(getValor(String.valueOf(maps.get("valor_compra"))));
            elemento.setEstado(String.valueOf(maps.get("estado")));
            elemento.setUbicacion(String.valueOf(maps.get("ubicacion")));
            elemento.setResponsable_administracion(String.valueOf(maps.get("responsable_administracion")));
            elemento.setValor_activo_hoy(getValor(String.valueOf(maps.get("valor_activo_hoy"))));
            elemento.setObservaciones_activo(String.valueOf(maps.get("observaciones_activo")));
            elemento.setTipo_documento(String.valueOf(maps.get("tipo_documento")));
            elemento.setDocumento(String.valueOf(maps.get("documento")));
            elemento.setUsuario(String.valueOf(maps.get("usuario")));
            elemento.setEmail(String.valueOf(maps.get("email")));
            elemento.setCiudad(String.valueOf(maps.get("ciudad")));
            elemento.setDepartamento(String.valueOf(maps.get("departamento")));
            elemento.setEmpresa(String.valueOf(maps.get("empresa")));
            elemento.setFecha_entrega(String.valueOf(maps.get("fecha_entrega"))==""?null:
                    getFecha(String.valueOf(maps.get("fecha_entrega"))));
            elemento.setEstado_pragmatico(String.valueOf(maps.get("estado_pragmatico")));
            elemento.setObservaciones_pragmatico(String.valueOf(maps.get("observaciones_pragmatico")));
            elemento.setPlaca_anterior(String.valueOf(maps.get("placa_anterior")));
            elemento.setTipo_activo(String.valueOf(maps.get("tipo_activo")));
            elemento.setComparativo_placa(String.valueOf(maps.get("comparativo_placa")));
            elemento.setComparativo_serial(String.valueOf(maps.get("comparativo_serial")));
            elementos.add(elemento);
        }

        ElementosRespuesta respuesta = new ElementosRespuesta();
        respuesta.setElementos(elementos);
        return respuesta;
    }

    private long getValor(String valor){
        if(valor.equals("")){
            return Float.valueOf(0).longValue();
        } else {
            return Float.valueOf(valor).longValue();
        }
    }

    private LocalDateTime getFecha(String fecha){
        ZonedDateTime zdt = ZonedDateTime.parse(fecha);
        LocalDateTime ldt = zdt.toLocalDateTime();
        return ldt;
    }
}
