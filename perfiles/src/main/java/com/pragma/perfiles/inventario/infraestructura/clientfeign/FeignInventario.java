package com.pragma.perfiles.inventario.infraestructura.clientfeign;

import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
@FeignClient(name = "FeignInventario", url = "${feign.modulo.inventario}",
        configuration = FeignClientConfiguration.class)
public interface FeignInventario {

    @GetMapping("/exec")
    Object[] obtenerInventario(@RequestParam(name = "email") String email);
}
