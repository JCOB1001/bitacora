package com.pragma.perfiles.intereses.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.intereses.dominio.useCase.InteresesCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerInteresesPorIdUsuario {

    private  final InteresesCrudUseCase interesesCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Stream<String>> ejecutar(String idUsuario){
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(idUsuario);

        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario no se encuentra");
        }

        Stream<String> interesesStream=interesesCrudUseCase.findByUsuarioId(usuario.getId()).map(ele -> ele.getNombre());

        return new ObjetoRespuesta<Stream<String>>(interesesStream);
    }
}
