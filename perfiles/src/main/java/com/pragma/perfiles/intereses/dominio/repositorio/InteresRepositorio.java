package com.pragma.perfiles.intereses.dominio.repositorio;

import com.pragma.perfiles.intereses.dominio.modelo.Interes;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface InteresRepositorio extends CrudRepositorioBase<String, Interes> {
}
