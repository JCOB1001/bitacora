package com.pragma.perfiles.intereses.infraestructura.endpoint;

import com.pragma.perfiles.intereses.aplicacion.manejador.ManejadorActualizarIntereses;
import com.pragma.perfiles.intereses.aplicacion.manejador.ManjadorObtenerListaInteres;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("/intereses")
@RequiredArgsConstructor
public class EndpointObtenerIntereses {

    private final ManjadorObtenerListaInteres manjadorObtenerListaInteres;

    @GetMapping
    public ObjetoRespuesta<Stream<String>> obtenerIntereses(){
            return manjadorObtenerListaInteres.ejecutar();
    }
}
