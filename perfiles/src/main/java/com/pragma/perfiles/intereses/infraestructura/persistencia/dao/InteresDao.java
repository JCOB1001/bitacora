package com.pragma.perfiles.intereses.infraestructura.persistencia.dao;

import com.pragma.perfiles.intereses.infraestructura.persistencia.entidad.InteresEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InteresDao extends JpaRepository<InteresEntity,String> {
}
