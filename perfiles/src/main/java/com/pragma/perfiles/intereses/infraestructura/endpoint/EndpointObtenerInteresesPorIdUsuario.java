package com.pragma.perfiles.intereses.infraestructura.endpoint;

import com.pragma.perfiles.intereses.aplicacion.manejador.ManejadorObtenerInteresesPorIdUsuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("/intereses")
@RequiredArgsConstructor
public class EndpointObtenerInteresesPorIdUsuario {

    private final ManejadorObtenerInteresesPorIdUsuario manejadorObtenerInteresesPorIdUsuario;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<Stream<String>> obetener(@PathVariable String idUsuario){
        return manejadorObtenerInteresesPorIdUsuario.ejecutar(idUsuario);
    }

}
