package com.pragma.perfiles.intereses.infraestructura.persistencia.dao;

import com.pragma.perfiles.intereses.infraestructura.persistencia.entidad.InteresesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface  InteresesDao extends JpaRepository<InteresesEntity,String> {

    Iterable<InteresesEntity> findByUsuarioId(String usuarioId);

    InteresesEntity findByUsuarioIdAndNombre(String usuarioId, String nombre);

    @Transactional
    @Modifying
    @Query("delete from InteresesEntity i where i.usuario.id=:#{#id}")
    void deleteByUsuarioId(@Param("id")String usuarioId);
}
