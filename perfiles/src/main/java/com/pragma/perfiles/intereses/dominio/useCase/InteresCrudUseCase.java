package com.pragma.perfiles.intereses.dominio.useCase;

import com.pragma.perfiles.intereses.dominio.modelo.Interes;
import com.pragma.perfiles.intereses.dominio.repositorio.InteresRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class InteresCrudUseCase extends CrudUseCaseCommon<InteresRepositorio, Interes,String> {

    private final InteresRepositorio interesRepositorio;

    public InteresCrudUseCase(InteresRepositorio interesRepositorio){
        super(interesRepositorio);
        this.interesRepositorio=interesRepositorio;
    }

    public Stream<Interes> obtenerTodo(){
        return this.interesRepositorio.buscarTodo();
    }

}
