package com.pragma.perfiles.intereses.infraestructura.persistencia.builder;

import com.pragma.perfiles.intereses.dominio.modelo.Interes;
import com.pragma.perfiles.intereses.infraestructura.persistencia.entidad.InteresEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InteresMapperInteresEntity extends ConvertidorBase<Interes, InteresEntity> {
}
