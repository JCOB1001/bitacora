package com.pragma.perfiles.intereses.infraestructura.persistencia.entidad;


import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "intereses",
        uniqueConstraints={@UniqueConstraint(name = "uq_usuario_id_nombre", columnNames={"usuario_id","nombre"})})
@Data
@NoArgsConstructor
@AllArgsConstructor

public class InteresesEntity implements IdEntidad <String> {

    public interface ATRIBUTOS extends Atributos {
        String NOMBRE = "nombre";
        String USUARIO = "usuario";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_intereses_usuario_usuario_id"))
    private UsuarioEntity usuario;
    private String nombre;
}
