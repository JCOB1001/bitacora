package com.pragma.perfiles.intereses.infraestructura.persistencia.builder;


import com.pragma.perfiles.intereses.dominio.modelo.Intereses;
import com.pragma.perfiles.intereses.infraestructura.persistencia.entidad.InteresesEntity;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad.OtrosEstudiosEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.pragmadex.dominio.modelo.HistoricoCambiosPragmadex;
import com.pragma.perfiles.pragmadex.infraestructura.persistencia.entidad.HistoricoCambiosPragmadexEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.*;


@Mapper(componentModel = "spring")
public interface IntereseMapperIntereseEntity extends ConvertidorBase<Intereses, InteresesEntity> {
    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "userId", target = "usuario.id")
    )
    InteresesEntity leftToRight(Intereses intereses);

    @Named("rightToLeft")
    @Override
    @Mappings(
            @Mapping(source = "usuario.id", target = "userId")
    )
    Intereses rightToLeft(InteresesEntity interesesEntity);

}
