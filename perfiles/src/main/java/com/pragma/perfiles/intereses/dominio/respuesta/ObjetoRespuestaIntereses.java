package com.pragma.perfiles.intereses.dominio.respuesta;

import com.pragma.perfiles.intereses.dominio.modelo.Intereses;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaIntereses  extends ObjetoRespuesta {

    public ObjetoRespuestaIntereses (Stream<Intereses> datos){ super(datos); }
}
