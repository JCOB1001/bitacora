package com.pragma.perfiles.intereses.aplicacion.comando;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActualizarInteresesComando {

    private String userId;
    private String[] interests;
}
