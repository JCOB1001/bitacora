package com.pragma.perfiles.intereses.infraestructura.configuracion;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.intereses.aplicacion.manejador.ManejadorActualizarIntereses;
import com.pragma.perfiles.intereses.aplicacion.manejador.ManejadorEliminarIntereses;
import com.pragma.perfiles.intereses.aplicacion.manejador.ManejadorObtenerInteresesPorIdUsuario;
import com.pragma.perfiles.intereses.aplicacion.manejador.ManjadorObtenerListaInteres;
import com.pragma.perfiles.intereses.dominio.repositorio.InteresRepositorio;
import com.pragma.perfiles.intereses.dominio.repositorio.InteresesRepositorio;
import com.pragma.perfiles.intereses.dominio.useCase.InteresCrudUseCase;
import com.pragma.perfiles.intereses.dominio.useCase.InteresesCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationIntereses {

    @Bean
    public InteresesCrudUseCase interesesCrudUseCase(InteresesRepositorio interesesRepositorio){
        return new InteresesCrudUseCase(interesesRepositorio);
    }

    @Bean
    public ManejadorObtenerInteresesPorIdUsuario manejadorObtenerInteresesPorIdUsuario(UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                       InteresesCrudUseCase interesesCrudUseCase){
        return new ManejadorObtenerInteresesPorIdUsuario(interesesCrudUseCase,usuarioCrudUseCase);
    }

    @Bean
    public ManejadorActualizarIntereses manejadorActualizarIntereses(UsuarioCrudUseCase usuarioCrudUseCase,InteresesCrudUseCase interesesCrudUseCase,ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso){
        return new ManejadorActualizarIntereses(usuarioCrudUseCase,interesesCrudUseCase,manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public ManejadorEliminarIntereses manejadorEliminarIntereses(UsuarioCrudUseCase usuarioCrudUseCase, InteresesCrudUseCase interesesCrudUseCase, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso){
        return new ManejadorEliminarIntereses(usuarioCrudUseCase, interesesCrudUseCase, manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public InteresCrudUseCase interesCrudUseCase(InteresRepositorio interesRepositorio){
        return new InteresCrudUseCase(interesRepositorio);
    }

    @Bean
    public ManjadorObtenerListaInteres manjadorObtenerListaInteres(InteresCrudUseCase interesCrudUseCase){
        return new ManjadorObtenerListaInteres(interesCrudUseCase);
    }
}
