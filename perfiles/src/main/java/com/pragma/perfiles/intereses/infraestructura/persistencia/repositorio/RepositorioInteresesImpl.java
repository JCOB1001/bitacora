package com.pragma.perfiles.intereses.infraestructura.persistencia.repositorio;


import com.pragma.perfiles.intereses.dominio.modelo.Intereses;
import com.pragma.perfiles.intereses.dominio.repositorio.InteresesRepositorio;
import com.pragma.perfiles.intereses.infraestructura.persistencia.builder.IntereseMapperIntereseEntity;
import com.pragma.perfiles.intereses.infraestructura.persistencia.dao.InteresesDao;
import com.pragma.perfiles.intereses.infraestructura.persistencia.entidad.InteresesEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class RepositorioInteresesImpl  extends CrudRepositorioBaseImpl<String, Intereses, InteresesEntity> implements InteresesRepositorio {


    private final InteresesDao interesesDao;
    private final IntereseMapperIntereseEntity intereseMapperIntereseEntity;

    @Override
    protected InteresesDao obtenerRepositorio(){
        return interesesDao;
    }

    @Override
    protected IntereseMapperIntereseEntity obtenerConversionBase(){
        return intereseMapperIntereseEntity;
    }

    @Override
    public Stream<Intereses> findByUsuarioId(String usuarioId) {
        return intereseMapperIntereseEntity.rightToLeft(
                interesesDao.findByUsuarioId(usuarioId)
        );
    }

    @Override
    public Intereses findByUsuarioIdAndNombre(String usuarioId, String nombre) {
        return intereseMapperIntereseEntity.rightToLeft(
                interesesDao.findByUsuarioIdAndNombre(usuarioId, nombre)
        );
    }

    @Override
    public void deleteByUsuarioId(String usuarioId) {
        interesesDao.deleteByUsuarioId(usuarioId);
    }

}
