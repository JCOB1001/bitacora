package com.pragma.perfiles.intereses.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.InteresesNovalido;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.intereses.aplicacion.comando.ActualizarInteresesComando;
import com.pragma.perfiles.intereses.dominio.modelo.Intereses;
import com.pragma.perfiles.intereses.dominio.useCase.InteresesCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.text.Normalizer;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorActualizarIntereses {

private final UsuarioCrudUseCase usuarioCrudUseCase;
private final InteresesCrudUseCase interesesCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public ObjetoRespuesta<Stream<String>> ejecutar(ActualizarInteresesComando actualizarInteresesComando){
        Usuario usuario =usuarioCrudUseCase.obtenerPorId(actualizarInteresesComando.getUserId());

        if(actualizarInteresesComando.getInterests()==null){
            throw new InteresesNovalido("Los intereses no pueden ser nulos");
        }
        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario no se encuentra");
        }
        interesesCrudUseCase.deleteByUsuarioId(usuario.getId());

        String[] interesesInput = actualizarInteresesComando.getInterests();

        for (int i=0; i<interesesInput.length; i++){
            interesesInput[i]=Normalizer.normalize(interesesInput[i], Normalizer.Form.NFD)
                    .replaceAll("[^\\p{ASCII}]", "");
        }

        Set<String> interesesUnicos=new HashSet<String>(
                Arrays.stream(interesesInput).map(String::toUpperCase).collect(Collectors.toList())
        );
        interesesCrudUseCase.guardarTodo(interesesUnicos.stream().map(ele -> new Intereses("",actualizarInteresesComando.getUserId(),ele)).collect(Collectors.toList()));

        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.actualizar(usuario);

        return new ObjetoRespuesta<Stream<String>>(interesesUnicos.stream());
    }

}
