package com.pragma.perfiles.intereses.infraestructura.endpoint;

import com.pragma.perfiles.intereses.aplicacion.manejador.ManejadorEliminarIntereses;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("intereses")
@RequiredArgsConstructor
@Valid
public class EndpointEliminarIntereses {

    private final ManejadorEliminarIntereses manejadorEliminarIntereses;

    @DeleteMapping("/{idUsuario}/{interes}")
    public ObjetoRespuesta<String> eliminarIntereses(@NotNull @PathVariable String idUsuario,
                                               @NotNull @PathVariable String interes){

        return manejadorEliminarIntereses.ejecutar(idUsuario, interes);
    }

}
