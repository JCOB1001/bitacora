package com.pragma.perfiles.intereses.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Intereses extends Modelo<String> {
    private String userId;
    private String nombre;
    public Intereses() {
        super(null);
    }

    public Intereses(String id,String userId,String nombre) {
        super(id);
        this.userId=userId;
        this.nombre=nombre;
    }
}
