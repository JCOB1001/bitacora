package com.pragma.perfiles.intereses.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.intereses.dominio.modelo.Interes;
import com.pragma.perfiles.intereses.dominio.repositorio.InteresRepositorio;
import com.pragma.perfiles.intereses.infraestructura.persistencia.builder.InteresMapperInteresEntity;
import com.pragma.perfiles.intereses.infraestructura.persistencia.dao.InteresDao;
import com.pragma.perfiles.intereses.infraestructura.persistencia.entidad.InteresEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class RepositorioInteresImple extends CrudRepositorioBaseImpl<String, Interes, InteresEntity> implements InteresRepositorio {

    public final InteresDao interesDao;
    public final InteresMapperInteresEntity interesMapperInteresEntity;

    @Override
    protected InteresDao obtenerRepositorio() {
        return interesDao;
    }

    @Override
    protected InteresMapperInteresEntity obtenerConversionBase() {
        return interesMapperInteresEntity;
    }
}
