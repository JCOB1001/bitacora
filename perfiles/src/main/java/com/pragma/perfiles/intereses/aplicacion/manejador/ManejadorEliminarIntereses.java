package com.pragma.perfiles.intereses.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.InteresesNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.intereses.dominio.modelo.Intereses;
import com.pragma.perfiles.intereses.dominio.useCase.InteresesCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorEliminarIntereses {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final InteresesCrudUseCase interesesCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;


    public ObjetoRespuesta<String> ejecutar(String idUsuario, String interes) {

        interes = interes.toUpperCase();
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+idUsuario+" no se encuntra registrado");
        }

        Intereses intereses = interesesCrudUseCase.findByUsuarioIdAndNombre(usuario.getId(), interes);
        if(intereses == null){
            throw  new InteresesNoEncontrado("No existe ningun interes con el UserId " + idUsuario
                    + " y nombre " + interes);
        }

        interesesCrudUseCase.eliminarPorId(intereses.getId());
        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.actualizar(usuario);

        return new ObjetoRespuesta<>("");
    }
}
