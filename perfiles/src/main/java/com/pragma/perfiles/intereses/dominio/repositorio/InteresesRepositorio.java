package com.pragma.perfiles.intereses.dominio.repositorio;

import com.pragma.perfiles.intereses.dominio.modelo.Intereses;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;


public interface InteresesRepositorio extends CrudRepositorioBase<String, Intereses> {

    Stream<Intereses> findByUsuarioId(String usuarioId);
    Intereses findByUsuarioIdAndNombre(String usuarioId, String nombre);
    void deleteByUsuarioId(String usuarioId);

}
