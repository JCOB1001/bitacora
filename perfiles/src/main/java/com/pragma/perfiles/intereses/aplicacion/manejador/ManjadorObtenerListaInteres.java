package com.pragma.perfiles.intereses.aplicacion.manejador;

import com.pragma.perfiles.intereses.dominio.useCase.InteresCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManjadorObtenerListaInteres {
 private final InteresCrudUseCase interesCrudUseCase;

 public ObjetoRespuesta<Stream <String>> ejecutar(){
     Stream<String> intereses=interesCrudUseCase.obtenerTodo().map(interes -> interes.getNombre());
     ObjetoRespuesta respuesta=new ObjetoRespuesta<Stream<String>>(intereses);

     return respuesta;
 }
}
