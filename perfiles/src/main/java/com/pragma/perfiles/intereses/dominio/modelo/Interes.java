package com.pragma.perfiles.intereses.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Interes extends Modelo<String> {

    private String nombre;
    public Interes() {
        super(null);
    }

    public Interes(String id,String userId,String nombre) {
        super(id);
        this.nombre=nombre;
    }
}
