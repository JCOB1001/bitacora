package com.pragma.perfiles.intereses.dominio.useCase;

import com.pragma.perfiles.intereses.dominio.modelo.Intereses;
import com.pragma.perfiles.intereses.dominio.repositorio.InteresesRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class InteresesCrudUseCase extends CrudUseCaseCommon<InteresesRepositorio, Intereses,String> {

    private final InteresesRepositorio interesesRepositorio;

    public InteresesCrudUseCase (InteresesRepositorio interesesRepositorio){
        super(interesesRepositorio);
        this.interesesRepositorio=interesesRepositorio;
    }

    public Stream<Intereses> findByUsuarioId(String usuarioId){
        return this.interesesRepositorio.findByUsuarioId(usuarioId);
    }

    public Intereses findByUsuarioIdAndNombre(String usuarioId, String nombre){
        return this.interesesRepositorio.findByUsuarioIdAndNombre(usuarioId, nombre);
    }

    public void deleteByUsuarioId(String usuarioId){ interesesRepositorio.deleteByUsuarioId(usuarioId); }

}
