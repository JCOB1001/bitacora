package com.pragma.perfiles.intereses.infraestructura.endpoint;

import com.pragma.perfiles.intereses.aplicacion.comando.ActualizarInteresesComando;
import com.pragma.perfiles.intereses.aplicacion.manejador.ManejadorActualizarIntereses;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("/intereses")
@RequiredArgsConstructor
@Valid
public class EndpointActualizarIntereses {

    private final ManejadorActualizarIntereses manejadorActualizarIntereses;

    @PutMapping()
    public ObjetoRespuesta<Stream<String>> actualizar(@NotNull @RequestBody ActualizarInteresesComando actualizarInteresesComando){
        return manejadorActualizarIntereses.ejecutar(actualizarInteresesComando);
    }
}
