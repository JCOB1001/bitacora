package com.pragma.perfiles.intereses.infraestructura.persistencia.entidad;

import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "interes",
        uniqueConstraints={@UniqueConstraint(
                name = "uq_interes_nombre",
                columnNames={"nombre"})
})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InteresEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos {
        String NOMBRE = "nombre";

    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String nombre;
}
