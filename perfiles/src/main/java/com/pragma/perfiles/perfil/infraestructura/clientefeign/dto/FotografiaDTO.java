package com.pragma.perfiles.perfil.infraestructura.clientefeign.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class FotografiaDTO implements Serializable {

    private String file;
    private String userId; // Nombre del archivo a guardar en el lambda
    private String group; // Carpeta en donde se guarda el archivo en el lambda

    public FotografiaDTO(String file, String userId, String group) {
        this.file = file;
        this.userId = userId;
        this.group = group;
    }
}

