package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioGeneralComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
@RequiredArgsConstructor
public class ManejadorActualizarUsuarioGeneralPorId {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public void ejecutar(String idUsuario, ActualizarUsuarioGeneralComando actualizarUsuarioComando) {

        Usuario usuario = usuarioCrudUseCase.findById(idUsuario);

        if (usuario == null) {
            throw new UsuarioNoEncontrado("El usuario con el id : " + idUsuario+ " no existe en el sistema");
        }
        if (actualizarUsuarioComando.getNombres()!=null){
            usuario.setNombres(actualizarUsuarioComando.getNombres());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setIdentificacion(actualizarUsuarioComando.getIdentificacion());
        }
        if (actualizarUsuarioComando.getApellidos()!=null){
            usuario.setApellidos(actualizarUsuarioComando.getApellidos());

        }
        if (actualizarUsuarioComando.getCorreoPersonal()!=null){
            usuario.setCorreoPersonal(actualizarUsuarioComando.getCorreoPersonal());
        }
        if (actualizarUsuarioComando.getTelefonoFijo()!=null){
            usuario.setTelefonoFijo(actualizarUsuarioComando.getTelefonoFijo());
        }
        if (actualizarUsuarioComando.getTelefonoCelular()!=null){
            usuario.setTelefonoCelular(actualizarUsuarioComando.getTelefonoCelular());

        }if (actualizarUsuarioComando.getIdNacionalidad()!=null){
            usuario.setIdNacionalidad(actualizarUsuarioComando.getIdNacionalidad());
        }
        if (actualizarUsuarioComando.getIdLugarNacimiento()!=null){
            usuario.setIdLugarNacimiento(actualizarUsuarioComando.getIdLugarNacimiento());
        }
        if (actualizarUsuarioComando.getIdLugarRecidencia()!=null){
            usuario.setIdLugarRecidencia(actualizarUsuarioComando.getIdLugarRecidencia());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setFechaNacimiento(actualizarUsuarioComando.getFechaNacimiento());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setDireccion(actualizarUsuarioComando.getDireccion());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setNombreContactoEmergencia(actualizarUsuarioComando.getNombreContactoEmergencia());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setParentescoContactoEmergencia(actualizarUsuarioComando.getParentescoContactoEmergencia());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setTelefonoContactoEmergencia(actualizarUsuarioComando.getTelefonoContactoEmergencia());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setTallaCamisa(actualizarUsuarioComando.getTallaCamisa());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setEstadoCivil(actualizarUsuarioComando.getEstadoCivil());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setGenero(actualizarUsuarioComando.getGenero());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setGrupoSanguineo(actualizarUsuarioComando.getGrupoSanguineo());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.setIdTipoDocumento(actualizarUsuarioComando.getIdTipoDocumento());
        }
        if (actualizarUsuarioComando.getIdentificacion()!=null){
            usuario.actualizarFechaUltimaActualizacion(LocalDate.now());
        }
        if (actualizarUsuarioComando.getAceptacionTerminos()!=null){
            usuario.setAceptacionTerminos(actualizarUsuarioComando.getAceptacionTerminos().booleanValue());
        }

        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuario.setFechaUltimaActualizacion(LocalDate.now());


        usuarioCrudUseCase.actualizar(usuario);
    }

}
