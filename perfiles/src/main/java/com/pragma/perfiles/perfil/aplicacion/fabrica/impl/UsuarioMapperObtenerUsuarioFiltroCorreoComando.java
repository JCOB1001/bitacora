package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuarioFiltroCorreoComando;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuarioCorreo;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioMapperObtenerUsuarioFiltroCorreoComando extends ConvertidorBase<FiltroUsuarioCorreo, ObtenerUsuarioFiltroCorreoComando> {
}
