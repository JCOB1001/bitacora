package com.pragma.perfiles.perfil.dominio.modelo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;
import com.pragma.perfiles.comun.infraestructura.error.EnumNoEncontrado;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum UsuarioGrupoSanguineo {

    O_POSITIVO("O+"), O_NEGATIVO("O-"),
    A_POSITIVO("A+"), A_NEGATIVO("A-"),
    B_POSITIVO("B+"), B_NEGATIVO("B-"),
    AB_POSITIVO("AB+"), AB_NEGATIVO("AB-"),
    NO_ESPECIFICA("NO_ESPECIFICA");

    private String grupoSanguineo;
    @JsonValue
    public String getValue() {
        return getGrupoSanguineo();
    }

    @JsonCreator
    public static UsuarioGrupoSanguineo fromValue(String value) {
        if (value == null || value.isEmpty()) {
            return UsuarioGrupoSanguineo.NO_ESPECIFICA;
        }
        for (UsuarioGrupoSanguineo p : values()) {
            if (p.getValue().equals(value)) {
                return p;
            }
        }
        throw new EnumNoEncontrado("Grupo sanguineo no encontrado:" + value);
    }
    private UsuarioGrupoSanguineo(String grupoSanguineo){
        this.grupoSanguineo = grupoSanguineo;
    }

    public String getGrupoSanguineo() {
        return grupoSanguineo;
    }
}
