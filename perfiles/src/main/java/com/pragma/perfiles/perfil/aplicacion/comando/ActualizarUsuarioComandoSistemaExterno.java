package com.pragma.perfiles.perfil.aplicacion.comando;

import lombok.Data;

@Data
public class ActualizarUsuarioComandoSistemaExterno {
    String tipoIdentificacion;
    String identificacion;
    String nombres;
    String apellidos;
    String area;
    String cargoNomina;
    String chapter;
    String tecnologiaPrincipal;
    String clasificacion;
    String correo;
    String sexo;
    String referido;
    String tipoContrato;
    String fechaInicioContratoActual;
    String fechaNacimiento;
    String salarioBasicoOrdinario;
    String salarioBasicoIntegral;
    String noConstitutivosSalario;
    String moneda;
    String profesion;
    String tarjetaProfesional;
    String pasaporte;
    String fechaInicioRelacion;
    String estadoPeriodoPrueba;
    String evaluadorPeriodoPrueba;
    String semestre;
}
