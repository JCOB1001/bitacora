package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioDatosHojaDeVidaComando;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioDetalle;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioDetalleCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorActualizarUsuarioDatosHojaDeVida {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final UsuarioDetalleCrudUseCase usuarioDetalleCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;


    public Usuario ejecutar(ActualizarUsuarioDatosHojaDeVidaComando actualizarUsuarioDatosHojaDeVidaComando){
        String idIdioma = HttpRequestContextHolder.getIdioma();
        Usuario usuarioParaActualizar = usuarioCrudUseCase.obtenerPorId(actualizarUsuarioDatosHojaDeVidaComando.getIdUsuario());
        UsuarioDetalle usuarioDetalle = usuarioDetalleCrudUseCase.findByUsuarioIdAndIdiomaId(actualizarUsuarioDatosHojaDeVidaComando.getIdUsuario(),idIdioma);
        if (usuarioParaActualizar == null) {
            throw new UsuarioNoEncontrado("No se encontró usuario");
        }
        if (usuarioParaActualizar.getId() == null || usuarioParaActualizar.getId().isBlank()) {
            throw new UsuarioNoEncontrado("No se encontró usuario");
        }

        usuarioParaActualizar.setFechaIngreso(actualizarUsuarioDatosHojaDeVidaComando.getFechaIngreso());
        usuarioParaActualizar.setId(actualizarUsuarioDatosHojaDeVidaComando.getIdUsuario());
        usuarioParaActualizar.setIdProfesion(actualizarUsuarioDatosHojaDeVidaComando.getIdProfesion());
        usuarioParaActualizar.setTarjetaProfesional(actualizarUsuarioDatosHojaDeVidaComando.getTarjetaProfesional());
        usuarioParaActualizar.setPerfilProfesional(actualizarUsuarioDatosHojaDeVidaComando.getPerfilProfesional());
        usuarioParaActualizar.setConocimientosTecnicos(actualizarUsuarioDatosHojaDeVidaComando.getConocimientosTecnicos());
        usuarioParaActualizar.setCargo(actualizarUsuarioDatosHojaDeVidaComando.getCargo());
        usuarioParaActualizar.setIdVicepresidencia(actualizarUsuarioDatosHojaDeVidaComando.getIdVicepresidencia());
        usuarioParaActualizar.setCapacidad(actualizarUsuarioDatosHojaDeVidaComando.getCapacidad());

        usuarioParaActualizar.setProyectosEEUU(actualizarUsuarioDatosHojaDeVidaComando.getProyectosEEUU());
        usuarioParaActualizar.setVisa(actualizarUsuarioDatosHojaDeVidaComando.getVisa());
        usuarioParaActualizar.setTotalAnosExperienciaLaboral(actualizarUsuarioDatosHojaDeVidaComando.getTotalAnosExperienciaLaboral());
        usuarioParaActualizar.setProfesion(actualizarUsuarioDatosHojaDeVidaComando.getProfesion());

        usuarioParaActualizar.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuarioParaActualizar.getId()));
        usuarioParaActualizar.setFechaUltimaActualizacion(LocalDate.now());

        usuarioCrudUseCase.actualizar(usuarioParaActualizar);
        if(usuarioDetalle==null){
            usuarioDetalle = new UsuarioDetalle(null,usuarioParaActualizar.getConocimientosTecnicos(),
                    usuarioParaActualizar.getCargo(),usuarioParaActualizar.getPerfilProfesional(),idIdioma,usuarioParaActualizar.getId(), usuarioParaActualizar.getProfesion());
            usuarioDetalleCrudUseCase.guardar(usuarioDetalle);
        }else{
            usuarioDetalle.setCargo(usuarioParaActualizar.getCargo());
            usuarioDetalle.setConocimientosTecnicos(usuarioParaActualizar.getConocimientosTecnicos());
            usuarioDetalle.setPerfilProfesional(usuarioParaActualizar.getPerfilProfesional());
            usuarioDetalle.setProfesion(usuarioParaActualizar.getProfesion());
            usuarioDetalle.setProfesion(usuarioParaActualizar.getProfesion());
            usuarioDetalleCrudUseCase.actualizar(usuarioDetalle);
        }

        return usuarioParaActualizar;
    }

}
