package com.pragma.perfiles.perfil.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarUsuarioRetiradoComando {

    private String tipoIdentificacion;
    private String identificacion;

}
