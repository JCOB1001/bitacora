package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioDatosHojaDeVidaComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioDatosHojaDeVida;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarUsuarioDatosHojaDeVida {

    private final ManejadorActualizarUsuarioDatosHojaDeVida manejadorActualizarUsuarioDatosHojaDeVida;

    @PutMapping("/hoja-de-vida")
    public ObjetoRespuesta<Usuario> ejecutarActualizarUsuarioDatosHojaDeVida(@NotNull @RequestBody ActualizarUsuarioDatosHojaDeVidaComando actualizarUsuarioDatosHojaDeVidaComando){

        Usuario usuario = manejadorActualizarUsuarioDatosHojaDeVida.ejecutar(actualizarUsuarioDatosHojaDeVidaComando);
        return new ObjetoRespuesta<Usuario>(HttpStatus.OK, usuario);
    }
}
