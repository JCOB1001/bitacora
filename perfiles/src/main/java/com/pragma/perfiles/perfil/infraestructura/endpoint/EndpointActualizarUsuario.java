package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuario;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
//@PreAuthorize("hasAuthority('Admin')")
public class EndpointActualizarUsuario {

    private final ManejadorActualizarUsuario manejadorActualizarUsuario;

    @PutMapping("/{idUsuario}")
    public RespuestaBase ejecutarActualizarUsuario(@NotNull @PathVariable String idUsuario,
                                                     @NotNull @RequestBody ActualizarUsuarioComando actualizarUsuarioComando) {
        manejadorActualizarUsuario.ejecutar(idUsuario,actualizarUsuarioComando);
        return RespuestaBase.ok(" Success ");
    }

}
