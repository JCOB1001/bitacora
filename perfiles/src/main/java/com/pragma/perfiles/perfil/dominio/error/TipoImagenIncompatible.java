package com.pragma.perfiles.perfil.dominio.error;

public class TipoImagenIncompatible extends RuntimeException {
    public TipoImagenIncompatible(String mensaje) {
        super(mensaje);
    }
}

