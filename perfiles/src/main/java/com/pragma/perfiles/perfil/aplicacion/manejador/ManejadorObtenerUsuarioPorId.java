package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioPorId {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Usuario> ejecutar(String idUsuario) {
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
        return new ObjetoRespuesta<Usuario>(usuario);
    }

}
