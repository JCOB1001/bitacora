package com.pragma.perfiles.perfil.aplicacion.comando;

import com.pragma.perfiles.perfil.dominio.modelo.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class GuardarUsuarioNuevoComando {

    private String correoEmpresarial;
    private String googleId;
    private String identificacion;
    private String nombres;
    private String apellidos;
    private String cliente;
    private LocalDate fechaIngreso;
    private String correoPersonal;
    private String telefonoFijo;
    private String telefonoCelular;
    private String idNacionalidad;
    private String idLugarNacimiento;
    private String idLugarRecidencia;
    private LocalDate fechaNacimiento;
    private String direccion;
    private String nombreContactoEmergencia;
    private String parentescoContactoEmergencia;
    private String telefonoContactoEmergencia;
    private String tarjetaProfesional;
    private String perfilProfesional;
    private String conocimientosTecnicos;
    private UsuarioTallaCamisa tallaCamisa;
    private UsuarioEstadoCivil estadoCivil;
    private UsuarioGenero genero;
    private UsuarioGrupoSanguineo grupoSanguineo;
    private Long idTipoDocumento;
    private Long idProfesion;
    private LocalDate fechaCreacion;
    private LocalDate fechaUltimaActualizacion;
    private String cargo;
    private String idVicepresidencia;
    private List<Long> capacidad;
    private List<Fotografia> fotografias;
    private double diasGanados;
    private int diasTomados;
    private boolean aceptacionTerminos;

    public GuardarUsuarioNuevoComando(){
        tallaCamisa = UsuarioTallaCamisa.M;
        estadoCivil = UsuarioEstadoCivil.SOLTERO_A;
        genero = UsuarioGenero.MASCULINO;
        grupoSanguineo = UsuarioGrupoSanguineo.O_POSITIVO;
    }
}
