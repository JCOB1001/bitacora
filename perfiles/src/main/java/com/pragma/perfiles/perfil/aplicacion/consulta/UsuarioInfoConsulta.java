package com.pragma.perfiles.perfil.aplicacion.consulta;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioInfoConsulta {
    private String nombres;
    private String apellidos;
    private String tipoDocumento;
    private String identificacion;
    private String id;
    private String correo;
    private LocalDate fechaInicioContrato;
    private String tipoContrato;
    private Boolean vacaciones;
}
