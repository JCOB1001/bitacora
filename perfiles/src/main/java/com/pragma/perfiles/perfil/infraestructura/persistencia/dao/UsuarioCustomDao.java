package com.pragma.perfiles.perfil.infraestructura.persistencia.dao;

import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuarioCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuarioPragmaDex;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.pragmadata.aplicacion.comando.ConsultaPragmaDataComando;
import com.pragma.perfiles.pragmadata.dominio.respuesta.ObjetoRespuestaUsuarioPragmaData;
import com.pragma.perfiles.pragmadex.dominio.filtros.FiltroNombreCorreo;

import java.util.List;

public interface UsuarioCustomDao {

    ObjetoRespuestaUsuario buscarTodosFiltrando(FiltroUsuario filtro);
    ObjetoRespuestaUsuario buscarTodosFiltrandoCorreo(FiltroUsuarioCorreo filtro);
    ObjetoRespuestaUsuarioPragmaDex buscarUsuarioPorNombreCorreo(FiltroNombreCorreo filtroNombreCorreo);
    ObjetoRespuestaUsuarioPragmaDex buscarUsuariosPorHabilidadPaginado(FiltroNombreCorreo filtroNombreCorreo);
    ObjetoRespuestaUsuarioPragmaDex buscarUsuariosPorEmpresaYNombrePaginado(FiltroNombreCorreo filtroNombreCorreo);
    ObjetoRespuestaUsuarioPragmaDex buscarUsuariosPorEmpresaYHabilidadPaginado(FiltroNombreCorreo filtroNombreCorreo);

    List<String> buscarDatosPorListaIds(List<String> idUsuario, Class table, FiltroNombreCorreo filtroNombreCorreo);
    List<Usuario> buscarPorNombreCorreo(String nombre);
    ObjetoRespuestaUsuarioPragmaData buscarUsuariosPorFiltros(List<ConsultaPragmaDataComando> campos, Integer numeroElementos, Integer numeroPagina);
    UsuarioEntity buscarPorIdentificacion(Long idTipoDocumento, String identificacionUsuario);
    ObjetoRespuestaUsuario obtenerInformacionTodosUsuarios(FiltroUsuario filtro);
}
