package com.pragma.perfiles.perfil.dominio.useCase;

import com.pragma.vacaciones.common.useCase.CrudUseCaseCommonFiltro;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorioFiltro;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;

public class UsuarioCrudUseCaseFiltro extends CrudUseCaseCommonFiltro<UsuarioRepositorioFiltro, String, Usuario, FiltroUsuario, ObjetoRespuestaUsuario> {

    public UsuarioCrudUseCaseFiltro(UsuarioRepositorioFiltro usuarioRepositorioFiltro) {
        super(usuarioRepositorioFiltro);
    }

}
