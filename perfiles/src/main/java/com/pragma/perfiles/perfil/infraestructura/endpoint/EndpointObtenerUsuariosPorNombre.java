package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorNombre;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Stream;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerUsuariosPorNombre {

    private final ManejadorObtenerUsuarioPorNombre manejadorObtenerUsuarioPorNombre;

   @GetMapping("/nombre")
    public ObjetoRespuesta<List<Usuario>> obtenerUsuariosPorNombre(@RequestParam("nombre") String nombre){
        ObjetoRespuesta<List<Usuario>> ObjetoRespuesta = manejadorObtenerUsuarioPorNombre.ejecutar(nombre);
        return new ObjetoRespuesta<List<Usuario>>(HttpStatus.OK,ObjetoRespuesta.getDato());
   }
}
