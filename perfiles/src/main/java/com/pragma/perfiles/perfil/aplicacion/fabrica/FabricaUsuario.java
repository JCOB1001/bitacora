package com.pragma.perfiles.perfil.aplicacion.fabrica;

import com.pragma.perfiles.comun.infraestructura.seguridad.jwt.UsuarioContextoComando;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.UsuarioIdiomasEntity;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.perfil.aplicacion.comando.*;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioPorTokenConsulta;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioPorTokenIdentificacionConsulta;
import com.pragma.perfiles.perfil.aplicacion.fabrica.impl.*;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuarioCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionConocimientoArchivo;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionIdiomasArchivo;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class FabricaUsuario {

    private final UsuarioMapperGuardarUsuarioComando usuarioMapperGuardarUsuarioComando;
    private final UsuarioMapperInformacionConocimientoArchivo usuarioMapperInformacionConocimientoArchivo;
    private final UsuarioMapperGuardarUsuarioNuevoComando usuarioMapperGuardarUsuarioNuevoComando;
    private final UsuarioMapperActualizarUsuarioComando usuarioMapperActualizarUsuarioComando;
    private final UsuarioMapperObtenerUsuariosFiltroComando usuarioMapperObtenerUsuariosFiltroComando;
    private final UsuarioMapperObtenerUsuarioFiltroCorreoComando usuarioMapperObtenerUsuarioFiltroCorreoComando;
    private final UsuarioMapperObtenerUsuarioPorTokenCorreo usuarioMapperObtenerUsuarioPorTokenCorreo;
    private final  UsuarioMapperGuardarFotografiaComando usuarioMapperGuardarFotografiaComando;
    private final  UsuarioMapperActualizarFotografiaComando usuarioMapperActualizarFotografiaComando;
    private final UsuarioMapperObtenerUsuarioPorTokenIdentificacion usuarioMapperObtenerUsuarioPorTokenIdentificacion;
    private final UsuarioMapperUsuarioContextoComando usuarioMapperUsuarioContextoComando;
    private final UsuarioMapperActualizarUsuarioNuevoPragmatico usuarioMapperActualizarUsuarioNuevoPragmatico;
    private final InformacionNominaMapperActualizarUsuarioNuevoPragmatico informacionNominaMapperActualizarUsuarioNuevoPragmatico;
    private final InformacionLaboralMapperActualizarNuevoPragmatico informacionLaboralMapperActualizarNuevoPragmatico;

    public Usuario guardarUsuario(GuardarUsuarioComando guardarUsuarioComando) {
        return usuarioMapperGuardarUsuarioComando.rightToLeft(guardarUsuarioComando);
    }

    public Usuario guardarUsuarioNuevo(GuardarUsuarioNuevoComando guardarUsuarioNuevoComando) {
        return usuarioMapperGuardarUsuarioNuevoComando.rightToLeft(guardarUsuarioNuevoComando);
    }

    public Usuario actualizarUsuario(ActualizarUsuarioComando actualizarUsuarioComando) {
        return usuarioMapperActualizarUsuarioComando.rightToLeft(actualizarUsuarioComando);
    }

    public Usuario guardarFotografiaUsuario(GuardarFotografiaUsuarioComando guardarFotografiaUsuarioComando){
        return usuarioMapperGuardarFotografiaComando.rightToLeft(guardarFotografiaUsuarioComando);
    }

    public Usuario actualizarFotografiaUsuario(ActualizarFotografiaUsuarioComando actualizarFotografiaUsuarioComando){
        return usuarioMapperActualizarFotografiaComando.rightToLeft(actualizarFotografiaUsuarioComando);
    }

    public FiltroUsuario cargarFiltroUsuario(ObtenerUsuariosFiltroComando obtenerUsuariosFiltroComando) {
        return usuarioMapperObtenerUsuariosFiltroComando.rightToLeft(obtenerUsuariosFiltroComando);
    }

    public FiltroUsuarioCorreo cargarFiltroUsuarioCorreo(ObtenerUsuarioFiltroCorreoComando obtenerUsuarioFiltroCorreoComando) {
        return usuarioMapperObtenerUsuarioFiltroCorreoComando.rightToLeft(obtenerUsuarioFiltroCorreoComando);
    }

    public UsuarioPorTokenConsulta obtenerUsuarioPorTokenConsulta(Usuario usuario) {
        return usuarioMapperObtenerUsuarioPorTokenCorreo.leftToRight(usuario);
    }

    public UsuarioPorTokenIdentificacionConsulta obtenerUsuarioPorTokenIdentificacionConsulta(Usuario usuario){
        return usuarioMapperObtenerUsuarioPorTokenIdentificacion.leftToRight(usuario);
    }

    public Usuario usuarioPorUsuarioContexto(UsuarioContextoComando usuarioContextoComando){
        return usuarioMapperUsuarioContextoComando.usuarioContextoToUsuario(usuarioContextoComando);
    }

    public Usuario guardarUsuarioNuevoPragmaticoComando(ActualizarUsuarioNuevoPragmaticoComando actualizarUsuarioNuevoPragmaticoComando){
        return usuarioMapperActualizarUsuarioNuevoPragmatico.rightToLeft(actualizarUsuarioNuevoPragmaticoComando);
    }

    public InformacionNomina guardarInformacionNominaNuevoPragmatico(ActualizarUsuarioNuevoPragmaticoComando actualizarUsuarioNuevoPragmaticoComando){
        return informacionNominaMapperActualizarUsuarioNuevoPragmatico.rightToLeft(actualizarUsuarioNuevoPragmaticoComando);
    }

    public InformacionLaboral guardarInformacionLaboralNuevoPragmatico(ActualizarUsuarioNuevoPragmaticoComando actualizarUsuarioNuevoPragmaticoComando){
        return informacionLaboralMapperActualizarNuevoPragmatico.rightToLeft(actualizarUsuarioNuevoPragmaticoComando);
    }

    public Stream<InformacionConocimientoArchivo> usuarioPorInformacionConocimientoArchivo(Iterable<UsuarioEntity> usuarioConocimientosTecnicos) {
        return usuarioMapperInformacionConocimientoArchivo.leftToRight(usuarioConocimientosTecnicos);
    }

}
