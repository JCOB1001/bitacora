package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioNuevoPragmaticoComando;
import com.pragma.perfiles.perfil.infraestructura.configuracion.InformacionArchivoNuevoPragmaticoPropiedades;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RequiredArgsConstructor
public class ManejadorObtenerNuevoPragmatico {

    private final InformacionArchivoNuevoPragmaticoPropiedades informacionArchivoNuevoPragmaticoPropiedades;

    public ObjetoRespuesta<ActualizarUsuarioNuevoPragmaticoComando> ejecutar(String email){
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(informacionArchivoNuevoPragmaticoPropiedades.getUrl());
        builder.queryParam("email", email);
        String uri= builder.build().encode().toUriString();
        ResponseEntity<ActualizarUsuarioNuevoPragmaticoComando> response
                = restTemplate.getForEntity(uri, ActualizarUsuarioNuevoPragmaticoComando.class);
        return new ObjetoRespuesta<ActualizarUsuarioNuevoPragmaticoComando>(HttpStatus.OK, response.getBody());
    }

}
