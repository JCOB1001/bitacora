package com.pragma.perfiles.perfil.dominio.respuesta;

import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioInfoConsulta;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioInfo;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import lombok.Getter;

import java.util.List;
import java.util.stream.Stream;

@Getter
public class ObjetoRespuestaUsuario extends ObjetoRespuesta {

    private Long cantidadPaginas;
    private boolean ultimaPagina;
    private  Integer totalElementos;

    public ObjetoRespuestaUsuario(Stream<Usuario> dato, Long cantidadPaginas, boolean ultimaPagina) {
        super(dato);
        this.cantidadPaginas = cantidadPaginas;
        this.ultimaPagina = ultimaPagina;
    }

    public ObjetoRespuestaUsuario(List<Object[]> dato, Long cantidadPaginas, boolean ultimaPagina, Integer totalElementos) {
        super(dato);
        this.cantidadPaginas = cantidadPaginas;
        this.ultimaPagina = ultimaPagina;
        this.totalElementos=totalElementos;
    }

    public ObjetoRespuestaUsuario(Stream<Usuario> dato, Long cantidadPaginas, boolean ultimaPagina, Integer totalElementos) {
        super(dato);
        this.cantidadPaginas = cantidadPaginas;
        this.ultimaPagina = ultimaPagina;
        this.totalElementos = totalElementos;
    }

    public ObjetoRespuestaUsuario(List<UsuarioInfo> usuarios, long numeropaginas, boolean ultimaPagina, int size) {
        super(usuarios);
        this.cantidadPaginas = numeropaginas;
        this.ultimaPagina = ultimaPagina;
        this.totalElementos=size;
    }
}
