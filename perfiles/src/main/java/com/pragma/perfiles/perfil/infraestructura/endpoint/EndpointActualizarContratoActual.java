package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarContratoUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarContratoActual;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarContratoActual {

    private final ManejadorActualizarContratoActual manejadorActualizarContratoActual;

    @PutMapping("/contrato")
    public RespuestaBase actualizarContratoActual(
            @NotNull @RequestBody ActualizarContratoUsuarioComando actualizarContratoUsuarioComando) {
        manejadorActualizarContratoActual.ejecutar(actualizarContratoUsuarioComando);
        return RespuestaBase.ok(" Success ");
    }

}
