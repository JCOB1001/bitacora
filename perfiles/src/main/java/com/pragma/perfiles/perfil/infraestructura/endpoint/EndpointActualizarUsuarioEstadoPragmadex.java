package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioEstadoPragmadexComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarEstadoUsuarioPragmadex;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarUsuarioEstadoPragmadex {

    private final ManejadorActualizarEstadoUsuarioPragmadex manejadorActualizarEstadoUsuarioPragmadex;

    @PutMapping("updateUserInfo")
    public RespuestaBase ejecutarActualizarEstadoPragmadex(
            @NotNull @RequestBody ActualizarUsuarioEstadoPragmadexComando actualizarUsuarioEstadoPragmadexComando
    ){
        manejadorActualizarEstadoUsuarioPragmadex.ejecutar(actualizarUsuarioEstadoPragmadexComando);
        return RespuestaBase.ok("Success");
    }

}
