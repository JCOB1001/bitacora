package com.pragma.perfiles.perfil.aplicacion.comando;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
public class ActualizarUsuarioDatosHojaDeVidaComando {


    private String googleId;
    private String idUsuario;
    private Long idProfesion;
    private String perfilProfesional;
    private String tarjetaProfesional;
    private String conocimientosTecnicos;
    private String cargo;
    private String idVicepresidencia;
    private LocalDate fechaIngreso;
    private List<Long> capacidad;
    private String profesion;

    private Boolean proyectosEEUU;
    private Boolean visa;
    private int totalAnosExperienciaLaboral;

}
