package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuariosFiltroComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioInfoConsulta;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerTodosUsuariosInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerTodosUsuariosInfo {

    private final ManejadorObtenerTodosUsuariosInfo manejadorObtenerTodosUsuariosInfo;

    @GetMapping("/todoss")
    public List<UsuarioInfoConsulta> obtenerTodosUsuariosInfo(@NotNull ObtenerUsuariosFiltroComando obtenerUsuariosFiltroComando){
        List<UsuarioInfoConsulta> usuarios = manejadorObtenerTodosUsuariosInfo.ejecutar(obtenerUsuariosFiltroComando);
        return usuarios;
    }
}
