package com.pragma.perfiles.perfil.aplicacion.consulta;

import com.pragma.perfiles.perfil.dominio.modelo.Fotografia;
import lombok.Data;

import java.util.List;

@Data
public class UsuarioPorTokenConsulta {
    private String correoEmpresarial;
    private String nombres;
    private String apellidos;
    private int diasDisponibles;
    private List<Fotografia> fotografias;
}
