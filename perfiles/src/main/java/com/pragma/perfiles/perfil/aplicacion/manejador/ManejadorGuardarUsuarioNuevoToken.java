package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException;
import com.amazonaws.services.cognitoidp.model.AdminRemoveUserFromGroupRequest;
import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesTipoContrato;
import com.pragma.perfiles.comun.infraestructura.error.TipoCambioNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoVerificadoEnEscalaSalarial;
import com.pragma.perfiles.comun.infraestructura.seguridad.jwt.UsuarioContextoComando;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.HistoricoDeCambiosCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.TipoCambioCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.TipoCambio;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioNuevoPragmaticoComando;
import com.pragma.perfiles.perfil.aplicacion.fabrica.CopyProperties;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.*;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import com.pragma.perfiles.perfil.infraestructura.configuracion.InformacionArchivoNuevoPragmaticoPropiedades;
import com.pragma.perfiles.tipo_contrato.dominio.repositorio.TipoContratoRepositorio;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@RequiredArgsConstructor
public class ManejadorGuardarUsuarioNuevoToken {

    private final FeignTipoDocumento feignTipoDocumento;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaUsuario fabricaUsuario;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;
    private final InformacionLaboralCrudUseCase informacionLaboralCrudUseCase;
    private final InformacionArchivoNuevoPragmaticoPropiedades informacionArchivoNuevoPragmaticoPropiedades;
    private final CopyProperties copyProperties = new CopyProperties();
    private final TipoContratoRepositorio tipoContratoRepositorio;
    private final HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase;
    private final TipoCambioCrudUseCase tipoCambioCrudUseCase;
    private final AWSCognitoIdentityProvider cognitoClient;
    @Value(value = "${aws.cognito.userPoolId}")
    private String userPoolId;
    @Value(value = "proceso_retiro")
    private String userRetirementProgressGroup;
    @Value(value = "${administradorCatalogo.constants.terminoContrato}")
    private String terminoContrato;

    public ObjetoRespuesta<Usuario> ejecutar(UsuarioContextoComando usuarioContextoComando) {

        Usuario usuario = null;
        Usuario usuarioBase = null;

        usuarioBase = usuarioCrudUseCase.findByCorreoEmpresarial(usuarioContextoComando.getCorreo());
        String idTipoContrato = tipoContratoRepositorio.obtenerIdTipoContratoPorCoincidenciaNombre(terminoContrato);

        if(usuarioBase != null && usuarioBase.getEstado().equals(UsuarioEstado.ACTIVO) &&
                usuarioBase.getGoogleId().equals(usuarioContextoComando.getId()) &&
                (usuarioBase.getTipoContratoId() != idTipoContrato)){
            return new ObjetoRespuesta<>(usuarioBase);
        }

        try {

            ActualizarUsuarioNuevoPragmaticoComando datos =
                    this.consultarServicioArchivoNuevoPragmatico(usuarioContextoComando.getCorreo()).getBody();

            ObjetoRespuesta<Usuario> usuarioExistente = this.buscarUsuarioExistente(usuarioContextoComando, datos.getIdentificacion());

            if (usuarioExistente != null && usuarioExistente.getDato().getEstado().equals(UsuarioEstado.ACTIVO)) {
                return usuarioExistente;
            }

            usuarioBase = usuarioCrudUseCase.findByIdentificacion(datos.getIdentificacion());
            Long tipoDocumentoData = feignTipoDocumento.obtenerIdTipoDocumentoPorCoincidenciaNombre(datos.getTipoIdentificacion());

            if(usuarioBase != null && usuarioBase.getIdentificacion().equals(datos.getIdentificacion())
                    && usuarioBase.getIdTipoDocumento().equals(tipoDocumentoData)){
                usuario = usuarioBase;
                usuario.setCorreoEmpresarial(usuarioContextoComando.getCorreo());
                usuario.setGoogleId(usuarioContextoComando.getId());
                long noOfDaysBetween = ChronoUnit.DAYS.between(usuario.getFechaInicioContrato(), LocalDate.now());
                if(noOfDaysBetween > 7) {
                    usuario.setEstado(UsuarioEstado.ACTIVO);
                    usuario = this.guardarDatosSimples(usuario, datos);
                    this.guardarOtrosDatos(usuario, datos);
                    try {
                        AdminRemoveUserFromGroupRequest adminRemoveUserFromGroupRequest = new AdminRemoveUserFromGroupRequest()
                                .withGroupName(userRetirementProgressGroup)
                                .withUserPoolId(userPoolId)
                                .withUsername("Google_" + usuarioContextoComando.getId());
                        cognitoClient.adminRemoveUserFromGroup(adminRemoveUserFromGroupRequest);
                    } catch (AWSCognitoIdentityProviderException e) {
                        throw new UsuarioNoEncontrado("El usuario con el tipo de ID: " + usuarioContextoComando.getId() + " no existe");
                    }
                }
            } else {
                usuario = this.guardadoDatosPrimeraVez(usuarioContextoComando);
                usuario = this.guardarDatosSimples(usuario, datos);
                this.guardarOtrosDatos(usuario, datos);
            }
        }
        catch (Exception e){
            throw new UsuarioNoVerificadoEnEscalaSalarial(
                    String.format("Hola Pragmático, el correo %s no se encuentra registrado, por favor valídalo con el área de nómina para solucionarlo",
                            usuarioContextoComando.getCorreo()));
        }
        return new ObjetoRespuesta<>(usuario);
    }

    private ObjetoRespuesta<Usuario> buscarUsuarioExistente(UsuarioContextoComando usuarioContextoComando, String documento) {
        Usuario usuarioExistente = usuarioCrudUseCase.findbyGoogleId(usuarioContextoComando.getId());
        if (usuarioExistente != null && (usuarioExistente.getCorreoEmpresarial().equals(usuarioContextoComando.getCorreo()))) {
            return new ObjetoRespuesta<>(usuarioExistente);
        }
        usuarioExistente = usuarioCrudUseCase.findByCorreoEmpresarial(usuarioContextoComando.getCorreo());
        if (usuarioExistente != null && (usuarioExistente.getIdentificacion().equals(documento))) {
            usuarioExistente.setGoogleId(usuarioContextoComando.getId());
            usuarioCrudUseCase.guardar(usuarioExistente);
            return new ObjetoRespuesta<>(usuarioExistente);
        }
        usuarioExistente = usuarioCrudUseCase.findByIdentificacion(documento);
        if (usuarioExistente != null) {
            usuarioExistente.setCorreoEmpresarial(usuarioContextoComando.getId());
            usuarioExistente.setGoogleId(usuarioContextoComando.getId());
            usuarioCrudUseCase.guardar(usuarioExistente);
            return new ObjetoRespuesta<>(usuarioExistente);
        }
        return null;
    }

    private Usuario guardadoDatosPrimeraVez(UsuarioContextoComando usuarioContextoComando) {
        Usuario usuario = defaultValues(fabricaUsuario.usuarioPorUsuarioContexto(usuarioContextoComando));
        usuario.setFechaCreacion(LocalDate.now());
        usuario.actualizarFechaUltimaActualizacion(LocalDate.now());
        usuario.setBitacoraPrimeraVez(true);
        return usuario;
    }

    private Usuario guardarDatosSimples(Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando datos)
            throws InvocationTargetException, IllegalAccessException {
        copyProperties.copyProperties(usuario, fabricaUsuario.guardarUsuarioNuevoPragmaticoComando(datos));
        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuario = this.guardarDatosContrato(usuario, datos);
        return usuario;
    }

    private Usuario guardarDatosContrato(Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando datos) throws ExcepcionesTipoContrato{
        String idTipoContrato = tipoContratoRepositorio.obtenerIdTipoContratoPorCoincidenciaNombre(datos.getTipoContrato());
        if(idTipoContrato.isBlank() || idTipoContrato.isEmpty()){
            throw new ExcepcionesTipoContrato("No existe este tipo de contrato");
        }
        LocalDate fechaInicioContratoActual = LocalDate.parse(datos.getFechaInicioContratoActual());
        usuario.setTipoContratoId(idTipoContrato);
        usuario.setFechaInicioContrato(fechaInicioContratoActual);
        usuario = usuarioCrudUseCase.guardar(usuario);
        return usuario;
    }

    public void guardarOtrosDatos(Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando datos) {
        try {
            this.guardarDatosConsultasServicios(usuario, datos);
            this.guardadoInformacionLaboral(usuario, datos);
            this.guardarHistoricoDeCambiosContrato(usuario, datos);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarDatosConsultasServicios(Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando datos) {
        Long idLugarRecidencia = feignTipoDocumento.obtenerIdCiudadPorCoincidenciaNombre(datos.getCiudad(), datos.getDepartamento(), datos.getPais());
        usuario.setIdLugarRecidencia(idLugarRecidencia.toString());
        Long idTipoDocumento = feignTipoDocumento.obtenerIdTipoDocumentoPorCoincidenciaNombre(datos.getTipoIdentificacion());
        usuario.setIdTipoDocumento(idTipoDocumento);
        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuarioCrudUseCase.actualizar(usuario);
    }

    private void guardarHistoricoDeCambiosContrato(Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando datos) throws TipoCambioNoEncontrado {
        String cambioContratoId = "02";
        TipoCambio tipoCambio = tipoCambioCrudUseCase.obtenerPorId(cambioContratoId);
        if(tipoCambio == null){
            throw new TipoCambioNoEncontrado("Tipo de cambio no encontrado");
        }
        HistoricoDeCambios historicoDeCambios = new HistoricoDeCambios(usuario.getId(), tipoCambio.getId(), LocalDate.now(), datos.getTipoContrato());
        historicoDeCambiosCrudUseCase.guardar(historicoDeCambios);
    }

    private void guardadoInformacionLaboral(Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando datos) {
        InformacionLaboral informacionLaboral = fabricaUsuario.guardarInformacionLaboralNuevoPragmatico(datos);
        informacionLaboral.setUsuarioId(usuario.getId());
        informacionLaboralCrudUseCase.guardar(informacionLaboral);
    }

    private ResponseEntity<ActualizarUsuarioNuevoPragmaticoComando> consultarServicioArchivoNuevoPragmatico(String correo) {

        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(informacionArchivoNuevoPragmaticoPropiedades.getUrl());
        builder.queryParam("email", correo);
        String uri = builder.build().encode().toUriString();

        return restTemplate.getForEntity(uri, ActualizarUsuarioNuevoPragmaticoComando.class);
    }

    private Usuario defaultValues(Usuario usuario) {
        usuario.setGrupoSanguineo(UsuarioGrupoSanguineo.NO_ESPECIFICA);
        usuario.setTallaCamisa(UsuarioTallaCamisa.NO_ESPECIFICADO);
        usuario.setEstadoCivil(UsuarioEstadoCivil.NO_ESPECIFICADO);
        return usuario;
    }

}
