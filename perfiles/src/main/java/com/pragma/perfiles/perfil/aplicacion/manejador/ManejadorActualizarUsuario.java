package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;
import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorActualizarUsuario {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public void ejecutar(String idUsuario, ActualizarUsuarioComando actualizarUsuarioComando) {
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);

        if (usuario == null) {
            throw new UsuarioNoEncontrado("El usuario con el ID :" + idUsuario + "No existe");
        }
        if (usuario.getId() == null || usuario.getId().isBlank()) {
            throw new UsuarioNoEncontrado("No se encontró usuario");
        }

        usuario.setNombres(actualizarUsuarioComando.getNombres());
        usuario.setIdentificacion(actualizarUsuarioComando.getIdentificacion());
        usuario.setApellidos(actualizarUsuarioComando.getApellidos());
        usuario.setCorreoPersonal(actualizarUsuarioComando.getCorreoPersonal());
        usuario.setTelefonoFijo(actualizarUsuarioComando.getTelefonoFijo());
        usuario.setTelefonoCelular(actualizarUsuarioComando.getTelefonoCelular());
        usuario.setIdNacionalidad(actualizarUsuarioComando.getIdNacionalidad());
        usuario.setIdLugarNacimiento(actualizarUsuarioComando.getIdLugarNacimiento());
        usuario.setIdLugarRecidencia(actualizarUsuarioComando.getIdLugarRecidencia());
        usuario.setFechaNacimiento(actualizarUsuarioComando.getFechaNacimiento());
        usuario.setDireccion(actualizarUsuarioComando.getDireccion());
        usuario.setNombreContactoEmergencia(actualizarUsuarioComando.getNombreContactoEmergencia());
        usuario.setParentescoContactoEmergencia(actualizarUsuarioComando.getParentescoContactoEmergencia());
        usuario.setTelefonoContactoEmergencia(actualizarUsuarioComando.getTelefonoContactoEmergencia());
        usuario.setTallaCamisa(actualizarUsuarioComando.getTallaCamisa());
        usuario.setEstadoCivil(actualizarUsuarioComando.getEstadoCivil());
        usuario.setGenero(actualizarUsuarioComando.getGenero());
        usuario.setGrupoSanguineo(actualizarUsuarioComando.getGrupoSanguineo());
        usuario.setIdTipoDocumento(actualizarUsuarioComando.getIdTipoDocumento());
        usuario.actualizarFechaUltimaActualizacion(LocalDate.now());
        usuario.setDiasGanados(actualizarUsuarioComando.getDiasGanados());
        usuario.setDiasTomados(actualizarUsuarioComando.getDiasTomados());
        usuario.calcularDiasDisponibles();

        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuario.setFechaUltimaActualizacion(LocalDate.now());

        usuarioCrudUseCase.actualizar(usuario);
    }

}
