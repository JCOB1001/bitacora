package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorId;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
//@PreAuthorize("hasAuthority('Admin')")
public class EndpointObtenerUsuarioPorId {

    private final ManejadorObtenerUsuarioPorId manejadorObtenerUsuarioPorId;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<Usuario> ejecutarObtenerUsuarioPorId(@NotNull @PathVariable String idUsuario) {
        ObjetoRespuesta<Usuario> objetoRespuestaUsuario = manejadorObtenerUsuarioPorId.ejecutar(idUsuario);
        if (objetoRespuestaUsuario.getDato() == null) {
            throw new UsuarioNoEncontrado("No existe este usuario");
        }
        return new ObjetoRespuesta<Usuario>(HttpStatus.OK,
                objetoRespuestaUsuario.getDato());
    }

}
