package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuariosFiltroComando;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioMapperObtenerUsuariosFiltroComando extends ConvertidorBase<FiltroUsuario, ObtenerUsuariosFiltroComando> {
}
