package com.pragma.perfiles.perfil.infraestructura.clientefeign;

import com.pragma.perfiles.pragmadata.dominio.modelo.TipoDocumento;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Stream;

@FeignClient(name = "FeignTipoDocumento", url = "${feign.modulo.administrarCatalogo}", configuration = FeignClientConfiguration.class)
public interface FeignTipoDocumento {


    @GetMapping("tipo-documento/documento")
    Long obtenerIdTipoDocumentoPorCoincidenciaNombre(@RequestParam(name = "nombre") String nombre);

    @GetMapping("ciudad/ubicacion")
    Long obtenerIdCiudadPorCoincidenciaNombre(@RequestParam(name = "ciudad") String ciudad, @RequestParam(name = "departamento") String departamento,@RequestParam(name = "pais") String pais);

    @GetMapping("tipo-documento")
    ObjetoRespuesta<List<TipoDocumento>> obtenerTipoDocumentos();

}
