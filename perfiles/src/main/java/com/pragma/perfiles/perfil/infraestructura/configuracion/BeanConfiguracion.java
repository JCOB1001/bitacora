package com.pragma.perfiles.perfil.infraestructura.configuracion;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.contrato_historico.dominio.UseCase.ContratoHistoricoCrudUseCase;
import com.pragma.perfiles.dias_celebracion.dominio.repositorio.DiasNoCelebrablesUsuarioRepositorio;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.HistoricoDeCambiosCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.TipoCambioCrudUseCase;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.perfil.aplicacion.fabrica.CopyProperties;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.aplicacion.fabrica.impl.*;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarContratoActual;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarDiasVacaciones;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarEstadoUsuarioPragmadex;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarFotografiaUsuario;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuario;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioDatosHojaDeVida;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioGeneralPorId;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioPorCorreo;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioRetirado;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioSistemaExterno;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorAgregarDiasTomadosUsuario;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorEliminarUsuarioPorId;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorGuardarFotografiaUsuario;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorGuardarUsuario;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorGuardarUsuarioNuevo;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorGuardarUsuarioNuevoToken;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorMigrarDatosSalariosPorPersona;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerNuevoPragmatico;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioContratoPorUsuarioId;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioCorreoSimple;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioFiltroCorreo;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioGoogleId;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioLista;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorCorreo;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorId;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorIdentificacion;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorNombre;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorToken;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorTokenCorreo;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuariosFiltro;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuariosPorId;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuariosPorVicepresidencia;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioDetalleRepositorio;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorio;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorioFiltro;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorioFiltroCorreo;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCaseFiltro;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCaseFiltroCorreo;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioDetalleCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerTodosUsuariosInfo;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.UsuarioMapperUsuarioEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.impl.FabricaUsuarioMapperImpl;
import com.pragma.perfiles.pragmadata.aplicacion.fabrica.FabricaPragmaData;
import com.pragma.perfiles.pragmadata.aplicacion.fabrica.impl.PragmaDataMapperPragmaDataArchivo;
import com.pragma.perfiles.pragmadata.aplicacion.manejador.ManejadorConsultaPragmaData;
import com.pragma.perfiles.pragmadata.aplicacion.manejador.ManejadorObtenerCampos;
import com.pragma.perfiles.pragmadata.infraestructura.clientefeign.FeignArchivosUtilitario;
import com.pragma.perfiles.tipo_contrato.aplicacion.manejador.ManejadorObtenerTipoContratoPorCoincidenciaNombre;
import com.pragma.perfiles.tipo_contrato.dominio.repositorio.TipoContratoRepositorio;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.dao.TipoContratoDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;

@Configuration
public class BeanConfiguracion {

    @Bean
    public ManejadorGuardarFotografiaUsuario manejadorGuardarFotografiaUsuario(UsuarioCrudUseCase usuarioCrudUseCase, FabricaUsuario fabricaUsuario, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso) {
        return new ManejadorGuardarFotografiaUsuario(usuarioCrudUseCase, fabricaUsuario, manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public ManejadorActualizarFotografiaUsuario manejadorActualizarFotografiaUsuario(UsuarioCrudUseCase usuarioCrudUseCase, FabricaUsuario fabricaUsuario, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso) {
        return new ManejadorActualizarFotografiaUsuario(usuarioCrudUseCase, fabricaUsuario, manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public ManejadorGuardarUsuarioNuevoToken manejadorGuardarUsuarioNuevoToken(FeignTipoDocumento feignTipoDocumento,
                                                                               UsuarioCrudUseCase usuarioCrudUseCase,
                                                                               FabricaUsuario fabricaUsuario,
                                                                               ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso,
                                                                               InformacionLaboralCrudUseCase informacionLaboralCrudUseCase,
                                                                               InformacionArchivoNuevoPragmaticoPropiedades informacionArchivoNuevoPragmaticoPropiedades,
                                                                               TipoContratoRepositorio tipoContratoRepositorio,
                                                                               HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase,
                                                                               TipoCambioCrudUseCase tipoCambioCrudUseCase,
                                                                               AWSCognitoIdentityProvider cognitoClient) {
        return new ManejadorGuardarUsuarioNuevoToken(feignTipoDocumento,
                usuarioCrudUseCase,
                fabricaUsuario,
                manejadorCalcularValorBarraProgreso,
                informacionLaboralCrudUseCase,
                informacionArchivoNuevoPragmaticoPropiedades,
                tipoContratoRepositorio,
                historicoDeCambiosCrudUseCase,
                tipoCambioCrudUseCase,
                cognitoClient);
    }

    @Bean
    public ManejadorActualizarUsuario manejadorActualizarUsuario(UsuarioCrudUseCase usuarioCrudUseCase, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso) {
        return new ManejadorActualizarUsuario(usuarioCrudUseCase, manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public ManejadorActualizarUsuarioDatosHojaDeVida manejadorActualizarUsuarioDatosHojaDeVida(UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                               ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgres,
                                                                                               UsuarioDetalleCrudUseCase usuarioDetalleCrudUseCase) {
        return new ManejadorActualizarUsuarioDatosHojaDeVida(usuarioCrudUseCase, usuarioDetalleCrudUseCase, manejadorCalcularValorBarraProgres);
    }

    @Bean
    public ManejadorEliminarUsuarioPorId manejadorEliminarUsuarioPorId(UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorEliminarUsuarioPorId(usuarioCrudUseCase);
    }

    @Bean
    public ManejadorGuardarUsuario manejadorGuardarUsuario(UsuarioCrudUseCase usuarioCrudUseCase, FabricaUsuario fabricaUsuario, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso) {
        return new ManejadorGuardarUsuario(usuarioCrudUseCase, fabricaUsuario, manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public ManejadorGuardarUsuarioNuevo manejadorGuardarUsuarioNuevo(UsuarioCrudUseCase usuarioCrudUseCase, FabricaUsuario fabricaUsuario, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso) {
        return new ManejadorGuardarUsuarioNuevo(usuarioCrudUseCase, fabricaUsuario, manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public ManejadorObtenerUsuarioPorId manejadorObtenerUsuarioPorId(UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorObtenerUsuarioPorId(usuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerUsuariosPorId manejadorObtenerUsuariosPorId(UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorObtenerUsuariosPorId(usuarioCrudUseCase);
    }

    @Bean
    public ManejadorActualizarUsuarioSistemaExterno manejadorActualizarUsuarioSistemaExterno(FeignTipoDocumento feignTipoDocumento, UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                             ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso,
                                                                                             InformacionNominaCrudUseCase informacionNominaCrudUseCase,
                                                                                             InformacionLaboralCrudUseCase informacionLaboralCrudUseCase,
                                                                                             ManejadorObtenerTipoContratoPorCoincidenciaNombre manejadorObtenerTipoContratoPorCoincidenciaNombre) {
        return new ManejadorActualizarUsuarioSistemaExterno(feignTipoDocumento, usuarioCrudUseCase,
                manejadorCalcularValorBarraProgreso,
                informacionNominaCrudUseCase,
                informacionLaboralCrudUseCase,
                manejadorObtenerTipoContratoPorCoincidenciaNombre);
    }

    @Bean
    public ManejadorObtenerTipoContratoPorCoincidenciaNombre manejadorObtenerTipoContratoPorCoincidenciaNombre(TipoContratoRepositorio tipoContratoRepositorio) {
        return new ManejadorObtenerTipoContratoPorCoincidenciaNombre(tipoContratoRepositorio);
    }

    @Bean
    public ManejadorMigrarDatosSalariosPorPersona manejadorMigrarDatosSalariosPorPersona(
            FeignTipoDocumento feignTipoDocumento,
            UsuarioCrudUseCase usuarioCrudUseCase,
            FabricaUsuario fabricaUsuario,
            ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso,
            InformacionNominaCrudUseCase informacionNominaCrudUseCase,
            InformacionLaboralCrudUseCase informacionLaboralCrudUseCase,
            InformacionArchivoNuevoPragmaticoPropiedades informacionArchivoNuevoPragmaticoPropiedades,
            UsuarioMapperUsuarioEntity usuarioMapperUsuarioEntity,
            ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase,
            ManejadorObtenerTipoContratoPorCoincidenciaNombre manejadorObtenerTipoContratoPorCoincidenciaNombre,
            CopyProperties copiaPropiedades) {
        return new ManejadorMigrarDatosSalariosPorPersona(feignTipoDocumento,
                usuarioCrudUseCase,
                fabricaUsuario,
                manejadorCalcularValorBarraProgreso,
                informacionNominaCrudUseCase,
                informacionLaboralCrudUseCase,
                informacionArchivoNuevoPragmaticoPropiedades,
                usuarioMapperUsuarioEntity,
                contratoHistoricoCrudUseCase,
                manejadorObtenerTipoContratoPorCoincidenciaNombre,
                copiaPropiedades);
    }

    @Bean
    public UsuarioCrudUseCase usuarioCrudUseCase(UsuarioRepositorio usuarioRepositorio) {
        return new UsuarioCrudUseCase(usuarioRepositorio);
    }

    @Bean
    public FabricaUsuario fabricaUsuario(UsuarioMapperGuardarUsuarioComando usuarioMapperGuardarUsuarioComando,
                                         UsuarioMapperGuardarUsuarioNuevoComando usuarioMapperGuardarUsuarioNuevoComando,
                                         UsuarioMapperActualizarUsuarioComando usuarioMapperActualizarUsuarioComando,
                                         UsuarioMapperObtenerUsuariosFiltroComando usuarioMapperObtenerUsuariosFiltroComando,
                                         UsuarioMapperObtenerUsuarioFiltroCorreoComando usuarioMapperObtenerUsuarioFiltroCorreoComando,
                                         UsuarioMapperObtenerUsuarioPorTokenCorreo usuarioMapperObtenerUsuarioPorTokenCorreo,
                                         UsuarioMapperGuardarFotografiaComando usuarioMapperGuardarFotografiaComando,
                                         UsuarioMapperActualizarFotografiaComando usuarioMapperActualizarFotografiaComando,
                                         UsuarioMapperObtenerUsuarioPorTokenIdentificacion usuarioMapperObtenerUsuarioPorTokenIdentificacion,
                                         UsuarioMapperUsuarioContextoComando usuarioMapperUsuarioContextoComando,
                                         UsuarioMapperActualizarUsuarioNuevoPragmatico usuarioMapperActualizarUsuarioNuevoPragmatico,
                                         UsuarioMapperInformacionConocimientoArchivo usuarioMapperInformacionConocimientoArchivo,
                                         InformacionNominaMapperActualizarUsuarioNuevoPragmatico informacionNominaMapperActualizarUsuarioNuevoPragmatico,
                                         InformacionLaboralMapperActualizarNuevoPragmatico informacionLaboralMapperActualizarNuevoPragmatico

    ) {
        return new FabricaUsuario(
                usuarioMapperGuardarUsuarioComando,
                usuarioMapperInformacionConocimientoArchivo,
                usuarioMapperGuardarUsuarioNuevoComando,
                usuarioMapperActualizarUsuarioComando,
                usuarioMapperObtenerUsuariosFiltroComando,
                usuarioMapperObtenerUsuarioFiltroCorreoComando,
                usuarioMapperObtenerUsuarioPorTokenCorreo,
                usuarioMapperGuardarFotografiaComando,
                usuarioMapperActualizarFotografiaComando,
                usuarioMapperObtenerUsuarioPorTokenIdentificacion,
                usuarioMapperUsuarioContextoComando,
                usuarioMapperActualizarUsuarioNuevoPragmatico,
                informacionNominaMapperActualizarUsuarioNuevoPragmatico,
                informacionLaboralMapperActualizarNuevoPragmatico
        );
    }

    @Bean
    public ManejadorObtenerUsuariosFiltro manejadorObtenerUsuariosFiltro(UsuarioCrudUseCaseFiltro usuarioCrudUseCaseFiltro,
                                                                         FabricaUsuario fabricaUsuario) {
        return new ManejadorObtenerUsuariosFiltro(usuarioCrudUseCaseFiltro, fabricaUsuario);
    }

    @Bean
    public ManejadorAgregarDiasTomadosUsuario manejadorAgregarDiasTomadosUsuario(UsuarioCrudUseCaseFiltroCorreo usuarioCrudUseCaseFiltroCorreo,
                                                                                 FabricaUsuario fabricaUsuario,
                                                                                 UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorAgregarDiasTomadosUsuario(usuarioCrudUseCaseFiltroCorreo, fabricaUsuario, usuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerUsuarioPorTokenCorreo manejadorObtenerUsuarioPorTokenCorreo(UsuarioCrudUseCaseFiltroCorreo usuarioCrudUseCaseFiltroCorreo,
                                                                                       FabricaUsuario fabricaUsuario) {
        return new ManejadorObtenerUsuarioPorTokenCorreo(usuarioCrudUseCaseFiltroCorreo, fabricaUsuario);
    }

    @Bean
    public UsuarioCrudUseCaseFiltro usuarioCrudUseCaseFiltro(UsuarioRepositorioFiltro usuarioRepositorioFiltro) {
        return new UsuarioCrudUseCaseFiltro(usuarioRepositorioFiltro);
    }

    @Bean
    public ManejadorObtenerUsuarioFiltroCorreo manejadorObtenerUsuarioFiltroCorreo(UsuarioCrudUseCaseFiltroCorreo usuarioCrudUseCaseFiltroCorreo,
                                                                                   FabricaUsuario fabricaUsuario) {
        return new ManejadorObtenerUsuarioFiltroCorreo(usuarioCrudUseCaseFiltroCorreo, fabricaUsuario);
    }

    @Bean
    public ManejadorObtenerUsuarioPorCorreo manejadorObtenerUsuarioPorCorreo(UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorObtenerUsuarioPorCorreo(usuarioCrudUseCase);
    }

    @Bean
    public UsuarioCrudUseCaseFiltroCorreo usuarioCrudUseCaseFiltroCorreo(UsuarioRepositorioFiltroCorreo usuarioRepositorioFiltroCorreo) {
        return new UsuarioCrudUseCaseFiltroCorreo(usuarioRepositorioFiltroCorreo);
    }

    @Bean
    public ManejadorObtenerUsuarioLista manejadorObtenerUsuarioLista(UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorObtenerUsuarioLista(usuarioCrudUseCase);
    }

    @Bean
    public ManejadorActualizarUsuarioPorCorreo manejadorActualizarUsuarioPorCorreo(UsuarioCrudUseCase usuarioCrudUseCase, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgres) {
        return new ManejadorActualizarUsuarioPorCorreo(usuarioCrudUseCase, manejadorCalcularValorBarraProgres);
    }

    @Bean
    public ManejadorActualizarEstadoUsuarioPragmadex actualizarEstadoUsuarioPragmadex(
            UsuarioCrudUseCase usuarioCrudUseCase
    ) {
        return new ManejadorActualizarEstadoUsuarioPragmadex(usuarioCrudUseCase);
    }

    @Bean
    public ManejadorActualizarDiasVacaciones manejadorActualizarDiasVacaciones(
            UsuarioCrudUseCase usuarioCrudUseCase, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgres
    ) {
        return new ManejadorActualizarDiasVacaciones(usuarioCrudUseCase, manejadorCalcularValorBarraProgres);
    }

    @Bean
    public ManejadorActualizarUsuarioGeneralPorId manejadorActualizarUsuarioGeneralPorId(UsuarioCrudUseCase usuarioCrudUseCase, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgres) {
        return new ManejadorActualizarUsuarioGeneralPorId(usuarioCrudUseCase, manejadorCalcularValorBarraProgres);
    }


    @Bean
    public ManejadorObtenerUsuariosPorVicepresidencia manejadorObtenerUsuariosPorViceprecidencia(UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorObtenerUsuariosPorVicepresidencia(usuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerUsuarioGoogleId manejadorObtenerUsuarioGoogleId(UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorObtenerUsuarioGoogleId(usuarioCrudUseCase);
    }

    @Bean
    public FabricaUsuarioMapperImpl fabricaUsuarioMapper(UsuarioMapperUsuarioEntity usuarioMapperUsuarioEntity) {
        return new FabricaUsuarioMapperImpl(usuarioMapperUsuarioEntity);
    }

    @Bean
    public ManejadorObtenerUsuarioPorToken manejadorObtenerUsuarioPorToken(UsuarioCrudUseCaseFiltroCorreo usuarioCrudUseCaseFiltroCorreo, FabricaUsuario fabricaUsuario) {
        return new ManejadorObtenerUsuarioPorToken(usuarioCrudUseCaseFiltroCorreo, fabricaUsuario);
    }

    @Bean
    public ManejadorObtenerUsuarioPorNombre manejadorObtenerUsuarioPorNombre(UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorObtenerUsuarioPorNombre(usuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerCampos manejadorObtenerCampos() {
        return new ManejadorObtenerCampos();
    }

    @Bean
    public ManejadorConsultaPragmaData manejadorConsultarPrgamaData(UsuarioRepositorio usuarioRepositorio, ConversionService conversionService, DiasNoCelebrablesUsuarioRepositorio diasNoCelebrablesUsuarioRepositorio, FabricaPragmaData fabricaPragmaData, FeignArchivosUtilitario feignArchivosUtilitario) {
        return new ManejadorConsultaPragmaData(usuarioRepositorio, diasNoCelebrablesUsuarioRepositorio, conversionService);
    }

    @Bean
    public ManejadorObtenerNuevoPragmatico manejadorObtenerNuevoPragmatico(InformacionArchivoNuevoPragmaticoPropiedades informacionArchivoNuevoPragmaticoPropiedades) {
        return new ManejadorObtenerNuevoPragmatico(informacionArchivoNuevoPragmaticoPropiedades);
    }

    @Bean
    public ManejadorObtenerUsuarioPorIdentificacion manejadorObtenerUsuarioPorIdentificacion(UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorObtenerUsuarioPorIdentificacion(usuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerUsuarioContratoPorUsuarioId manejadorObtenerUsuarioContratoPorUsuarioId(UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerUsuarioContratoPorUsuarioId(usuarioCrudUseCase);
    }

    @Bean
    public ManejadorActualizarContratoActual manejadorActualizarContratoActual(FeignTipoDocumento feignTipoDocumento, InformacionNominaCrudUseCase informacionNomina, HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase, UsuarioCrudUseCase usuarioCrudUseCase, TipoContratoDao tipoContratoDao, TipoCambioCrudUseCase tipoCambioCrudUseCase){
        return new ManejadorActualizarContratoActual(feignTipoDocumento, informacionNomina, historicoDeCambiosCrudUseCase, usuarioCrudUseCase, tipoContratoDao, tipoCambioCrudUseCase);
    }

    @Bean
    public CopyProperties copyProperties() {
        return new CopyProperties();
    }

    @Bean
    public FabricaPragmaData fabricaPragmaData(PragmaDataMapperPragmaDataArchivo pragmaDataMapperPragmaDataArchivo) {
        return new FabricaPragmaData(pragmaDataMapperPragmaDataArchivo);
    }

    @Bean
    public ManejadorActualizarUsuarioRetirado manejadorGuardarInfoContratoNominaRetirado(
        FeignTipoDocumento feignTipoDocumento,
        HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase,
        UsuarioCrudUseCase usuarioCrudUseCase,
        AWSCognitoIdentityProvider awsCognitoIdentityProvider) {
        return new ManejadorActualizarUsuarioRetirado(feignTipoDocumento,
                historicoDeCambiosCrudUseCase,
                usuarioCrudUseCase,
                awsCognitoIdentityProvider);
    }

    @Bean
    public ManejadorObtenerTodosUsuariosInfo manejadorObtenerTodosUsuariosInfo(UsuarioCrudUseCase usuarioCrudUseCase, FeignTipoDocumento feignTipoDocumento, FabricaUsuario fabricaUsuario){
        return new ManejadorObtenerTodosUsuariosInfo(usuarioCrudUseCase,feignTipoDocumento,fabricaUsuario);
    }

    @Bean
    public ManejadorObtenerUsuarioCorreoSimple manejadorObtenerUsuarioCorreoSimple(UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerUsuarioCorreoSimple(usuarioCrudUseCase);
    }
}
