package com.pragma.perfiles.perfil.infraestructura.clientefeign;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObjetoFeignRespuestaEliminar {
    private String message;
    private String file;
}
