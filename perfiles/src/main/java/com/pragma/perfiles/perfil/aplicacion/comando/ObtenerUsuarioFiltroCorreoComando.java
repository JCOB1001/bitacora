package com.pragma.perfiles.perfil.aplicacion.comando;

import com.pragma.vacaciones.common.filtro.FiltroBase;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
public class ObtenerUsuarioFiltroCorreoComando extends FiltroBase {

    private String correo;

    public ObtenerUsuarioFiltroCorreoComando(){
        super(0, 1);
    }

    public ObtenerUsuarioFiltroCorreoComando(String correo) {
        super(0, 1); //numeroPagina = 0 y totalElementos = 1
        this.correo = correo;
    }

}
