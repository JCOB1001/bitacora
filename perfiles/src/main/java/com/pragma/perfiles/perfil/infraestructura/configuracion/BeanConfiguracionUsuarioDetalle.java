package com.pragma.perfiles.perfil.infraestructura.configuracion;

import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioDetalleRepositorio;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioDetalleCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguracionUsuarioDetalle {
    @Bean
    public UsuarioDetalleCrudUseCase usuarioDetalleCrudUseCase(UsuarioDetalleRepositorio usuarioDetalleRepositorio){
        return  new UsuarioDetalleCrudUseCase(usuarioDetalleRepositorio);
    }
}
