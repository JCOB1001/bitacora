package com.pragma.perfiles.perfil.infraestructura.persistencia.dao.impl;

import com.pragma.perfiles.comun.infraestructura.error.SolicitudIncorrecta;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.UsuarioConocimientoEntity;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.UsuarioConocimientoEntity.ATRIBUTOS;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuarioCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.EstadoProfesion;
import com.pragma.perfiles.perfil.dominio.modelo.Fotografia;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstadoCivil;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGrupoSanguineo;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioInfo;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioTallaCamisa;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuarioPragmaDex;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.UsuarioMapperUsuarioEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.impl.FabricaUsuarioMapperImpl;
import com.pragma.perfiles.perfil.infraestructura.persistencia.dao.UsuarioCustomDao;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity.Atributos;
import com.pragma.perfiles.pragmadata.aplicacion.comando.ConsultaPragmaDataComando;
import com.pragma.perfiles.pragmadata.dominio.modelo.TipoDato;
import com.pragma.perfiles.pragmadata.dominio.respuesta.ObjetoRespuestaUsuarioPragmaData;
import com.pragma.perfiles.pragmadex.dominio.filtros.FiltroNombreCorreo;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.entidad.TipoContratoEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Transactional(readOnly = true)
@RequiredArgsConstructor
public class UsuarioDaoImpl implements UsuarioCustomDao {

    private final EntityManager entityManager;
    private final FabricaUsuarioMapperImpl fabricaUsuarioMapper;
    private final UsuarioMapperUsuarioEntity usuarioMapperUsuarioEntity;

    @Override
    public ObjetoRespuestaUsuario buscarTodosFiltrando(FiltroUsuario filtroUsuario) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        From alcanceFrom = criteriaQuery.from(UsuarioEntity.class);
        aplicarWhere(criteriaQuery, alcanceFrom,criteriaBuilder, filtroUsuario);


        TypedQuery<Long> resultQueryCantidad = entityManager.createQuery(
                criteriaQuery
                        .select(
                                criteriaBuilder.count(alcanceFrom)
                        )
        );

        double numeroPaginasDividendo = 0;

        if (filtroUsuario.getTotalElementos() == null) {
            numeroPaginasDividendo = 1;
        } else {
            numeroPaginasDividendo = filtroUsuario.getTotalElementos();
        }

        long numeropaginas = (long) Math.ceil((double) resultQueryCantidad.getSingleResult() / numeroPaginasDividendo);
        aplicarOrden(criteriaQuery, alcanceFrom, criteriaBuilder, filtroUsuario);
        TypedQuery<UsuarioEntity> resultQuery = entityManager.createQuery(
                criteriaQuery
                        .select(
                                alcanceFrom
                        )
        );
        aplicarCantidad(resultQuery, filtroUsuario.getNumeroPagina(), filtroUsuario.getTotalElementos());
        List<UsuarioEntity> usuariosEntity = resultQuery.getResultList();
        List<Usuario> usuarios = new ArrayList<>();
        for (UsuarioEntity usuarioEntity : usuariosEntity) {
            List<Fotografia> fotografias = usuarioEntity.getFotografias()
                    .stream()
                    .map(foto -> new Fotografia(foto.getId(), foto.getContenido(), foto.getTipoFotografia()))
                    .collect(Collectors.toList()
                    );

            usuarios.add(usuarioMapperUsuarioEntity.rightToLeft(usuarioEntity));
        }
        boolean ultimaPagina = false;
        if (filtroUsuario.getNumeroPagina() != null) {
            ultimaPagina = filtroUsuario.getNumeroPagina() == (numeropaginas - 1);
        }
        return new ObjetoRespuestaUsuario(usuarios.stream(),
                numeropaginas,
                ultimaPagina);
    }

    private void aplicarWhere(CriteriaQuery criteriaQuery, From alcanceFrom,CriteriaBuilder criteriaBuilder, FiltroUsuario filtroUsuario) {
        List<Predicate> predicates = new ArrayList<>();

        if (filtroUsuario.getIdentificacion() != null) {
            if (!filtroUsuario.getIdentificacion().isBlank()) {
                predicates.add(alcanceFrom.get(UsuarioEntity.Atributos.IDENTIFICACION).in(filtroUsuario.getIdentificacion()));
            }
        }
        if (filtroUsuario.getNombres() != null){
            String nombreCompleto = new StringBuilder("%"+filtroUsuario.getNombres().trim().replace(" ", "%")+"%").toString();
            predicates.add(criteriaBuilder.like(criteriaBuilder.concat(alcanceFrom.get(Atributos.NOMBRES), alcanceFrom.get(Atributos.APELLIDOS)), nombreCompleto));
        }
        if (filtroUsuario.getEstado() != null){
            predicates.add(criteriaBuilder.equal(alcanceFrom.get(UsuarioEntity.Atributos.ESTADO),filtroUsuario.getEstado()));
        }
        if (!predicates.isEmpty()) {
            criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        }
    }

    private void aplicarOrden(CriteriaQuery criteriaQuery, From alcanceFrom, CriteriaBuilder criteriaBuilder, FiltroUsuario filtroUsuario) {
        List<Order> orders = new ArrayList<>();
        if (filtroUsuario.getFechaCreacionAscendente() != null) {
            if (filtroUsuario.getFechaCreacionAscendente()) {
                orders.add(criteriaBuilder.asc(alcanceFrom.get(UsuarioEntity.Atributos.FECHA_CREACION)));
            } else {
                orders.add(criteriaBuilder.desc(alcanceFrom.get(UsuarioEntity.Atributos.FECHA_CREACION)));
            }
        }
        if (filtroUsuario.getFechaInicioContratoAscendente() != null){
            if (filtroUsuario.getFechaInicioContratoAscendente()){
                orders.add(criteriaBuilder.asc(alcanceFrom.get(UsuarioEntity.Atributos.FECHA_INICIO_CONTRATO)));
            }else{
                orders.add(criteriaBuilder.desc(alcanceFrom.get(UsuarioEntity.Atributos.FECHA_INICIO_CONTRATO)));
            }
        }
        if (!orders.isEmpty()) {
            criteriaQuery.orderBy(orders.toArray(new Order[orders.size()]));
        }
    }

    private void aplicarCantidad(TypedQuery<UsuarioEntity> resultQuery, Integer numeroPagina, Integer totalElementos) {
        if (numeroPagina != null && totalElementos != null && (numeroPagina != -1 && totalElementos != -1)) {
            resultQuery.setFirstResult(numeroPagina * totalElementos);
            resultQuery.setMaxResults(totalElementos);
        }
    }

    @Override
    public ObjetoRespuestaUsuario buscarTodosFiltrandoCorreo(FiltroUsuarioCorreo filtroUsuarioCorreo) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        From alcanceFrom = criteriaQuery.from(UsuarioEntity.class);
        aplicarWhereCorreo(criteriaQuery, alcanceFrom, filtroUsuarioCorreo);
        TypedQuery<Long> resultQueryCantidad = entityManager.createQuery(
                criteriaQuery
                        .select(
                                criteriaBuilder.count(alcanceFrom)
                        )
        );
        long numeropaginas = (long) Math.ceil((double) resultQueryCantidad.getSingleResult() / (double) filtroUsuarioCorreo.getTotalElementos());
        TypedQuery<UsuarioEntity> resultQuery = entityManager.createQuery(
                criteriaQuery
                        .select(
                                alcanceFrom
                        )
        );
        aplicarCantidad(resultQuery, filtroUsuarioCorreo.getNumeroPagina(), filtroUsuarioCorreo.getTotalElementos());
        List<UsuarioEntity> usuariosEntity = resultQuery.getResultList();
        List<Usuario> usuarios = new ArrayList<>();
        for (UsuarioEntity usuarioEntity : usuariosEntity) {
            List<Fotografia> fotografias = usuarioEntity.getFotografias()
                    .stream()
                    .map(foto -> new Fotografia(foto.getId(), foto.getContenido(), foto.getTipoFotografia()))
                    .collect(Collectors.toList()
                    );
            usuarios.add(usuarioMapperUsuarioEntity.rightToLeft(usuarioEntity));
        }
        return new ObjetoRespuestaUsuario(usuarios.stream(),
                numeropaginas,
                filtroUsuarioCorreo.getNumeroPagina() == numeropaginas - 1);
    }

    private void aplicarWhereCorreo(CriteriaQuery criteriaQuery, From alcanceFrom, FiltroUsuarioCorreo filtroUsuarioCorreo) {
        List<Predicate> predicates = new ArrayList<>();
        if (filtroUsuarioCorreo.getCorreo() != null) {
            if (!filtroUsuarioCorreo.getCorreo().isBlank()) {
                predicates.add(alcanceFrom.get(UsuarioEntity.Atributos.CORREO_EMPRESARIAL).in(filtroUsuarioCorreo.getCorreo()));
            }
        }
        if (!predicates.isEmpty()) {
            criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        }
    }

    @Override
    public ObjetoRespuestaUsuarioPragmaDex buscarUsuarioPorNombreCorreo(FiltroNombreCorreo filtroNombreCorreo) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UsuarioEntity> criteriaQuery = criteriaBuilder.createQuery(UsuarioEntity.class);
        CriteriaQuery<Long> criteriaQueryCount = criteriaBuilder.createQuery(Long.class);
        Root<UsuarioEntity> usuarioRoot = criteriaQuery.from(UsuarioEntity.class);
        List<Predicate> predicatesOr = new ArrayList<>();
        if (filtroNombreCorreo.getNombreOrCorreo() != null && !filtroNombreCorreo.getNombreOrCorreo().isBlank()) {
            predicatesOr.add(
                    criteriaBuilder.like(
                            usuarioRoot.get(UsuarioEntity.Atributos.NOMBRES),
                            "%" + filtroNombreCorreo.getNombreOrCorreo() + "%"
                    )
            );
            predicatesOr.add(
                    criteriaBuilder.like(
                            usuarioRoot.get(UsuarioEntity.Atributos.CORREO_EMPRESARIAL),
                            "%" + filtroNombreCorreo.getNombreOrCorreo() + "%"
                    )
            );
            predicatesOr.add(
                    criteriaBuilder.like(
                            usuarioRoot.get(UsuarioEntity.Atributos.APELLIDOS),
                            "%" + filtroNombreCorreo.getNombreOrCorreo() + "%"
                    )
            );
        }

        usuarioRoot.fetch(UsuarioEntity.Atributos.FOTOGRAFIAS, JoinType.LEFT);
        List<Predicate> predicatesAnd = new ArrayList<>();
        if (!predicatesOr.isEmpty()) {
            predicatesAnd.add(criteriaBuilder.or(predicatesOr.toArray(new Predicate[]{})));
        }
        if (!filtroNombreCorreo.isAdmin()) {
            predicatesAnd.add(criteriaBuilder.equal(usuarioRoot.get(Atributos.ESTADO), UsuarioEstado.ACTIVO));
        }
        if (filtroNombreCorreo.getMinPorcentaje() > -1) {
            predicatesAnd.add(criteriaBuilder.greaterThanOrEqualTo(usuarioRoot.get(Atributos.BARRA_PROGRESO), (float) filtroNombreCorreo.getMinPorcentaje() / 100));
        }
        if (filtroNombreCorreo.getMaxPorcentaje() > -1) {
            predicatesAnd.add(criteriaBuilder.lessThanOrEqualTo(usuarioRoot.get(Atributos.BARRA_PROGRESO), (float) filtroNombreCorreo.getMaxPorcentaje() / 100));
        }
        TypedQuery<UsuarioEntity> resultQuery = entityManager.createQuery(
                criteriaQuery
                        .select(usuarioRoot)
                        .where(
                                predicatesAnd.toArray(new Predicate[]{})
                        )
                        .orderBy(criteriaBuilder.asc(usuarioRoot.get(Atributos.NOMBRES)))
        );
        Long count = entityManager.createQuery(
                criteriaQueryCount
                        .select(
                                criteriaBuilder.count(criteriaQueryCount.from(UsuarioEntity.class))
                        )
                        .where(predicatesAnd.toArray(new Predicate[]{}))).getSingleResult();
        aplicarCantidad(resultQuery, filtroNombreCorreo.getNumeroPagina(), filtroNombreCorreo.getTotalElementos());
        List<UsuarioEntity> usuarios = resultQuery.getResultList();
        long numeroPaginas = (long) Math.ceil((double) count / filtroNombreCorreo.getTotalElementos());
        boolean ultimaPagina = numeroPaginas == (Long.valueOf(filtroNombreCorreo.getNumeroPagina()) - 1);
        if (filtroNombreCorreo.getNumeroPagina() != null) {
            ultimaPagina = filtroNombreCorreo.getNumeroPagina() == (numeroPaginas - 1);
        }
        return new ObjetoRespuestaUsuarioPragmaDex(usuarios.stream(),
                count,
                ultimaPagina,
                usuarios.size()
        );
    }

    @Override
    public ObjetoRespuestaUsuarioPragmaDex buscarUsuariosPorHabilidadPaginado(FiltroNombreCorreo filtroNombreCorreo) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UsuarioEntity> criteriaQuery = criteriaBuilder.createQuery(UsuarioEntity.class);
        CriteriaQuery<Long> criteriaQueryCount = criteriaBuilder.createQuery(Long.class);
        Root<UsuarioEntity> usuarioRoot = criteriaQuery.from(UsuarioEntity.class);

        Subquery<String> subConsulta = criteriaQuery.subquery(String.class);
        Root<UsuarioConocimientoEntity> subRoot = subConsulta.from(UsuarioConocimientoEntity.class);

        subConsulta.select(subRoot.get(ATRIBUTOS.USUARIO));
        subConsulta.where(criteriaBuilder.like(subRoot.get(ATRIBUTOS.NOMBRE_SKILL), '%' + filtroNombreCorreo.getNombreOrCorreo() + '%'));
        subConsulta.distinct(true);

        List<Predicate> predicatesOr = new ArrayList<>();
        List<Predicate> predicatesAnd = new ArrayList<>();
        long numeroPaginas;

        usuarioRoot.fetch(UsuarioEntity.Atributos.FOTOGRAFIAS, JoinType.LEFT);
        if (!filtroNombreCorreo.isAdmin()) {
            predicatesAnd.add(criteriaBuilder.equal(usuarioRoot.get(Atributos.ESTADO), UsuarioEstado.ACTIVO));
        }
        if (!predicatesOr.isEmpty()) {
            predicatesAnd.add(criteriaBuilder.or(predicatesOr.toArray(new Predicate[]{})));
        }
        if (filtroNombreCorreo.getMinPorcentaje() > -1) {
            predicatesAnd.add(criteriaBuilder.greaterThanOrEqualTo(usuarioRoot.get(Atributos.BARRA_PROGRESO), (float) filtroNombreCorreo.getMinPorcentaje() / 100));
        }
        if (filtroNombreCorreo.getMaxPorcentaje() > -1) {
            predicatesAnd.add(criteriaBuilder.lessThanOrEqualTo(usuarioRoot.get(Atributos.BARRA_PROGRESO), (float) filtroNombreCorreo.getMaxPorcentaje() / 100));
        }
        if (filtroNombreCorreo.getNombreOrCorreo() != null && !filtroNombreCorreo.getNombreOrCorreo().isBlank()) {
            predicatesAnd.add(
                    (usuarioRoot.get(Atributos.ID).in(subConsulta))
            );

        }

        TypedQuery<UsuarioEntity> resultQuery = entityManager.createQuery(
                criteriaQuery
                        .select(usuarioRoot)
                        .where(
                                predicatesAnd.toArray(new Predicate[]{})
                        )
                        .orderBy(criteriaBuilder.asc(usuarioRoot.get(Atributos.NOMBRES)))
        )
                .setFirstResult(filtroNombreCorreo.getNumeroPagina() * filtroNombreCorreo.getTotalElementos())
                .setMaxResults(filtroNombreCorreo.getTotalElementos());
        Long count = entityManager.createQuery(
                criteriaQueryCount
                        .select(
                                criteriaBuilder.count(criteriaQueryCount.from(UsuarioEntity.class))
                        )
                        .where(predicatesAnd.toArray(new Predicate[]{}))).getSingleResult();
        List<UsuarioEntity> usuarios = resultQuery.getResultList();
        numeroPaginas = (long) Math.ceil((double) count / filtroNombreCorreo.getTotalElementos());
        boolean ultimaPagina = numeroPaginas == (filtroNombreCorreo.getNumeroPagina() - 1);
        return new ObjetoRespuestaUsuarioPragmaDex(usuarios.stream(),
                count,
                ultimaPagina,
                usuarios.size()
        );
    }

    @Override
    public List<String> buscarDatosPorListaIds(List<String> usuariosId, Class table, FiltroNombreCorreo filtroNombreCorreo) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        var criteriaQuery = criteriaBuilder.createQuery(UsuarioEntity.class);
        var usuarioRoot = criteriaQuery.from(table);
        List<UsuarioEntity> usuariosIds = usuariosId.stream().map(UsuarioEntity::new).collect(Collectors.toList());
        Stream<UsuarioEntity> resultList = entityManager.createQuery(
                criteriaQuery
                        .select(usuarioRoot.get("usuario"))
                        .where(
                                usuarioRoot.get("usuario").in(usuariosIds)
                        ).distinct(true)
        ).getResultStream();

        return resultList.map((UsuarioEntity::getId)).collect(Collectors.toList());
    }

    @Override
    public List<Usuario> buscarPorNombreCorreo(String nombre) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        From alcanceFrom = criteriaQuery.from(UsuarioEntity.class);
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(
                criteriaBuilder.like(alcanceFrom.get(Atributos.CORREO_EMPRESARIAL),
                        "%" + nombre + "%")
        );

        TypedQuery<UsuarioEntity> resultQuery = entityManager.createQuery(
                criteriaQuery
                        .select(alcanceFrom)
                        .where(predicates.toArray(new Predicate[predicates.size()]))
        );

        List<UsuarioEntity> usuarioStreamEntity = resultQuery.getResultList();
        List<Usuario> usuarios = new ArrayList<>();
        for (UsuarioEntity usuarioEntity : usuarioStreamEntity){
            usuarios.add(fabricaUsuarioMapper.usuarioEntityAUsuario(usuarioEntity));
        }

        return usuarios;
    }

    @Override
    public ObjetoRespuestaUsuarioPragmaData buscarUsuariosPorFiltros(List<ConsultaPragmaDataComando> campos, Integer numeroElementos, Integer numeroPagina) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UsuarioEntity> criteriaQuery = criteriaBuilder.createQuery(UsuarioEntity.class);
        CriteriaQuery<Long> criteriaQueryCount = criteriaBuilder.createQuery(Long.class);
        Root<UsuarioEntity> usuarioRoot = criteriaQuery.from(UsuarioEntity.class);
        List<Predicate> predicatesAnd = new ArrayList<>();
        for (ConsultaPragmaDataComando campo : campos) {
            if(campo.isObligatorio()){
                List<Predicate> predicatesOr = new ArrayList<>();
                campo.getFiltros().forEach(c -> {
                    predicatesOr.add(criteriaBuilder.like(
                            usuarioRoot.get(campo.getCampoTabla()),
                            "%" + c + "%"));
                });
                predicatesAnd.add(criteriaBuilder.or(predicatesOr.toArray(new Predicate[]{})));
            }
            else if(campo.getTipoDato().equals(TipoDato.TEXTO)){
                predicatesAnd.add(
                        usuarioRoot.get(campo.getCampoTabla()).in(campo.getFiltros()));
            }


            else if(campo.getTipoDato().equals(TipoDato.BOOLEANO)){
                Boolean aux = Objects.equals(campo.getFiltros().get(0).toLowerCase(), "si");
                if(Objects.equals(campo.getCampoTabla(), Atributos.VISA) || Objects.equals(campo.getCampoTabla(), Atributos.PROYECTOS_EEUU)) {
                    if (Objects.equals(aux, true)) {
                        predicatesAnd.add(criteriaBuilder.equal(usuarioRoot.get(campo.getCampoTabla()), Boolean.valueOf(true)));
                    } else{
                        if (Objects.equals(campo.getFiltros().get(0).toLowerCase(), "no")) {
                            predicatesAnd.add(criteriaBuilder.equal(usuarioRoot.get(campo.getCampoTabla()), Boolean.valueOf(false)));
                        }
                        else {
                            List<UsuarioEntity> usuarioPragmaData = new ArrayList<>();
                            return new ObjetoRespuestaUsuarioPragmaData(usuarioPragmaData.stream(),
                                    0L,
                                    false,
                                    0);
                        }
                    }
                }
            }


            else if(campo.getTipoDato().equals(TipoDato.ESTADO)){
                if(campo.getFiltros().get(0).toLowerCase().equals("inactivo")) {
                    predicatesAnd.add(criteriaBuilder.equal(usuarioRoot.get(Atributos.ESTADO), UsuarioEstado.INACTIVO));
                }else if(campo.getFiltros().get(0).toLowerCase().equals("activo")){
                    predicatesAnd.add(criteriaBuilder.equal(usuarioRoot.get(Atributos.ESTADO), UsuarioEstado.ACTIVO));
                }else{
                    List<UsuarioEntity> usuarios = new ArrayList<UsuarioEntity>();
                    return new ObjetoRespuestaUsuarioPragmaData(usuarios.stream(),
                            0L,
                            false,
                            0
                    );
                }
            }
            else if(campo.getTipoDato().equals(TipoDato.FECHA)){
                List<Predicate> predicatesOr = new ArrayList<>();
                campo.getFiltros().stream().forEach(s -> {
                    String[] fechas;

                    if  (s.contains("/")) {
                        throw new SolicitudIncorrecta("Formato de fecha invalido");
                    }

                    fechas = s.split("@");


                    if(fechas.length==1){
                        if  (fechas[0].split("-").length == 3) {
                            predicatesOr.add(criteriaBuilder
                                    .greaterThanOrEqualTo(
                                            usuarioRoot.get(campo.getCampoTabla()), LocalDate.parse(fechas[0])));
                        }
                        else {
                            predicatesOr.add(
                                    criteriaBuilder.and(
                                            criteriaBuilder.greaterThanOrEqualTo(
                                                    criteriaBuilder.function(
                                                            "MONTH", Integer.class, usuarioRoot.get(campo.getCampoTabla())),
                                                    Integer.parseInt(fechas[0].substring(0,2))),
                                            criteriaBuilder.greaterThanOrEqualTo(
                                                    criteriaBuilder.function("DAY", Integer.class, usuarioRoot.get(campo.getCampoTabla())),
                                                    Integer.parseInt(fechas[0].substring(3))
                                            )
                                    )
                            );
                        }
                    }
                    else if(fechas.length==2 && fechas[0].isBlank()){
                        if(fechas[1].split("-").length == 3) {
                            predicatesOr.add(criteriaBuilder
                                    .lessThanOrEqualTo(
                                            usuarioRoot.get(campo.getCampoTabla()), LocalDate.parse(fechas[1])));
                        }
                        else {
                            predicatesOr.add(
                                    criteriaBuilder.and(
                                            criteriaBuilder.lessThanOrEqualTo(
                                                    criteriaBuilder.function(
                                                            "MONTH", Integer.class, usuarioRoot.get(campo.getCampoTabla())),
                                                    Integer.parseInt(fechas[1].substring(0,2))),
                                            criteriaBuilder.lessThanOrEqualTo(
                                                    criteriaBuilder.function("DAY", Integer.class, usuarioRoot.get(campo.getCampoTabla())),
                                                    Integer.parseInt(fechas[1].substring(3))
                                            )
                                    )
                            );
                        }
                    }
                    else {
                        if(fechas[0].split("-").length == 3) {
                            predicatesOr.add(criteriaBuilder.between(
                                    usuarioRoot.get(
                                            campo.getCampoTabla()),
                                    LocalDate.parse(fechas[0]),
                                    LocalDate.parse(fechas[1]) )
                            );
                        }
                        else {
                            predicatesOr.add(
                                    criteriaBuilder.and(
                                            criteriaBuilder.between(
                                                    criteriaBuilder.function(
                                                            "MONTH", Integer.class, usuarioRoot.get(campo.getCampoTabla())
                                                    ),
                                                    Integer.parseInt(fechas[0].substring(0,2)),
                                                    Integer.parseInt(fechas[1].substring(0,2))
                                            ),
                                            criteriaBuilder.between(
                                                    criteriaBuilder.function(
                                                            "DAY", Integer.class, usuarioRoot.get(campo.getCampoTabla())
                                                    ),
                                                    Integer.parseInt(fechas[0].substring(3)),
                                                    Integer.parseInt(fechas[1].substring(3))
                                            ))
                            );
                        }
                    }
                });
                predicatesAnd.add(criteriaBuilder.or(predicatesOr.toArray(new Predicate[]{})));

            }else{
                List<Predicate> predicatesOr = new ArrayList<>();
                try {
                    campo.getFiltros().forEach(valorCampo -> {
                        Enum valor = stringToEnum(valorCampo, campo.getTipoDato());
                        predicatesOr.add(criteriaBuilder.equal(usuarioRoot.get(campo.getCampoTabla()), valor));
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new SolicitudIncorrecta("Objeto no reconocido como enum");
                }
                predicatesAnd.add(criteriaBuilder.or(predicatesOr.toArray(new Predicate[]{})));

            }

        }



        usuarioRoot.fetch(UsuarioEntity.Atributos.FOTOGRAFIAS, JoinType.LEFT);
        //predicatesAnd.add(criteriaBuilder.equal(usuarioRoot.get(Atributos.ESTADO), UsuarioEstado.ACTIVO));

        TypedQuery<UsuarioEntity> resultQuery = entityManager.createQuery(
                criteriaQuery
                        .select(usuarioRoot)
                        .where(
                                predicatesAnd.toArray(new Predicate[]{})
                        )
                        .orderBy(criteriaBuilder.asc(usuarioRoot.get(Atributos.NOMBRES)))
        );
        Long count = entityManager.createQuery(
                criteriaQueryCount
                        .select(
                                criteriaBuilder.count(criteriaQueryCount.from(UsuarioEntity.class))
                        )
                        .where(predicatesAnd.toArray(new Predicate[]{}))).getSingleResult();

        aplicarCantidad(resultQuery, numeroPagina, numeroElementos);
        List<UsuarioEntity> usuarios = resultQuery.getResultList();
        long numeroPaginas = (long) Math.ceil((double) count / numeroElementos);
        boolean ultimaPagina = numeroPaginas == (Long.valueOf(numeroPagina) - 1);
        return new ObjetoRespuestaUsuarioPragmaData(usuarios.stream(),
                count,
                ultimaPagina,
                usuarios.size()
        );
    }

    private Enum stringToEnum(String dato, TipoDato tipo){
        String datoUpper= dato.toUpperCase();
        switch (tipo){
            case GRUPO_SANGUINEO:
                return UsuarioGrupoSanguineo.valueOf(datoUpper);
            case TALLA_CAMISETA:
                return UsuarioTallaCamisa.valueOf(datoUpper);
            case ESTADO_CIVIL:
                return UsuarioEstadoCivil.valueOf(datoUpper);
            case ESTADO_PROFESION:
                return EstadoProfesion.valueOf(datoUpper);
            case ESTADO:
                return UsuarioEstado.valueOf(datoUpper);
            default:
                return null;
        }
    }

    @Override
    public UsuarioEntity buscarPorIdentificacion(Long idTipoDocumento, String identificacionUsuario) {
        CriteriaBuilder constructorCriteria = entityManager.getCriteriaBuilder();
        CriteriaQuery consultaCriteria = constructorCriteria.createQuery();
        From entidadSeleccionada = consultaCriteria.from(UsuarioEntity.class);

        Predicate predicadoIdTipoDocumento = constructorCriteria
                .equal(
                        entidadSeleccionada.get(Atributos.ID_TIPODOCUMENTO),
                        idTipoDocumento
                );

        Predicate predicadoIdentificacion = constructorCriteria
                .equal(
                        entidadSeleccionada.get(Atributos.IDENTIFICACION),
                        identificacionUsuario
                );

        Predicate predicadoFinal = constructorCriteria
                .and(predicadoIdTipoDocumento, predicadoIdentificacion);

        TypedQuery<UsuarioEntity> resultadoConsulta = entityManager.createQuery(
                consultaCriteria
                        .select(entidadSeleccionada)
                        .where(predicadoFinal)
        );

        List<UsuarioEntity> usuarioEntity = resultadoConsulta.getResultList();

        if (usuarioEntity.isEmpty()){
            throw new UsuarioNoEncontrado("usuario no encontrado");
        }

        return usuarioEntity.get(0);
    }

    @Override
    public ObjetoRespuestaUsuarioPragmaDex buscarUsuariosPorEmpresaYNombrePaginado(FiltroNombreCorreo filtroNombreCorreo) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UsuarioEntity> criteriaQuery = criteriaBuilder.createQuery(UsuarioEntity.class);
        CriteriaQuery<Long> criteriaQueryCount = criteriaBuilder.createQuery(Long.class);
        Root<UsuarioEntity> usuarioRoot = criteriaQuery.from(UsuarioEntity.class);
        List<Predicate> predicatesOr = new ArrayList<>();

        if (filtroNombreCorreo.getNombreOrCorreo() != null && !filtroNombreCorreo.getNombreOrCorreo().isBlank()) {
            predicatesOr.add(criteriaBuilder.and(
                    criteriaBuilder.like(usuarioRoot.get(UsuarioEntity.Atributos.EMPRESA),
                            "%" + filtroNombreCorreo.getEmpresa() + "%"),
                    criteriaBuilder.like(usuarioRoot.get(Atributos.NOMBRES),
                            "%" + filtroNombreCorreo.getNombreOrCorreo() + "%")
            ));
            predicatesOr.add(criteriaBuilder.and(
                    criteriaBuilder.like(usuarioRoot.get(UsuarioEntity.Atributos.EMPRESA),
                            "%" + filtroNombreCorreo.getEmpresa() + "%"),
                    criteriaBuilder.like(usuarioRoot.get(Atributos.CORREO_EMPRESARIAL),
                            "%" + filtroNombreCorreo.getNombreOrCorreo() + "%")
            ));
            predicatesOr.add(criteriaBuilder.and(
                    criteriaBuilder.like(usuarioRoot.get(UsuarioEntity.Atributos.EMPRESA),
                            "%" + filtroNombreCorreo.getEmpresa() + "%"),
                    criteriaBuilder.like(usuarioRoot.get(Atributos.APELLIDOS),
                            "%" + filtroNombreCorreo.getNombreOrCorreo() + "%")
            ));
        } else {
            if (filtroNombreCorreo.getEmpresa() != null && !filtroNombreCorreo.getEmpresa().isBlank()) {
                predicatesOr.add(
                        criteriaBuilder.like(
                                usuarioRoot.get(UsuarioEntity.Atributos.EMPRESA),
                                "%" + filtroNombreCorreo.getEmpresa() + "%"
                        )
                );
            }
        }

        usuarioRoot.fetch(UsuarioEntity.Atributos.FOTOGRAFIAS, JoinType.LEFT);
        List<Predicate> predicatesAnd = new ArrayList<>();
        if (!predicatesOr.isEmpty()) {
            predicatesAnd.add(criteriaBuilder.or(predicatesOr.toArray(new Predicate[]{})));
        }
        if (!filtroNombreCorreo.isAdmin()) {
            predicatesAnd.add(criteriaBuilder.equal(usuarioRoot.get(Atributos.ESTADO), UsuarioEstado.ACTIVO));
        }
        if (filtroNombreCorreo.getMinPorcentaje() > -1) {
            predicatesAnd.add(criteriaBuilder.greaterThanOrEqualTo(usuarioRoot.get(Atributos.BARRA_PROGRESO), (float) filtroNombreCorreo.getMinPorcentaje() / 100));
        }
        if (filtroNombreCorreo.getMaxPorcentaje() > -1) {
            predicatesAnd.add(criteriaBuilder.lessThanOrEqualTo(usuarioRoot.get(Atributos.BARRA_PROGRESO), (float) filtroNombreCorreo.getMaxPorcentaje() / 100));
        }
        TypedQuery<UsuarioEntity> resultQuery = entityManager.createQuery(
                criteriaQuery
                        .select(usuarioRoot)
                        .where(
                                predicatesAnd.toArray(new Predicate[]{})
                        )
                        .orderBy(criteriaBuilder.asc(usuarioRoot.get(Atributos.NOMBRES)))
        );
        Long count = entityManager.createQuery(
                criteriaQueryCount
                        .select(
                                criteriaBuilder.count(criteriaQueryCount.from(UsuarioEntity.class))
                        )
                        .where(predicatesAnd.toArray(new Predicate[]{}))).getSingleResult();
        aplicarCantidad(resultQuery, filtroNombreCorreo.getNumeroPagina(), filtroNombreCorreo.getTotalElementos());
        List<UsuarioEntity> usuarios = resultQuery.getResultList();
        long numeroPaginas = (long) Math.ceil((double) count / filtroNombreCorreo.getTotalElementos());
        boolean ultimaPagina = numeroPaginas == (Long.valueOf(filtroNombreCorreo.getNumeroPagina()) - 1);
        if (filtroNombreCorreo.getNumeroPagina() != null) {
            ultimaPagina = filtroNombreCorreo.getNumeroPagina() == (numeroPaginas - 1);
        }
        return new ObjetoRespuestaUsuarioPragmaDex(usuarios.stream(),
                count,
                ultimaPagina,
                usuarios.size()
        );
    }

    @Override
    public ObjetoRespuestaUsuarioPragmaDex buscarUsuariosPorEmpresaYHabilidadPaginado(FiltroNombreCorreo filtroNombreCorreo) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UsuarioEntity> criteriaQuery = criteriaBuilder.createQuery(UsuarioEntity.class);
        CriteriaQuery<Long> criteriaQueryCount = criteriaBuilder.createQuery(Long.class);
        Root<UsuarioEntity> usuarioRoot = criteriaQuery.from(UsuarioEntity.class);
        Subquery<String> subConsulta = criteriaQuery.subquery(String.class);
        Root<UsuarioConocimientoEntity> subRoot = subConsulta.from(UsuarioConocimientoEntity.class);

        subConsulta.select(subRoot.get(ATRIBUTOS.USUARIO));
        subConsulta.where(criteriaBuilder.and(
                criteriaBuilder.like(usuarioRoot.get(UsuarioEntity.Atributos.EMPRESA),
                        "%" + filtroNombreCorreo.getEmpresa() + "%"),
                criteriaBuilder.like(subRoot.get(ATRIBUTOS.NOMBRE_SKILL),
                        '%' + filtroNombreCorreo.getNombreOrCorreo() + '%')
        ));
        subConsulta.distinct(true);

        List<Predicate> predicatesOr = new ArrayList<>();
        List<Predicate> predicatesAnd = new ArrayList<>();
        long numeroPaginas;
        usuarioRoot.fetch(UsuarioEntity.Atributos.FOTOGRAFIAS, JoinType.LEFT);
        if (!filtroNombreCorreo.isAdmin()) {
            predicatesAnd.add(criteriaBuilder.equal(usuarioRoot.get(Atributos.ESTADO), UsuarioEstado.ACTIVO));
        }
        if (!predicatesOr.isEmpty()) {
            predicatesAnd.add(criteriaBuilder.or(predicatesOr.toArray(new Predicate[]{})));
        }
        if (filtroNombreCorreo.getMinPorcentaje() > -1) {
            predicatesAnd.add(criteriaBuilder.greaterThanOrEqualTo(usuarioRoot.get(Atributos.BARRA_PROGRESO), (float) filtroNombreCorreo.getMinPorcentaje() / 100));
        }
        if (filtroNombreCorreo.getMaxPorcentaje() > -1) {
            predicatesAnd.add(criteriaBuilder.lessThanOrEqualTo(usuarioRoot.get(Atributos.BARRA_PROGRESO), (float) filtroNombreCorreo.getMaxPorcentaje() / 100));
        }
        if (filtroNombreCorreo.getNombreOrCorreo() != null && !filtroNombreCorreo.getNombreOrCorreo().isBlank()) {
            predicatesAnd.add(
                    (usuarioRoot.get(Atributos.ID).in(subConsulta))
            );

        }

        TypedQuery<UsuarioEntity> resultQuery = entityManager.createQuery(
                        criteriaQuery
                                .select(usuarioRoot)
                                .where(
                                        predicatesAnd.toArray(new Predicate[]{})
                                )
                                .orderBy(criteriaBuilder.asc(usuarioRoot.get(Atributos.NOMBRES)))
                )
                .setFirstResult(filtroNombreCorreo.getNumeroPagina() * filtroNombreCorreo.getTotalElementos())
                .setMaxResults(filtroNombreCorreo.getTotalElementos());
        Long count = entityManager.createQuery(
                criteriaQueryCount
                        .select(
                                criteriaBuilder.count(criteriaQueryCount.from(UsuarioEntity.class))
                        )
                        .where(predicatesAnd.toArray(new Predicate[]{}))).getSingleResult();
        List<UsuarioEntity> usuarios = resultQuery.getResultList();
        numeroPaginas = (long) Math.ceil((double) count / filtroNombreCorreo.getTotalElementos());
        boolean ultimaPagina = numeroPaginas == (filtroNombreCorreo.getNumeroPagina() - 1);
        return new ObjetoRespuestaUsuarioPragmaDex(usuarios.stream(),
                count,
                ultimaPagina,
                usuarios.size()
        );
    }

    @Override
    public ObjetoRespuestaUsuario obtenerInformacionTodosUsuarios(FiltroUsuario filtroUsuario) {
        CriteriaBuilder constructorCriteria = entityManager.getCriteriaBuilder();
        CriteriaQuery consultaCriteria = constructorCriteria.createQuery();
        From entidadSeleccionada = consultaCriteria.from(UsuarioEntity.class);
        Join <UsuarioEntity, TipoContratoEntity> join = entidadSeleccionada.join("tipoContrato", JoinType.INNER);

        aplicarWhere(consultaCriteria, entidadSeleccionada,constructorCriteria, filtroUsuario);
        aplicarOrden(consultaCriteria, entidadSeleccionada, constructorCriteria, filtroUsuario);

        consultaCriteria.multiselect(entidadSeleccionada.get(Atributos.NOMBRES),
                entidadSeleccionada.get(Atributos.APELLIDOS),
                entidadSeleccionada.get(Atributos.ID_TIPODOCUMENTO),
                entidadSeleccionada.get(Atributos.IDENTIFICACION),
                entidadSeleccionada.get(Atributos.ID),
                entidadSeleccionada.get(Atributos.CORREO_EMPRESARIAL),
                entidadSeleccionada.get(Atributos.FECHA_INICIO_CONTRATO),
                join.get(TipoContratoEntity.ATRIBUTOS.DESCRIPCION),
                join.get(TipoContratoEntity.ATRIBUTOS.VACACIONES)
        );

        TypedQuery<UsuarioEntity> resultadoConsulta = entityManager.createQuery(consultaCriteria);

        aplicarCantidad(resultadoConsulta, filtroUsuario.getNumeroPagina(), filtroUsuario.getTotalElementos());

        List<UsuarioEntity> usuariosObject = resultadoConsulta.getResultList();
        List<UsuarioInfo> usuarios= new ArrayList<UsuarioInfo>();
        Iterator it = usuariosObject.listIterator();
        while(it.hasNext()){
            Object[] fields = (Object[]) it.next();
            Long tipoDocumento = Long.valueOf(0);
            if( fields[2] != null ){ tipoDocumento = (Long)fields[2]; }
            UsuarioInfo temporal = new UsuarioInfo((String)fields[0], (String)fields[1], tipoDocumento, (String)fields[3],(String) fields[4],(String) fields[5], (LocalDate) fields[6], (String)fields[7], (Boolean)fields[8]);
            usuarios.add(temporal);
        }
        TypedQuery<Long> resultQueryCantidad = entityManager.createQuery( consultaCriteria.select(constructorCriteria.count(entidadSeleccionada)) );

        double numeroPaginasDividendo = 0;

        if (filtroUsuario.getTotalElementos() == null) {
            numeroPaginasDividendo = 1;
        } else {
            numeroPaginasDividendo = filtroUsuario.getTotalElementos();
        }
        long numeropaginas = (long) Math.ceil((double) resultQueryCantidad.getSingleResult() / numeroPaginasDividendo);

        boolean ultimaPagina = false;
        if (filtroUsuario.getNumeroPagina() != null) {
            ultimaPagina = filtroUsuario.getNumeroPagina() == (numeropaginas - 1);
        }
        return new ObjetoRespuestaUsuario(usuarios,
                numeropaginas,
                ultimaPagina,
                usuarios.size());
    }

}