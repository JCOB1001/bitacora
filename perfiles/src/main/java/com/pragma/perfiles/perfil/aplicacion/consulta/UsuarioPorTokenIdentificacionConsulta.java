package com.pragma.perfiles.perfil.aplicacion.consulta;

import lombok.Data;

@Data
public class UsuarioPorTokenIdentificacionConsulta {

    private String id;
    private String nombres;
    private String apellidos;
    private String correoEmpresarial;
    private Long idTipoDocumento;
    private String identificacion;

}
