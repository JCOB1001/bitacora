package com.pragma.perfiles.perfil.infraestructura.clientefeign;

import com.pragma.perfiles.perfil.infraestructura.clientefeign.dto.EliminarArchivoDto;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.dto.FotografiaDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "FeignFotografias", url = "${feign.modulo.fotografia}", configuration = FeignClientConfiguration.class)
public interface FeignFotografias {

    @PostMapping("uploadFile")
    ObjetoFeignRespuesta guardarArchivo(@RequestBody FotografiaDTO fotografia);

    @DeleteMapping("deleteFile")
    ObjetoFeignRespuestaEliminar eliminarArchivo(@RequestBody EliminarArchivoDto eliminarArchivoDto);
}

