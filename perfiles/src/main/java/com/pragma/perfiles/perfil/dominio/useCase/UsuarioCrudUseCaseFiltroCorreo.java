package com.pragma.perfiles.perfil.dominio.useCase;

import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuarioCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorioFiltroCorreo;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommonFiltro;

public class UsuarioCrudUseCaseFiltroCorreo extends CrudUseCaseCommonFiltro<UsuarioRepositorioFiltroCorreo, String, Usuario, FiltroUsuarioCorreo, ObjetoRespuestaUsuario> {

    public UsuarioCrudUseCaseFiltroCorreo(UsuarioRepositorioFiltroCorreo usuarioRepositorioFiltroCorreo) {
        super(usuarioRepositorioFiltroCorreo);
    }

}
