package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioGoogleId;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
public class EndpointObtenerUsuarioGoogleId {

    private final ManejadorObtenerUsuarioGoogleId manejadorObtenerUsuarioGoogleId;

    @GetMapping("/google-id/{googleId}")
    public ObjetoRespuesta<Usuario> obtenerUsuarioPorGoogleId(@PathVariable String googleId){
        return manejadorObtenerUsuarioGoogleId.obtenerUsuarioGoogleId(googleId);
    }
}
