package com.pragma.perfiles.perfil.infraestructura.persistencia.builder.impl;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.UsuarioMapperUsuarioEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaUsuarioMapperImpl {
    private final UsuarioMapperUsuarioEntity usuarioMapperUsuarioEntity;

    public Usuario usuarioEntityAUsuario(UsuarioEntity usuarioEntity){
        return usuarioMapperUsuarioEntity.rightToLeft(usuarioEntity);
    }
}
