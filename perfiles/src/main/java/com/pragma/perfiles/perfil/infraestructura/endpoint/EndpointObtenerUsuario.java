package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.seguridad.jwt.UsuarioContextoComando;
import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuarioFiltroCorreoComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioPorTokenConsulta;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorTokenCorreo;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
//@PreAuthorize("hasAuthority('Admin')")
public class EndpointObtenerUsuario {

    private final ManejadorObtenerUsuarioPorTokenCorreo manejadorObtenerUsuarioPorTokenCorreo;

    @GetMapping("/token")
    public ObjetoRespuesta<UsuarioPorTokenConsulta> ejecutarObtenerUsuario() {

        UsuarioContextoComando usuarioDatos = (UsuarioContextoComando) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ObtenerUsuarioFiltroCorreoComando obtenerUsuarioFiltroCorreoComando = new ObtenerUsuarioFiltroCorreoComando(usuarioDatos.getCorreo());

        return manejadorObtenerUsuarioPorTokenCorreo.ejecutar(obtenerUsuarioFiltroCorreoComando);
    }

}
