package com.pragma.perfiles.perfil.infraestructura.persistencia.builder;

import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioDetalleEntity;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioDetalle;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioDetalleEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface UsuarioDetalleMapperUsuarioDetalleEntity extends ConvertidorBase<UsuarioDetalle, UsuarioDetalleEntity> {

    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "idUsuario", target = "usuario.id"),
            @Mapping(source = "idIdioma", target = "idioma.id")
    })
    UsuarioDetalleEntity leftToRight(UsuarioDetalle usuarioDetalle);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "usuario.id", target = "idUsuario"),
            @Mapping(source = "idioma.id", target = "idIdioma")
    })
    UsuarioDetalle rightToLeft(UsuarioDetalleEntity usuarioDetalleEntity);
}
