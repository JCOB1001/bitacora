package com.pragma.perfiles.perfil.infraestructura.endpoint;


import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuariosPorVicepresidencia;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Stream;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerUsuariosPorViceprecidencia {

    private final ManejadorObtenerUsuariosPorVicepresidencia manejadorObtenerUsuariosPorVicepresidencia;

    @GetMapping("/vicepresidencia/{idVicepresidencia}")
    public ObjetoRespuesta<List<Usuario>> ejecutarObtenerUsuarioPorIdVicepresidencia(@NotNull @PathVariable("idVicepresidencia") String idVicepresidencia) {

        return manejadorObtenerUsuariosPorVicepresidencia.ejecutar(idVicepresidencia);
    }
}
