package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesTipoContrato;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.HistoricoDeCambiosCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.TipoCambioCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.informacion_nomina.aplicacion.comando.ActualizarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarContratoUsuarioComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.dao.TipoContratoDao;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ManejadorActualizarContratoActual {

    private final FeignTipoDocumento feignTipoDocumento;
    private final InformacionNominaCrudUseCase informacionNominaCrudUseCase;
    private final HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final TipoContratoDao tipoContratoDao;
    private final TipoCambioCrudUseCase tipoCambioCrudUseCase;

    public void ejecutar(ActualizarContratoUsuarioComando actualizarContratoUsuarioComando) {

        String idTipoCambio = tipoCambioCrudUseCase.findByNombreCambio(actualizarContratoUsuarioComando.getTipoCambio()).getId(); //CAMBIO DE CONTRATO
        String cambio = "";

        Long idTipoDocumento = feignTipoDocumento.obtenerIdTipoDocumentoPorCoincidenciaNombre(actualizarContratoUsuarioComando.getTipoIdentificacion());

        if (idTipoDocumento == null) {
            throw new UsuarioNoEncontrado("El usuario con el tipo de ID: " + actualizarContratoUsuarioComando.getTipoIdentificacion() + " no existe");
        }
        InformacionNomina informacionNomina = new InformacionNomina();
        Usuario usuario = usuarioCrudUseCase.buscarPorIdentificacion(idTipoDocumento, actualizarContratoUsuarioComando.getIdentificacion());
        List<InformacionNomina> byUsuarioId = informacionNominaCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList());
        if (byUsuarioId.size() > 0) {
            informacionNomina = byUsuarioId.get(0);
        }

        if (usuario.getId() == null || usuario.getEstado() == UsuarioEstado.INACTIVO) {
            throw new UsuarioNoEncontrado("El usuario con el tipo de ID: " + idTipoDocumento
                    + " y el número de ID: " + actualizarContratoUsuarioComando.getIdentificacion() + " no existe");
        }

        switch (actualizarContratoUsuarioComando.getTipoCambio()){
            case "CONTRATO": {
                String idtipoContrato = idtipoContrato = tipoContratoDao.obtenerIdTipoContratoPorCoincidenciaNombre(actualizarContratoUsuarioComando.getCambio());
                if (idtipoContrato == null || idtipoContrato.isEmpty()) {
                    throw new ExcepcionesTipoContrato("El contrato no es válido. El tipo de contrato " + actualizarContratoUsuarioComando.getCambio() + " no existe.");
                }
                cambio = usuario.getTipoContratoId();
                usuario.setTipoContratoId(idtipoContrato);
                break;
            }
            case "SALARIO ORDINARIO": {
                cambio = informacionNomina.getSalarioBasicoOrdinario().toString();
                if (!actualizarContratoUsuarioComando.getCambio().isBlank() && actualizarContratoUsuarioComando.getCambio().chars().allMatch(Character::isDigit)){
                    informacionNomina.setSalarioBasicoOrdinario(Double.valueOf(actualizarContratoUsuarioComando.getCambio()));
                }else {
                    informacionNomina.setSalarioBasicoOrdinario(0.0);
                }
                break;
            }
            case "FECHA INICIO DE CONTRATO": {
                cambio = usuario.getFechaInicioContrato().toString();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                usuario.setFechaInicioContrato(LocalDate.parse(actualizarContratoUsuarioComando.getCambio(), formatter));
                break;
            }
            case "NOMBRE": {
                cambio = usuario.getNombres();
                usuario.setNombres(actualizarContratoUsuarioComando.getCambio());
                break;
            }
            case "APELLIDO": {
                cambio = usuario.getApellidos();
                usuario.setApellidos(actualizarContratoUsuarioComando.getCambio());
                break;
            }
            case "CORREO": {
                cambio = usuario.getCorreoEmpresarial();
                usuario.setCorreoEmpresarial(actualizarContratoUsuarioComando.getCambio());
                break;
            }
            case "MONEDA": {
                cambio = informacionNomina.getMoneda();
                informacionNomina.setMoneda(actualizarContratoUsuarioComando.getCambio());
                break;
            }
            case "CARGO": {
                cambio = usuario.getCargo();
                usuario.setCargo(actualizarContratoUsuarioComando.getCambio());
                informacionNomina.setCargoNomina(actualizarContratoUsuarioComando.getCambio());
                break;
            }
            case "SALARIO INTEGRAL": {
                cambio = informacionNomina.getSalarioBasicoIntegral().toString();
                if (!actualizarContratoUsuarioComando.getCambio().isBlank() && actualizarContratoUsuarioComando.getCambio().chars().allMatch(Character::isDigit)) {
                    informacionNomina.setSalarioBasicoIntegral(Double.valueOf(actualizarContratoUsuarioComando.getCambio()));
                }else {
                    informacionNomina.setSalarioBasicoIntegral(0.0);
                }
                break;
            }
            case "SALARIO NO CONSTITUTIVO": {
                cambio = informacionNomina.getNoConstitutivosSalario().toString();
                if (!actualizarContratoUsuarioComando.getCambio().isBlank() && actualizarContratoUsuarioComando.getCambio().chars().allMatch(Character::isDigit)){
                    informacionNomina.setNoConstitutivosSalario(Double.valueOf(actualizarContratoUsuarioComando.getCambio()));
                }else {
                    informacionNomina.setNoConstitutivosSalario(0.0);
                }
                break;
            }
        }

        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.guardar(usuario);

        informacionNomina.setIdUsuario(usuario.getId());
        informacionNominaCrudUseCase.actualizar(informacionNomina);

        HistoricoDeCambios nuevo = new HistoricoDeCambios(usuario.getId(), idTipoCambio, LocalDate.now(), cambio);
        historicoDeCambiosCrudUseCase.guardar(nuevo);
    }

}
