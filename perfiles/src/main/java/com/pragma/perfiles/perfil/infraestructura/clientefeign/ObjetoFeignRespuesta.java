package com.pragma.perfiles.perfil.infraestructura.clientefeign;

import com.pragma.perfiles.perfil.infraestructura.clientefeign.dto.File;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ObjetoFeignRespuesta {
    private String message;
    private File file;
}

