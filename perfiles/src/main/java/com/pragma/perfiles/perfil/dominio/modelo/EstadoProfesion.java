package com.pragma.perfiles.perfil.dominio.modelo;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum EstadoProfesion {
    ESTUDIANDO("ESTUDIANDO"),ABANDONO("ABANDONO"),GRADUADO("GRADUADO");

    private String estado;

}
