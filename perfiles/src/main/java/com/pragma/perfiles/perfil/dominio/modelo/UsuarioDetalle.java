package com.pragma.perfiles.perfil.dominio.modelo;

import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioDetalle extends Modelo<String> {

    private String perfilProfesional;
    private String conocimientosTecnicos;
    private String cargo;
    private String idIdioma;
    private String idUsuario;
    private String profesion;


    public UsuarioDetalle(String id,
                          String conocimientosTecnicos,
                          String cargo,
                          String perfilProfesional,
                          String idIdioma,
                          String idUsuario,
                          String profesion) {
        super(id);
        this.conocimientosTecnicos = conocimientosTecnicos;
        this.cargo = cargo;
        this.idIdioma = idIdioma;
        this.idUsuario = idUsuario;
        this.perfilProfesional = perfilProfesional;
        this.profesion = profesion;

    }
}
