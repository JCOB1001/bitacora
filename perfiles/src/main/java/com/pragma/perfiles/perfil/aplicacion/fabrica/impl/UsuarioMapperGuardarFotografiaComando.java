package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.perfil.aplicacion.comando.GuardarFotografiaUsuarioComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface UsuarioMapperGuardarFotografiaComando extends ConvertidorBase<Usuario, GuardarFotografiaUsuarioComando> {


    @Named("rightToLeft")
    @Override
    @Mappings(
            @Mapping(source = "userId", target = "id")
    )
    Usuario rightToLeft(GuardarFotografiaUsuarioComando guardarFotografiaUsuarioComando);

    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "id", target = "userId")
    )
    GuardarFotografiaUsuarioComando leftToRight(Usuario usuario);



}
