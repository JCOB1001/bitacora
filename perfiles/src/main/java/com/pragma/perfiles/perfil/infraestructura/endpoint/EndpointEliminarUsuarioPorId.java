package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorEliminarUsuarioPorId;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
//@PreAuthorize("hasAuthority('Admin')")
public class EndpointEliminarUsuarioPorId {

    private final ManejadorEliminarUsuarioPorId manejadorEliminarUsuarioPorId;

    @DeleteMapping("/{idUsuario}")
    public RespuestaBase ejecutarEliminarUsuarioPorId(@NotNull @PathVariable String idUsuario) {
        manejadorEliminarUsuarioPorId.ejecutar(idUsuario);
        return RespuestaBase.ok("Eliminacion exitosa");
    }

}
