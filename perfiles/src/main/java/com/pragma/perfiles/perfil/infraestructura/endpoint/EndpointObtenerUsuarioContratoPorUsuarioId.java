package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioContratoPorUsuarioId;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioContrato;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerUsuarioContratoPorUsuarioId {

    private final ManejadorObtenerUsuarioContratoPorUsuarioId manejadorObtenerUsuarioContratoPorUsuarioId;

   @GetMapping("/contrato")
    public ObjetoRespuesta<UsuarioContrato> obtenerUsuariosContratoPorUsuarioId(@RequestParam("usuarioId") String usuarioId){
        ObjetoRespuesta<UsuarioContrato> ObjetoRespuesta = manejadorObtenerUsuarioContratoPorUsuarioId.ejecutar(usuarioId);
        return new ObjetoRespuesta<UsuarioContrato>(HttpStatus.OK,ObjetoRespuesta.getDato());
   }
}
