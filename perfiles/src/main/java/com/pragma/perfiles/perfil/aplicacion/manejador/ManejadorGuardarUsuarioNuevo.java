package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.perfil.aplicacion.comando.GuardarUsuarioNuevoComando;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorGuardarUsuarioNuevo {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaUsuario fabricaUsuario;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public ObjetoRespuesta<Usuario> ejecutar(GuardarUsuarioNuevoComando guardarUsuarioNuevoComando) {
        Usuario usuario = fabricaUsuario.guardarUsuarioNuevo(guardarUsuarioNuevoComando);
        usuario.actualizarFechaUltimaActualizacion(LocalDate.now());

        usuario = usuarioCrudUseCase.guardar(usuario);

        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuarioCrudUseCase.actualizar(usuario);

        return new ObjetoRespuesta<Usuario>(usuario);
    }
}
