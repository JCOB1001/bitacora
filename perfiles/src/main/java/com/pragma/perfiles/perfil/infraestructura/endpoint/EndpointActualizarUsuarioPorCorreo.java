package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioPorCorreo;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarUsuarioPorCorreo {

    private final ManejadorActualizarUsuarioPorCorreo manejadorActualizarUsuarioPorCorreo;

    @PutMapping("/correo/{correo}")
    public RespuestaBase actualizarUsuarioPorCorreo(
            @NotNull @PathVariable String correo,
            @NotNull @RequestBody ActualizarUsuarioComando actualizarUsuarioComando) {
        manejadorActualizarUsuarioPorCorreo.ejecutar(correo,actualizarUsuarioComando);
        return RespuestaBase.ok(" Success ");
    }

}
