package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionConocimientoArchivo;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioMapperInformacionConocimientoArchivo extends ConvertidorBase<UsuarioEntity, InformacionConocimientoArchivo> {
}
