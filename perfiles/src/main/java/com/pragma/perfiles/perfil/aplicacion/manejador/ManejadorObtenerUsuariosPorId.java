package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ManejadorObtenerUsuariosPorId {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public List<Usuario> ejecutar(List<String> idUsuarios) {
        List<Usuario> usuarios = new ArrayList<>();
        for (String idUsuario : idUsuarios) {
            Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
            if (usuario != null)
                usuarios.add(usuario);
        }
        return usuarios;
    }

}
