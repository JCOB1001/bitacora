package com.pragma.perfiles.perfil.infraestructura.persistencia.builder;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface ComandoMapperEntity {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void convertirComandoEnEntidad(Usuario usuario, @MappingTarget UsuarioEntity usuarioEntity);

}
