package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioGeneralComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioGeneralPorId;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarUsuarioGeneral {

    private final ManejadorActualizarUsuarioGeneralPorId manejadorActualizarUsuarioGeneralPorId;

    @PatchMapping("/{idUsuario}")
    public RespuestaBase actualizarUsuarioGeneral(
            @NotNull @PathVariable String idUsuario,
            @NotNull @RequestBody ActualizarUsuarioGeneralComando actualizarUsuarioGeneralComando  ){
        manejadorActualizarUsuarioGeneralPorId.ejecutar(idUsuario,actualizarUsuarioGeneralComando);
        return RespuestaBase.ok("OK");
    }
}
