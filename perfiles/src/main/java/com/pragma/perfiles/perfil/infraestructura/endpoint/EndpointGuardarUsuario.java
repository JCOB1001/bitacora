package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import com.pragma.perfiles.perfil.aplicacion.comando.GuardarUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorGuardarUsuario;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarUsuario {

    private final ManejadorGuardarUsuario manejadorGuardarUsuario;

    @PostMapping
    public RespuestaBase ejecutarGuardarUsuario(@NotNull @RequestBody GuardarUsuarioComando guardarUsuarioComando) {

        manejadorGuardarUsuario.ejecutar(guardarUsuarioComando);
        return RespuestaBase.ok("Guardado Exitoso");
    }

}
