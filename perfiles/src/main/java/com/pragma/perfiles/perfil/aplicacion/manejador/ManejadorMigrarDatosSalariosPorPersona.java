package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.contrato_historico.dominio.UseCase.ContratoHistoricoCrudUseCase;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioNuevoPragmaticoComando;
import com.pragma.perfiles.perfil.aplicacion.fabrica.CopyProperties;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.*;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import com.pragma.perfiles.perfil.infraestructura.configuracion.InformacionArchivoNuevoPragmaticoPropiedades;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.UsuarioMapperUsuarioEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.tipo_contrato.aplicacion.manejador.ManejadorObtenerTipoContratoPorCoincidenciaNombre;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ManejadorMigrarDatosSalariosPorPersona {
    private final FeignTipoDocumento feignTipoDocumento;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaUsuario fabricaUsuario;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;
    private final InformacionNominaCrudUseCase informacionNominaCrudUseCase;
    private final InformacionLaboralCrudUseCase informacionLaboralCrudUseCase;
    private final InformacionArchivoNuevoPragmaticoPropiedades informacionArchivoNuevoPragmaticoPropiedades;
    private final UsuarioMapperUsuarioEntity usuarioMapperUsuarioEntity;
    private final ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase;
    private final ManejadorObtenerTipoContratoPorCoincidenciaNombre manejadorObtenerTipoContratoPorCoincidenciaNombre;
    private final CopyProperties copiaPropiedades;

    public void ejecutar() throws InvocationTargetException, IllegalAccessException {

        List<UsuarioEntity> usuarioExistente = usuarioCrudUseCase.findByEstado(UsuarioEstado.ACTIVO);
        List<Usuario> usuarios = usuarioMapperUsuarioEntity.rightToLeft(usuarioExistente).collect(Collectors.toList());

        for (Usuario users : usuarios) {
            try {
                ActualizarUsuarioNuevoPragmaticoComando datos = this.consultarServicioArchivoNuevoPragmatico(users.getCorreoEmpresarial()).getBody();
                Usuario usuarioTemp = fabricaUsuario.guardarUsuarioNuevoPragmaticoComando(datos);
                usuarioTemp.setId(users.getId());
                usuarioTemp.setCorreoEmpresarial(users.getCorreoEmpresarial());
                usuarioTemp = GuardarDatosConsultasServicios(usuarioTemp, datos);
                copiaPropiedades.copyProperties(users, usuarioTemp);
                usuarioCrudUseCase.actualizar(users);
                GuardadoInformacionLaboral(users, datos);
                GuardarInformacionNomina(users, datos);
                guardarInfoContratoHistorico(users, datos);
            } catch (Exception e) {
                continue;
            }


        }

    }

    private Usuario GuardarDatosConsultasServicios(Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando datos) {
        if (usuario.getIdLugarRecidencia() == null) {
            Long id_lugar_residencia = feignTipoDocumento.obtenerIdCiudadPorCoincidenciaNombre(datos.getCiudad(), datos.getDepartamento(), datos.getPais());
            usuario.setIdLugarRecidencia(id_lugar_residencia.toString());
            usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        }

        if (usuario.getIdTipoDocumento() == null) {
            Long idTipoDocumento = feignTipoDocumento.obtenerIdTipoDocumentoPorCoincidenciaNombre(datos.getTipoIdentificacion());
            usuario.setIdTipoDocumento(idTipoDocumento);
            usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        }

        return usuario;
    }

    private void GuardarInformacionNomina(Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando datos) {
        InformacionNomina informacionNomina = new InformacionNomina();

        informacionNomina = informacionNominaCrudUseCase.findByUsuarioId(usuario.getId()).findAny().orElse(null);

        if (informacionNomina == null) {
            informacionNomina = fabricaUsuario.guardarInformacionNominaNuevoPragmatico(datos);
            informacionNomina.setIdUsuario(usuario.getId());
            informacionNominaCrudUseCase.guardar(informacionNomina);
        }


    }

    private void GuardadoInformacionLaboral(Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando datos) {
        InformacionLaboral informacionLaboral = new InformacionLaboral();

        informacionLaboral = informacionLaboralCrudUseCase.findByUsuarioId(usuario.getId()).findAny().orElse(null);

        if (informacionLaboral == null) {
            informacionLaboral = fabricaUsuario.guardarInformacionLaboralNuevoPragmatico(datos);
            informacionLaboral.setUsuarioId(usuario.getId());
            informacionLaboralCrudUseCase.guardar(informacionLaboral);
        }

    }

    private void guardarInfoContratoHistorico(Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando datos) {

        ContratoHistorico contratoHistorico;
        contratoHistorico = contratoHistoricoCrudUseCase.findByUsuarioId(usuario.getId()).findAny().orElse(null);

        if (contratoHistorico == null) {
            contratoHistorico = new ContratoHistorico();
            contratoHistorico.setIdUsuario(usuario.getId());
            contratoHistorico.setFechaInicio(LocalDate.parse(datos.getFechaInicioContratoActual()));
            contratoHistorico.setIdTipoContrato(manejadorObtenerTipoContratoPorCoincidenciaNombre.ejecutar(datos.getTipoContrato()).getDato());
            contratoHistorico.setEstado(ContratoHistoricoEstado.ACTIVO);
            contratoHistoricoCrudUseCase.guardar(contratoHistorico);
        }


    }

    private ResponseEntity<ActualizarUsuarioNuevoPragmaticoComando> consultarServicioArchivoNuevoPragmatico(String correo) {

        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(informacionArchivoNuevoPragmaticoPropiedades.getUrl());
        builder.queryParam("email", correo);
        String uri = builder.build().encode().toUriString();
        ResponseEntity<ActualizarUsuarioNuevoPragmaticoComando> response
                = restTemplate.getForEntity(uri, ActualizarUsuarioNuevoPragmaticoComando.class);

        if (response != null)
            return response;
        else
            return null;
    }

    private Usuario defaultValues(Usuario usuario) {
        usuario.setGrupoSanguineo(UsuarioGrupoSanguineo.NO_ESPECIFICA);
        usuario.setTallaCamisa(UsuarioTallaCamisa.NO_ESPECIFICADO);
        usuario.setEstadoCivil(UsuarioEstadoCivil.NO_ESPECIFICADO);
        return usuario;
    }

}
