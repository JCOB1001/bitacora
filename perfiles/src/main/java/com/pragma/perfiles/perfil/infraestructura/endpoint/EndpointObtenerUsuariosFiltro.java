package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuariosFiltroComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuariosFiltro;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
//@PreAuthorize("hasAuthority('Admin')")
public class EndpointObtenerUsuariosFiltro {

    private final ManejadorObtenerUsuariosFiltro manejadorObtenerUsuariosFiltro;

    @GetMapping()
    public ObjetoRespuestaUsuario ejecutarObtenerUsuarios(@NotNull ObtenerUsuariosFiltroComando obtenerUsuariosFiltroComando) {
        ObjetoRespuestaUsuario usuarios = manejadorObtenerUsuariosFiltro.ejecutar(obtenerUsuariosFiltroComando);
        return usuarios;
    }

}
