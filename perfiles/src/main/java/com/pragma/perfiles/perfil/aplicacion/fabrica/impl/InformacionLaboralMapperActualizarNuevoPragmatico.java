package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioNuevoPragmaticoComando;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.*;

import static com.pragma.perfiles.informacion_laboral.dominio.modelo.EstadoPeriodoPrueba.fromValueEstadoPeridoPrueba;

@Mapper(componentModel = "spring")
public interface InformacionLaboralMapperActualizarNuevoPragmatico extends ConvertidorBase<InformacionLaboral, ActualizarUsuarioNuevoPragmaticoComando> {

    @Named("rightToLeft")
    @Mapping(target = "estadoPeriodoPrueba", ignore = true)
    InformacionLaboral rightToLeft(ActualizarUsuarioNuevoPragmaticoComando actualizarUsuarioNuevoPragmaticoComando);

    @AfterMapping
    default void setSalarios(@MappingTarget InformacionLaboral informacionLaboral, ActualizarUsuarioNuevoPragmaticoComando actualizarUsuarioNuevoPragmaticoComando) {
        informacionLaboral.setEstadoPeriodoPrueba(fromValueEstadoPeridoPrueba(actualizarUsuarioNuevoPragmaticoComando.getEstadoPeriodoPrueba()));
    }
}
