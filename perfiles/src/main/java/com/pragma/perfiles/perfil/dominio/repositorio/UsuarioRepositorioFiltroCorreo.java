package com.pragma.perfiles.perfil.dominio.repositorio;

import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuarioCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBaseFiltro;

public interface UsuarioRepositorioFiltroCorreo extends CrudRepositorioBaseFiltro<String, Usuario, FiltroUsuarioCorreo, ObjetoRespuestaUsuario> {
}
