package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioGoogleId {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Usuario> obtenerUsuarioGoogleId(String googleId){

        Usuario usuario=usuarioCrudUseCase.findbyGoogleId(googleId);

        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario no se encuentra en registrado");
        }

        return new ObjetoRespuesta<>(usuario);
    }
}
