package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.comun.infraestructura.seguridad.jwt.UsuarioContextoComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UsuarioMapperUsuarioContextoComando extends ConvertidorBase<UsuarioContextoComando, Usuario> {

    UsuarioContextoComando INSTANCE = Mappers.getMapper(UsuarioContextoComando.class);

    @Mapping(source = "correo", target = "correoEmpresarial")
    @Mapping(source = "id", target = "googleId")
    @Mapping(target = "id", ignore = true)
    Usuario usuarioContextoToUsuario(UsuarioContextoComando usuarioContextoComando);

}
