package com.pragma.perfiles.perfil.infraestructura.persistencia.repositorio;

import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseFiltroImpl;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorioFiltro;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.UsuarioMapperUsuarioEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.dao.UsuarioDao;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class RepositorioUsuarioFiltroImpl
        extends CrudRepositorioBaseFiltroImpl<String, Usuario, UsuarioEntity, FiltroUsuario, ObjetoRespuestaUsuario>
        implements UsuarioRepositorioFiltro {

    private final UsuarioDao usuarioDao;
    private final UsuarioMapperUsuarioEntity usuarioMapperUsuarioEntity;

    @Override
    protected UsuarioDao obtenerRepositorio() {
        return usuarioDao;
    }

    @Override
    protected UsuarioMapperUsuarioEntity obtenerConversionBase() {
        return usuarioMapperUsuarioEntity;
    }

    @Override
    protected ObjetoRespuestaUsuario buscarPorFiltro(FiltroUsuario filtro) {
        return usuarioDao.buscarTodosFiltrando(filtro);
    }

}
