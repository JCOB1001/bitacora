package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuarioFiltroCorreoComando;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuarioCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCaseFiltroCorreo;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorAgregarDiasTomadosUsuario {

    private final UsuarioCrudUseCaseFiltroCorreo usuarioCrudUseCaseFiltroCorreo;
    private final FabricaUsuario fabricaUsuario;

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Usuario> ejecutar(String correo, Integer diasTomados) {
        ObtenerUsuarioFiltroCorreoComando obtenerUsuarioFiltroCorreoComando = new ObtenerUsuarioFiltroCorreoComando();
        obtenerUsuarioFiltroCorreoComando.setCorreo(correo);
        FiltroUsuarioCorreo filtroUsuarioCorreo = fabricaUsuario.cargarFiltroUsuarioCorreo(obtenerUsuarioFiltroCorreoComando);
        ObjetoRespuestaUsuario objetoRespuestaUsuario = usuarioCrudUseCaseFiltroCorreo.obtenerTodos(filtroUsuarioCorreo);
        if (objetoRespuestaUsuario.getDato() == null) {
            throw new UsuarioNoEncontrado("No se encontró usuario");
        }
        Stream<Usuario> streamUsuarios = (Stream<Usuario>)objetoRespuestaUsuario.getDato();
        Optional<Usuario> optionalUsuario = streamUsuarios.findFirst();
        if (optionalUsuario.isEmpty()) {
            throw new UsuarioNoEncontrado("No se encontró usuario");
        }

        Usuario usuario = optionalUsuario.get();
        //usuario.agregarDiasTomados(diasTomados);
        usuarioCrudUseCase.actualizar(usuario);
        return new ObjetoRespuesta<Usuario>(usuario);
    }

}
