package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.seguridad.jwt.UsuarioContextoComando;
import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuarioFiltroCorreoComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioPorTokenIdentificacionConsulta;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorToken;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerUsuarioPorTokenConIdentificacion {

    private final ManejadorObtenerUsuarioPorToken manejadorObtenerUsuarioPorToken;

    @GetMapping("/token/identificacion")
    public ObjetoRespuesta<UsuarioPorTokenIdentificacionConsulta> ejecutarObtenerUsuarioPorTokenConIdentificacion(){

        UsuarioContextoComando usuarioDatos = (UsuarioContextoComando) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ObtenerUsuarioFiltroCorreoComando obtenerUsuarioFiltroCorreoComando = new ObtenerUsuarioFiltroCorreoComando(usuarioDatos.getCorreo());

        return manejadorObtenerUsuarioPorToken.ejecutar(obtenerUsuarioFiltroCorreoComando);
    }

}
