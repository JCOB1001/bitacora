package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException;
import com.amazonaws.services.cognitoidp.model.AdminAddUserToGroupRequest;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.HistoricoDeCambiosCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioRetiradoComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorActualizarUsuarioRetirado {

    private final FeignTipoDocumento feignTipoDocumento;
    private final HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final AWSCognitoIdentityProvider cognitoClient;

    @Value(value = "${aws.cognito.userPoolId}")
    private  String userPoolId;
    @Value(value = "proceso_retiro")
    private  String userRetirementProgressGroup;

    public void ejecutar(ActualizarUsuarioRetiradoComando actualizarUsuarioRetiradoComando){

        String idTipoCambio = "01";
        String tipoContratoId = "16";
        String detalle = "Termino de contrato";

        Long idTipoDocumento = feignTipoDocumento.obtenerIdTipoDocumentoPorCoincidenciaNombre(actualizarUsuarioRetiradoComando.getTipoIdentificacion());

        if (idTipoDocumento == null) {
            throw new UsuarioNoEncontrado("El usuario con el tipo de ID: " + actualizarUsuarioRetiradoComando.getTipoIdentificacion() + " no existe");
        }

        Usuario usuario = usuarioCrudUseCase.buscarPorIdentificacion(idTipoDocumento, actualizarUsuarioRetiradoComando.getIdentificacion());

        if(usuario == null){
            throw new UsuarioNoEncontrado("El usuario con el tipo de ID: "+ idTipoDocumento
                    +" y el número de ID: "+ actualizarUsuarioRetiradoComando.getIdentificacion()+" no existe");
        }

        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuario.setTipoContratoId(tipoContratoId);
        usuario.setFechaInicioContrato(LocalDate.now());
        usuario.setEstado(UsuarioEstado.INACTIVO);
        usuarioCrudUseCase.guardar(usuario);

        /**try {
            AdminAddUserToGroupRequest addUserToGroupRequest = new AdminAddUserToGroupRequest()
                .withGroupName(userRetirementProgressGroup)
                .withUserPoolId(userPoolId)
                .withUsername("Google_"+usuario.getGoogleId());

            cognitoClient.adminAddUserToGroup(addUserToGroupRequest);
        } catch(AWSCognitoIdentityProviderException e) {
            throw new UsuarioNoEncontrado("El usuario con el tipo de ID: " + actualizarUsuarioRetiradoComando.getTipoIdentificacion() + " no existe");
        }*/

        HistoricoDeCambios nuevo = new HistoricoDeCambios(usuario.getId(), idTipoCambio, LocalDate.now(), detalle);
        historicoDeCambiosCrudUseCase.guardar(nuevo);
    }

}
