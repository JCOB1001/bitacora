package com.pragma.perfiles.perfil.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorio;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuarioPragmaDex;
import com.pragma.perfiles.perfil.infraestructura.clienteaws.AwsCloudfrontClient;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignFotografias;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.ObjetoFeignRespuesta;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.dto.EliminarArchivoDto;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.dto.FotografiaDTO;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.FotografiaMapperFotografiaEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.UsuarioMapperUsuarioEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.impl.FabricaUsuarioMapperImpl;
import com.pragma.perfiles.perfil.infraestructura.persistencia.dao.FotografiaDao;
import com.pragma.perfiles.perfil.infraestructura.persistencia.dao.UsuarioDao;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.FotografiaEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.pragmadata.aplicacion.comando.ConsultaPragmaDataComando;
import com.pragma.perfiles.pragmadata.dominio.modelo.*;
import com.pragma.perfiles.pragmadata.dominio.respuesta.ObjetoRespuestaUsuarioPragmaData;
import com.pragma.perfiles.pragmadata.infraestructura.clientefeign.FeignAdministrarCatalogo;
import com.pragma.perfiles.pragmadex.dominio.filtros.FiltroNombreCorreo;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
@Log4j2
public class RepositorioUsuarioImpl extends CrudRepositorioBaseImpl<String, Usuario, UsuarioEntity> implements UsuarioRepositorio {

    private final UsuarioDao usuarioDao;
    private final UsuarioMapperUsuarioEntity usuarioMapperUsuarioEntity;
    private final FotografiaMapperFotografiaEntity fotografiaMapperFotografiaEntity;
    private final FotografiaDao fotografiaDao;
    private final FeignFotografias fotografiasFeign;
    private final FeignAdministrarCatalogo feignAdministrarCatalogo;
    private final FabricaUsuario fabricaUsuario;
    private final FabricaUsuarioMapperImpl fabricaUsuarioMapper;
    private final AwsCloudfrontClient awsCloudfrontClient;

    private static final String RUTA_CARPETA_FEIGN = "users_avatar"; // TODO cambiar a una variable de configuracion externa

    @Override
    protected UsuarioDao obtenerRepositorio() {
        return usuarioDao;
    }

    @Override
    protected UsuarioMapperUsuarioEntity obtenerConversionBase() {
        return usuarioMapperUsuarioEntity;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void actualizarConFoto(Usuario modelo) {
        if (modelo.getFotografias() != null) {

            if (modelo.getCorreoEmpresarial() == null) {
                throw new UsuarioNoEncontrado("Necesita enviar un correo para poder guardar");
            }

            UsuarioEntity usuarioBusquedaCorreo = usuarioDao.findByCorreoEmpresarial(modelo.getCorreoEmpresarial());
            if (usuarioBusquedaCorreo == null) {
                throw new UsuarioNoEncontrado("El usuario con el correo " + modelo.getCorreoEmpresarial() + " no se encuentra registrado en el sistema");
            }
            if (!usuarioBusquedaCorreo.getFotografias().isEmpty()) {
                fotografiaDao.deleteAll(usuarioBusquedaCorreo.getFotografias());
            }
            List<FotografiaEntity> fotografiaEntityList = obtenerFotografiasParaGuardar(modelo, usuarioBusquedaCorreo);

            // invalida la caché de la imagen actualizada en cloudfront
            awsCloudfrontClient.invalidarCache(
                    fotografiaEntityList
                            .stream()
                            .map(fotografiaEntity -> String.format("/%s", fotografiaEntity.getContenido()))
                            .collect(Collectors.toList()));

            fotografiaEntityList = fotografiaDao.saveAll(fotografiaEntityList);
            usuarioBusquedaCorreo.setFotografias(fotografiaEntityList);
            modelo = usuarioMapperUsuarioEntity.rightToLeft(usuarioBusquedaCorreo);
        }
        super.actualizar(modelo);
    }

    private List<FotografiaEntity> obtenerFotografiasParaGuardar(Usuario modelo, UsuarioEntity busqueda) {
        AtomicInteger contador = new AtomicInteger();
        return fotografiaMapperFotografiaEntity
                .leftToRight(modelo.getFotografias())
                .map(foto -> {
                    foto.setUsuarioEntity(busqueda);
                    foto.setFechaCreacion(LocalDate.now());
                    String nombreArchivo = formatearDatosCorreoTipoFotografia(modelo.getCorreoEmpresarial(), String.valueOf(foto.getTipoFotografia()));
                    FotografiaDTO fotografiaDTO = new FotografiaDTO(
                            foto.getContenido(),
                            nombreArchivo,
                            RUTA_CARPETA_FEIGN
                    );
                    if (busqueda.getFotografias() != null && !busqueda.getFotografias().isEmpty() && contador.get() < busqueda.getFotografias().size()) {
                        eliminarFotografiaS3Feign(busqueda, contador, foto);
                    }
                    ObjetoFeignRespuesta objetoFeignRespuesta = fotografiasFeign.guardarArchivo(fotografiaDTO);
                    foto.setContenido(objetoFeignRespuesta.getFile().getFullPath());
                    return foto;
                })
                .collect(Collectors.toList());
    }

    private String formatearDatosCorreoTipoFotografia(String correo, String tipoFotografia) {
        StringBuilder nombreArchivo = new StringBuilder();
        try {
            String correoFormateado = correo.replace(".", "_");
            nombreArchivo.append(correoFormateado);
            nombreArchivo.append("_");
            nombreArchivo.append(tipoFotografia);
        }
        catch (Exception exception) {
            log.error(exception.getMessage(), exception);
        }

        return nombreArchivo.toString();
    }

    private void eliminarFotografiaS3Feign(UsuarioEntity busqueda, AtomicInteger contador, FotografiaEntity foto) {
        FotografiaEntity fotoEliminar = busqueda.getFotografias().get(contador.get());
        contador.getAndIncrement();
        if (foto.getContenido() != null) {
            EliminarArchivoDto eliminarArchivoDto = new EliminarArchivoDto();
            StringBuilder urlEliminar = new StringBuilder();
            urlEliminar.append(fotoEliminar.getContenido());
            //urlEliminar.delete(urlEliminar.indexOf("."),urlEliminar.length());
            eliminarArchivoDto.setFullPath(urlEliminar.toString());
            try {
                //fotografiasFeign.eliminarArchivo(eliminarArchivoDto);
            }
            catch (Exception e) {
                log.info(e.getMessage(), e);
            }
        }
    }

    @Override
    public Usuario findByIdentificacion(String identificacion) {
        return obtenerConversionBase().rightToLeft(usuarioDao.findByIdentificacion(identificacion));
    }

    @Override
    public Usuario findBycorreoEmpresarial(String correoEmpresarial) {
        return obtenerConversionBase().rightToLeft(usuarioDao.findByCorreoEmpresarial(correoEmpresarial));
    }

    @Override
    public Usuario findByCorreoPersonal(String correoPersonal) {
        UsuarioEntity usuario = usuarioDao.findByCorreoPersonal(correoPersonal);
        return obtenerConversionBase().rightToLeft(usuario);
    }

    @Override
    public Usuario findByGoogleId(String googleId) {
        return obtenerConversionBase().rightToLeft(usuarioDao.findByGoogleId(googleId));
    }


    @Override
    public Usuario findByid(String idUsuario) {
        return obtenerConversionBase().rightToLeft(usuarioDao.findById(idUsuario).get());
    }

    @Override
    public Stream<Usuario> findByIdVicepresidencia(String idViceprecidencia) {
        return obtenerConversionBase().rightToLeft(usuarioDao.findByIdVicepresidencia(idViceprecidencia));
    }

    @Override
    public ObjetoRespuestaUsuario findByNombreOrCorreoEmpresarialPaginated(FiltroNombreCorreo filtroNombreCorreo) {
        ObjetoRespuestaUsuarioPragmaDex respuesta = usuarioDao.buscarUsuarioPorNombreCorreo(filtroNombreCorreo);
        List<UsuarioEntity> usuarioEntities = respuesta.getDato().collect(Collectors.toList());
        return new ObjetoRespuestaUsuario(
                usuarioMapperUsuarioEntity.rightToLeft(usuarioEntities),
                respuesta.getCantidadPaginas(),
                respuesta.isUltimaPagina(),
                respuesta.getTotalElementos()
        );
    }

    @Override
    public ObjetoRespuestaUsuario buscarPorHabilidadPaginado(FiltroNombreCorreo filtroNombreCorreo) {
        ObjetoRespuestaUsuarioPragmaDex respuesta = usuarioDao.buscarUsuariosPorHabilidadPaginado(filtroNombreCorreo);
        List<UsuarioEntity> usuarioEntities = respuesta.getDato().collect(Collectors.toList());
        return new ObjetoRespuestaUsuario(
                usuarioMapperUsuarioEntity.rightToLeft(usuarioEntities),
                respuesta.getCantidadPaginas(),
                respuesta.isUltimaPagina(),
                respuesta.getTotalElementos()
        );
    }

    @Override
    public ObjetoRespuestaUsuario buscarPorEmpresaYNombrePaginado(FiltroNombreCorreo filtroNombreCorreo) {
        ObjetoRespuestaUsuarioPragmaDex respuesta = usuarioDao.buscarUsuariosPorEmpresaYNombrePaginado(filtroNombreCorreo);
        List<UsuarioEntity> usuarioEntities = respuesta.getDato().collect(Collectors.toList());
        return new ObjetoRespuestaUsuario(
                usuarioMapperUsuarioEntity.rightToLeft(usuarioEntities),
                respuesta.getCantidadPaginas(),
                respuesta.isUltimaPagina(),
                respuesta.getTotalElementos()
        );
    }

    @Override
    public ObjetoRespuestaUsuario buscarPorEmpresaYHabilidadPaginado(FiltroNombreCorreo filtroNombreCorreo) {
        ObjetoRespuestaUsuarioPragmaDex respuesta = usuarioDao.buscarUsuariosPorEmpresaYHabilidadPaginado(filtroNombreCorreo);
        List<UsuarioEntity> usuarioEntities = respuesta.getDato().collect(Collectors.toList());
        return new ObjetoRespuestaUsuario(
                usuarioMapperUsuarioEntity.rightToLeft(usuarioEntities),
                respuesta.getCantidadPaginas(),
                respuesta.isUltimaPagina(),
                respuesta.getTotalElementos()
        );
    }

    @Override
    public List<String> buscarDatosPorListaIds(List<String> idUsuario, Class table, FiltroNombreCorreo filtroNombreCorreo) {
        return usuarioDao.buscarDatosPorListaIds(idUsuario, table, filtroNombreCorreo);
    }

    @Override
    public List<Usuario> buscarPorNombreCorreo(String nombre) {
        return usuarioDao.buscarPorNombreCorreo(nombre);
    }


    @Override
    public List<Vicepresidencia> obtenerVicepresidencias() {
        ObjetoRespuesta<List<Vicepresidencia>> vicepresidencias = feignAdministrarCatalogo.ejecutarObtenerVicepresidencias();

        return vicepresidencias.getDato();
    }

    @Override
    public List<TipoDocumento> obtenerTiposDeDocumentos() {
        ObjetoRespuesta<List<TipoDocumento>> tiposDocumentos = feignAdministrarCatalogo.ejecutarObtenerTipoDocumentos();

        return tiposDocumentos.getDato();
    }

    @Override
    public List<Profesion> obtenerProfesionPorListaId(List<String> idProfesion) {
        ObjetoRespuesta<List<Profesion>> profesiones = feignAdministrarCatalogo.ejecutarObtenerProfesionPorListaId(idProfesion);

        return profesiones.getDato();
    }

    @Override
    public List<UbicacionGeograficaCompleta> obtenerUbicacionesGeograficas(List<String> ubicacionesIds) {
        ObjetoRespuesta<List<UbicacionGeograficaCompleta>> ubicaciones = feignAdministrarCatalogo.ejecutarObtenerUbicacionesGeograficas(ubicacionesIds);

        return ubicaciones.getDato();
    }

    @Override
    public List<UbicacionGeograficaBasica> obtenerUbicacionesGeograficasLista(List<String> ubicacionesIds, boolean padre, String nivel) {
        ObjetoRespuesta<List<UbicacionGeograficaBasica>> ubicaciones = feignAdministrarCatalogo.ejecutarObtenerUbicacionesGeograficasLista(ubicacionesIds, padre, nivel);
        return ubicaciones.getDato();
    }

    @Override
    public ObjetoRespuestaUsuario buscarUsuariosPorFiltros(List<ConsultaPragmaDataComando> campos, Integer numeroElementos, Integer numeroPagina) {
        ObjetoRespuestaUsuarioPragmaData respuesta = usuarioDao.buscarUsuariosPorFiltros(campos, numeroElementos, numeroPagina);
        List<UsuarioEntity> usuarioEntities = respuesta.getDato().collect(Collectors.toList());
        return new ObjetoRespuestaUsuario(
                usuarioMapperUsuarioEntity.rightToLeft(usuarioEntities),
                respuesta.getCantidadPaginas(),
                respuesta.isUltimaPagina(),
                respuesta.getTotalElementos()
        );
    }

    @Override
    public Usuario buscarPorIdentificacion(Long idTipoDocumento, String identificacionUsuario) {
        UsuarioEntity usuarioObtenidoEntidad = usuarioDao.buscarPorIdentificacion(idTipoDocumento, identificacionUsuario);
        Usuario usuarioObtenido = fabricaUsuarioMapper.usuarioEntityAUsuario(usuarioObtenidoEntidad);
        return usuarioObtenido;
    }

    @Override
    public List<UsuarioEntity> findByEstado(UsuarioEstado estado) {
        return usuarioDao.findByEstado(UsuarioEstado.ACTIVO);


    }

    @Override
    public Stream<Usuario> findByCorreoEmpresarialIn(List<String> correos) {
        return usuarioMapperUsuarioEntity.rightToLeft(usuarioDao.findByCorreoEmpresarialIn(correos));
    }


    @Override
    public ObjetoRespuestaUsuario obtenerInformacionTodosUsuarios(FiltroUsuario filtro) {
        return usuarioDao.obtenerInformacionTodosUsuarios(filtro);
    }

    @Override
    public Stream<InformacionConocimientoArchivo> findByUsuarioIn(Collection<String> idLista){
        Stream<InformacionConocimientoArchivo> info = fabricaUsuario.usuarioPorInformacionConocimientoArchivo(usuarioDao.findByIdIn(idLista));
        return info;
    }


}
