package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.GuardarUsuarioNuevoComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorGuardarUsuarioNuevo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios/nuevo")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarUsuarioNuevo {

    private final ManejadorGuardarUsuarioNuevo manejadorGuardarUsuarioNuevo;

    @PostMapping
    public ObjetoRespuesta<Usuario> ejecutarGuardarUsuarioNuevo(@NotNull @RequestBody GuardarUsuarioNuevoComando guardarUsuarioNuevoComando) {

        ObjetoRespuesta<Usuario> objetoRespuestaUsuario = manejadorGuardarUsuarioNuevo.ejecutar(guardarUsuarioNuevoComando);
        return new ObjetoRespuesta<Usuario>(HttpStatus.OK,
                objetoRespuestaUsuario.getDato());
    }

}
