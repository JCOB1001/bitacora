package com.pragma.perfiles.perfil.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Fotografia extends Modelo<String> {

    private String contenido;
    private TipoFotografia tipoFotografia;

    public Fotografia() {
        super(null);
    }

    public Fotografia(String id, String contenido, TipoFotografia tipoFotografia) {
        super(id);
        this.contenido = contenido;
        this.tipoFotografia = tipoFotografia;
    }
}

