package com.pragma.perfiles.perfil.aplicacion.comando;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ActualizarContratoUsuarioComando {

    private String identificacion;
    private String tipoIdentificacion;
    private String cambio;
    private String tipoCambio;
}
