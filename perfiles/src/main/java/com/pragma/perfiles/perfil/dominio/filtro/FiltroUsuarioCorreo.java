package com.pragma.perfiles.perfil.dominio.filtro;

import com.pragma.vacaciones.common.filtro.FiltroBase;
import lombok.Getter;

@Getter
public class FiltroUsuarioCorreo extends FiltroBase {

    private String correo;

    public FiltroUsuarioCorreo(Integer numeroPagina,
                               Integer totalElementos,
                               String correo) {
        super(numeroPagina, totalElementos);
        this.correo = correo;
    }

}
