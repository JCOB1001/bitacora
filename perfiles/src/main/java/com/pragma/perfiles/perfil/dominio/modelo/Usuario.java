package com.pragma.perfiles.perfil.dominio.modelo;


import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
public class Usuario extends Modelo<String> {


    private String correoEmpresarial;
    private String identificacion;
    private String nombres;
    private String apellidos;
    private String cliente;
    private LocalDate fechaIngreso;
    private String googleId;
    private String correoPersonal;
    private String telefonoFijo;
    private String telefonoCelular;
    private String idNacionalidad;
    private String idLugarNacimiento;
    private String idLugarRecidencia;
    private LocalDate fechaNacimiento;
    private String direccion;
    private String nombreContactoEmergencia;
    private String parentescoContactoEmergencia;
    private String telefonoContactoEmergencia;
    private String tarjetaProfesional;
    private String perfilProfesional;
    private String conocimientosTecnicos;
    private UsuarioTallaCamisa tallaCamisa;
    private UsuarioEstadoCivil estadoCivil;
    private UsuarioGenero genero;
    private UsuarioGrupoSanguineo grupoSanguineo;
    private UsuarioEstado estado;
    private Long idTipoDocumento;
    private Long idProfesion;
    private LocalDate fechaCreacion;
    private LocalDate fechaUltimaActualizacion;
    private String cargo;
    private String idVicepresidencia;
    private List<Long> capacidad;
    private List<Fotografia> fotografias;
    private boolean aceptacionTerminos;
    private boolean bitacoraPrimeraVez;
    private double barraProgreso;
    private double diasGanados;
    private int diasTomados;
    private double diasDisponibles;
    private String nombrePreferencia;
    private String pasaporte;
    private int estrato;
    private String semestre;
    private EstadoProfesion estadoProfesion;
    private String tipoContratoId;
    private LocalDate fechaInicioContrato;
    private String empresa;
    private String contraseniaComputador;
    private String profesion;
    private boolean proyectosEEUU;
    private boolean visa;
    private int totalAnosExperienciaLaboral;


    public Usuario(String id,
                   String correoEmpresarial,
                   String identificacion,
                   String nombres,
                   String apellidos,
                   String cliente,
                   LocalDate fechaIngreso,
                   double diasGanados,
                   int diasTomados,
                   String googleId,
                   String correoPersonal,
                   String telefonoFijo,
                   String telefonoCelular,
                   String idNacionalidad,
                   String idLugarNacimiento,
                   String idLugarRecidencia,
                   LocalDate fechaNacimiento,
                   String direccion,
                   String nombreContactoEmergencia,
                   String parentescoContactoEmergencia,
                   String telefonoContactoEmergencia,
                   String tarjetaProfesional,
                   String perfilProfesional,
                   String conocimientosTecnicos,
                   UsuarioTallaCamisa tallaCamisa,
                   UsuarioEstadoCivil estadoCivil,
                   UsuarioGenero genero,
                   UsuarioGrupoSanguineo grupoSanguineo,
                   Long idTipoDocumento,
                   Long idProfesion,
                   LocalDate fechaCreacion,
                   LocalDate fechaUltimaActualizacion,
                   String cargo,
                   String idVicepresidencia,
                   List<Long> capacidad,
                   List<Fotografia> fotografias,
                   boolean aceptacionTerminos,
                   double barraProgreso,
                   String nombrePreferencia,
                   String pasaporte,
                   int estrato,
                   String semestre,
                   EstadoProfesion estadoProfesion,
                   String tipoContratoId,
                   LocalDate fechaInicioContrato,
                   String empresa,
                   String contraseniaComputador,
                   boolean proyectosEEUU,
                   boolean visa,
                   int totalAnosExperienciaLaboral,
                   String profesion) {
        super(id);
        this.correoEmpresarial = correoEmpresarial;
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.idVicepresidencia = idVicepresidencia;
        this.cliente = cliente;
        this.fechaIngreso = fechaIngreso;
        this.diasGanados = diasGanados;
        this.diasTomados = diasTomados;
        this.fechaCreacion = fechaCreacion;
        this.googleId = googleId;
        this.correoPersonal = correoPersonal;
        this.telefonoFijo = telefonoFijo;
        this.telefonoCelular = telefonoCelular;
        this.idNacionalidad = idNacionalidad;
        this.idLugarNacimiento = idLugarNacimiento;
        this.idLugarRecidencia = idLugarRecidencia;
        this.fechaNacimiento = fechaNacimiento;
        this.direccion = direccion;
        this.nombreContactoEmergencia = nombreContactoEmergencia;
        this.parentescoContactoEmergencia = parentescoContactoEmergencia;
        this.telefonoContactoEmergencia = telefonoContactoEmergencia;
        this.tarjetaProfesional = tarjetaProfesional;
        this.perfilProfesional = perfilProfesional;
        this.conocimientosTecnicos = conocimientosTecnicos;
        this.tallaCamisa = tallaCamisa;
        this.estadoCivil = estadoCivil;
        this.genero = genero;
        this.grupoSanguineo = grupoSanguineo;
        this.estado = UsuarioEstado.ACTIVO;
        this.idTipoDocumento = idTipoDocumento;
        this.idProfesion = idProfesion;
        this.fechaUltimaActualizacion = fechaUltimaActualizacion;
        this.cargo = cargo;
        this.capacidad = capacidad;
        this.aceptacionTerminos = aceptacionTerminos;
        this.bitacoraPrimeraVez = false;
        this.barraProgreso = barraProgreso;
        actualizarFechaUltimaActualizacion(LocalDate.now());
        this.fotografias = Objects.requireNonNullElseGet(fotografias, ArrayList::new);
        calcularDiasDisponibles();
        this.nombrePreferencia=nombrePreferencia;
        this.pasaporte = pasaporte;
        this.estrato = estrato;
        this.semestre = semestre;
        this.estadoProfesion = estadoProfesion;
        this.tipoContratoId = tipoContratoId;
        this.fechaInicioContrato = fechaInicioContrato;
        this.empresa = empresa;
        this.contraseniaComputador = contraseniaComputador;
        this.profesion = profesion;
        this.proyectosEEUU = proyectosEEUU;
        this.visa = visa;
        this.totalAnosExperienciaLaboral = totalAnosExperienciaLaboral;
    }

    public Fotografia obtenerFotografiaPorTipo(TipoFotografia tipoFotografia) {
        return fotografias
                .stream()
                .filter(fotografia -> fotografia.getTipoFotografia().compareTo(tipoFotografia) == 0)
                .findFirst()
                .orElse(new Fotografia());
    }

    public void actualizarFechaUltimaActualizacion(LocalDate fechaUltimaActualizacion) {
        this.fechaUltimaActualizacion = fechaUltimaActualizacion;
    }

    public void actualizarFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public void agregarDiasTomados(int agregarDiasTomados) {
        this.diasTomados = this.diasTomados + agregarDiasTomados;
        calcularDiasDisponibles();
    }

    public void calcularDiasDisponibles() {
        this.diasDisponibles = this.diasGanados - this.diasTomados;
    }

}
