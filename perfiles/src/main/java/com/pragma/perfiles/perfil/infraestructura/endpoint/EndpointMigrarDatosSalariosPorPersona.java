package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorMigrarDatosSalariosPorPersona;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.InvocationTargetException;

@RestController
@RequestMapping("migracion")
@RequiredArgsConstructor
@Validated
public class EndpointMigrarDatosSalariosPorPersona {

    private final ManejadorMigrarDatosSalariosPorPersona manejadorMigrarDatosSalariosPorPersona;

    @GetMapping("/activos")
    public ResponseEntity<HttpStatus> ejecutarMigrarDatos() throws InvocationTargetException, IllegalAccessException {
        manejadorMigrarDatosSalariosPorPersona.ejecutar();

        return new ResponseEntity(HttpStatus.OK);
    }


}
