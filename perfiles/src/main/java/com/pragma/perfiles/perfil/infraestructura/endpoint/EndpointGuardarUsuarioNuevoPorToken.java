package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.seguridad.jwt.UsuarioContextoComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorGuardarUsuarioNuevoToken;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarUsuarioNuevoPorToken {


    private final ManejadorGuardarUsuarioNuevoToken manejadorGuardarUsuarioNuevoToken;

    @PostMapping("/token")
    public ObjetoRespuesta<Usuario> ejecutarGuardarUsuarioNuevo() {
        UsuarioContextoComando usuarioContextoComando=(UsuarioContextoComando) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return  manejadorGuardarUsuarioNuevoToken.ejecutar(usuarioContextoComando);
    }

}
