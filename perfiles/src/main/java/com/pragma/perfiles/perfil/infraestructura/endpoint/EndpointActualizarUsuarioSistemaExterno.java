package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComandoSistemaExterno;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioPorCorreo;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioSistemaExterno;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequiredArgsConstructor
@Validated
public class EndpointActualizarUsuarioSistemaExterno {

    private final ManejadorActualizarUsuarioSistemaExterno manejadorActualizarUsuarioPorCorreo;

    @PutMapping("updateUserInfoExternalService")
    public RespuestaBase actualizarUsuarioPorCorreo(
            @NotNull @RequestBody ActualizarUsuarioComandoSistemaExterno actualizarUsuarioComando) {
        manejadorActualizarUsuarioPorCorreo.ejecutar(actualizarUsuarioComando);
        return RespuestaBase.ok(" Success ");
    }

}
