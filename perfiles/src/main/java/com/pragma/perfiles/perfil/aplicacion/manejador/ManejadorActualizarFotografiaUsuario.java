package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarFotografiaUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.comando.GuardarFotografiaUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioConFotografias;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorActualizarFotografiaUsuario {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaUsuario fabricaUsuario;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public ObjetoRespuesta<UsuarioConFotografias> ejecutar(ActualizarFotografiaUsuarioComando actualizarFotografiaUsuarioComando){
        Usuario usuarioConFotografia = fabricaUsuario.actualizarFotografiaUsuario(actualizarFotografiaUsuarioComando);

        usuarioConFotografia.setId(actualizarFotografiaUsuarioComando.getUserId());

        usuarioConFotografia.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuarioConFotografia.getId()));
        usuarioConFotografia.setFechaUltimaActualizacion(LocalDate.now());

        usuarioCrudUseCase.actualizarConFoto(usuarioConFotografia);
        usuarioConFotografia = usuarioCrudUseCase.findByCorreoEmpresarial(actualizarFotografiaUsuarioComando.getCorreoEmpresarial());
        return new ObjetoRespuesta<UsuarioConFotografias>(UsuarioConFotografias
                .builder()
                .userId(usuarioConFotografia.getId())
                .correoEmpresarial(usuarioConFotografia.getCorreoEmpresarial())
                .fotografias(usuarioConFotografia.getFotografias())
                .build());
    }

}
