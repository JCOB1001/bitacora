package com.pragma.perfiles.perfil.dominio.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UsuarioEstado {

    ACTIVO("ACTIVO", true),
    INACTIVO("INACTIVO", false);

    private String estado;

    private boolean valor;

}
