package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuariosPorId;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerUsuariosPorId {

    private final ManejadorObtenerUsuariosPorId manejadorObtenerUsuariosPorId;

    @PostMapping("/id")
    public ObjetoRespuesta<List<Usuario>> ejecutarObtenerUsuarioPorId(@NotNull @RequestBody List<String> idUsuarios) {
        List<Usuario> usuarios = manejadorObtenerUsuariosPorId.ejecutar(idUsuarios);
        return new ObjetoRespuesta<>(HttpStatus.OK, usuarios);
    }

}