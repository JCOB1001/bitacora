package com.pragma.perfiles.perfil.aplicacion.comando;

import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.vacaciones.common.filtro.FiltroBase;
import lombok.Getter;

@Getter
public class ObtenerUsuariosFiltroComando extends FiltroBase {

    private String identificacion;
    private Boolean fechaCreacionAscendente;
    private Boolean fechaInicioContratoAscendente;
    private UsuarioEstado estado;
    private String nombres;
    private String apellidos;

    public ObtenerUsuariosFiltroComando(Integer numeroPagina, Integer totalElementos, String identificacion, Boolean fechaCreacionAscendente,Boolean fechaInicioContratoAscendente,UsuarioEstado estado,String nombres, String apellidos) {
        super(numeroPagina, totalElementos);
        this.identificacion = identificacion;
        this.fechaCreacionAscendente = fechaCreacionAscendente;
        this.fechaInicioContratoAscendente = fechaInicioContratoAscendente;
        this.estado = estado;
        this.nombres = nombres;
        this.apellidos = apellidos;
    }

}
