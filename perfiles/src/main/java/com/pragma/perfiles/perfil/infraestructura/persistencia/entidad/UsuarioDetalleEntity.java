package com.pragma.perfiles.perfil.infraestructura.persistencia.entidad;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "usuario_detalle")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDetalleEntity implements IdEntidad<String> {
    public interface Atributos extends IdEntidad.Atributos {
        String PERFIL_PROFESIONAL = "perfilProfesional";
        String CONOCIMIENTOS_TECNICOS = "conocimientosTecnicos";
        String CARGO = "cargo";
        String IDIOMA = "idioma";
        String USUARIO = "usuario";
        String PROFESION = "profesion";
    }
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String profesion;
    private String perfilProfesional;
    private String conocimientosTecnicos;
    private String cargo;
    @ManyToOne()
    @JoinColumn(name = "idiomas_id", foreignKey = @ForeignKey(name = "fk_usuario_detalle_idiomas_idiomas_id"))
    private IdiomasEntity idioma;
    @JsonManagedReference
    @ManyToOne()
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_usuario_id"))
    private UsuarioEntity usuario;
}
