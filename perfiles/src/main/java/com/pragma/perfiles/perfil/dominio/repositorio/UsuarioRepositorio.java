package com.pragma.perfiles.perfil.dominio.repositorio;

import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.pragmadata.aplicacion.comando.ConsultaPragmaDataComando;
import com.pragma.perfiles.pragmadata.dominio.modelo.*;
import com.pragma.perfiles.pragmadex.dominio.filtros.FiltroNombreCorreo;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public interface UsuarioRepositorio extends CrudRepositorioBase<String, Usuario> {

    public Usuario findByIdentificacion(String identificacion);

    public Usuario findBycorreoEmpresarial(String correoEmpresarial);

    public Usuario findByCorreoPersonal(String correoPersonal);

    public Usuario findByGoogleId(String googleId);

    public Usuario findByid(String idUsuario);

    public void actualizarConFoto(Usuario modelo);

    public Stream<Usuario> findByIdVicepresidencia(String idViceprecidencia);

    ObjetoRespuestaUsuario findByNombreOrCorreoEmpresarialPaginated(FiltroNombreCorreo filtroNombreCorreo);

    ObjetoRespuestaUsuario buscarPorHabilidadPaginado(FiltroNombreCorreo filtroNombreCorreo);

    ObjetoRespuestaUsuario buscarPorEmpresaYNombrePaginado(FiltroNombreCorreo filtroNombreCorreo);

    ObjetoRespuestaUsuario buscarPorEmpresaYHabilidadPaginado(FiltroNombreCorreo paginas);

    List<String> buscarDatosPorListaIds(List<String> idUsuario, Class table, FiltroNombreCorreo filtroNombreCorreo);

    List<Usuario> buscarPorNombreCorreo(String nombre);

    List<Vicepresidencia> obtenerVicepresidencias();

    List<TipoDocumento> obtenerTiposDeDocumentos();

    List<Profesion> obtenerProfesionPorListaId(List<String> idProfesion);

    List<UbicacionGeograficaCompleta> obtenerUbicacionesGeograficas(List<String> ubicacionesIds);

    List<UbicacionGeograficaBasica> obtenerUbicacionesGeograficasLista(List<String> ubicacionesIds, boolean padre, String nivel);

    ObjetoRespuestaUsuario buscarUsuariosPorFiltros(List<ConsultaPragmaDataComando> campos, Integer numeroElementos, Integer numeroPagina);

    Usuario buscarPorIdentificacion(Long idTipoDocumento, String identificacionUsuario);

    List<UsuarioEntity> findByEstado(UsuarioEstado estado);

    Stream<Usuario> findByCorreoEmpresarialIn(List<String> correos);

    ObjetoRespuestaUsuario obtenerInformacionTodosUsuarios(FiltroUsuario filtro);

    Stream<InformacionConocimientoArchivo> findByUsuarioIn(Collection<String> idLista);
}
