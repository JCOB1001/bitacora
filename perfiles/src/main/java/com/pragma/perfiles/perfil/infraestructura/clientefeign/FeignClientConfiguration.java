package com.pragma.perfiles.perfil.infraestructura.clientefeign;

import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignClientConfiguration {

    private String slackOauthAccessToken;

    @Bean
    public RequestInterceptor bearerTokenRequestInterceptor() {
        return new RequestInterceptor() {
            @Override
            public void apply(RequestTemplate template) {
                template.header("Authorization", HttpRequestContextHolder.getIdentificacion());
                template.header("identification", HttpRequestContextHolder.getAutorizacion());
                template.header("Cache-Control","no-cache, must-revalidate");
            }
        };
    }

}

