package com.pragma.perfiles.perfil.aplicacion.consulta;

import com.pragma.perfiles.perfil.dominio.modelo.Fotografia;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioConFotografias {
    private String correoEmpresarial;
    private String userId;
    private List<Fotografia> fotografias;
}
