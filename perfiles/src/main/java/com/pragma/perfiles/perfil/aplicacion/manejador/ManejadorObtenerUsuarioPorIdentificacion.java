package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioPorIdentificacion {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public Usuario ejecutar(Long idTipoDocumento,String identificacionUsuario) {
        Usuario usuario = usuarioCrudUseCase.buscarPorIdentificacion(idTipoDocumento, identificacionUsuario);
        return usuario;
    }

}
