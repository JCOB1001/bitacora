package com.pragma.perfiles.perfil.infraestructura.clientefeign.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class File {
    private int size;
    private String name;
    private String fullPath;
}

