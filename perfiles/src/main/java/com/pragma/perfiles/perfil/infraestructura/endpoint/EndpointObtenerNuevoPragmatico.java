package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.NuevoPragmaticoNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioNuevoPragmaticoComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerNuevoPragmatico;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerNuevoPragmatico {

    private final ManejadorObtenerNuevoPragmatico manejadorObtenerNuevoPragmatico;

    @GetMapping("/nuevopragmatico")
    public ObjetoRespuesta<ActualizarUsuarioNuevoPragmaticoComando> ejecutarObtenerNuevoPragmatico(@RequestParam("email") String email) throws Exception {

        ObjetoRespuesta<ActualizarUsuarioNuevoPragmaticoComando>  objetoRespuestaNuevoPragmatico = manejadorObtenerNuevoPragmatico.ejecutar(email);

        if(objetoRespuestaNuevoPragmatico.getDato() == null){
            throw new NuevoPragmaticoNoEncontrado("No existe Informacion relacionada a este email");
        }
        return new ObjetoRespuesta<ActualizarUsuarioNuevoPragmaticoComando>(HttpStatus.OK,
                objetoRespuestaNuevoPragmatico.getDato());
    }

}
