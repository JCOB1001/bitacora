package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarDiasVacacionesComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorActualizarDiasVacaciones {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public void ejecutar(String idUsuario,
                         ActualizarDiasVacacionesComando actualizarDiasVacacionesComando){
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no se encuentra registrado" +
                    "en la base de datos");
        }
        usuario.setDiasGanados(actualizarDiasVacacionesComando.getDiasGanados());
        usuario.setDiasTomados(actualizarDiasVacacionesComando.getDiasTomados());
        usuario.calcularDiasDisponibles();

        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuario.setFechaUltimaActualizacion(LocalDate.now());

        usuarioCrudUseCase.actualizar(usuario);
    }

}
