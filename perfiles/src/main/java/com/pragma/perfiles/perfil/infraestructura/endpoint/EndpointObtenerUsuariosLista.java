package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioLista;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerUsuariosLista {

    private final ManejadorObtenerUsuarioLista manejadorObtenerUsuarioLista;

    @GetMapping("/lista")
    public ObjetoRespuesta<Stream<Usuario>> obtenerTodosLosUsuarios(){
        ObjetoRespuesta<Stream<Usuario>> streamObjetoRespuesta = manejadorObtenerUsuarioLista.obtenerUsuarios();
        return new ObjetoRespuesta<Stream<Usuario>>(HttpStatus.OK,streamObjetoRespuesta.getDato());
    }

}
