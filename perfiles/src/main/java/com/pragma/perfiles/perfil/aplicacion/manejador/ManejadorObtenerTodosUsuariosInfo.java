package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuariosFiltroComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioInfoConsulta;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioInfo;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import com.pragma.perfiles.pragmadata.dominio.modelo.TipoDocumento;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ManejadorObtenerTodosUsuariosInfo {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FeignTipoDocumento feignTipoDocumento;
    private final FabricaUsuario fabricaUsuario;

    public List<UsuarioInfoConsulta> ejecutar(ObtenerUsuariosFiltroComando obtenerUsuariosFiltroComando){
        FiltroUsuario filtroUsuario = fabricaUsuario.cargarFiltroUsuario(obtenerUsuariosFiltroComando);

        List<TipoDocumento> documentoList = feignTipoDocumento.obtenerTipoDocumentos().getDato();
        List<UsuarioInfo> usuarioInfos = (List<UsuarioInfo>) usuarioCrudUseCase.obtenerInformacionTodosUsuarios(filtroUsuario).getDato();

        List<UsuarioInfoConsulta> consultaList = new ArrayList<>();
        for (UsuarioInfo consulta : usuarioInfos) {
            String nombreTipoDocumento = "";
            for (TipoDocumento consultatipo : documentoList) {
                if (consultatipo.getId().equals(consulta.getIdTipoDocumento())){
                    nombreTipoDocumento = consultatipo.getNombre();
                }
            }
            UsuarioInfoConsulta usuarioInfoConsulta = UsuarioInfoConsulta.builder()
                    .nombres(consulta.getNombres())
                    .apellidos(consulta.getApellidos())
                    .tipoDocumento(nombreTipoDocumento)
                    .identificacion(consulta.getIdentificacion())
                    .id(consulta.getId())
                    .correo(consulta.getCorreo())
                    .fechaInicioContrato(consulta.getFechaInicioContrato())
                    .tipoContrato(consulta.getTipoContrato())
                    .vacaciones(consulta.getVacaciones())
                    .build();

            consultaList.add(usuarioInfoConsulta);
        }
        return consultaList;
    }
}
