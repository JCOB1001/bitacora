package com.pragma.perfiles.perfil.infraestructura.persistencia.entidad;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.pragma.perfiles.perfil.dominio.modelo.TipoFotografia;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "fotografia")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class FotografiaEntity implements IdEntidad<String> {

    public interface Atributos extends IdEntidad.Atributos {
        String CONTENIDO = "contenido";
        String TIPOFOTOGRAFIA = "tipoFotografia";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String contenido;
    @Column(name = "tipo_fotografia")
    @Enumerated(value = EnumType.STRING)
    private TipoFotografia tipoFotografia;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_fotografia_usuario_usuario_id"))
    private UsuarioEntity usuarioEntity;

    @CreatedDate
    private LocalDate fechaCreacion;

}

