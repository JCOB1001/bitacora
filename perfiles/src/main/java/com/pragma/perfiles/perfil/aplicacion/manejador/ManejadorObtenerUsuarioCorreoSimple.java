package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioInfo;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioCorreoSimple {
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<UsuarioInfo> ejecutar(String correoEmpresarialUsuario) throws Exception{
        Usuario usuario = usuarioCrudUseCase.findByCorreoEmpresarial(correoEmpresarialUsuario);
        UsuarioInfo usuarioInfo = new UsuarioInfo(
                usuario.getNombres(),
                usuario.getApellidos(),
                usuario.getIdTipoDocumento(),
                usuario.getIdentificacion(),
                usuario.getId(),
                usuario.getCorreoEmpresarial(),
                usuario.getFechaInicioContrato(),
                usuario.getTipoContratoId(),
                false
        );
        return new ObjetoRespuesta<UsuarioInfo>(HttpStatus.OK, usuarioInfo);
    }
}
