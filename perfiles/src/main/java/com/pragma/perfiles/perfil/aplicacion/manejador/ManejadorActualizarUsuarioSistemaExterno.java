package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComandoSistemaExterno;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import com.pragma.perfiles.tipo_contrato.aplicacion.manejador.ManejadorObtenerTipoContratoPorCoincidenciaNombre;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.stream.Collectors;

import static com.pragma.perfiles.informacion_laboral.dominio.modelo.EstadoPeriodoPrueba.fromValueEstadoPeridoPrueba;
import static com.pragma.perfiles.perfil.dominio.modelo.UsuarioGenero.fromValue;

@RequiredArgsConstructor
public class ManejadorActualizarUsuarioSistemaExterno {

    private final FeignTipoDocumento feignTipoDocumento;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;
    private final InformacionNominaCrudUseCase informacionNominaCrudUseCase;
    private final InformacionLaboralCrudUseCase informacionLaboralCrudUseCase;
    private final ManejadorObtenerTipoContratoPorCoincidenciaNombre manejadorObtenerTipoContratoPorCoincidenciaNombre;

    public ObjetoRespuesta<Usuario> ejecutar(ActualizarUsuarioComandoSistemaExterno actualizarUsuarioComandoSistemaExterno) {


        Usuario usuario;

        //Se busca el usuario por correo
        usuario = usuarioCrudUseCase.findByCorreoEmpresarial(actualizarUsuarioComandoSistemaExterno.getCorreo());

        //Enviamos a guardar todo lo que no necesita un micro externo
        usuario = GuardarDatosSimples(usuario, actualizarUsuarioComandoSistemaExterno);

        //Enviamos a guardar todo lo que necesita un micro externo
        usuario = GuardarDatosConsultasServicios(usuario, actualizarUsuarioComandoSistemaExterno);

        //Guardado de nomina
        GuardarInformacionNomina(usuario, actualizarUsuarioComandoSistemaExterno);

        //Guardado de informacion laboral
        GuardadoInformacionLaboral(usuario, actualizarUsuarioComandoSistemaExterno);

        return new ObjetoRespuesta<Usuario>(usuario);
    }

    public Usuario GuardarDatosSimples(Usuario usuarioActualizacion, ActualizarUsuarioComandoSistemaExterno datos) {
        usuarioActualizacion.setNombres(datos.getNombres());
        usuarioActualizacion.setApellidos(datos.getApellidos());
        usuarioActualizacion.setIdentificacion(datos.getIdentificacion());
        usuarioActualizacion.setFechaNacimiento(LocalDate.parse(datos.getFechaNacimiento()));
        usuarioActualizacion.setFechaIngreso(LocalDate.parse(datos.getFechaInicioContratoActual()));
        usuarioActualizacion.setGenero(fromValue(datos.getSexo())); //LLega del archivo y en BD se encuentra MASCULINO FEMENINO ****REVISAR***
        usuarioActualizacion.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuarioActualizacion.getId()));
        usuarioActualizacion.setFechaUltimaActualizacion(LocalDate.now());
        usuarioActualizacion.setPasaporte(datos.getPasaporte());
        usuarioCrudUseCase.actualizar(usuarioActualizacion);
        return usuarioActualizacion;
    }

    public void GuardarInformacionNomina(Usuario usuario, ActualizarUsuarioComandoSistemaExterno datos) {
        InformacionNomina informacionNomina = new InformacionNomina();
        try {
            informacionNomina = informacionNominaCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList()).get(0);
        } catch (Exception e) {

        }
        informacionNomina.setIdUsuario(usuario.getId());
        informacionNomina.setMoneda(datos.getMoneda());
        informacionNomina.setCargoNomina(datos.getCargoNomina());
        informacionNomina.setNoConstitutivosSalario(Double.valueOf(datos.getNoConstitutivosSalario().replace(".", "")));
        informacionNomina.setSalarioBasicoIntegral(Double.valueOf(datos.getSalarioBasicoIntegral().replace(".", "")));
        informacionNomina.setSalarioBasicoOrdinario(Double.valueOf(datos.getSalarioBasicoOrdinario().replace(".", "")));
        informacionNominaCrudUseCase.guardar(informacionNomina);
    }

    public void GuardadoInformacionLaboral(Usuario usuario, ActualizarUsuarioComandoSistemaExterno datos) {
        InformacionLaboral informacionLaboral = new InformacionLaboral();
        try {
            informacionLaboral = informacionLaboralCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList()).get(0);
        } catch (Exception e) {

        }
        informacionLaboral.setUsuarioId(usuario.getId());
        informacionLaboral.setEstadoPeriodoPrueba(fromValueEstadoPeridoPrueba(datos.getEstadoPeriodoPrueba()));
        informacionLaboral.setReferido(datos.getReferido());
        informacionLaboral.setEvaluadorPeriodoPrueba(datos.getEvaluadorPeriodoPrueba());
        informacionLaboral.setFechaInicioContratoActual(LocalDate.parse(datos.getFechaInicioContratoActual()));
        informacionLaboral.setFechaInicioRelacion(LocalDate.parse(datos.getFechaInicioRelacion()));
        String id = manejadorObtenerTipoContratoPorCoincidenciaNombre.ejecutar(datos.getTipoContrato()).getDato();

        if (id != null) {
            informacionLaboral.setTipoContrato(id);
        }
        informacionLaboralCrudUseCase.guardar(informacionLaboral);
    }

    public Usuario GuardarDatosConsultasServicios(Usuario usuario, ActualizarUsuarioComandoSistemaExterno datos) {
        Long idTipoDocumento = feignTipoDocumento.obtenerIdTipoDocumentoPorCoincidenciaNombre(datos.getTipoIdentificacion());
        usuario.setIdTipoDocumento(idTipoDocumento);
        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuarioCrudUseCase.actualizar(usuario);
        return usuario;
    }

}
