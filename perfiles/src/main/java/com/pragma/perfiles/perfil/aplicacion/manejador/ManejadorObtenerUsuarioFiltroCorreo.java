package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuarioFiltroCorreoComando;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuarioCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCaseFiltroCorreo;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioFiltroCorreo {

    private final UsuarioCrudUseCaseFiltroCorreo usuarioCrudUseCaseFiltroCorreo;
    private final FabricaUsuario fabricaUsuario;

    public ObjetoRespuesta<Usuario> ejecutar(ObtenerUsuarioFiltroCorreoComando obtenerUsuarioFiltroCorreoComando) {
        FiltroUsuarioCorreo filtroUsuarioCorreo = fabricaUsuario.cargarFiltroUsuarioCorreo(obtenerUsuarioFiltroCorreoComando);
        ObjetoRespuestaUsuario objetoRespuestaUsuario = usuarioCrudUseCaseFiltroCorreo.obtenerTodos(filtroUsuarioCorreo);
        if (objetoRespuestaUsuario.getDato() == null) {
            throw new UsuarioNoEncontrado("No se encontró usuario");
        }
        Stream<Usuario> streamUsuarios = (Stream<Usuario>)objetoRespuestaUsuario.getDato();
        Optional<Usuario> optionalUsuario = streamUsuarios.findFirst();
        if (optionalUsuario.isEmpty()) {
            throw new UsuarioNoEncontrado("No se encontró usuario");
        }
        return new ObjetoRespuesta<Usuario>(HttpStatus.OK, optionalUsuario.get());
    }

}
