package com.pragma.perfiles.perfil.infraestructura.clienteaws;

import com.pragma.perfiles.comun.infraestructura.error.CloudFrontServiceException;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cloudfront.CloudFrontClient;
import software.amazon.awssdk.services.cloudfront.model.CreateInvalidationRequest;
import software.amazon.awssdk.services.cloudfront.model.InvalidationBatch;
import software.amazon.awssdk.services.cloudfront.model.Paths;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class AwsCloudfrontClient {

    private final AwsCloudfrontClientConfiguracion cloudfrontConfig;
    private final CloudFrontClient cloudFrontClient;

    public AwsCloudfrontClient(AwsCloudfrontClientConfiguracion cloudfrontConfig) {

        this.cloudfrontConfig = cloudfrontConfig;
        AwsBasicCredentials awsCredentials = AwsBasicCredentials.create(
                cloudfrontConfig.getAccessKeyId(),
                cloudfrontConfig.getSecretAccessKey()
        );

        cloudFrontClient = CloudFrontClient
                .builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsCredentials))
                .region(Region.AWS_GLOBAL)
                .build();

    }


    /**
     * crea una invalidación en la distribución de CloudFront para los path indicados
     *
     * @param pathsList Lista de strings con las rutas a invalidar, el path debe iniciar con "/"
     */
    public void invalidarCache(List<String> pathsList) {
        InvalidationBatch invalidationBatch = InvalidationBatch
                .builder()
                .paths(Paths.builder().items(pathsList.toArray(new String[0])).quantity(pathsList.size()).build())
                .callerReference(LocalDateTime.now().toString()) // asigna un id único para cada petición, tomado de la marca de tiempo
                .build();

        try {
            cloudFrontClient.createInvalidation(CreateInvalidationRequest
                    .builder()
                    .distributionId(cloudfrontConfig.getDistributionId())
                    .invalidationBatch(invalidationBatch)
                    .build());
        }
        catch (SdkException e) {
            throw new CloudFrontServiceException("Error en la configuracion de aws cloudfront service");
        }
    }

}
