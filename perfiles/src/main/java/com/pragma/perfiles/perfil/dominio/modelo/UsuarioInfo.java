package com.pragma.perfiles.perfil.dominio.modelo;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class UsuarioInfo {
    private String nombres;
    private String apellidos;
    private long idTipoDocumento;
    private String identificacion;
    private String id;
    private String correo;
    private LocalDate fechaInicioContrato;
    private String tipoContrato;
    private Boolean vacaciones;

    public UsuarioInfo(
            String nombres,
            String apellidos,
            long idTipoDocumento,
            String identificacion,
            String id,
            String correo,
            LocalDate fechaInicioContrato,
            String tipoContrato,
            Boolean vacaciones
    ){
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.idTipoDocumento = idTipoDocumento;
        this.identificacion = identificacion;
        this.id = id;
        this.correo = correo;
        this.fechaInicioContrato = fechaInicioContrato;
        this.tipoContrato = tipoContrato;
        this.vacaciones = vacaciones;
    }
}
