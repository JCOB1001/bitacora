package com.pragma.perfiles.perfil.dominio.useCase;

import com.pragma.perfiles.comun.infraestructura.seguridad.entity.UsuarioEntity;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioDetalle;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioDetalleRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

public class UsuarioDetalleCrudUseCase  extends CrudUseCaseCommon<UsuarioDetalleRepositorio, UsuarioDetalle, String> {

    private final UsuarioDetalleRepositorio usuarioDetalleRepositorio;

    public UsuarioDetalleCrudUseCase(UsuarioDetalleRepositorio usuarioDetalleRepositorio) {
        super(usuarioDetalleRepositorio);
        this.usuarioDetalleRepositorio = usuarioDetalleRepositorio;
    }

    public UsuarioDetalle findByUsuarioIdAndIdiomaId(String usuarioId,String idiomaId){
        return usuarioDetalleRepositorio.findByUsuarioIdAndIdiomaId(usuarioId,idiomaId);
    }
}
