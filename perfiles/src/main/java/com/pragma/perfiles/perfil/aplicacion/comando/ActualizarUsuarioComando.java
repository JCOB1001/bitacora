package com.pragma.perfiles.perfil.aplicacion.comando;

import com.pragma.perfiles.perfil.dominio.modelo.*;
import lombok.Data;

import java.time.LocalDate;

@Data
public class ActualizarUsuarioComando {

    private String nombres;
    private String googleId;
    private String identificacion;
    private String apellidos;
    private String correoPersonal;
    private String telefonoFijo;
    private String telefonoCelular;
    private String  idNacionalidad;
    private String idLugarNacimiento;
    private String idLugarRecidencia;
    private LocalDate fechaNacimiento;
    private String direccion;
    private String nombreContactoEmergencia;
    private String parentescoContactoEmergencia;
    private String telefonoContactoEmergencia;
    private UsuarioTallaCamisa tallaCamisa;
    private UsuarioEstadoCivil estadoCivil;
    private UsuarioGenero genero;
    private UsuarioGrupoSanguineo grupoSanguineo;
    private Long idTipoDocumento;

    private double diasGanados;
    private int diasTomados;
    private double diasDisponibles;
    private double diasPendientes;
    private int estrato;
    private String contraseniaComputador;
}
