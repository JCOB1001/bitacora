package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioNuevoPragmaticoComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.*;

import static com.pragma.perfiles.perfil.dominio.modelo.UsuarioGenero.fromValue;

@Mapper(componentModel = "spring")
public interface UsuarioMapperActualizarUsuarioNuevoPragmatico extends ConvertidorBase<Usuario, ActualizarUsuarioNuevoPragmaticoComando> {

    @Named("rightToLeft")
    @Mapping(source = "fechaInicioContratoActual", target = "fechaIngreso")
    @Mapping(target = "genero", ignore = true)
    @Mapping(source = "cargoNomina", target = "cargo")
    Usuario rightToLeft(ActualizarUsuarioNuevoPragmaticoComando actualizarUsuarioNuevoPragmaticoComando);

    @AfterMapping
    default void setGenero(@MappingTarget Usuario usuario, ActualizarUsuarioNuevoPragmaticoComando actualizarUsuarioNuevoPragmaticoComando) {
        usuario.setGenero(fromValue(actualizarUsuarioNuevoPragmaticoComando.getSexo()));
    }
}
