package com.pragma.perfiles.perfil.dominio.modelo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;
import com.pragma.perfiles.comun.infraestructura.error.EnumNoEncontrado;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum UsuarioTallaCamisa {

    XXXXL("XXXXL"),XXXL("XXXL"),XXL("XXL"),XL("XL"),L("L"),M("M"),S("S"),XS("XS"),DIECISEIS("16"), NO_ESPECIFICADO("NN");

    private String tallaCamisa;

    @JsonValue
    public String getValue() {
        return getTallaCamisa();
    }

    @JsonCreator
    public static UsuarioTallaCamisa fromValue(String value) {
        if (value == null || value.isEmpty()) {
            return UsuarioTallaCamisa.NO_ESPECIFICADO;
        }
        for (UsuarioTallaCamisa p : values()) {
            if (p.getValue().equals(value)) {
                return p;
            }
        }
        throw new EnumNoEncontrado("Talla no encontrada " + value);
    }

    private UsuarioTallaCamisa(String tallaCamisa){
        this.tallaCamisa = tallaCamisa;
    }

    public String getTallaCamisa() {
        return tallaCamisa;
    }
}
