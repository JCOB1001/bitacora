package com.pragma.perfiles.perfil.infraestructura.configuracion;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "enlace-archivo-pragmatico")
@RequiredArgsConstructor
@Getter
@Setter
public class InformacionArchivoNuevoPragmaticoPropiedades {

    private String url;
}
