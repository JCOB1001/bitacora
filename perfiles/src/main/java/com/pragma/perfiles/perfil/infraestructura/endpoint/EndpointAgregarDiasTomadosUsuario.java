package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.seguridad.jwt.UsuarioContextoComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorAgregarDiasTomadosUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointAgregarDiasTomadosUsuario {

    private final ManejadorAgregarDiasTomadosUsuario manejadorAgregarDiasTomadosUsuario;

    @PutMapping("/agregardiastomados/{diasTomados}")
    public ObjetoRespuesta<Usuario> manejadorAgregarDiasTomadosUsuario(@NotNull @PathVariable Integer diasTomados) {

        UsuarioContextoComando usuarioDatos = (UsuarioContextoComando) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        ObjetoRespuesta<Usuario> objetoRespuestaUsuario = manejadorAgregarDiasTomadosUsuario.ejecutar(usuarioDatos.getCorreo(), diasTomados);
        if (objetoRespuestaUsuario.getDato() == null) {
            throw new UsuarioNoEncontrado("No existe esta usuario");
        }
        return new ObjetoRespuesta<Usuario>(HttpStatus.OK,
                objetoRespuestaUsuario.getDato());
    }

}
