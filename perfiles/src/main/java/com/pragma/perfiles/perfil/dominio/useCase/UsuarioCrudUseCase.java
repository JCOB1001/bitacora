package com.pragma.perfiles.perfil.dominio.useCase;

import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorio;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionConocimientoArchivo;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionIdiomasArchivo;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UsuarioCrudUseCase extends CrudUseCaseCommon<UsuarioRepositorio, Usuario, String> {

    private final UsuarioRepositorio usuarioRepositorio;

    public UsuarioCrudUseCase(UsuarioRepositorio usuarioRepositorio) {
        super(usuarioRepositorio);
        this.usuarioRepositorio = usuarioRepositorio;
    }

    public Usuario findByCorreoEmpresarial(String correoEmpresarial){
        return usuarioRepositorio.findBycorreoEmpresarial(correoEmpresarial);
    }

    public Usuario findByIdentificacion(String identificacion){
        return usuarioRepositorio.findByIdentificacion(identificacion);
    }

    public Usuario findByCorreoPersonal(String correoPersonal){
        return usuarioRepositorio.findByCorreoPersonal(correoPersonal);
    }

    public Usuario findById(String idUsuario){
        return usuarioRepositorio.findByid(idUsuario);
    }

    public Usuario findbyGoogleId(String googleId){
        return usuarioRepositorio.findByGoogleId(googleId);
    }

    public void actualizarConFoto(Usuario modelo){
        usuarioRepositorio.actualizarConFoto(modelo);
    }

    public List<Usuario> findByIdVicepresidencia(String idViceprecidencia){
        return usuarioRepositorio.findByIdVicepresidencia(idViceprecidencia).collect(Collectors.toList());
    }

    public List<Usuario> buscarPorNombreCorreo(String nombre){
        return usuarioRepositorio.buscarPorNombreCorreo(nombre);
    }

    public Usuario buscarPorIdentificacion(Long idTipoDocumento, String identificacionUsuario){
        return usuarioRepositorio.buscarPorIdentificacion(idTipoDocumento, identificacionUsuario);
    }
    public List<UsuarioEntity> findByEstado(UsuarioEstado estado){
        return usuarioRepositorio.findByEstado(estado);
    }

    public Stream<Usuario> findByCorreoEmpresarialIn(List<String> correos) {
        return usuarioRepositorio.findByCorreoEmpresarialIn(correos);
    }

    public ObjetoRespuestaUsuario obtenerInformacionTodosUsuarios(FiltroUsuario filtro){
        return usuarioRepositorio.obtenerInformacionTodosUsuarios(filtro);
    }

    public Stream<InformacionConocimientoArchivo> findByUsuarioIn(Collection<String> idLista) {
        return usuarioRepositorio.findByUsuarioIn(idLista);
    }
}
