package com.pragma.perfiles.perfil.dominio.repositorio;

import com.pragma.perfiles.comun.infraestructura.seguridad.entity.UsuarioEntity;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioDetalle;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface UsuarioDetalleRepositorio extends CrudRepositorioBase<String, UsuarioDetalle> {

    public UsuarioDetalle findByUsuarioIdAndIdiomaId(String usuarioId,String idiomaId);
}
