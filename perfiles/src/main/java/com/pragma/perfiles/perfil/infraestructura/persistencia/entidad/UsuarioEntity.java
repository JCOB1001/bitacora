package com.pragma.perfiles.perfil.infraestructura.persistencia.entidad;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.pragma.perfiles.perfil.dominio.modelo.EstadoProfesion;
import com.pragma.perfiles.perfil.dominio.modelo.Fotografia;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstadoCivil;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGenero;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGrupoSanguineo;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioTallaCamisa;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.entidad.TipoContratoEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "usuario",
        uniqueConstraints={@UniqueConstraint(
                name = "uq_correo_empresarial",
                columnNames={"correoEmpresarial"})
        })
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioEntity implements IdEntidad<String> {

    public UsuarioEntity(String id){
        this.id = id;
    }

    public interface Atributos extends IdEntidad.Atributos {
        String CORREO_EMPRESARIAL = "correoEmpresarial";
        String IDENTIFICACION = "identificacion";
        String NOMBRES = "nombres";
        String APELLIDOS = "apellidos";
        String VICEPRESIDENCIA = "vicepresidencia";
        String CLIENTE = "cliente";
        String FECHA_INGRESO = "fechaIngreso";
        String DIAS_GANADOS = "diasGanados";
        String DIAS_TOMADOS = "diasTomados";
        String GOOGLE_ID = "googleId";
        String CORREO_PERSONAL = "correoPersonal";
        String TELEFONO_FIJO = "telefonoFijo";
        String TELEFONO_CELULAR = "telefonoCelular";
        String ID_NACIONALIDAD = "idNacionalidad";
        String ID_LUGAR_NACIMIENTO = "idLugarNacimiento";
        String ID_LUGAR_RECIDENCIA = "idLugarRecidencia";
        String FECHA_NACIMIENTO = "fechaNacimiento";
        String DIRECCION = "direccion";
        String NOMBRE_CONTACTO_EMERGENCIA = "nombreContactoEmergencia";
        String PARENTES_COCONTACTO_EMERGENCIA = "parentescoContactoEmergencia";
        String TELEFONO_CONTACTO_EMERGENCIA = "telefonoContactoEmergencia";
        String TARJETA_PROFESIONAL = "tarjetaProfesional";
        String PERFIL_PROFESIONAL = "perfilProfesional";
        String CONOCIMIENTOS_TECNICOS = "conocimientosTecnicos";
        String TALLA_CAMISA = "tallaCamisa";
        String ESTADO_CIVIL = "estadoCivil";
        String GENERO = "genero";
        String GRUPO_SANGUINEO = "grupoSanguineo";
        String ESTADO = "estado";
        String ID_TIPODOCUMENTO = "idTipoDocumento";
        String ID_PROFESION = "idProfesion";
        String FECHA_ULTIMA_ACTUALIZACION = "fechaUltimaActualizacion";
        String FECHA_CREACION = "fechaCreacion";
        String CAPACIDADES = "capacidades";
        String ID_VICEPRESIDENCIA = "idVicepresidencia";
        String CARGO = "cargo";
        String CAPACIDAD = "capacidad";
        String FOTOGRAFIAS = "fotografias";
        String ACEPTACION_TERMINOS = "aceptacionTerminos";
        String BARRA_PROGRESO = "barraProgreso";
        String NOMBRE_PREFERENCIA = "nombrePreferencia";
        String PASAPORTE = "pasaporte";
        String ESTRATO = "estrato";
        String SEMESTRE = "semestre";
        String EMPRESA = "empresa";
        String PROFESION = "profesion";
        String FECHA_INICIO_CONTRATO = "fechaInicioContrato";
        String USUARIODETALLE = "usuarioDetalle";
        String PROYECTOS_EEUU = "proyectosEEUU";
        String VISA = "visa";
        String TOTAL_ANOS_EXPERIENCIA_LABORAL = "totalAnosExperienciaLaboral";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    @Column(nullable = false)
    private String correoEmpresarial;
    private String identificacion;
    private String profesion;
    private String nombres;
    private String apellidos;
    private String cliente;
    private LocalDate fechaIngreso;
    private double diasGanados;
    private int diasTomados;
    private String googleId;
    private String correoPersonal;
    private String telefonoFijo;
    private String telefonoCelular;
    private String idNacionalidad;
    private String idLugarNacimiento;
    @Column(name = "id_lugar_residencia")
    private String idLugarRecidencia;
    private LocalDate fechaNacimiento;
    private String direccion;
    private String nombreContactoEmergencia;
    private String parentescoContactoEmergencia;
    private String telefonoContactoEmergencia;
    private String tarjetaProfesional;
    private String perfilProfesional;
    private String conocimientosTecnicos;
    @Enumerated(value = EnumType.STRING)
    private UsuarioTallaCamisa tallaCamisa;
    @Enumerated(value = EnumType.STRING)
    private UsuarioEstadoCivil estadoCivil;
    @Enumerated(value = EnumType.STRING)
    private UsuarioGenero genero;
    @Enumerated(value = EnumType.STRING)
    private UsuarioGrupoSanguineo grupoSanguineo;
    @Enumerated(value = EnumType.STRING)
    private UsuarioEstado estado;
    private Long idTipoDocumento;
    private Long idProfesion;
    private LocalDate fechaCreacion;
    private LocalDate fechaUltimaActualizacion;
    private String cargo;
    private String idVicepresidencia;
    private String empresa;
    private String contraseniaComputador;
    @Column(name="proyectosEEUU")
    private Boolean proyectosEEUU;
    @Column(name="visa")
    private Boolean visa;
    @Column(name="total_anos_experiencia_laboral")
    private int totalAnosExperienciaLaboral;

    @ElementCollection(targetClass = Long.class, fetch = FetchType.LAZY)
    @CollectionTable(
            name = "usuario_entity_capacidad",
            joinColumns=@JoinColumn(name = "usuario_id",
                    referencedColumnName = "id",
                    foreignKey = @ForeignKey(name = "fk_usuario_entity_capacidad_usuario_usuario_id"))
    )
    @Column(name="capacidad")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Long> capacidad;

    @JsonManagedReference
    @OneToMany(mappedBy = "usuarioEntity", fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<FotografiaEntity> fotografias;

    @Column(name = "aceptacion_de_terminos")
    private boolean aceptacionTerminos;

    @Column(name = "barra_de_progreso", columnDefinition = "Decimal(10,2) default'0.0'")
    private double barraProgreso;

    @Column(name = "nombre_preferencia")
    private String nombrePreferencia;

    @Column(name = "pasaporte")
    private String pasaporte;
    @Column(name = "estrato")
    private int estrato;
    @Column(name = "semestre")
    private String semestre;

    @Column(name = "estado_profesion")
    @Enumerated(EnumType.STRING)
    private EstadoProfesion estadoProfesion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipo_contrato_id", foreignKey = @ForeignKey(name = "fk_usuario_tipo_contrato_tipo_contrato_id"))
    private TipoContratoEntity tipoContrato;

    private LocalDate fechaInicioContrato;

    @JsonManagedReference
    @OneToMany(mappedBy = "usuario",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
    private List<UsuarioDetalleEntity> usuarioDetalle;

    @JsonIgnore
    @Transient
    public List<Fotografia> getListFotografiaModel() {
        if (this.fotografias == null || this.fotografias.isEmpty()) {
            return null;
        }
        List<Fotografia> listaFotografiaModel = new ArrayList();
        for (FotografiaEntity fotoEntity : this.fotografias) {
            Fotografia fotografiasModel = new Fotografia(
                    fotoEntity.getId(),
                    fotoEntity.getContenido(),
                    fotoEntity.getTipoFotografia()
            );
            listaFotografiaModel.add(fotografiasModel);
        }
        return listaFotografiaModel;

    }


}