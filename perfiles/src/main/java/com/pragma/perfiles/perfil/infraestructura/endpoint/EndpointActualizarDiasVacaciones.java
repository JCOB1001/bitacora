package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarDiasVacacionesComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarDiasVacaciones;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarDiasVacaciones {

    private final ManejadorActualizarDiasVacaciones manejadorActualizarDiasVacaciones;

    @PutMapping("/actualizar-dias/{idUsuario}")
    public ObjetoRespuesta<String> actualizarDiasVacaciones(@NotNull @PathVariable String idUsuario,
                                                            @NotNull @RequestBody ActualizarDiasVacacionesComando actualizarDiasVacacionesComando){
        manejadorActualizarDiasVacaciones.ejecutar(idUsuario, actualizarDiasVacacionesComando);
        return new ObjetoRespuesta<>(HttpStatus.OK, "Actualizado exitosamente");
    }

}
