package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.perfil.aplicacion.comando.GuardarFotografiaUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioConFotografias;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorGuardarFotografiaUsuario {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaUsuario fabricaUsuario;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public ObjetoRespuesta<UsuarioConFotografias> ejecutar(GuardarFotografiaUsuarioComando guardarFotografiaUsuarioComando){
        Usuario usuarioConFotografia = fabricaUsuario.guardarFotografiaUsuario(guardarFotografiaUsuarioComando);
        usuarioCrudUseCase.actualizarConFoto(usuarioConFotografia);

        usuarioConFotografia.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuarioConFotografia.getId()));
        usuarioConFotografia.actualizarFechaUltimaActualizacion(LocalDate.now());

        usuarioConFotografia = usuarioCrudUseCase.findByCorreoEmpresarial(guardarFotografiaUsuarioComando.getCorreoEmpresarial());
        return new ObjetoRespuesta<UsuarioConFotografias>(UsuarioConFotografias
                .builder()
                .userId(usuarioConFotografia.getId())
                .correoEmpresarial(usuarioConFotografia.getCorreoEmpresarial())
                .fotografias(usuarioConFotografia.getFotografias())
                .build());
    }

}
