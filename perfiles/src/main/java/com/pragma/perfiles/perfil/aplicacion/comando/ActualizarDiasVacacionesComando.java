package com.pragma.perfiles.perfil.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarDiasVacacionesComando {

    private double diasGanados;
    private int diasTomados;

}
