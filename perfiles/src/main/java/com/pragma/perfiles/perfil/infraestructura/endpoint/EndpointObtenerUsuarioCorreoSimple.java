package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioCorreoSimple;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioInfo;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerUsuarioCorreoSimple {

    private final ManejadorObtenerUsuarioCorreoSimple manejadorObtenerUsuarioPorCorreo;

    @GetMapping("/correoSimple")
    public ObjetoRespuesta<UsuarioInfo> ejecutarObtenerUsuarioPorCorreoSimple(@RequestParam("correo") String correo) throws Exception {

        ObjetoRespuesta<UsuarioInfo> objetoRespuestaUsuario = manejadorObtenerUsuarioPorCorreo.ejecutar(correo);

        if(objetoRespuestaUsuario.getDato() == null){
            throw new UsuarioNoEncontrado("No existe un usuario con este correo");
        }

        return new ObjetoRespuesta<UsuarioInfo>(HttpStatus.OK,
                objetoRespuestaUsuario.getDato());
    }

}
