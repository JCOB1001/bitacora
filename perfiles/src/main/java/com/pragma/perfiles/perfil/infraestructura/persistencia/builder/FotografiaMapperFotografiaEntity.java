package com.pragma.perfiles.perfil.infraestructura.persistencia.builder;

import com.pragma.perfiles.perfil.dominio.modelo.Fotografia;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.FotografiaEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface FotografiaMapperFotografiaEntity extends ConvertidorBase<Fotografia, FotografiaEntity> {}
