package com.pragma.perfiles.perfil.dominio.modelo;

public enum UsuarioEstadoCivil {

    SOLTERO_A("SOLTERO/A"), CASADO_A("CASADO/A"), UNION_MARITAL_DE_HECHO("UNION MARITAL DE HECHO"), NO_ESPECIFICADO("No especificado");
    private String usuarioEstadoCivil;

    private UsuarioEstadoCivil(String usuarioEstadoCivil){
        this.usuarioEstadoCivil = usuarioEstadoCivil;
    }
    public String getUsuarioEstadoCivil() {
        return usuarioEstadoCivil;
    }
}
