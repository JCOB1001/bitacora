package com.pragma.perfiles.perfil.infraestructura.persistencia.dao;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioDetalleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioDetalleDao extends JpaRepository<UsuarioDetalleEntity, String>, UsuarioDetalleCustomDao {

    public UsuarioDetalleEntity findByUsuarioIdAndIdiomaId(String usuarioId,String idiomaId);
}
