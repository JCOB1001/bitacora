package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioLista {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Stream<Usuario>> obtenerUsuarios(){
        Stream<Usuario> usuarioStream = usuarioCrudUseCase.obtenerTodos();
        return new ObjetoRespuesta<Stream<Usuario>>(usuarioStream);
    }
}
