package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioNuevoPragmaticoComando;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface InformacionNominaMapperActualizarUsuarioNuevoPragmatico extends ConvertidorBase<InformacionNomina, ActualizarUsuarioNuevoPragmaticoComando> {

    @Named("rightToLeft")
    @Mapping(target = "salarioBasicoOrdinario", ignore = true)
    @Mapping(target = "salarioBasicoIntegral", ignore = true)
    @Mapping(target = "noConstitutivosSalario", ignore = true)
    InformacionNomina rightToLeft(ActualizarUsuarioNuevoPragmaticoComando actualizarUsuarioNuevoPragmaticoComando);

    @AfterMapping
    default void setSalarios(@MappingTarget InformacionNomina informacionNomina, ActualizarUsuarioNuevoPragmaticoComando actualizarUsuarioNuevoPragmaticoComando) {

        if(actualizarUsuarioNuevoPragmaticoComando.getNoConstitutivosSalario().isBlank()){
            informacionNomina.setNoConstitutivosSalario(Double.valueOf(0));
        }
        else{
            informacionNomina.setNoConstitutivosSalario(Double.valueOf(actualizarUsuarioNuevoPragmaticoComando.getNoConstitutivosSalario().replace(".", "")));
        }

        if(actualizarUsuarioNuevoPragmaticoComando.getSalarioBasicoIntegral().isBlank()){
            informacionNomina.setSalarioBasicoIntegral(Double.valueOf(0));
        }
        else{
            informacionNomina.setSalarioBasicoIntegral(Double.valueOf(actualizarUsuarioNuevoPragmaticoComando.getSalarioBasicoIntegral().replace(".", "")));
        }

        if(actualizarUsuarioNuevoPragmaticoComando.getSalarioBasicoOrdinario().isBlank()){
            informacionNomina.setSalarioBasicoOrdinario(Double.valueOf(0));
        }
        else{
            informacionNomina.setSalarioBasicoOrdinario(Double.valueOf(actualizarUsuarioNuevoPragmaticoComando.getSalarioBasicoOrdinario().replace(".", "")));
        }


    }
}
