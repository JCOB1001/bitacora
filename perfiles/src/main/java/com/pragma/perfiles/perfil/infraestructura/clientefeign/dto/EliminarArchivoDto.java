package com.pragma.perfiles.perfil.infraestructura.clientefeign.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class EliminarArchivoDto implements Serializable {

    private String fullPath;

}
