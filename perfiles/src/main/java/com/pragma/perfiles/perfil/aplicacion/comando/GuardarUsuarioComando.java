package com.pragma.perfiles.perfil.aplicacion.comando;

import lombok.Data;

@Data
public class GuardarUsuarioComando {

    private String correoEmpresarial;
    private double diasGanados;
    private int diasTomados;


}
