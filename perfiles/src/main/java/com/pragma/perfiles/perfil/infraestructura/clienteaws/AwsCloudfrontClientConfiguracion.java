package com.pragma.perfiles.perfil.infraestructura.clienteaws;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Data
@Component
@ConfigurationProperties(prefix = "aws.cloudfront")
public class AwsCloudfrontClientConfiguracion {

    private String accessKeyId;
    private String secretAccessKey;
    private String distributionId;

}
