package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarUsuarioPorId {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public void ejecutar(String idUsuario) {
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no se encuentra registrado en la base de datos");
        }
        usuarioCrudUseCase.eliminarPorId(idUsuario);
    }

}
