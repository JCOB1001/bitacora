package com.pragma.perfiles.perfil.aplicacion.comando;

import lombok.Data;

@Data
public class ActualizarUsuarioEstadoPragmadexComando {

    private String corporateMail;
    private boolean status;

}
