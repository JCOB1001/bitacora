package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarFotografiaUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.comando.GuardarFotografiaUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioConFotografias;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarFotografiaUsuario;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorGuardarFotografiaUsuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarFotografiaUsuario {

    private final ManejadorActualizarFotografiaUsuario manejadorActualizarFotografiaUsuario;

    @PutMapping("/fotografia")
    public ObjetoRespuesta<UsuarioConFotografias> ejecutarGuardarFotografiaUsuario(@NotNull @RequestBody ActualizarFotografiaUsuarioComando actualizarFotografiaUsuarioComando){
        return manejadorActualizarFotografiaUsuario.ejecutar(actualizarFotografiaUsuarioComando);
    }

}
