package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.GuardarFotografiaUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioConFotografias;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorGuardarFotografiaUsuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarFotografiaUsuario {

    private final ManejadorGuardarFotografiaUsuario manejadorGuardarFotografiaUsuario;

    @PostMapping("/fotografia")
    public ObjetoRespuesta<UsuarioConFotografias> ejecutarGuardarFotografiaUsuario(@NotNull @RequestBody GuardarFotografiaUsuarioComando guardarFotografiaUsuarioComando){
        return manejadorGuardarFotografiaUsuario.ejecutar(guardarFotografiaUsuarioComando);
    }

}
