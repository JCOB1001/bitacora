package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerUsuariosPorVicepresidencia {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<List<Usuario>> ejecutar(String idViceprecidencia){
        List<Usuario> usuarios = usuarioCrudUseCase.findByIdVicepresidencia(idViceprecidencia);

        return new ObjetoRespuesta<List<Usuario>>(HttpStatus.OK, usuarios);
    }
}
