package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioContrato;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioContratoPorUsuarioId {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<UsuarioContrato> ejecutar(String usuarioId) {

        Usuario usuario = usuarioCrudUseCase.findById(usuarioId);

        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario no se encuentra en registrado");
        }
        return new ObjetoRespuesta<UsuarioContrato>(new UsuarioContrato(usuario.getId(),usuario.getTipoContratoId(), usuario.getFechaInicioContrato()));
    }
}
