package com.pragma.perfiles.perfil.dominio.error;

public class ListaImagenesIncompleta extends RuntimeException {

    public ListaImagenesIncompleta(String mensaje) {
        super(mensaje);
    }

}

