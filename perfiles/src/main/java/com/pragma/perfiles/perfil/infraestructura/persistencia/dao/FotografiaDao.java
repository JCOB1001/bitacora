package com.pragma.perfiles.perfil.infraestructura.persistencia.dao;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.FotografiaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FotografiaDao extends JpaRepository<FotografiaEntity,String> {}
