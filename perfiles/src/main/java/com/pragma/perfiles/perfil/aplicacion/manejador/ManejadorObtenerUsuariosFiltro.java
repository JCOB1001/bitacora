package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuariosFiltroComando;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCaseFiltro;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerUsuariosFiltro {

    private final UsuarioCrudUseCaseFiltro usuarioCrudUseCaseFiltro;
    private final FabricaUsuario fabricaUsuario;

    public ObjetoRespuestaUsuario ejecutar(ObtenerUsuariosFiltroComando obtenerUsuariosFiltroComando) {
        FiltroUsuario filtroUsuario = fabricaUsuario.cargarFiltroUsuario(obtenerUsuariosFiltroComando);
        return usuarioCrudUseCaseFiltro.obtenerTodos(filtroUsuario);
    }

}
