package com.pragma.perfiles.perfil.aplicacion.comando;

import lombok.Data;

@Data
public class ActualizarUsuarioNuevoPragmaticoComando {
    String tipoIdentificacion;
    String identificacion;
    String nombres;
    String apellidos;
    String nombrePreferencia;
    String area;
    String cargoNomina;
    String chapter;
    String tecnologiaPrincipal;
    String clasificacion;
    String ciudad;
    String departamento;
    String pais;
    String correoPersonal;
    String sexo;
    String referido;
    String tipoContrato;
    String fechaInicioContratoActual;
    String fechaNacimiento;
    String salarioBasicoOrdinario;
    String salarioBasicoIntegral;
    String noConstitutivosSalario;
    String moneda;
    String profesion;
    String tarjetaProfesional;
    String pasaporte;
    String cliente;
    String proyecto;
    String fechaInicioRelacion;
    String estadoPeriodoPrueba;
    String evaluadorPeriodoPrueba;
    String empresa;
}
