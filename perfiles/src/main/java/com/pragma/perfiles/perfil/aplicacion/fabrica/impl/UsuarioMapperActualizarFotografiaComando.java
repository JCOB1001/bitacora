package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarFotografiaUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.comando.GuardarFotografiaUsuarioComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioMapperActualizarFotografiaComando extends ConvertidorBase<Usuario, ActualizarFotografiaUsuarioComando> {}
