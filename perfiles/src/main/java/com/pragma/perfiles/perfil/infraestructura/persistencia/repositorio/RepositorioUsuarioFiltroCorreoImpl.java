package com.pragma.perfiles.perfil.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuarioCorreo;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseFiltroImpl;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorioFiltroCorreo;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.UsuarioMapperUsuarioEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.dao.UsuarioDao;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class RepositorioUsuarioFiltroCorreoImpl
        extends CrudRepositorioBaseFiltroImpl<String, Usuario, UsuarioEntity, FiltroUsuarioCorreo, ObjetoRespuestaUsuario>
        implements UsuarioRepositorioFiltroCorreo {

    private final UsuarioDao usuarioDao;
    private final UsuarioMapperUsuarioEntity usuarioMapperUsuarioEntity;

    @Override
    protected UsuarioDao obtenerRepositorio() {
        return usuarioDao;
    }

    @Override
    protected UsuarioMapperUsuarioEntity obtenerConversionBase() {
        return usuarioMapperUsuarioEntity;
    }

    @Override
    protected ObjetoRespuestaUsuario buscarPorFiltro(FiltroUsuarioCorreo filtro) {
        return usuarioDao.buscarTodosFiltrandoCorreo(filtro);
    }

}
