package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioRetiradoComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorActualizarUsuarioRetirado;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Valid
public class EndpointActualizarUsuarioRetirado {
    private final ManejadorActualizarUsuarioRetirado manejadorActualizarUsuarioRetirado;

    @PutMapping("/retiro")
    public RespuestaBase ejecutarActualizarUsuarioRetirado(@NotNull @RequestBody ActualizarUsuarioRetiradoComando actualizarUsuarioRetiradoComando){
        manejadorActualizarUsuarioRetirado.ejecutar(actualizarUsuarioRetiradoComando);
        return RespuestaBase.ok("Actualizado Exitosamente");
    }
}
