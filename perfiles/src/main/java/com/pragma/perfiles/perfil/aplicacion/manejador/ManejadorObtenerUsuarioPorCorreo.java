package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioPorCorreo {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Usuario> ejecutar(String correoEmpresarialUsuario) throws Exception{
        Usuario usuario = usuarioCrudUseCase.findByCorreoEmpresarial(correoEmpresarialUsuario);
        return new ObjetoRespuesta<Usuario>(HttpStatus.OK, usuario);
    }
}
