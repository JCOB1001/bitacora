package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioPorTokenConsulta;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioMapperObtenerUsuarioPorTokenCorreo extends ConvertidorBase<Usuario, UsuarioPorTokenConsulta> {
}
