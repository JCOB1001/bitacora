package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioPorTokenIdentificacionConsulta;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioMapperObtenerUsuarioPorTokenIdentificacion extends ConvertidorBase<Usuario, UsuarioPorTokenIdentificacionConsulta> {
}
