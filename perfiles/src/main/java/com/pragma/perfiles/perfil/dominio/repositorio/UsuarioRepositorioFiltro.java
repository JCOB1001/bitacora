package com.pragma.perfiles.perfil.dominio.repositorio;

import com.pragma.vacaciones.common.servicio.CrudRepositorioBaseFiltro;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;

public interface UsuarioRepositorioFiltro extends CrudRepositorioBaseFiltro<String, Usuario, FiltroUsuario, ObjetoRespuestaUsuario> {
}
