package com.pragma.perfiles.perfil.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.comun.infraestructura.seguridad.entity.UsuarioEntity;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.builder.EstudioDetalleMapperEstudioDetalleEntity;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioDetalle;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioDetalleRepositorio;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.UsuarioDetalleMapperUsuarioDetalleEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.dao.UsuarioDetalleDao;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioDetalleEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
@Log4j2
public class RepositorioUsuarioDetalleImpl extends CrudRepositorioBaseImpl<String, UsuarioDetalle, UsuarioDetalleEntity> implements UsuarioDetalleRepositorio {

    private final UsuarioDetalleDao usuarioDetalleDao;
    private final UsuarioDetalleMapperUsuarioDetalleEntity usuarioDetalleMapperUsuarioDetalleEntity;

    @Override
    protected UsuarioDetalleDao obtenerRepositorio() {
        return usuarioDetalleDao;
    }

    @Override
    protected UsuarioDetalleMapperUsuarioDetalleEntity obtenerConversionBase(){
        return usuarioDetalleMapperUsuarioDetalleEntity;
    }

    @Override
    public UsuarioDetalle findByUsuarioIdAndIdiomaId(String usuarioId,String idiomaId) {
        return obtenerConversionBase().rightToLeft(usuarioDetalleDao.findByUsuarioIdAndIdiomaId(usuarioId,idiomaId));
    }
}
