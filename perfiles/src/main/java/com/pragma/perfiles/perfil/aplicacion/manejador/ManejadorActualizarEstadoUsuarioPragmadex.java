package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioEstadoPragmadexComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorActualizarEstadoUsuarioPragmadex {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public void ejecutar(ActualizarUsuarioEstadoPragmadexComando actualizarUsuarioEstadoPragmadexComando){

        Usuario usuarioParaActualizar = usuarioCrudUseCase.findByCorreoEmpresarial(actualizarUsuarioEstadoPragmadexComando.getCorporateMail());
        if(usuarioParaActualizar == null){
            throw new UsuarioNoEncontrado("No se encontro el usuario");
        }

        UsuarioEstado usuarioEstado = (actualizarUsuarioEstadoPragmadexComando.isStatus() == true) ? UsuarioEstado.ACTIVO : UsuarioEstado.INACTIVO;
        usuarioParaActualizar.setEstado(usuarioEstado);

        usuarioCrudUseCase.actualizar(usuarioParaActualizar);
    }

}
