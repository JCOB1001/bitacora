package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuarioFiltroCorreoComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioPorTokenIdentificacionConsulta;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuarioCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCaseFiltroCorreo;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioPorToken {

    private final UsuarioCrudUseCaseFiltroCorreo usuarioCrudUseCaseFiltroCorreo;
    private final FabricaUsuario fabricaUsuario;

    public ObjetoRespuesta<UsuarioPorTokenIdentificacionConsulta> ejecutar(ObtenerUsuarioFiltroCorreoComando obtenerUsuarioFiltroCorreoComando){
        FiltroUsuarioCorreo filtroUsuarioCorreo = fabricaUsuario.cargarFiltroUsuarioCorreo(obtenerUsuarioFiltroCorreoComando);
        ObjetoRespuestaUsuario objetoRespuestaUsuario = usuarioCrudUseCaseFiltroCorreo.obtenerTodos(filtroUsuarioCorreo);

        if(objetoRespuestaUsuario.getDato() == null){
            throw new UsuarioNoEncontrado("No se ha encontrado el usuario");
        }

        Stream<Usuario> streamUsuario = (Stream<Usuario>) objetoRespuestaUsuario.getDato();
        Optional<Usuario> optionalUsuario = streamUsuario.findFirst();

        if(optionalUsuario.isEmpty()){
            throw new UsuarioNoEncontrado("No se ha encontrado el usuario");
        }

        return new ObjetoRespuesta<>(fabricaUsuario.obtenerUsuarioPorTokenIdentificacionConsulta(optionalUsuario.get()));
    }

}
