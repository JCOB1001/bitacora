package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.UsuarioMapperUsuarioEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.dao.UsuarioDao;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorActualizarUsuarioPorCorreo {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public void ejecutar(String correo, ActualizarUsuarioComando actualizarUsuarioComando) {

        Usuario usuario = usuarioCrudUseCase.findByCorreoEmpresarial(correo);

        if (usuario == null) {
            throw new UsuarioNoEncontrado("El usuario con el correo : " + correo+ " no existe en el sistema");
        }

        usuario.setNombres(actualizarUsuarioComando.getNombres());
        usuario.setIdentificacion(actualizarUsuarioComando.getIdentificacion());
        usuario.setApellidos(actualizarUsuarioComando.getApellidos());
        usuario.setCorreoPersonal(actualizarUsuarioComando.getCorreoPersonal());
        usuario.setTelefonoFijo(actualizarUsuarioComando.getTelefonoFijo());
        usuario.setTelefonoCelular(actualizarUsuarioComando.getTelefonoCelular());
        usuario.setIdNacionalidad(actualizarUsuarioComando.getIdNacionalidad());
        usuario.setIdLugarNacimiento(actualizarUsuarioComando.getIdLugarNacimiento());
        usuario.setIdLugarRecidencia(actualizarUsuarioComando.getIdLugarRecidencia());
        usuario.setFechaNacimiento(actualizarUsuarioComando.getFechaNacimiento());
        usuario.setDireccion(actualizarUsuarioComando.getDireccion());
        usuario.setNombreContactoEmergencia(actualizarUsuarioComando.getNombreContactoEmergencia());
        usuario.setParentescoContactoEmergencia(actualizarUsuarioComando.getParentescoContactoEmergencia());
        usuario.setTelefonoContactoEmergencia(actualizarUsuarioComando.getTelefonoContactoEmergencia());
        usuario.setTallaCamisa(actualizarUsuarioComando.getTallaCamisa());
        usuario.setEstadoCivil(actualizarUsuarioComando.getEstadoCivil());
        usuario.setGenero(actualizarUsuarioComando.getGenero());
        usuario.setGrupoSanguineo(actualizarUsuarioComando.getGrupoSanguineo());
        usuario.setIdTipoDocumento(actualizarUsuarioComando.getIdTipoDocumento());
        usuario.actualizarFechaUltimaActualizacion(LocalDate.now());
        usuario.setContraseniaComputador(actualizarUsuarioComando.getContraseniaComputador());

        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuario.setEstrato(actualizarUsuarioComando.getEstrato());

        usuarioCrudUseCase.actualizar(usuario);
    }
}

