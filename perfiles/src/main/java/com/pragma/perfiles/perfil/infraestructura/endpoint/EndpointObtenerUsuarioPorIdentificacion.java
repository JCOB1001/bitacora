package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorIdentificacion;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerUsuarioPorIdentificacion {

    private final ManejadorObtenerUsuarioPorIdentificacion manejadorObtenerUsuarioPorIdentificacion;

    @GetMapping("/{tipoDocumento}/{identificacion}")
    public ObjetoRespuesta<Usuario> ejecutarObtenerUsuarioPorIdentificacion(
            @NotNull @PathVariable(value = "tipoDocumento") Long idTipoDocumento,
            @NotNull @PathVariable(value = "identificacion") String identificacionUsuario
    ) {
        Usuario usuario = manejadorObtenerUsuarioPorIdentificacion.ejecutar(idTipoDocumento, identificacionUsuario);
        ObjetoRespuesta<Usuario> objetoRespuestaUsuario = new ObjetoRespuesta<>(usuario);

        return new ObjetoRespuesta<>(
                HttpStatus.OK,
                objetoRespuestaUsuario.getDato()
        );
    }

}
