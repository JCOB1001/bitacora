package com.pragma.perfiles.perfil.infraestructura.persistencia.builder;

import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioDetalleEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.*;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface UsuarioMapperUsuarioEntity extends ConvertidorBase<Usuario, UsuarioEntity> {

    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "tipoContratoId", target = "tipoContrato.id")
    )
    UsuarioEntity leftToRight(Usuario usuario);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "tipoContrato.id", target = "tipoContratoId")
    })
    Usuario rightToLeft(UsuarioEntity usuarioEntity);

    @AfterMapping
    default void afterRightToLeft(@MappingTarget Usuario usuario, UsuarioEntity usuarioEntity) {
        if(usuarioEntity.getUsuarioDetalle()!=null){
            List<UsuarioDetalleEntity> usuarioDetalle = usuarioEntity.getUsuarioDetalle().stream().filter(item->
                    item.getIdioma().getId().equals(HttpRequestContextHolder.getIdioma())).collect(Collectors.toList());
            if(usuarioDetalle.isEmpty()) {
                usuarioDetalle.add(new UsuarioDetalleEntity());
            }
                usuario.setPerfilProfesional(usuarioDetalle.get(0).getPerfilProfesional());
                usuario.setConocimientosTecnicos(usuarioDetalle.get(0).getConocimientosTecnicos());
                usuario.setProfesion(usuarioDetalle.get(0).getProfesion());
                usuario.setCargo(usuarioDetalle.get(0).getCargo());
        }
    }

}
