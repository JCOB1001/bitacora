package com.pragma.perfiles.perfil.aplicacion.comando;

import com.pragma.perfiles.perfil.dominio.modelo.Fotografia;
import lombok.Data;

import java.util.List;

@Data
public class ActualizarFotografiaUsuarioComando {

    private String correoEmpresarial;
    private String userId;
    private List<Fotografia> fotografias;

}
