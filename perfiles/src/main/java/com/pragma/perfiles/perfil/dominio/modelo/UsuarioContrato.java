package com.pragma.perfiles.perfil.dominio.modelo;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class UsuarioContrato {

    private String usuarioId;
    private String tipoContratoId;
    private LocalDate fechaInicio;
    public UsuarioContrato(String usuarioId,
                           String tipoContratoId,
                           LocalDate fechaInicio){
        this.usuarioId = usuarioId;
        this.tipoContratoId = tipoContratoId;
        this.fechaInicio = fechaInicio;
    }
}
