package com.pragma.perfiles.perfil.infraestructura.configuracion;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "grupo-token-externo")
@Data
@NoArgsConstructor
public class GrupoAnticiposExternoPropiedades {
    private String nombreGroupPool;
}
