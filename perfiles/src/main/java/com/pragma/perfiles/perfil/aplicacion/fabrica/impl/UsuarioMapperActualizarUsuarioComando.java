package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioMapperActualizarUsuarioComando extends ConvertidorBase<Usuario, ActualizarUsuarioComando> {

}
