package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioPorNombre {

    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<List<Usuario>> ejecutar(String nombre){
        List<Usuario> usuarioStream = usuarioCrudUseCase.buscarPorNombreCorreo(nombre);
        return new ObjetoRespuesta<List<Usuario>>(usuarioStream);
    }
}
