package com.pragma.perfiles.perfil.infraestructura.persistencia.dao;

import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface UsuarioDao extends JpaRepository<UsuarioEntity, String>, UsuarioCustomDao {

    public UsuarioEntity findByIdentificacion(String identificacion);
    public UsuarioEntity findByCorreoEmpresarial(String correoEmpresarial);

    public UsuarioEntity findByCorreoPersonal(String correoPersonal);

    public UsuarioEntity findByGoogleId(String googleId);

    public List<UsuarioEntity> findByIdVicepresidencia(String idViceprecidencia);

    public List<UsuarioEntity> findByEstado(UsuarioEstado estado);

    Iterable<UsuarioEntity> findByCorreoEmpresarialIn(List<String> correos);

    Iterable<UsuarioEntity> findByIdIn(Collection<String> idLista);
}
