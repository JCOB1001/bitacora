package com.pragma.perfiles.perfil.aplicacion.fabrica.impl;

import com.pragma.perfiles.perfil.aplicacion.comando.GuardarUsuarioNuevoComando;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioMapperGuardarUsuarioNuevoComando extends ConvertidorBase<Usuario, GuardarUsuarioNuevoComando> {
}
