package com.pragma.perfiles.perfil.dominio.modelo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;
import com.pragma.perfiles.comun.infraestructura.error.EnumNoEncontrado;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum UsuarioGenero {


    MASCULINO("MASCULINO"), FEMENINO("FEMENINO");

    private String genero;

    @JsonValue
    public String getValue() {
        return getGenero();
    }

    @JsonCreator
    public static UsuarioGenero fromValue(String value) {
        if (value != null && value.isEmpty()) {
            return null;
        }
        for (UsuarioGenero p : values()) {
            if (p.getValue().equalsIgnoreCase(value)) {
                return p;
            }
        }
        return MASCULINO;
    }
    private UsuarioGenero(String genero) {
        this.genero=genero;
    }

    public String getGenero() {
        return genero;
    }
}
