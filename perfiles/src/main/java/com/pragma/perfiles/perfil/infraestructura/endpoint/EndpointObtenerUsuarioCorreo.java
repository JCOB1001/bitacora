package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerUsuarioPorCorreo;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor
@Validated
//@PreAuthorize("hasAuthority('Admin')")
public class EndpointObtenerUsuarioCorreo {

    private final ManejadorObtenerUsuarioPorCorreo manejadorObtenerUsuarioPorCorreo;

    @GetMapping("/correo")
    public ObjetoRespuesta<Usuario> ejecutarObtenerUsuarios( @RequestParam("correo") String correo) throws Exception {

        ObjetoRespuesta<Usuario> objetoRespuestaUsuario = manejadorObtenerUsuarioPorCorreo.ejecutar(correo);

        if(objetoRespuestaUsuario.getDato() == null){
            throw new UsuarioNoEncontrado("No existe un usuario con este correo");
        }

        return new ObjetoRespuesta<Usuario>(HttpStatus.OK,
                objetoRespuestaUsuario.getDato());
    }

}
