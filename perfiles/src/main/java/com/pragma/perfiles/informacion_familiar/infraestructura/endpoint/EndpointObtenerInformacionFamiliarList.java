package com.pragma.perfiles.informacion_familiar.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorObtenerInformacionFamiliarList;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("informacion-familiar")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerInformacionFamiliarList {

    private final ManejadorObtenerInformacionFamiliarList manejadorObtenerInformacionFamiliarList;

    @GetMapping
    public ObjetoRespuesta<Stream<InformacionFamiliar>> ejecutarObtenerInformacionFamiliarList(){
        ObjetoRespuesta<Stream<InformacionFamiliar>> informacionFamiliarList = manejadorObtenerInformacionFamiliarList.ejecutar();
        if(informacionFamiliarList.getDato() == null){
            throw new ExcepcionesInformacionFamiliar("No existen registro de información familiar");
        }
        return new ObjetoRespuesta<Stream<InformacionFamiliar>>(HttpStatus.OK, informacionFamiliarList.getDato());
    }

}
