package com.pragma.perfiles.informacion_familiar.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerInformacionFamiliarPorIdUsuario {

    private final InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase;

    public Stream<InformacionFamiliar> ejecutar(String idUsuario){
        Stream<InformacionFamiliar> informacionFamiliarStream = informacionFamiliarCrudUseCase.findByIdUsuario(idUsuario);
        return informacionFamiliarStream;
    }

}
