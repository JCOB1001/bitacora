package com.pragma.perfiles.informacion_familiar.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.aplicacion.comando.ActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorActualizarInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("informacion-familiar")
@RequiredArgsConstructor
@Validated
public class EndPointActualizarInformacionFamiliar {

    private final ManejadorActualizarInformacionFamiliar manejadorActualizarInformacionFamiliar;

    @PutMapping("/{idInformacionFamiliar}")
    public ObjetoRespuesta<InformacionFamiliar> ejecutarGuardarInformacionFamiliar(@NotNull @PathVariable String idInformacionFamiliar,
                                                                                   @NotNull @RequestBody ActualizarInformacionFamiliarComando
                                                                                           actualizarInformacionFamiliarComando){
        ObjetoRespuesta<InformacionFamiliar> informacionFamiliarObjetoRespuesta = manejadorActualizarInformacionFamiliar.ejecutar(idInformacionFamiliar,
                actualizarInformacionFamiliarComando);
        if(informacionFamiliarObjetoRespuesta.getDato() == null){
            throw new ExcepcionesInformacionFamiliar("No existe esta Informacion Familiar");
        }
        return new ObjetoRespuesta<InformacionFamiliar>(HttpStatus.OK, informacionFamiliarObjetoRespuesta.getDato());
    }


}
