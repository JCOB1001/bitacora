package com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.dao;

import com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.entidad.InformacionFamiliarEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface InformacionFamiliarDao extends JpaRepository<InformacionFamiliarEntity, String> {

    public Iterable<InformacionFamiliarEntity> findByUsuarioId(String idUsuario);

    Iterable<InformacionFamiliarEntity> findByUsuarioIdIn(Collection<String> idLista);
}
