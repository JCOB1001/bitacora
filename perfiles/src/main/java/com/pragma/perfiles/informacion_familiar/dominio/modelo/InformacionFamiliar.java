package com.pragma.perfiles.informacion_familiar.dominio.modelo;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;


@Getter
@Setter
public class InformacionFamiliar extends Modelo<String> {

    private String nombres;
    private String apellidos;
    private Genero genero;
    private LocalDate fechaNacimiento;
    private String idUsuario;
    private Parentesco parentesco;
    private boolean dependiente;

    public InformacionFamiliar(){
        super(null);
    }


    public InformacionFamiliar(String id, String nombres, String apellidos, LocalDate fechaNacimiento, Parentesco parentesco, Genero genero, String idUsuario, Boolean dependiente){
        super(id);
        validaciones(nombres, apellidos);
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.fechaNacimiento = fechaNacimiento;
        this.idUsuario = idUsuario;
        this.parentesco = parentesco;
        this.genero = genero;
        this.dependiente = dependiente;
    }

    private void validaciones(String nombres, String apellidos){
        validacionesNombres(nombres);
        validacionesApellidos(apellidos);
    }

    private void validacionesNombres(String nombres){
        if(nombres.length() < 2){
            throw new ExcepcionesInformacionFamiliar("El nombre debe tener al menos 2 letras");
        }
        for(int i = 0; i < nombres.length(); i++){
            if(!isLetra(nombres.charAt(i)))
                throw new ExcepcionesInformacionFamiliar("El nombre solo puede contener letras");
        }
    }

    private void validacionesApellidos(String apellidos){
        if(apellidos.length() < 2){
            throw new ExcepcionesInformacionFamiliar("El apellido tener al menos 2 letras");
        }
        for(int i = 0; i < apellidos.length(); i++){
            if(!isLetra(apellidos.charAt(i)))
                throw new ExcepcionesInformacionFamiliar("El apellido solo puede contener letras");
        }
    }


    private boolean isLetra(char c){
        if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
            return true;
        return false;
    }

}
