package com.pragma.perfiles.informacion_familiar.dominio.modelo;

import lombok.Data;

@Data
public class InformacionFamiliarConsultaPragmaData {

    private String nombres;
    private String apellidos;
    private String genero;
    private String fechaNacimiento;
    private String parentesco;
    private String dependiente;

}
