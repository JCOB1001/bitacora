package com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.repositorio.InformacionFamiliarRepositorio;
import com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.builder.InformacionFamiliarMapperInformacionFamiliarEntity;
import com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.dao.InformacionFamiliarDao;
import com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.entidad.InformacionFamiliarEntity;
import com.pragma.perfiles.pragmadata.aplicacion.fabrica.FabricaInformacionFamiliarArchivo;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionFamiliarArchivo;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class InformacionFamiliarRepositorioImpl extends CrudRepositorioBaseImpl<String, InformacionFamiliar, InformacionFamiliarEntity> implements InformacionFamiliarRepositorio {

    private final InformacionFamiliarDao informacionFamiliarDao;
    private final InformacionFamiliarMapperInformacionFamiliarEntity informacionFamiliarMapperInformacionFamiliarEntity;
    private final FabricaInformacionFamiliarArchivo fabricaInformacionFamiliarArchivo;

    @Override
    protected InformacionFamiliarDao obtenerRepositorio(){
        return informacionFamiliarDao;
    }

    @Override
    protected InformacionFamiliarMapperInformacionFamiliarEntity obtenerConversionBase(){
        return informacionFamiliarMapperInformacionFamiliarEntity;
    }

    @Override
    public Stream<InformacionFamiliar> findByIdUsuario(String idUsuario) {
        return informacionFamiliarMapperInformacionFamiliarEntity.rightToLeft(
                informacionFamiliarDao.findByUsuarioId(idUsuario)
        );
    }

    @Override
    public Stream<InformacionFamiliarArchivo> findByUsuarioIn(Collection<String> idLista) {
        Iterable<InformacionFamiliarEntity> info = informacionFamiliarDao.findByUsuarioIdIn(idLista);
        return fabricaInformacionFamiliarArchivo.informacionFamiliarPorInformacionFamiliarArchivo(
                info
        );
    }
}
