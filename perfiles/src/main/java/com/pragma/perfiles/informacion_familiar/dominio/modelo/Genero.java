package com.pragma.perfiles.informacion_familiar.dominio.modelo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;
import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum Genero {

    MASCULINO("Masculino"), FEMENINO("Femenino");

    private String genero;

    @JsonValue
    public String getValue() {
        return getGenero();
    }

    @JsonCreator
    public static Genero fromValue(String value) {
        if (value != null && value.isEmpty()) {
            return null;
        }
        for (Genero p : values()) {
            if (p.getValue().equals(value)) {
                return p;
            }
        }
        throw new ExcepcionesInformacionFamiliar("estado");
    }



    private Genero(String genero){
        this.genero = genero;
    }

    public String getGenero() {
        return genero;
    }
}
