package com.pragma.perfiles.informacion_familiar.aplicacion.manejador;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerInformacionFamiliarList {

    private final InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase;

    public ObjetoRespuesta<Stream<InformacionFamiliar>> ejecutar(){
        Stream<InformacionFamiliar> informacionFamiliarStream = informacionFamiliarCrudUseCase.obtenerTodos();
        return new ObjetoRespuesta<Stream<InformacionFamiliar>>(informacionFamiliarStream);
    }

}
