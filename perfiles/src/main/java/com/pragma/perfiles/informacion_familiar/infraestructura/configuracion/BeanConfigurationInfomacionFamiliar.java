package com.pragma.perfiles.informacion_familiar.infraestructura.configuracion;

import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.FabricaInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.impl.InformacionFamiliarMapperActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.impl.InformacionFamiliarMapperGuardarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.impl.InformacionFamiliarMapperInformacionFamiliarPragmaData;
import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.*;
import com.pragma.perfiles.informacion_familiar.dominio.repositorio.InformacionFamiliarRepositorio;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import com.pragma.perfiles.parentesco.aplicacion.manejador.ManejadorObtenerParentescoPorId;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationInfomacionFamiliar {

    @Bean
    public ManejadorGuardarInformacionFamiliar manejadorGuardarInformacionFamiliar(InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase,
                                                                                   FabricaInformacionFamiliar fabricaInformacionFamiliar,
                                                                                   ManejadorObtenerParentescoPorId manejadorObtenerParentescoPorId,
                                                                                   UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorGuardarInformacionFamiliar(informacionFamiliarCrudUseCase, fabricaInformacionFamiliar,manejadorObtenerParentescoPorId,
                usuarioCrudUseCase);
    }

    @Bean
    public ManejadorActualizarInformacionFamiliar manejadorActualizarInformacionFamiliar(InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase,
                                                                                         FabricaInformacionFamiliar fabricaInformacionFamiliar,
                                                                                         ManejadorObtenerParentescoPorId manejadorObtenerParentescoPorId,
                                                                                         UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorActualizarInformacionFamiliar(informacionFamiliarCrudUseCase, fabricaInformacionFamiliar, manejadorObtenerParentescoPorId,
                usuarioCrudUseCase);
    }


    @Bean
    public ManejadorEliminarInformacionFamiliar manejadorEliminarInformacionFamiliar(InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase,
                                                                                     UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorEliminarInformacionFamiliar(informacionFamiliarCrudUseCase,usuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerInformacionFamiliarPorId manejadorObtenerInformacionFamiliarPorId(InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase){
        return new ManejadorObtenerInformacionFamiliarPorId(informacionFamiliarCrudUseCase);
    }

    @Bean
    public ManejadorObtenerInformacionFamiliarList manejadorObtenerInformacionFamiliarList(InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase){
        return new ManejadorObtenerInformacionFamiliarList(informacionFamiliarCrudUseCase);
    }

    @Bean
    public ManejadorObtenerInformacionFamiliarPorCorreo manejadorObtenerFamiliaresDependientesPorCorreo(InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase, UsuarioCrudUseCase usuarioCrudUseCase, FabricaInformacionFamiliar fabricaInformacionFamiliar){
        return new ManejadorObtenerInformacionFamiliarPorCorreo(informacionFamiliarCrudUseCase, usuarioCrudUseCase, fabricaInformacionFamiliar);
    }

    @Bean
    public InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase(InformacionFamiliarRepositorio informacionFamiliarRepositorio){
        return new InformacionFamiliarCrudUseCase(informacionFamiliarRepositorio);
    }

    @Bean
    public ManejadorObtenerInformacionFamiliarPorIdUsuario manejadorObtenerInformacionFamiliarPorIdUsuario(InformacionFamiliarCrudUseCase
                                                                                                                       informacionFamiliarCrudUseCase){
        return new ManejadorObtenerInformacionFamiliarPorIdUsuario(informacionFamiliarCrudUseCase);
    }

    @Bean
    public FabricaInformacionFamiliar fabricaInformacionFamiliar(
            InformacionFamiliarMapperGuardarInformacionFamiliarComando informacionFamiliarMapperGuardarInformacionFamiliarComando,
            InformacionFamiliarMapperActualizarInformacionFamiliarComando informacionFamiliarMapperActualizarInformacionFamiliarComando,
            InformacionFamiliarMapperInformacionFamiliarPragmaData informacionFamiliarMapperInformacionFamiliarPragmaData){
        return new FabricaInformacionFamiliar(informacionFamiliarMapperActualizarInformacionFamiliarComando,informacionFamiliarMapperGuardarInformacionFamiliarComando,informacionFamiliarMapperInformacionFamiliarPragmaData);
    }

}
