package com.pragma.perfiles.informacion_familiar.dominio.useCase;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.repositorio.InformacionFamiliarRepositorio;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionFamiliarArchivo;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.Collection;
import java.util.stream.Stream;


public class InformacionFamiliarCrudUseCase extends CrudUseCaseCommon<InformacionFamiliarRepositorio, InformacionFamiliar, String> {

    private final InformacionFamiliarRepositorio informacionFamiliarRepositorio;

    public InformacionFamiliarCrudUseCase(InformacionFamiliarRepositorio informacionFamiliarRepositorio){
        super(informacionFamiliarRepositorio);
        this.informacionFamiliarRepositorio = informacionFamiliarRepositorio;
    }

    public Stream<InformacionFamiliar> findByIdUsuario(String idUsuario){
        return informacionFamiliarRepositorio.findByIdUsuario(idUsuario);
    }

    public Stream<InformacionFamiliarArchivo> findByUsuarioIn(Collection<String> idLista) {
        return informacionFamiliarRepositorio.findByUsuarioIn(idLista);
    }
}
