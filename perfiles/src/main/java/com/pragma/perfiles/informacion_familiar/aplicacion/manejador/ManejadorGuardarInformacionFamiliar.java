package com.pragma.perfiles.informacion_familiar.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.informacion_familiar.aplicacion.comando.GuardarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.FabricaInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import com.pragma.perfiles.parentesco.aplicacion.manejador.ManejadorObtenerParentescoPorId;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorGuardarInformacionFamiliar {

    private final InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase;
    private final FabricaInformacionFamiliar fabricaInformacionFamiliar;
    private final ManejadorObtenerParentescoPorId manejadorObtenerParentescoPorId;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public void ejecutar(GuardarInformacionFamiliarComando guardarInformacionFamiliarComando){
        Usuario usuario= usuarioCrudUseCase.obtenerPorId(guardarInformacionFamiliarComando.getIdUsuario());

        if(usuario==null){
            throw new UsuarioNoEncontrado("No se encontro el usuario con el ID: "+guardarInformacionFamiliarComando.getIdUsuario());
        }
        InformacionFamiliar informacionFamiliar = fabricaInformacionFamiliar.guardarInformacionFamiliar(guardarInformacionFamiliarComando);
        ObjetoRespuesta<Parentesco> parentescoObjetoRespuesta = manejadorObtenerParentescoPorId.ejecutar(guardarInformacionFamiliarComando.getIdParentesco());
        Parentesco parentesco = parentescoObjetoRespuesta.getDato();
        informacionFamiliar.setParentesco(parentesco);
        informacionFamiliarCrudUseCase.guardar(informacionFamiliar);
    }

}
