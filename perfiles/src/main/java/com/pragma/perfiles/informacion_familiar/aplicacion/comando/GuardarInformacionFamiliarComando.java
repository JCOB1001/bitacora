package com.pragma.perfiles.informacion_familiar.aplicacion.comando;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.Genero;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuardarInformacionFamiliarComando {

    private String nombres;
    private String apellidos;
    private Genero genero;
    private LocalDate fechaNacimiento;
    private String idUsuario;
    private Long idParentesco;
    private boolean dependiente;

}
