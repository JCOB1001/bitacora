package com.pragma.perfiles.informacion_familiar.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.FabricaInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliarConsultaPragmaData;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerInformacionFamiliarPorCorreo {

    private final InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaInformacionFamiliar fabricaInformacionFamiliar;

    public Stream<InformacionFamiliarConsultaPragmaData> ejecutar(String correo){

        Usuario usuario = usuarioCrudUseCase.findByCorreoEmpresarial(correo);

        if (usuario == null) {
            throw new UsuarioNoEncontrado("El usuario con el correo " + correo+ " no existe en el sistema");
        }

        Iterable<InformacionFamiliar> informacionFamiliarList = informacionFamiliarCrudUseCase.findByIdUsuario(usuario.getId()).collect(Collectors.toList());
        return fabricaInformacionFamiliar.consultaPragmaData(informacionFamiliarList);
    }

}