package com.pragma.perfiles.informacion_familiar.aplicacion.fabrica;

import com.pragma.perfiles.informacion_familiar.aplicacion.comando.ActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.comando.GuardarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.impl.InformacionFamiliarMapperActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.impl.InformacionFamiliarMapperGuardarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.impl.InformacionFamiliarMapperInformacionFamiliarPragmaData;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliarConsultaPragmaData;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class FabricaInformacionFamiliar {

    private final InformacionFamiliarMapperActualizarInformacionFamiliarComando informacionFamiliarMapperActualizarInformacionFamiliarComando;
    private final InformacionFamiliarMapperGuardarInformacionFamiliarComando informacionFamiliarMapperGuardarInformacionFamiliarComando;
    private final InformacionFamiliarMapperInformacionFamiliarPragmaData informacionFamiliarMapperInformacionFamiliarPragmaData;

    public InformacionFamiliar guardarInformacionFamiliar(GuardarInformacionFamiliarComando guardarInformacionFamiliarComando){
        return informacionFamiliarMapperGuardarInformacionFamiliarComando.rightToLeft(guardarInformacionFamiliarComando);
    }

    public InformacionFamiliar actualizarInformacionFamiliar(ActualizarInformacionFamiliarComando actualizarInformacionFamiliarComando){
        return informacionFamiliarMapperActualizarInformacionFamiliarComando.rightToLeft(actualizarInformacionFamiliarComando);
    }

    public Stream<InformacionFamiliarConsultaPragmaData> consultaPragmaData(Iterable<InformacionFamiliar> informacionFamiliar) {
        return informacionFamiliarMapperInformacionFamiliarPragmaData.leftToRight(informacionFamiliar);
    }

}
