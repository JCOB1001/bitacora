package com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.entidad;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.Genero;
import com.pragma.perfiles.parentesco.infraestructura.persistencia.entidad.ParentescoEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "informacion_familiar")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InformacionFamiliarEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos{
        String NOMBRES = "nombres";
        String APELLIDOS = "apellidos";
        String GENERO = "genero";
        String FECHA_NACIMIENTO = "fechaNacimiento";
        String USUARIO = "usuario";
        String PARENTESCO_ENTITY = "parentescoEntity";
        String DEPENDIENTE = "dependiente";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String nombres;
    private String apellidos;
    @Enumerated(value = EnumType.STRING)
    private Genero genero;
    private LocalDate fechaNacimiento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_informacion_familiar_usuario_usuario_id"))
    private UsuarioEntity usuario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentesco_id", foreignKey = @ForeignKey(name = "fk_informacion_familiar_parentesco_parentesco_id"))
    private ParentescoEntity parentescoEntity;

    private boolean dependiente;

}
