package com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.impl;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliarConsultaPragmaData;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface InformacionFamiliarMapperInformacionFamiliarPragmaData extends ConvertidorBase<InformacionFamiliar, InformacionFamiliarConsultaPragmaData> {

    @AfterMapping
    default void setDependiente(@MappingTarget InformacionFamiliarConsultaPragmaData informacionFamiliarConsultaPragmaData,
                                       InformacionFamiliar informacionFamiliar) {
        String dependiente = informacionFamiliar.isDependiente() ? "Si" : "No";
        informacionFamiliarConsultaPragmaData.setDependiente(dependiente);
    }

    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "parentesco.nombre", target = "parentesco")
    })
    InformacionFamiliarConsultaPragmaData leftToRight(InformacionFamiliar informacionFamiliar);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "parentesco", target = "parentesco.nombre")
    })
    InformacionFamiliar rightToLeft(InformacionFamiliarConsultaPragmaData informacionFamiliar);

}
