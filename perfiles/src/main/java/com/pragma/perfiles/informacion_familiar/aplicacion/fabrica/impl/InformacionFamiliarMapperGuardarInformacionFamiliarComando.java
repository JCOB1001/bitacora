package com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.impl;

import com.pragma.perfiles.informacion_familiar.aplicacion.comando.GuardarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InformacionFamiliarMapperGuardarInformacionFamiliarComando extends ConvertidorBase<InformacionFamiliar, GuardarInformacionFamiliarComando> {
}
