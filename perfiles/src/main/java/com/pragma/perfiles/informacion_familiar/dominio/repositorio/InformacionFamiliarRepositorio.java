package com.pragma.perfiles.informacion_familiar.dominio.repositorio;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionFamiliarArchivo;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.Collection;
import java.util.stream.Stream;

public interface InformacionFamiliarRepositorio extends CrudRepositorioBase<String, InformacionFamiliar> {

    public Stream<InformacionFamiliar> findByIdUsuario(String idUsuario);

    Stream<InformacionFamiliarArchivo> findByUsuarioIn(Collection<String> idLista);
}
