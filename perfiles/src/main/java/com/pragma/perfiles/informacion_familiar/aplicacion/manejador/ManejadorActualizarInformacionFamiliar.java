package com.pragma.perfiles.informacion_familiar.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.informacion_familiar.aplicacion.comando.ActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.FabricaInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import com.pragma.perfiles.parentesco.aplicacion.manejador.ManejadorObtenerParentescoPorId;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorActualizarInformacionFamiliar {
    private final InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase;
    private final FabricaInformacionFamiliar fabricaInformacionFamiliar;
    private final ManejadorObtenerParentescoPorId manejadorObtenerParentescoPorId;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    public ObjetoRespuesta<InformacionFamiliar> ejecutar(String idInformacionFamiliar, ActualizarInformacionFamiliarComando actualizarInformacionFamiliarComando){
        InformacionFamiliar informacionFamiliar = informacionFamiliarCrudUseCase.obtenerPorId(idInformacionFamiliar);
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(actualizarInformacionFamiliarComando.getIdUsuario());

        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+actualizarInformacionFamiliarComando.getIdUsuario()+" no existe");
        }

        if(informacionFamiliar != null){
            InformacionFamiliar informacionFamiliarNuevo = fabricaInformacionFamiliar.actualizarInformacionFamiliar(actualizarInformacionFamiliarComando);
            informacionFamiliarNuevo.setId(idInformacionFamiliar);
            ObjetoRespuesta<Parentesco> parentescoObjetoRespuesta = manejadorObtenerParentescoPorId.ejecutar(actualizarInformacionFamiliarComando.getIdParentesco());
            Parentesco parentesco = parentescoObjetoRespuesta.getDato();
            informacionFamiliarNuevo.setParentesco(parentesco);
            informacionFamiliarCrudUseCase.actualizar(informacionFamiliarNuevo);
            informacionFamiliar = informacionFamiliarNuevo;
        }
        return new ObjetoRespuesta<InformacionFamiliar>(informacionFamiliar);
    }

}
