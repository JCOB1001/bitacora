package com.pragma.perfiles.informacion_familiar.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorObtenerInformacionFamiliarPorId;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("informacion-familiar")
@RequiredArgsConstructor
@Validated
public class EndPointObtenerInformacionFamiliarPorId {

    private final ManejadorObtenerInformacionFamiliarPorId manejadorObtenerInformacionFamiliarPorId;

    @GetMapping("/{idInformacionFamiliar}")
    public ObjetoRespuesta<InformacionFamiliar> ejecutarObtenerInformacionFamiliarPorId(@NotNull @PathVariable String idInformacionFamiliar){
        ObjetoRespuesta<InformacionFamiliar> informacionFamiliarObjetoRespuesta = manejadorObtenerInformacionFamiliarPorId.ejecutar(idInformacionFamiliar);
        if(informacionFamiliarObjetoRespuesta.getDato() == null){
            throw new ExcepcionesInformacionFamiliar("No existe esta informacion familiar");
        }
        return new ObjetoRespuesta<InformacionFamiliar>(HttpStatus.OK, informacionFamiliarObjetoRespuesta.getDato());
    }

}
