package com.pragma.perfiles.informacion_familiar.aplicacion.manejador;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerInformacionFamiliarPorId {

    private final InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase;

    public ObjetoRespuesta<InformacionFamiliar> ejecutar(String idInformacionFamiliar){
        InformacionFamiliar informacionFamiliar = informacionFamiliarCrudUseCase.obtenerPorId(idInformacionFamiliar);
        return new ObjetoRespuesta<InformacionFamiliar>(informacionFamiliar);
    }

}
