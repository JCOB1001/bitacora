package com.pragma.perfiles.informacion_familiar.infraestructura.endpoint;

import com.pragma.perfiles.informacion_familiar.aplicacion.comando.GuardarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorGuardarInformacionFamiliar;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("informacion-familiar")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarInformacionFamiliar {

    private final ManejadorGuardarInformacionFamiliar manejadorGuardarInformacionFamiliar;

    @PostMapping
    public RespuestaBase ejecutarGuardarInformacionFamiliar(@NotNull @RequestBody GuardarInformacionFamiliarComando guardarInformacionFamiliarComando){
        manejadorGuardarInformacionFamiliar.ejecutar(guardarInformacionFamiliarComando);
        return RespuestaBase.ok("Guardado Exitosamente");
    }

}
