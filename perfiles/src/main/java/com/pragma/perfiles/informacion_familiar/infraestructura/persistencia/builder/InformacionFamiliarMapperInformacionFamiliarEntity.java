package com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.builder;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.entidad.InformacionFamiliarEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface InformacionFamiliarMapperInformacionFamiliarEntity extends ConvertidorBase<InformacionFamiliar, InformacionFamiliarEntity> {

    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "parentesco", target = "parentescoEntity"),
            @Mapping(source = "idUsuario", target = "usuario.id")
    })
    InformacionFamiliarEntity leftToRight(InformacionFamiliar informacionFamiliar);


    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "parentescoEntity", target = "parentesco"),
            @Mapping(source = "usuario.id", target = "idUsuario")
    })
    InformacionFamiliar rightToLeft(InformacionFamiliarEntity informacionFamiliarEntity);


}
