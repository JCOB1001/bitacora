package com.pragma.perfiles.informacion_familiar.infraestructura.endpoint;


import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorEliminarInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("informacion-familiar")
@RequiredArgsConstructor
@Validated
public class EndPointEliminarInformacionFamiliar {

    private final ManejadorEliminarInformacionFamiliar manejadorEliminarInformacionFamiliar;

    @DeleteMapping("/{idInformacionFamiliar}/{idUsuario}")
    public ObjetoRespuesta<InformacionFamiliar> ejecutarEliminarInformacionFamiliar(@PathVariable  String idInformacionFamiliar,@PathVariable String idUsuario){
        ObjetoRespuesta<InformacionFamiliar> informacionFamiliarObjetoRespuesta = manejadorEliminarInformacionFamiliar.ejecutar(idInformacionFamiliar, idUsuario);
        return new ObjetoRespuesta<InformacionFamiliar>(HttpStatus.OK, informacionFamiliarObjetoRespuesta.getDato());
    }

}
