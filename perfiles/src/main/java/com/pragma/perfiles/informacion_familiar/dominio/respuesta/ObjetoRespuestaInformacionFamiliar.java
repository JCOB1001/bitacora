package com.pragma.perfiles.informacion_familiar.dominio.respuesta;

import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaInformacionFamiliar extends ObjetoRespuesta {

    public ObjetoRespuestaInformacionFamiliar(Stream<InformacionFamiliar> dato){
        super(dato);
    }
}
