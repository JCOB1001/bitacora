package com.pragma.perfiles.informacion_familiar.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarInformacionFamiliar {

    private final InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<InformacionFamiliar> ejecutar(String idInformacionFamiliar,String idUsuario){
        InformacionFamiliar informacionFamiliar = informacionFamiliarCrudUseCase.obtenerPorId(idInformacionFamiliar);

        Usuario usuario=usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario==null){
            throw  new UsuarioNoEncontrado("El usuario con el ID: "+idUsuario+" no existe");
        }

        if(informacionFamiliar == null){
            throw new ExcepcionesInformacionFamiliar("No existe esta información familiar");
        }

        if(!usuario.getId().equals(informacionFamiliar.getIdUsuario())){
            throw new ExcepcionesInformacionFamiliar("el Id del usuario no coincide con el de la informacio familiar");
        }

        informacionFamiliarCrudUseCase.eliminarPorId(idInformacionFamiliar);
        return new ObjetoRespuesta<InformacionFamiliar>(informacionFamiliar);
    }

}
