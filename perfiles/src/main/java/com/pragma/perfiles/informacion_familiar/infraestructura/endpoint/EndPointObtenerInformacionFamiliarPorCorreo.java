package com.pragma.perfiles.informacion_familiar.infraestructura.endpoint;

import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorObtenerInformacionFamiliarPorCorreo;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliarConsultaPragmaData;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("informacion-familiar")
@RequiredArgsConstructor
@Validated
public class EndPointObtenerInformacionFamiliarPorCorreo {

    private final ManejadorObtenerInformacionFamiliarPorCorreo manejadorObtenerInformacionFamiliarPorCorreo;

    @GetMapping("/correo/{correoEmpresarial}")
    public ObjetoRespuesta<Stream<InformacionFamiliarConsultaPragmaData>> ejecutarObtenerInformacionFamiliarPorCorreo(@NotNull @PathVariable String correoEmpresarial) {
        Stream<InformacionFamiliarConsultaPragmaData> informacionFamiliar =  manejadorObtenerInformacionFamiliarPorCorreo.ejecutar(correoEmpresarial);
        return new ObjetoRespuesta<>(HttpStatus.OK, informacionFamiliar);
    }

}
