package com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.impl;

import com.pragma.perfiles.informacion_familiar.aplicacion.comando.ActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InformacionFamiliarMapperActualizarInformacionFamiliarComando extends ConvertidorBase<InformacionFamiliar, ActualizarInformacionFamiliarComando> {
}
