package com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "usuario_idiomas", uniqueConstraints = {@UniqueConstraint(name = "uq_usuario_id_idiomas_id", columnNames = {"usuario_id", "idiomas_id"})})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioIdiomasEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends IdEntidad.Atributos {

        String ID = "id";
        String USUARIO = "usuario";
        String IDIOMA = "idioma";
        String NIVEL = "nivel";
        String PRUEBA_SUFICIENCIA = "hasPruebaSuficiencia";
        String NOMBRE_PRUEBA = "nombrePrueba";
        String PUNTAJE = "puntaje";
        String URL = "urlCertificado";

    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_idiomas_usuario_usuario_id"))
    private UsuarioEntity usuario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idiomas_id", foreignKey = @ForeignKey(name = "fk_usuario_idiomas_idiomas_idiomas_id"))
    private IdiomasEntity idioma;

    @Column(name = "nivel")
    private int nivel;

    @Column(name = "prueba_suficiencia")
    private boolean hasPruebaSuficiencia;

    @Column (name = "nombre_prueba")
    private String nombrePrueba;

    @Column (name = "puntaje")
    private String puntaje;

    @Column (name = "url_certificado")
    private String urlCertificado;
}
