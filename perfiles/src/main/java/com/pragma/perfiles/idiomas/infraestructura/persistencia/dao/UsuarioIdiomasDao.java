package com.pragma.perfiles.idiomas.infraestructura.persistencia.dao;

import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.UsuarioIdiomasEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface UsuarioIdiomasDao extends JpaRepository<UsuarioIdiomasEntity, String> {

    Iterable<UsuarioIdiomasEntity> findByUsuarioId(String usuarioId);
    UsuarioIdiomasEntity findByUsuarioIdAndIdiomaId(String UsuarioId, String idiomaId);
    Iterable<UsuarioIdiomasEntity> findByUsuarioIdIn(Collection<String> idLista);
}
