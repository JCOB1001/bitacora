package com.pragma.perfiles.idiomas.aplicacion.fabrica.impl;

import com.pragma.perfiles.idiomas.aplicacion.comando.ActualizarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarArchivoIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaUsuarioIdiomas {
    private final UsuarioIdiomasMapperActualizarUsuarioIdiomas usuarioIdiomasMapperActualizarUsuarioIdiomas;
    private final IdiomasMapperGuardarIdiomasComando idiomasMapperGuardarIdiomasComando;
    private final UsuarioIdiomasMapperGuardarUsuarioIdiomas usuarioIdiomasMapperGuardarUsuarioIdiomas;

    public UsuarioIdiomas ActualizarUsuarioIdiomas(ActualizarUsuarioIdiomasComando actualizarUsuarioIdiomasComando){
        return  usuarioIdiomasMapperActualizarUsuarioIdiomas.rightToLeft(actualizarUsuarioIdiomasComando);
    }

    public Idioma guardarIdiomas(GuardarArchivoIdiomasComando guardarIdiomasComando){
        return  idiomasMapperGuardarIdiomasComando.righToLeft(guardarIdiomasComando);
    }

    public UsuarioIdiomas guardarUsuarioIdiomas(GuardarUsuarioIdiomasComando guardarUsuarioIdiomasComando){
        return usuarioIdiomasMapperGuardarUsuarioIdiomas.rightToLeft(guardarUsuarioIdiomasComando);
    }
}
