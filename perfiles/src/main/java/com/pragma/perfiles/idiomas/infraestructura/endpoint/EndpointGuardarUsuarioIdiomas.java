package com.pragma.perfiles.idiomas.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorGuardarUsuarioConocimiento;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.manejador.ManejadorGuardarUsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("idiomas")
@RequiredArgsConstructor
@Valid
public class EndpointGuardarUsuarioIdiomas {

    private final ManejadorGuardarUsuarioIdiomas manejadorGuardarUsuarioIdiomas;

    @PostMapping
    public ObjetoRespuesta<UsuarioIdiomas> guardarUsuarioIdiomas(@NotNull @RequestBody GuardarUsuarioIdiomasComando guardarUsuarioIdiomasComando) {
        return manejadorGuardarUsuarioIdiomas.ejecutar(guardarUsuarioIdiomasComando);
    }
}
