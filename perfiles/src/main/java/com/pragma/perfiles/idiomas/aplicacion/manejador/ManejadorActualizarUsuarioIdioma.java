package com.pragma.perfiles.idiomas.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.IdiomaNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.NivelNoValido;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioIdiomasNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.idiomas.aplicacion.comando.ActualizarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarArchivoIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.impl.FabricaUsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.useCase.IdiomaCrudUseCase;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.otros_estudios.infraestructura.configuracion.TextosComunes;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@RequiredArgsConstructor
public class ManejadorActualizarUsuarioIdioma {

    private final FabricaUsuarioIdiomas fabricaUsuarioIdiomas;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase;
    private final IdiomaCrudUseCase idiomaCrudUseCase;

    public ObjetoRespuesta<UsuarioIdiomas> ejecutar(ActualizarUsuarioIdiomasComando actualizarUsuarioIdiomasComando){

        if(!(actualizarUsuarioIdiomasComando.getNivel() >= 1 && actualizarUsuarioIdiomasComando.getNivel() <= 5)){
            throw new NivelNoValido("El rango de niveles permitidos es entre 1 y 5");
        }

        Usuario usuario = usuarioCrudUseCase.obtenerPorId(actualizarUsuarioIdiomasComando.getIdUsuario());
        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID "+actualizarUsuarioIdiomasComando.getIdUsuario()+" no se encuentra registrado");
        }

        Idioma idioma = idiomaCrudUseCase.obtenerPorId(actualizarUsuarioIdiomasComando.getIdIdioma());
        if(idioma==null){
            throw new IdiomaNoEncontrado("El idioma con el ID "+actualizarUsuarioIdiomasComando.getIdIdioma()+" no se encuentra registrado");
        }

        UsuarioIdiomas usuarioIdiomasValidacion = usuarioIdiomasCrudUseCase.findByUsuarioIdAndIdiomaId(
                usuario.getId(),
                idioma.getId()
        );
        if(usuarioIdiomasValidacion == null){
            throw new UsuarioIdiomasNoEncontrado(
                    "El usuario con id: "+actualizarUsuarioIdiomasComando.getIdUsuario()+" no tiene registrado el idioma con id: "+actualizarUsuarioIdiomasComando.getIdIdioma()
            );
        }

        String urlFormateada="https://d2y95fubzeqro6.cloudfront.net/general/certicados/20200327120334.";
        String urlDefecto="https://d2y95fubzeqro6.cloudfront.net/general/certicados/20200327120334.";
        if(actualizarUsuarioIdiomasComando.getHasPruebaSuficiencia() != null
                && actualizarUsuarioIdiomasComando.getHasPruebaSuficiencia().booleanValue()) {
            //tieneArchivo
            if (actualizarUsuarioIdiomasComando.getArchivo() != null
                    && actualizarUsuarioIdiomasComando.getExtension() != null) {
                GuardarArchivoIdiomasComando guardarArchivoIdiomasComando = GuardarArchivoIdiomasComando.builder()
                        .nombreArchivo(UUID.randomUUID().toString()+actualizarUsuarioIdiomasComando.getExtension())
                        .carpeta(TextosComunes.CARPETA_GUARDAR_CERTIFICADO_IDIOMA)
                        .file(actualizarUsuarioIdiomasComando.getArchivo()).build();
                String url = usuarioIdiomasCrudUseCase.guardarCertificado(guardarArchivoIdiomasComando);
                urlFormateada = formatUrl(url);
            }
        }

        UsuarioIdiomas usuarioIdiomasNuevo = fabricaUsuarioIdiomas.ActualizarUsuarioIdiomas(actualizarUsuarioIdiomasComando);

        if(usuarioIdiomasNuevo.getNombrePrueba()==null){
            usuarioIdiomasNuevo.setNombrePrueba(usuarioIdiomasValidacion.getNombrePrueba());
        }
        if(usuarioIdiomasNuevo.getPuntaje()==null){
            usuarioIdiomasNuevo.setPuntaje(usuarioIdiomasValidacion.getPuntaje());
        }

        usuarioIdiomasNuevo.setId(usuarioIdiomasValidacion.getId());
        usuarioIdiomasNuevo.setUrlCertificado(urlFormateada);

        if (usuarioIdiomasNuevo.getUrlCertificado()==urlDefecto){
            usuarioIdiomasNuevo.setUrlCertificado(usuarioIdiomasValidacion.getUrlCertificado());
        }

        usuarioIdiomasCrudUseCase.actualizar(usuarioIdiomasNuevo);

        return  new ObjetoRespuesta<UsuarioIdiomas>(usuarioIdiomasNuevo);
    }

    public String formatUrl(String url){
        String res[]=url.replace("{","").replace("}","").replace("\"","").split("/");
        String respuesta="https://";
        for (int i=2;i<res.length;i++ ){
            respuesta+=res[i];
            if(i<res.length-1){
                respuesta+="/";}
        }
        return respuesta;
    }
}
