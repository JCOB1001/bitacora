package com.pragma.perfiles.idiomas.dominio.repositorio;

import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarArchivoIdiomasComando;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionIdiomasArchivo;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.Collection;
import java.util.stream.Stream;

public interface UsuarioIdiomasRepositorio extends CrudRepositorioBase<String, UsuarioIdiomas> {

    Stream<UsuarioIdiomas> findByUsuarioId(String usuarioId);

    UsuarioIdiomas findByUsuarioIdAndIdiomaId(String usuarioId, String idiomaId);

    void deleteByUsuarioIdAndIdiomaId(String idUsuario, String nombreIdioma);

    String guardarCertificado(GuardarArchivoIdiomasComando guardarArchivoComando);

    void eliminarCertificado(GuardarArchivoIdiomasComando guardarArchivoIdiomasComando);

    Stream<InformacionIdiomasArchivo> findByUsuarioIn(Collection<String> idLista);
}
