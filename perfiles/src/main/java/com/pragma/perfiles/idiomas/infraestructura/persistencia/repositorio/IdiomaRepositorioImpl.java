package com.pragma.perfiles.idiomas.infraestructura.persistencia.repositorio;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.repositorio.IdiomasRepositorio;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.builder.IdiomasMapperIdiomasEntity;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.dao.IdiomasDao;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.stream.Stream;
@Repository
@RequiredArgsConstructor
public class IdiomaRepositorioImpl extends CrudRepositorioBaseImpl<String, Idioma, IdiomasEntity> implements IdiomasRepositorio {

    private final IdiomasDao idiomaDao;
    private final IdiomasMapperIdiomasEntity idiomaMapperIdiomaEntity;

    @Override
    protected IdiomasDao obtenerRepositorio() {
        return idiomaDao;
    }

    @Override
    protected IdiomasMapperIdiomasEntity obtenerConversionBase() {
        return idiomaMapperIdiomaEntity;
    }

    @Override
    public Stream<Idioma> findByIdUsuario(String idUsuario) {
        return null;
    }

    @Override
    public UsuarioIdiomas findByIdUsuarioAndNombreSkill(String idUsuario, String nombreSkill) {
        return null;
    }

    @Override
    public Idioma findById(String id) {
        return idiomaDao.findById(id).map(idioma -> idiomaMapperIdiomaEntity.rightToLeft(idioma))
                .orElse(null);
    }
}
