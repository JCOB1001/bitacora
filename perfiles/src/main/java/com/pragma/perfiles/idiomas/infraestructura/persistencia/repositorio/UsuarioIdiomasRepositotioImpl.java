package com.pragma.perfiles.idiomas.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.comun.infraestructura.error.IdiomaNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarArchivoIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.FabricaIdiomas;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.repositorio.UsuarioIdiomasRepositorio;
import com.pragma.perfiles.idiomas.infraestructura.clienteFeing.FeignApi;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.builder.UsuarioIdiomaMapperUsuarioIdiomaEntity;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.dao.IdiomasDao;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.dao.UsuarioIdiomasDao;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.UsuarioIdiomasEntity;
import com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.entidad.InformacionFamiliarEntity;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionIdiomasArchivo;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class UsuarioIdiomasRepositotioImpl extends CrudRepositorioBaseImpl<String, UsuarioIdiomas, UsuarioIdiomasEntity> implements UsuarioIdiomasRepositorio {

    private final UsuarioIdiomasDao usuarioIdiomasDao;
    private final UsuarioIdiomaMapperUsuarioIdiomaEntity usuarioIdiomaMapperUsuarioIdiomaEntity;
    private final FeignApi feignApi;
    private final FabricaIdiomas fabricaIdiomas;
    private final IdiomasDao idiomasDao;

    @Override
    protected UsuarioIdiomasDao obtenerRepositorio() { return usuarioIdiomasDao;}

    @Override
    protected UsuarioIdiomaMapperUsuarioIdiomaEntity obtenerConversionBase() {
        return  usuarioIdiomaMapperUsuarioIdiomaEntity;
    }

    @Override
    public Stream<UsuarioIdiomas> findByUsuarioId(String usuarioId) {
        return usuarioIdiomaMapperUsuarioIdiomaEntity.rightToLeft(
                usuarioIdiomasDao.findByUsuarioId(usuarioId)
        );
    }

    @Override
    public UsuarioIdiomas findByUsuarioIdAndIdiomaId(String usuarioId, String idiomaId) {
        return usuarioIdiomaMapperUsuarioIdiomaEntity.rightToLeft(
                usuarioIdiomasDao.findByUsuarioIdAndIdiomaId(usuarioId,idiomaId)
        );
    }

    /*@Override
    public Stream<UsuarioIdiomas> findByIdUsuarioAndIdioma(String idUsuario, String nombreIdioma) {
        return usuarioIdiomaMapperUsuarioIdiomaEntity.rightToLeft(
                usuarioIdiomasDao.findByIdUsuarioAndIdioma(idUsuario, nombreIdioma)
        );
    }*/

    @Override
    public void deleteByUsuarioIdAndIdiomaId(String idUsuario, String nombreIdioma) {

    }

    @Override
    public String guardarCertificado(GuardarArchivoIdiomasComando guardarArchivoComando) {
        if(guardarArchivoComando.getFile()==null){
            throw  new UsuarioNoEncontrado("El archivo no debe ser valido ");
        }
        return feignApi.fileUpload(guardarArchivoComando).getBody();
    }

    @Override
    public void eliminarCertificado(GuardarArchivoIdiomasComando guardarArchivoComando) {
        feignApi.deleteFile(guardarArchivoComando);
    }

    @Override
    public Stream<InformacionIdiomasArchivo> findByUsuarioIn(Collection<String> idLista) {
        Stream<InformacionIdiomasArchivo> info = fabricaIdiomas.idiomaPorInformacionIdiomaArchivo(usuarioIdiomasDao.findByUsuarioIdIn(idLista));
        return info;
    }
}
