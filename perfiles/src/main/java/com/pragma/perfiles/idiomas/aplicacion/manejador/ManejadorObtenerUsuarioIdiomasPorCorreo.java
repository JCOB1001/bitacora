package com.pragma.perfiles.idiomas.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.FabricaIdiomas;
import com.pragma.perfiles.idiomas.dominio.respuesta.UsuarioIdiomasConsultaPragmaData;
import com.pragma.perfiles.idiomas.dominio.useCase.IdiomaCrudUseCase;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioIdiomasPorCorreo {
    private final UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase;
    private final IdiomaCrudUseCase idiomaCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaIdiomas fabricaIdiomas;

    public Stream<UsuarioIdiomasConsultaPragmaData> ejecutar(String correoEmpresarial){
        return Optional.ofNullable(usuarioCrudUseCase.findByCorreoEmpresarial(correoEmpresarial))
            .map((usuario) -> fabricaIdiomas.usuarioIdiomasToUsuarioIdiomasConsultaPragmaData(
                        usuarioIdiomasCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList()))
                            .map(usuarioIdioma -> {
                                usuarioIdioma.setIdioma(idiomaCrudUseCase.findById(usuarioIdioma.getIdioma()).getNombre());
                                return usuarioIdioma;
                            })
            )
            .orElseThrow(() -> new UsuarioNoEncontrado("El usuario con el correo " + correoEmpresarial+ " no existe en el sistema"));
    }
}
