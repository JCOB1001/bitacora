package com.pragma.perfiles.idiomas.aplicacion.fabrica.impl;

import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.UsuarioIdiomasEntity;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionIdiomasArchivo;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface IdiomasMapperInformacionIdiomasArchivo extends ConvertidorBase<UsuarioIdiomasEntity, InformacionIdiomasArchivo> {
    @AfterMapping
    default void hasPruebaSuficiencia(@MappingTarget InformacionIdiomasArchivo informacionIdiomasArchivo, UsuarioIdiomasEntity usuarioIdiomasEntity) {

        if(
            Boolean.FALSE.equals(usuarioIdiomasEntity.isHasPruebaSuficiencia())
                || usuarioIdiomasEntity.getNombrePrueba()==null
                || usuarioIdiomasEntity.getPuntaje()==null
        ) {
            usuarioIdiomasEntity.setNombrePrueba("N/A");
            usuarioIdiomasEntity.setPuntaje("N/A");
        }
        informacionIdiomasArchivo.setNombrePrueba(usuarioIdiomasEntity.getNombrePrueba());
        informacionIdiomasArchivo.setPuntaje(usuarioIdiomasEntity.getPuntaje());
    }

    @Named("leftToRight")
    @Override
    @Mappings({
        @Mapping(source = "idioma.nombre", target = "idioma"),
        @Mapping(source = "usuario.correoEmpresarial", target = "correoEmpresarial")
    })
    InformacionIdiomasArchivo leftToRight(UsuarioIdiomasEntity usuarioIdiomas);

    @Named("rightToLeft")
    @Override
    @Mappings({
        @Mapping(source = "idioma", target = "idioma.nombre"),
        @Mapping(source = "correoEmpresarial", target = "usuario.correoEmpresarial")
    })
    UsuarioIdiomasEntity rightToLeft(InformacionIdiomasArchivo usuarioIdiomasConsultaPragmaData);
}
