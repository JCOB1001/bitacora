package com.pragma.perfiles.idiomas.dominio.respuesta;

import lombok.Data;

@Data
public class UsuarioIdiomasConsultaPragmaData {
    private String idioma;
    private int nivel;
    private String nombrePrueba;
    private String puntaje;
}
