package com.pragma.perfiles.idiomas.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarUsuarioIdiomasComando {

    private String idUsuario;
    private String idIdioma;
    private int nivel;
    private Boolean hasPruebaSuficiencia;
    private String nombrePrueba;
    private String puntaje;

    private String archivo;
    private String extension;

    public ActualizarUsuarioIdiomasComando(String idUsuario, String idIdioma, int nivel) {
        this.idUsuario = idUsuario;
        this.idIdioma = idIdioma;
        this.nivel = nivel;
    }
}
