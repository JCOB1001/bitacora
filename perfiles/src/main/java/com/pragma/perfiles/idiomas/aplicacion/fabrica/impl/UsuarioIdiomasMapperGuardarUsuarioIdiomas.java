package com.pragma.perfiles.idiomas.aplicacion.fabrica.impl;

import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")

public interface UsuarioIdiomasMapperGuardarUsuarioIdiomas extends ConvertidorBase<UsuarioIdiomas, GuardarUsuarioIdiomasComando> {
}
