package com.pragma.perfiles.idiomas.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarUsuarioIdiomasComandoUrl {

    private String idUsuario;
    private String idIdioma;
    private int nivel;
    private Boolean hasPruebaSuficiencia;
    private String nombrePrueba;
    private String puntaje;
    private String urlCertificado;
}

