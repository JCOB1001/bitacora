package com.pragma.perfiles.idiomas.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorEliminarUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.idiomas.aplicacion.manejador.ManejadorEliminarUsuarioIdioma;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
@RestController
@RequestMapping("idiomas")
@RequiredArgsConstructor
@Valid
public class EndpointEliminarUsuarioIdioma {

    private final ManejadorEliminarUsuarioIdioma manejadorEliminarUsuarioIdioma;

    @DeleteMapping("/{idUsuario}/{idIdioma}")
    public ObjetoRespuesta<UsuarioIdiomas> eliminarIdiomas(@NotNull @PathVariable String idUsuario,
                                                           @NotNull @PathVariable String idIdioma){

        return manejadorEliminarUsuarioIdioma.ejecutar(idUsuario, idIdioma);
    }


}
