package com.pragma.perfiles.idiomas.dominio.useCase;

import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarArchivoIdiomasComando;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.repositorio.UsuarioIdiomasRepositorio;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionIdiomasArchivo;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.Collection;
import java.util.stream.Stream;

public class UsuarioIdiomasCrudUseCase extends CrudUseCaseCommon<UsuarioIdiomasRepositorio, UsuarioIdiomas, String> {

    private final UsuarioIdiomasRepositorio usuarioIdiomasRepositorio;

    public UsuarioIdiomasCrudUseCase(UsuarioIdiomasRepositorio usuarioIdiomasRepositorio) {
        super(usuarioIdiomasRepositorio);
        this.usuarioIdiomasRepositorio = usuarioIdiomasRepositorio;
    }

    public Stream<UsuarioIdiomas> findByUsuarioId(String usuarioId) {
        return this.usuarioIdiomasRepositorio.findByUsuarioId(usuarioId);
    }

    public UsuarioIdiomas findByUsuarioIdAndIdiomaId(String usuarioId, String idiomaId) {
        return this.usuarioIdiomasRepositorio.findByUsuarioIdAndIdiomaId(usuarioId, idiomaId);
    }

    public void deleteByIdUsuarioAndIdioma(String idUsuario, String idIdioma) {
        this.deleteByIdUsuarioAndIdioma(idUsuario, idIdioma);
    }

    public String guardarCertificado(GuardarArchivoIdiomasComando guardarArchivoIdiomasComando){
        return this.usuarioIdiomasRepositorio.guardarCertificado(guardarArchivoIdiomasComando);
    }
    public void eliminarCertificado(GuardarArchivoIdiomasComando guardarArchivoIdiomasComando){
        usuarioIdiomasRepositorio.eliminarCertificado(guardarArchivoIdiomasComando);
    }

    public Stream<InformacionIdiomasArchivo> findByUsuarioIn(Collection<String> idLista) {
        return usuarioIdiomasRepositorio.findByUsuarioIn(idLista);
    }
}
