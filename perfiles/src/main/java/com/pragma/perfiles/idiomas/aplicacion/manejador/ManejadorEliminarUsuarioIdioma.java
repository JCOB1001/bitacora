package com.pragma.perfiles.idiomas.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.IdiomaNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioIdiomasNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.useCase.IdiomaCrudUseCase;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarUsuarioIdioma {

    private final IdiomaCrudUseCase idiomaCrudUseCase;
    private final UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public ObjetoRespuesta<UsuarioIdiomas> ejecutar(String idUsuario, String idIdioma) {

        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario == null){
            throw new UsuarioNoEncontrado("El usuario con id " + idUsuario + " no existe en la base de datos");
        }

        Idioma idioma = idiomaCrudUseCase.obtenerPorId(idIdioma);
        if(idioma == null) {
            throw new IdiomaNoEncontrado("El idioma con el ID: "+idIdioma+" no se encuentra registrado");
        }

        UsuarioIdiomas usuarioIdiomas = usuarioIdiomasCrudUseCase.findByUsuarioIdAndIdiomaId(usuario.getId(), idioma.getId());
        if(usuarioIdiomas == null) {
            throw new UsuarioIdiomasNoEncontrado("No existe ningun idioma " + idIdioma + " asignado al usuario " + idUsuario);
        }

        usuarioIdiomasCrudUseCase.eliminarPorId(usuarioIdiomas.getId());

        double valorPorcentajeTotal = manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(idUsuario);
        usuario.setBarraProgreso(valorPorcentajeTotal);
        usuarioCrudUseCase.actualizar(usuario);

        return new ObjetoRespuesta<UsuarioIdiomas>(usuarioIdiomas);
    }
}
