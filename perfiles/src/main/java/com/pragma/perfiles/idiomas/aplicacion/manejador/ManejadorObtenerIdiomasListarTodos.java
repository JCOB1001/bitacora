package com.pragma.perfiles.idiomas.aplicacion.manejador;

import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.useCase.IdiomaCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerIdiomasListarTodos {

    private final IdiomaCrudUseCase idiomaCrudUseCase;

    public ObjetoRespuesta<Stream<Idioma>> ejecutar() {
        //Stream<String> idiomas = idiomaCrudUseCase.obtenerTodos().map(idioma -> idioma.getNombre());
        Stream<Idioma> idiomas = idiomaCrudUseCase.obtenerTodos();
        return new ObjetoRespuesta<Stream<Idioma>>(idiomas);
    }
}
