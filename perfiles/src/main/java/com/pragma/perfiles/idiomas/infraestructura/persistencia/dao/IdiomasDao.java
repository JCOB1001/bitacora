package com.pragma.perfiles.idiomas.infraestructura.persistencia.dao;

import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.entidad.ExperienciaLaboralPragmaEntity;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import org.springframework.data.jpa.repository.JpaRepository;



    public interface IdiomasDao extends JpaRepository<IdiomasEntity, String> {
}
