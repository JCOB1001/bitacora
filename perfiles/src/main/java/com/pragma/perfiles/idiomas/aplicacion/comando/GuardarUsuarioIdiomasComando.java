package com.pragma.perfiles.idiomas.aplicacion.comando;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuardarUsuarioIdiomasComando {

    private String idUsuario;
    private String idIdioma;
    private int nivel;
    private Boolean hasPruebaSuficiencia;
    private String nombrePrueba;
    private String puntaje;

    private String archivo;
    private String extension;


}

