package com.pragma.perfiles.idiomas.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ExperienciaLaboralNoEncontrada;
import com.pragma.perfiles.comun.infraestructura.error.IdiomaNoEncontrado;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.useCase.IdiomaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerIdiomasPorId {

    private final IdiomaCrudUseCase idiomaCrudUseCase;

    public ObjetoRespuesta<Idioma> ejecutar(String idIdioma){
        Idioma idioma = idiomaCrudUseCase.obtenerPorId(idIdioma);

        if(idioma == null){
            throw new IdiomaNoEncontrado("El idioma no se encuentra registrado");
        }

        return new ObjetoRespuesta<>(idioma);
    }



}
