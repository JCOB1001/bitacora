package com.pragma.perfiles.idiomas.infraestructura.persistencia.builder;


import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioEntity;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.UsuarioIdiomasEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface UsuarioIdiomaMapperUsuarioIdiomaEntity extends ConvertidorBase<UsuarioIdiomas, UsuarioIdiomasEntity> {
    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "idUsuario", target = "usuario.id"),
            @Mapping(source = "idIdioma", target = "idioma.id")
    })
    UsuarioIdiomasEntity leftToRight(UsuarioIdiomas usuarioIdiomas);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "usuario.id", target = "idUsuario"),
            @Mapping(source = "idioma.id", target = "idIdioma")
    })
    UsuarioIdiomas rightToLeft(UsuarioIdiomasEntity usuarioIdiomasEntity);

}
