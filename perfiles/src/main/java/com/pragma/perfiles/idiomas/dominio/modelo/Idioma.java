package com.pragma.perfiles.idiomas.dominio.modelo;


import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class Idioma extends Modelo<String> {

    private String nombre;
    private double barraProgreso;
    private LocalDate fechaUltimaActualizacion;

    public Idioma() { super(null); }

    public Idioma(String id, String nombre) {
        super(id);
        this.nombre = nombre;
    }
}
