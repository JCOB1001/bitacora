package com.pragma.perfiles.idiomas.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.stream.Stream;
@RequiredArgsConstructor

public class ManejadorObtenerIdiomasPorIdUsuario {

    private final UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Stream<UsuarioIdiomas>> ejecutar(String idUsuario){
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }
        Stream<UsuarioIdiomas> usuarioIdiomasStream = usuarioIdiomasCrudUseCase.findByUsuarioId(usuario.getId());

        return new ObjetoRespuesta<Stream<UsuarioIdiomas>>(HttpStatus.OK, usuarioIdiomasStream);
    }



}
