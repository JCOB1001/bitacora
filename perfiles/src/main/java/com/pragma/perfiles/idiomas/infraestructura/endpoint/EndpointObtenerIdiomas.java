package com.pragma.perfiles.idiomas.infraestructura.endpoint;

import com.pragma.perfiles.idiomas.aplicacion.manejador.ManejadorObtenerIdiomasListarTodos;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("/idiomas")
@RequiredArgsConstructor
public class EndpointObtenerIdiomas {

    private final ManejadorObtenerIdiomasListarTodos manejadorObtenerIdiomasListarTodos;

    @GetMapping
    public ObjetoRespuesta<Stream<Idioma>> obtenerIdiomas() { return manejadorObtenerIdiomasListarTodos.ejecutar(); }
}
