package com.pragma.perfiles.idiomas.infraestructura.clienteFeing;

import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarArchivoIdiomasComando;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarArchivoComando;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "FeignApi", url = "${feign.modulo.filesApi}/file")
public interface FeignApi {

    @PostMapping
    ResponseEntity<String> fileUpload(@RequestBody GuardarArchivoIdiomasComando file);

    @DeleteMapping
    ResponseEntity deleteFile(@RequestBody GuardarArchivoIdiomasComando file);
}
