package com.pragma.perfiles.idiomas.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UsuarioIdiomas extends Modelo<String> {

    private String idUsuario;
    private String idIdioma;
    private int nivel;
    private Boolean hasPruebaSuficiencia;
    private String nombrePrueba;
    private String puntaje;
    private String urlCertificado;

    public UsuarioIdiomas() {
        super(null);
    }

    public UsuarioIdiomas(String id, String idUsuario, String idIdioma, int nivel, Boolean hasPruebaSuficiencia, String nombrePrueba, String puntaje, String urlCertificado) {
        super(id);
        this.idUsuario = idUsuario;
        this.idIdioma = idIdioma;
        this.nivel = nivel;
        this.hasPruebaSuficiencia = hasPruebaSuficiencia;
        this.nombrePrueba = nombrePrueba;
        this.puntaje = puntaje;
        this.urlCertificado = urlCertificado;
    }
}
