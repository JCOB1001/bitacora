package com.pragma.perfiles.idiomas.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorActualizarUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.idiomas.aplicacion.comando.ActualizarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.manejador.ManejadorActualizarUsuarioIdioma;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
@RestController
@RequestMapping("idiomas")
@RequiredArgsConstructor
@Valid
public class EndpointActualizarUsuarioIdioma {

    private final ManejadorActualizarUsuarioIdioma manejadorActualizarUsuarioIdioma;

    @PutMapping()
    public ObjetoRespuesta<UsuarioIdiomas> actualizarUsuarioIdiomas(@NotNull @RequestBody ActualizarUsuarioIdiomasComando actualizarUsuarioIdiomasComando){

        return manejadorActualizarUsuarioIdioma.ejecutar(actualizarUsuarioIdiomasComando);
    }
}
