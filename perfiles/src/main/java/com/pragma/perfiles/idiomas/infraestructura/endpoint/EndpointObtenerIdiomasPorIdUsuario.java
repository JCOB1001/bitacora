package com.pragma.perfiles.idiomas.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador.ManejadorObtenerExperienciaLaboralPragmaPorIdUsuario;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.idiomas.aplicacion.manejador.ManejadorObtenerIdiomasPorIdUsuario;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;
@RestController
///@RequestMapping("info-laboral-pragma")
@RequestMapping("/idiomas")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerIdiomasPorIdUsuario {

    private final ManejadorObtenerIdiomasPorIdUsuario manejadorObtenerIdiomasPorIdUsuario;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<Stream<UsuarioIdiomas>> ejecutarObtenerIdiomasPorIdUsuario(@NotNull @PathVariable String idUsuario){

        return manejadorObtenerIdiomasPorIdUsuario.ejecutar(idUsuario);
    }


}
