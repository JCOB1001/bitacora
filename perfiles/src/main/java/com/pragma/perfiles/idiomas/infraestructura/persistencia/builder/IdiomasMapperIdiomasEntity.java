package com.pragma.perfiles.idiomas.infraestructura.persistencia.builder;

import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IdiomasMapperIdiomasEntity extends ConvertidorBase<Idioma,IdiomasEntity> {
}

