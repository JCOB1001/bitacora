package com.pragma.perfiles.idiomas.infraestructura.endpoint;

import com.pragma.perfiles.idiomas.aplicacion.manejador.ManejadorObtenerUsuarioIdiomasPorCorreo;
import com.pragma.perfiles.idiomas.dominio.respuesta.UsuarioIdiomasConsultaPragmaData;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("/idiomas")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerIdiomasPorCorreo {
    private final ManejadorObtenerUsuarioIdiomasPorCorreo manejadorObtenerUsuarioIdiomasPorCorreo;

    @GetMapping("/correo/{correoEmpresarial}")
    public ObjetoRespuesta<Stream<UsuarioIdiomasConsultaPragmaData>> ejecutarObtenerIdiomasPorCorreo(@NotNull @PathVariable String correoEmpresarial) {
        return new ObjetoRespuesta<>(HttpStatus.OK, manejadorObtenerUsuarioIdiomasPorCorreo.ejecutar(correoEmpresarial));
    }
}
