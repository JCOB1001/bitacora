package com.pragma.perfiles.idiomas.aplicacion.fabrica;

import com.pragma.perfiles.idiomas.aplicacion.comando.ActualizarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.impl.IdiomasMapperActualizarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.impl.IdiomasMapperInformacionIdiomasArchivo;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.impl.IdiomasMapperUsuarioIdiomasConsultaPragmaData;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.impl.IdiomasMapperUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.respuesta.UsuarioIdiomasConsultaPragmaData;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.UsuarioIdiomasEntity;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionIdiomasArchivo;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class FabricaIdiomas {

    private final IdiomasMapperUsuarioIdiomasComando idiomasMapperUsuarioIdiomasComando;
    private final IdiomasMapperActualizarUsuarioIdiomasComando idiomasMapperActualizarUsuarioIdiomasComando;
    private final IdiomasMapperUsuarioIdiomasConsultaPragmaData idiomasMapperIdiomasUsuarioConsultaPragmaData;
    private final IdiomasMapperInformacionIdiomasArchivo idiomasMapperInformacionIdiomasArchivo;

    public UsuarioIdiomas guardarUsuarioIdiomas(GuardarUsuarioIdiomasComando guardarUsuarioIdiomasComando) {
        return idiomasMapperUsuarioIdiomasComando.rightToLeft(guardarUsuarioIdiomasComando);
    }

    public UsuarioIdiomas actualizarUsuarioIdiomas(ActualizarUsuarioIdiomasComando actualizarUsuarioIdiomasComando) {
        return idiomasMapperActualizarUsuarioIdiomasComando.rightToLeft(actualizarUsuarioIdiomasComando);
    }

    public Stream<UsuarioIdiomasConsultaPragmaData> usuarioIdiomasToUsuarioIdiomasConsultaPragmaData(Iterable<UsuarioIdiomas> usuarioIdiomas) {
        return idiomasMapperIdiomasUsuarioConsultaPragmaData.leftToRight(usuarioIdiomas);
    }

    public Stream<InformacionIdiomasArchivo> idiomaPorInformacionIdiomaArchivo(Iterable<UsuarioIdiomasEntity> usuarioIdiomas) {
        return idiomasMapperInformacionIdiomasArchivo.leftToRight(usuarioIdiomas);
    }
}
