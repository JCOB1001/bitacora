package com.pragma.perfiles.idiomas.aplicacion.fabrica.impl;

import com.pragma.perfiles.idiomas.aplicacion.comando.ActualizarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IdiomasMapperUsuarioIdiomasComando extends ConvertidorBase<UsuarioIdiomas, GuardarUsuarioIdiomasComando> {
}
