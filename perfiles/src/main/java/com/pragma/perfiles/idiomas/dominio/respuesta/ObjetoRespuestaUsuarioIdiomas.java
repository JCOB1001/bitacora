package com.pragma.perfiles.idiomas.dominio.respuesta;

import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaUsuarioIdiomas extends ObjetoRespuesta {

    public ObjetoRespuestaUsuarioIdiomas(Stream<UsuarioIdiomas> datos) {super(datos); }
}
