package com.pragma.perfiles.idiomas.aplicacion.fabrica.impl;

import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.respuesta.UsuarioIdiomasConsultaPragmaData;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface IdiomasMapperUsuarioIdiomasConsultaPragmaData extends ConvertidorBase<UsuarioIdiomas, UsuarioIdiomasConsultaPragmaData> {

    @AfterMapping
    default void hasPruebaSuficiencia(@MappingTarget UsuarioIdiomasConsultaPragmaData usuarioIdiomasConsultaPragmaData, UsuarioIdiomas usuarioIdiomas) {
        if(
            Boolean.FALSE.equals(usuarioIdiomas.getHasPruebaSuficiencia())
                || usuarioIdiomas.getNombrePrueba()==null
                || usuarioIdiomas.getPuntaje()==null
        ) {
            usuarioIdiomas.setNombrePrueba("N/A");
            usuarioIdiomas.setPuntaje("N/A");
        }
        usuarioIdiomasConsultaPragmaData.setNombrePrueba(usuarioIdiomas.getNombrePrueba());
        usuarioIdiomasConsultaPragmaData.setPuntaje(usuarioIdiomas.getPuntaje());
    }

    @Named("leftToRight")
    @Override
    @Mappings({
        @Mapping(source = "idIdioma", target = "idioma")
    })
    UsuarioIdiomasConsultaPragmaData leftToRight(UsuarioIdiomas usuarioIdiomas);

    @Named("rightToLeft")
    @Override
    @Mappings({
        @Mapping(source = "idioma", target = "idIdioma")
    })
    UsuarioIdiomas rightToLeft(UsuarioIdiomasConsultaPragmaData usuarioIdiomasConsultaPragmaData);
}
