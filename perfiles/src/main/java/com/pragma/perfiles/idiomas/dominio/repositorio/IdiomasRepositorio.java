package com.pragma.perfiles.idiomas.dominio.repositorio;

import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;

public interface IdiomasRepositorio extends CrudRepositorioBase<String, Idioma> {
    public Stream<Idioma> findByIdUsuario(String idUsuario);
    UsuarioIdiomas findByIdUsuarioAndNombreSkill(String idUsuario, String nombreSkill);
    Idioma findById(String id);
}
