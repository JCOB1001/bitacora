package com.pragma.perfiles.idiomas.infraestructura.configuracion;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.FabricaIdiomas;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.impl.*;
import com.pragma.perfiles.idiomas.aplicacion.manejador.*;
import com.pragma.perfiles.idiomas.dominio.repositorio.IdiomasRepositorio;
import com.pragma.perfiles.idiomas.dominio.repositorio.UsuarioIdiomasRepositorio;
import com.pragma.perfiles.idiomas.dominio.useCase.IdiomaCrudUseCase;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationIdiomas {

    @Bean
    public IdiomaCrudUseCase idiomaCrudUseCase(IdiomasRepositorio idiomasRepositorio) {
        return new IdiomaCrudUseCase(idiomasRepositorio);
    }

    @Bean
    public ManejadorObtenerIdiomasListarTodos manejadorObtenerIdiomasListarTodos(IdiomaCrudUseCase idiomaCrudUseCase) {
        return new ManejadorObtenerIdiomasListarTodos(idiomaCrudUseCase);
    }

    @Bean
    public UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase(UsuarioIdiomasRepositorio usuarioIdiomasRepositorio) {
        return new UsuarioIdiomasCrudUseCase(usuarioIdiomasRepositorio);
    }

    @Bean
    public FabricaIdiomas fabricaIdiomas(
            IdiomasMapperUsuarioIdiomasComando idiomasMapperUsuarioIdiomasComando,
            IdiomasMapperActualizarUsuarioIdiomasComando idiomasMapperActualizarUsuarioIdiomasComando,
            IdiomasMapperUsuarioIdiomasConsultaPragmaData idiomasMapperUsuarioIdiomasConsultaPragmaData,
            IdiomasMapperInformacionIdiomasArchivo idiomasMapperInformacionIdiomasArchivo
    ) {
        return new FabricaIdiomas(idiomasMapperUsuarioIdiomasComando, idiomasMapperActualizarUsuarioIdiomasComando, idiomasMapperUsuarioIdiomasConsultaPragmaData, idiomasMapperInformacionIdiomasArchivo);
    }

    @Bean
    public ManejadorActualizarUsuarioIdioma manejadorActualizarUsuarioIdioma(
            FabricaUsuarioIdiomas fabricaUsuarioIdiomas,
            UsuarioCrudUseCase usuarioCrudUseCase,
            UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase,
            IdiomaCrudUseCase idiomaCrudUseCase
    ){
        return new ManejadorActualizarUsuarioIdioma(fabricaUsuarioIdiomas,usuarioCrudUseCase,usuarioIdiomasCrudUseCase,idiomaCrudUseCase);
    }

    @Bean
    public  FabricaUsuarioIdiomas fabricaUsuarioIdiomas(
            UsuarioIdiomasMapperActualizarUsuarioIdiomas usuarioIdiomasMapperActualizarUsuarioIdiomas,
            IdiomasMapperGuardarIdiomasComando idiomasMapperGuardarIdiomasComando,
            UsuarioIdiomasMapperGuardarUsuarioIdiomas usuarioIdiomasMapperGuardarUsuarioIdiomas){

        return new FabricaUsuarioIdiomas(usuarioIdiomasMapperActualizarUsuarioIdiomas,
                idiomasMapperGuardarIdiomasComando,usuarioIdiomasMapperGuardarUsuarioIdiomas);
    }
    @Bean
    public ManejadorGuardarUsuarioIdiomas manejadorGuardarUsuarioIdiomas(
            UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase,
            FabricaIdiomas fabricaIdiomas,
            UsuarioCrudUseCase usuarioCrudUseCase,
            IdiomaCrudUseCase idiomaCrudUseCase,
            ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso) {
        return new ManejadorGuardarUsuarioIdiomas(usuarioIdiomasCrudUseCase, fabricaIdiomas, usuarioCrudUseCase, idiomaCrudUseCase, manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public ManejadorObtenerIdiomasPorIdUsuario manejadorObtenerIdiomasPorIdUsuario(
            UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase,
            UsuarioCrudUseCase usuarioCrudUseCase
    ) {
        return new ManejadorObtenerIdiomasPorIdUsuario(usuarioIdiomasCrudUseCase, usuarioCrudUseCase);
    }

    @Bean
    public ManejadorEliminarUsuarioIdioma manejadorEliminarUsuarioIdioma(
            IdiomaCrudUseCase idiomaCrudUseCase,
            UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase,
            UsuarioCrudUseCase usuarioCrudUseCase,
            ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso
    ){
        return new ManejadorEliminarUsuarioIdioma(idiomaCrudUseCase,usuarioIdiomasCrudUseCase, usuarioCrudUseCase, manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public ManejadorObtenerUsuarioIdiomasPorCorreo manejadorObtenerUsuarioIdiomasPorCorreo(
        UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase,
        IdiomaCrudUseCase idiomaCrudUseCase,
        UsuarioCrudUseCase usuarioCrudUseCase,
        FabricaIdiomas fabricaIdiomas
    ) {
        return new ManejadorObtenerUsuarioIdiomasPorCorreo(usuarioIdiomasCrudUseCase, idiomaCrudUseCase, usuarioCrudUseCase, fabricaIdiomas);
    }
}
