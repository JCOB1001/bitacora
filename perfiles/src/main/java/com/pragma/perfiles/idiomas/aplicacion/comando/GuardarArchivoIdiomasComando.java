package com.pragma.perfiles.idiomas.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GuardarArchivoIdiomasComando {

    private String file;
    private String carpeta;
    private String nombreArchivo;
}
