package com.pragma.perfiles.idiomas.dominio.useCase;

import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.repositorio.IdiomasRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class IdiomaCrudUseCase extends CrudUseCaseCommon<IdiomasRepositorio, Idioma, String> {

    private final IdiomasRepositorio idiomasRepositorio;

    public IdiomaCrudUseCase(IdiomasRepositorio idiomasRepositorio) {
        super(idiomasRepositorio);
        this.idiomasRepositorio = idiomasRepositorio;
    }

    public Idioma findById(String id){
        return idiomasRepositorio.findById(id);
    }

    /*public Stream<Idioma> obtenerPorIdIdioma(String id) {
        return idiomasRepositorio.obtenerPorIdIdioma(id);
    }
    public Stream<Idioma> obtenerPorNombre(String nombre) {return idiomasRepositorio.obtenerPorNombre(nombre);}*/
    public Stream<Idioma> findByIdUsuario(String idUsuario){
        return idiomasRepositorio.findByIdUsuario(idUsuario);
    }


}
