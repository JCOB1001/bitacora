package com.pragma.perfiles.idiomas.aplicacion.fabrica.impl;

import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarConocimientoComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarArchivoIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface IdiomasMapperGuardarIdiomasComando extends ConvertidorBase<Idioma, GuardarUsuarioIdiomasComando> {
    Idioma righToLeft(GuardarArchivoIdiomasComando guardarIdiomasComando);
}
