package com.pragma.perfiles.idiomas.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.IdiomaExistente;
import com.pragma.perfiles.comun.infraestructura.error.IdiomaNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.NivelNoValido;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarArchivoIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.FabricaIdiomas;
import com.pragma.perfiles.idiomas.dominio.modelo.Idioma;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.useCase.IdiomaCrudUseCase;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.otros_estudios.infraestructura.configuracion.TextosComunes;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.UUID;

@RequiredArgsConstructor
public class ManejadorGuardarUsuarioIdiomas {

    private final UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase;
    private final FabricaIdiomas fabricaIdiomas;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final IdiomaCrudUseCase idiomaCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public ObjetoRespuesta<UsuarioIdiomas> ejecutar(GuardarUsuarioIdiomasComando guardarUsuarioIdiomasComando) {

        if(!(guardarUsuarioIdiomasComando.getNivel() >= 1 && guardarUsuarioIdiomasComando.getNivel() <= 5)){
            throw new NivelNoValido("El rango de niveles permitidos es entre 1 y 5");
        }

        Usuario usuario = usuarioCrudUseCase.obtenerPorId(guardarUsuarioIdiomasComando.getIdUsuario());
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }

        Idioma idioma = idiomaCrudUseCase.findById(guardarUsuarioIdiomasComando.getIdIdioma());
        if (idioma == null){
            throw new IdiomaNoEncontrado("El idioma no existe en la base de datos");
        }

        UsuarioIdiomas usuarioIdiomasValidacion = usuarioIdiomasCrudUseCase.findByUsuarioIdAndIdiomaId(
                usuario.getId(),
                idioma.getId()
        );
        if(usuarioIdiomasValidacion != null){
            throw new IdiomaExistente("El usuario ya tiene registrado el idioma con id " + idioma.getId());
        }

        String urlFormateada="https://d2y95fubzeqro6.cloudfront.net/general/certicados/20200327120334.";
        if(guardarUsuarioIdiomasComando.getHasPruebaSuficiencia() != null
                && guardarUsuarioIdiomasComando.getHasPruebaSuficiencia().booleanValue()) {

            //tieneArchivo
            if (guardarUsuarioIdiomasComando.getArchivo() != null
                    && guardarUsuarioIdiomasComando.getExtension() != null) {
                GuardarArchivoIdiomasComando guardarArchivoIdiomasComando = GuardarArchivoIdiomasComando.builder()
                        .nombreArchivo(UUID.randomUUID().toString()+guardarUsuarioIdiomasComando.getExtension())
                        .carpeta(TextosComunes.CARPETA_GUARDAR_CERTIFICADO_IDIOMA)
                        .file(guardarUsuarioIdiomasComando.getArchivo()).build();

                String url = usuarioIdiomasCrudUseCase.guardarCertificado(guardarArchivoIdiomasComando);
                urlFormateada = formatUrl(url);
            }
        }

        UsuarioIdiomas usuarioIdiomas = fabricaIdiomas.guardarUsuarioIdiomas(guardarUsuarioIdiomasComando);
        usuarioIdiomas.setUrlCertificado(urlFormateada);
        UsuarioIdiomas nuevoUsuarioIdioma = usuarioIdiomasCrudUseCase.guardar(usuarioIdiomas);

        double valorPorcentajeTotal = manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(guardarUsuarioIdiomasComando.getIdUsuario());
        usuario.setBarraProgreso(valorPorcentajeTotal);
        usuarioCrudUseCase.actualizar(usuario);

        return new ObjetoRespuesta<UsuarioIdiomas>(HttpStatus.OK, nuevoUsuarioIdioma);
    }

    public String formatUrl(String url){
        String res[]=url.replace("{","").replace("}","").replace("\"","").split("/");
        String respuesta="https://";
        for (int i=2;i<res.length;i++ ){
            respuesta+=res[i];
            if(i<res.length-1){
                respuesta+="/";}
        }
        return respuesta;
    }
}
