package com.pragma.perfiles.conocimientos.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioConocimientoNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.FabricaUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ManejadorActualizarUsuarioConocimiento {

    private final FabricaUsuarioConocimiento fabricaUsuarioConocimiento;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final UsuarioConocimientoCrudUseCase estudioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    private final ManejadorGuardarUsuarioConocimiento manejadorGuardarUsuarioConocimiento;

    public ObjetoRespuesta<UsuarioConocimiento> ejecutar(ActualizarUsuarioConocimientoComando actualizarUsuarioConocimientoComando){
        Usuario usuario = Optional.ofNullable(usuarioCrudUseCase.obtenerPorId(actualizarUsuarioConocimientoComando.getIdUsuario()))
            .orElseThrow(()-> new UsuarioNoEncontrado("El usuario con el ID "+
                actualizarUsuarioConocimientoComando.getIdUsuario()+" no se encuentra registrado"));

        List<UsuarioConocimiento> usuarioConocimientos = estudioCrudUseCase.findByUsuarioId(usuario.getId())
            .filter(uc -> uc.getNombreSkill().equals(actualizarUsuarioConocimientoComando.getNombreSkill())).collect(Collectors.toList());

        if (usuarioConocimientos.isEmpty()){
            GuardarUsuarioConocimientoComando gucc = new
                    GuardarUsuarioConocimientoComando(
                    actualizarUsuarioConocimientoComando.getNombreSkill(),
                    actualizarUsuarioConocimientoComando.getPuntaje(),
                    actualizarUsuarioConocimientoComando.getIdUsuario());
            return  new ObjetoRespuesta<UsuarioConocimiento>(manejadorGuardarUsuarioConocimiento.ejecutar(gucc).getDato());
        }

        UsuarioConocimiento usuarioConocimiento = usuarioConocimientos.get(0);

        UsuarioConocimiento usuarioConocimientoNuevo = fabricaUsuarioConocimiento.ActualizarUsuarioConocimiento(actualizarUsuarioConocimientoComando);
        usuarioConocimientoNuevo.setId(usuarioConocimiento.getId());
        usuarioConocimientoNuevo.setIndice(usuarioConocimiento.getIndice());
        estudioCrudUseCase.actualizar(usuarioConocimientoNuevo);
        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.actualizar(usuario);

        return  new ObjetoRespuesta<UsuarioConocimiento>(usuarioConocimientoNuevo);
    }

}
