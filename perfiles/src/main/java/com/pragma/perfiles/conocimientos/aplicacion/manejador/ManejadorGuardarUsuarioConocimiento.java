package com.pragma.perfiles.conocimientos.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioConocimientoNoValido;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.FabricaUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.useCase.ConocimientosCrudUseCase;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ManejadorGuardarUsuarioConocimiento {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ConocimientosCrudUseCase conocimientosCrudUseCase;
    private final UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase;
    private final FabricaUsuarioConocimiento fabricaUsuarioConocimiento;
    private final int vecesRequeridasParaInsertar=4;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public ObjetoRespuesta<UsuarioConocimiento> ejecutar(GuardarUsuarioConocimientoComando guardarUsuarioConocimientoComando){
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(guardarUsuarioConocimientoComando.getIdUsuario());

        if(usuario==null){
            throw new  UsuarioNoEncontrado("El usuario con el id "+guardarUsuarioConocimientoComando.getIdUsuario()+" no se encuentra registrado");
        }

        if(guardarUsuarioConocimientoComando.getNombreSkill()==null || guardarUsuarioConocimientoComando.getNombreSkill().isBlank()){
             throw new UsuarioConocimientoNoValido("El conocimiento no es valido");
        }

        Conocimientos conocimiento=conocimientosCrudUseCase.obtenerPorNombre(guardarUsuarioConocimientoComando.getNombreSkill());

        if(conocimiento==null){
            int vecesInsertadas= usuarioConocimientoCrudUseCase.obtenerCatidadVecesUsadas(guardarUsuarioConocimientoComando.getNombreSkill());
            if(vecesInsertadas>=4){
                conocimiento=new Conocimientos();
                conocimiento.setNombre(guardarUsuarioConocimientoComando.getNombreSkill());
                conocimientosCrudUseCase.guardar(conocimiento);
            }
        }
        UsuarioConocimiento usuarioConocimiento=fabricaUsuarioConocimiento.guardarUsuarioConocimiento(guardarUsuarioConocimientoComando);
        usuarioConocimiento.setIndice(usuarioConocimientoCrudUseCase.findByUsuarioId(guardarUsuarioConocimientoComando.getIdUsuario()).collect(Collectors.toList()).size());
        usuarioConocimientoCrudUseCase.guardar(usuarioConocimiento);
        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.actualizar(usuario);


        return new ObjetoRespuesta<>(usuarioConocimiento);
    }

}
