package com.pragma.perfiles.conocimientos.infraestructura.persistencia.dao;

import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.UsuarioConocimientoEntity;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.UsuarioIdiomasEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UsuarioConocimientoDao extends JpaRepository<UsuarioConocimientoEntity,String>, UsuarioConocimientoCustomDao {

    Iterable<UsuarioConocimientoEntity> findByUsuarioId(String usuarioid);

    @Query("select count(uct) from UsuarioConocimientoEntity uct where uct.nombreSkill=?1")
    int cantidadVecesIngresadas(String nombreSkill);

    UsuarioConocimientoEntity findByUsuarioIdAndNombreSkill(String usuarioid, String nombreSkill);

    List<UsuarioConocimientoEntity> findAllByUsuarioIdIn(List<String> ids);

    Iterable<UsuarioConocimientoEntity> findByUsuarioIdIn(Collection<String> idLista);

}
