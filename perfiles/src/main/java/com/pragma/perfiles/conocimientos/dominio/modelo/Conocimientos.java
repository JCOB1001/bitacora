package com.pragma.perfiles.conocimientos.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Conocimientos  extends Modelo<String> {

    private String nombre;
    private String enlace;

    public Conocimientos(){ super(null); }

    public Conocimientos(String id, String nombre, String enlace){
        super(id);
        this.nombre = nombre;
        this.enlace = enlace;
    }
}
