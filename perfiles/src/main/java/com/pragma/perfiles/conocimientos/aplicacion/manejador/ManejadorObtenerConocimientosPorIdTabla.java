package com.pragma.perfiles.conocimientos.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.useCase.ConocimientosCrudUseCase;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.Comparator;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerConocimientosPorIdTabla {
    private final ConocimientosCrudUseCase conocimientosCrudUseCase;

    public Conocimientos ejecutar(String id){
        return conocimientosCrudUseCase.obtenerPorId(id);
    }
}
