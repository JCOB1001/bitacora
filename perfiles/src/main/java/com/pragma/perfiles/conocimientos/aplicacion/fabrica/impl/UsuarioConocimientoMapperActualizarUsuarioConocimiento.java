package com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl;

import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioConocimientoMapperActualizarUsuarioConocimiento extends ConvertidorBase<UsuarioConocimiento, ActualizarUsuarioConocimientoComando> {
}
