package com.pragma.perfiles.conocimientos.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.InteresesNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarIndicesUsuarioConocimientosComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ManejadorEliminarUsuarioConocimiento {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;
    private final ManejadorActualizarIndicesUsuarioConocimientos manejadorActualizarIndicesUsuarioConocimientos;

    public ObjetoRespuesta<UsuarioConocimiento> ejecutar(String idUsuario, String nombreSkill) {

        Usuario usuario=usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+idUsuario+" no se encuntra registrado");
        }

        UsuarioConocimiento usuarioConocimiento = usuarioConocimientoCrudUseCase.findByUsuarioIdAndNombreSkill(usuario.getId(), nombreSkill);
        if(nombreSkill == null){
            throw  new InteresesNoEncontrado("No existe ningun interes con el UserId " + idUsuario
                    + " y nombre " + nombreSkill);
        }

        List<String> usuarioConocimientos = usuarioConocimientoCrudUseCase.findByUsuarioId(usuario.getId())
            .sorted(Comparator.comparing(UsuarioConocimiento::getIndice)).map(UsuarioConocimiento::getNombreSkill).collect(Collectors.toList());

        usuarioConocimientos.remove(usuarioConocimiento.getNombreSkill());

        usuarioConocimientoCrudUseCase.eliminarPorId(usuarioConocimiento.getId());
        manejadorActualizarIndicesUsuarioConocimientos.ejecutar(new ActualizarIndicesUsuarioConocimientosComando(usuario.getId(), usuarioConocimientos));
        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.actualizar(usuario);

        return new ObjetoRespuesta<UsuarioConocimiento>(usuarioConocimiento);
    }
}
