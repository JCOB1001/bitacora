package com.pragma.perfiles.conocimientos.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarIndicesUsuarioConocimientosComando {
    private String idUsuario;
    private List<String> skillsName;
}
