package com.pragma.perfiles.conocimientos.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioConocimientoNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarIndicesUsuarioConocimientosComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ManejadorActualizarIndicesUsuarioConocimientos {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final UsuarioConocimientoCrudUseCase estudioCrudUseCase;

    public RespuestaBase ejecutar(ActualizarIndicesUsuarioConocimientosComando actualizarIndicesUsuarioConocimientosComando) {
        Optional.ofNullable(usuarioCrudUseCase.obtenerPorId(actualizarIndicesUsuarioConocimientosComando.getIdUsuario()))
            .orElseThrow(()-> new UsuarioNoEncontrado("El usuario con el ID "+
                actualizarIndicesUsuarioConocimientosComando.getIdUsuario()+" no se encuentra registrado"));

        List<UsuarioConocimiento> usuarioConocimientos = estudioCrudUseCase
            .findByUsuarioId(actualizarIndicesUsuarioConocimientosComando.getIdUsuario()).collect(Collectors.toList());

        usuarioConocimientos
            .forEach(skill -> {
                if(!actualizarIndicesUsuarioConocimientosComando.getSkillsName().contains(skill.getNombreSkill())){
                    throw new UsuarioConocimientoNoEncontrado("El usuario con id: "+
                        actualizarIndicesUsuarioConocimientosComando.getIdUsuario()+" no posee todas los conocimientos enviados");
                }

                skill.setIndice(actualizarIndicesUsuarioConocimientosComando.getSkillsName().indexOf(skill.getNombreSkill()));
            });

        estudioCrudUseCase.guardarTodo(usuarioConocimientos);

        return new RespuestaBase("OK");
    }
}
