package com.pragma.perfiles.conocimientos.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.conocimientos.aplicacion.fabrica.FabricaUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.repositorio.UsuarioConocimientoRepositorio;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.builder.UsuarioConocimientoMapperUsuarioConocimientoEntity;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.dao.UsuarioConocimientoDao;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.UsuarioConocimientoEntity;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.infraestructura.persistencia.builder.UsuarioMapperUsuarioEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionConocimientoArchivo;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class UsuarioConocimientoRepositorioImpl extends CrudRepositorioBaseImpl<String, UsuarioConocimiento, UsuarioConocimientoEntity> implements UsuarioConocimientoRepositorio {

    private final UsuarioConocimientoDao usuarioConocimientoDao;
    private final UsuarioConocimientoMapperUsuarioConocimientoEntity usuarioConocimientoMapperUsuarioConocimientoEntity;
    private final UsuarioMapperUsuarioEntity usuarioMapperUsuarioEntity;
    private final FabricaUsuarioConocimiento fabricaUsuarioConocimiento;

    @Override
    protected UsuarioConocimientoDao obtenerRepositorio() {
        return usuarioConocimientoDao;
    }

    @Override
    protected UsuarioConocimientoMapperUsuarioConocimientoEntity obtenerConversionBase() {
        return usuarioConocimientoMapperUsuarioConocimientoEntity;
    }

    @Override
    public Stream<UsuarioConocimiento> findByUsuarioId(String usuarioId) {
        return usuarioConocimientoMapperUsuarioConocimientoEntity.rightToLeft(
                usuarioConocimientoDao.findByUsuarioId(usuarioId)
        );
    }

    @Override

    public int vecesInsertadas(String nombre) {
        return usuarioConocimientoDao.cantidadVecesIngresadas(nombre);
    }

    @Override
    public Stream<UsuarioConocimiento> buscarHabilidadesPorListaId(List<String> usuarios) {
        return usuarioConocimientoDao
                .findAllByUsuarioIdIn(usuarios)
                .stream()
                .map(u->usuarioConocimientoMapperUsuarioConocimientoEntity.rightToLeft(u));
    }

    public UsuarioConocimiento findByUsuarioIdAndNombreSkill(String usuarioId, String nombreSkill) {
        return usuarioConocimientoMapperUsuarioConocimientoEntity.rightToLeft(
                usuarioConocimientoDao.findByUsuarioIdAndNombreSkill(usuarioId, nombreSkill));
    }

    public Stream<InformacionConocimientoArchivo> findByUsuarioIn(Collection<String> idLista){
        return fabricaUsuarioConocimiento.conocimientoPorInformacionConocimientoArchivo(usuarioConocimientoDao.findByUsuarioIdIn(idLista));
    }

}
