package com.pragma.perfiles.conocimientos.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorGuardarUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("conocimientos")
@RequiredArgsConstructor
@Valid
public class EndpointGuardarUsuarioConocimiento {

    private final ManejadorGuardarUsuarioConocimiento manejadorGuardarUsuarioConocimiento;

    @PostMapping
    public ObjetoRespuesta<UsuarioConocimiento> guardarUsuarioConocimiento(@NotNull @RequestBody GuardarUsuarioConocimientoComando guardarUsuarioConocimientoComando){

        return manejadorGuardarUsuarioConocimiento.ejecutar(guardarUsuarioConocimientoComando);
    }
}
