package com.pragma.perfiles.conocimientos.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.Comparator;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerConocimientosPorId {
    private final UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public Stream<UsuarioConocimiento> ejecutar(String idUsuario){
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(idUsuario);

        if (usuario == null){
            throw new UsuarioNoEncontrado("El usuario no existe");
        }

        return usuarioConocimientoCrudUseCase.findByUsuarioId(usuario.getId())
            .sorted(Comparator.comparing(UsuarioConocimiento::getIndice));
    }
}
