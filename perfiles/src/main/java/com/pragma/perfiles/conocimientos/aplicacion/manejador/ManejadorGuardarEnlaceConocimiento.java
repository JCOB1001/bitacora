package com.pragma.perfiles.conocimientos.aplicacion.manejador;

import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarEnlaceConocimientoComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.dominio.useCase.ConocimientosCrudUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorGuardarEnlaceConocimiento {

    private final ConocimientosCrudUseCase conocimientosCrudUseCase;

    public Conocimientos ejecutar(
            GuardarEnlaceConocimientoComando guardarEnlaceConocimientoComando) {
        Conocimientos conocimiento = conocimientosCrudUseCase.obtenerPorNombre(guardarEnlaceConocimientoComando.getNombre());
        conocimiento.setEnlace(guardarEnlaceConocimientoComando.getEnlace());
        return conocimientosCrudUseCase.guardar(conocimiento);
    }
}
