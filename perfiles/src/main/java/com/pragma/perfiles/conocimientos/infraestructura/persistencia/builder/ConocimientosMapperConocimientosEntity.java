package com.pragma.perfiles.conocimientos.infraestructura.persistencia.builder;

import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.ConocimientosEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ConocimientosMapperConocimientosEntity extends ConvertidorBase<Conocimientos, ConocimientosEntity> {
}
