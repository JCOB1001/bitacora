package com.pragma.perfiles.conocimientos.dominio.useCase;

import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.repositorio.UsuarioConocimientoRepositorio;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionConocimientoArchivo;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.Collection;
import java.util.stream.Stream;

public class UsuarioConocimientoCrudUseCase extends CrudUseCaseCommon<UsuarioConocimientoRepositorio, UsuarioConocimiento, String> {

    private final UsuarioConocimientoRepositorio usuarioConocimientoRepositorio;

    public UsuarioConocimientoCrudUseCase(UsuarioConocimientoRepositorio usuarioConocimientoRepositorio){
        super(usuarioConocimientoRepositorio);
        this.usuarioConocimientoRepositorio=usuarioConocimientoRepositorio;
    }

    public UsuarioConocimiento findByUsuarioIdAndNombreSkill(String usuario, String nombreSkill) {
        return this.usuarioConocimientoRepositorio.findByUsuarioIdAndNombreSkill(usuario,nombreSkill);
    }

    public Stream<UsuarioConocimiento> findByUsuarioId(String usuario){
        return  this.usuarioConocimientoRepositorio.findByUsuarioId(usuario);
    }

    public int obtenerCatidadVecesUsadas(String nombre) {
        return usuarioConocimientoRepositorio.vecesInsertadas(nombre);
    }

    public Stream<InformacionConocimientoArchivo> findByUsuarioIn(Collection<String> idLista){
        return usuarioConocimientoRepositorio.findByUsuarioIn(idLista);
    }
}
