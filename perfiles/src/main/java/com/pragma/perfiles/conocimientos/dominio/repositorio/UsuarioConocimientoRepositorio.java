package com.pragma.perfiles.conocimientos.dominio.repositorio;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionConocimientoArchivo;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public interface UsuarioConocimientoRepositorio extends CrudRepositorioBase<String, UsuarioConocimiento> {
     Stream<UsuarioConocimiento> findByUsuarioId(String usuarioId);
     UsuarioConocimiento findByUsuarioIdAndNombreSkill(String usuarioId, String nombreSkill);
     int vecesInsertadas (String nombre);
     Stream<UsuarioConocimiento> buscarHabilidadesPorListaId(List<String> ids);
     Stream<InformacionConocimientoArchivo> findByUsuarioIn(Collection<String> idLista);

}
