package com.pragma.perfiles.conocimientos.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorObtenerConocimientosPorId;
import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorObtenerConocimientosPorIdTabla;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;


@RestController
@RequestMapping("conocimientos")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerConocimientosPorIdTabla {

    private final ManejadorObtenerConocimientosPorIdTabla
            manejadorObtenerConocimientosPorIdTabla;

    @GetMapping("/idTabla/{id}")
    public ObjetoRespuesta<Conocimientos> ejecutarObtenerConocimientosPorId(@NotNull @PathVariable String id){
        Conocimientos conocimientos = manejadorObtenerConocimientosPorIdTabla
                .ejecutar(id);
        return new ObjetoRespuesta<>(HttpStatus.OK, conocimientos);
    }
}
