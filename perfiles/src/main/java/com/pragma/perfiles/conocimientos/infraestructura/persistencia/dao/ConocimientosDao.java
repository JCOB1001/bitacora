package com.pragma.perfiles.conocimientos.infraestructura.persistencia.dao;

import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.ConocimientosEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ConocimientosDao extends JpaRepository<ConocimientosEntity, String> {

    public ConocimientosEntity findByNombre(String nombre);
    public Optional<ConocimientosEntity> findById(String id);
}
