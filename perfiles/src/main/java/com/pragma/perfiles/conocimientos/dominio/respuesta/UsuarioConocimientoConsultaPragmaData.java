package com.pragma.perfiles.conocimientos.dominio.respuesta;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UsuarioConocimientoConsultaPragmaData {
    private String skill_name;
    private int rate;
}
