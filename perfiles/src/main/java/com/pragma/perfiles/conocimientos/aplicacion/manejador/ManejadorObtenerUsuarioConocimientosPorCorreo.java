package com.pragma.perfiles.conocimientos.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.respuesta.UsuarioConocimientoConsultaPragmaData;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.idiomas.aplicacion.fabrica.FabricaIdiomas;
import com.pragma.perfiles.idiomas.dominio.respuesta.UsuarioIdiomasConsultaPragmaData;
import com.pragma.perfiles.idiomas.dominio.useCase.IdiomaCrudUseCase;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioConocimientosPorCorreo {
    private final UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public Stream<UsuarioConocimientoConsultaPragmaData> ejecutar(String correoEmpresarial){
        Usuario user = usuarioCrudUseCase.findByCorreoEmpresarial(correoEmpresarial);
        Stream<UsuarioConocimiento> usuarios = usuarioConocimientoCrudUseCase.findByUsuarioId(user.getId());
        ArrayList<UsuarioConocimientoConsultaPragmaData> usuarioRespuesta = new ArrayList<>();
        for(UsuarioConocimiento u : usuarios.collect(Collectors.toList())){
            UsuarioConocimientoConsultaPragmaData usuarioConocimientoConsultaPragmaData = new UsuarioConocimientoConsultaPragmaData(u.getNombreSkill(),u.getPuntaje());
            usuarioRespuesta.add(usuarioConocimientoConsultaPragmaData);
        }
        return usuarioRespuesta.stream();
    }
}
