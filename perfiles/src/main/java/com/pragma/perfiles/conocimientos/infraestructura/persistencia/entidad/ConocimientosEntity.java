package com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad;

import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "conocimientos",
        uniqueConstraints={@UniqueConstraint(
                name = "uq_nombre",
                columnNames={"nombre"})
})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConocimientosEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos {
        String NOMBRE = "nombre";
        String ENLACE = "enlace";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String nombre;
    @Column(name = "enlace")
    private String enlace;

}
