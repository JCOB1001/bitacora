package com.pragma.perfiles.conocimientos.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarUsuarioConocimientoComando {

    private String idUsuario;
    private String nombreSkill;
    private int puntaje;

}
