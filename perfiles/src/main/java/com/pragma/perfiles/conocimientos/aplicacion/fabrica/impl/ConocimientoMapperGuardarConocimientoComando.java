package com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl;

import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarConocimientoComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ConocimientoMapperGuardarConocimientoComando extends ConvertidorBase<Conocimientos, GuardarConocimientoComando> {
}
