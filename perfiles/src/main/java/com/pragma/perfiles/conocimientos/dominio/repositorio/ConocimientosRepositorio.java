package com.pragma.perfiles.conocimientos.dominio.repositorio;

import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface ConocimientosRepositorio  extends CrudRepositorioBase<String, Conocimientos> {

        Conocimientos obtenerPorId(String id);
        Conocimientos obtenerPorNombre(String nombre);

}
