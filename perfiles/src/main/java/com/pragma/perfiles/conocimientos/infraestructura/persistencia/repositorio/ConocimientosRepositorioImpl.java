package com.pragma.perfiles.conocimientos.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.repositorio.ConocimientosRepositorio;
import com.pragma.perfiles.conocimientos.dominio.repositorio.UsuarioConocimientoRepositorio;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.builder.ConocimientosMapperConocimientosEntity;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.dao.ConocimientosDao;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.ConocimientosEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class ConocimientosRepositorioImpl extends CrudRepositorioBaseImpl<String, Conocimientos, ConocimientosEntity> implements ConocimientosRepositorio {

    private final ConocimientosDao conocimientosDao;
    private final ConocimientosMapperConocimientosEntity conocimientosMapperConocimientosEntity;

    @Override
    protected ConocimientosDao obtenerRepositorio() {
        return conocimientosDao;
    }

    @Override
    protected ConocimientosMapperConocimientosEntity obtenerConversionBase() {
        return conocimientosMapperConocimientosEntity;
    }

    @Override
    public Conocimientos obtenerPorNombre(String nombre){
        return conocimientosMapperConocimientosEntity.rightToLeft(conocimientosDao.findByNombre(nombre));
    }

    @Override
    public Conocimientos obtenerPorId (String id){
        return conocimientosMapperConocimientosEntity.rightToLeft(conocimientosDao.findById(id).get());
    }

}
