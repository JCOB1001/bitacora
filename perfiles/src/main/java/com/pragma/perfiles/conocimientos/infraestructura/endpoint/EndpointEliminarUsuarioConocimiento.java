package com.pragma.perfiles.conocimientos.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorEliminarUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.intereses.aplicacion.manejador.ManejadorEliminarIntereses;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("conocimientos")
@RequiredArgsConstructor
@Valid
public class EndpointEliminarUsuarioConocimiento {

    private final ManejadorEliminarUsuarioConocimiento manejadorEliminarUsuarioConocimiento;

    @DeleteMapping("/{idUsuario}/{nombreSkill}")
    public ObjetoRespuesta<UsuarioConocimiento> eliminarIntereses(@NotNull @PathVariable String idUsuario,
                                                                  @NotNull @PathVariable String nombreSkill){

        return manejadorEliminarUsuarioConocimiento.ejecutar(idUsuario, nombreSkill);
    }

}
