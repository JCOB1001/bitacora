package com.pragma.perfiles.conocimientos.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarEnlaceConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorGuardarEnlaceConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("conocimientos")
@RequiredArgsConstructor
@Valid
public class EndpointGuardarEnlaceConocimiento {

    private final ManejadorGuardarEnlaceConocimiento manejadorGuardarEnlaceConocimiento;

    @PostMapping("/enlace")
    public ObjetoRespuesta<Conocimientos> guardarEnlaceConocimiento(@NotNull @RequestBody GuardarEnlaceConocimientoComando guardarEnlaceConocimientoComando){
        Conocimientos conocimiento = manejadorGuardarEnlaceConocimiento.ejecutar(guardarEnlaceConocimientoComando);
        return new ObjetoRespuesta<>(HttpStatus.OK, conocimiento);
    }

}
