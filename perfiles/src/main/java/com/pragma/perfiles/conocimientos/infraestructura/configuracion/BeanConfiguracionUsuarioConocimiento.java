package com.pragma.perfiles.conocimientos.infraestructura.configuracion;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.FabricaUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl.ConocimientoMapperInformacionConocimientoArchivo;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl.UsuarioConocimientoMapperActualizarUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl.UsuarioConocimientoMapperGuardarUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.aplicacion.manejador.*;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl.ConocimientoMapperGuardarConocimientoComando;
import com.pragma.perfiles.conocimientos.dominio.repositorio.ConocimientosRepositorio;
import com.pragma.perfiles.conocimientos.dominio.repositorio.UsuarioConocimientoRepositorio;
import com.pragma.perfiles.conocimientos.dominio.useCase.ConocimientosCrudUseCase;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguracionUsuarioConocimiento {

    @Bean
    public UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase(UsuarioConocimientoRepositorio usuarioConocimientoRepositorio){
        return new UsuarioConocimientoCrudUseCase(usuarioConocimientoRepositorio);
    }

    @Bean
    public  ConocimientosCrudUseCase conocimientosCrudUseCase(ConocimientosRepositorio conocimientosRepositorio){
        return new ConocimientosCrudUseCase(conocimientosRepositorio);
    }

    @Bean
    public  FabricaUsuarioConocimiento fabricaUsuarioConocimiento(
            UsuarioConocimientoMapperActualizarUsuarioConocimiento usuarioConocimientoMapperActualizarUsuarioConocimiento,
            ConocimientoMapperGuardarConocimientoComando conocimientoMapperGuardarConocimientoComando,
            UsuarioConocimientoMapperGuardarUsuarioConocimiento usuarioConocimientoMapperGuardarUsuarioConocimiento,
            ConocimientoMapperInformacionConocimientoArchivo conocimientoMapperInformacionConocimientoArchivo){

        return new FabricaUsuarioConocimiento(usuarioConocimientoMapperActualizarUsuarioConocimiento,
                conocimientoMapperGuardarConocimientoComando,usuarioConocimientoMapperGuardarUsuarioConocimiento,
                conocimientoMapperInformacionConocimientoArchivo);
    }

    @Bean
    public ManejadorObtenerConocimientosListarTodo manejadorObtenerConocimientosListarTodo(ConocimientosCrudUseCase conocimientosCrudUseCase){
        return new ManejadorObtenerConocimientosListarTodo(conocimientosCrudUseCase);
    }

    @Bean
    public ManejadorObtenerConocimientosPorId manejadorObtenerConocimientosPorId(UsuarioCrudUseCase usuarioCrudUseCase, UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase){
        return new ManejadorObtenerConocimientosPorId(usuarioConocimientoCrudUseCase,usuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerConocimientosPorIdTabla manejadorObtenerConocimientosPorIdTabla(ConocimientosCrudUseCase conocimientosCrudUseCase){
        return new ManejadorObtenerConocimientosPorIdTabla(conocimientosCrudUseCase);
    }

    @Bean
    public ManejadorActualizarUsuarioConocimiento manejadorActualizarUsuarioConocimiento(FabricaUsuarioConocimiento fabricaUsuarioConocimiento,
                                                                                         UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                         UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase,
                                                                                         ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso, ManejadorGuardarUsuarioConocimiento manejadorGuardarUsuarioConocimiento){
        return new ManejadorActualizarUsuarioConocimiento(fabricaUsuarioConocimiento,usuarioCrudUseCase,usuarioConocimientoCrudUseCase, manejadorCalcularValorBarraProgreso, manejadorGuardarUsuarioConocimiento);
    }

    @Bean
    public ManejadorGuardarConocimiento ManejadorGuardarConocimiento(
            FabricaUsuarioConocimiento fabricaUsuarioConocimiento,
            ConocimientosCrudUseCase conocimientosCrudUseCase) {

        return new ManejadorGuardarConocimiento(fabricaUsuarioConocimiento,
                conocimientosCrudUseCase);
    }

    @Bean
   public  ManejadorGuardarUsuarioConocimiento guardarUsuarioConocimiento(UsuarioCrudUseCase usuarioCrudUseCase,FabricaUsuarioConocimiento fabricaUsuarioConocimiento,
                                                                   UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase,
                                                                   ConocimientosCrudUseCase conocimientosCrudUseCase,
                                                                   ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso){
        return new ManejadorGuardarUsuarioConocimiento(usuarioCrudUseCase,conocimientosCrudUseCase,usuarioConocimientoCrudUseCase,fabricaUsuarioConocimiento,manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public ManejadorEliminarUsuarioConocimiento manejadorEliminarUsuarioConocimiento(UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                     UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase,
                                                                                     ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso,
                                                                                     ManejadorActualizarIndicesUsuarioConocimientos manejadorActualizarIndicesUsuarioConocimientos){

        return new ManejadorEliminarUsuarioConocimiento(usuarioCrudUseCase,usuarioConocimientoCrudUseCase,manejadorCalcularValorBarraProgreso, manejadorActualizarIndicesUsuarioConocimientos);
    }

    @Bean
    public ManejadorObtenerUsuarioConocimientosListarTodo manejadorObtenerUsuarioConocimientosListarTodo(UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase) {
        return new ManejadorObtenerUsuarioConocimientosListarTodo(usuarioConocimientoCrudUseCase);
    }

    @Bean
    public ManejadorActualizarIndicesUsuarioConocimientos manejadorActualizarIndicesUsuarioConocimientos (UsuarioCrudUseCase usuarioCrudUseCase, UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase) {
        return new ManejadorActualizarIndicesUsuarioConocimientos(usuarioCrudUseCase, usuarioConocimientoCrudUseCase);
    }


    @Bean
    public ManejadorGuardarEnlaceConocimiento manejadorGuardarEnlaceConocimiento(ConocimientosCrudUseCase conocimientosCrudUseCase){
        return new ManejadorGuardarEnlaceConocimiento(conocimientosCrudUseCase);
    }

    @Bean
    public ManejadorObtenerUsuarioConocimientosPorCorreo manejadorObtenerUsuarioConocimientosPorCorreo(UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase, UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerUsuarioConocimientosPorCorreo(usuarioConocimientoCrudUseCase, usuarioCrudUseCase);
    }
}
