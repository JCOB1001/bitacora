package com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl;

import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.ConocimientosEntity;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.UsuarioConocimientoEntity;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionConocimientoArchivo;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ConocimientoMapperInformacionConocimientoArchivo extends ConvertidorBase<UsuarioConocimientoEntity, InformacionConocimientoArchivo> {
}
