package com.pragma.perfiles.conocimientos.dominio.useCase;

import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.dominio.repositorio.ConocimientosRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

public class ConocimientosCrudUseCase extends CrudUseCaseCommon<ConocimientosRepositorio,Conocimientos,String> {

    private final ConocimientosRepositorio conocimientosRepositorio;

    public ConocimientosCrudUseCase(ConocimientosRepositorio conocimientosRepositorio){
        super(conocimientosRepositorio);
        this.conocimientosRepositorio=conocimientosRepositorio;
    }

    public Conocimientos obtenerPorNombre (String nombre){
        return conocimientosRepositorio.obtenerPorNombre(nombre);
    }

    public Conocimientos obtenerPorId (String id){ return conocimientosRepositorio.obtenerPorId(id); }
}
