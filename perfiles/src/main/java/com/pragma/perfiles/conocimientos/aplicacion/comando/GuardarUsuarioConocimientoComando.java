package com.pragma.perfiles.conocimientos.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GuardarUsuarioConocimientoComando {

    private String nombreSkill;
    private int puntaje;
    private String idUsuario;

}
