package com.pragma.perfiles.conocimientos.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarIndicesUsuarioConocimientosComando;
import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorActualizarIndicesUsuarioConocimientos;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("conocimientos")
@RequiredArgsConstructor
@Valid
public class EndpointActualizarIndicesUsuarioConocimientos {
    private final ManejadorActualizarIndicesUsuarioConocimientos manejadorActualizarIndicesUsuarioConocimientos;

    @PatchMapping
    public RespuestaBase actualizarIndicesUsuarioConocimientos(@NotNull @RequestBody ActualizarIndicesUsuarioConocimientosComando actualizarIndicesUsuarioConocimientosComando) {
        return manejadorActualizarIndicesUsuarioConocimientos.ejecutar(actualizarIndicesUsuarioConocimientosComando);
    }
}
