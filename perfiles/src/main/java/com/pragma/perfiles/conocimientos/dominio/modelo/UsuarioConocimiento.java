package com.pragma.perfiles.conocimientos.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioConocimiento extends Modelo<String> {

    private String idUsuario;
    private String nombreSkill;
    private int puntaje;
    private int indice;

    public UsuarioConocimiento(){ super(null); }

    public UsuarioConocimiento(String id, String idUsuario, String nombreSkill, int puntaje, int indice){
        super(id);
        this.idUsuario = idUsuario;
        this.nombreSkill = nombreSkill;
        this.puntaje = puntaje;
        this.indice = indice;
    }
}
