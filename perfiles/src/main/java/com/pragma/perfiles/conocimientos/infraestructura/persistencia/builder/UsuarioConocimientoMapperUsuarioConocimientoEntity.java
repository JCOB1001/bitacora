package com.pragma.perfiles.conocimientos.infraestructura.persistencia.builder;

import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.UsuarioConocimientoEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface UsuarioConocimientoMapperUsuarioConocimientoEntity extends ConvertidorBase<UsuarioConocimiento, UsuarioConocimientoEntity> {
    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "idUsuario", target = "usuario.id")
    )
    UsuarioConocimientoEntity leftToRight(UsuarioConocimiento usuarioConocimiento);

    @Named("rightToLeft")
    @Override
    @Mappings(
            @Mapping(source = "usuario.id", target = "idUsuario")
    )
    UsuarioConocimiento rightToLeft(UsuarioConocimientoEntity usuarioConocimientoEntity);

}
