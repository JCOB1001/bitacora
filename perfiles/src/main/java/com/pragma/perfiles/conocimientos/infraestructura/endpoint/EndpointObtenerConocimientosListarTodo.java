package com.pragma.perfiles.conocimientos.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorObtenerConocimientosListarTodo;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("conocimientos")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerConocimientosListarTodo {

    private final ManejadorObtenerConocimientosListarTodo manejadorObtenerConocimientosListarTodo;

    @GetMapping
    public ObjetoRespuesta<Stream<Conocimientos>> ejecutar(){
        Stream<Conocimientos> conocimientosStream = manejadorObtenerConocimientosListarTodo.ejecutar();
        return  new ObjetoRespuesta<>(HttpStatus.OK,conocimientosStream);
    }

}
