package com.pragma.perfiles.conocimientos.aplicacion.manejador;

import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.FabricaUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.dominio.useCase.ConocimientosCrudUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorGuardarConocimiento {

    private final FabricaUsuarioConocimiento fabricaUsuarioConocimiento;
    private final ConocimientosCrudUseCase conocimientosCrudUseCase;

    public Conocimientos ejecutar(
            GuardarConocimientoComando guardarConocimientoComando) {
        Conocimientos conocimiento = fabricaUsuarioConocimiento.guardarConocimiento(guardarConocimientoComando);
        return conocimientosCrudUseCase.guardar(conocimiento);
    }
}
