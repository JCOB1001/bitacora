package com.pragma.perfiles.conocimientos.aplicacion.fabrica;

import com.pragma.perfiles.comun.infraestructura.seguridad.dao.UsuarioDao;
import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl.ConocimientoMapperInformacionConocimientoArchivo;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl.UsuarioConocimientoMapperActualizarUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl.UsuarioConocimientoMapperGuardarUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl.ConocimientoMapperGuardarConocimientoComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.UsuarioConocimientoEntity;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.pragmadata.dominio.modelo.InformacionConocimientoArchivo;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class FabricaUsuarioConocimiento {

    private final UsuarioConocimientoMapperActualizarUsuarioConocimiento usuarioConocimientoMapperActualizarUsuarioConocimiento;
    private final ConocimientoMapperGuardarConocimientoComando conocimientoMapperGuardarConocimiento;
    private final UsuarioConocimientoMapperGuardarUsuarioConocimiento usuarioConocimientoMapperGuardarUsuarioConocimiento;
    private final ConocimientoMapperInformacionConocimientoArchivo conocimientoMapperInformacionConocimientoArchivo;

    public UsuarioConocimiento ActualizarUsuarioConocimiento(ActualizarUsuarioConocimientoComando actualizarUsuarioConocimientoComando){
        return  usuarioConocimientoMapperActualizarUsuarioConocimiento.rightToLeft(actualizarUsuarioConocimientoComando);
    }

    public Conocimientos guardarConocimiento(GuardarConocimientoComando guardarConocimientosComando){
        return  conocimientoMapperGuardarConocimiento.rightToLeft(guardarConocimientosComando);
    }

    public UsuarioConocimiento guardarUsuarioConocimiento(GuardarUsuarioConocimientoComando guardarUsuarioConocimientoComando){
        return usuarioConocimientoMapperGuardarUsuarioConocimiento.rightToLeft(guardarUsuarioConocimientoComando);
    }

    public Stream<InformacionConocimientoArchivo> conocimientoPorInformacionConocimientoArchivo(Iterable<UsuarioConocimientoEntity> usuarioConocimiento){
        ArrayList<InformacionConocimientoArchivo> informacionConocimientoArchivo = new ArrayList<>();
        usuarioConocimiento.forEach(new Consumer<UsuarioConocimientoEntity>() {
            @Override
            public void accept(UsuarioConocimientoEntity usuarioConocimientoEntity) {
                InformacionConocimientoArchivo i = new InformacionConocimientoArchivo(usuarioConocimientoEntity.getUsuario().getCorreoEmpresarial(), usuarioConocimientoEntity.getNombreSkill(), usuarioConocimientoEntity.getPuntaje());
                informacionConocimientoArchivo.add(i);
            }
        });
        System.out.println(informacionConocimientoArchivo);
        return informacionConocimientoArchivo.stream();
        //return conocimientoMapperInformacionConocimientoArchivo.leftToRight(usuarioConocimiento);
    }
}
