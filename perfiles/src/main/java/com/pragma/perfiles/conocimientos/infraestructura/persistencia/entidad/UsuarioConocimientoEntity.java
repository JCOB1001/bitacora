package com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "usuario_conocimiento",
        uniqueConstraints={@UniqueConstraint(name = "uq_usuario_id_skill_name",
                columnNames={"usuario_id","skill_name"})})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioConocimientoEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos {
        String USUARIO = "usuario";
        String NOMBRE_SKILL = "nombreSkill";
        String PUNTAJE = "puntaje";
        String INDICE = "indice";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_conocimiento_usuario_usuario_id"))
    private UsuarioEntity usuario;

    @Column(name = "skill_name")
    private String nombreSkill;

    @Column(name = "rate")
    private int puntaje;

    @Column(name = "indice", nullable = false)
    private int indice;
}
