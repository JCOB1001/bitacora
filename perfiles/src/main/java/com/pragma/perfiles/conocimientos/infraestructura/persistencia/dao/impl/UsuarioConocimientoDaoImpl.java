package com.pragma.perfiles.conocimientos.infraestructura.persistencia.dao.impl;

import com.pragma.perfiles.conocimientos.infraestructura.persistencia.dao.UsuarioConocimientoCustomDao;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.dao.UsuarioConocimientoDao;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.UsuarioConocimientoEntity;
import com.pragma.perfiles.conocimientos.infraestructura.persistencia.entidad.UsuarioConocimientoEntity.ATRIBUTOS;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.perfiles.pragmadex.dominio.filtros.FiltroNombreCorreo;
import lombok.RequiredArgsConstructor;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class UsuarioConocimientoDaoImpl implements UsuarioConocimientoCustomDao {

}
