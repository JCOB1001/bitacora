package com.pragma.perfiles.conocimientos.aplicacion.manejador;


import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.Comparator;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerUsuarioConocimientosListarTodo {

    private final UsuarioConocimientoCrudUseCase usuarioConocimientosCrudUseCase;

    public Stream<UsuarioConocimiento> ejecutar(){
        return usuarioConocimientosCrudUseCase.obtenerTodos()
            .sorted(Comparator.comparing(UsuarioConocimiento::getIndice))
            .sorted(Comparator.comparing(UsuarioConocimiento::getIdUsuario));
    }
}
