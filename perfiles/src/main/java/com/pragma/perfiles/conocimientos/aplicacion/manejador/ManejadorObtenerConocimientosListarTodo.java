package com.pragma.perfiles.conocimientos.aplicacion.manejador;


import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.perfiles.conocimientos.dominio.useCase.ConocimientosCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerConocimientosListarTodo {

    private final ConocimientosCrudUseCase conocimientosCrudUseCase;

    public Stream<Conocimientos> ejecutar(){
        return conocimientosCrudUseCase.obtenerTodos();
    }
}
