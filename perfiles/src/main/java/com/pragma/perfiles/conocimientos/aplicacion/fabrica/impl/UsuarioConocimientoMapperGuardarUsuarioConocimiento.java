package com.pragma.perfiles.conocimientos.aplicacion.fabrica.impl;

import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsuarioConocimientoMapperGuardarUsuarioConocimiento extends ConvertidorBase<UsuarioConocimiento, GuardarUsuarioConocimientoComando> {
}
