package com.pragma.perfiles.conocimientos.dominio.respuesta;

import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaUsuarioConocimiento extends ObjetoRespuesta {

    public ObjetoRespuestaUsuarioConocimiento(Stream<UsuarioConocimiento> datos){ super(datos); }
}
