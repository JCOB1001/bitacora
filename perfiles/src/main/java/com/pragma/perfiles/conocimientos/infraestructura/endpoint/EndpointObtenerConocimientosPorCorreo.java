package com.pragma.perfiles.conocimientos.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorObtenerUsuarioConocimientosPorCorreo;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.respuesta.UsuarioConocimientoConsultaPragmaData;
import com.pragma.perfiles.idiomas.dominio.respuesta.UsuarioIdiomasConsultaPragmaData;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("conocimientos")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerConocimientosPorCorreo {

    private final ManejadorObtenerUsuarioConocimientosPorCorreo manejadorObtenerConocimientosPorCorreo;

    @GetMapping("/correo/{correoEmpresarial}")
    public ObjetoRespuesta<Stream<UsuarioConocimientoConsultaPragmaData>> ejecutarObtenerConsultaPorCorreo(@NotNull @PathVariable String correoEmpresarial) {
        return new ObjetoRespuesta<>(HttpStatus.OK, manejadorObtenerConocimientosPorCorreo.ejecutar(correoEmpresarial));
    }
}
