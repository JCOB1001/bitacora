package com.pragma.perfiles.conocimientos.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorActualizarUsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.estudio.aplicacion.comando.ActualizarEstudioComando;
import com.pragma.perfiles.estudio.aplicacion.manejador.ManejadorActualizarEstudio;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("conocimientos")
@RequiredArgsConstructor
@Valid
public class EndpointActualizarUsuarioConocimiento {

    private final ManejadorActualizarUsuarioConocimiento manejadorActualizarUsuarioConocimiento;

    @PutMapping()
    public ObjetoRespuesta<UsuarioConocimiento> actualizarUsuarioConocimiento(@NotNull @RequestBody ActualizarUsuarioConocimientoComando actualizarUsuarioConocimientoComando){

        return manejadorActualizarUsuarioConocimiento.ejecutar(actualizarUsuarioConocimientoComando);
    }

}
