package com.pragma.perfiles.conocimientos.infraestructura.endpoint;

import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.manejador.ManejadorGuardarConocimiento;
import com.pragma.perfiles.conocimientos.dominio.modelo.Conocimientos;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("conocimientos")
@RequiredArgsConstructor
@Valid
public class EndpointGuardarConocimiento {

    private final ManejadorGuardarConocimiento manejadorGuardarConocimiento;

    @PostMapping("/conocimiento")
    public ObjetoRespuesta<Conocimientos> guardarConocimiento(@NotNull @RequestBody GuardarConocimientoComando guardarConocimientoComando){

        Conocimientos conocimiento = manejadorGuardarConocimiento.ejecutar(guardarConocimientoComando);
        return new ObjetoRespuesta<>(HttpStatus.OK, conocimiento);
    }
}
