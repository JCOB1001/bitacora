package com.pragma.perfiles.dias_celebracion.dominio.useCase;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasCelebracion;
import com.pragma.perfiles.dias_celebracion.dominio.repositorio.DiasCelebracionRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

public class DiasCelebracionCrudUseCase extends CrudUseCaseCommon<DiasCelebracionRepositorio, DiasCelebracion, String> {

    private final DiasCelebracionRepositorio diasCelebracionRepositorio;


    public DiasCelebracionCrudUseCase(DiasCelebracionRepositorio diasCelebracionRepositorio) {
        super(diasCelebracionRepositorio);
        this.diasCelebracionRepositorio = diasCelebracionRepositorio;
    }

}
