package com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad;

import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "dias_celebracion")
@NoArgsConstructor
@AllArgsConstructor
public class DiasCelebracionEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos {
        String NOMBRE = "nombre";
        String ID = "id";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "nombre")
    private String nombre;

}
