package com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiaNoCelebrablePragmaData;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import com.pragma.perfiles.dias_celebracion.dominio.repositorio.DiasNoCelebrablesUsuarioRepositorio;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.builder.DiasNoCelebrablesUsuarioMapperDiasNoCelebrablesUsuarioEntity;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.dao.DiasNoCelebrablesUsuarioDao;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad.DiasNoCelebrablesUsuarioEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class DiasNoCelebrablesUsuarioRepositorioImpl extends CrudRepositorioBaseImpl<String, DiasNoCelebrablesUsuario, DiasNoCelebrablesUsuarioEntity> implements DiasNoCelebrablesUsuarioRepositorio {

    private final DiasNoCelebrablesUsuarioDao diasNoCelebrablesUsuarioDao;
    private final DiasNoCelebrablesUsuarioMapperDiasNoCelebrablesUsuarioEntity diasNoCelebrablesUsuarioMapperDiasNoCelebrablesUsuarioEntity;

    @Override
    protected CrudRepository<DiasNoCelebrablesUsuarioEntity, String> obtenerRepositorio() {
        return this.diasNoCelebrablesUsuarioDao;
    }

    @Override
    protected ConvertidorBase<DiasNoCelebrablesUsuario, DiasNoCelebrablesUsuarioEntity> obtenerConversionBase() {
        return this.diasNoCelebrablesUsuarioMapperDiasNoCelebrablesUsuarioEntity;
    }

    @Override
    public Stream<DiasNoCelebrablesUsuario> findByUsuarioId(String idUsuario) {
        return diasNoCelebrablesUsuarioMapperDiasNoCelebrablesUsuarioEntity.rightToLeft(
                diasNoCelebrablesUsuarioDao.findByUsuarioId(idUsuario)
        );
    }

    @Override
    public DiasNoCelebrablesUsuario findByUsuarioIdAndDiaCelebracionId(String usuarioId, String diaCelebracionId) {
        return diasNoCelebrablesUsuarioMapperDiasNoCelebrablesUsuarioEntity.rightToLeft(
                diasNoCelebrablesUsuarioDao.findByUsuarioIdAndDiaCelebracionId(usuarioId, diaCelebracionId)
        );
    }

    @Override
    public Map<String, List<DiaNoCelebrablePragmaData>> obtenerDiasNoCelebrablesListaUsuarios(List<String> ids) {
        return diasNoCelebrablesUsuarioDao.obtenerDiasNoCelebrablesListaUsuarios(ids);
    }
}
