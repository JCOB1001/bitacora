package com.pragma.perfiles.dias_celebracion.dominio.repositorio;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiaNoCelebrablePragmaData;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public interface DiasNoCelebrablesUsuarioRepositorio extends CrudRepositorioBase<String, DiasNoCelebrablesUsuario> {
    Stream<DiasNoCelebrablesUsuario> findByUsuarioId(String idUsuario);

    DiasNoCelebrablesUsuario findByUsuarioIdAndDiaCelebracionId(String usuarioId, String diaCelebracionId);

    Map<String, List<DiaNoCelebrablePragmaData>> obtenerDiasNoCelebrablesListaUsuarios(List<String> ids);
}
