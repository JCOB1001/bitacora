package com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.builder;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasCelebracion;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad.DiasCelebracionEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DiasCelebracionMapperDiasCelebracionEntity extends ConvertidorBase<DiasCelebracion, DiasCelebracionEntity> {
}
