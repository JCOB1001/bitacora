package com.pragma.perfiles.dias_celebracion.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import com.pragma.perfiles.dias_celebracion.dominio.useCase.DiasNoCelebrablesUsuarioCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerDiasNoCelebrables {

    private final DiasNoCelebrablesUsuarioCrudUseCase diasNoCelebrablesUsuarioCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Stream<DiasNoCelebrablesUsuario>> ejecutar(String idUsuario) {
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }

        Stream<DiasNoCelebrablesUsuario> diasNoCelebrablesUsuario = diasNoCelebrablesUsuarioCrudUseCase.findByIdUsuario(usuario.getId());

        return new ObjetoRespuesta<>(HttpStatus.OK, diasNoCelebrablesUsuario);

    }
}
