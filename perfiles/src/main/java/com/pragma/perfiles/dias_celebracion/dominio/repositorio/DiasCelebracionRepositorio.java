package com.pragma.perfiles.dias_celebracion.dominio.repositorio;


import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasCelebracion;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface DiasCelebracionRepositorio extends CrudRepositorioBase<String, DiasCelebracion> {

}
