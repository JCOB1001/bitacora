package com.pragma.perfiles.dias_celebracion.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuardarUsuarioDiasNoCelebrablesComando {
    private String usuarioId;
    private String diaCelebracionId;
}