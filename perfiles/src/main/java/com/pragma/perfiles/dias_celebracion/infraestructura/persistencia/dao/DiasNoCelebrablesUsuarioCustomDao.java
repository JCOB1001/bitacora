package com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.dao;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiaNoCelebrablePragmaData;

import java.util.List;
import java.util.Map;

public interface DiasNoCelebrablesUsuarioCustomDao {

    Map<String, List<DiaNoCelebrablePragmaData>> obtenerDiasNoCelebrablesListaUsuarios(List<String> ids);

}
