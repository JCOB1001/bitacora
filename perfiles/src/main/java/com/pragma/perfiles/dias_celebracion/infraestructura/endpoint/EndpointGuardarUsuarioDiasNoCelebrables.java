package com.pragma.perfiles.dias_celebracion.infraestructura.endpoint;

import com.pragma.perfiles.dias_celebracion.aplicacion.comando.GuardarUsuarioDiasNoCelebrablesComando;
import com.pragma.perfiles.dias_celebracion.aplicacion.manejador.ManejadorGuardarUsuarioDiasNoCelebrables;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/dias-no-celebrables")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarUsuarioDiasNoCelebrables {

    private final ManejadorGuardarUsuarioDiasNoCelebrables manejadorGuardarUsuarioDiasNoCelebrables;

    @PostMapping
    public ObjetoRespuesta<String> guardarUsuarioDiasNoCelebrables(@NotNull @RequestBody GuardarUsuarioDiasNoCelebrablesComando guardarUsuarioDiasNoCelebrablesComando) {
        return manejadorGuardarUsuarioDiasNoCelebrables.ejecutar(guardarUsuarioDiasNoCelebrablesComando);
    }
}
