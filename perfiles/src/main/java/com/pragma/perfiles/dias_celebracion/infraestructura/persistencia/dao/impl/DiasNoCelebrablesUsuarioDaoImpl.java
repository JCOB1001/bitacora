package com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.dao.impl;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiaNoCelebrablePragmaData;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.dao.DiasNoCelebrablesUsuarioCustomDao;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad.DiasCelebracionEntity;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad.DiasNoCelebrablesUsuarioEntity;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad.DiasNoCelebrablesUsuarioEntity.ATRIBUTOS;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Transactional(readOnly = true)
@RequiredArgsConstructor
public class DiasNoCelebrablesUsuarioDaoImpl implements DiasNoCelebrablesUsuarioCustomDao {

    private final EntityManager entityManager;

    @Override
    public Map<String, List<DiaNoCelebrablePragmaData>> obtenerDiasNoCelebrablesListaUsuarios(List<String> ids) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DiaNoCelebrablePragmaData> criteriaQuery = criteriaBuilder.createQuery(DiaNoCelebrablePragmaData.class);
        Root<DiasNoCelebrablesUsuarioEntity> diasNoCelebrablesRoot = criteriaQuery.from(DiasNoCelebrablesUsuarioEntity.class);

        Join<Object, Object> join = diasNoCelebrablesRoot.join(ATRIBUTOS.DIA_CELEBRACION);
        List<UsuarioEntity> usuarios = ids.stream().map(UsuarioEntity::new).collect(Collectors.toList());
        List<DiaNoCelebrablePragmaData> result = entityManager.createQuery(
                criteriaQuery
                        .multiselect(
                                join.get(DiasCelebracionEntity.ATRIBUTOS.NOMBRE),
                                diasNoCelebrablesRoot.get(UsuarioEntity.Atributos.ID)
                        )
                        .where(diasNoCelebrablesRoot.get(ATRIBUTOS.USUARIO).in(usuarios))
        ).getResultList();

        return result.stream().collect(Collectors.groupingBy(DiaNoCelebrablePragmaData::getUsuarioId));
    }

}