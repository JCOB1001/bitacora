package com.pragma.perfiles.dias_celebracion.aplicacion.fabrica.impl;

import com.pragma.perfiles.dias_celebracion.aplicacion.comando.GuardarUsuarioDiasNoCelebrablesComando;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DiasNoCelebrablesUsuarioMapperGuardarUsuarioDiasNoCelebrablesComando extends ConvertidorBase<DiasNoCelebrablesUsuario, GuardarUsuarioDiasNoCelebrablesComando> {
}
