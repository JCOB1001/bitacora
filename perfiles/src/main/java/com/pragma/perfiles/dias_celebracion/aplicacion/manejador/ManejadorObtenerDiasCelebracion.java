package com.pragma.perfiles.dias_celebracion.aplicacion.manejador;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasCelebracion;
import com.pragma.perfiles.dias_celebracion.dominio.useCase.DiasCelebracionCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;


@RequiredArgsConstructor
public class ManejadorObtenerDiasCelebracion {

    private final DiasCelebracionCrudUseCase diasCelebracionCrudUseCase;

    public ObjetoRespuesta<Stream<DiasCelebracion>> ejecutar() {
        Stream<DiasCelebracion> diasCelebracion = diasCelebracionCrudUseCase.obtenerTodos();
        return new ObjetoRespuesta<>(diasCelebracion);
    }
}
