package com.pragma.perfiles.dias_celebracion.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiasCelebracion extends Modelo<String> {

    private String id;

    private String nombre;

    public DiasCelebracion() { super(null); }

    public DiasCelebracion(String s) {
        super(s);
    }
}
