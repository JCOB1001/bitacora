package com.pragma.perfiles.dias_celebracion.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiasNoCelebrablesUsuario extends Modelo<String> {

    private String id;

    private String usuarioId;

    private String diaCelebracionId;

    public DiasNoCelebrablesUsuario(String s) {
        super(s);
    }

    public DiasNoCelebrablesUsuario() { super(null); }
}
