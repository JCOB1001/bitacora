package com.pragma.perfiles.dias_celebracion.infraestructura.endpoint;

import com.pragma.perfiles.dias_celebracion.aplicacion.manejador.ManejadorObtenerDiasNoCelebrables;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("/dias-no-celebrables")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerDiasNoCelebrablesPorIdUsuario {

    private final ManejadorObtenerDiasNoCelebrables manejadorObtenerDiasNoCelebrables;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<Stream<DiasNoCelebrablesUsuario>> ejecutarObtenerIdiomasPorIdUsuario(@NotNull @PathVariable String idUsuario){

            return manejadorObtenerDiasNoCelebrables.ejecutar(idUsuario);
    }
}
