package com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "dias_no_celebracion_usuario")
@NoArgsConstructor
@AllArgsConstructor
public class DiasNoCelebrablesUsuarioEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos {
        String USUARIO = "usuario";
        String DIA_CELEBRACION = "diaCelebracion";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_dias_no_celebracion_usuario_usuario_usuario_id"))
    private UsuarioEntity usuario;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "dia_celebracion_id", foreignKey = @ForeignKey(name = "fk_dncu_dias_celebracion_dia_celebracion_id"), nullable = false)
    private DiasCelebracionEntity diaCelebracion;

}
