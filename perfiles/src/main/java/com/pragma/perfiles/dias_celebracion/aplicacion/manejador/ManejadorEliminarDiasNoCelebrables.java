package com.pragma.perfiles.dias_celebracion.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.DiaNoCelebrableNoEncontrado;
import com.pragma.perfiles.dias_celebracion.dominio.useCase.DiasNoCelebrablesUsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarDiasNoCelebrables {

    private final DiasNoCelebrablesUsuarioCrudUseCase diasNoCelebrablesUsuarioCrudUseCase;

    public ObjetoRespuesta<String> ejecutar(String id) {
        if (diasNoCelebrablesUsuarioCrudUseCase.obtenerPorId(id) == null) {
            throw new DiaNoCelebrableNoEncontrado("dia no celebrable no se encontrado");
        }

        diasNoCelebrablesUsuarioCrudUseCase.eliminarPorId(id);
        return new ObjetoRespuesta<>("eliminado correctamente");

    }
}
