package com.pragma.perfiles.dias_celebracion.aplicacion.fabrica;

import com.pragma.perfiles.dias_celebracion.aplicacion.comando.GuardarUsuarioDiasNoCelebrablesComando;
import com.pragma.perfiles.dias_celebracion.aplicacion.fabrica.impl.DiasNoCelebrablesUsuarioMapperGuardarUsuarioDiasNoCelebrablesComando;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaDiasNoCelebrablesUsuario {

    private final DiasNoCelebrablesUsuarioMapperGuardarUsuarioDiasNoCelebrablesComando diasNoCelebrablesUsuarioMapperGuardarUsuarioDiasNoCelebrablesComando;


    public DiasNoCelebrablesUsuario guardarDiasNoCelebrablesUsuario(GuardarUsuarioDiasNoCelebrablesComando guardarUsuarioDiasNoCelebrablesComando) {
        return diasNoCelebrablesUsuarioMapperGuardarUsuarioDiasNoCelebrablesComando.rightToLeft(guardarUsuarioDiasNoCelebrablesComando);
    }
}
