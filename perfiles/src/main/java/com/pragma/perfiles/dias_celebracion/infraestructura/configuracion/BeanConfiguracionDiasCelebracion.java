package com.pragma.perfiles.dias_celebracion.infraestructura.configuracion;

import com.pragma.perfiles.dias_celebracion.aplicacion.fabrica.FabricaDiasNoCelebrablesUsuario;
import com.pragma.perfiles.dias_celebracion.aplicacion.fabrica.impl.DiasNoCelebrablesUsuarioMapperGuardarUsuarioDiasNoCelebrablesComando;
import com.pragma.perfiles.dias_celebracion.aplicacion.manejador.ManejadorEliminarDiasNoCelebrables;
import com.pragma.perfiles.dias_celebracion.aplicacion.manejador.ManejadorGuardarUsuarioDiasNoCelebrables;
import com.pragma.perfiles.dias_celebracion.aplicacion.manejador.ManejadorObtenerDiasCelebracion;
import com.pragma.perfiles.dias_celebracion.aplicacion.manejador.ManejadorObtenerDiasNoCelebrables;
import com.pragma.perfiles.dias_celebracion.dominio.repositorio.DiasCelebracionRepositorio;
import com.pragma.perfiles.dias_celebracion.dominio.repositorio.DiasNoCelebrablesUsuarioRepositorio;
import com.pragma.perfiles.dias_celebracion.dominio.useCase.DiasCelebracionCrudUseCase;
import com.pragma.perfiles.dias_celebracion.dominio.useCase.DiasNoCelebrablesUsuarioCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguracionDiasCelebracion {

    @Bean
    public DiasCelebracionCrudUseCase diasCelebracionCrudUseCase(DiasCelebracionRepositorio diasCelebracionRepositorio) {
        return new DiasCelebracionCrudUseCase(diasCelebracionRepositorio);
    }

    @Bean
    public DiasNoCelebrablesUsuarioCrudUseCase diasNoCelebrablesUsuarioCrudUseCase(DiasNoCelebrablesUsuarioRepositorio diasNoCelebrablesUsuarioRepositorio) {
        return new DiasNoCelebrablesUsuarioCrudUseCase(diasNoCelebrablesUsuarioRepositorio);
    }

    @Bean
    public FabricaDiasNoCelebrablesUsuario fabricaDiasNoCelebrablesUsuario(DiasNoCelebrablesUsuarioMapperGuardarUsuarioDiasNoCelebrablesComando diasNoCelebrablesUsuarioMapperGuardarUsuarioDiasNoCelebrablesComando) {
        return new FabricaDiasNoCelebrablesUsuario(diasNoCelebrablesUsuarioMapperGuardarUsuarioDiasNoCelebrablesComando);
    }

    @Bean
    public ManejadorObtenerDiasCelebracion manejadorObtenerDiasCelebracion(DiasCelebracionCrudUseCase diasCelebracionCrudUseCase) {
        return new ManejadorObtenerDiasCelebracion(diasCelebracionCrudUseCase);
    }

    @Bean
    public ManejadorObtenerDiasNoCelebrables manejadorObtenerDiasNoCelebrables(DiasNoCelebrablesUsuarioCrudUseCase diasNoCelebrablesUsuarioCrudUseCase,
                                                                               UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorObtenerDiasNoCelebrables(diasNoCelebrablesUsuarioCrudUseCase, usuarioCrudUseCase);
    }

    @Bean
    public ManejadorGuardarUsuarioDiasNoCelebrables manejadorGuardarUsuarioDiasNoCelebrable(DiasNoCelebrablesUsuarioCrudUseCase diasNoCelebrablesUsuarioCrudUseCase,
                                                                                            DiasCelebracionCrudUseCase diasCelebracionCrudUseCase,
                                                                                            UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                            FabricaDiasNoCelebrablesUsuario fabricaDiasNoCelebrablesUsuario) {
        return new ManejadorGuardarUsuarioDiasNoCelebrables(diasNoCelebrablesUsuarioCrudUseCase, diasCelebracionCrudUseCase, usuarioCrudUseCase, fabricaDiasNoCelebrablesUsuario);
    }

    @Bean
    public ManejadorEliminarDiasNoCelebrables manejadorEliminarDiasNoCelebrables(DiasNoCelebrablesUsuarioCrudUseCase diasNoCelebrablesUsuarioCrudUseCase) {
        return new ManejadorEliminarDiasNoCelebrables(diasNoCelebrablesUsuarioCrudUseCase);
    }
}
