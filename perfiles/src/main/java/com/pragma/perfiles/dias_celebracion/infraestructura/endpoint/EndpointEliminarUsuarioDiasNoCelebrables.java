package com.pragma.perfiles.dias_celebracion.infraestructura.endpoint;

import com.pragma.perfiles.dias_celebracion.aplicacion.manejador.ManejadorEliminarDiasNoCelebrables;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/dias-no-celebrables")
@RequiredArgsConstructor
@Validated
public class EndpointEliminarUsuarioDiasNoCelebrables {

    private final ManejadorEliminarDiasNoCelebrables manejadorEliminarDiasNoCelebrables;

    @DeleteMapping("/{id}")
    public ObjetoRespuesta<String> eliminaDiasNoCelebrables(@PathVariable String id){
        return manejadorEliminarDiasNoCelebrables.ejecutar(id);
    }

}
