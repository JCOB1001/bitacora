package com.pragma.perfiles.dias_celebracion.infraestructura.endpoint;

import com.pragma.perfiles.dias_celebracion.aplicacion.manejador.ManejadorObtenerDiasCelebracion;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasCelebracion;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;


@RestController
@RequestMapping("dias-celebracion")
@RequiredArgsConstructor
public class EndpointObtenerDiasCelebracion {

    private final ManejadorObtenerDiasCelebracion manejadorObtenerDiasCelebracion;

    @GetMapping
    public ObjetoRespuesta<Stream<DiasCelebracion>> obtenerDiasCelebracion(){
        return manejadorObtenerDiasCelebracion.ejecutar();
    }
}
