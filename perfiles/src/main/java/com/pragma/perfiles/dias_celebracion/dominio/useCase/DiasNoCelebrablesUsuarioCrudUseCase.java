package com.pragma.perfiles.dias_celebracion.dominio.useCase;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import com.pragma.perfiles.dias_celebracion.dominio.repositorio.DiasNoCelebrablesUsuarioRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class DiasNoCelebrablesUsuarioCrudUseCase extends CrudUseCaseCommon<DiasNoCelebrablesUsuarioRepositorio, DiasNoCelebrablesUsuario, String> {

    private final DiasNoCelebrablesUsuarioRepositorio diasNoCelebrablesUsuarioRepositorio;

    public DiasNoCelebrablesUsuarioCrudUseCase(DiasNoCelebrablesUsuarioRepositorio diasNoCelebrablesUsuarioRepositorio) {
        super(diasNoCelebrablesUsuarioRepositorio);
        this.diasNoCelebrablesUsuarioRepositorio= diasNoCelebrablesUsuarioRepositorio;
    }

    public Stream<DiasNoCelebrablesUsuario> findByIdUsuario(String idUsuario) {
        return this.diasNoCelebrablesUsuarioRepositorio.findByUsuarioId(idUsuario);
    }

    public DiasNoCelebrablesUsuario obtenerDiasNoCelebrablesPorUsuarioIdAndDiaId(String usuarioId, String diaCelebracionId) {
        return this.diasNoCelebrablesUsuarioRepositorio.findByUsuarioIdAndDiaCelebracionId(usuarioId, diaCelebracionId);
    }
}
