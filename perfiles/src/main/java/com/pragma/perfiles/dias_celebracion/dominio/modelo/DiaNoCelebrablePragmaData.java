package com.pragma.perfiles.dias_celebracion.dominio.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DiaNoCelebrablePragmaData {

    String nombre;

    String usuarioId;

}
