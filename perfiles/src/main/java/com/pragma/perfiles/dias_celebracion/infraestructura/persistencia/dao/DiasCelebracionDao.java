package com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.dao;

import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad.DiasCelebracionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiasCelebracionDao extends JpaRepository<DiasCelebracionEntity, String> {

}
