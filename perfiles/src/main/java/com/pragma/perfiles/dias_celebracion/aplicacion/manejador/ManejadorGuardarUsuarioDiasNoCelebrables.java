package com.pragma.perfiles.dias_celebracion.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.DiaCelebracionNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.DiaNoCelebrableYaExiste;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.dias_celebracion.aplicacion.comando.GuardarUsuarioDiasNoCelebrablesComando;
import com.pragma.perfiles.dias_celebracion.aplicacion.fabrica.FabricaDiasNoCelebrablesUsuario;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasCelebracion;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import com.pragma.perfiles.dias_celebracion.dominio.useCase.DiasCelebracionCrudUseCase;
import com.pragma.perfiles.dias_celebracion.dominio.useCase.DiasNoCelebrablesUsuarioCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@RequiredArgsConstructor
public class ManejadorGuardarUsuarioDiasNoCelebrables {

    private final DiasNoCelebrablesUsuarioCrudUseCase diasNoCelebrablesUsuarioCrudUseCase;
    private final DiasCelebracionCrudUseCase diasCelebracionCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaDiasNoCelebrablesUsuario fabricaDiasNoCelebrablesUsuario;

    public ObjetoRespuesta<String> ejecutar(GuardarUsuarioDiasNoCelebrablesComando guardarUsuarioDiasNoCelebrablesComando) {
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(guardarUsuarioDiasNoCelebrablesComando.getUsuarioId());
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }

        DiasCelebracion diasCelebracion = diasCelebracionCrudUseCase.obtenerPorId(guardarUsuarioDiasNoCelebrablesComando.getDiaCelebracionId());
        if(diasCelebracion == null){
            throw new DiaCelebracionNoEncontrado("Este día de celebración no existe en la base de datos");
        }

        DiasNoCelebrablesUsuario diasNoCelebrablesUsuario = diasNoCelebrablesUsuarioCrudUseCase.obtenerDiasNoCelebrablesPorUsuarioIdAndDiaId(guardarUsuarioDiasNoCelebrablesComando.getUsuarioId(),guardarUsuarioDiasNoCelebrablesComando.getDiaCelebracionId());
        if (diasNoCelebrablesUsuario != null) {
            throw new DiaNoCelebrableYaExiste("Este día no celebrable ya existe en la base de datos");
        }
        diasNoCelebrablesUsuario = fabricaDiasNoCelebrablesUsuario.guardarDiasNoCelebrablesUsuario(guardarUsuarioDiasNoCelebrablesComando);
        diasNoCelebrablesUsuarioCrudUseCase.guardar(diasNoCelebrablesUsuario);
        return new ObjetoRespuesta<>("Guardado exitoso");
    }
}
