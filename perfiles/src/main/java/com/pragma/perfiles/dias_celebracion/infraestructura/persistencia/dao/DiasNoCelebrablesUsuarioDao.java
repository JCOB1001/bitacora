package com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.dao;

import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad.DiasNoCelebrablesUsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiasNoCelebrablesUsuarioDao extends JpaRepository<DiasNoCelebrablesUsuarioEntity, String>, DiasNoCelebrablesUsuarioCustomDao {


    Iterable<DiasNoCelebrablesUsuarioEntity> findByUsuarioId(String idUsuario);

    DiasNoCelebrablesUsuarioEntity findByUsuarioIdAndDiaCelebracionId(String usuarioId, String diaCelebracionId);
}
