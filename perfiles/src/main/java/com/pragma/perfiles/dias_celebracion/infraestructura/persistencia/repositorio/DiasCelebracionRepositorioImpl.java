package com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasCelebracion;
import com.pragma.perfiles.dias_celebracion.dominio.repositorio.DiasCelebracionRepositorio;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.builder.DiasCelebracionMapperDiasCelebracionEntity;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.dao.DiasCelebracionDao;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad.DiasCelebracionEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DiasCelebracionRepositorioImpl extends CrudRepositorioBaseImpl<String, DiasCelebracion, DiasCelebracionEntity> implements DiasCelebracionRepositorio {

    private final DiasCelebracionDao diasCelebracionDao;
    private final DiasCelebracionMapperDiasCelebracionEntity diasCelebracionMapperDiasCelebracionEntity;

    @Override
    protected CrudRepository<DiasCelebracionEntity, String> obtenerRepositorio() {
        return this.diasCelebracionDao;
    }

    @Override
    protected ConvertidorBase<DiasCelebracion, DiasCelebracionEntity> obtenerConversionBase() {
        return this.diasCelebracionMapperDiasCelebracionEntity;
    }
}
