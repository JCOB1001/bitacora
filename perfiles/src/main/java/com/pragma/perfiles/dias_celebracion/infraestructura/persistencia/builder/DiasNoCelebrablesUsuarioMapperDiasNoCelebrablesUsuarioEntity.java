package com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.builder;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import com.pragma.perfiles.dias_celebracion.infraestructura.persistencia.entidad.DiasNoCelebrablesUsuarioEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface DiasNoCelebrablesUsuarioMapperDiasNoCelebrablesUsuarioEntity extends ConvertidorBase<DiasNoCelebrablesUsuario, DiasNoCelebrablesUsuarioEntity> {
    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "usuarioId", target = "usuario.id"),
            @Mapping(source = "diaCelebracionId", target = "diaCelebracion.id")
    })
    DiasNoCelebrablesUsuarioEntity leftToRight(DiasNoCelebrablesUsuario diasNoCelebrablesUsuario);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "usuario.id", target = "usuarioId"),
            @Mapping(source = "diaCelebracion.id", target = "diaCelebracionId")
    })
    DiasNoCelebrablesUsuario rightToLeft(DiasNoCelebrablesUsuarioEntity diasNoCelebrablesUsuarioEntity);

}
