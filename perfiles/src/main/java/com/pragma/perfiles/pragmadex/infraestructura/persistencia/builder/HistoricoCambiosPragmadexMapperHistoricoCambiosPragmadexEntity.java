package com.pragma.perfiles.pragmadex.infraestructura.persistencia.builder;

import com.pragma.perfiles.pragmadex.dominio.modelo.HistoricoCambiosPragmadex;
import com.pragma.perfiles.pragmadex.infraestructura.persistencia.entidad.HistoricoCambiosPragmadexEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface HistoricoCambiosPragmadexMapperHistoricoCambiosPragmadexEntity
        extends ConvertidorBase<HistoricoCambiosPragmadex, HistoricoCambiosPragmadexEntity> {

    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "idUsuarioModifica", target = "usuario.id")
    )
    HistoricoCambiosPragmadexEntity leftToRight(HistoricoCambiosPragmadex historicoCambiosPragmadex);

    @Named("rightToLeft")
    @Override
    @Mappings(
            @Mapping(source = "usuario.id", target = "idUsuarioModifica")
    )
    HistoricoCambiosPragmadex rightToLeft(HistoricoCambiosPragmadexEntity historicoCambiosPragmadexEntity);

}
