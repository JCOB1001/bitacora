package com.pragma.perfiles.pragmadex.aplicacion.converter;

import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.perfil.dominio.modelo.Fotografia;
import com.pragma.perfiles.perfil.dominio.modelo.TipoFotografia;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.pragmadex.dominio.modelo.HabilidadesPragmaDex;
import com.pragma.perfiles.pragmadex.dominio.modelo.InfoEstado;
import com.pragma.perfiles.pragmadex.dominio.modelo.UsuarioPragmaDex;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UsuarioConocimientoToHabilidadesPragmaDexConverter implements Converter<UsuarioConocimiento, HabilidadesPragmaDex> {

    @Override
    public HabilidadesPragmaDex convert(UsuarioConocimiento usuarioConocimiento) {
        HabilidadesPragmaDex habilidadesPragmaDex = new HabilidadesPragmaDex();
        habilidadesPragmaDex.setSkillName(usuarioConocimiento.getNombreSkill());
        habilidadesPragmaDex.setRate(usuarioConocimiento.getPuntaje());
        habilidadesPragmaDex.setIndice(usuarioConocimiento.getIndice());
        return habilidadesPragmaDex;
    }
}
