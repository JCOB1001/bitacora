package com.pragma.perfiles.pragmadex.dominio.useCase;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ValidarDatosPersonalesCompletadosUseCase {

    public boolean validar(Usuario usuario) {
        if (
                usuario.getNombres() == null
                        || usuario.getNombres().isBlank()
                        || usuario.getApellidos() == null
                        || usuario.getApellidos().isBlank()
                        || usuario.getPerfilProfesional() == null
                        || usuario.getPerfilProfesional().isBlank()
                        || usuario.getCargo() == null
                        || usuario.getCargo().isBlank()
        ) {
            return false;
        }
        return true;
    }

}
