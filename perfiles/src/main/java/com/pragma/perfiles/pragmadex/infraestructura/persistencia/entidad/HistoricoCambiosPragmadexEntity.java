package com.pragma.perfiles.pragmadex.infraestructura.persistencia.entidad;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "historico_cambios_pragmadex")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HistoricoCambiosPragmadexEntity implements IdEntidad<Long>{

    public interface Atributos extends IdEntidad.Atributos {
        String CAMPO_MODIFICADO = "campoModificado";
        String TABLA_MODIFICADA = "tablaModificada";
        String VALOR_NUEVO = "valorNuevo";
        String VALOR_ANTERIOR = "valorAnterior";
        String USUARIO = "usuario";
        String FECHA_MODIFICACION = "fechaModificacion";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String campoModificado;
    private String tablaModificada;
    private String valorNuevo;
    private String valorAnterior;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_modifica_id", foreignKey = @ForeignKey(name = "fk_historico_cambios_pragmadex_usuario_usuario_modifica_id"))
    private UsuarioEntity usuario;
    private LocalDate fechaModificacion;
}
