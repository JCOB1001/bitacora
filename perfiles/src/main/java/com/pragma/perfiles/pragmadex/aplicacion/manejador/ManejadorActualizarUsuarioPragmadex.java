package com.pragma.perfiles.pragmadex.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.contrato_historico.dominio.UseCase.ContratoHistoricoCrudUseCase;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.pragmadex.aplicacion.comando.ActualizarUsuarioPragmadexComando;
import com.pragma.perfiles.pragmadex.dominio.modelo.HistoricoCambiosPragmadex;
import com.pragma.perfiles.pragmadex.dominio.useCase.HistoricoCambiosPragmadexCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ManejadorActualizarUsuarioPragmadex {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final InformacionNominaCrudUseCase informacionNominaCrudUseCase;
    private final ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase;
    private final HistoricoCambiosPragmadexCrudUseCase historicoCambiosPragmadexCrudUseCase;

    public void ejecutar(String idUsuarioModifica, ActualizarUsuarioPragmadexComando actualizarUsuarioPragmadexComando) {
        Usuario usuarioModifica = usuarioCrudUseCase.findById(idUsuarioModifica);
        if (usuarioModifica == null) {
            throw new UsuarioNoEncontrado("No se encontro el usuario que modifica");
        }
        //buscamos el usuario y lo guardamos
        Usuario usuarioParaActualizar = usuarioCrudUseCase.findByCorreoEmpresarial(actualizarUsuarioPragmadexComando.getCorreoEmpresarial());
        if (usuarioParaActualizar == null) {
            throw new UsuarioNoEncontrado("No se encontro el usuario modificado");
        }
        guardarUsuario(usuarioParaActualizar, actualizarUsuarioPragmadexComando,idUsuarioModifica);

        //buscamos la informacion de la nomina y lo guardamos
        InformacionNomina informacionNomina = informacionNominaCrudUseCase
                .findByUsuarioId(usuarioParaActualizar.getId())
                .findFirst()
                .orElse(new InformacionNomina(null, usuarioParaActualizar.getId(), actualizarUsuarioPragmadexComando.getCargoNomina(), 0.0, 0.0,                        0.0,""));

        guardarInformacionNomina(informacionNomina, actualizarUsuarioPragmadexComando,idUsuarioModifica);

        //buscamos la informacion del ContratoHistorico activo y lo guardamos
        //en caso de no encontrarla se crea uno nuevo con la fecha de inicio que aparece en actualizarUsuarioPragmadexComando, fecha fin null y tipo contrato vacio
        ContratoHistorico contratoHistorico;
        contratoHistorico = contratoHistoricoCrudUseCase.findByUsuarioIdAndEstado(usuarioParaActualizar.getId(), ContratoHistoricoEstado.ACTIVO);
        if (contratoHistorico == null) {
            contratoHistorico = new ContratoHistorico(null,usuarioParaActualizar.getId(), actualizarUsuarioPragmadexComando.getFechaInicioContratoActual(), null, "", ContratoHistoricoEstado.ACTIVO);
        }
        guardarContratoHistorico(contratoHistorico, actualizarUsuarioPragmadexComando,idUsuarioModifica);


    }

    private void guardarContratoHistorico(ContratoHistorico contratoHistorico,
                                          ActualizarUsuarioPragmadexComando actualizarUsuarioPragmadexComando,
                                          String idUsuarioModifica) {
        final String ID_TIPO_CONTRATO_INDEFINIDO = "2";
        String tabla = "ContratoHistorico";
        List<HistoricoCambiosPragmadex> historicoCambiosPragmadexList = new ArrayList<>();

        //se validan los cambios para guardarlos en el historial de cambios pragmadex
        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"FechaInicio",tabla
                        ,actualizarUsuarioPragmadexComando.getFechaInicioContratoActual()+"",
                        contratoHistorico.getFechaInicio()+"",idUsuarioModifica,LocalDate.now()));
        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"IdTipoContrato",tabla
                        ,actualizarUsuarioPragmadexComando.getIdTipocontrato()+"",
                        contratoHistorico.getIdTipoContrato()+"",idUsuarioModifica,LocalDate.now()));

        //si el contrato es vacio, significa que es la primer vez que se crea un historico de contrato
        //se setea el tipo contrato y se guarda
        if(contratoHistorico.getIdTipoContrato().isEmpty()){
            contratoHistorico.setIdTipoContrato(actualizarUsuarioPragmadexComando.getIdTipocontrato());
            contratoHistoricoCrudUseCase.guardar(contratoHistorico);
        }else {
            //si el tipo de contrato es diferente al anterior se inactiva y se guarda uno nuevo
            if(!contratoHistorico.getIdTipoContrato().equalsIgnoreCase(actualizarUsuarioPragmadexComando.getIdTipocontrato())){
                //setea el contrato actual como inactivo
                contratoHistorico.setEstado(ContratoHistoricoEstado.INACTIVO);
                contratoHistorico.setFechaFin(actualizarUsuarioPragmadexComando.getFechaInicioContratoActual());
                contratoHistoricoCrudUseCase.guardar(contratoHistorico);

                //crea un nuevo contrato activo
                ContratoHistorico contratoHistoricoNuevo = new ContratoHistorico(null,contratoHistorico.getIdUsuario(),
                        actualizarUsuarioPragmadexComando.getFechaInicioContratoActual(), null,
                        actualizarUsuarioPragmadexComando.getIdTipocontrato(), ContratoHistoricoEstado.ACTIVO);
                contratoHistoricoCrudUseCase.guardar(contratoHistoricoNuevo);
            }else{
                //cuando se quiere modificar la fecha de inicio del contrato actual
                contratoHistorico.setFechaInicio(actualizarUsuarioPragmadexComando.getFechaInicioContratoActual());
                contratoHistoricoCrudUseCase.guardar(contratoHistorico);
            }
        }
        guardarHistoricoCambiosPragmadex(historicoCambiosPragmadexList);
    }

    private void guardarInformacionNomina(InformacionNomina informacionNomina,
                                          ActualizarUsuarioPragmadexComando actualizarUsuarioPragmadexComando,
                                          String idUsuarioModifica) {
        String tabla = "InformacionNomina";
        List<HistoricoCambiosPragmadex> historicoCambiosPragmadexList = new ArrayList<>();

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"CargoNomina",tabla
                        ,actualizarUsuarioPragmadexComando.getCargoNomina(),
                        informacionNomina.getCargoNomina(),idUsuarioModifica,LocalDate.now()));
        informacionNomina.setCargoNomina(actualizarUsuarioPragmadexComando.getCargoNomina());

        informacionNominaCrudUseCase.guardar(informacionNomina);

        guardarHistoricoCambiosPragmadex(historicoCambiosPragmadexList);

    }

    private void guardarUsuario(Usuario usuarioParaActualizar,
                                ActualizarUsuarioPragmadexComando actualizarUsuarioPragmadexComando,
                                String idUsuarioModifica) {

        String tabla = "usuario";
        List<HistoricoCambiosPragmadex> historicoCambiosPragmadexList = new ArrayList<>();

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"CorreoEmpresarial",tabla
                        ,actualizarUsuarioPragmadexComando.getCorreoEmpresarial(),
                        usuarioParaActualizar.getCorreoEmpresarial(),idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setCorreoEmpresarial(actualizarUsuarioPragmadexComando.getCorreoEmpresarial());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"IdTipoDocumento",tabla
                        ,actualizarUsuarioPragmadexComando.getIdTipoDocumento()+"",
                        usuarioParaActualizar.getIdTipoDocumento()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setIdTipoDocumento(actualizarUsuarioPragmadexComando.getIdTipoDocumento());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"Identificacion",tabla
                        ,actualizarUsuarioPragmadexComando.getIdentificacion()+"",
                        usuarioParaActualizar.getIdentificacion()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setIdentificacion(actualizarUsuarioPragmadexComando.getIdentificacion());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"Nombres",tabla
                        ,actualizarUsuarioPragmadexComando.getNombres()+"",
                        usuarioParaActualizar.getNombres()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setNombres(actualizarUsuarioPragmadexComando.getNombres());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"Apellidos",tabla
                        ,actualizarUsuarioPragmadexComando.getApellidos()+"",
                        usuarioParaActualizar.getApellidos()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setApellidos(actualizarUsuarioPragmadexComando.getApellidos());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"IdVicepresidencia",tabla
                        ,actualizarUsuarioPragmadexComando.getIdVicepresidencia()+"",
                        usuarioParaActualizar.getIdVicepresidencia()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setIdVicepresidencia(actualizarUsuarioPragmadexComando.getIdVicepresidencia());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"IdProfesion",tabla
                        ,actualizarUsuarioPragmadexComando.getIdProfesion()+"",
                        usuarioParaActualizar.getIdProfesion()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setIdProfesion(actualizarUsuarioPragmadexComando.getIdProfesion());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"EstadoProfesion",tabla
                        ,actualizarUsuarioPragmadexComando.getEstadoProfesion()+"",
                        usuarioParaActualizar.getEstadoProfesion()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setEstadoProfesion(actualizarUsuarioPragmadexComando.getEstadoProfesion());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"Semestre",tabla
                        ,actualizarUsuarioPragmadexComando.getEstadoProfesion()+"",
                        usuarioParaActualizar.getSemestre()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setSemestre(actualizarUsuarioPragmadexComando.getSemestre());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"TarjetaProfesional",tabla
                        ,actualizarUsuarioPragmadexComando.getTarjetaProfesional()+"",
                        usuarioParaActualizar.getTarjetaProfesional()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setTarjetaProfesional(actualizarUsuarioPragmadexComando.getTarjetaProfesional());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"FechaNacimiento",tabla
                        ,actualizarUsuarioPragmadexComando.getFechaNacimiento()+"",
                        usuarioParaActualizar.getFechaNacimiento()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setFechaNacimiento(actualizarUsuarioPragmadexComando.getFechaNacimiento());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"IdLugarNacimiento",tabla
                        ,actualizarUsuarioPragmadexComando.getIdLugarNacimiento()+"",
                        usuarioParaActualizar.getIdLugarNacimiento()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setIdLugarNacimiento(actualizarUsuarioPragmadexComando.getIdLugarNacimiento());

        validarCambios(historicoCambiosPragmadexList,
                new HistoricoCambiosPragmadex(null,"FechaIngreso",tabla
                        ,actualizarUsuarioPragmadexComando.getFechaIngreso()+"",
                        usuarioParaActualizar.getFechaIngreso()+"",idUsuarioModifica,LocalDate.now()));
        usuarioParaActualizar.setFechaIngreso(actualizarUsuarioPragmadexComando.getFechaIngreso());

        usuarioParaActualizar.setFechaUltimaActualizacion(LocalDate.now());

        usuarioCrudUseCase.actualizar(usuarioParaActualizar);

        guardarHistoricoCambiosPragmadex(historicoCambiosPragmadexList);
    }


    private void guardarHistoricoCambiosPragmadex(List<HistoricoCambiosPragmadex> historicoCambiosPragmadexList) {
        if(!historicoCambiosPragmadexList.isEmpty()){
            historicoCambiosPragmadexCrudUseCase.actualizarTodo(historicoCambiosPragmadexList);
        }
    }

    private void validarCambios(List<HistoricoCambiosPragmadex> historicoCambiosPragmadexList,
                                HistoricoCambiosPragmadex historicoCambiosPragmadex) {

        if(!historicoCambiosPragmadex.getValorAnterior()
                .equalsIgnoreCase(historicoCambiosPragmadex.getValorNuevo())){

            historicoCambiosPragmadexList.add(historicoCambiosPragmadex);
        }
    }

}
