package com.pragma.perfiles.pragmadex.infraestructura.endpoints;

import com.pragma.perfiles.pragmadex.aplicacion.manejador.ManejadorObtenerTodosLosUsuariosPragmaDex;
import com.pragma.perfiles.pragmadex.dominio.modelo.Pragmadex;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("pragma-dex")
@RequiredArgsConstructor
public class EndpointObtenerTodosLosUsuariosPragmaDex {

    private final ManejadorObtenerTodosLosUsuariosPragmaDex manejadorObtenerTodosLosUsuariosPragmaDex;

    @GetMapping
    public Pragmadex obtenerUsuariosPragmadex(){

        return manejadorObtenerTodosLosUsuariosPragmaDex.ejecutar();

    }
}
