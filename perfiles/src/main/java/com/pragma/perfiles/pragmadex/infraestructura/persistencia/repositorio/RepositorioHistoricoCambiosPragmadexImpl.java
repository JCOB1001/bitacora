package com.pragma.perfiles.pragmadex.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.pragmadex.dominio.modelo.HistoricoCambiosPragmadex;
import com.pragma.perfiles.pragmadex.dominio.repositorio.HistoricoCambiosPragmadexRepositorio;
import com.pragma.perfiles.pragmadex.infraestructura.persistencia.builder.HistoricoCambiosPragmadexMapperHistoricoCambiosPragmadexEntity;
import com.pragma.perfiles.pragmadex.infraestructura.persistencia.dao.HistoricoCambiosPragmadexDao;
import com.pragma.perfiles.pragmadex.infraestructura.persistencia.entidad.HistoricoCambiosPragmadexEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
@Log4j2
public class RepositorioHistoricoCambiosPragmadexImpl
        extends CrudRepositorioBaseImpl<Long, HistoricoCambiosPragmadex, HistoricoCambiosPragmadexEntity>
        implements HistoricoCambiosPragmadexRepositorio {

    private final HistoricoCambiosPragmadexDao historicoCambiosPragmadexDao;
    private final HistoricoCambiosPragmadexMapperHistoricoCambiosPragmadexEntity historicoCambiosPragmadexMapperHistoricoCambiosPragmadexEntity;


    @Override
    protected HistoricoCambiosPragmadexDao obtenerRepositorio() {
        return historicoCambiosPragmadexDao;
    }

    @Override
    protected HistoricoCambiosPragmadexMapperHistoricoCambiosPragmadexEntity obtenerConversionBase() {
        return historicoCambiosPragmadexMapperHistoricoCambiosPragmadexEntity;
    }
}
