package com.pragma.perfiles.pragmadex.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HistoricoCambiosPragmadex extends Modelo<Long> {

    private String campoModificado;
    private String tablaModificada;
    private String valorNuevo;
    private String valorAnterior;
    private String idUsuarioModifica;
    private LocalDate fechaModificacion;

    public HistoricoCambiosPragmadex() { super(null); }


    public HistoricoCambiosPragmadex(Long id,
                                     String campoModificado,
                                     String tablaModificada,
                                     String valorNuevo,
                                     String valorAnterior,
                                     String idUsuarioModifica,
                                     LocalDate fechaModificacion) {
        super(id);
        this.campoModificado = campoModificado;
        this.tablaModificada = tablaModificada;
        this.valorNuevo = valorNuevo;
        this.valorAnterior = valorAnterior;
        this.idUsuarioModifica = idUsuarioModifica;
        this.fechaModificacion = fechaModificacion;
    }

}
