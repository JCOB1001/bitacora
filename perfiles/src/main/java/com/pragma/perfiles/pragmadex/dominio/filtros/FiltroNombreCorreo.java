package com.pragma.perfiles.pragmadex.dominio.filtros;

import com.pragma.vacaciones.common.filtro.FiltroBase;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FiltroNombreCorreo extends FiltroBase {

    private String nombreOrCorreo;
    private boolean admin;
    private int minPorcentaje;
    private int maxPorcentaje;
    private String empresa;
}
