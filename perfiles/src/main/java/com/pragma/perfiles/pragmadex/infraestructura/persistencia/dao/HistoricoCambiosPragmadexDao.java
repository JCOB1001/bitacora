package com.pragma.perfiles.pragmadex.infraestructura.persistencia.dao;

import com.pragma.perfiles.pragmadex.infraestructura.persistencia.entidad.HistoricoCambiosPragmadexEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoricoCambiosPragmadexDao extends JpaRepository<HistoricoCambiosPragmadexEntity,Long> {
}
