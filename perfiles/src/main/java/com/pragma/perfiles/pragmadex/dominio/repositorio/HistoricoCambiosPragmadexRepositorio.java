package com.pragma.perfiles.pragmadex.dominio.repositorio;

import com.pragma.perfiles.pragmadex.dominio.modelo.HistoricoCambiosPragmadex;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface HistoricoCambiosPragmadexRepositorio
        extends CrudRepositorioBase<Long, HistoricoCambiosPragmadex> {
}
