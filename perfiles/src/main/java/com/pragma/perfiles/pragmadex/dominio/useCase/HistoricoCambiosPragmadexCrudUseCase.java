package com.pragma.perfiles.pragmadex.dominio.useCase;

import com.pragma.perfiles.pragmadex.dominio.modelo.HistoricoCambiosPragmadex;
import com.pragma.perfiles.pragmadex.dominio.repositorio.HistoricoCambiosPragmadexRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

public class HistoricoCambiosPragmadexCrudUseCase extends CrudUseCaseCommon<HistoricoCambiosPragmadexRepositorio, HistoricoCambiosPragmadex, Long> {

    private final HistoricoCambiosPragmadexRepositorio historicoCambiosPragmadexRepositorio;

    public HistoricoCambiosPragmadexCrudUseCase(HistoricoCambiosPragmadexRepositorio historicoCambiosPragmadexRepositorio) {
        super(historicoCambiosPragmadexRepositorio);
        this.historicoCambiosPragmadexRepositorio = historicoCambiosPragmadexRepositorio;
    }
}
