package com.pragma.perfiles.pragmadex.infraestructura.endpoints;

import com.pragma.perfiles.pragmadex.aplicacion.comando.ObtenerUsuarioPorIdQuery;
import com.pragma.perfiles.pragmadex.aplicacion.manejador.ManejadorObtenerUsuarioPorGoogleId;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@Validated
@RestController
@RequestMapping("pragma-dex")
@RequiredArgsConstructor
public class EndpointObtenerPorGoogleId {

    private final ManejadorObtenerUsuarioPorGoogleId manejadorObtenerUsuarioPorGoogleId;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<ObtenerUsuarioPorIdQuery> obtenerUsuarioPorGoogleId(@NotNull @PathVariable String idUsuario) {

        return manejadorObtenerUsuarioPorGoogleId.ejecutar(idUsuario);
    }
}
