package com.pragma.perfiles.pragmadex.aplicacion.converter;

import com.pragma.perfiles.perfil.dominio.modelo.Fotografia;
import com.pragma.perfiles.perfil.dominio.modelo.TipoFotografia;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.pragmadex.dominio.modelo.InfoEstado;
import com.pragma.perfiles.pragmadex.dominio.modelo.UsuarioPragmaDex;
import com.pragma.perfiles.pragmadex.dominio.useCase.ValidarDatosPersonalesCompletadosUseCase;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UsuarioToUsuarioPragmaDexConverter implements Converter<Usuario, UsuarioPragmaDex> {

    private ValidarDatosPersonalesCompletadosUseCase validarDatosPersonalesCompletadosUseCase;

    public UsuarioToUsuarioPragmaDexConverter(@Lazy ValidarDatosPersonalesCompletadosUseCase validarDatosPersonalesCompletadosUseCase) {
        this.validarDatosPersonalesCompletadosUseCase = validarDatosPersonalesCompletadosUseCase;
    }


    @Override
    public UsuarioPragmaDex convert(Usuario usuario) {
        UsuarioPragmaDex usuarioPragmaDex = new UsuarioPragmaDex();
        usuarioPragmaDex.setCorporateEmail(usuario.getCorreoEmpresarial());
        usuarioPragmaDex.setUserName(usuario.getNombres());
        usuarioPragmaDex.setLastName(usuario.getApellidos());
        usuarioPragmaDex.setUserId(usuario.getGoogleId());
        usuarioPragmaDex.setNumberId(usuario.getIdentificacion());
        Fotografia foto = usuario.obtenerFotografiaPorTipo(TipoFotografia.CUADRADO);
        usuarioPragmaDex.setAvatarPath(foto.getContenido());
        usuarioPragmaDex.setUserRole(usuario.getCargo());
        usuarioPragmaDex.setActive(usuario.getEstado().isValor());

        InfoEstado infoEstado = new InfoEstado();
        infoEstado.setProgressBar(usuario.getBarraProgreso());

        boolean datosPersonales = validarDatosPersonalesCompletadosUseCase.validar(usuario);
        infoEstado.setPersonalInfo(datosPersonales);
        infoEstado.setLaboralInfo(false);
        infoEstado.setAcademicInfo(false);
        usuarioPragmaDex.setInfoStatus(infoEstado);
        return usuarioPragmaDex;
    }
}
