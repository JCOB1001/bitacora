package com.pragma.perfiles.pragmadex.dominio.modelo;

import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Setter
public class UsuarioPragmaDex {

    private String corporateEmail;
    private String userName;
    private String lastName;
    private String userId;
    private List<HabilidadesPragmaDex> skills;
    private String numberId;//cedula
    private List<String> capabilities;
    private String avatarPath;
    private String userRole;
    private boolean isActive;
    private InfoEstado infoStatus;

    public UsuarioPragmaDex() { }

    public UsuarioPragmaDex(String corporateEmail, String userName, String lastName, String userId, List<HabilidadesPragmaDex> skills, String numberId, List<String> capabilities, String avatarPath, String userRole, boolean isActive, InfoEstado infoStatus) {
        this.corporateEmail = corporateEmail;
        this.userName = userName;
        this.lastName = lastName;
        this.userId = userId;
        this.skills = skills;
        this.numberId = numberId;
        this.capabilities = capabilities;
        this.avatarPath = avatarPath;
        this.userRole = userRole;
        this.isActive = isActive;
        this.infoStatus = infoStatus;
    }

    public String getCorporateEmail() {
        return corporateEmail;
    }

    public String getUserName() {
        return userName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUserId() {
        return userId;
    }

    public List<HabilidadesPragmaDex> getSkills() {
        return skills;
    }

    public String getNumberId() {
        return numberId;
    }

    public List<String> getCapabilities() {
        return capabilities;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public String getUserRole() {
        return userRole;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public InfoEstado getInfoStatus() {
        return infoStatus;
    }
}
