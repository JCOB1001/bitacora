package com.pragma.perfiles.pragmadex.infraestructura.endpoints;

import com.pragma.perfiles.pragmadex.aplicacion.comando.ActualizarUsuarioPragmadexComando;
import com.pragma.perfiles.pragmadex.aplicacion.manejador.ManejadorActualizarUsuarioPragmadex;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("pragma-dex")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarUsuarioPragmadex {
    private final ManejadorActualizarUsuarioPragmadex manejadorActualizarUsuarioPragmadex;

    @PutMapping("update-user-info-pragma-dex/{idUsuarioModifica}")
    public RespuestaBase ejecutarActualizarEstadoPragmadex(@NotNull @PathVariable String idUsuarioModifica,
                                                           @NotNull @RequestBody ActualizarUsuarioPragmadexComando actualizarUsuarioPragmadexComando){

        manejadorActualizarUsuarioPragmadex.ejecutar(idUsuarioModifica,actualizarUsuarioPragmadexComando);
        return RespuestaBase.ok("Success");
    }
}
