package com.pragma.perfiles.pragmadex.dominio.error;

public class UsuarioPorGoogleIdNoEncontrado extends RuntimeException{

   public UsuarioPorGoogleIdNoEncontrado(String mensaje){super(mensaje);}

}
