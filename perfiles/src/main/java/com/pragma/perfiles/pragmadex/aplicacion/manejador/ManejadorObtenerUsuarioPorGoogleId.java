package com.pragma.perfiles.pragmadex.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.SolicitudIncorrecta;
import com.pragma.perfiles.contrato_historico.dominio.UseCase.ContratoHistoricoCrudUseCase;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.repositorio.InformacionNominaRepositorio;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorio;
import com.pragma.perfiles.pragmadex.aplicacion.comando.ObtenerUsuarioPorIdQuery;
import com.pragma.perfiles.pragmadex.dominio.error.UsuarioPorGoogleIdNoEncontrado;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
public class ManejadorObtenerUsuarioPorGoogleId {

    private final UsuarioRepositorio usuarioRepositorio;
    private final InformacionNominaRepositorio informacionNominaRepositorio;
    private final ConversionService conversionService;
    private final ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase;


    public ObjetoRespuesta<ObtenerUsuarioPorIdQuery> ejecutar(String id) {
        ObjetoRespuesta<ObtenerUsuarioPorIdQuery> objetoRespuesta;
        ObtenerUsuarioPorIdQuery obtenerUsuarioPorIdQuery = null;
        objetoRespuesta = null;
        try {

            Usuario usuarioConsulta = usuarioRepositorio.findByGoogleId(id);
            if (usuarioConsulta == null) {
                throw new UsuarioPorGoogleIdNoEncontrado("No se encontró el usuario con el id especificado");
            }
            ContratoHistorico contratoHistorico = contratoHistoricoCrudUseCase.findByUsuarioIdAndEstado(usuarioConsulta.getId(), ContratoHistoricoEstado.ACTIVO);
            Usuario usuarioRespuesta = usuarioConsulta;

            obtenerUsuarioPorIdQuery = conversionService.convert(usuarioRespuesta, ObtenerUsuarioPorIdQuery.class);
            if (contratoHistorico == null) {
                obtenerUsuarioPorIdQuery.setIdTipoContrato(null);
                obtenerUsuarioPorIdQuery.setFechaInicioContratoActual(null);
            } else {
                obtenerUsuarioPorIdQuery.setIdTipoContrato(contratoHistorico.getIdTipoContrato());
                obtenerUsuarioPorIdQuery.setFechaInicioContratoActual(String.valueOf(contratoHistorico.getFechaInicio()));
            }

            InformacionNomina informacionNomina = informacionNominaRepositorio.findByUsuarioId(usuarioRespuesta.getId()).findFirst().orElse(null);
            if (informacionNomina == null) {
                obtenerUsuarioPorIdQuery.setCargoNomina(null);
            } else {
                obtenerUsuarioPorIdQuery.setCargoNomina(String.valueOf(informacionNomina.getCargoNomina()));
            }

            objetoRespuesta = new ObjetoRespuesta<ObtenerUsuarioPorIdQuery>(
                    HttpStatus.OK,
                    "Encontrado con éxito",
                    obtenerUsuarioPorIdQuery);


        } catch (SolicitudIncorrecta e) {
            objetoRespuesta = new ObjetoRespuesta<ObtenerUsuarioPorIdQuery>(
                    HttpStatus.NOT_FOUND,
                    "No se encontró un pragmático con el id especificado",
                    obtenerUsuarioPorIdQuery);
            return objetoRespuesta;
        }
        return objetoRespuesta;
    }


}
