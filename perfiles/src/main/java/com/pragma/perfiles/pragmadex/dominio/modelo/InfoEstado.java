package com.pragma.perfiles.pragmadex.dominio.modelo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InfoEstado {
    private boolean personalInfo;
    private boolean laboralInfo;
    private boolean academicInfo;
    private double progressBar;

    public InfoEstado() { }

    public InfoEstado(boolean informacionPersonal, boolean informacionLaboral, boolean informacionAcademica, double barraProgreso) {
        this.personalInfo = informacionPersonal;
        this.laboralInfo = informacionLaboral;
        this.academicInfo = informacionAcademica;
        this.progressBar = barraProgreso;
    }
}
