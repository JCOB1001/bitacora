package com.pragma.perfiles.pragmadex.dominio.respuesta;

import com.pragma.perfiles.pragmadex.dominio.modelo.Pragmadex;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaPragmadex extends ObjetoRespuesta {
    public ObjetoRespuestaPragmadex(Stream<Pragmadex> dato){
        super(dato);
    }
}
