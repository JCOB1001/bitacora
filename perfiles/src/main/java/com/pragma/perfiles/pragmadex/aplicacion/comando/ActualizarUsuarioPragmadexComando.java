package com.pragma.perfiles.pragmadex.aplicacion.comando;

import com.pragma.perfiles.perfil.dominio.modelo.EstadoProfesion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarUsuarioPragmadexComando {

    //Usuario
    private String correoEmpresarial;
    private Long idTipoDocumento;
    private String identificacion;
    private String nombres;
    private String apellidos;
    private String idVicepresidencia;
    private Long idProfesion;
    private EstadoProfesion estadoProfesion;
    private String semestre;
    private String tarjetaProfesional;
    private LocalDate fechaNacimiento;
    private String idLugarNacimiento;
    private LocalDate fechaIngreso;
    //informacion_nomina
    private String cargoNomina;
    private String idTipocontrato;
    //contrato historico
    private LocalDate fechaInicioContratoActual;


}
