package com.pragma.perfiles.pragmadex.dominio.modelo;

import lombok.Data;

@Data
public class HabilidadesPragmaDex {
    private String skillName;
    private int rate;
    private int indice;

}
