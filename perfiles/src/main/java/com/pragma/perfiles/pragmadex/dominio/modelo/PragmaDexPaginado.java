package com.pragma.perfiles.pragmadex.dominio.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PragmaDexPaginado {

    private String message;
    private int status;
    private int registrosPorPagina;
    private Long totalRegistros;
    private boolean ultimaPagina;
    private List<UsuarioPragmaDex> body;

    public PragmaDexPaginado( List<UsuarioPragmaDex> body) {
        this.body = body;
    }

}
