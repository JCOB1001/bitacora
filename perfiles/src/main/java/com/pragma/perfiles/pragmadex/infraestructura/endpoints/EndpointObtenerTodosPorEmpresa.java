package com.pragma.perfiles.pragmadex.infraestructura.endpoints;

import com.pragma.perfiles.pragmadex.aplicacion.manejador.ManejadorObtenerTodosByEmpresa;
import com.pragma.perfiles.pragmadex.dominio.filtros.FiltroNombreCorreo;
import com.pragma.perfiles.pragmadex.dominio.modelo.PragmaDexPaginado;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@Validated
@RestController
@RequestMapping("pragma-dex")
@RequiredArgsConstructor
public class EndpointObtenerTodosPorEmpresa {

    private final ManejadorObtenerTodosByEmpresa manejadorObtenerTodosByEmpresa;

    @GetMapping("/empresa")
    public PragmaDexPaginado obtenerTodosPD(@NotNull FiltroNombreCorreo paginas){
        return manejadorObtenerTodosByEmpresa.ejecutar(paginas);
    }
}
