package com.pragma.perfiles.pragmadex.aplicacion.converter;

import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.pragmadex.aplicacion.comando.ObtenerUsuarioPorIdQuery;
import com.pragma.perfiles.pragmadex.dominio.modelo.UsuarioPragmaDex;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UsuarioToObtenerUsuarioPorIdQueryConverter implements Converter<Usuario, ObtenerUsuarioPorIdQuery> {

    @Override
    public ObtenerUsuarioPorIdQuery convert(Usuario usuario){
        ObtenerUsuarioPorIdQuery obtenerUsuarioPorIdQuery= new ObtenerUsuarioPorIdQuery();
        obtenerUsuarioPorIdQuery.setIdTipoDocumento(String.valueOf(usuario.getIdTipoDocumento()));
        obtenerUsuarioPorIdQuery.setIdentificacion(usuario.getIdentificacion());
        obtenerUsuarioPorIdQuery.setNombres(usuario.getNombres());
        obtenerUsuarioPorIdQuery.setApellidos(usuario.getApellidos());
        obtenerUsuarioPorIdQuery.setCorreoEmpresarial(usuario.getCorreoEmpresarial());
        obtenerUsuarioPorIdQuery.setFechaIngreso(String.valueOf(usuario.getFechaIngreso()));
        obtenerUsuarioPorIdQuery.setFechaNacimiento(String.valueOf(usuario.getFechaNacimiento()));
        obtenerUsuarioPorIdQuery.setIdLugarNacimiento(usuario.getIdLugarNacimiento());
        obtenerUsuarioPorIdQuery.setIdVicepresidencia(usuario.getIdVicepresidencia());
        obtenerUsuarioPorIdQuery.setCargoNomina(usuario.getCargo());
        obtenerUsuarioPorIdQuery.setProfesion(String.valueOf(usuario.getIdProfesion()));
        obtenerUsuarioPorIdQuery.setTarjetaProfesional(usuario.getTarjetaProfesional());
        obtenerUsuarioPorIdQuery.setProfesion(String.valueOf(usuario.getIdProfesion()));
        obtenerUsuarioPorIdQuery.setEstadoProfesion(String.valueOf(usuario.getEstadoProfesion()));
        return obtenerUsuarioPorIdQuery;
    }


}
