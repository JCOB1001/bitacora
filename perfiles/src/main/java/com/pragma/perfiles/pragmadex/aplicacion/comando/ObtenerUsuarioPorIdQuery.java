package com.pragma.perfiles.pragmadex.aplicacion.comando;

import lombok.Data;

@Data
public class ObtenerUsuarioPorIdQuery {

    private String idTipoDocumento; //usuario
    private String identificacion; //usuario
    private String nombres; //usuario
    private String apellidos; //usuario
    private String correoEmpresarial; //usuario
    private String fechaIngreso;  //usuario
    private String fechaInicioContratoActual; //contrato histórico
    private String fechaNacimiento; //usuario
    private String idLugarNacimiento;
    private String idVicepresidencia; //USUARIO
    private String cargoNomina; //info nomina
    private String idTipoContrato; //contrato historico
    private String profesion; //usuario
    private String estadoProfesion;
    private String tarjetaProfesional; // usuario
    private String semestre;

}
