package com.pragma.perfiles.pragmadex.dominio.modelo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Pragmadex  {
    private String message;
    private int status;
    private List<UsuarioPragmaDex> body;

    public Pragmadex() {

    }

    public Pragmadex( List<UsuarioPragmaDex> body) {
        this.body = body;
    }
}
