package com.pragma.perfiles.pragmadex.infraestructura.configuracion;

import com.pragma.perfiles.capacidad.dominio.useCase.CapacidadCrudUseCase;
import com.pragma.perfiles.conocimientos.dominio.repositorio.UsuarioConocimientoRepositorio;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.contrato_historico.dominio.UseCase.ContratoHistoricoCrudUseCase;
import com.pragma.perfiles.estudio.dominio.repositorio.EstudioRepositorio;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.informacion_nomina.dominio.repositorio.InformacionNominaRepositorio;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorio;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.pragmadex.aplicacion.manejador.*;
import com.pragma.perfiles.pragmadex.dominio.repositorio.HistoricoCambiosPragmadexRepositorio;
import com.pragma.perfiles.pragmadex.dominio.useCase.HistoricoCambiosPragmadexCrudUseCase;
import com.pragma.perfiles.pragmadex.dominio.useCase.ValidarDatosPersonalesCompletadosUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;

@Configuration
public class BeanConfigurationPragmadex {

    @Bean
    public ManejadorObtenerTodosLosUsuariosPragmaDex manejadorObtenerTodosLosUsuariosPragmaDex(UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                               ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase,
                                                                                               ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase,
                                                                                               UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase,
                                                                                               EstudioCrudUseCase estudioCrudUseCase,
                                                                                               OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase,
                                                                                               CapacidadCrudUseCase capacidadCrudUseCase
    ) {

        return new ManejadorObtenerTodosLosUsuariosPragmaDex(usuarioCrudUseCase, experienciaLaboralExternaCrudUseCase, experienciaLaboralPragmaCrudUseCase,
                usuarioConocimientoCrudUseCase, estudioCrudUseCase, otrosEstudiosCrudUseCase, capacidadCrudUseCase);
    }

    @Bean
    public ManejadorObtenerTodos manejadorObtenerTodos(UsuarioRepositorio usuarioRepositorio,
                                                       UsuarioConocimientoRepositorio usuarioConocimientoRepositorio,
                                                       ConversionService conversionService
    ) {
        return new ManejadorObtenerTodos(usuarioRepositorio,
                usuarioConocimientoRepositorio,
                conversionService
        );
    }

    @Bean
    public ManejadorObtenerTodosBySkill manejadorObtenerTodosBySkill(UsuarioRepositorio usuarioRepositorio,
                                                                     UsuarioConocimientoRepositorio usuarioConocimientoRepositorio,
                                                                     ConversionService conversionService) {
        return new ManejadorObtenerTodosBySkill(usuarioRepositorio,
                usuarioConocimientoRepositorio,
                conversionService
        );
    }

    @Bean
    public ManejadorActualizarUsuarioPragmadex manejadorActualizarUsuarioPragmadex(UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                   InformacionLaboralCrudUseCase informacionLaboralCrudUseCase,
                                                                                   InformacionNominaCrudUseCase informacionNominaCrudUseCase,
                                                                                   ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase,
                                                                                   HistoricoCambiosPragmadexCrudUseCase historicoCambiosPragmadexCrudUseCase){

        return new ManejadorActualizarUsuarioPragmadex(usuarioCrudUseCase,
                                                       informacionNominaCrudUseCase,
                                                       contratoHistoricoCrudUseCase,
                                                       historicoCambiosPragmadexCrudUseCase);
    }

    @Bean
    public ValidarDatosPersonalesCompletadosUseCase validarDatosPersonalesCompletadosUseCase() {
        return new ValidarDatosPersonalesCompletadosUseCase();
    }

    @Bean
    public ManejadorObtenerUsuarioPorGoogleId manejadorObtenerUsuarioPorIdPragmadex(UsuarioRepositorio usuarioRepositorio,
                                                                                    InformacionNominaRepositorio informacionNominaRepositorio,
                                                                                    ConversionService conversionService,
                                                                                    ContratoHistoricoCrudUseCase contratoHistoricoCrudUseCase
                                                                                    ) {
        return new ManejadorObtenerUsuarioPorGoogleId(usuarioRepositorio,informacionNominaRepositorio,conversionService,contratoHistoricoCrudUseCase);
    }

    @Bean
    public HistoricoCambiosPragmadexCrudUseCase historicoCambiosPragmadexCrudUseCase(HistoricoCambiosPragmadexRepositorio historicoCambiosPragmadexRepositorio) {
        return new HistoricoCambiosPragmadexCrudUseCase(historicoCambiosPragmadexRepositorio);
    }

    @Bean
    public ManejadorObtenerTodosByEmpresa manejadorObtenerTodosByEmpresa(UsuarioRepositorio usuarioRepositorio,
                                                                         UsuarioConocimientoRepositorio usuarioConocimientoRepositorio,
                                                                         ConversionService conversionService){
        return new ManejadorObtenerTodosByEmpresa(usuarioRepositorio, usuarioConocimientoRepositorio, conversionService);
    }
}
