package com.pragma.perfiles.pragmadex.infraestructura.configuracion;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.support.DefaultConversionService;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class ConverterConfig {

    private final List<Converter> converters;

    @Bean
    public ConversionService conversionService () {

        DefaultConversionService service = new DefaultConversionService();
        converters.forEach(c -> service.addConverter(c));
        return service;
    }

}
