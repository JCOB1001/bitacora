package com.pragma.perfiles.pragmadex.aplicacion.manejador;

import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.capacidad.dominio.useCase.CapacidadCrudUseCase;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.TipoFotografia;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.pragmadex.dominio.modelo.HabilidadesPragmaDex;
import com.pragma.perfiles.pragmadex.dominio.modelo.InfoEstado;
import com.pragma.perfiles.pragmadex.dominio.modelo.Pragmadex;
import com.pragma.perfiles.pragmadex.dominio.modelo.UsuarioPragmaDex;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ManejadorObtenerTodosLosUsuariosPragmaDex {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase;
    private final ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase;
    private final UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase;
    private final EstudioCrudUseCase estudioCrudUseCase;
    private final OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase;
    private final CapacidadCrudUseCase capacidadCrudUseCase;



    public Pragmadex ejecutar(){

        Map<String,List<HabilidadesPragmaDex>> conocimientousuarios=obtenerConocimientos();
        Map<String,Boolean> contieneEstudio=tienesEstudio();
        Map<String,Boolean> contieneExperiencia=tieneExperiencia();
        Map<Long,String> capacidad=capacidadCrudUseCase.obtenerTodos().collect(Collectors.toMap(Capacidad::getId,Capacidad::getNombre));

        List<UsuarioPragmaDex> usuariosPragmaDexes =usuarioCrudUseCase.obtenerTodos().map(usuario -> {
            UsuarioPragmaDex usuarioPragmaDexToAdd =new UsuarioPragmaDex();
             usuarioPragmaDexToAdd.setActive(usuario.getEstado()==UsuarioEstado.ACTIVO);

             ArrayList<String> capacidades=new ArrayList<String>();
            for (Long id:usuario.getCapacidad()) {
                if(capacidad.get(id)!=null){
                    capacidades.add(capacidad.get(id));
                }
            }
            usuarioPragmaDexToAdd.setCapabilities(capacidades);

            if (usuario.getFotografias()!=null && !usuario.getFotografias().isEmpty()){
            usuarioPragmaDexToAdd.setAvatarPath(usuario.obtenerFotografiaPorTipo(TipoFotografia.CUADRADO).getContenido());
            }else{
                usuarioPragmaDexToAdd.setAvatarPath(null);
            }
            InfoEstado estado=new InfoEstado();
            estado.setAcademicInfo(contieneEstudio.get(usuario.getId())!=null);
            estado.setLaboralInfo(contieneExperiencia.get(usuario.getId())!=null);

            if(usuario.getNombres()==null || usuario.getNombres().isBlank() || usuario.getApellidos()==null || usuario.getApellidos().isBlank() ||
                    usuario.getPerfilProfesional()==null || usuario.getPerfilProfesional().isBlank() || usuario.getCargo()==null || usuario.getCargo().isBlank()){
                estado.setPersonalInfo(false);
            }  else {
                estado.setPersonalInfo(true);
            }
            estado.setProgressBar(usuario.getBarraProgreso());
            usuarioPragmaDexToAdd.setInfoStatus(estado);

            usuarioPragmaDexToAdd.setCorporateEmail(usuario.getCorreoEmpresarial());
            usuarioPragmaDexToAdd.setNumberId(usuario.getIdentificacion());
            usuarioPragmaDexToAdd.setLastName(usuario.getApellidos());
            usuarioPragmaDexToAdd.setUserId(usuario.getGoogleId());
            usuarioPragmaDexToAdd.setUserName(usuario.getNombres());

            if(conocimientousuarios.get(usuario.getId())==null || conocimientousuarios.get(usuario.getId()).isEmpty()){
                usuarioPragmaDexToAdd.setSkills(new ArrayList<HabilidadesPragmaDex>());
            }else{
                usuarioPragmaDexToAdd.setSkills(conocimientousuarios.get(usuario.getId()));
            }

            usuarioPragmaDexToAdd.setUserRole(usuario.getCargo());
            return usuarioPragmaDexToAdd;
        }).collect(Collectors.toList());

        Pragmadex pragmadex=new Pragmadex();
        pragmadex.setStatus(200);
        pragmadex.setMessage("Users Found");
        pragmadex.setBody(usuariosPragmaDexes);

        return pragmadex;
    }

    public Map<String, Boolean> tienesEstudio(){
          Map<String,Boolean> estudios=new HashMap<>();
          List<Estudio> estudio =estudioCrudUseCase.obtenerTodos().collect(Collectors.toList());
        for (Estudio est:estudio) {
            estudios.put(est.getIdUsuario(),true);
        }
        List<OtrosEstudios> otrosEstudios =otrosEstudiosCrudUseCase.obtenerTodos().collect(Collectors.toList());

        for(OtrosEstudios otro: otrosEstudios){
            estudios.put(otro.getIdUsuario(),true);
        }

        return estudios;
    }


    public Map<String,Boolean> tieneExperiencia(){
        Map<String,Boolean> experiencia=new HashMap<>();
        List<ExperienciaLaboralPragma> expePragma=experienciaLaboralPragmaCrudUseCase.obtenerTodos().collect(Collectors.toList());
        for (ExperienciaLaboralPragma expe:expePragma){
            experiencia.put(expe.getIdUsuario(),true);
        }
        List<ExperienciaLaboralExterna> expeExterna=experienciaLaboralExternaCrudUseCase.obtenerTodos().collect(Collectors.toList());
        for (ExperienciaLaboralExterna expe:expeExterna) {
            experiencia.put(expe.getIdUsuario(),true);
        }
        return experiencia;
    }

    private Map<String,List<HabilidadesPragmaDex>> obtenerConocimientos(){

        Map<String,List<HabilidadesPragmaDex>> usuarioConocimientos=new HashMap<>();
       List<UsuarioConocimiento> conocimientos= usuarioConocimientoCrudUseCase.obtenerTodos()
           .map(conimineto->{ return conimineto; })
           .sorted(Comparator.comparing(UsuarioConocimiento::getIndice))
           .sorted(Comparator.comparing(UsuarioConocimiento::getIdUsuario))
           .collect(Collectors.toList());

        for (UsuarioConocimiento conocimie: conocimientos) {

            HabilidadesPragmaDex habilidad=new HabilidadesPragmaDex();
            habilidad.setRate(conocimie.getPuntaje());
            habilidad.setSkillName(conocimie.getNombreSkill());
            habilidad.setIndice(conocimie.getIndice());

            List<HabilidadesPragmaDex> conocimientoTeporal;
            if (usuarioConocimientos.containsKey(conocimie.getIdUsuario())) {

                conocimientoTeporal = usuarioConocimientos.get(conocimie.getIdUsuario());
                conocimientoTeporal.add(habilidad);
                usuarioConocimientos.put(conocimie.getIdUsuario(), conocimientoTeporal);

            } else {
                conocimientoTeporal = new ArrayList<>();
                conocimientoTeporal.add(habilidad);
                usuarioConocimientos.put(conocimie.getIdUsuario(), conocimientoTeporal);
            }

        }


        return usuarioConocimientos;
    }

}
