package com.pragma.perfiles.pragmadex.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.SolicitudIncorrecta;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.repositorio.UsuarioConocimientoRepositorio;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioEntity;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad.ExperienciaLaboralExternaEntity;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.entidad.ExperienciaLaboralPragmaEntity;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad.OtrosEstudiosEntity;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.repositorio.UsuarioRepositorio;
import com.pragma.perfiles.perfil.dominio.respuesta.ObjetoRespuestaUsuario;
import com.pragma.perfiles.pragmadex.dominio.filtros.FiltroNombreCorreo;
import com.pragma.perfiles.pragmadex.dominio.modelo.HabilidadesPragmaDex;
import com.pragma.perfiles.pragmadex.dominio.modelo.PragmaDexPaginado;
import com.pragma.perfiles.pragmadex.dominio.modelo.UsuarioPragmaDex;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerTodosByEmpresa {

    private final UsuarioRepositorio usuarioRepositorio;

    private final UsuarioConocimientoRepositorio usuarioConocimientoRepositorio;

    private final ConversionService conversionService;

    public PragmaDexPaginado ejecutar(FiltroNombreCorreo paginas){

        PragmaDexPaginado pragmaDexPaginado = null;

        try {
            ObjetoRespuestaUsuario usuariosConsulta = usuarioRepositorio.buscarPorEmpresaYNombrePaginado(paginas);

            List<Usuario> usuarioPragmaDexRespuesta = ((Stream<Usuario>) usuariosConsulta.getDato()).collect(Collectors.toList());
            List<String> idUsuarios = usuarioPragmaDexRespuesta
                    .stream()
                    .map(Usuario::getId)
                    .collect(Collectors.toList());
            Stream<UsuarioConocimiento> usuarioConocimientos = usuarioConocimientoRepositorio.buscarHabilidadesPorListaId(idUsuarios);
            Map<String, List<UsuarioConocimiento>> usuarioConocimientoMapa = usuarioConocimientos
                    .collect(
                            Collectors.groupingBy(UsuarioConocimiento::getIdUsuario)
                    );
            Map<String, Integer> interna = usuarioRepositorio.buscarDatosPorListaIds(idUsuarios, ExperienciaLaboralPragmaEntity.class, paginas).stream()
                    .collect(Collectors.toMap(s -> s, s -> 0));
            Map<String, Integer> externa = usuarioRepositorio.buscarDatosPorListaIds(idUsuarios, ExperienciaLaboralExternaEntity.class, paginas).stream()
                    .collect(Collectors.toMap(s -> s, s -> 0));
            Map<String, Integer> estudios = usuarioRepositorio.buscarDatosPorListaIds(idUsuarios, EstudioEntity.class, paginas).stream()
                    .collect(Collectors.toMap(s -> s, s -> 0));
            Map<String, Integer> otrosEstudios = usuarioRepositorio.buscarDatosPorListaIds(idUsuarios, OtrosEstudiosEntity.class, paginas).stream()
                    .collect(Collectors.toMap(s -> s, s -> 0));
            List<UsuarioPragmaDex> usuariosPragmaRespuesta = usuarioPragmaDexRespuesta
                    .stream().map(usuario -> {
                        UsuarioPragmaDex usuarioPragmaDex = conversionService.convert(usuario, UsuarioPragmaDex.class);
                        List<HabilidadesPragmaDex> habilidadesPragmaDexes = usuarioConocimientoMapa
                                .getOrDefault(usuario.getId(), new ArrayList<>())
                                .stream()
                                .map(usuarioConocimiento -> conversionService.convert(usuarioConocimiento, HabilidadesPragmaDex.class))
                                .collect(Collectors.toList());
                        usuarioPragmaDex.setSkills(habilidadesPragmaDexes);
                        if(interna.get(usuario.getId()) != null || externa.get(usuario.getId()) != null){
                            usuarioPragmaDex.getInfoStatus().setLaboralInfo(true);
                        }
                        if(estudios.get(usuario.getId()) != null|| otrosEstudios.get(usuario.getId()) != null){
                            usuarioPragmaDex.getInfoStatus().setAcademicInfo(true);
                        }
                        return usuarioPragmaDex;
                    })
                    .collect(Collectors.toList());

            pragmaDexPaginado = new PragmaDexPaginado(
                    "Users Found",
                    HttpStatus.OK.value(),
                    usuariosConsulta.getTotalElementos(),
                    usuariosConsulta.getCantidadPaginas(),
                    usuariosConsulta.isUltimaPagina(),
                    usuariosPragmaRespuesta
            );
        }catch (SolicitudIncorrecta e) {
            pragmaDexPaginado = new PragmaDexPaginado();
            pragmaDexPaginado.setMessage(e.getMessage());
            pragmaDexPaginado.setStatus(HttpStatus.NOT_FOUND.value());
        }catch (Exception e){
            pragmaDexPaginado = new PragmaDexPaginado();
            pragmaDexPaginado.setMessage("Error desconocido, contactar al administrador");
            pragmaDexPaginado.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return pragmaDexPaginado;
    }
}
