package com.pragma.perfiles.barra_progreso.aplicacion.manejadores;

import com.pragma.perfiles.barra_progreso.dominio.modelo.PesoValoresInformacionUsuario;
import com.pragma.perfiles.barra_progreso.dominio.useCase.PesoValoresInfoUsuarioCrudUseCase;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.idiomas.dominio.modelo.UsuarioIdiomas;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.intereses.dominio.modelo.Intereses;
import com.pragma.perfiles.intereses.dominio.useCase.InteresesCrudUseCase;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstadoCivil;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGrupoSanguineo;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioTallaCamisa;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ManejadorCalcularValorBarraProgreso {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase;
    private final ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase;
    private final UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase;
    private final InteresesCrudUseCase interesesCrudUseCase;
    private final EstudioCrudUseCase estudioCrudUseCase;
    private final OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase;
    private final PesoValoresInfoUsuarioCrudUseCase pesoValoresInfoUsuarioCrudUseCase;
    private final UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase;


    public double obtenerBarraProgreso(String id){
        Map<String,Double> valores=keyValue();
        double progreso=0.0;
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(id);
        if(usuario.getNombres()!=null && !usuario.getNombres().isBlank()){
            progreso+=valores.get("nombres");
        }
        if(usuario.getApellidos()!=null && !usuario.getApellidos().isBlank()){
            progreso+=valores.get("apellidos");
        }
        if(usuario.getCorreoEmpresarial()!= null && !usuario.getCorreoEmpresarial().isBlank()){
            progreso+=valores.get("correoEmpresarial");
        }
        if(usuario.getCorreoPersonal()!=null && !usuario.getCorreoPersonal().isBlank()){
            progreso+=valores.get("correoPersonal");
        }
        if(usuario.getIdTipoDocumento()!=null){
            progreso+=valores.get("idTipoDocumento");
        }
        if(usuario.getIdentificacion()!=null && !usuario.getIdentificacion().isBlank()){
            progreso+= valores.get("identificacion");
        }
        if(usuario.getGrupoSanguineo()!=null && usuario.getGrupoSanguineo() != UsuarioGrupoSanguineo.NO_ESPECIFICA){
            progreso+=valores.get("grupoSanguineo");
        }
        if(usuario.getTallaCamisa()!=null && usuario.getTallaCamisa() != UsuarioTallaCamisa.NO_ESPECIFICADO){
            progreso+=valores.get("tallaCamisa");
        }
        if(usuario.getEstadoCivil()!=null && usuario.getEstadoCivil() != UsuarioEstadoCivil.NO_ESPECIFICADO){
            progreso+=valores.get("estadoCivil");
        }
        if(usuario.getTelefonoFijo()!=null && !usuario.getTelefonoFijo().isBlank()){
            progreso+=valores.get("telefonoFijo");
        }
        if(usuario.getTelefonoCelular()!=null && !usuario.getTelefonoCelular().isBlank()){
            progreso+=valores.get("telefonoCelular");
        }
        if(usuario.getFechaNacimiento()!=null){
            progreso+=valores.get("fechaNacimiento");
        }
        if(usuario.getIdLugarNacimiento()!=null){
            progreso+=valores.get("idLugarNacimiento");
        }
        if(usuario.getIdLugarRecidencia()!=null){
            progreso+=valores.get("idLugarRecidencia");
        }
        if(usuario.getIdNacionalidad()!=null){
            progreso+=valores.get("idNacionalidad");
        }
        if(usuario.getDireccion()!=null && !usuario.getDireccion().isBlank()){
            progreso+=valores.get("direccion");
        }
        if(usuario.getNombreContactoEmergencia()!=null && !usuario.getNombreContactoEmergencia().isBlank()){
            progreso+=valores.get("nombreContactoEmergencia");
        }
        if(usuario.getParentescoContactoEmergencia()!=null && !usuario.getParentescoContactoEmergencia().isBlank()){
            progreso+=valores.get("parentescoContactoEmergencia");
        }
        if(usuario.getTelefonoContactoEmergencia()!=null && !usuario.getTelefonoContactoEmergencia().isBlank()){
            progreso+=valores.get("telefonoContactoEmergencia");
        }
        if(usuario.getIdProfesion()!=null){
            progreso+=valores.get("idProfesion");
        }
        if(usuario.getTarjetaProfesional()!=null && !usuario.getTarjetaProfesional().isBlank()){
            progreso+=valores.get("tarjetaProfesional");
        }
        if(usuario.getPerfilProfesional()!=null && !usuario.getPerfilProfesional().isBlank()){
            progreso+=valores.get("perfilProfesional");
        }
        if(usuario.getConocimientosTecnicos()!=null && !usuario.getConocimientosTecnicos().isBlank()){
            progreso+=valores.get("conocimientosTecnicos");
        }
        if(usuario.getCargo()!=null && !usuario.getCargo().isBlank()){
            progreso+=valores.get("cargo");
        }
        if(usuario.getFechaIngreso()!=null){
            progreso+= valores.get("fechaIngreso");
        }
        if(usuario.getIdVicepresidencia()!=null && !usuario.getIdVicepresidencia().isBlank()){
            progreso+=valores.get("idVicepresidencia");
        }
        if(usuario.getFotografias() != null && !usuario.getFotografias().isEmpty()){
            progreso+=valores.get("fotografias");
        }
        if(usuario.getCapacidad()!=null&&!usuario.getCapacidad().isEmpty()){
            progreso+=valores.get("capacidad");
        }

        List<ExperienciaLaboralExterna> experienciaLaboralExterna=experienciaLaboralExternaCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList());
        if(experienciaLaboralExterna.size()>0){
            progreso+=valores.get("experienciaLaboralExterna");
        }

        List<ExperienciaLaboralPragma> experienciaLaboralPragmas = experienciaLaboralPragmaCrudUseCase.findByIdUsuarioId(usuario.getId()).collect(Collectors.toList());
        if(experienciaLaboralPragmas.size()>0){
            progreso+=valores.get("experienciaLaboralInterna");
        }

        List<Estudio> estudios = estudioCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList());
        if(estudios.size()>0){
            progreso+=valores.get("estudios");
        }

        List<OtrosEstudios> otrosEstudios = otrosEstudiosCrudUseCase.findByUsuario(id).collect(Collectors.toList());
        if(otrosEstudios.size()>0){
            progreso+=valores.get("otrosEstudios");
        }

        List<Intereses> intereses = interesesCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList());
        if(intereses.size()>0){
            progreso+=valores.get("intereses");
        }

        List<UsuarioConocimiento> usuarioConocimientos= usuarioConocimientoCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList());
        if(usuarioConocimientos.size()>0){
            progreso+=valores.get("conocimientos");
        }

        List<UsuarioIdiomas> idiomas = usuarioIdiomasCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList());
        if(idiomas.size()>0){
            progreso+=valores.get("idiomas");
        }


        return progreso/100;
    }



    public Map<String,Double> keyValue(){
        Map<String,Double> valores=new HashMap<>();

       List<PesoValoresInformacionUsuario> pesoValoresInformacionUsuarios= pesoValoresInfoUsuarioCrudUseCase.obtenerTodos().collect(Collectors.toList());
        for (PesoValoresInformacionUsuario temporal:pesoValoresInformacionUsuarios) {
            valores.put(temporal.getNombreCampo(),temporal.getValor());
        }

        return valores;
    }

}
