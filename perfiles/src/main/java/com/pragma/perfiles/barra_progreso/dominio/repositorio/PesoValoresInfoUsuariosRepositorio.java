package com.pragma.perfiles.barra_progreso.dominio.repositorio;

import com.pragma.perfiles.barra_progreso.dominio.modelo.PesoValoresInformacionUsuario;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface PesoValoresInfoUsuariosRepositorio extends CrudRepositorioBase<String, PesoValoresInformacionUsuario> {
}
