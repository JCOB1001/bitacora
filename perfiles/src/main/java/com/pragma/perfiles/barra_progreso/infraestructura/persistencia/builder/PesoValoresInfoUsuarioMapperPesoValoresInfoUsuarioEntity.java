package com.pragma.perfiles.barra_progreso.infraestructura.persistencia.builder;

import com.pragma.perfiles.barra_progreso.dominio.modelo.PesoValoresInformacionUsuario;
import com.pragma.perfiles.barra_progreso.infraestructura.persistencia.entidad.PesoValoresInfoUsuarioEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PesoValoresInfoUsuarioMapperPesoValoresInfoUsuarioEntity extends ConvertidorBase<PesoValoresInformacionUsuario, PesoValoresInfoUsuarioEntity> {
}
