package com.pragma.perfiles.barra_progreso.aplicacion.manejadores;

import com.pragma.perfiles.barra_progreso.dominio.modelo.InfoEstado;
import com.pragma.perfiles.barra_progreso.dominio.modelo.Response;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ManejadorObtenerValoresInfoUsuarioPorId {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase;
    private final EstudioCrudUseCase estudioCrudUseCase;
    private final ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase;
    private final ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public Response ejecutar(String idUsuario) {
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
        if (usuario == null) {
            throw new UsuarioNoEncontrado("Usuario no encontrado.");
        }
        InfoEstado infoEstado = new InfoEstado();

        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));

        infoEstado.setProgressBar(usuario.getBarraProgreso());

        List<OtrosEstudios> otrosEstudios = otrosEstudiosCrudUseCase.findByUsuario(idUsuario).collect(Collectors.toList());
        List<ExperienciaLaboralExterna> experienciaExterna = experienciaLaboralExternaCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList());
        List<ExperienciaLaboralPragma> experienciaLaboralPragmas = experienciaLaboralPragmaCrudUseCase.findByIdUsuarioId(usuario.getId()).collect(Collectors.toList());
        List<Estudio> estudios = estudioCrudUseCase.findByUsuarioId(usuario.getId()).collect(Collectors.toList());


        if (usuario.getNombres() == null || usuario.getNombres().isBlank() || usuario.getApellidos() == null || usuario.getApellidos().isBlank() ||
                usuario.getPerfilProfesional() == null || usuario.getPerfilProfesional().isBlank() || usuario.getCargo() == null || usuario.getCargo().isBlank()) {
            infoEstado.setPersonalInfo(false);
        } else {
            infoEstado.setPersonalInfo(true);
        }
        if(!otrosEstudios.isEmpty() || !estudios.isEmpty()) {
            infoEstado.setAcademicInfo(true);
        } else {
            infoEstado.setAcademicInfo(false);
        }

        if(!experienciaExterna.isEmpty() || !experienciaLaboralPragmas.isEmpty()) {
            infoEstado.setLaboralInfo(true);
        } else {
            infoEstado.setAcademicInfo(false);
        }

        usuarioCrudUseCase.actualizar(usuario);

        Response response = new Response();
        response.setBody(infoEstado);
        response.setStatus(200);
        response.setMensaje("progress");

        return response;
    }

}
