package com.pragma.perfiles.barra_progreso.dominio.modelo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
//@AllArgsConstructor
@Getter @Setter
public class Response {

    private String mensaje;
    private int status;
    private InfoEstado body;

    public Response() {

    }
}
