package com.pragma.perfiles.barra_progreso.dominio.respuesta;

import com.pragma.perfiles.barra_progreso.dominio.modelo.PesoValoresInformacionUsuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaPesoValoresInfoUsuarios extends ObjetoRespuesta {
    public ObjetoRespuestaPesoValoresInfoUsuarios(Stream<PesoValoresInformacionUsuario> dato){
        super(dato);
    }
}
