package com.pragma.perfiles.barra_progreso.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.barra_progreso.dominio.modelo.PesoValoresInformacionUsuario;
import com.pragma.perfiles.barra_progreso.dominio.repositorio.PesoValoresInfoUsuariosRepositorio;
import com.pragma.perfiles.barra_progreso.infraestructura.persistencia.builder.PesoValoresInfoUsuarioMapperPesoValoresInfoUsuarioEntity;
import com.pragma.perfiles.barra_progreso.infraestructura.persistencia.dao.PesoValoresInfoUsuarioDao;
import com.pragma.perfiles.barra_progreso.infraestructura.persistencia.entidad.PesoValoresInfoUsuarioEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class PesoValoresInfoUsuarioRepositorioImpl extends CrudRepositorioBaseImpl<String, PesoValoresInformacionUsuario, PesoValoresInfoUsuarioEntity> implements PesoValoresInfoUsuariosRepositorio {
    private final PesoValoresInfoUsuarioDao pesoValoresInfoUsuarioDao;
    private final PesoValoresInfoUsuarioMapperPesoValoresInfoUsuarioEntity pesoValoresInfoUsuarioMapperPesoValoresInfoUsuarioEntity;


    @Override
    protected CrudRepository obtenerRepositorio() {
        return pesoValoresInfoUsuarioDao;
    }

    @Override
    protected ConvertidorBase obtenerConversionBase() {
        return pesoValoresInfoUsuarioMapperPesoValoresInfoUsuarioEntity;
    }
}
