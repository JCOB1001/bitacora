package com.pragma.perfiles.barra_progreso.infraestructura.endpoints;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorObtenerValoresInfoUsuarioListarTodo;
import com.pragma.perfiles.barra_progreso.dominio.modelo.PesoValoresInformacionUsuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("valores-informacion-usuario")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerValoresInfoUsuarioListarTodo {

    private final ManejadorObtenerValoresInfoUsuarioListarTodo manejadorObtenerValoresInfoUsuarioListarTodo;

    @GetMapping
    public ObjetoRespuesta<Stream<PesoValoresInformacionUsuario>> ejecutar(){
        Stream<PesoValoresInformacionUsuario> pesoValoresInformacionUsuarioStream = manejadorObtenerValoresInfoUsuarioListarTodo.ejecutar();
        return new ObjetoRespuesta<>(HttpStatus.OK, pesoValoresInformacionUsuarioStream);
    }
}
