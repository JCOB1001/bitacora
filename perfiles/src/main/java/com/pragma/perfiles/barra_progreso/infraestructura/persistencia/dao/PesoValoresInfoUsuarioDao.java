package com.pragma.perfiles.barra_progreso.infraestructura.persistencia.dao;

import com.pragma.perfiles.barra_progreso.infraestructura.persistencia.entidad.PesoValoresInfoUsuarioEntity;
import org.springframework.data.repository.CrudRepository;

public interface PesoValoresInfoUsuarioDao extends CrudRepository<PesoValoresInfoUsuarioEntity,String> {
}
