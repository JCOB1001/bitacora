package com.pragma.perfiles.barra_progreso.dominio.useCase;

import com.pragma.perfiles.barra_progreso.dominio.modelo.PesoValoresInformacionUsuario;
import com.pragma.perfiles.barra_progreso.dominio.repositorio.PesoValoresInfoUsuariosRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

public class PesoValoresInfoUsuarioCrudUseCase extends CrudUseCaseCommon<PesoValoresInfoUsuariosRepositorio, PesoValoresInformacionUsuario,String> {

    private final PesoValoresInfoUsuariosRepositorio pesoValoresInfoUsuariosRepositorio;

    public PesoValoresInfoUsuarioCrudUseCase(PesoValoresInfoUsuariosRepositorio pesoValoresInfoUsuariosRepositorio){
        super(pesoValoresInfoUsuariosRepositorio);
        this.pesoValoresInfoUsuariosRepositorio=pesoValoresInfoUsuariosRepositorio;
    }
}
