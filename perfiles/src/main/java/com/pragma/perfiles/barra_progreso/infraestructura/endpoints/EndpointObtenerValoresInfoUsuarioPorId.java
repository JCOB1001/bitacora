package com.pragma.perfiles.barra_progreso.infraestructura.endpoints;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorObtenerValoresInfoUsuarioPorId;
import com.pragma.perfiles.barra_progreso.dominio.modelo.InfoEstado;
import com.pragma.perfiles.barra_progreso.dominio.modelo.Response;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("valores-informacion-usuario")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerValoresInfoUsuarioPorId {

    @Autowired
    private final ManejadorObtenerValoresInfoUsuarioPorId manejadorObtenerValoresInfoUsuarioPorId;

    @GetMapping("/{idUsuario}")
    public Response ejecutarObtenerValoresInfoUsuarioPorId(@NotNull @PathVariable String idUsuario){
        return manejadorObtenerValoresInfoUsuarioPorId.ejecutar(idUsuario);
    }

}
