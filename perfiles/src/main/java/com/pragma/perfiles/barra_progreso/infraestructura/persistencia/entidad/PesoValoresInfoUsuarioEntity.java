package com.pragma.perfiles.barra_progreso.infraestructura.persistencia.entidad;

import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "barra_progreso",
        uniqueConstraints = {@UniqueConstraint(
                name = "uq_nombre_campo",
                columnNames = {"nombre_campo"})
})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PesoValoresInfoUsuarioEntity implements IdEntidad<String>{

    public interface ATRIBUTOS extends Atributos {
        String NOMBRE_CAMPO="nombreCampo";
        String VALOR="valor";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "nombre_campo")
    private String nombreCampo;

    @Column(name = "valor",columnDefinition = "Decimal(10,3) default'3.125'")
    private double valor;

}
