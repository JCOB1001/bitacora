package com.pragma.perfiles.barra_progreso.infraestructura.configuracion;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorObtenerValoresInfoUsuarioListarTodo;
import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorObtenerValoresInfoUsuarioPorId;
import com.pragma.perfiles.barra_progreso.dominio.repositorio.PesoValoresInfoUsuariosRepositorio;
import com.pragma.perfiles.barra_progreso.dominio.useCase.PesoValoresInfoUsuarioCrudUseCase;
import com.pragma.perfiles.conocimientos.dominio.useCase.UsuarioConocimientoCrudUseCase;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.useCase.ExperienciaLaboralExternaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.idiomas.dominio.useCase.UsuarioIdiomasCrudUseCase;
import com.pragma.perfiles.intereses.dominio.useCase.InteresesCrudUseCase;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationPesoValoresInfoUsuario {

    @Bean
    public PesoValoresInfoUsuarioCrudUseCase pesoValoresInfoUsuarioCrudUseCase(PesoValoresInfoUsuariosRepositorio pesoValoresInfoUsuariosRepositorio){
        return new PesoValoresInfoUsuarioCrudUseCase(pesoValoresInfoUsuariosRepositorio);
    }

    @Bean
    public ManejadorObtenerValoresInfoUsuarioListarTodo manejadorObtenerValoresInfoUsuarioListarTodo(PesoValoresInfoUsuarioCrudUseCase pesoValoresInfoUsuarioCrudUseCase){
        return new ManejadorObtenerValoresInfoUsuarioListarTodo(pesoValoresInfoUsuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerValoresInfoUsuarioPorId manejadorObtenerValoresInfoUsuarioPorId(UsuarioCrudUseCase usuarioCrudUseCase, OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase, EstudioCrudUseCase estudioCrudUseCase, ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase, ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase, ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso ){
        return new ManejadorObtenerValoresInfoUsuarioPorId(usuarioCrudUseCase,otrosEstudiosCrudUseCase,estudioCrudUseCase,experienciaLaboralExternaCrudUseCase,experienciaLaboralPragmaCrudUseCase,manejadorCalcularValorBarraProgreso);
    }

    @Bean
    public ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso(UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                   ExperienciaLaboralExternaCrudUseCase experienciaLaboralExternaCrudUseCase,
                                                                                   ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase,
                                                                                   UsuarioConocimientoCrudUseCase usuarioConocimientoCrudUseCase,
                                                                                   InteresesCrudUseCase interesesCrudUseCase,
                                                                                   EstudioCrudUseCase estudioCrudUseCase,
                                                                                   OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase,
                                                                                   PesoValoresInfoUsuarioCrudUseCase pesoValoresInfoUsuarioCrudUseCase,
                                                                                   UsuarioIdiomasCrudUseCase usuarioIdiomasCrudUseCase){
        return new ManejadorCalcularValorBarraProgreso(usuarioCrudUseCase,experienciaLaboralExternaCrudUseCase,experienciaLaboralPragmaCrudUseCase,usuarioConocimientoCrudUseCase,interesesCrudUseCase,estudioCrudUseCase,otrosEstudiosCrudUseCase,pesoValoresInfoUsuarioCrudUseCase,usuarioIdiomasCrudUseCase);
    }
}
