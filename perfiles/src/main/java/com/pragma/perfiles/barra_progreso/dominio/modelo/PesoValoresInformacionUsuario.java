package com.pragma.perfiles.barra_progreso.dominio.modelo;

import com.pragma.perfiles.perfil.dominio.modelo.*;
import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class PesoValoresInformacionUsuario extends Modelo<String> {
    private String nombreCampo;
    private double valor;

    public PesoValoresInformacionUsuario(){
        super(null);
    }

    public PesoValoresInformacionUsuario(String id
            ,String nombreCampo
            , double valor
            ) {
        super(id);
        this.nombreCampo = nombreCampo;
        this.valor = valor;
    }
}
