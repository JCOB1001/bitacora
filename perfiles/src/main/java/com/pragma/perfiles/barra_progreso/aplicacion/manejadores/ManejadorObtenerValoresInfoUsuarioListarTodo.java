package com.pragma.perfiles.barra_progreso.aplicacion.manejadores;

import com.pragma.perfiles.barra_progreso.dominio.modelo.PesoValoresInformacionUsuario;
import com.pragma.perfiles.barra_progreso.dominio.useCase.PesoValoresInfoUsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerValoresInfoUsuarioListarTodo {
    private final PesoValoresInfoUsuarioCrudUseCase pesoValoresInfoUsuarioCrudUseCase;

    public Stream<PesoValoresInformacionUsuario> ejecutar(){
        return pesoValoresInfoUsuarioCrudUseCase.obtenerTodos();
    }
}
