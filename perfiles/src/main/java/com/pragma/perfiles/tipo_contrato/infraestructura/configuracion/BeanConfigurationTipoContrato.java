package com.pragma.perfiles.tipo_contrato.infraestructura.configuracion;

import com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.FabricaInformacionNomina;
import com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.impl.InformacionNominaMapperActualizarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.impl.InformacionNominaMapperGuardarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.manejador.ManejadorEliminarInformacionNomina;
import com.pragma.perfiles.informacion_nomina.aplicacion.manejador.ManejadorObtenerInformacionNominaList;
import com.pragma.perfiles.informacion_nomina.aplicacion.manejador.ManejadorObtenerInformacionNominaPorId;
import com.pragma.perfiles.informacion_nomina.dominio.repositorio.InformacionNominaRepositorio;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.tipo_contrato.aplicacion.fabrica.FabricaTipoContrato;
import com.pragma.perfiles.tipo_contrato.aplicacion.fabrica.impl.TipoContratoMapperActualizarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.fabrica.impl.TipoContratoMapperGuardarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.manejador.*;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.repositorio.TipoContratoRepositorio;
import com.pragma.perfiles.tipo_contrato.dominio.useCase.TipoContratoCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationTipoContrato {
    @Bean
    public ManejadorGuardarTipoContrato manejadorGuardarTipoContrato(TipoContratoCrudUseCase tipoContratoCrudUseCase,
                                                                     FabricaTipoContrato fabricaTipoContrato){
        return new ManejadorGuardarTipoContrato(tipoContratoCrudUseCase, fabricaTipoContrato);
    }

    @Bean
    public ManejadorActualizarTipoContrato manejadorActualizarTipoContrato(TipoContratoCrudUseCase tipoContratoCrudUseCase,
                                                                           FabricaTipoContrato fabricaTipoContrato){
        return new ManejadorActualizarTipoContrato(tipoContratoCrudUseCase, fabricaTipoContrato);
    }

    @Bean
    public ManejadorEliminarTipoContrato manejadorEliminarTipoContrato(TipoContratoCrudUseCase tipoContratoCrudUseCase){
        return new ManejadorEliminarTipoContrato(tipoContratoCrudUseCase);
    }

    @Bean
    public ManejadorObtenerTipoContratoPorId manejadorObtenerTipoContratoPorId(TipoContratoCrudUseCase tipoContratoCrudUseCase){
        return new ManejadorObtenerTipoContratoPorId(tipoContratoCrudUseCase);
    }

    @Bean
    public ManejadorObtenerTipoContratoList manejadorObtenerTipoContratoList(TipoContratoCrudUseCase tipoContratoCrudUseCase){
        return new ManejadorObtenerTipoContratoList(tipoContratoCrudUseCase);
    }

    @Bean
    public TipoContratoCrudUseCase tipoContratoCrudUseCase(TipoContratoRepositorio tipoContratoRepositorio){
        return new TipoContratoCrudUseCase(tipoContratoRepositorio);
    }

    @Bean
    public FabricaTipoContrato fabricaTipoContrato(
            TipoContratoMapperGuardarTipoContratoComando tipoContratoMapperGuardarTipoContratoComando,
            TipoContratoMapperActualizarTipoContratoComando tipoContratoMapperActualizarTipoContratoComando){
        return new FabricaTipoContrato(tipoContratoMapperGuardarTipoContratoComando, tipoContratoMapperActualizarTipoContratoComando);
    }
}
