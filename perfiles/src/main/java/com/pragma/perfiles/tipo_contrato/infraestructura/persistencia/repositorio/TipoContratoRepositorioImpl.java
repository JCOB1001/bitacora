package com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.repositorio.TipoContratoRepositorio;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.builder.TipoContratoMapperTipoContratoEntity;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.dao.TipoContratoCustomDao;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.dao.TipoContratoDao;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.entidad.TipoContratoEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class TipoContratoRepositorioImpl extends CrudRepositorioBaseImpl<String, TipoContrato, TipoContratoEntity> implements TipoContratoRepositorio {
    private final TipoContratoDao tipoContratoDao;
    private final TipoContratoMapperTipoContratoEntity tipoContratoMapperTipoContratoEntity;
    //private final TipoContratoCustomDao tipoContratoCustomDao;

    @Override
    protected TipoContratoDao obtenerRepositorio() {
        return tipoContratoDao;
    }

    @Override
    protected TipoContratoMapperTipoContratoEntity obtenerConversionBase() {
        return tipoContratoMapperTipoContratoEntity;
    }

    @Override
    public String obtenerIdTipoContratoPorCoincidenciaNombre(String nombre){
        return this.tipoContratoDao.obtenerIdTipoContratoPorCoincidenciaNombre(nombre);
    }

}
