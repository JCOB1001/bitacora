package com.pragma.perfiles.tipo_contrato.aplicacion.manejador;

import com.pragma.perfiles.tipo_contrato.aplicacion.comando.GuardarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.fabrica.FabricaTipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.useCase.TipoContratoCrudUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorGuardarTipoContrato {

    private final TipoContratoCrudUseCase tipoContratoCrudUseCase;
    private final FabricaTipoContrato fabricaTipoContrato;

    public void ejecutar(GuardarTipoContratoComando guardarTipoContratoComando){
        TipoContrato tipoContrato = fabricaTipoContrato.guardarTipoContrato(guardarTipoContratoComando);
        TipoContrato tipoContratoNuevo = tipoContratoCrudUseCase.guardar(tipoContrato);
    }
}
