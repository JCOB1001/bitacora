package com.pragma.perfiles.tipo_contrato.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesTipoContrato;
import com.pragma.perfiles.tipo_contrato.aplicacion.manejador.ManejadorObtenerTipoContratoPorId;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("tipo-contrato")
@RequiredArgsConstructor
@Validated
public class EndPointObtenerTipoContratoPorId {

    private final ManejadorObtenerTipoContratoPorId manejadorObtenerTipoContratoPorId;

    @GetMapping("/{idTipoContrato}")
    public ObjetoRespuesta<TipoContrato> ejecutarObtenerTipoContratoPorId(@NotNull @PathVariable String idTipoContrato){
        ObjetoRespuesta<TipoContrato> tipoContratoObjetoRespuesta = manejadorObtenerTipoContratoPorId.ejecutar(idTipoContrato);
        if(tipoContratoObjetoRespuesta.getDato() == null){
            throw new ExcepcionesTipoContrato("No existe este tipo de contrato");
        }
        return new ObjetoRespuesta<TipoContrato>(HttpStatus.OK, tipoContratoObjetoRespuesta.getDato());
    }
}
