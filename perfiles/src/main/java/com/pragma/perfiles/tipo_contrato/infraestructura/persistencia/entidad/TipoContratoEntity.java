package com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.entidad;

import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "tipo_contrato")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TipoContratoEntity implements IdEntidad<String> {
    public interface ATRIBUTOS extends IdEntidad.Atributos {
        String DESCRIPCION= "descripcion";
        String VACACIONES="vacaciones";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "vacaciones")
    private boolean vacaciones;

}
