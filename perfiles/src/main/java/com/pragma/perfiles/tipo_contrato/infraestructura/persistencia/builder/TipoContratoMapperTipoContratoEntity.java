package com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.builder;

import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.entidad.TipoContratoEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TipoContratoMapperTipoContratoEntity extends ConvertidorBase<TipoContrato, TipoContratoEntity> {
}
