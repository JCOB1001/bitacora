package com.pragma.perfiles.tipo_contrato.dominio.respuesta;

import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaTipoContrato extends ObjetoRespuesta {
    public ObjetoRespuestaTipoContrato(Stream<TipoContrato> dato){
        super(dato);
    }

}
