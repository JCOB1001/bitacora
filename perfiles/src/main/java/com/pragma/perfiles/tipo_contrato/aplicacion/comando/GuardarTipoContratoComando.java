package com.pragma.perfiles.tipo_contrato.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuardarTipoContratoComando {
    private String descripcion;
    private boolean vacaciones;
}
