package com.pragma.perfiles.tipo_contrato.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TipoContrato extends Modelo<String> {
    private String descripcion;

    private boolean vacaciones;

    public TipoContrato(String id, String descripcion, boolean vacaciones) {
        super(id);
        this.descripcion = descripcion;
        this.vacaciones = vacaciones;
    }

    public TipoContrato() {
        super(null);
    }
}
