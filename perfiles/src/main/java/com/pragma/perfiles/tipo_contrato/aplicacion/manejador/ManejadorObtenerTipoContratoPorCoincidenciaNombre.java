package com.pragma.perfiles.tipo_contrato.aplicacion.manejador;

import com.pragma.perfiles.tipo_contrato.dominio.repositorio.TipoContratoRepositorio;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.repositorio.TipoContratoRepositorioImpl;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerTipoContratoPorCoincidenciaNombre {

    private final TipoContratoRepositorio tipoContratoRepositorio;

    public ObjetoRespuesta<String> ejecutar(String nombre){
        String tipoDocumento = tipoContratoRepositorio.obtenerIdTipoContratoPorCoincidenciaNombre(nombre);
        return new ObjetoRespuesta<String>(tipoDocumento);


    }
}
