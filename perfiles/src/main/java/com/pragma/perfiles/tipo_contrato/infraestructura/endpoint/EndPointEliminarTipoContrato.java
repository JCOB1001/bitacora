package com.pragma.perfiles.tipo_contrato.infraestructura.endpoint;

import com.pragma.perfiles.tipo_contrato.aplicacion.manejador.ManejadorEliminarTipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("tipo-contrato")
@RequiredArgsConstructor
@Validated
public class EndPointEliminarTipoContrato {
    private final ManejadorEliminarTipoContrato manejadorEliminarTipoContrato;

    @DeleteMapping("/{idTipoContrato}")
    public ObjetoRespuesta<TipoContrato> ejecutarEliminarTipoContrato(@PathVariable String idTipoContrato){
        ObjetoRespuesta<TipoContrato> tipoContratoObjetoRespuesta = manejadorEliminarTipoContrato.ejecutar(idTipoContrato);
        return new ObjetoRespuesta<TipoContrato>(HttpStatus.OK, tipoContratoObjetoRespuesta.getDato());
    }
}
