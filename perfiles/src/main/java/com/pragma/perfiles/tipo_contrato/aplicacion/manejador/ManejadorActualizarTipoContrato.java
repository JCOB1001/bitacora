package com.pragma.perfiles.tipo_contrato.aplicacion.manejador;

import com.pragma.perfiles.tipo_contrato.aplicacion.comando.ActualizarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.fabrica.FabricaTipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.useCase.TipoContratoCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorActualizarTipoContrato {
    private final TipoContratoCrudUseCase tipoContratoCrudUseCase;
    private final FabricaTipoContrato fabricaTipoContrato;

    public ObjetoRespuesta<TipoContrato> ejecutar(String idTipoContrato , ActualizarTipoContratoComando actualizarTipoContratoComando){
        TipoContrato tipoContrato = tipoContratoCrudUseCase.obtenerPorId(idTipoContrato);

        if (tipoContrato !=null){
            TipoContrato tipoContratoNuevo = fabricaTipoContrato.actualizarTipoContrato(actualizarTipoContratoComando);
            tipoContratoNuevo.setId(idTipoContrato);
            tipoContratoCrudUseCase.actualizar(tipoContratoNuevo);
            tipoContrato = tipoContratoNuevo;
        }

        return new ObjetoRespuesta<TipoContrato>(tipoContrato);
    }
}
