package com.pragma.perfiles.tipo_contrato.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesTipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.useCase.TipoContratoCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarTipoContrato {
    private final TipoContratoCrudUseCase tipoContratoCrudUseCase;

    public ObjetoRespuesta<TipoContrato> ejecutar(String idTipoContrato){
        TipoContrato tipoContrato = tipoContratoCrudUseCase.obtenerPorId(idTipoContrato);

        if(tipoContrato == null){
            throw new ExcepcionesTipoContrato("No existe este tipo de contrato");
        }

        tipoContratoCrudUseCase.eliminarPorId(idTipoContrato);
        return new ObjetoRespuesta<TipoContrato>(tipoContrato);
    }
}
