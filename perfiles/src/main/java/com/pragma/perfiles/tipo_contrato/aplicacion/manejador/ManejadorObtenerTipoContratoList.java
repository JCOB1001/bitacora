package com.pragma.perfiles.tipo_contrato.aplicacion.manejador;


import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.useCase.TipoContratoCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerTipoContratoList {
    private final TipoContratoCrudUseCase tipoContratoCrudUseCase;

    public ObjetoRespuesta<Stream<TipoContrato>> ejecutar(){
        Stream<TipoContrato> tipoContratoStream = tipoContratoCrudUseCase.obtenerTodos();
        return new ObjetoRespuesta<Stream<TipoContrato>>(tipoContratoStream);
    }
}
