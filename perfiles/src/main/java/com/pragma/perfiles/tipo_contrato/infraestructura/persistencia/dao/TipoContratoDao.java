package com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.dao;

import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.entidad.TipoContratoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoContratoDao extends JpaRepository<TipoContratoEntity,String>, TipoContratoCustomDao {

}
