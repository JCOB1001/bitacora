package com.pragma.perfiles.tipo_contrato.dominio.repositorio;

import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface TipoContratoRepositorio extends CrudRepositorioBase<String, TipoContrato> {

    public String obtenerIdTipoContratoPorCoincidenciaNombre(String nombre);
}
