package com.pragma.perfiles.tipo_contrato.infraestructura.endpoint;

import com.pragma.perfiles.informacion_nomina.aplicacion.comando.GuardarInformacionNominaComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.comando.GuardarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.manejador.ManejadorGuardarTipoContrato;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("tipo-contrato")
@RequiredArgsConstructor
@Validated
public class EndPointGuardarTipoContrato {
    private final ManejadorGuardarTipoContrato manejadorGuardarTipoContrato;

    @PostMapping
    public RespuestaBase ejecutarGuardarTipoContrato(@NotNull @RequestBody GuardarTipoContratoComando guardarTipoContratoComando){
        manejadorGuardarTipoContrato.ejecutar(guardarTipoContratoComando);
        return RespuestaBase.ok("Guardado Exitosamente");
    }
}
