package com.pragma.perfiles.tipo_contrato.aplicacion.fabrica;

import com.pragma.perfiles.tipo_contrato.aplicacion.comando.ActualizarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.comando.GuardarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.fabrica.impl.TipoContratoMapperActualizarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.fabrica.impl.TipoContratoMapperGuardarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaTipoContrato {

    private final TipoContratoMapperGuardarTipoContratoComando tipoContratoMapperGuardarTipoContratoComando;
    private final TipoContratoMapperActualizarTipoContratoComando tipoContratoMapperActualizarTipoContratoComando;


    public TipoContrato guardarTipoContrato(GuardarTipoContratoComando guardarTipoContratoComando){
        return tipoContratoMapperGuardarTipoContratoComando.rightToLeft(guardarTipoContratoComando);
    }

    public TipoContrato actualizarTipoContrato(ActualizarTipoContratoComando actualizarTipoContratoComando){
        return tipoContratoMapperActualizarTipoContratoComando.rightToLeft(actualizarTipoContratoComando);
    }
}
