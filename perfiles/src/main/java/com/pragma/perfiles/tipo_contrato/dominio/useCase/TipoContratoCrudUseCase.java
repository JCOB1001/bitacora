package com.pragma.perfiles.tipo_contrato.dominio.useCase;

import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.repositorio.TipoContratoRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

public class TipoContratoCrudUseCase extends CrudUseCaseCommon<TipoContratoRepositorio, TipoContrato, String> {

    private final TipoContratoRepositorio tipoContratoRepositorio;

    public TipoContratoCrudUseCase(TipoContratoRepositorio tipoContratoRepositorio) {
        super(tipoContratoRepositorio);
        this.tipoContratoRepositorio=tipoContratoRepositorio;
    }

}
