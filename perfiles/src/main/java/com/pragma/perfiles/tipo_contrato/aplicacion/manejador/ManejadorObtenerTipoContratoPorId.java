package com.pragma.perfiles.tipo_contrato.aplicacion.manejador;

import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.useCase.TipoContratoCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerTipoContratoPorId {
    private final TipoContratoCrudUseCase tipoContratoCrudUseCase;

    public ObjetoRespuesta<TipoContrato> ejecutar(String idTipoContrato){
        TipoContrato tipoContrato = tipoContratoCrudUseCase.obtenerPorId(idTipoContrato);
        return new ObjetoRespuesta<TipoContrato>(tipoContrato);
    }
}
