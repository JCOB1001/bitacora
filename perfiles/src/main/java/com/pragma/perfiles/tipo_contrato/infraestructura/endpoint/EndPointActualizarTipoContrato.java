package com.pragma.perfiles.tipo_contrato.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionNomina;
import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesTipoContrato;
import com.pragma.perfiles.informacion_nomina.aplicacion.comando.ActualizarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.tipo_contrato.aplicacion.comando.ActualizarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.manejador.ManejadorActualizarTipoContrato;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("tipo-contrato")
@RequiredArgsConstructor
@Validated
public class EndPointActualizarTipoContrato {
    private final ManejadorActualizarTipoContrato manejadorActualizarTipoContrato;

    @PutMapping("/{idTipoContrato}")
    public ObjetoRespuesta<TipoContrato> ejecutarActualizarTipoContrato(@NotNull @PathVariable String idTipoContrato,
                                                                          @NotNull @RequestBody ActualizarTipoContratoComando
                                                                                       actualizarTipoContratoComando){
        ObjetoRespuesta<TipoContrato> tipoContratoObjetoRespuesta = manejadorActualizarTipoContrato.ejecutar(idTipoContrato,
                actualizarTipoContratoComando);
        if(tipoContratoObjetoRespuesta.getDato() == null){
            throw new ExcepcionesTipoContrato("No existe el tipo de contrato");
        }
        return new ObjetoRespuesta<TipoContrato>(HttpStatus.OK, tipoContratoObjetoRespuesta.getDato());
    }
}
