package com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.dao;

public interface TipoContratoCustomDao {
    public String obtenerIdTipoContratoPorCoincidenciaNombre(String nombre);

}
