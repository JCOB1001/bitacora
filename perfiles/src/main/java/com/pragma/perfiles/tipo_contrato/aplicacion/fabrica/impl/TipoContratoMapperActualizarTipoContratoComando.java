package com.pragma.perfiles.tipo_contrato.aplicacion.fabrica.impl;


import com.pragma.perfiles.tipo_contrato.aplicacion.comando.ActualizarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TipoContratoMapperActualizarTipoContratoComando extends ConvertidorBase<TipoContrato, ActualizarTipoContratoComando> {
}
