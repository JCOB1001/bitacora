package com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.dao.impl;

import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.dao.TipoContratoCustomDao;
import com.pragma.perfiles.tipo_contrato.infraestructura.persistencia.entidad.TipoContratoEntity;
import lombok.RequiredArgsConstructor;
import net.ricecode.similarity.JaroWinklerStrategy;
import net.ricecode.similarity.SimilarityStrategy;
import net.ricecode.similarity.StringSimilarityService;
import net.ricecode.similarity.StringSimilarityServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.text.Normalizer;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Transactional(readOnly = true)
@RequiredArgsConstructor
public class TipoContratoCustomDaoImpl implements TipoContratoCustomDao {

    private final EntityManager entityManager;
    private final String REGEX = "[^\\p{ASCII}]";


    public String obtenerIdTipoContratoPorCoincidenciaNombre(String nombre){
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<TipoContratoEntity> cq = cb.createQuery(TipoContratoEntity.class);
        Root<TipoContratoEntity> tipoContratoEntity = cq.from(TipoContratoEntity.class);
        cq.select(tipoContratoEntity);
        List<TipoContratoEntity> tipoContrato = entityManager.createQuery(cq).getResultList();

        tipoContrato = tipoContrato.stream().map(s -> {
            s.setDescripcion(Normalizer.normalize(s.getDescripcion().toLowerCase(), Normalizer.Form.NFD));
            return s;
        }).map(s -> {
            s.setDescripcion(s.getDescripcion().replaceAll(REGEX, ""));
            return s;
        }).collect(Collectors.toList());

        nombre = Normalizer.normalize(nombre.toLowerCase(), Normalizer.Form.NFD);
        final String targetDescripcion = nombre.replaceAll(REGEX, "");

        SimilarityStrategy strategy = new JaroWinklerStrategy();
        StringSimilarityService service = new StringSimilarityServiceImpl(strategy);
        List<Double> scores = tipoContrato.stream().map(s -> service.score(s.getDescripcion(), targetDescripcion))
            .collect(Collectors.toList());

        Double maxSim = Collections.max(scores);

        return maxSim >= 1?tipoContrato.get(scores.indexOf(maxSim)).getId():null;
    }

}
