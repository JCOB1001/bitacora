package com.pragma.perfiles.tipo_contrato.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesTipoContrato;
import com.pragma.perfiles.tipo_contrato.aplicacion.manejador.ManejadorObtenerTipoContratoList;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("tipo-contrato")
@RequiredArgsConstructor
@Validated
public class EndPointObtenerTipoContratoList {

    private final ManejadorObtenerTipoContratoList manejadorObtenerTipoContratoList;

    @GetMapping
    public ObjetoRespuesta<Stream<TipoContrato>> ejecutarObtenerTipoContratoList(){
        ObjetoRespuesta<Stream<TipoContrato>> tipoContratoList = manejadorObtenerTipoContratoList.ejecutar();
        if(tipoContratoList.getDato() == null){
            throw new ExcepcionesTipoContrato("No existen registro de tipo de contrato");
        }
        return new ObjetoRespuesta<Stream<TipoContrato>>(HttpStatus.OK, tipoContratoList.getDato());
    }
}
