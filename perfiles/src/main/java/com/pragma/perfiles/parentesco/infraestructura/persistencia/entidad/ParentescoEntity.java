package com.pragma.perfiles.parentesco.infraestructura.persistencia.entidad;

import com.pragma.perfiles.informacion_familiar.infraestructura.persistencia.entidad.InformacionFamiliarEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="parentesco")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParentescoEntity implements IdEntidad<Long>{

    public interface Atributos extends IdEntidad.Atributos{
        String NOMBRE = "nombre";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;

    @OneToMany(mappedBy = "parentescoEntity")
    private List<InformacionFamiliarEntity> informacionFamiliarEntityStream;

}
