package com.pragma.perfiles.parentesco.infraestructura.endpoint;


import com.pragma.perfiles.comun.infraestructura.error.ParentescoNoEncontrado;
import com.pragma.perfiles.parentesco.aplicacion.comando.ActualizarParentescoComando;
import com.pragma.perfiles.parentesco.aplicacion.manejador.ManejadorActualizarParentesco;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("parentesco")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarParentesco {

    private final ManejadorActualizarParentesco manejadorActualizarParentesco;

    @PutMapping("/{idParentesco}")
    public ObjetoRespuesta<Parentesco> ejecutarActualizarParentesco(@NotNull @PathVariable Long idParentesco,
                                                                    @NotNull @RequestBody ActualizarParentescoComando actualizarParentescoComando){
        ObjetoRespuesta<Parentesco> parentescoObjetoRespuesta = manejadorActualizarParentesco.ejecutar(idParentesco, actualizarParentescoComando);
        if(parentescoObjetoRespuesta.getDato() == null){
            throw new ParentescoNoEncontrado("No existe este parentesco");
        }
        return new ObjetoRespuesta<Parentesco>(HttpStatus.OK, parentescoObjetoRespuesta.getDato());

    }

}
