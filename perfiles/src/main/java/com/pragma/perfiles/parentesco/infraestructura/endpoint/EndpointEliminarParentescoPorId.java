package com.pragma.perfiles.parentesco.infraestructura.endpoint;


import com.pragma.perfiles.comun.infraestructura.error.ParentescoNoEncontrado;
import com.pragma.perfiles.parentesco.aplicacion.manejador.ManejadorEliminarParentescoPorId;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("parentesco")
@RequiredArgsConstructor
@Validated
public class EndpointEliminarParentescoPorId {

    private final ManejadorEliminarParentescoPorId manejadorEliminarParentescoPorId;

    @DeleteMapping("/{idParentesco}")
    public ObjetoRespuesta<Parentesco> ejecutarEliminarParentescoPorId(@NotNull @PathVariable Long idParentesco){
        ObjetoRespuesta<Parentesco> parentescoObjetoRespuesta = manejadorEliminarParentescoPorId.ejecutar(idParentesco);
        if(parentescoObjetoRespuesta.getDato() == null)
            throw new ParentescoNoEncontrado("No existe este parentesco");
        return new ObjetoRespuesta<Parentesco>(HttpStatus.OK, parentescoObjetoRespuesta.getDato());
    }

}
