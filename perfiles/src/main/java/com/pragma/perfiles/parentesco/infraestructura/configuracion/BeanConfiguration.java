package com.pragma.perfiles.parentesco.infraestructura.configuracion;

import com.pragma.perfiles.parentesco.aplicacion.fabrica.FabricaParentesco;
import com.pragma.perfiles.parentesco.aplicacion.fabrica.impl.ParentescoMapperActualizarParentescoComando;
import com.pragma.perfiles.parentesco.aplicacion.fabrica.impl.ParentescoMapperGuardarParentescoComando;
import com.pragma.perfiles.parentesco.aplicacion.manejador.*;
import com.pragma.perfiles.parentesco.dominio.repositorio.ParentescoRepositorio;
import com.pragma.perfiles.parentesco.dominio.useCase.ParentescoCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public ManejadorActualizarParentesco manejadorActualizarParentesco(ParentescoCrudUseCase parentescoCrudUseCase, FabricaParentesco fabricaParentesco){
        return new ManejadorActualizarParentesco(parentescoCrudUseCase, fabricaParentesco);
    }

    @Bean
    public ManejadorEliminarParentescoPorId manejadorEliminarParentescoPorId(ParentescoCrudUseCase parentescoCrudUseCase){
        return new ManejadorEliminarParentescoPorId(parentescoCrudUseCase);
    }

    @Bean
    public ManejadorGuardarParentesco manejadorGuardarParentesco(ParentescoCrudUseCase parentescoCrudUseCase, FabricaParentesco fabricaParentesco){
        return new ManejadorGuardarParentesco(parentescoCrudUseCase, fabricaParentesco);
    }

    @Bean
    public ManejadorObtenerParentescoPorId manejadorObtenerParentescoPorId(ParentescoCrudUseCase parentescoCrudUseCase){
        return new ManejadorObtenerParentescoPorId(parentescoCrudUseCase);
    }

    @Bean
    public ManejadorObtenerParentescoList manejadorObtenerParentescoList(ParentescoCrudUseCase parentescoCrudUseCase){
        return new ManejadorObtenerParentescoList(parentescoCrudUseCase);
    }

    @Bean
    public ParentescoCrudUseCase parentescoCrudUseCase(ParentescoRepositorio parentescoRepositorio){
        return new ParentescoCrudUseCase(parentescoRepositorio);
    }

    @Bean
    public FabricaParentesco fabricaParentesco(ParentescoMapperGuardarParentescoComando parentescoMapperGuardarParentescoComando,
                                               ParentescoMapperActualizarParentescoComando parentescoMapperActualizarParentescoComando){
        return new FabricaParentesco(parentescoMapperGuardarParentescoComando, parentescoMapperActualizarParentescoComando);
    }




}
