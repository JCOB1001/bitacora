package com.pragma.perfiles.parentesco.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Parentesco extends Modelo<Long>{

 private String nombre;

 public Parentesco(){
  super(null);
 }

 public Parentesco(Long id, String nombre){
  super(id);
  this.nombre=nombre;
 }

}
