package com.pragma.perfiles.parentesco.aplicacion.manejador;

import com.pragma.perfiles.parentesco.aplicacion.comando.ActualizarParentescoComando;
import com.pragma.perfiles.parentesco.aplicacion.fabrica.FabricaParentesco;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.perfiles.parentesco.dominio.useCase.ParentescoCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorActualizarParentesco {

    private final ParentescoCrudUseCase parentescoCrudUseCase;
    private final FabricaParentesco fabricaParentesco;

    public ObjetoRespuesta<Parentesco> ejecutar(Long idParentesco, ActualizarParentescoComando actualizarParentescoComando){
        Parentesco parentesco = parentescoCrudUseCase.obtenerPorId(idParentesco);
        if(parentesco != null){
            Parentesco parentescoNuevo = fabricaParentesco.actualizarParentesco(actualizarParentescoComando);
            parentescoNuevo.setId(parentesco.getId());
            parentescoNuevo.setNombre(parentescoNuevo.getNombre());
            parentescoCrudUseCase.actualizar(parentescoNuevo);
            parentesco = parentescoNuevo;
        }
        return new ObjetoRespuesta<Parentesco>(parentesco);
    }


}
