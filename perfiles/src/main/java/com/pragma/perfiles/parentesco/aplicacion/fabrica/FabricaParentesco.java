package com.pragma.perfiles.parentesco.aplicacion.fabrica;

import com.pragma.perfiles.parentesco.aplicacion.comando.ActualizarParentescoComando;
import com.pragma.perfiles.parentesco.aplicacion.comando.GuardarParentescoComando;
import com.pragma.perfiles.parentesco.aplicacion.fabrica.impl.ParentescoMapperActualizarParentescoComando;
import com.pragma.perfiles.parentesco.aplicacion.fabrica.impl.ParentescoMapperGuardarParentescoComando;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaParentesco {

    private final ParentescoMapperGuardarParentescoComando parentescoMapperGuardarParentescoComando;
    private final ParentescoMapperActualizarParentescoComando parentescoMapperActualizarParentescoComando;

    public Parentesco guardarParentesco(GuardarParentescoComando guardarParentescoComando){
        return parentescoMapperGuardarParentescoComando.rightToLeft(guardarParentescoComando);
    }

    public Parentesco actualizarParentesco(ActualizarParentescoComando actualizarParentescoComando){
        return parentescoMapperActualizarParentescoComando.rightToLeft(actualizarParentescoComando);
    }



}
