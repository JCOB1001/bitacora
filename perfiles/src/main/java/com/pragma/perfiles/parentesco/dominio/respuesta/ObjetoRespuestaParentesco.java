package com.pragma.perfiles.parentesco.dominio.respuesta;

import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaParentesco extends ObjetoRespuesta {


    public ObjetoRespuestaParentesco(Stream<Parentesco> dato) {
        super(dato);
    }


}
