package com.pragma.perfiles.parentesco.dominio.useCase;

import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.perfiles.parentesco.dominio.repositorio.ParentescoRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;


public class ParentescoCrudUseCase extends CrudUseCaseCommon<ParentescoRepositorio, Parentesco, Long> {

    private ParentescoRepositorio parentescoRepositorio;

    public ParentescoCrudUseCase(ParentescoRepositorio ParentescoRepositorio){
        super(ParentescoRepositorio);
    }

    public Parentesco findByNombre(String nombre){
        return parentescoRepositorio.findByNombre(nombre);
    }


}
