package com.pragma.perfiles.parentesco.infraestructura.persistencia.dao;

import com.pragma.perfiles.parentesco.infraestructura.persistencia.entidad.ParentescoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParentescoDao extends JpaRepository<ParentescoEntity, Long> {

    public ParentescoEntity findByNombre(String nombre);
}
