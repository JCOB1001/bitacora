package com.pragma.perfiles.parentesco.aplicacion.manejador;

import com.pragma.perfiles.parentesco.aplicacion.comando.GuardarParentescoComando;
import com.pragma.perfiles.parentesco.aplicacion.fabrica.FabricaParentesco;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.perfiles.parentesco.dominio.useCase.ParentescoCrudUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorGuardarParentesco {

    private final ParentescoCrudUseCase parentescoCrudUseCase;
    private final FabricaParentesco fabricaParentesco;

    public void ejecutar(GuardarParentescoComando guardarParentescoComando){
            Parentesco parentesco = fabricaParentesco.guardarParentesco(guardarParentescoComando);
            parentescoCrudUseCase.guardar(parentesco);
    }

}
