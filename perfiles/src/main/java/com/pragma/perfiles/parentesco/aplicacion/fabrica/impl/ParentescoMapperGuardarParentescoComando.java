package com.pragma.perfiles.parentesco.aplicacion.fabrica.impl;

import com.pragma.perfiles.parentesco.aplicacion.comando.GuardarParentescoComando;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ParentescoMapperGuardarParentescoComando extends ConvertidorBase<Parentesco, GuardarParentescoComando> {
}
