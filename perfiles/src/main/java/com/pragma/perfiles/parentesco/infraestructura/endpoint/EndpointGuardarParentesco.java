package com.pragma.perfiles.parentesco.infraestructura.endpoint;

import com.pragma.perfiles.parentesco.aplicacion.comando.GuardarParentescoComando;
import com.pragma.perfiles.parentesco.aplicacion.manejador.ManejadorGuardarParentesco;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("parentesco")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarParentesco {

    private final ManejadorGuardarParentesco manejadorGuardarParentesco;

    @PostMapping
    public RespuestaBase ejecutarGuardarParentesco(@NotNull @RequestBody GuardarParentescoComando guardarParentescoComando){
        manejadorGuardarParentesco.ejecutar(guardarParentescoComando);
        return RespuestaBase.ok("Guardado exitoso");
    }

}
