package com.pragma.perfiles.parentesco.dominio.repositorio;

import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface ParentescoRepositorio extends CrudRepositorioBase<Long, Parentesco> {

    public Parentesco findByNombre(String nombre);

}
