package com.pragma.perfiles.parentesco.aplicacion.manejador;

import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.perfiles.parentesco.dominio.useCase.ParentescoCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerParentescoList {

    private final ParentescoCrudUseCase parentescoCrudUseCase;

    public ObjetoRespuesta<Stream<Parentesco>> ejecutar(){
        Stream<Parentesco> parentescos = parentescoCrudUseCase.obtenerTodos();
        return new ObjetoRespuesta<Stream<Parentesco>>(parentescos);
    }
}
