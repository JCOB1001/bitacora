package com.pragma.perfiles.parentesco.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActualizarParentescoComando {

    String nombre;

}
