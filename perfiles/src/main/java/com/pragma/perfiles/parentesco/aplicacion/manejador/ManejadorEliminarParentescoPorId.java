package com.pragma.perfiles.parentesco.aplicacion.manejador;

import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.perfiles.parentesco.dominio.useCase.ParentescoCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarParentescoPorId {

    private final ParentescoCrudUseCase parentescoCrudUseCase;

    public ObjetoRespuesta<Parentesco> ejecutar(Long idParentesco){
        Parentesco parentesco = parentescoCrudUseCase.obtenerPorId(idParentesco);
        if(parentesco != null){
            parentescoCrudUseCase.eliminarPorId(idParentesco);
        }
        return new ObjetoRespuesta<Parentesco>(parentesco);
    }

}
