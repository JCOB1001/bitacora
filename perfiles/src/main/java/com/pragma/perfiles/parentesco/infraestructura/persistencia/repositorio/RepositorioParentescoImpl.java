package com.pragma.perfiles.parentesco.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.perfiles.parentesco.dominio.repositorio.ParentescoRepositorio;
import com.pragma.perfiles.parentesco.infraestructura.persistencia.builder.ParentescoMapperParentescoEntity;
import com.pragma.perfiles.parentesco.infraestructura.persistencia.dao.ParentescoDao;
import com.pragma.perfiles.parentesco.infraestructura.persistencia.entidad.ParentescoEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class RepositorioParentescoImpl extends CrudRepositorioBaseImpl<Long, Parentesco, ParentescoEntity> implements ParentescoRepositorio {

    private final ParentescoDao parentescoDao;
    private final ParentescoMapperParentescoEntity parentescoMapperParentescoEntity;

    @Override
    protected ParentescoDao obtenerRepositorio() {
        return parentescoDao;
    }

    @Override
    public ParentescoMapperParentescoEntity obtenerConversionBase() {
        return parentescoMapperParentescoEntity;
    }

    @Override
    public Parentesco findByNombre(String nombre) {
        return parentescoMapperParentescoEntity.rightToLeft(parentescoDao.findByNombre(nombre));
    }
}
