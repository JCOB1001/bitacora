package com.pragma.perfiles.parentesco.infraestructura.persistencia.builder;


import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.perfiles.parentesco.infraestructura.persistencia.entidad.ParentescoEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ParentescoMapperParentescoEntity extends ConvertidorBase<Parentesco, ParentescoEntity> {
}
