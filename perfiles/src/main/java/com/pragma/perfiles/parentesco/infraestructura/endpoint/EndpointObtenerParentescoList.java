package com.pragma.perfiles.parentesco.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ParentescoNoEncontrado;
import com.pragma.perfiles.parentesco.aplicacion.manejador.ManejadorObtenerParentescoList;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("parentesco")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerParentescoList {

    private final ManejadorObtenerParentescoList manejadorObtenerParentescoList;

    @GetMapping
    public ObjetoRespuesta<Stream<Parentesco>> ejecutarObtenerParentescoList(){
        ObjetoRespuesta<Stream<Parentesco>> parentescoObjetoRespuesta = manejadorObtenerParentescoList.ejecutar();
        if(parentescoObjetoRespuesta.getDato() == null)
            throw new ParentescoNoEncontrado("No existen registros de parentescos");
        return new ObjetoRespuesta<Stream<Parentesco>>(HttpStatus.OK, parentescoObjetoRespuesta.getDato());
    }


}
