package com.pragma.perfiles.comun.infraestructura.error;

public class NivelNoValido extends RuntimeException{

    public NivelNoValido(String mensaje){
        super(mensaje);
    }

}
