package com.pragma.perfiles.comun.infraestructura.error;

public class ExperienciaLaboralNoEncontrada extends RuntimeException{

    public ExperienciaLaboralNoEncontrada(String mensaje){
        super(mensaje);
    }

}
