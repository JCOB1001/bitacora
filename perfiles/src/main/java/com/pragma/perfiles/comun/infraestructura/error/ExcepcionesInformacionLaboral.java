package com.pragma.perfiles.comun.infraestructura.error;

public class ExcepcionesInformacionLaboral extends RuntimeException{

    public ExcepcionesInformacionLaboral(String mensaje){
        super(mensaje);
    }

}
