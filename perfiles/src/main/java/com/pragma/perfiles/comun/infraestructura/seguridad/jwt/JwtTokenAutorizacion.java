package com.pragma.perfiles.comun.infraestructura.seguridad.jwt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.perfiles.comun.infraestructura.seguridad.configuracion.JwtPropiedades;
import com.pragma.perfiles.perfil.infraestructura.configuracion.GrupoAnticiposExternoPropiedades;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class JwtTokenAutorizacion extends OncePerRequestFilter {

    private final JwtPropiedades jwtPropiedades;

    private final GrupoAnticiposExternoPropiedades grupoAnticiposExternoPropiedades;

    UsuarioContextoComando usuarioContexto;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String authorization = request.getHeader("Authorization");
        if (authorization == null || authorization.isEmpty()) {
            throw new IllegalStateException("Para seguir con la peticion debe existir el HEADER");
        }

        try {
            procesarTokenAutorizacion(authorization);
        }
        catch (Exception e) {
            //throw new IllegalStateException("token invalido");
            String identification = request.getHeader("identification");
            if (identification == null || identification.isEmpty()) {
                throw new IllegalStateException("Para seguir con la peticion debe existir el HEADER");
            }
            try {
                procesarTokenAutorizacion(identification);
            }
            catch (Exception errorInterno) {
                System.out.println(errorInterno.getMessage());
            }
        }

        filterChain.doFilter(request, response);
    }

    private void procesarTokenAutorizacion(String authorization) throws JsonProcessingException {
        Base64 base64url = new Base64(true);

        String[] payloadCodificado = authorization.split("\\.");
        String payloadDecodificado = new String(base64url.decode(payloadCodificado[1]));

        ObjectMapper mapper = new ObjectMapper();

        UsuarioComando detallesUsuario = mapper.readerFor(UsuarioComando.class).readValue(payloadDecodificado);
        if (detallesUsuario.getTipoUsuario() != null){
            if (detallesUsuario.getTipoUsuario().get(0).equals(grupoAnticiposExternoPropiedades.getNombreGroupPool())){
                usuarioContexto = new UsuarioContextoComando(detallesUsuario.getEmail(),detallesUsuario.getEmail());
            } else {
                usuarioContexto =new UsuarioContextoComando(
                        detallesUsuario.getIdentidad().get(0).usuarioId,
                        detallesUsuario.getEmail());
            }
        } else {
            throw new IllegalStateException("Error, el usuario no esta afiliado a ningún grupo");
        }
        Set<SimpleGrantedAuthority> simpleGrantedAuthoritySet = detallesUsuario.getTipoUsuario()
                .stream()
                .map(m -> new SimpleGrantedAuthority(m))
                .collect(Collectors.toSet());

        Authentication authentication = new UsernamePasswordAuthenticationToken(
                usuarioContexto,
                null,
                simpleGrantedAuthoritySet
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
