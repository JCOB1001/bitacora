package com.pragma.perfiles.comun.infraestructura.error;

public class ContratoHistoricoNoEncontrado extends RuntimeException{

    public ContratoHistoricoNoEncontrado(String mensaje) {
        super(mensaje);
    }
}
