package com.pragma.perfiles.comun.infraestructura.error;

public class UsuarioConocimientoNoValido extends RuntimeException{

    public UsuarioConocimientoNoValido(String mensaje){
        super(mensaje);
    }
}
