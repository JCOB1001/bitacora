package com.pragma.perfiles.comun.infraestructura.error;

public class FechaInicialMenorAFechaFinal extends RuntimeException {

    public FechaInicialMenorAFechaFinal(String mensaje) {
        super(mensaje);
    }

}
