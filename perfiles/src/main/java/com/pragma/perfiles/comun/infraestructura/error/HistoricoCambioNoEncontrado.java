package com.pragma.perfiles.comun.infraestructura.error;

public class HistoricoCambioNoEncontrado extends RuntimeException{

    public HistoricoCambioNoEncontrado(String mensaje) {
        super(mensaje);
    }
}
