package com.pragma.perfiles.comun.infraestructura.error;

public class EnumNoEncontrado extends RuntimeException {

    public EnumNoEncontrado(String mensaje) {
        super(mensaje);
    }

}
