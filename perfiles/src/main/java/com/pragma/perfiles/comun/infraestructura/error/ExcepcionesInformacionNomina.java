package com.pragma.perfiles.comun.infraestructura.error;

public class ExcepcionesInformacionNomina  extends RuntimeException{
    public ExcepcionesInformacionNomina(String mensaje) {
        super(mensaje);
    }
}
