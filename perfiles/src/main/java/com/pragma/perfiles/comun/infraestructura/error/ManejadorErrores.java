package com.pragma.perfiles.comun.infraestructura.error;

import com.pragma.perfiles.perfil.dominio.error.ListaImagenesIncompleta;
import com.pragma.perfiles.perfil.dominio.error.TipoImagenIncompatible;
import com.pragma.perfiles.pragmadex.dominio.error.UsuarioPorGoogleIdNoEncontrado;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
@Log4j2
public class ManejadorErrores extends ResponseEntityExceptionHandler {

    private static final int OCURRIO_UN_ERROR_FAVOR_CONTACTAR_AL_ADMINISTRADOR_CODE = HttpStatus.INTERNAL_SERVER_ERROR.value();
    private static final String OCURRIO_UN_ERROR_FAVOR_CONTACTAR_AL_ADMINISTRADOR = "Ocurrió un error favor contactar al administrador.";
    private static final ConcurrentHashMap<String, Integer> CODIGOS_ESTADO = new ConcurrentHashMap<>();

    public ManejadorErrores() {
        CODIGOS_ESTADO.put(
                EnumNoEncontrado.class.getSimpleName(), HttpStatus.NO_CONTENT.value()
        );
        CODIGOS_ESTADO.put(
                FechaInicialMenorAFechaFinal.class.getSimpleName(), HttpStatus.BAD_REQUEST.value()
        );
        CODIGOS_ESTADO.put(
                UsuarioNoEncontrado.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                TipoCambioNoEncontrado.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                HistoricoCambioNoEncontrado.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                ListaImagenesIncompleta.class.getSimpleName(), HttpStatus.BAD_REQUEST.value()
        );
        CODIGOS_ESTADO.put(
                TipoImagenIncompatible.class.getSimpleName(), HttpStatus.BAD_REQUEST.value()
        );
        CODIGOS_ESTADO.put(
                UsuarioRegistrado.class.getSimpleName(), HttpStatus.CONFLICT.value()
        );
        CODIGOS_ESTADO.put(
                CapacidadNoEncontrada.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                ParentescoNoEncontrado.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                ExcepcionesInformacionFamiliar.class.getSimpleName(), HttpStatus.BAD_REQUEST.value()
        );
        CODIGOS_ESTADO.put(
                ExperienciaLaboralNoEncontrada.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                EstudioNoEncontrado.class.getSimpleName(),HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                UsuarioConocimientoNoEncontrado.class.getSimpleName(),HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                OtrosEstudiosNoEncontrado.class.getSimpleName(),HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                ArchivoNoValido.class.getSimpleName(),HttpStatus.BAD_REQUEST.value()
        );
        CODIGOS_ESTADO.put(
                InteresesNoEncontrado.class.getSimpleName(),HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                InteresesNovalido.class.getSimpleName(),HttpStatus.BAD_REQUEST.value()
        );
        CODIGOS_ESTADO.put(
                UsuarioConocimientoNoValido.class.getSimpleName(),HttpStatus.BAD_REQUEST.value()
        );
        CODIGOS_ESTADO.put(
                IdiomaNoEncontrado.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                NivelNoValido.class.getSimpleName(), HttpStatus.BAD_REQUEST.value()
        );
        CODIGOS_ESTADO.put(
                IdiomaExistente.class.getSimpleName(), HttpStatus.BAD_REQUEST.value()
        );
        CODIGOS_ESTADO.put(
                UsuarioIdiomasNoEncontrado.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                UsuarioPorGoogleIdNoEncontrado.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                DiaCelebracionNoEncontrado.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                DiaNoCelebrableNoEncontrado.class.getSimpleName(), HttpStatus.NOT_FOUND.value()
        );
        CODIGOS_ESTADO.put(
                DiaNoCelebrableYaExiste.class.getSimpleName(), HttpStatus.NOT_MODIFIED.value()
        );
        CODIGOS_ESTADO.put(
                CloudFrontServiceException.class.getSimpleName(), HttpStatus.NOT_MODIFIED.value()
        );
        CODIGOS_ESTADO.put(
                UsuarioNoVerificadoEnEscalaSalarial.class.getSimpleName(), HttpStatus.FORBIDDEN.value()
        );
        CODIGOS_ESTADO.put(
            CognitoIllegalStateException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value()
        );

    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Error> handleAllExceptions(Exception exception) {
        ResponseEntity<Error> resultado;

        String excepcionNombre = exception.getClass().getSimpleName();
        String mensaje = exception.getMessage();
        Integer codigo = CODIGOS_ESTADO.get(excepcionNombre);

        log.error(exception.getMessage(), exception);

        if (codigo != null) {
            Error error = new Error(excepcionNombre, mensaje, codigo);
            resultado = new ResponseEntity<>(error, HttpStatus.valueOf(codigo));
        } else {
            Error error = new Error(excepcionNombre,
                    OCURRIO_UN_ERROR_FAVOR_CONTACTAR_AL_ADMINISTRADOR,
                    OCURRIO_UN_ERROR_FAVOR_CONTACTAR_AL_ADMINISTRADOR_CODE);
            resultado = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resultado;
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseEntity<Object> resultado;

        String excepcionNombre = ex.getClass().getSimpleName();
        String mensaje = ex.getMessage();
        Integer codigo = HttpStatus.BAD_REQUEST.value();

        log.error(ex.getMessage(), ex);

        Error error = new Error(excepcionNombre, mensaje, codigo);
        resultado = new ResponseEntity<>(error, HttpStatus.valueOf(codigo));
        return resultado;
    }
}
