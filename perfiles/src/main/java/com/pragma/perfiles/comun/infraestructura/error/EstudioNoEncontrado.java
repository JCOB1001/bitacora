package com.pragma.perfiles.comun.infraestructura.error;

public class EstudioNoEncontrado extends RuntimeException{

    public  EstudioNoEncontrado(String mensaje){
        super(mensaje);
    }

}
