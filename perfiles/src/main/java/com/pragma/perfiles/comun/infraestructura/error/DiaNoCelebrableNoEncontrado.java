package com.pragma.perfiles.comun.infraestructura.error;

public class DiaNoCelebrableNoEncontrado extends RuntimeException {
    public DiaNoCelebrableNoEncontrado(String mensaje) {
        super(mensaje);
    }
}
