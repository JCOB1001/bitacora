package com.pragma.perfiles.comun.infraestructura.seguridad.dao;


import com.pragma.perfiles.comun.infraestructura.seguridad.entity.UsuarioEntity;

import java.util.Optional;


public interface UsuarioDao {

    Optional<UsuarioEntity> findByUsername(String username);

}
