package com.pragma.perfiles.comun.infraestructura.error;

public class UsuarioConocimientoNoEncontrado extends RuntimeException{

    public UsuarioConocimientoNoEncontrado(String mensaje){
        super(mensaje);
    }

}
