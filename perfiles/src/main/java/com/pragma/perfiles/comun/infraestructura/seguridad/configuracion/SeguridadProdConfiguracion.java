package com.pragma.perfiles.comun.infraestructura.seguridad.configuracion;

import com.pragma.perfiles.comun.infraestructura.seguridad.autenticacion.AplicacionLoginService;
import com.pragma.perfiles.comun.infraestructura.seguridad.jwt.JwtLoginFilter;
import com.pragma.perfiles.comun.infraestructura.seguridad.jwt.JwtTokenAutorizacion;
import com.pragma.perfiles.perfil.infraestructura.configuracion.GrupoAnticiposExternoPropiedades;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@Profile({"dev", "pre-prod", "prod"})
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
public class SeguridadProdConfiguracion extends WebSecurityConfigurerAdapter {

    private final AplicacionLoginService aplicacionLoginService;
    private final PasswordEncoder passwordEncoder;
    private final JwtPropiedades jwtPropiedades;
    private final GrupoAnticiposExternoPropiedades grupoAnticiposExternoPropiedades;

    private static final String[] AUTH_WHITELIST = {
            // -- Swagger UI v2
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**",
            "/swagger-ui/**"
            // other public endpoints of your API may be appended to this array
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                //.addFilter(new JwtLoginFilter(authenticationManager(), jwtPropiedades))
                .addFilterAfter(new JwtTokenAutorizacion(jwtPropiedades,grupoAnticiposExternoPropiedades),
                        JwtLoginFilter.class)
                .authorizeRequests()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .anyRequest()
                .authenticated();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(AUTH_WHITELIST);
    }

}
