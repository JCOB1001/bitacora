package com.pragma.perfiles.comun.infraestructura.error;

public class DiaNoCelebrableYaExiste extends RuntimeException {
    public DiaNoCelebrableYaExiste(String mensaje) {
        super(mensaje);
    }
}
