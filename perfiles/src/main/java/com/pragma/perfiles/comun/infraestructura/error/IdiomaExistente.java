package com.pragma.perfiles.comun.infraestructura.error;

public class IdiomaExistente extends RuntimeException{

    public IdiomaExistente(String mensaje){
        super(mensaje);
    }

}
