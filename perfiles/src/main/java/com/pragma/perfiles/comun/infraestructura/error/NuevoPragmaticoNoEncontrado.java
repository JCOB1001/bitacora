package com.pragma.perfiles.comun.infraestructura.error;

public class NuevoPragmaticoNoEncontrado  extends RuntimeException{
        public NuevoPragmaticoNoEncontrado(String mensaje){
            super(mensaje);
        }
}
