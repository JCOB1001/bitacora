package com.pragma.perfiles.comun.infraestructura.error;

public class CapacidadNoEncontrada extends RuntimeException{

    public CapacidadNoEncontrada(String mensaje) {
        super(mensaje);
    }
}
