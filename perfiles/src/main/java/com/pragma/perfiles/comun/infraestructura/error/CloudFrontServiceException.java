package com.pragma.perfiles.comun.infraestructura.error;

/**
 * Excepcion lanzada cuando el sdk de aws genera un error en el servicio de cloudFront
 */
public class CloudFrontServiceException extends RuntimeException{
    public CloudFrontServiceException(String message){
        super(message);
    }
}
