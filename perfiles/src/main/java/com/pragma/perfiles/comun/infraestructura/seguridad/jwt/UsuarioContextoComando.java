package com.pragma.perfiles.comun.infraestructura.seguridad.jwt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioContextoComando {

    String id;
    String correo;
}
