package com.pragma.perfiles.comun.infraestructura.error;

public class InteresesNovalido extends RuntimeException {

    public InteresesNovalido(String mensaje){
        super(mensaje);
    }
}
