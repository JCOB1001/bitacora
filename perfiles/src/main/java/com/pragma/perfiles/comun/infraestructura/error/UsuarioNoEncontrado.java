package com.pragma.perfiles.comun.infraestructura.error;

public class UsuarioNoEncontrado extends RuntimeException {

    public UsuarioNoEncontrado(String mensaje) {
        super(mensaje);
    }

}
