package com.pragma.perfiles.comun.infraestructura.error;

public class OtrosEstudiosNoEncontrado extends RuntimeException{

    public OtrosEstudiosNoEncontrado(String mensaje){
        super(mensaje);
    }
}
