package com.pragma.perfiles.comun.infraestructura.error;

public class UsuarioNoVerificadoEnEscalaSalarial extends RuntimeException{

    public UsuarioNoVerificadoEnEscalaSalarial(String mensaje){
        super(mensaje) ;
    }
}
