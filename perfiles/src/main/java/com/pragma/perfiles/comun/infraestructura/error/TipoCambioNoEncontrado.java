package com.pragma.perfiles.comun.infraestructura.error;

public class TipoCambioNoEncontrado extends RuntimeException {

    public TipoCambioNoEncontrado(String mensaje) {
        super(mensaje);
    }

}
