package com.pragma.perfiles.comun.infraestructura.error;

public class InteresesNoEncontrado extends RuntimeException {
    public InteresesNoEncontrado(String mensaje) {
        super(mensaje);
    }
}
