package com.pragma.perfiles.comun.infraestructura.error;

public class ArchivoNoValido extends RuntimeException{

    public ArchivoNoValido (String mensaje){
        super(mensaje);
    }
}
