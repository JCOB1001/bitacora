package com.pragma.perfiles.comun.infraestructura.error;

public class IdiomaNoEncontrado extends RuntimeException{
    public IdiomaNoEncontrado(String mensaje){
        super(mensaje);
    }
}
