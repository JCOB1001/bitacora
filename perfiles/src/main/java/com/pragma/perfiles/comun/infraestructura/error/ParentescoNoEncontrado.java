package com.pragma.perfiles.comun.infraestructura.error;

public class ParentescoNoEncontrado extends RuntimeException {

    public ParentescoNoEncontrado(String mensaje){
        super(mensaje);
    }

}
