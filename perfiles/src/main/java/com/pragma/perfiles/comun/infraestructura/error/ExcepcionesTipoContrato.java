package com.pragma.perfiles.comun.infraestructura.error;

public class ExcepcionesTipoContrato extends RuntimeException {
    public ExcepcionesTipoContrato(String mensaje) {
        super(mensaje);
    }
}
