package com.pragma.perfiles.comun.infraestructura.error;

public class SolicitudIncorrecta extends RuntimeException{

    public SolicitudIncorrecta(String mensaje){
        super(mensaje);
    }
}
