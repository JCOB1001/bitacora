package com.pragma.perfiles.comun.infraestructura.error;

public class UsuarioIdiomasNoEncontrado extends RuntimeException {

    public UsuarioIdiomasNoEncontrado(String mensaje) {
        super(mensaje);
    }

}
