package com.pragma.perfiles.comun.infraestructura.error;

public class UsuarioRegistrado extends RuntimeException {
    public UsuarioRegistrado(String mensaje) {
        super(mensaje);
    }
}

