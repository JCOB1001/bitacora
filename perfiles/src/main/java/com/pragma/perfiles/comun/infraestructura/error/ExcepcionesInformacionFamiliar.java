package com.pragma.perfiles.comun.infraestructura.error;

public class ExcepcionesInformacionFamiliar extends RuntimeException{

    public ExcepcionesInformacionFamiliar(String mensaje){
        super(mensaje);
    }

}
