package com.pragma.perfiles.comun.infraestructura.error;

public class DiaCelebracionNoEncontrado extends RuntimeException{
    public DiaCelebracionNoEncontrado(String mensaje) {
        super(mensaje);
    }
}
