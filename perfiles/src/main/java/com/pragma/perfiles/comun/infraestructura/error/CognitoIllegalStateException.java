package com.pragma.perfiles.comun.infraestructura.error;

public class CognitoIllegalStateException extends RuntimeException {
    public CognitoIllegalStateException(String message) {
        super(message);
    }
}
