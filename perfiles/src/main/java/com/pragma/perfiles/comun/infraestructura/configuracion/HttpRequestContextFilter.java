package com.pragma.perfiles.comun.infraestructura.configuracion;

import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class HttpRequestContextFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequestWrapper wrapper = new HttpServletRequestWrapper((HttpServletRequest) request);

        String authorization = wrapper.getHeader("Authorization");
        String identificacion = wrapper.getHeader("identification");
        String idioma = wrapper.getHeader("idioma");

        try {
            if(idioma != null && !idioma.isEmpty()) {
                HttpRequestContextHolder.setIdioma(idioma);
            }else {
                HttpRequestContextHolder.setIdioma("2");
            }

            if (
                    authorization != null && !authorization.isEmpty() &&
                            identificacion != null && !identificacion.isEmpty()
            ) {
                HttpRequestContextHolder.setAutorizacion(authorization);
                HttpRequestContextHolder.setIdentificacion(identificacion);
            } else {
                HttpRequestContextHolder.setAutorizacion(null);
                HttpRequestContextHolder.setIdentificacion(null);
            }
            chain.doFilter(request, response);
        } catch (Exception e) {
            ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

    @Override
    public void destroy() {
    }
}

