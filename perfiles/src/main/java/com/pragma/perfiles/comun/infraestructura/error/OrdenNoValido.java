package com.pragma.perfiles.comun.infraestructura.error;

public class OrdenNoValido extends RuntimeException {
    public OrdenNoValido(String mensaje) {
        super(mensaje);
    }
}

