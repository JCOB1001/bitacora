package com.pragma.perfiles.comun.infraestructura.configuracion.util;

public class HttpRequestContextHolder {

    private HttpRequestContextHolder() {
        super();
    }

    private static final ThreadLocal<String> AUTORIZACION_HOLDER = new ThreadLocal<>();
    private static final ThreadLocal<String> IDENTIFICACION_HOLDER = new ThreadLocal<>();
    private static final ThreadLocal<String> IDIOMA_HOLDER = new ThreadLocal<>();

    public static void setAutorizacion(String autorizacion) {
        AUTORIZACION_HOLDER.set(autorizacion);
    }

    public static String getAutorizacion() {

        return AUTORIZACION_HOLDER.get();
    }

    public static void clearAutorizacion() {

        AUTORIZACION_HOLDER.remove();
    }

    public static void setIdentificacion(String identificacion) {
        IDENTIFICACION_HOLDER.set(identificacion);
    }

    public static String getIdentificacion() {

        return IDENTIFICACION_HOLDER.get();
    }

    public static void clearIdentificacion() {

        IDENTIFICACION_HOLDER.remove();
    }

    public static void setIdioma(String idioma) {
        IDIOMA_HOLDER.set(idioma);
    }

    public static String getIdioma() {

        return IDIOMA_HOLDER.get();
    }

    public static void clearIdioma() {

        IDIOMA_HOLDER.remove();
    }
}

