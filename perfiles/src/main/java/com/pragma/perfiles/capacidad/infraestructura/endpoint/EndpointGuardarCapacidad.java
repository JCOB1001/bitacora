package com.pragma.perfiles.capacidad.infraestructura.endpoint;

import com.pragma.perfiles.capacidad.aplicacion.comando.GuardarCapacidadComando;
import com.pragma.perfiles.capacidad.aplicacion.manejadores.ManejadorGuardarCapacidad;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RequestMapping("capacidad")
@Validated
@RestController
@RequiredArgsConstructor
public class EndpointGuardarCapacidad {

    private final ManejadorGuardarCapacidad manejadorGuardarCapacidad;

    @PostMapping
    public ObjetoRespuesta<Capacidad> guardarCapacidad(@NotNull @RequestBody GuardarCapacidadComando guardarCapacidadComando){
        Capacidad capacidad = manejadorGuardarCapacidad.ejecutar(guardarCapacidadComando);
        return new ObjetoRespuesta<Capacidad>(HttpStatus.OK, capacidad);
    }


}
