package com.pragma.perfiles.capacidad.infraestructura.persistencia.entidad;

import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="capacidad")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CapacidadEntity implements IdEntidad<Long>{

    public interface Atributos extends IdEntidad.Atributos{
        String NOMBRE = "nombre";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;

}
