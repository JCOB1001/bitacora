package com.pragma.perfiles.capacidad.aplicacion.manejadores;

import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.capacidad.dominio.useCase.CapacidadCrudUseCase;
import com.pragma.perfiles.comun.infraestructura.error.CapacidadNoEncontrada;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarCapacidad {

    private final CapacidadCrudUseCase capacidadCrudUseCase;

    public Capacidad eliminarCapacidad(Long idCapacidad){
        Capacidad capacidad = capacidadCrudUseCase.obtenerPorId(idCapacidad);
        if(capacidad == null){
            throw new CapacidadNoEncontrada("Esta capacidad no se encuentra registrada en la base de datos");
        }
        capacidadCrudUseCase.eliminarPorId(idCapacidad);
        return capacidad;
    }

}
