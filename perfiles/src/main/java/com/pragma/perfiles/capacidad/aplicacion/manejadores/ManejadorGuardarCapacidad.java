package com.pragma.perfiles.capacidad.aplicacion.manejadores;

import com.pragma.perfiles.capacidad.aplicacion.comando.GuardarCapacidadComando;
import com.pragma.perfiles.capacidad.aplicacion.fabrica.FabricaCapacidad;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.capacidad.dominio.useCase.CapacidadCrudUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorGuardarCapacidad {

    private final FabricaCapacidad fabricaCapacidad;
    private final CapacidadCrudUseCase capacidadCrudUseCase;

    public Capacidad ejecutar(GuardarCapacidadComando guardarCapacidadComando){
        Capacidad capacidad = fabricaCapacidad.guardarCapacidad(guardarCapacidadComando);
        return capacidadCrudUseCase.guardar(capacidad);
    }

}
