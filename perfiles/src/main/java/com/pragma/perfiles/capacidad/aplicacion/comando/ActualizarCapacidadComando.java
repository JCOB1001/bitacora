package com.pragma.perfiles.capacidad.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActualizarCapacidadComando {

    Long id;
    String nombre;
}
