package com.pragma.perfiles.capacidad.infraestructura.endpoint;

import com.pragma.perfiles.capacidad.aplicacion.manejadores.ManejadorEliminarCapacidad;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RequestMapping("capacidad")
@Validated
@RestController
@RequiredArgsConstructor
public class EndpointEliminarCapacidad {

    private final ManejadorEliminarCapacidad manejadorEliminarCapacidad;

    @DeleteMapping("/{idCapacidad}")
    public ObjetoRespuesta<Capacidad> eliminarCapacidad(@NotNull @PathVariable Long idCapacidad){
        Capacidad capacidad = manejadorEliminarCapacidad.eliminarCapacidad(idCapacidad);
        return new ObjetoRespuesta<>(HttpStatus.OK, capacidad);
    }
}
