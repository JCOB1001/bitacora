package com.pragma.perfiles.capacidad.infraestructura.persistencia.dao;

import com.pragma.perfiles.capacidad.infraestructura.persistencia.entidad.CapacidadEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CapacidadDao extends JpaRepository<CapacidadEntity, Long> {
}
