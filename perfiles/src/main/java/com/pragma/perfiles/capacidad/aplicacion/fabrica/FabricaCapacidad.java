package com.pragma.perfiles.capacidad.aplicacion.fabrica;

import com.pragma.perfiles.capacidad.aplicacion.comando.ActualizarCapacidadComando;
import com.pragma.perfiles.capacidad.aplicacion.comando.GuardarCapacidadComando;
import com.pragma.perfiles.capacidad.aplicacion.fabrica.impl.CapacidadMapperActualizarCapacidadComando;
import com.pragma.perfiles.capacidad.aplicacion.fabrica.impl.CapacidadMapperGuardarCapacidadComando;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaCapacidad {

    private final CapacidadMapperGuardarCapacidadComando capacidadMapperGuardarCapacidadComando;
    private final CapacidadMapperActualizarCapacidadComando capacidadMapperActualizarCapacidadComando;

    public Capacidad guardarCapacidad(GuardarCapacidadComando guardarCapacidadComando){
        return capacidadMapperGuardarCapacidadComando.rightToLeft(guardarCapacidadComando);
    }

    public Capacidad actualizarCapacidad(ActualizarCapacidadComando actualizarCapacidadComando){
        return capacidadMapperActualizarCapacidadComando.rightToLeft(actualizarCapacidadComando);
    }

}
