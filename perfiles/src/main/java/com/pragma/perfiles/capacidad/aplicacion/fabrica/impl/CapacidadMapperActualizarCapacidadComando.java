package com.pragma.perfiles.capacidad.aplicacion.fabrica.impl;

import com.pragma.perfiles.capacidad.aplicacion.comando.ActualizarCapacidadComando;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CapacidadMapperActualizarCapacidadComando extends ConvertidorBase<Capacidad, ActualizarCapacidadComando> {
}
