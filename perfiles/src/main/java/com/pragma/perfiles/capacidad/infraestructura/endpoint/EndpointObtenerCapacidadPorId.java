package com.pragma.perfiles.capacidad.infraestructura.endpoint;


import com.pragma.perfiles.capacidad.aplicacion.manejadores.ManejadorObtenerCapacidadPorId;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.capacidad.dominio.useCase.CapacidadCrudUseCase;
import com.pragma.perfiles.comun.infraestructura.error.CapacidadNoEncontrada;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("capacidad")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerCapacidadPorId {

    private final ManejadorObtenerCapacidadPorId manejadorObtenerCapacidadPorId;

    @GetMapping("/{idCapacidad}")
    public ObjetoRespuesta<Capacidad> ajecutarObtenerCapacidadPorId(@NotNull @PathVariable Long idCapacidad){
        ObjetoRespuesta<Capacidad> capacidadObjetoRespuesta =manejadorObtenerCapacidadPorId.ejecutar(idCapacidad);
        return new ObjetoRespuesta<Capacidad>(HttpStatus.OK, capacidadObjetoRespuesta.getDato());
    }
}
