package com.pragma.perfiles.capacidad.dominio.repositorio;

import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface CapacidadRepositorio extends CrudRepositorioBase<Long, Capacidad> {
}
