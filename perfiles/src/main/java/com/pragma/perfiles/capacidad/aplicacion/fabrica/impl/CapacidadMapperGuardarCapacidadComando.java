package com.pragma.perfiles.capacidad.aplicacion.fabrica.impl;

import com.pragma.perfiles.capacidad.aplicacion.comando.GuardarCapacidadComando;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")

public interface CapacidadMapperGuardarCapacidadComando extends ConvertidorBase<Capacidad, GuardarCapacidadComando> {
}
