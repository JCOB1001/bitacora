package com.pragma.perfiles.capacidad.infraestructura.configuracion;

import com.pragma.perfiles.capacidad.aplicacion.fabrica.FabricaCapacidad;
import com.pragma.perfiles.capacidad.aplicacion.fabrica.impl.CapacidadMapperActualizarCapacidadComando;
import com.pragma.perfiles.capacidad.aplicacion.fabrica.impl.CapacidadMapperGuardarCapacidadComando;
import com.pragma.perfiles.capacidad.aplicacion.manejadores.ManejadorActualizarCapacidad;
import com.pragma.perfiles.capacidad.aplicacion.manejadores.ManejadorEliminarCapacidad;
import com.pragma.perfiles.capacidad.aplicacion.manejadores.ManejadorGuardarCapacidad;
import com.pragma.perfiles.capacidad.aplicacion.manejadores.ManejadorObtenerCapacidadList;
import com.pragma.perfiles.capacidad.aplicacion.manejadores.ManejadorObtenerCapacidadPorId;
import com.pragma.perfiles.capacidad.dominio.repositorio.CapacidadRepositorio;
import com.pragma.perfiles.capacidad.dominio.useCase.CapacidadCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CapacidadBeanConfiguracion {

    @Bean
    public ManejadorActualizarCapacidad manejadorActualizarCapacidad(CapacidadCrudUseCase capacidadCrudUseCase,
                                                                     FabricaCapacidad fabricaCapacidad){
        return new ManejadorActualizarCapacidad(fabricaCapacidad, capacidadCrudUseCase);
    }

    @Bean
    public ManejadorObtenerCapacidadPorId manejadorObtenerCapacidadPorId(CapacidadCrudUseCase capacidadCrudUseCase){
        return new ManejadorObtenerCapacidadPorId(capacidadCrudUseCase);
    }

    @Bean
    public ManejadorGuardarCapacidad manejadorGuardarCapacidad(CapacidadCrudUseCase capacidadCrudUseCase,
                                                               FabricaCapacidad fabricaCapacidad){
        return new ManejadorGuardarCapacidad(fabricaCapacidad, capacidadCrudUseCase);
    }

    @Bean
    public ManejadorEliminarCapacidad manejadorEliminarCapacidad(CapacidadCrudUseCase capacidadCrudUseCase){
        return new ManejadorEliminarCapacidad(capacidadCrudUseCase);
    }


    @Bean
    public CapacidadCrudUseCase capacidadCrudUseCase(CapacidadRepositorio capacidadRepositorio){
        return new CapacidadCrudUseCase(capacidadRepositorio);
    }

    @Bean
    public ManejadorObtenerCapacidadList manejadorObtenerCapacidadList(CapacidadCrudUseCase capacidadCrudUseCase){
        return new ManejadorObtenerCapacidadList(capacidadCrudUseCase);
    }


    @Bean
    public FabricaCapacidad fabricaCapacidad(CapacidadMapperActualizarCapacidadComando capacidadMapperActualizarCapacidadComando,
                                                     CapacidadMapperGuardarCapacidadComando capacidadMapperGuardarCapacidadComando){
        return new FabricaCapacidad(capacidadMapperGuardarCapacidadComando, capacidadMapperActualizarCapacidadComando);
    }




}
