package com.pragma.perfiles.capacidad.dominio.useCase;

import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.capacidad.dominio.repositorio.CapacidadRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

public class CapacidadCrudUseCase extends CrudUseCaseCommon<CapacidadRepositorio, Capacidad, Long> {

    private CapacidadRepositorio capacidadRepositorio;

    public CapacidadCrudUseCase(CapacidadRepositorio capacidadRepositorio){
        super(capacidadRepositorio);
    }
}
