package com.pragma.perfiles.capacidad.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Capacidad extends Modelo<Long> {

    private String nombre;

    public Capacidad(){
        super(null);
    }

    public Capacidad(Long id, String nombre){
        super(id);
        this.nombre = nombre;

    }


}
