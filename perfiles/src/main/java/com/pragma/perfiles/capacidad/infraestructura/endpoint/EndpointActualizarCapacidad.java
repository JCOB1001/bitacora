package com.pragma.perfiles.capacidad.infraestructura.endpoint;

import com.pragma.perfiles.capacidad.aplicacion.comando.ActualizarCapacidadComando;
import com.pragma.perfiles.capacidad.aplicacion.manejadores.ManejadorActualizarCapacidad;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RequestMapping("capacidad")
@Validated
@RestController
@RequiredArgsConstructor
public class EndpointActualizarCapacidad {

    private final ManejadorActualizarCapacidad manejadorActualizarCapacidad;

    @PutMapping
    public ObjetoRespuesta<Capacidad> actualizarCapacidad(@NotNull @RequestBody ActualizarCapacidadComando actualizarCapacidadComando){
        Capacidad capacidad = manejadorActualizarCapacidad.ejecutar(actualizarCapacidadComando);
        return new ObjetoRespuesta<Capacidad>(HttpStatus.OK, capacidad);
    }


}
