package com.pragma.perfiles.capacidad.infraestructura.endpoint;

import com.pragma.perfiles.capacidad.aplicacion.manejadores.ManejadorObtenerCapacidadList;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RequestMapping("capacidad")
@Validated
@RestController
@RequiredArgsConstructor
public class EndPointObtenerCapacidadList {

    private final ManejadorObtenerCapacidadList manejadorObtenerCapacidadList;

    @GetMapping
    public ObjetoRespuesta<Stream<Capacidad>> getCapacidades(){
        return new ObjetoRespuesta<Stream<Capacidad>>(HttpStatus.OK, manejadorObtenerCapacidadList.ejecutar().getDato());
    }

}
