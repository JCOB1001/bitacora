package com.pragma.perfiles.capacidad.infraestructura.persistencia.builder;

import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.capacidad.infraestructura.persistencia.entidad.CapacidadEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CapacidadMapperCapacidadEntity extends ConvertidorBase<Capacidad, CapacidadEntity> {
}
