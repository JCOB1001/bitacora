package com.pragma.perfiles.capacidad.aplicacion.manejadores;

import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.capacidad.dominio.useCase.CapacidadCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerCapacidadList {

    private final CapacidadCrudUseCase capacidadCrudUseCase;

    public ObjetoRespuesta<Stream<Capacidad>> ejecutar(){
        Stream<Capacidad> capacidades = capacidadCrudUseCase.obtenerTodos();
        return new ObjetoRespuesta<Stream<Capacidad>>(capacidades);
    }

}
