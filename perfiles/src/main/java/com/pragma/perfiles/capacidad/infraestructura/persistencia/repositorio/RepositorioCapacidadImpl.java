package com.pragma.perfiles.capacidad.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.capacidad.dominio.repositorio.CapacidadRepositorio;
import com.pragma.perfiles.capacidad.infraestructura.persistencia.builder.CapacidadMapperCapacidadEntity;
import com.pragma.perfiles.capacidad.infraestructura.persistencia.dao.CapacidadDao;
import com.pragma.perfiles.capacidad.infraestructura.persistencia.entidad.CapacidadEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class RepositorioCapacidadImpl extends CrudRepositorioBaseImpl<Long, Capacidad, CapacidadEntity> implements CapacidadRepositorio {

    private final CapacidadDao capacidadDao;
    private final CapacidadMapperCapacidadEntity capacidadaMapperCapacidadEntity;

    @Override
    protected CapacidadDao obtenerRepositorio() {
        return capacidadDao;
    }

    @Override
    public CapacidadMapperCapacidadEntity obtenerConversionBase() {
        return capacidadaMapperCapacidadEntity;
    }

}
