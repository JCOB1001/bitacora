package com.pragma.perfiles.capacidad.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuardarCapacidadComando {

    String nombre;

}
