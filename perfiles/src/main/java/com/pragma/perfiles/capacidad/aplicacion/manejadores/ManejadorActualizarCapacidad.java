package com.pragma.perfiles.capacidad.aplicacion.manejadores;

import com.pragma.perfiles.capacidad.aplicacion.comando.ActualizarCapacidadComando;
import com.pragma.perfiles.capacidad.aplicacion.fabrica.FabricaCapacidad;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.capacidad.dominio.useCase.CapacidadCrudUseCase;
import com.pragma.perfiles.comun.infraestructura.error.CapacidadNoEncontrada;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorActualizarCapacidad {

    private final FabricaCapacidad fabricaCapacidad;
    private final CapacidadCrudUseCase capacidadCrudUseCase;

    public Capacidad ejecutar(ActualizarCapacidadComando actualizarCapacidadComando){
        Capacidad capacidad = capacidadCrudUseCase.obtenerPorId(actualizarCapacidadComando.getId());
        if(capacidad == null){
            throw new CapacidadNoEncontrada("Esta capacidad no existe en la Base de Datos");
        }
        Capacidad capacidadNueva = fabricaCapacidad.actualizarCapacidad(actualizarCapacidadComando);
        capacidadCrudUseCase.actualizar(capacidadNueva);
        return capacidadNueva;
    }


}
