package com.pragma.perfiles.capacidad.aplicacion.manejadores;

import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import com.pragma.perfiles.capacidad.dominio.useCase.CapacidadCrudUseCase;
import com.pragma.perfiles.comun.infraestructura.error.CapacidadNoEncontrada;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerCapacidadPorId {

    private final CapacidadCrudUseCase capacidadCrudUseCase;

    public ObjetoRespuesta<Capacidad> ejecutar(Long idCapacidad){
        Capacidad capacidad = capacidadCrudUseCase.obtenerPorId(idCapacidad);
        if (capacidad == null){
            throw new CapacidadNoEncontrada("No Existe esta capacidad");
        }
        return  new ObjetoRespuesta<Capacidad>(capacidad);
    }

}
