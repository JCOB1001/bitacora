package com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuardarExperienciaLaboralPragmaComando {

    private String idUsuario;
    private String proyecto;
    private String rol;
    private LocalDate fechaDeIngreso;
    private LocalDate fechaDeFinalizacion;
    private long tiempoDeExperiencia;
    private String logros;
    private String herramientasUtilizadas;
    private String descripcion;
}
