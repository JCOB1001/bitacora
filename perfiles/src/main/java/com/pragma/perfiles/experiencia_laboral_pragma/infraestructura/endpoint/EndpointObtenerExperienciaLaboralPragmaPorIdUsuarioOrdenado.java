package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador.ManejadorObtenerExperienciaLaboralPragmaOrdenada;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequiredArgsConstructor
@RequestMapping("info-laboral-pragma")
@Validated
public class EndpointObtenerExperienciaLaboralPragmaPorIdUsuarioOrdenado {

    private final ManejadorObtenerExperienciaLaboralPragmaOrdenada manejadorObtenerExperienciaLaboralPragmaOrdenada;

    @GetMapping("/{usuarioId}/{ordenFecha}")
    public ObjetoRespuesta<Stream<ExperienciaLaboralPragma>> experienciaLaboralPragmaStream(@NotNull @PathVariable String usuarioId,
                                                                                           @PathVariable @NotNull String ordenFecha){
        return manejadorObtenerExperienciaLaboralPragmaOrdenada.buscarInformacionOrdenada(usuarioId,ordenFecha);
    }
}
