package com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragmaDetalle;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.repositorio.ExperienciaLaboralPragmaDetalleRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

public class ExperienciaLaboralPragmaDetalleCrudUseCase extends CrudUseCaseCommon<ExperienciaLaboralPragmaDetalleRepositorio, ExperienciaLaboralPragmaDetalle, String> {
    private final ExperienciaLaboralPragmaDetalleRepositorio experienciaLaboralPragmaDetalleRepositorio;

    public ExperienciaLaboralPragmaDetalleCrudUseCase(ExperienciaLaboralPragmaDetalleRepositorio experienciaLaboralPragmaDetalleRepositorio){
        super(experienciaLaboralPragmaDetalleRepositorio);
        this.experienciaLaboralPragmaDetalleRepositorio=experienciaLaboralPragmaDetalleRepositorio;
    }

    public ExperienciaLaboralPragmaDetalle findByExperienciaLaboralPragmaIdAndIdiomaId(String experienciaLaboralPragmaDetalleId, String idiomaId) {
        return this.experienciaLaboralPragmaDetalleRepositorio.findByExperienciaLaboralPragmaIdAndIdiomaId(experienciaLaboralPragmaDetalleId,idiomaId);
    }
}
