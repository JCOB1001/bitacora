package com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerExperienciaLaboralPragmaOrdenada {

    private final ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Stream<ExperienciaLaboralPragma>> buscarInformacionOrdenada(String usuarioId, String ordenFecha){
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(usuarioId);
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }
        Stream<ExperienciaLaboralPragma> ordenarInformacion = experienciaLaboralPragmaCrudUseCase.encontrarOrdenado(usuario.getId(),ordenFecha);

        return new ObjetoRespuesta<>(HttpStatus.OK, ordenarInformacion);
    }
}
