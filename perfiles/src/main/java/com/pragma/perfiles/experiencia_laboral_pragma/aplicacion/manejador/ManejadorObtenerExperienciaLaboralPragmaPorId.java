package com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ExperienciaLaboralNoEncontrada;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerExperienciaLaboralPragmaPorId {

    private final ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase;

    public ObjetoRespuesta<ExperienciaLaboralPragma> ejecutar(String idExperienciaLaboralPragma){
        ExperienciaLaboralPragma experienciaLaboralPragma = experienciaLaboralPragmaCrudUseCase.obtenerPorId(idExperienciaLaboralPragma);

        if(experienciaLaboralPragma == null){
            throw new ExperienciaLaboralNoEncontrada("La experiencia laboral no se encuentra registrada");
        }

        return new ObjetoRespuesta<>(experienciaLaboralPragma);
    }

}
