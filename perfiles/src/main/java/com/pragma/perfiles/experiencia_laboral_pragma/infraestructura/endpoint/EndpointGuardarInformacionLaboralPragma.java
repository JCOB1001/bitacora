package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.comando.GuardarExperienciaLaboralPragmaComando;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador.ManejadorGuardarInformacionLaboralPragma;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("info-laboral-pragma")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarInformacionLaboralPragma {

    private final ManejadorGuardarInformacionLaboralPragma manejadorGuardarInformacionLaboralPragma;

    @PostMapping
    public RespuestaBase ejecutarGuardarInformacionLaboralPragma(@NotNull @RequestBody GuardarExperienciaLaboralPragmaComando guardarExperienciaLaboralPragmaComando){
        manejadorGuardarInformacionLaboralPragma.ejecutar(guardarExperienciaLaboralPragmaComando);
        return RespuestaBase.ok("Guardado Exitosamente");
    }

}
