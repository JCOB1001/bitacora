package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador.ManejadorObtenerExperienciaLaboralFiltro;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador.ManejadorObtenerExperienciaLaboralPragmaPorIdUsuario;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.filtro.FiltroExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.respuesta.ObjetoRespuestaExperienciaLaboralPragma;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotNull;


@RestController
@RequestMapping("info-laboral-pragma-filtro")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerExperienciaLaboralFiltro {

    private final ManejadorObtenerExperienciaLaboralFiltro manejadorObtenerExperienciaLaboralFiltro;

    @GetMapping
    public ObjetoRespuestaExperienciaLaboralPragma ejecutarObtenerExperienciaLaboralPragmaPorIdUsuario(@NotNull  FiltroExperienciaLaboralPragma filtroExperienciaLaboralPragma){
        return  manejadorObtenerExperienciaLaboralFiltro.ejecutar(filtroExperienciaLaboralPragma);
    }


}
