package com.pragma.perfiles.experiencia_laboral_pragma.dominio.respuesta;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.Builder;
import lombok.Getter;

import java.util.List;


@Getter
public class ObjetoRespuestaExperienciaLaboralPragma extends ObjetoRespuesta {

    private long cantidadPaginas;
    private boolean ultimaPagina;

    public ObjetoRespuestaExperienciaLaboralPragma(List<ExperienciaLaboralPragma> dato, long cantidadPaginas, boolean ultimaPagina) {
        super(dato);
        this.cantidadPaginas = cantidadPaginas;
        this.ultimaPagina = ultimaPagina;
    }



}

