package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador.ManejadorObtenerExperienciaLaboralPragmaPorIdUsuario;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("info-laboral-pragma")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerExperienciaLaboralPragmaPorIdUsuario {

    private final ManejadorObtenerExperienciaLaboralPragmaPorIdUsuario manejadorObtenerExperienciaLaboralPragmaPorIdUsuario;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<Stream<ExperienciaLaboralPragma>> ejecutarObtenerExperienciaLaboralPragmaPorIdUsuario(@NotNull @PathVariable String idUsuario){
        return manejadorObtenerExperienciaLaboralPragmaPorIdUsuario.ejecutar(idUsuario);
    }


}
