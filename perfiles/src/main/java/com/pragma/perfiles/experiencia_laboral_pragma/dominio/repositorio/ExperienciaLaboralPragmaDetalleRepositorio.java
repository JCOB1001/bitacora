package com.pragma.perfiles.experiencia_laboral_pragma.dominio.repositorio;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragmaDetalle;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface ExperienciaLaboralPragmaDetalleRepositorio extends CrudRepositorioBase<String, ExperienciaLaboralPragmaDetalle>{

    ExperienciaLaboralPragmaDetalle findByExperienciaLaboralPragmaIdAndIdiomaId(String experienciaLaboralPragmaId, String idiomaId);
}
