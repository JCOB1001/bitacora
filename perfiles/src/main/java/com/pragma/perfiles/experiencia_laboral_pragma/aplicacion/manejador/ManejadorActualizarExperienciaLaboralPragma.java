package com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.comun.infraestructura.error.ExperienciaLaboralNoEncontrada;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.comando.ActualizarExperienciaLaboralPragmaComando;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.fabrica.FabricaExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragmaDetalle;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaDetalleCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorActualizarExperienciaLaboralPragma {
    private final ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase;
    private final FabricaExperienciaLaboralPragma fabricaExperienciaLaboralPragma;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ExperienciaLaboralPragmaDetalleCrudUseCase experienciaLaboralPragmaDetalleCrudUseCase;

    public void ejecutar(String idInfoLaboralPragma, ActualizarExperienciaLaboralPragmaComando actualizarExperienciaLaboralPragmaComando){
        String idIdioma = HttpRequestContextHolder.getIdioma();
        ExperienciaLaboralPragma experienciaLaboralPragma = experienciaLaboralPragmaCrudUseCase.obtenerPorId(idInfoLaboralPragma);
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(actualizarExperienciaLaboralPragmaComando.getIdUsuario());
        ExperienciaLaboralPragmaDetalle experienciaLaboralPragmaDetalle = experienciaLaboralPragmaDetalleCrudUseCase.findByExperienciaLaboralPragmaIdAndIdiomaId(idInfoLaboralPragma,idIdioma);

        if(usuario == null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+ actualizarExperienciaLaboralPragmaComando.getIdUsuario()+" no existe");
        }

        if(experienciaLaboralPragma == null){
            throw new ExperienciaLaboralNoEncontrada("La experiencia laboral interna con el id: "+ idInfoLaboralPragma +" no fue encontrada");
        }

        if(!experienciaLaboralPragma.getIdUsuario().equals(actualizarExperienciaLaboralPragmaComando.getIdUsuario())){
            throw new ExperienciaLaboralNoEncontrada("El id del usuario enviado no coincide con los registros de la base de dato ");
        }

        usuario.setFechaUltimaActualizacion(LocalDate.now());
        ExperienciaLaboralPragma experienciaLaboralPragmaNueva = fabricaExperienciaLaboralPragma.actualizarExperienciaLaboralPragma(actualizarExperienciaLaboralPragmaComando);
        experienciaLaboralPragmaNueva.setId(experienciaLaboralPragma.getId());

        experienciaLaboralPragmaCrudUseCase.actualizar(experienciaLaboralPragmaNueva);
        usuarioCrudUseCase.actualizar(usuario);

        if(experienciaLaboralPragmaDetalle==null){
            experienciaLaboralPragmaDetalle = new ExperienciaLaboralPragmaDetalle(null,experienciaLaboralPragmaNueva.getRol(),experienciaLaboralPragmaNueva.getLogros(),experienciaLaboralPragmaNueva.getHerramientasUtilizadas(),
                    idIdioma,experienciaLaboralPragmaNueva.getDescripcion(),experienciaLaboralPragmaNueva.getId(), experienciaLaboralPragmaNueva.getProyecto());
            experienciaLaboralPragmaDetalleCrudUseCase.guardar(experienciaLaboralPragmaDetalle);
        }else{
            experienciaLaboralPragmaDetalle.setProyecto(experienciaLaboralPragmaNueva.getProyecto());
            experienciaLaboralPragmaDetalle.setDescripcion(experienciaLaboralPragmaNueva.getDescripcion());
            experienciaLaboralPragmaDetalle.setHerramientasUtilizadas(experienciaLaboralPragmaNueva.getHerramientasUtilizadas());
            experienciaLaboralPragmaDetalle.setLogros(experienciaLaboralPragmaNueva.getLogros());
            experienciaLaboralPragmaDetalle.setRol(experienciaLaboralPragmaNueva.getRol());
            experienciaLaboralPragmaDetalleCrudUseCase.actualizar(experienciaLaboralPragmaDetalle);
        }
    }
}
