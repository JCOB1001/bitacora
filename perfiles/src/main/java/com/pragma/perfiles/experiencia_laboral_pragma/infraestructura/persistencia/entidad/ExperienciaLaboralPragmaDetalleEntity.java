package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.entidad;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "experiencia_laboral_pragma_detalle")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExperienciaLaboralPragmaDetalleEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos {
        String IDIOMA ="idioma";
        String ROL = "rol";
        String LOGROS = "logros";
        String HERRAMIENTAS_UTILIZADAS = "herramientasUtilizadas";
        String DESCRIPCION = "descripcion";
        String EXPERIENCIALABORALPRAGMA = "experienciaLaboralPragma";
        String PROYECTO = "proyecto";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    private String rol;
    private String logros;
    private String proyecto;
    private String herramientasUtilizadas;
    private String descripcion;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idiomas_id", foreignKey = @ForeignKey(name = "fk_experiencia_laboral_pragma_detalle_idiomas_idiomas_id"))
    private IdiomasEntity idioma;
    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "experiencia_laboral_pragma_id", foreignKey = @ForeignKey(name = "fk_experiencia_laboral_pragma_experiencia_laboral_pragma_id"))
    private ExperienciaLaboralPragmaEntity experienciaLaboralPragma;
}
