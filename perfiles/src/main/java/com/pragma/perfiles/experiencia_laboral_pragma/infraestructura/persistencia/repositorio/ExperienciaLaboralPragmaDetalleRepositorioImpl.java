package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragmaDetalle;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.repositorio.ExperienciaLaboralPragmaDetalleRepositorio;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.builder.ExperienciaPragmaDetalleMapperExperienciaPragmaDetalleEntity;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.dao.ExperienciaLaboralPragmaDetalleDao;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.entidad.ExperienciaLaboralPragmaDetalleEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ExperienciaLaboralPragmaDetalleRepositorioImpl extends CrudRepositorioBaseImpl<String, ExperienciaLaboralPragmaDetalle, ExperienciaLaboralPragmaDetalleEntity> implements ExperienciaLaboralPragmaDetalleRepositorio {

    private final ExperienciaLaboralPragmaDetalleDao experienciaLaboralPragmaDetalleDao;
    private final ExperienciaPragmaDetalleMapperExperienciaPragmaDetalleEntity experienciaPragmaDetalleMapperExperienciaPragmaDetalleEntity;

    @Override
    public ExperienciaLaboralPragmaDetalleDao obtenerRepositorio(){ return experienciaLaboralPragmaDetalleDao; }

    @Override
    protected ExperienciaPragmaDetalleMapperExperienciaPragmaDetalleEntity obtenerConversionBase(){
        return experienciaPragmaDetalleMapperExperienciaPragmaDetalleEntity;
    }

    public ExperienciaLaboralPragmaDetalle findByExperienciaLaboralPragmaIdAndIdiomaId(String experienciaLaboralPragmaId, String idiomaId){
        return obtenerConversionBase().rightToLeft(experienciaLaboralPragmaDetalleDao.findByExperienciaLaboralPragmaIdAndIdiomaId(experienciaLaboralPragmaId,idiomaId));
    }
}
