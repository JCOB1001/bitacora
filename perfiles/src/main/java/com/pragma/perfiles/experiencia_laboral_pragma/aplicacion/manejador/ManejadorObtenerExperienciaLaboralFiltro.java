package com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.filtro.FiltroExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.respuesta.ObjetoRespuestaExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerExperienciaLaboralFiltro {

    private final ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuestaExperienciaLaboralPragma ejecutar(FiltroExperienciaLaboralPragma filtroExperienciaLaboralPragma){

        ObjetoRespuestaExperienciaLaboralPragma consultaPragma;

        Usuario usuario = usuarioCrudUseCase.obtenerPorId(filtroExperienciaLaboralPragma.getIdUsuario());
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }
        if(filtroExperienciaLaboralPragma.getTotalElementos() == null || filtroExperienciaLaboralPragma.getTotalElementos() < 0){
            filtroExperienciaLaboralPragma.setTotalElementos(1);
        }
        if(filtroExperienciaLaboralPragma.getNumeroPagina() == null || filtroExperienciaLaboralPragma.getNumeroPagina() > 0){
            filtroExperienciaLaboralPragma.setNumeroPagina(0);
        }
            consultaPragma = experienciaLaboralPragmaCrudUseCase.buscarFiltrando(filtroExperienciaLaboralPragma);

        if(filtroExperienciaLaboralPragma.getTotalElementos() == 1) {
                filtroExperienciaLaboralPragma.setTotalElementos((int) consultaPragma.getCantidadPaginas());
        }

        consultaPragma = experienciaLaboralPragmaCrudUseCase.buscarFiltrando(filtroExperienciaLaboralPragma);



        return  consultaPragma;
    }

}
