package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.dao.impl;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.filtro.FiltroExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.respuesta.ObjetoRespuestaExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.builder.ExperienciaLaboralPragmaMapperExperienciaLaboralPragmaEntity;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.dao.ExperienciciaLaboralPragmaCustomDao;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.entidad.ExperienciaLaboralPragmaEntity;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ExperienciaLaboralPragmaDaoImpl implements ExperienciciaLaboralPragmaCustomDao {

    private final EntityManager entityManager;
    private final ExperienciaLaboralPragmaMapperExperienciaLaboralPragmaEntity experienciaLaboralPragmaMapper;

    @Override
    public ObjetoRespuestaExperienciaLaboralPragma buscarFiltrando(FiltroExperienciaLaboralPragma filtroExperienciaLaboralPragma) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        From alcanceFrom = criteriaQuery.from(ExperienciaLaboralPragmaEntity.class);
        List<Predicate> predicados = new ArrayList<>();

        if(!filtroExperienciaLaboralPragma.getIdUsuario().isEmpty()) {
            predicados
                    .add(
                            criteriaBuilder
                                    .equal(
                                            alcanceFrom
                                                    .get(
                                                            ExperienciaLaboralPragmaEntity.ATRIBUTOS.USUARIO)
                                                    .get(UsuarioEntity.Atributos.ID),
                                            filtroExperienciaLaboralPragma
                                                    .getIdUsuario()));
        }
        if (!predicados.isEmpty()) {
            criteriaQuery.where(predicados.toArray(new Predicate[predicados.size()]));
        }
        TypedQuery<Long> cantidadQuery = entityManager.createQuery(criteriaQuery.select(
                criteriaBuilder.count(alcanceFrom)
        ));
        long numeroPaginas = (long)Math.ceil((double) cantidadQuery.getSingleResult()/(double) filtroExperienciaLaboralPragma.getTotalElementos());

        aplicarOrden(criteriaQuery, alcanceFrom, criteriaBuilder, filtroExperienciaLaboralPragma);

        TypedQuery<ExperienciaLaboralPragmaEntity> resultQuery = entityManager.createQuery(
                criteriaQuery.select(alcanceFrom)
        );
        aplicarCantidad(resultQuery, filtroExperienciaLaboralPragma.getNumeroPagina(),filtroExperienciaLaboralPragma.getTotalElementos());
        List<ExperienciaLaboralPragmaEntity> experianciaLaboralRecorrer = resultQuery.getResultList();
        List<ExperienciaLaboralPragma> experienciaLaboralPragmaEntityLista = new ArrayList<>();
        for (ExperienciaLaboralPragmaEntity experienciaEntity : experianciaLaboralRecorrer) {
            experienciaLaboralPragmaEntityLista.add((experienciaLaboralPragmaMapper.rightToLeft(experienciaEntity)));
        }
        return new ObjetoRespuestaExperienciaLaboralPragma(experienciaLaboralPragmaEntityLista, numeroPaginas,filtroExperienciaLaboralPragma.getNumeroPagina() == numeroPaginas - 1);


    }

    @Override
    public Stream<ExperienciaLaboralPragma> buscarInformacionOrdenada(String idUsuario, String ordenFecha) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
        From alcanceFrom = criteriaQuery.from(ExperienciaLaboralPragmaEntity.class);
        List<Predicate> predicados = new ArrayList<>();
        if(!idUsuario.isEmpty()){
            predicados
                    .add(
                            criteriaBuilder
                                            .equal(
                                                    alcanceFrom.get(
                                                                    ExperienciaLaboralPragmaEntity.ATRIBUTOS.USUARIO).get(
                                                                            UsuarioEntity.Atributos.ID), idUsuario
                                                    ) );
        }
        if (!predicados.isEmpty()) {
            criteriaQuery.where(predicados.toArray(new Predicate[predicados.size()]));
        }
        TypedQuery<Long> cantidadQuery = entityManager.createQuery(criteriaQuery.select(
                criteriaBuilder.count(alcanceFrom)
        ));
        aplicarOrdenExperiencia(criteriaQuery, alcanceFrom, criteriaBuilder, idUsuario,ordenFecha);

        TypedQuery<ExperienciaLaboralPragmaEntity> resultQuery = entityManager.createQuery(
                criteriaQuery
                        .select(
                                alcanceFrom
                        )
        );
        List<ExperienciaLaboralPragmaEntity> experianciaLaboralRecorrer = resultQuery.getResultList();
        List<ExperienciaLaboralPragma> experienciaLaboralPragmaEntityLista = new ArrayList<>();
        for (ExperienciaLaboralPragmaEntity experienciaEntity : experianciaLaboralRecorrer) {
            experienciaLaboralPragmaEntityLista.add((experienciaLaboralPragmaMapper.rightToLeft(experienciaEntity)));
        }
        return experienciaLaboralPragmaEntityLista.stream();
    }

    private void aplicarOrdenExperiencia(CriteriaQuery criteriaQuery, From alcanceFrom,CriteriaBuilder criteriaBuilder,
                              String idUsuario, String ordenFecha){
        List<Order> orders = new ArrayList<>();
        if(!ordenFecha.isEmpty()){
            if(ordenFecha.toLowerCase().equalsIgnoreCase("ASC")){
                orders.add(criteriaBuilder.desc(alcanceFrom.get(ExperienciaLaboralPragmaEntity.ATRIBUTOS.FECHA_DE_INGRESO)));
            }
            else{
                orders.add(criteriaBuilder.asc(alcanceFrom.get(ExperienciaLaboralPragmaEntity.ATRIBUTOS.FECHA_DE_INGRESO)));
            }

        }
        if (!orders.isEmpty()) {
            criteriaQuery.orderBy(orders.toArray(new Order[orders.size()]));
        }

    }
    private void aplicarOrden(CriteriaQuery criteriaQuery, From alcanceFrom, CriteriaBuilder criteriaBuilder, FiltroExperienciaLaboralPragma filtroExperienciaLaboralPragma){
        List<Order> orders = new ArrayList<>();
        if(!filtroExperienciaLaboralPragma.getOrdenFecha().isEmpty()){
            if(filtroExperienciaLaboralPragma.getOrdenFecha().toLowerCase().equalsIgnoreCase("ASC")) {
                orders.add(criteriaBuilder.asc(alcanceFrom.get(ExperienciaLaboralPragmaEntity.ATRIBUTOS.FECHA_DE_INGRESO)));
            }
            else if(filtroExperienciaLaboralPragma.getOrdenFecha().toLowerCase().equalsIgnoreCase("DES")){
                orders.add(criteriaBuilder.desc(alcanceFrom.get(ExperienciaLaboralPragmaEntity.ATRIBUTOS.FECHA_DE_INGRESO)));
            }

        }
        if (!orders.isEmpty()) {
            criteriaQuery.orderBy(orders.toArray(new Order[orders.size()]));
        }
    }

    private void aplicarCantidad(TypedQuery<ExperienciaLaboralPragmaEntity> resultQuery, Integer numeroPagina, Integer totalElementos) {
        if (numeroPagina != null && totalElementos != null) {
            resultQuery.setFirstResult(numeroPagina * totalElementos);
            resultQuery.setMaxResults(totalElementos);
        }
    }
}
