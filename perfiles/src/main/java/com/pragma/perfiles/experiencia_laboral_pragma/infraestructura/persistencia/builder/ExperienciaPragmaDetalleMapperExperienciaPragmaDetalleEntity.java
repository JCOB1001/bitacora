package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.builder;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragmaDetalle;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.entidad.ExperienciaLaboralPragmaDetalleEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface ExperienciaPragmaDetalleMapperExperienciaPragmaDetalleEntity extends ConvertidorBase<ExperienciaLaboralPragmaDetalle, ExperienciaLaboralPragmaDetalleEntity> {

    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "idExperienciaLaboralPragma", target = "experienciaLaboralPragma.id"),
            @Mapping(source = "idIdioma", target = "idioma.id")
    })
    ExperienciaLaboralPragmaDetalleEntity leftToRight(ExperienciaLaboralPragmaDetalle experienciaLaboralPragmaDetalle);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "experienciaLaboralPragma.id", target = "idExperienciaLaboralPragma"),
            @Mapping(source = "idioma.id", target = "idIdioma")
    })
    ExperienciaLaboralPragmaDetalle rightToLeft(ExperienciaLaboralPragmaDetalleEntity experienciaLaboralPragmaDetalleEntity);
}
