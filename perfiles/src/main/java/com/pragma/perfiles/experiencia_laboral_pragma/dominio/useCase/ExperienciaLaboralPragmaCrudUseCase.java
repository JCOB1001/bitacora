package com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.filtro.FiltroExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.repositorio.ExperienciaLaboralPragmaRepositorio;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.respuesta.ObjetoRespuestaExperienciaLaboralPragma;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class ExperienciaLaboralPragmaCrudUseCase extends CrudUseCaseCommon<ExperienciaLaboralPragmaRepositorio, ExperienciaLaboralPragma, String> {

    private final ExperienciaLaboralPragmaRepositorio experienciaLaboralPragmaRepositorio;

    public ExperienciaLaboralPragmaCrudUseCase(ExperienciaLaboralPragmaRepositorio experienciaLaboralPragmaRepositorio){
        super(experienciaLaboralPragmaRepositorio);
        this.experienciaLaboralPragmaRepositorio = experienciaLaboralPragmaRepositorio;
    }

    public Stream<ExperienciaLaboralPragma> findByIdUsuarioId(String usuarioId){
        return experienciaLaboralPragmaRepositorio.findByUsuarioId(usuarioId);
    }

    public ObjetoRespuestaExperienciaLaboralPragma buscarFiltrando(FiltroExperienciaLaboralPragma filtroExperienciaLaboralPragma){
        return  experienciaLaboralPragmaRepositorio.buscarFiltrando(filtroExperienciaLaboralPragma);
    }

    public Stream<ExperienciaLaboralPragma> encontrarOrdenado(String usuarioId, String ordenFecha){
        return  experienciaLaboralPragmaRepositorio.encontrarFiltrando(usuarioId,ordenFecha);
    }
}
