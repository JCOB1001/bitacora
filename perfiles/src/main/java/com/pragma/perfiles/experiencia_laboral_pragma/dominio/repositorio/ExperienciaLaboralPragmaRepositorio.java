package com.pragma.perfiles.experiencia_laboral_pragma.dominio.repositorio;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.filtro.FiltroExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.respuesta.ObjetoRespuestaExperienciaLaboralPragma;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;

public interface ExperienciaLaboralPragmaRepositorio extends CrudRepositorioBase<String, ExperienciaLaboralPragma> {
    Stream<ExperienciaLaboralPragma> findByUsuarioId(String usuarioId);

    ObjetoRespuestaExperienciaLaboralPragma buscarFiltrando(FiltroExperienciaLaboralPragma filtroExperienciaLaboralPragma);

    Stream<ExperienciaLaboralPragma> encontrarFiltrando(String usuarioId, String ordenFecha);
}
