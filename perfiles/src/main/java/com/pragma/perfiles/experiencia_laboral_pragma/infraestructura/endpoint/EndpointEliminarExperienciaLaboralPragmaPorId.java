package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador.ManejadorEliminarExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(TextosComunes.RUTA_CONTROLADOR_EXP_LABORAL_PRAGMA)
@RequiredArgsConstructor
@Validated
public class EndpointEliminarExperienciaLaboralPragmaPorId {

    private final ManejadorEliminarExperienciaLaboralPragma manejadorEliminarExperienciaLaboralPragma;

    @DeleteMapping(TextosComunes.RUTA_VARIABLE_ID_EXPERIENCIA_LABORAL_PRAGMA)
    public RespuestaBase ejecutarEliminarExperienciaLaboralPragma(@NotNull @PathVariable String idExperienciaLaboralPragma){
        manejadorEliminarExperienciaLaboralPragma.ejecutar(idExperienciaLaboralPragma);
        return RespuestaBase.ok("Borrado Exitosamente");
    }

}