package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.configuracion;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.repositorio.ExperienciaLaboralPragmaDetalleRepositorio;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaDetalleCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationExperienciaLaboralPragmaDetalle {
    @Bean
    public ExperienciaLaboralPragmaDetalleCrudUseCase experienciaLaboralPragmaDetalleCrudUseCase(ExperienciaLaboralPragmaDetalleRepositorio experienciaLaboralPragmaDetalleRepositorio){
        return  new ExperienciaLaboralPragmaDetalleCrudUseCase(experienciaLaboralPragmaDetalleRepositorio);
    }
}
