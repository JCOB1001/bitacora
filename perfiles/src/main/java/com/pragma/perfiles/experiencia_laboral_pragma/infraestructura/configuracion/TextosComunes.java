package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.configuracion;

public class TextosComunes {

    public static final String RUTA_CONTROLADOR_EXP_LABORAL_PRAGMA ="info-laboral-pragma";
    public static final String RUTA_VARIABLE_ID_EXPERIENCIA_LABORAL_PRAGMA ="/{idExperienciaLaboralPragma}";
}
