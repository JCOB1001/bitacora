package com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.fabrica.impl;

import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.comando.ActualizarExperienciaLaboralPragmaComando;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ExperienciaLaboralPragmaMapperActualizarExperienciaLaboralPragmaComando extends ConvertidorBase<ExperienciaLaboralPragma, ActualizarExperienciaLaboralPragmaComando> {
}
