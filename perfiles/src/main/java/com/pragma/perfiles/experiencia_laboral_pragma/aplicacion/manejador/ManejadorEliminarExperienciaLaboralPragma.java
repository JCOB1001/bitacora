package com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.ExperienciaLaboralNoEncontrada;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorEliminarExperienciaLaboralPragma {

    private final ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public void ejecutar(String idExperienciaLaboralPragma){

        ExperienciaLaboralPragma experienciaLaboralPragma = experienciaLaboralPragmaCrudUseCase.obtenerPorId(idExperienciaLaboralPragma);
        if(experienciaLaboralPragma == null){
            throw new ExperienciaLaboralNoEncontrada("Esta experiencia laboral no existe");
        }

        Usuario usuario=usuarioCrudUseCase.obtenerPorId(experienciaLaboralPragma.getIdUsuario());
        if(usuario!=null){
            usuario.setFechaUltimaActualizacion(LocalDate.now());
            usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
            usuarioCrudUseCase.actualizar(usuario);
        }


        experienciaLaboralPragmaCrudUseCase.eliminarPorId(idExperienciaLaboralPragma);
    }

}
