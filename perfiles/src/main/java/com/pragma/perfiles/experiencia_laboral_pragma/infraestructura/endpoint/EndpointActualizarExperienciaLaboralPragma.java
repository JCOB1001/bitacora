package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.endpoint;

import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.comando.ActualizarExperienciaLaboralPragmaComando;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador.ManejadorActualizarExperienciaLaboralPragma;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("info-laboral-pragma")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarExperienciaLaboralPragma {

    private final ManejadorActualizarExperienciaLaboralPragma manejadorActualizarExperienciaLaboralPragma;

    @PutMapping("/{idInfoLaboralPragma}")
    public RespuestaBase ejecutarActualizarExperienciaLaboralPragma(@NotNull @PathVariable String idInfoLaboralPragma,
                                                                    @RequestBody ActualizarExperienciaLaboralPragmaComando actualizarExperienciaLaboralPragmaComando){
        manejadorActualizarExperienciaLaboralPragma.ejecutar(idInfoLaboralPragma, actualizarExperienciaLaboralPragmaComando);
        return RespuestaBase.ok("Actualizado Exitosamente");
    }
}
