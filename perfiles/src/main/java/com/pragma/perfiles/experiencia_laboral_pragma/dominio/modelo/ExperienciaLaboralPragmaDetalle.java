package com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ExperienciaLaboralPragmaDetalle extends Modelo<String> {

    private String idExperienciaLaboralPragma;
    private String descripcion;
    private String idIdioma;
    private String herramientasUtilizadas;
    private String logros;
    private String rol;
    private String proyecto;

    public ExperienciaLaboralPragmaDetalle(){super(null);}

    public ExperienciaLaboralPragmaDetalle(String id, String rol, String logros, String herramientasUtilizadas, String idIdioma, String descripcion, String idExperienciaLaboralPragma, String proyecto){
        super(id);
        this.rol = rol;
        this.logros = logros;
        this.herramientasUtilizadas = herramientasUtilizadas;
        this.idIdioma = idIdioma;
        this.idExperienciaLaboralPragma = idExperienciaLaboralPragma;
        this.descripcion = descripcion;
        this.proyecto = proyecto;
    }
}
