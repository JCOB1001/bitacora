package com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.comando.GuardarExperienciaLaboralPragmaComando;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.fabrica.FabricaExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragmaDetalle;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaDetalleCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorGuardarInformacionLaboralPragma {

    private final FabricaExperienciaLaboralPragma fabricaExperienciaLaboralPragma;
    private final ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;
    private final ExperienciaLaboralPragmaDetalleCrudUseCase experienciaLaboralPragmaDetalleCrudUseCase;

    public void ejecutar(GuardarExperienciaLaboralPragmaComando guardarExperienciaLaboralPragmaComando) {
        String idIdioma = HttpRequestContextHolder.getIdioma();
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(guardarExperienciaLaboralPragmaComando.getIdUsuario());

        if(usuario == null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+ guardarExperienciaLaboralPragmaComando.getIdUsuario()+" no existe");
        }
        ExperienciaLaboralPragma experienciaLaboralPragma = fabricaExperienciaLaboralPragma.guardarInformacionLaboralPragma(guardarExperienciaLaboralPragmaComando);
        ExperienciaLaboralPragma experienciaLaboralPragmaNuevo = experienciaLaboralPragmaCrudUseCase.guardar(experienciaLaboralPragma);
        ExperienciaLaboralPragmaDetalle experienciaLaboralPragmaDetalle = new ExperienciaLaboralPragmaDetalle(null,experienciaLaboralPragmaNuevo.getRol(),experienciaLaboralPragmaNuevo.getLogros()
                ,experienciaLaboralPragmaNuevo.getHerramientasUtilizadas(),idIdioma,experienciaLaboralPragmaNuevo.getDescripcion(),experienciaLaboralPragmaNuevo.getId(), experienciaLaboralPragmaNuevo.getProyecto());
        experienciaLaboralPragmaDetalleCrudUseCase.guardar(experienciaLaboralPragmaDetalle);

        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuario.setBarraProgreso(manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(usuario.getId()));
        usuarioCrudUseCase.actualizar(usuario);

    }
}
