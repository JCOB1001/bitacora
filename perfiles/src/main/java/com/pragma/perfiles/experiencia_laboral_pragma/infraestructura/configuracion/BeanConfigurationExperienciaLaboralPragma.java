package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.configuracion;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.fabrica.FabricaExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.fabrica.impl.ExperienciaLaboralPragmaMapperActualizarExperienciaLaboralPragmaComando;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.manejador.*;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.fabrica.impl.InformacionLaboralPragmaMapperGuardarInformacionLaboralPragmaComando;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.repositorio.ExperienciaLaboralPragmaRepositorio;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaCrudUseCase;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.useCase.ExperienciaLaboralPragmaDetalleCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationExperienciaLaboralPragma {

    @Bean
    public ManejadorActualizarExperienciaLaboralPragma manejadorActualizarExperienciaLaboralPragma(
            ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase,
            FabricaExperienciaLaboralPragma fabricaExperienciaLaboralPragma,
            UsuarioCrudUseCase usuarioCrudUseCase,
            ExperienciaLaboralPragmaDetalleCrudUseCase experienciaLaboralPragmaDetalleCrudUseCase){

        return new ManejadorActualizarExperienciaLaboralPragma(experienciaLaboralPragmaCrudUseCase,
                fabricaExperienciaLaboralPragma,
                usuarioCrudUseCase,
                experienciaLaboralPragmaDetalleCrudUseCase);
    }

    @Bean
    public ManejadorObtenerExperienciaLaboralPragmaPorId manejadorObtenerExperienciaLaboralPragmaPorId(
            ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase){

        return new ManejadorObtenerExperienciaLaboralPragmaPorId(experienciaLaboralPragmaCrudUseCase);
    }

    @Bean
    public ManejadorObtenerExperienciaLaboralPragmaPorIdUsuario manejadorObtenerExperienciaLaboralPragmaPorIdUsuario(
            ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase,
            UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerExperienciaLaboralPragmaPorIdUsuario(
                experienciaLaboralPragmaCrudUseCase,
                usuarioCrudUseCase
        );
    }

    @Bean
    public ManejadorEliminarExperienciaLaboralPragma manejadorEliminarExperienciaLaboralPragma(ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase,
                                                                                               ManejadorCalcularValorBarraProgreso calcularValorBarraProgreso,
                                                                                               UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorEliminarExperienciaLaboralPragma(experienciaLaboralPragmaCrudUseCase,calcularValorBarraProgreso,usuarioCrudUseCase);
    }
    @Bean
    public ManejadorObtenerExperienciaLaboralPragmaOrdenada manejadorObtenerExperienciaLaboralPragmaOrdenada(ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase,
                                                                                                             UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerExperienciaLaboralPragmaOrdenada(experienciaLaboralPragmaCrudUseCase,usuarioCrudUseCase);
    }
    @Bean
    public ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase(
            ExperienciaLaboralPragmaRepositorio experienciaLaboralPragmaRepositorio){
        return new ExperienciaLaboralPragmaCrudUseCase(experienciaLaboralPragmaRepositorio);
    }
    @Bean
    public ManejadorObtenerExperienciaLaboralFiltro manejadorObtenerExperienciaLaboralFiltro(ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase,
                                                                                             UsuarioCrudUseCase usuarioCrudUseCase){
      return new ManejadorObtenerExperienciaLaboralFiltro(experienciaLaboralPragmaCrudUseCase,usuarioCrudUseCase);
    }
    @Bean
    public FabricaExperienciaLaboralPragma fabricaInformacionLaboralPragma(
            ExperienciaLaboralPragmaMapperActualizarExperienciaLaboralPragmaComando experienciaLaboralPragmaMapperActualizarExperienciaLaboralPragmaComando,
            InformacionLaboralPragmaMapperGuardarInformacionLaboralPragmaComando informacionLaboralPragmaMapperGuardarInformacionLaboralPragmaComando){

        return new FabricaExperienciaLaboralPragma(experienciaLaboralPragmaMapperActualizarExperienciaLaboralPragmaComando,
                informacionLaboralPragmaMapperGuardarInformacionLaboralPragmaComando);

    }
    @Bean
    public ManejadorGuardarInformacionLaboralPragma manejadorGuardarInformacionLaboralPragma(FabricaExperienciaLaboralPragma fabricaExperienciaLaboralPragma,
                                                                                             ExperienciaLaboralPragmaCrudUseCase experienciaLaboralPragmaCrudUseCase,
                                                                                             UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                             ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso,
                                                                                             ExperienciaLaboralPragmaDetalleCrudUseCase experienciaLaboralPragmaDetalleCrudUseCase){

        return new ManejadorGuardarInformacionLaboralPragma(fabricaExperienciaLaboralPragma,experienciaLaboralPragmaCrudUseCase,
                usuarioCrudUseCase,manejadorCalcularValorBarraProgreso,
                experienciaLaboralPragmaDetalleCrudUseCase);
    }



}
