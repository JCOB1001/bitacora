package com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ExperienciaLaboralPragma extends Modelo<String> {

    private String proyecto;
    private String rol;
    private LocalDate fechaDeIngreso;
    private LocalDate fechaDeFinalizacion;
    private long tiempoDeExperiencia;
    private String logros;
    private String herramientasUtilizadas;
    private String idUsuario;
    private String descripcion;

    public ExperienciaLaboralPragma(){super(null);}

    public ExperienciaLaboralPragma(String id, String proyecto, String rol, LocalDate fechaDeIngreso, LocalDate fechaDeFinalizacion,
                                    long tiempoDeExperiencia, String logros, String herramientasUtilizadas, String idUsuario,String descripcion){
        super(id);
        this.proyecto = proyecto;
        this.rol = rol;
        this.fechaDeIngreso = fechaDeIngreso;
        this.fechaDeFinalizacion = fechaDeFinalizacion;
        this.tiempoDeExperiencia = tiempoDeExperiencia;
        this.logros = logros;
        this.herramientasUtilizadas = herramientasUtilizadas;
        this.idUsuario = idUsuario;
        this.descripcion = descripcion;
    }
}
