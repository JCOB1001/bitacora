package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.dao;

import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.entidad.ExperienciaLaboralPragmaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExperienciaLaboralPragmaDao extends JpaRepository<ExperienciaLaboralPragmaEntity, String>, ExperienciciaLaboralPragmaCustomDao{

    Iterable<ExperienciaLaboralPragmaEntity> findByUsuarioId(String usuarioId);


}
