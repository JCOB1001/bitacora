package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.dao;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.filtro.FiltroExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.respuesta.ObjetoRespuestaExperienciaLaboralPragma;

import java.util.stream.Stream;


public interface ExperienciciaLaboralPragmaCustomDao {

    ObjetoRespuestaExperienciaLaboralPragma buscarFiltrando(FiltroExperienciaLaboralPragma filtroExperienciaLaboralPragma);

    Stream<ExperienciaLaboralPragma> buscarInformacionOrdenada(String idUsuario, String ordenFecha);

}
