package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.experiencia_laboral_pragma.dominio.filtro.FiltroExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.repositorio.ExperienciaLaboralPragmaRepositorio;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.respuesta.ObjetoRespuestaExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.builder.ExperienciaLaboralPragmaMapperExperienciaLaboralPragmaEntity;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.dao.ExperienciaLaboralPragmaDao;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.entidad.ExperienciaLaboralPragmaEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class ExperienciaLaboralPragmaRepositorioImpl extends CrudRepositorioBaseImpl<String, ExperienciaLaboralPragma, ExperienciaLaboralPragmaEntity> implements ExperienciaLaboralPragmaRepositorio {

    @Autowired
    private final ExperienciaLaboralPragmaDao experienciaLaboralPragmaDao;
    @Autowired
    private final ExperienciaLaboralPragmaMapperExperienciaLaboralPragmaEntity experienciaLaboralPragmaMapperExperienciaLaboralPragmaEntity;

    @Override
    protected ExperienciaLaboralPragmaDao obtenerRepositorio() {
        return experienciaLaboralPragmaDao;
    }

    @Override
    protected ExperienciaLaboralPragmaMapperExperienciaLaboralPragmaEntity obtenerConversionBase() {
        return experienciaLaboralPragmaMapperExperienciaLaboralPragmaEntity;
    }

    @Override
    public Stream<ExperienciaLaboralPragma> findByUsuarioId(String usuarioId) {
        return experienciaLaboralPragmaMapperExperienciaLaboralPragmaEntity.rightToLeft(
                experienciaLaboralPragmaDao.findByUsuarioId(usuarioId)
        );
    }

    @Override
    public ObjetoRespuestaExperienciaLaboralPragma buscarFiltrando(FiltroExperienciaLaboralPragma filtroExperienciaLaboralPragma) {
        return experienciaLaboralPragmaDao.buscarFiltrando(filtroExperienciaLaboralPragma);
    }

    @Override
    public Stream<ExperienciaLaboralPragma> encontrarFiltrando(String usuarioId, String ordenFecha) {
        return (Stream<ExperienciaLaboralPragma>) experienciaLaboralPragmaDao.buscarInformacionOrdenada(usuarioId,ordenFecha);
    }

}
