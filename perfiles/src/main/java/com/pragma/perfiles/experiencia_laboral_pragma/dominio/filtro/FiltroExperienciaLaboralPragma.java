package com.pragma.perfiles.experiencia_laboral_pragma.dominio.filtro;

import com.pragma.vacaciones.common.filtro.FiltroBase;
import lombok.Getter;

@Getter
public class FiltroExperienciaLaboralPragma extends FiltroBase {

    private String idUsuario;
    private String ordenFecha;

    public FiltroExperienciaLaboralPragma(Integer numeroPagina, Integer totalElementos, String idUsuario, String ordenFecha){
        super(numeroPagina,totalElementos);
        this.idUsuario = idUsuario;
        this.ordenFecha = ordenFecha;
    }
}

