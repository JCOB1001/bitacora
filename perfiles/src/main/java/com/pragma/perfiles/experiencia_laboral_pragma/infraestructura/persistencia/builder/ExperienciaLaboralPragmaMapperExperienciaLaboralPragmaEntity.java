package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.builder;

import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad.ExperienciaLaboralExternaDetalleEntity;
import com.pragma.perfiles.experiencia_laboral_externa.infraestructura.persistencia.entidad.ExperienciaLaboralExternaEntity;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.entidad.ExperienciaLaboralPragmaDetalleEntity;
import com.pragma.perfiles.experiencia_laboral_pragma.infraestructura.persistencia.entidad.ExperienciaLaboralPragmaEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.*;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ExperienciaLaboralPragmaMapperExperienciaLaboralPragmaEntity extends ConvertidorBase<ExperienciaLaboralPragma, ExperienciaLaboralPragmaEntity> {
    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "idUsuario", target = "usuario.id")
    )
    ExperienciaLaboralPragmaEntity leftToRight(ExperienciaLaboralPragma experienciaLaboralPragma);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "usuario.id", target = "idUsuario")
    })
    ExperienciaLaboralPragma rightToLeft(ExperienciaLaboralPragmaEntity experienciaLaboralPragmaEntity);

    @AfterMapping
    default void afterRightToLeft(@MappingTarget ExperienciaLaboralPragma experienciaLaboralPragma, ExperienciaLaboralPragmaEntity experienciaLaboralPragmaEntity) {
        if(experienciaLaboralPragmaEntity.getExperienciaLaboralPragmaDetalle()!=null){
            List<ExperienciaLaboralPragmaDetalleEntity> experienciaLaboralPragmaDetalle = experienciaLaboralPragmaEntity.getExperienciaLaboralPragmaDetalle().stream().filter(item->
                    item.getIdioma().getId().equals(HttpRequestContextHolder.getIdioma())).collect(Collectors.toList());
            if(experienciaLaboralPragmaDetalle.isEmpty()) {
                experienciaLaboralPragmaDetalle.add(new ExperienciaLaboralPragmaDetalleEntity());
            }
                experienciaLaboralPragma.setDescripcion(experienciaLaboralPragmaDetalle.get(0).getDescripcion());
                experienciaLaboralPragma.setHerramientasUtilizadas(experienciaLaboralPragmaDetalle.get(0).getHerramientasUtilizadas());
                experienciaLaboralPragma.setLogros(experienciaLaboralPragmaDetalle.get(0).getLogros());
                experienciaLaboralPragma.setRol(experienciaLaboralPragmaDetalle.get(0).getRol());
                experienciaLaboralPragma.setProyecto(experienciaLaboralPragmaDetalle.get(0).getProyecto());
        }
    }

}
