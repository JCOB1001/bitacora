package com.pragma.perfiles.estado_retiro.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuardarEstadoRetirosComando {
    public String id_usuario;
    public int estado;
}
