package com.pragma.perfiles.estado_retiro.infraestructura.endpoint;

import com.pragma.perfiles.estado_retiro.aplicacion.comando.ActualizarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.manejador.ManejadorActualizarEstadoRetiros;
import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("estado-retiro")
@RequiredArgsConstructor
@Validated
public class EndpointActualizarEstadoRetiros {

    private final ManejadorActualizarEstadoRetiros manejadorActualizarEstadoRetiros;


    @PutMapping("actualizar")
    public ObjetoRespuesta<EstadoRetiro> ejecutarActualizarEstadoRetiros(@RequestParam(value="idEstadoRetiro") String idEstadoRetiro,
                                                         @NotNull @RequestBody ActualizarEstadoRetirosComando actualizarEstadoRetiroComando){

        return manejadorActualizarEstadoRetiros.ejecutar(idEstadoRetiro,actualizarEstadoRetiroComando);
    }


}
