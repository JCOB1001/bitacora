package com.pragma.perfiles.estado_retiro.infraestructura.persistencia.builder;

import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.perfiles.estado_retiro.infraestructura.persistencia.entidad.EstadoRetiroEntity;

import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface EstadoRetiroMapperEstadoRetiroEntity extends ConvertidorBase<EstadoRetiro, EstadoRetiroEntity> {

    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "id_usuario", target = "usuario.id")
    )
    EstadoRetiroEntity leftToRight(EstadoRetiro EstadoRetiro);

    @Named("rightToLeft")
    @Override
    @Mappings(
            @Mapping(source = "usuario.id", target = "id_usuario")
    )
    EstadoRetiro rightToLeft(EstadoRetiroEntity EstadoRetiroEntity);

}
