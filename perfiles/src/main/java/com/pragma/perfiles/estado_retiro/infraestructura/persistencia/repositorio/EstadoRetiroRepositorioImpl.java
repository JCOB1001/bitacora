package com.pragma.perfiles.estado_retiro.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.perfiles.estado_retiro.dominio.repositorio.EstadoRetiroRepositorio;
import com.pragma.perfiles.estado_retiro.infraestructura.persistencia.builder.EstadoRetiroMapperEstadoRetiroEntity;
import com.pragma.perfiles.estado_retiro.infraestructura.persistencia.dao.EstadoRetiroDao;
import com.pragma.perfiles.estado_retiro.infraestructura.persistencia.entidad.EstadoRetiroEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class EstadoRetiroRepositorioImpl extends CrudRepositorioBaseImpl<String, EstadoRetiro, EstadoRetiroEntity> implements EstadoRetiroRepositorio {

    private final EstadoRetiroDao estadoRetiroDao;

    private final EstadoRetiroMapperEstadoRetiroEntity estadoRetiroMapperEstadoRetiroEntity;

    @Override
    protected EstadoRetiroDao obtenerRepositorio() {
        return estadoRetiroDao;
    }

    @Override
    public EstadoRetiroMapperEstadoRetiroEntity obtenerConversionBase() {
        return estadoRetiroMapperEstadoRetiroEntity;
    }

    @Override
    public Stream<EstadoRetiro> findbyId_usuario(String identificacion) {
        System.out.println("intento encontrar usuario");
        return estadoRetiroMapperEstadoRetiroEntity.rightToLeft(estadoRetiroDao.findByUsuarioId(identificacion));
    }

}
