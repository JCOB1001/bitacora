package com.pragma.perfiles.estado_retiro.aplicacion.fabrica.impl;

import com.pragma.perfiles.estado_retiro.aplicacion.comando.ActualizarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EstadoRetiroMapperActualizarEstadoRetirosComando extends ConvertidorBase<EstadoRetiro, ActualizarEstadoRetirosComando> {
}
