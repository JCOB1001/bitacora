package com.pragma.perfiles.estado_retiro.infraestructura.endpoint;

import com.pragma.perfiles.estado_retiro.aplicacion.manejador.ManejadorObtenerEstadoRetiros;
import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("estado-retiro")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerEstadoRetiros {

    private final ManejadorObtenerEstadoRetiros manejadorObtenerEstadoRetiros;

    @GetMapping("/ping")
    public ObjetoRespuesta<String> ping() {
        return new ObjetoRespuesta<>(HttpStatus.OK, "pong");
    }

    @GetMapping
    public ObjetoRespuesta<Stream<EstadoRetiro>> ejecutarPasosUsuario(@NotNull @RequestParam("userId") String userId){
        return manejadorObtenerEstadoRetiros.ejecutar(userId);

    }
}
