package com.pragma.perfiles.estado_retiro.aplicacion.manejador;


import com.pragma.perfiles.estado_retiro.aplicacion.comando.ActualizarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.fabrica.FabricaEstadoRetiro;
import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.perfiles.estado_retiro.dominio.useCase.EstadoRetiroCrudUseCase;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;

import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@RequiredArgsConstructor
public class ManejadorActualizarEstadoRetiros {

    private final EstadoRetiroCrudUseCase estadoRetiroCrudUseCase;
    private final FabricaEstadoRetiro fabricaEstadoRetiro;

    public ObjetoRespuesta<EstadoRetiro> ejecutar(String idEstadoRetiro, ActualizarEstadoRetirosComando actualizarEstadoRetiroComando){

        EstadoRetiro EstadoRetiroNuevo = fabricaEstadoRetiro.actualizarEstadoRetiros(actualizarEstadoRetiroComando);
        EstadoRetiroNuevo.setId(idEstadoRetiro);
        estadoRetiroCrudUseCase.actualizar(EstadoRetiroNuevo);


        return new ObjetoRespuesta<EstadoRetiro>(HttpStatus.OK,EstadoRetiroNuevo);
    }

}
