package com.pragma.perfiles.estado_retiro.dominio.repositorio;

import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;

public interface EstadoRetiroRepositorio extends CrudRepositorioBase<String, EstadoRetiro> {

   Stream<EstadoRetiro> findbyId_usuario(String id_usuario);
}
