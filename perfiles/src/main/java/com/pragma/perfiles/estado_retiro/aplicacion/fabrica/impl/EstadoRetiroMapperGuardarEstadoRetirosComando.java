package com.pragma.perfiles.estado_retiro.aplicacion.fabrica.impl;

import com.pragma.perfiles.estado_retiro.aplicacion.comando.GuardarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EstadoRetiroMapperGuardarEstadoRetirosComando extends ConvertidorBase<EstadoRetiro, GuardarEstadoRetirosComando> {
}
