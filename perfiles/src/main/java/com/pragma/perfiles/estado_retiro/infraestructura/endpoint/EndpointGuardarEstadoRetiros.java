package com.pragma.perfiles.estado_retiro.infraestructura.endpoint;

import com.pragma.perfiles.estado_retiro.aplicacion.comando.ActualizarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.comando.GuardarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.manejador.ManejadorActualizarEstadoRetiros;
import com.pragma.perfiles.estado_retiro.aplicacion.manejador.ManejadorGuardarEstadoRetiros;
import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("estado-retiro")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarEstadoRetiros {

    private final ManejadorGuardarEstadoRetiros manejadorGuardarEstadoRetiros;


    @PostMapping("guardar")
    public ObjetoRespuesta<EstadoRetiro> ejecutarActualizarEstadoRetiros(@NotNull @RequestBody GuardarEstadoRetirosComando guardarEstadoRetiroComando){

        return manejadorGuardarEstadoRetiros.ejecutar(guardarEstadoRetiroComando);
    }


}
