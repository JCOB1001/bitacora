package com.pragma.perfiles.estado_retiro.infraestructura.configuracion;
import com.pragma.perfiles.estado_retiro.aplicacion.fabrica.FabricaEstadoRetiro;
import com.pragma.perfiles.estado_retiro.aplicacion.fabrica.impl.EstadoRetiroMapperActualizarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.fabrica.impl.EstadoRetiroMapperGuardarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.manejador.ManejadorActualizarEstadoRetiros;
import com.pragma.perfiles.estado_retiro.aplicacion.manejador.ManejadorGuardarEstadoRetiros;
import com.pragma.perfiles.estado_retiro.aplicacion.manejador.ManejadorObtenerEstadoRetiros;
import com.pragma.perfiles.estado_retiro.dominio.repositorio.EstadoRetiroRepositorio;
import com.pragma.perfiles.estado_retiro.dominio.useCase.EstadoRetiroCrudUseCase;
import com.pragma.perfiles.estado_retiro.infraestructura.persistencia.builder.EstadoRetiroMapperEstadoRetiroEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguracionEstadoRetiro {

    @Bean
    public ManejadorActualizarEstadoRetiros manejadorActualizarEstadoRetiros(EstadoRetiroCrudUseCase estadoRetiroCrudUseCase,
                                                                             FabricaEstadoRetiro fabricaEstadoRetiro){
        return new ManejadorActualizarEstadoRetiros(estadoRetiroCrudUseCase,fabricaEstadoRetiro);
    }

    @Bean
    public ManejadorGuardarEstadoRetiros manejadorGuardarEstadoRetiros(EstadoRetiroCrudUseCase estadoRetiroCrudUseCase,
                                                                       FabricaEstadoRetiro fabricaEstadoRetiro){
        return new ManejadorGuardarEstadoRetiros(estadoRetiroCrudUseCase,fabricaEstadoRetiro);
    }

    @Bean
    public EstadoRetiroCrudUseCase estadoRetiroCrudUseCase(EstadoRetiroRepositorio estadoRetiroRepositorio ){
        return new  EstadoRetiroCrudUseCase(estadoRetiroRepositorio);
    }

    @Bean
    public ManejadorObtenerEstadoRetiros manejadorObtenerEstadoRetiros(EstadoRetiroCrudUseCase estadoRetiroCrudUseCase){
        return new ManejadorObtenerEstadoRetiros(estadoRetiroCrudUseCase);
    }

    @Bean
    public FabricaEstadoRetiro fabricaEstadoRetiroMapper(
            EstadoRetiroMapperActualizarEstadoRetirosComando estadoRetiroMapperActualizarEstadoRetirosComando,
            EstadoRetiroMapperGuardarEstadoRetirosComando estadoRetiroMapperGuardarEstadoRetirosComando){
        return new FabricaEstadoRetiro(estadoRetiroMapperActualizarEstadoRetirosComando,estadoRetiroMapperGuardarEstadoRetirosComando);
    }

}
