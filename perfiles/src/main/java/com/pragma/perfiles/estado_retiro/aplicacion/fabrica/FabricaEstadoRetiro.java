package com.pragma.perfiles.estado_retiro.aplicacion.fabrica;

import com.pragma.perfiles.estado_retiro.aplicacion.comando.ActualizarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.comando.GuardarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.fabrica.impl.EstadoRetiroMapperActualizarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.fabrica.impl.EstadoRetiroMapperGuardarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaEstadoRetiro {
    private final EstadoRetiroMapperActualizarEstadoRetirosComando estadoRetiroMapperActualizarEstadoRetirosComando;

    private final EstadoRetiroMapperGuardarEstadoRetirosComando estadoRetiroMapperGuardarEstadoRetirosComando;


    public EstadoRetiro actualizarEstadoRetiros(ActualizarEstadoRetirosComando actualizarEstadoRetirosComando){
        return estadoRetiroMapperActualizarEstadoRetirosComando.rightToLeft(actualizarEstadoRetirosComando);
    }

    public EstadoRetiro guardarEstadoRetiros(GuardarEstadoRetirosComando guardarEstadoRetirosComando){
        return estadoRetiroMapperGuardarEstadoRetirosComando.rightToLeft(guardarEstadoRetirosComando);
    }
}
