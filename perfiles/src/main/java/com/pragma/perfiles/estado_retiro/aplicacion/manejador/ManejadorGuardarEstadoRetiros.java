package com.pragma.perfiles.estado_retiro.aplicacion.manejador;


import com.pragma.perfiles.estado_retiro.aplicacion.comando.ActualizarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.comando.GuardarEstadoRetirosComando;
import com.pragma.perfiles.estado_retiro.aplicacion.fabrica.FabricaEstadoRetiro;
import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.perfiles.estado_retiro.dominio.useCase.EstadoRetiroCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@RequiredArgsConstructor
public class ManejadorGuardarEstadoRetiros {

    private final EstadoRetiroCrudUseCase estadoRetiroCrudUseCase;
    private final FabricaEstadoRetiro fabricaEstadoRetiro;

    public ObjetoRespuesta<EstadoRetiro> ejecutar(GuardarEstadoRetirosComando guardarEstadoRetiroComando){

        EstadoRetiro estadoRetiro = fabricaEstadoRetiro.guardarEstadoRetiros(guardarEstadoRetiroComando);

        EstadoRetiro EstadoRetiroNuevo =estadoRetiroCrudUseCase.guardar(estadoRetiro);


        return new ObjetoRespuesta<EstadoRetiro>(HttpStatus.OK,EstadoRetiroNuevo);

    }

}
