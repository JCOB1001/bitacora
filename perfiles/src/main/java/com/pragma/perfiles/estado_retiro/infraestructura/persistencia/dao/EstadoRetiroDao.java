package com.pragma.perfiles.estado_retiro.infraestructura.persistencia.dao;


import com.pragma.perfiles.estado_retiro.infraestructura.persistencia.entidad.EstadoRetiroEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstadoRetiroDao extends JpaRepository<EstadoRetiroEntity, String>{

    public Iterable<EstadoRetiroEntity> findByUsuarioId(String usuarioId);
}
