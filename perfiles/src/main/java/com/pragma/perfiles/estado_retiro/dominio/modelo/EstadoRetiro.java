package com.pragma.perfiles.estado_retiro.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EstadoRetiro extends Modelo<String> {

    public String id_usuario;
    public Integer estado;

    public EstadoRetiro() {
        super(null);
    }

    public EstadoRetiro(String id, String id_usuario, int estado) {
        super(id);
        this.id_usuario = id_usuario;
        this.estado = estado;
    }
}
