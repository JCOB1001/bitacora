package com.pragma.perfiles.estado_retiro.dominio.useCase;

import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.perfiles.estado_retiro.dominio.repositorio.EstadoRetiroRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class EstadoRetiroCrudUseCase extends CrudUseCaseCommon<EstadoRetiroRepositorio, EstadoRetiro, String> {

    private final EstadoRetiroRepositorio estadoRetiroRepositorio;

    public EstadoRetiroCrudUseCase(EstadoRetiroRepositorio estadoRetiroRepositorio) {
        super(estadoRetiroRepositorio);
        this.estadoRetiroRepositorio = estadoRetiroRepositorio;
    }

    public Stream<EstadoRetiro> obtenerPorId_usuario(String id_usuario) {
        return estadoRetiroRepositorio.findbyId_usuario(id_usuario);
    }
}
