package com.pragma.perfiles.estado_retiro.aplicacion.manejador;

import com.pragma.perfiles.estado_retiro.dominio.modelo.EstadoRetiro;
import com.pragma.perfiles.estado_retiro.dominio.useCase.EstadoRetiroCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerEstadoRetiros {

    public final EstadoRetiroCrudUseCase estadoRetiroCrudUseCase;

    public ObjetoRespuesta<Stream<EstadoRetiro>> ejecutar(String userId) {
        return new ObjetoRespuesta<>(HttpStatus.OK,estadoRetiroCrudUseCase.obtenerPorId_usuario(userId));
    }
}
