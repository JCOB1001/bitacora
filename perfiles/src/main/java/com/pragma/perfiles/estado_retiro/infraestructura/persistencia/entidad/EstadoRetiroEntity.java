package com.pragma.perfiles.estado_retiro.infraestructura.persistencia.entidad;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "estado_retiro")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EstadoRetiroEntity implements IdEntidad<String> {

    public EstadoRetiroEntity(String id){
        this.id = id;
    }

    public interface atributos extends IdEntidad.Atributos {
        String ID = "id";
        String USUARIO = "usuario";
        String ESTADO = "estado";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_estado_retiro_usuario_id"))
    private UsuarioEntity usuario;
    @Column(name = "estado")
    private Integer estado;
}
