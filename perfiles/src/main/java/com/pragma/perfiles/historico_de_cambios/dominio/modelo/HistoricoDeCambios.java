package com.pragma.perfiles.historico_de_cambios.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HistoricoDeCambios extends Modelo<String> {

    private String idUsuario;
    private String idTipoCambio;
    private LocalDate fechaHito;
    private String detalle;

    public HistoricoDeCambios() { super(null); }

    public HistoricoDeCambios(String idUsuario, String idTipoCambio, LocalDate fechaHito, String detalle) {
        this.idUsuario = idUsuario;
        this.idTipoCambio = idTipoCambio;
        this.fechaHito = fechaHito;
        this.detalle = detalle;
    }
}
