package com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.entidad;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;


@Entity
@Table(name = "historico_de_cambios")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoricoDeCambiosEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos {
        String USUARIO = "usuario";
        String FECHA_HITO = "fechaHito";
        String TIPO_CAMBIO = "tipoCambio";
        String DETALLE = "detalle";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_historico_de_cambios_usuario_usuario_id"))
    private UsuarioEntity usuario;

    @ManyToOne
    @JoinColumn(name = "tipo_cambio_id", foreignKey = @ForeignKey(name = "fk_historico_de_cambios_tipo_cambio_tipo_cambio_id"))
    private TipoCambioEntity tipoCambio;

    @Column(name = "fecha_hito")
    private LocalDate fechaHito;

    @Column(name = "detalle")
    private String detalle;



}
