package com.pragma.perfiles.historico_de_cambios.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;

@Getter
public class TipoCambio extends Modelo<String> {

    private String nombreCambio;

    public TipoCambio(String nombreCambio) {

        this.nombreCambio = nombreCambio;
    }
}
