package com.pragma.perfiles.historico_de_cambios.infraestructura.endpoint;

import com.pragma.perfiles.historico_de_cambios.aplicacion.comando.GuardarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.aplicacion.manejador.ManejadorGuardarHistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(TextosComunes.HISTORICO_DE_CAMBIOS)
@RequiredArgsConstructor
@Validated
public class EndpointGuardarHistoricoDeCambios {

    private final ManejadorGuardarHistoricoDeCambios manejadorGuardarHistoricoDeCambios;

    @PostMapping
    public RespuestaBase ejecutarGuardarInformacionLaboralPragma(@NotNull @RequestBody GuardarHistoricoDeCambiosComando guardarHistoricoDeCambiosComando){
        manejadorGuardarHistoricoDeCambios.ejecutar(guardarHistoricoDeCambiosComando);
        return RespuestaBase.ok("Guardado Exitosamente");
    }
}
