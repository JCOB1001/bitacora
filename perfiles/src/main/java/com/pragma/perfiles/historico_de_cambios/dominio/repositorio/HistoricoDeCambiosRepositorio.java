package com.pragma.perfiles.historico_de_cambios.dominio.repositorio;

import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;

public interface HistoricoDeCambiosRepositorio extends CrudRepositorioBase<String, HistoricoDeCambios> {
    Stream<HistoricoDeCambios> findByUsuarioId(String usuarioId);

}
