package com.pragma.perfiles.historico_de_cambios.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.HistoricoCambioNoEncontrado;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.HistoricoDeCambiosCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorEliminarHistoricoDeCambios {

    private final HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public void ejecutar(String idHistoricoDeCambios){
        HistoricoDeCambios historicoDeCambios = historicoDeCambiosCrudUseCase.obtenerPorId(idHistoricoDeCambios);

        if(historicoDeCambios == null){
            throw new HistoricoCambioNoEncontrado("Este historico de cambios no existe");
        }

        Usuario usuario=usuarioCrudUseCase.obtenerPorId(historicoDeCambios.getIdUsuario());
        if(usuario!=null){
            usuario.setFechaUltimaActualizacion(LocalDate.now());
            usuarioCrudUseCase.actualizar(usuario);
        }
        historicoDeCambiosCrudUseCase.eliminarPorId(idHistoricoDeCambios);
    }
}
