package com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.dao;

import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.entidad.TipoCambioEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TipoCambioDao extends JpaRepository<TipoCambioEntity, String> {
     Optional<TipoCambioEntity> findByNombreCambio(String nombreCambio);
}
