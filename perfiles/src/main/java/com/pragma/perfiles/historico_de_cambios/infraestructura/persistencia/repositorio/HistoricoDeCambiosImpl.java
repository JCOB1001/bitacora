package com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.repositorio;


import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.dominio.repositorio.HistoricoDeCambiosRepositorio;
import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.builder.HistoricoDeCambiosMapperHistoricoDeCambiosEntity;
import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.dao.HistoricoDeCambiosDao;
import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.entidad.HistoricoDeCambiosEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class HistoricoDeCambiosImpl extends CrudRepositorioBaseImpl<String, HistoricoDeCambios, HistoricoDeCambiosEntity> implements HistoricoDeCambiosRepositorio {

    private final HistoricoDeCambiosDao historicoDeCambiosDao;
    private final HistoricoDeCambiosMapperHistoricoDeCambiosEntity historicoDeCambiosMapperHistoricoDeCambiosEntity;

    @Override
    protected HistoricoDeCambiosDao obtenerRepositorio() {
        return historicoDeCambiosDao;
    }

    @Override
    protected HistoricoDeCambiosMapperHistoricoDeCambiosEntity obtenerConversionBase() {
        return historicoDeCambiosMapperHistoricoDeCambiosEntity;
    }

    @Override
    public Stream<HistoricoDeCambios> findByUsuarioId(String usuarioId) {
        return historicoDeCambiosMapperHistoricoDeCambiosEntity.rightToLeft(historicoDeCambiosDao.findByUsuarioId(usuarioId));
    }

}
