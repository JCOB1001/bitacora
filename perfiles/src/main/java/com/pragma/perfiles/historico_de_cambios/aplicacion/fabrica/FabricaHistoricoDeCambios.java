package com.pragma.perfiles.historico_de_cambios.aplicacion.fabrica;


import com.pragma.perfiles.historico_de_cambios.aplicacion.comando.ActualizarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.aplicacion.comando.GuardarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.aplicacion.fabrica.impl.HistoricoDeCambiosMapperActualizarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.aplicacion.fabrica.impl.HistoricoDeCambiosMapperGuardarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaHistoricoDeCambios {

    private final HistoricoDeCambiosMapperGuardarHistoricoDeCambiosComando historicoDeCambiosMapperGuardarHistoricoDeCambiosComando;
    private final HistoricoDeCambiosMapperActualizarHistoricoDeCambiosComando historicoDeCambiosMapperActualizarHistoricoDeCambiosComando;

    public HistoricoDeCambios guardarHistoricoDeCambios(GuardarHistoricoDeCambiosComando guardarHistoricoDeCambiosComando){
        return historicoDeCambiosMapperGuardarHistoricoDeCambiosComando.rightToLeft(guardarHistoricoDeCambiosComando);
    }

    public HistoricoDeCambios actualizarHistoricoDeCambios(ActualizarHistoricoDeCambiosComando actualizarHistoricoDeCambiosComando){
        return historicoDeCambiosMapperActualizarHistoricoDeCambiosComando.rightToLeft(actualizarHistoricoDeCambiosComando);
    }

}
