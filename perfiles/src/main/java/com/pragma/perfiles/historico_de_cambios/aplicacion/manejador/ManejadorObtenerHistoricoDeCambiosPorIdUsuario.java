package com.pragma.perfiles.historico_de_cambios.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.HistoricoDeCambiosCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerHistoricoDeCambiosPorIdUsuario {

    private final HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<Stream<HistoricoDeCambios>> ejecutar(String idUsuario){
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }
        Stream<HistoricoDeCambios> HistoricoDeCambiosStream = historicoDeCambiosCrudUseCase.findByUsuarioId(usuario.getId());
        return new ObjetoRespuesta(HttpStatus.OK, HistoricoDeCambiosStream);
    }
}
