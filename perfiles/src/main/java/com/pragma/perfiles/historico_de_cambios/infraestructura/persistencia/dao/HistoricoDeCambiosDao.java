package com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.dao;

import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.entidad.HistoricoDeCambiosEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoricoDeCambiosDao extends JpaRepository<HistoricoDeCambiosEntity, String> {

    Iterable<HistoricoDeCambiosEntity> findByUsuarioId(String usuarioId);

}
