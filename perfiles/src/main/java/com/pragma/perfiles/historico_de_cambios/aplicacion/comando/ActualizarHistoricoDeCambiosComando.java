package com.pragma.perfiles.historico_de_cambios.aplicacion.comando;

import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarHistoricoDeCambiosComando {
    private String idTipoCambio;
    private LocalDate fechaHito;
    private String detalle;
    private String tipoIdentificacion;
    private String identificacion;
}
