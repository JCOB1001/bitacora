package com.pragma.perfiles.historico_de_cambios.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.HistoricoCambioNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.historico_de_cambios.aplicacion.comando.ActualizarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.aplicacion.fabrica.FabricaHistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.HistoricoDeCambiosCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorActualizarHistoricoDeCambios {

    private final HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase;
    private final FabricaHistoricoDeCambios fabricaHistoricoDeCambios;
    private final FeignTipoDocumento feignTipoDocumento;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public void ejecutar(String idHistoricoDeCambios , ActualizarHistoricoDeCambiosComando actualizarHistoricoDeCambiosComando){

        Long idDocumento = feignTipoDocumento.obtenerIdTipoDocumentoPorCoincidenciaNombre(actualizarHistoricoDeCambiosComando.getTipoIdentificacion());
        if (idDocumento == null) {
            throw new UsuarioNoEncontrado("El usuario con el ID: " + actualizarHistoricoDeCambiosComando.getIdentificacion() + " no existe");
        }

        Usuario usuario = usuarioCrudUseCase.buscarPorIdentificacion(idDocumento, actualizarHistoricoDeCambiosComando.getIdentificacion());

        if (usuario == null) {
            throw new UsuarioNoEncontrado("El usuario con el ID: " + actualizarHistoricoDeCambiosComando.getIdentificacion() + " no existe");
        }

        HistoricoDeCambios historicoDeCambios = historicoDeCambiosCrudUseCase.obtenerPorId(idHistoricoDeCambios);
        if(historicoDeCambios == null){
            throw new HistoricoCambioNoEncontrado("Este historico no existe en la Base de Datos. ");
        }

        if(!historicoDeCambios.getIdUsuario().equals(usuario.getId())){
            throw new HistoricoCambioNoEncontrado("El id del usuario enviado no coincide con los registros de la base de dato ");
        }

        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.guardar(usuario);
        HistoricoDeCambios historicoCambiosNuevo = fabricaHistoricoDeCambios.actualizarHistoricoDeCambios(actualizarHistoricoDeCambiosComando);
        historicoCambiosNuevo.setId(idHistoricoDeCambios);
        historicoDeCambiosCrudUseCase.actualizar(historicoCambiosNuevo);
    }

}
