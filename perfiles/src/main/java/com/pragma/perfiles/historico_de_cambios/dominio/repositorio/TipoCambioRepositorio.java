package com.pragma.perfiles.historico_de_cambios.dominio.repositorio;

import com.pragma.perfiles.historico_de_cambios.dominio.modelo.TipoCambio;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.Optional;
import java.util.stream.Stream;

public interface TipoCambioRepositorio extends CrudRepositorioBase<String, TipoCambio> {
    TipoCambio findByTipoCambioId(String idTipoCambio);
    TipoCambio findByNombreCambio(String tipoCambio);
}
