package com.pragma.perfiles.historico_de_cambios.dominio.UseCase;

import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.TipoCambio;
import com.pragma.perfiles.historico_de_cambios.dominio.repositorio.HistoricoDeCambiosRepositorio;
import com.pragma.perfiles.historico_de_cambios.dominio.repositorio.TipoCambioRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.Optional;
import java.util.stream.Stream;

public class TipoCambioCrudUseCase extends CrudUseCaseCommon<TipoCambioRepositorio, TipoCambio, String>{

    private final TipoCambioRepositorio tipoCambioRepositorio;

    public TipoCambioCrudUseCase(TipoCambioRepositorio tipoCambioRepositorio) {
        super(tipoCambioRepositorio);
        this.tipoCambioRepositorio = tipoCambioRepositorio;
    }

    public TipoCambio findByTipoCambioId(String idTipoCambio){
        return tipoCambioRepositorio.findByTipoCambioId(idTipoCambio);
    }

    public TipoCambio findByNombreCambio(String nombreCambio) {
        return tipoCambioRepositorio.findByNombreCambio(nombreCambio);
    }
}
