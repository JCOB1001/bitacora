package com.pragma.perfiles.historico_de_cambios.dominio.UseCase;

import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.dominio.repositorio.HistoricoDeCambiosRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class HistoricoDeCambiosCrudUseCase extends CrudUseCaseCommon<HistoricoDeCambiosRepositorio, HistoricoDeCambios, String> {

    private final HistoricoDeCambiosRepositorio historicoDeCambiosRepositorio;

    public HistoricoDeCambiosCrudUseCase(HistoricoDeCambiosRepositorio historicoDeCambiosRepositorio) {
        super(historicoDeCambiosRepositorio);
        this.historicoDeCambiosRepositorio = historicoDeCambiosRepositorio;
    }

    public Stream<HistoricoDeCambios> findByUsuarioId(String usuarioId){
        return historicoDeCambiosRepositorio.findByUsuarioId(usuarioId);
    }

}
