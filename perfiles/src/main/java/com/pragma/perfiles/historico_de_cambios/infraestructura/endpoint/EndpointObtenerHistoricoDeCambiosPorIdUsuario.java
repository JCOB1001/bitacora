package com.pragma.perfiles.historico_de_cambios.infraestructura.endpoint;

import com.pragma.perfiles.historico_de_cambios.aplicacion.manejador.ManejadorObtenerHistoricoDeCambiosPorIdUsuario;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping(TextosComunes.HISTORICO_DE_CAMBIOS)
@RequiredArgsConstructor
@Validated
public class EndpointObtenerHistoricoDeCambiosPorIdUsuario {

    private final ManejadorObtenerHistoricoDeCambiosPorIdUsuario manejadorObtenerHistoricoDeCambiosPorIdUsuario;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<Stream<HistoricoDeCambios>> ejecutarObtenerHistoricoDeCambiosPorIdUsuario(@NotNull @PathVariable String idUsuario){
        return manejadorObtenerHistoricoDeCambiosPorIdUsuario.ejecutar(idUsuario);
    }
}
