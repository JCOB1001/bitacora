package com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.builder;

import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.TipoCambio;
import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.entidad.HistoricoDeCambiosEntity;
import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.entidad.TipoCambioEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface TipoCambioMapperTipoCambioEntity extends ConvertidorBase<TipoCambio, TipoCambioEntity> {

    TipoCambio leftToRight(TipoCambioEntity tipoCambioEntity);

    TipoCambioEntity rightToLeft(TipoCambio tipoCambio);
}
