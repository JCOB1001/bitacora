package com.pragma.perfiles.historico_de_cambios.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.TipoCambioNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.historico_de_cambios.aplicacion.comando.GuardarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.HistoricoDeCambiosCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.TipoCambioCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.TipoCambio;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@RequiredArgsConstructor
public class ManejadorGuardarHistoricoDeCambios {

    private final HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FeignTipoDocumento feignTipoDocumento;
    private final TipoCambioCrudUseCase tipoCambioCrudUseCase;

    public void ejecutar(GuardarHistoricoDeCambiosComando guardarHistoricoDeCambiosComando) {

        Long idDocumento = feignTipoDocumento.obtenerIdTipoDocumentoPorCoincidenciaNombre(guardarHistoricoDeCambiosComando.getTipoIdentificacion());
        if (idDocumento == null) {
            throw new UsuarioNoEncontrado("El usuario con el ID: " + guardarHistoricoDeCambiosComando.getIdentificacion() + " no existe");
        }

        Usuario usuario = usuarioCrudUseCase.buscarPorIdentificacion(idDocumento, guardarHistoricoDeCambiosComando.getIdentificacion());

        if (usuario == null) {
            throw new UsuarioNoEncontrado("El usuario con el ID: " + guardarHistoricoDeCambiosComando.getIdentificacion() + " no existe");
        }

        TipoCambio cambio = tipoCambioCrudUseCase.findByTipoCambioId(guardarHistoricoDeCambiosComando.getIdTipoCambio());

        if(cambio == null){
            throw new TipoCambioNoEncontrado("El cambio a realizar no es valido.");
        }

        HistoricoDeCambios nuevo = new HistoricoDeCambios(usuario.getId(), guardarHistoricoDeCambiosComando.getIdTipoCambio(), guardarHistoricoDeCambiosComando.getFechaHito(), guardarHistoricoDeCambiosComando.getDetalle());
        historicoDeCambiosCrudUseCase.guardar(nuevo);
        usuario.setFechaUltimaActualizacion(LocalDate.now());
        usuarioCrudUseCase.actualizar(usuario);
    }

}
