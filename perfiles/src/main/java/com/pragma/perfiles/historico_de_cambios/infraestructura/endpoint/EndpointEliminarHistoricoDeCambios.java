package com.pragma.perfiles.historico_de_cambios.infraestructura.endpoint;

import com.pragma.perfiles.historico_de_cambios.aplicacion.manejador.ManejadorEliminarHistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(TextosComunes.HISTORICO_DE_CAMBIOS)
@RequiredArgsConstructor
@Validated
public class EndpointEliminarHistoricoDeCambios {
    private final ManejadorEliminarHistoricoDeCambios manejadorEliminarHistoricoDeCambios;

    @DeleteMapping("/{idHistoricoDeCambios}")
    public RespuestaBase ejecutarEliminarContratoHistorico(@NotNull @PathVariable String idHistoricoDeCambios){
        manejadorEliminarHistoricoDeCambios.ejecutar(idHistoricoDeCambios);
        return RespuestaBase.ok("Borrado Exitosamente");
    }
}
