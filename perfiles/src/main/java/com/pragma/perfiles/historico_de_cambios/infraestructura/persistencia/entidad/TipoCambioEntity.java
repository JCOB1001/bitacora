package com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.entidad;

import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_cambio")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TipoCambioEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos {
        String NOMBRE_CAMBIO = "nombreCambio";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name="nombre_cambio")
    private String nombreCambio;
}
