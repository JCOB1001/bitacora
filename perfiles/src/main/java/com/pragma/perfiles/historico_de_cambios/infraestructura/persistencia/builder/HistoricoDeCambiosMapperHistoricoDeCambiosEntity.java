package com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.builder;

import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.entidad.HistoricoDeCambiosEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface HistoricoDeCambiosMapperHistoricoDeCambiosEntity extends ConvertidorBase<HistoricoDeCambios, HistoricoDeCambiosEntity> {

    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "idUsuario", target = "usuario.id"),
            @Mapping(source = "idTipoCambio", target = "tipoCambio.id")
    })
    HistoricoDeCambiosEntity leftToRight(HistoricoDeCambios historicoDeCambios);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "usuario.id", target = "idUsuario"),
            @Mapping(source = "tipoCambio.id", target = "idTipoCambio")
    })
    HistoricoDeCambios rightToLeft(HistoricoDeCambiosEntity historicoDeCambiosEntity);

}
