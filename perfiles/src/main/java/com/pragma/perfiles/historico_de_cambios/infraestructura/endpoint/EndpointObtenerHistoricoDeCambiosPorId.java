package com.pragma.perfiles.historico_de_cambios.infraestructura.endpoint;

import com.pragma.perfiles.historico_de_cambios.aplicacion.manejador.ManejadorObtenerHistoricoDeCambiosPorId;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(TextosComunes.HISTORICO_DE_CAMBIOS)
@RequiredArgsConstructor
@Validated
public class EndpointObtenerHistoricoDeCambiosPorId {
    private final ManejadorObtenerHistoricoDeCambiosPorId manejadorObtenerHistoricoDeCambiosPorId;

    @GetMapping("/obtener/{idHistoricoDeCambios}")
    public ObjetoRespuesta<HistoricoDeCambios> obtenerPoId(@NotNull @PathVariable String idHistoricoDeCambios){

        ObjetoRespuesta<HistoricoDeCambios> HistoricoDeCambiosObjetoRespuestaObjetoRespuesta= manejadorObtenerHistoricoDeCambiosPorId.ejecutar(idHistoricoDeCambios);
        return new ObjetoRespuesta<>(HttpStatus.OK,HistoricoDeCambiosObjetoRespuestaObjetoRespuesta.getDato());
    }
}
