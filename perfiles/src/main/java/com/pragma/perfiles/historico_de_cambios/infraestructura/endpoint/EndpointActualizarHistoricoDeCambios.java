package com.pragma.perfiles.historico_de_cambios.infraestructura.endpoint;

import com.pragma.perfiles.historico_de_cambios.aplicacion.comando.ActualizarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.aplicacion.manejador.ManejadorActualizarHistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.infraestructura.configuracion.TextosComunes;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(TextosComunes.HISTORICO_DE_CAMBIOS)
@RequiredArgsConstructor
@Validated
public class EndpointActualizarHistoricoDeCambios {
    private final ManejadorActualizarHistoricoDeCambios manejadorActualizarHistoricoDeCambios;

    @PutMapping("/{idHistoricoDeCambios}")
    public RespuestaBase ejecutarActualizarExperienciaLaboralPragma(@NotNull @PathVariable String idHistoricoDeCambios,
                                                                    @NotNull @RequestBody ActualizarHistoricoDeCambiosComando actualizarHistoricoDeCambiosComando){
        manejadorActualizarHistoricoDeCambios.ejecutar(idHistoricoDeCambios, actualizarHistoricoDeCambiosComando);
        return RespuestaBase.ok("Actualizado Exitosamente");
    }
}
