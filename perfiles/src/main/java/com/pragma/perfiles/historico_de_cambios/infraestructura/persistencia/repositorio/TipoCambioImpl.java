package com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.historico_de_cambios.dominio.modelo.TipoCambio;
import com.pragma.perfiles.historico_de_cambios.dominio.repositorio.TipoCambioRepositorio;
import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.builder.TipoCambioMapperTipoCambioEntity;
import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.dao.TipoCambioDao;
import com.pragma.perfiles.historico_de_cambios.infraestructura.persistencia.entidad.TipoCambioEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class TipoCambioImpl extends CrudRepositorioBaseImpl<String, TipoCambio, TipoCambioEntity> implements TipoCambioRepositorio {

    private final TipoCambioDao tipoCambioDao;
    private final TipoCambioMapperTipoCambioEntity tipoCambioMapperTipoCambioEntity;


    @Override
    protected TipoCambioDao obtenerRepositorio() {
        return tipoCambioDao;
    }

    @Override
    protected ConvertidorBase<TipoCambio, TipoCambioEntity> obtenerConversionBase() {
        return tipoCambioMapperTipoCambioEntity;
    }

    @Override
    public TipoCambio findByTipoCambioId(String idTipoCambio) {
        Optional<TipoCambioEntity> tipoCambio = tipoCambioDao.findById(idTipoCambio);
        if (tipoCambio.isEmpty()) {
            return null;
        } else {
            return tipoCambioMapperTipoCambioEntity.rightToLeft(tipoCambio.get());

        }

    }

    @Override
    public TipoCambio findByNombreCambio(String nombreCambio) {
        Optional<TipoCambioEntity> tipoCambio = tipoCambioDao.findByNombreCambio(nombreCambio);
        if (tipoCambio.isEmpty()) {
            return null;
        } else {
            return tipoCambioMapperTipoCambioEntity.rightToLeft(tipoCambio.get());

        }
    }
}
