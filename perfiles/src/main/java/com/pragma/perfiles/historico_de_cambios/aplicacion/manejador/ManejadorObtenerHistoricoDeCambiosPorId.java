package com.pragma.perfiles.historico_de_cambios.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.HistoricoCambioNoEncontrado;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.HistoricoDeCambiosCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorObtenerHistoricoDeCambiosPorId {
    private final HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase;

    public ObjetoRespuesta<HistoricoDeCambios> ejecutar(String idHistoricoDeCambios){
        HistoricoDeCambios historicoDeCambios = historicoDeCambiosCrudUseCase.obtenerPorId(idHistoricoDeCambios);

        if(historicoDeCambios == null){
            throw new HistoricoCambioNoEncontrado("El historico de cambios no se encuentra registrado");
        }
        return new ObjetoRespuesta<>(historicoDeCambios);
    }
}
