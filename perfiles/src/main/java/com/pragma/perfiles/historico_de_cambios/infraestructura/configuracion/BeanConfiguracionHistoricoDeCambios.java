package com.pragma.perfiles.historico_de_cambios.infraestructura.configuracion;

import com.pragma.perfiles.historico_de_cambios.aplicacion.fabrica.FabricaHistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.aplicacion.fabrica.impl.HistoricoDeCambiosMapperActualizarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.aplicacion.fabrica.impl.HistoricoDeCambiosMapperGuardarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.aplicacion.manejador.ManejadorActualizarHistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.aplicacion.manejador.ManejadorEliminarHistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.aplicacion.manejador.ManejadorGuardarHistoricoDeCambios;
import com.pragma.perfiles.historico_de_cambios.aplicacion.manejador.ManejadorObtenerHistoricoDeCambiosPorId;
import com.pragma.perfiles.historico_de_cambios.aplicacion.manejador.ManejadorObtenerHistoricoDeCambiosPorIdUsuario;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.HistoricoDeCambiosCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.UseCase.TipoCambioCrudUseCase;
import com.pragma.perfiles.historico_de_cambios.dominio.repositorio.HistoricoDeCambiosRepositorio;
import com.pragma.perfiles.historico_de_cambios.dominio.repositorio.TipoCambioRepositorio;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguracionHistoricoDeCambios {

    @Bean
    public ManejadorActualizarHistoricoDeCambios manejadorActualizarHistoricoDeCambios(
            HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase,
            FabricaHistoricoDeCambios fabricaHistoricoDeCambios, FeignTipoDocumento feignTipoDocumento,
            UsuarioCrudUseCase usuarioCrudUseCase) {
        return new ManejadorActualizarHistoricoDeCambios(historicoDeCambiosCrudUseCase,
                fabricaHistoricoDeCambios, feignTipoDocumento,
                usuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerHistoricoDeCambiosPorId manejadorObtenerHistoricoDeCambiosPorId(
            HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase){
       return new ManejadorObtenerHistoricoDeCambiosPorId(historicoDeCambiosCrudUseCase);
    }

    @Bean
    public ManejadorObtenerHistoricoDeCambiosPorIdUsuario manejadorObtenerHistoricoDeCambiosPorIdUsuario(
            HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase,
            UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerHistoricoDeCambiosPorIdUsuario(
                historicoDeCambiosCrudUseCase,
                usuarioCrudUseCase);
    }

    @Bean
    public ManejadorEliminarHistoricoDeCambios manejadorEliminarHistoricoDeCambios(HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase,
                                                                                   UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorEliminarHistoricoDeCambios(historicoDeCambiosCrudUseCase,usuarioCrudUseCase);
    }

    @Bean
    public HistoricoDeCambiosCrudUseCase HistoricoDeCambiosCrudUseCase(
            HistoricoDeCambiosRepositorio historicoDeCambiosRepositorio){
        return new HistoricoDeCambiosCrudUseCase(historicoDeCambiosRepositorio);
    }

    @Bean
    public TipoCambioCrudUseCase tipoCambioCrudUseCase(
            TipoCambioRepositorio tipoCambioRepositorio){
        return new TipoCambioCrudUseCase(tipoCambioRepositorio);
    }

    @Bean
    public FabricaHistoricoDeCambios fabricaHistoricoDeCambios(
            HistoricoDeCambiosMapperActualizarHistoricoDeCambiosComando historicoDeCambiosMapperActualizarHistoricoDeCambiosComando,
            HistoricoDeCambiosMapperGuardarHistoricoDeCambiosComando contratoHistoricoMapperGuardarInfoContratoNominaComando){

        return new FabricaHistoricoDeCambios(contratoHistoricoMapperGuardarInfoContratoNominaComando,
                historicoDeCambiosMapperActualizarHistoricoDeCambiosComando);

    }
    @Bean
    public ManejadorGuardarHistoricoDeCambios manejadorGuardarHistoricoDeCambios(HistoricoDeCambiosCrudUseCase historicoDeCambiosCrudUseCase,
                                                                                 FabricaHistoricoDeCambios fabricaHistoricoDeCambios,
                                                                                 UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                 FeignTipoDocumento feignTipoDocumento, TipoCambioCrudUseCase tipoCambioCrudUseCase){

        return new ManejadorGuardarHistoricoDeCambios(historicoDeCambiosCrudUseCase,
                usuarioCrudUseCase, feignTipoDocumento, tipoCambioCrudUseCase);
    }
}
