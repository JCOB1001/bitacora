package com.pragma.perfiles.historico_de_cambios.aplicacion.fabrica.impl;


import com.pragma.perfiles.historico_de_cambios.aplicacion.comando.ActualizarHistoricoDeCambiosComando;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface HistoricoDeCambiosMapperActualizarHistoricoDeCambiosComando extends ConvertidorBase<HistoricoDeCambios, ActualizarHistoricoDeCambiosComando> {
}
