package com.pragma.perfiles.otros_estudios.dominio.useCase;

import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudiosDetalle;
import com.pragma.perfiles.otros_estudios.dominio.repositorio.OtrosEstudiosDetalleRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

public class OtrosEstudiosDetalleCrudUseCase extends CrudUseCaseCommon<OtrosEstudiosDetalleRepositorio, OtrosEstudiosDetalle, String> {

    private final OtrosEstudiosDetalleRepositorio otrosEstudiosDetalleRepositorio;

    public OtrosEstudiosDetalleCrudUseCase(OtrosEstudiosDetalleRepositorio otrosEstudiosDetalleRepositorio){

        super(otrosEstudiosDetalleRepositorio);
        this.otrosEstudiosDetalleRepositorio=otrosEstudiosDetalleRepositorio;
    }

    public OtrosEstudiosDetalle findByOtrosEstudiosIdAndIdiomaId(String otrosEstudiosId, String idiomaId) {
        return this.otrosEstudiosDetalleRepositorio.findByOtrosEstudiosIdAndIdiomaId(otrosEstudiosId,idiomaId);
    }
}
