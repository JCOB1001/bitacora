package com.pragma.perfiles.otros_estudios.dominio.repositorio;

import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudiosDetalle;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

public interface OtrosEstudiosDetalleRepositorio extends CrudRepositorioBase<String, OtrosEstudiosDetalle> {

    OtrosEstudiosDetalle findByOtrosEstudiosIdAndIdiomaId(String otrosEstudiosId, String idiomaId);
}
