package com.pragma.perfiles.otros_estudios.dominio.useCase;

import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarArchivoComando;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.repositorio.OtrosEstudiosRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class OtrosEstudiosCrudUseCase extends CrudUseCaseCommon<OtrosEstudiosRepositorio,
        OtrosEstudios, String> {
    private final OtrosEstudiosRepositorio otrosEstudiosRepositorio;

    public  OtrosEstudiosCrudUseCase(OtrosEstudiosRepositorio otrosEstudiosRepositorio){
        super(otrosEstudiosRepositorio);
        this.otrosEstudiosRepositorio = otrosEstudiosRepositorio;
    }

    public Stream<OtrosEstudios> findByUsuario(String idUsuario){
        return otrosEstudiosRepositorio.findByIdUsuario(idUsuario);
    }

    public String guardarCertificado(GuardarArchivoComando guardarArchivoComando){
        return otrosEstudiosRepositorio.guardarCertificado(guardarArchivoComando);
    }
    public void eliminarCertificado(GuardarArchivoComando guardarArchivoComando){
        otrosEstudiosRepositorio.eliminarCertificado(guardarArchivoComando);
    }
}
