package com.pragma.perfiles.otros_estudios.infraestructura.endpoint;

import com.pragma.perfiles.otros_estudios.aplicacion.comando.ActualizarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.manejador.ManejadorActualizarOtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("info-academica-otro")
@RequiredArgsConstructor
@Validated
public class EndPointActualizarOtrosEstudios {

    private final ManejadorActualizarOtrosEstudios manejadorActualizarOtrosEstudios;

    @PutMapping("/{idInfoOtro}")
    public ObjetoRespuesta<OtrosEstudios> actualizarOtrosEstudios(@NotNull @RequestBody ActualizarOtrosEstudiosComando actualizarOtrosEstudiosComando,
                                                                  @NotNull @PathVariable String idInfoOtro){
        return manejadorActualizarOtrosEstudios.ejecutarActualizarOtrosEstudios(actualizarOtrosEstudiosComando, idInfoOtro);
    }
}
