package com.pragma.perfiles.otros_estudios.infraestructura.endpoint;

import com.pragma.perfiles.otros_estudios.aplicacion.comando.ActualizarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.ActualizarOtrosEstudiosComandoUrl;
import com.pragma.perfiles.otros_estudios.aplicacion.manejador.ManejadorActualizarOtrosEstudios;
import com.pragma.perfiles.otros_estudios.aplicacion.manejador.ManejadorActualizarOtrosEstudiosUrl;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("info-academica-otro")
@RequiredArgsConstructor
@Validated
public class EndPointActualizarOtrosEstudiosUrl {

    private final ManejadorActualizarOtrosEstudiosUrl manejadorActualizarOtrosEstudiosUrl;

    @PutMapping("/url/{idInfoOtro}")
    public ObjetoRespuesta<OtrosEstudios> actualizarOtrosEstudios(@NotNull @RequestBody ActualizarOtrosEstudiosComandoUrl actualizarOtrosEstudiosComandoUrl,
                                                                  @NotNull @PathVariable String idInfoOtro){
        return manejadorActualizarOtrosEstudiosUrl.ejecutarActualizarOtrosEstudios(actualizarOtrosEstudiosComandoUrl, idInfoOtro);
    }
}
