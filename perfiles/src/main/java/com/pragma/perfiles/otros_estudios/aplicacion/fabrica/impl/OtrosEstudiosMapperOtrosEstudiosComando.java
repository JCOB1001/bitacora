package com.pragma.perfiles.otros_estudios.aplicacion.fabrica.impl;

import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OtrosEstudiosMapperOtrosEstudiosComando extends ConvertidorBase<OtrosEstudios, GuardarOtrosEstudiosComando> {
}
