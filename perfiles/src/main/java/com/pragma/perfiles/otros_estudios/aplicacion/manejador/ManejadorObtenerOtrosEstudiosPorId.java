package com.pragma.perfiles.otros_estudios.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerOtrosEstudiosPorId {

    private final OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public Stream<OtrosEstudios> ejecutar(String idUsuario){

        Usuario usuario=usuarioCrudUseCase.obtenerPorId(idUsuario);

        if (usuario == null){
            throw new UsuarioNoEncontrado("El usuario no existe");
        }

        return otrosEstudiosCrudUseCase.findByUsuario(idUsuario);
    }

}
