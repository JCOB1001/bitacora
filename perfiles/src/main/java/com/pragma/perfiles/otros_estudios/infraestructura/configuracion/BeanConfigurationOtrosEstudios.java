package com.pragma.perfiles.otros_estudios.infraestructura.configuracion;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.otros_estudios.aplicacion.fabrica.FabricaOtrosEstudios;
import com.pragma.perfiles.otros_estudios.aplicacion.fabrica.impl.OtrosEstudiosMapperActualizarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.fabrica.impl.OtrosEstudiosMapperActualizarOtrosEstudiosComandoUrl;
import com.pragma.perfiles.otros_estudios.aplicacion.fabrica.impl.OtrosEstudiosMapperOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.manejador.*;
import com.pragma.perfiles.otros_estudios.dominio.repositorio.OtrosEstudiosRepositorio;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosDetalleCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationOtrosEstudios {

    @Bean
    public OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase(OtrosEstudiosRepositorio otrosEstudiosRepositorio){
        return new OtrosEstudiosCrudUseCase(otrosEstudiosRepositorio);
    }

    @Bean
    public FabricaOtrosEstudios fabricaOtrosEstudios(OtrosEstudiosMapperOtrosEstudiosComando otrosEstudiosMapperOtrosEstudiosComando, OtrosEstudiosMapperActualizarOtrosEstudiosComando otrosEstudiosMapperActualizarOtrosEstudiosComando,
                                                     OtrosEstudiosMapperActualizarOtrosEstudiosComandoUrl otrosEstudiosMapperActualizarOtrosEstudiosComandoUrl){
        return new FabricaOtrosEstudios(otrosEstudiosMapperOtrosEstudiosComando, otrosEstudiosMapperActualizarOtrosEstudiosComando,otrosEstudiosMapperActualizarOtrosEstudiosComandoUrl);

    }

    @Bean
    public ManejadorGuardarOtrosEstudios manejadorGuardarOtrosEstudios(
            OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase,
            FabricaOtrosEstudios fabricaOtrosEstudios,
            UsuarioCrudUseCase usuarioCrudUseCase,ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso,
            OtrosEstudiosDetalleCrudUseCase otrosEstudiosDetalleCrudUseCase){
        return new ManejadorGuardarOtrosEstudios(otrosEstudiosCrudUseCase,
                fabricaOtrosEstudios, usuarioCrudUseCase, manejadorCalcularValorBarraProgreso,
                otrosEstudiosDetalleCrudUseCase);
    }

    @Bean
    public ManejadorActualizarOtrosEstudios manejadorActualizarOtrosEstudios(
            OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase,
            FabricaOtrosEstudios fabricaOtrosEstudios,
            UsuarioCrudUseCase usuarioCrudUseCase,
            OtrosEstudiosDetalleCrudUseCase otrosEstudiosDetalleCrudUseCase
    ){
        return  new ManejadorActualizarOtrosEstudios(
                otrosEstudiosCrudUseCase,
                fabricaOtrosEstudios,
                usuarioCrudUseCase,
                otrosEstudiosDetalleCrudUseCase);
    }

    @Bean
    public ManejadorActualizarOtrosEstudiosUrl manejadorActualizarOtrosEstudiosUrl(
            OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase,
            FabricaOtrosEstudios fabricaOtrosEstudios,
            UsuarioCrudUseCase usuarioCrudUseCase,
            OtrosEstudiosDetalleCrudUseCase otrosEstudiosDetalleCrudUseCase
    ){
        return  new ManejadorActualizarOtrosEstudiosUrl(
                otrosEstudiosCrudUseCase,
                fabricaOtrosEstudios,
                usuarioCrudUseCase,
                otrosEstudiosDetalleCrudUseCase);
    }


    @Bean
    public ManejadorObtenerOtrosEstudiosListarTodo manejadorObtenerOtrosEstudiosListarTodo(
            OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase
    ){
        return new ManejadorObtenerOtrosEstudiosListarTodo(otrosEstudiosCrudUseCase);
    }

    @Bean
    public ManejadorObtenerOtrosEstudiosPorId manejadorObtenerOtrosEstudiosPorId(
            OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase,
            UsuarioCrudUseCase usuarioCrudUseCase
    ){
        return new ManejadorObtenerOtrosEstudiosPorId(
                otrosEstudiosCrudUseCase,
                usuarioCrudUseCase);
    }

    @Bean
    public ManejadorEliminarOtrosEstudios manejadorEliminarOtrosEstudios(UsuarioCrudUseCase usuarioCrudUseCase, OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase,ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso){

        return new ManejadorEliminarOtrosEstudios(usuarioCrudUseCase,otrosEstudiosCrudUseCase, manejadorCalcularValorBarraProgreso);
    }

}
