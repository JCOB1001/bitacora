package com.pragma.perfiles.otros_estudios.dominio.repositorio;

import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarArchivoComando;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.List;
import java.util.stream.Stream;

public interface OtrosEstudiosRepositorio extends CrudRepositorioBase<String, OtrosEstudios> {
    Stream<OtrosEstudios> findByIdUsuario(String idUsuario);

    String guardarCertificado(GuardarArchivoComando guardarArchivoComando);

    void eliminarCertificado(GuardarArchivoComando guardarArchivoComando);

}
