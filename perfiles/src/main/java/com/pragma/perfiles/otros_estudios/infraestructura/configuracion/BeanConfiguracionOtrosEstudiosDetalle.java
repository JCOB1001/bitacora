package com.pragma.perfiles.otros_estudios.infraestructura.configuracion;

import com.pragma.perfiles.otros_estudios.dominio.repositorio.OtrosEstudiosDetalleRepositorio;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosDetalleCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguracionOtrosEstudiosDetalle {

    @Bean
    public OtrosEstudiosDetalleCrudUseCase otrosEstudiosDetalleCrudUseCase(OtrosEstudiosDetalleRepositorio otrosEstudiosDetalleRepositorio){
        return  new OtrosEstudiosDetalleCrudUseCase(otrosEstudiosDetalleRepositorio);
    }
}
