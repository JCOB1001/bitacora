package com.pragma.perfiles.otros_estudios.infraestructura.endpoint;

import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.manejador.ManejadorGuardarOtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("info-academica-otro")
@RequiredArgsConstructor
@Validated
public class EndpointGuardarOtrosEstudios {

    private final ManejadorGuardarOtrosEstudios manejadorGuardarOtrosEstudios;

    @PostMapping
    public ObjetoRespuesta<OtrosEstudios> ejecutarGuardarOtrosEstudios(@NotNull @RequestBody GuardarOtrosEstudiosComando guardarOtrosEstudiosComando){
       return  manejadorGuardarOtrosEstudios.ejecutarGuardarOtrosEstudios(guardarOtrosEstudiosComando);

    }


}
