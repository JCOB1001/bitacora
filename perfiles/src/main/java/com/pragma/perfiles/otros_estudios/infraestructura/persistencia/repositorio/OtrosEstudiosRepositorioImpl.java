package com.pragma.perfiles.otros_estudios.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarArchivoComando;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.repositorio.OtrosEstudiosRepositorio;
import com.pragma.perfiles.otros_estudios.infraestructura.clienteFeing.FeignFilesApi;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.builder.OtrosEstudiosMapperOtrosEstudiosEntity;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.dao.OtrosEstudiosDao;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad.OtrosEstudiosEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class OtrosEstudiosRepositorioImpl extends CrudRepositorioBaseImpl<String, OtrosEstudios, OtrosEstudiosEntity> implements OtrosEstudiosRepositorio {

    private final OtrosEstudiosDao otrosEstudiosDao;
    private final OtrosEstudiosMapperOtrosEstudiosEntity otrosEstudiosMapperOtrosEstudiosEntity;
    private final FeignFilesApi feignFilesApi;

    @Override
    protected CrudRepository<OtrosEstudiosEntity, String> obtenerRepositorio() {
        return otrosEstudiosDao;
    }

    @Override
    protected ConvertidorBase<OtrosEstudios, OtrosEstudiosEntity> obtenerConversionBase() {
        return otrosEstudiosMapperOtrosEstudiosEntity;
    }

    @Override
    public Stream<OtrosEstudios> findByIdUsuario(String idUsuario) {
        return otrosEstudiosMapperOtrosEstudiosEntity.rightToLeft(otrosEstudiosDao.findByUsuarioId(idUsuario));
    }

    @Override
    public String guardarCertificado(GuardarArchivoComando guardarArchivoComando) {
        if(guardarArchivoComando.getFile()==null){
            throw  new UsuarioNoEncontrado("El archivo no debe ser valido ");
        }
            return feignFilesApi.fileUpload(guardarArchivoComando).getBody();
    }
    @Override
    public void eliminarCertificado(GuardarArchivoComando guardarArchivoComando) {
        feignFilesApi.deleteFile(guardarArchivoComando);
    }

}
