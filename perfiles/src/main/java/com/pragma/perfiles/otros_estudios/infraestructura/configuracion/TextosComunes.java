package com.pragma.perfiles.otros_estudios.infraestructura.configuracion;

public class TextosComunes {

    public static final String RUTA_CONTROLADOR ="otros-estudios";
    public static final String CARPETA_GUARDAR_CERTTIFICADO="certicados";
    public static final String CARPETA_GUARDAR_CERTIFICADO_IDIOMA = "idiomas";
}
