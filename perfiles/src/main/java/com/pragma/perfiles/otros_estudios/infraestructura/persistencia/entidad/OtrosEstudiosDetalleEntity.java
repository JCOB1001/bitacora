package com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad;

import com.pragma.perfiles.idiomas.infraestructura.persistencia.entidad.IdiomasEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "otros_estudios_detalle")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OtrosEstudiosDetalleEntity implements IdEntidad<String> {
    public interface ATRIBUTOS extends Atributos{
        String ID = "id";
        String ETIQUETA = "etiquetas";
        String TITULO = "titulo";
        String OTROSESTUDIOS = "otrosEstudios";
        String INSTITUTO = "instituto";
        String IDIOMA = "idioma";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idiomas_id", foreignKey = @ForeignKey(name = "fk_otros_estudios_idiomas_idiomas_id"))
    private IdiomasEntity idioma;
    private String etiquetas;
    private String titulo;
    private String instituto;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "otros_estudios_id", foreignKey = @ForeignKey(name = "fk_otros_estudios_otros_estudios_id"))
    private OtrosEstudiosEntity otrosEstudios;
}
