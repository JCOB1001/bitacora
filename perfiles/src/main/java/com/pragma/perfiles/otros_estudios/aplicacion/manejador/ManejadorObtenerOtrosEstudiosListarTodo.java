package com.pragma.perfiles.otros_estudios.aplicacion.manejador;

import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerOtrosEstudiosListarTodo {

    private final OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase;

    public Stream<OtrosEstudios> ejecutar(){
        return otrosEstudiosCrudUseCase.obtenerTodos();
    }


}
