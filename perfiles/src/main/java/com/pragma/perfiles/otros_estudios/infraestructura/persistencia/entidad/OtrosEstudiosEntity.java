package com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "otros_estudios")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OtrosEstudiosEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends Atributos{
        String ID = "id";
        String FECHA_EXPIRACION = "fechaExpiracion";
        String HORAS = "horas";
        String ETIQUETA = "etiquetas";
        String TITULO = "titulo";
        String USUARIO = "usuario";
        String ANIO = "anio";
        String URL = "urlCertificado";
        String INSTITUTO = "instituto";
        String OTROSESTUDIOSDETALLE = "otrosEstudiosDetalle";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_otros_estudios_usuario_usuario_id"))
    private UsuarioEntity usuario;
    @Column(name = "fecha_de_expiracion")
    private LocalDate fechaExpiracion;
    private Double horas;
    private String etiquetas;
    private String titulo;
    private String anio;
    private String instituto;
    private String urlCertificado;
    @JsonManagedReference
    @OneToMany(mappedBy = "otrosEstudios",fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<OtrosEstudiosDetalleEntity> otrosEstudiosDetalle;
}
