package com.pragma.perfiles.otros_estudios.aplicacion.manejador;


import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarArchivoComando;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.fabrica.FabricaOtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudiosDetalle;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosDetalleCrudUseCase;
import com.pragma.perfiles.otros_estudios.infraestructura.configuracion.TextosComunes;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.UUID;

@RequiredArgsConstructor
public class ManejadorGuardarOtrosEstudios {


    private final OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase;
    private final FabricaOtrosEstudios fabricaOtrosEstudios;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;
    private final OtrosEstudiosDetalleCrudUseCase otrosEstudiosDetalleCrudUseCase;

    public ObjetoRespuesta<OtrosEstudios> ejecutarGuardarOtrosEstudios(GuardarOtrosEstudiosComando guardarOtrosEstudiosComando){
        String idIdioma = HttpRequestContextHolder.getIdioma();
        Usuario usuario = usuarioCrudUseCase.obtenerPorId(guardarOtrosEstudiosComando.getIdUsuario());

        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }

        OtrosEstudios otrosEstudios = fabricaOtrosEstudios.guardarOtrosEstudios(guardarOtrosEstudiosComando);

        String urlFormateada="https://d2y95fubzeqro6.cloudfront.net/general/certicados/20200327120334.";
        if (!guardarOtrosEstudiosComando.getArchivo().isEmpty()){
            GuardarArchivoComando guardarArchivoComando=GuardarArchivoComando.builder()
                    .nombreArchivo(UUID.randomUUID().toString()+guardarOtrosEstudiosComando.getExtension())
                    .carpeta(TextosComunes.CARPETA_GUARDAR_CERTTIFICADO)
                    .file(guardarOtrosEstudiosComando.getArchivo()).build();

            String url =otrosEstudiosCrudUseCase.guardarCertificado(guardarArchivoComando);
            urlFormateada=formatUrl(url);
            System.out.println("La url es " + urlFormateada);
            otrosEstudios.setUrlCertificado(urlFormateada);
        }



        OtrosEstudios nuevoOtroEstudio = otrosEstudiosCrudUseCase.guardar(otrosEstudios);

        OtrosEstudiosDetalle otrosEstudiosDetalle=new OtrosEstudiosDetalle(null,nuevoOtroEstudio.getEtiquetas(),nuevoOtroEstudio.getTitulo(),nuevoOtroEstudio.getId(),nuevoOtroEstudio.getInstituto(),idIdioma);
        otrosEstudiosDetalleCrudUseCase.guardar(otrosEstudiosDetalle);

        double valorPorcentajeTotal = manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(guardarOtrosEstudiosComando.getIdUsuario());
        usuario.setBarraProgreso(valorPorcentajeTotal);
        usuarioCrudUseCase.actualizar(usuario);

        return new ObjetoRespuesta<OtrosEstudios>(HttpStatus.OK, nuevoOtroEstudio);
    }


    public String formatUrl(String url){

        String res[]=url.replace("{","").replace("}","").replace("\"","").split("/");
        String respuesta="https://";
        for (int i=2;i<res.length;i++ ){
            respuesta+=res[i];
            if(i<res.length-1){
                respuesta+="/";}
        }
        return respuesta;
    }

    public String getNombreFromUrl(String url){
        String res=url.split("/")[5];
        return res;
    }

}
