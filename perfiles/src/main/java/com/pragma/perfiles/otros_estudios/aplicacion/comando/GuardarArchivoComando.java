package com.pragma.perfiles.otros_estudios.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GuardarArchivoComando {

    private String file;
    private String carpeta;
    private String nombreArchivo;
}
