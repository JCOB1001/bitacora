package com.pragma.perfiles.otros_estudios.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorObtenerInformacionFamiliarPorId;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.otros_estudios.aplicacion.manejador.ManejadorObtenerOtrosEstudiosListarTodo;
import com.pragma.perfiles.otros_estudios.aplicacion.manejador.ManejadorObtenerOtrosEstudiosPorId;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("info-academica-otro")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerOtrosEstudiosPorId {

    private final ManejadorObtenerOtrosEstudiosPorId
            manejadorObtenerOtrosEstudiosPorId;

    @GetMapping("/{idUsuario}")
    public ObjetoRespuesta<Stream<OtrosEstudios>> ejecutarObtenerOtrosEstudiosPorId(@NotNull @PathVariable String idUsuario){
        Stream<OtrosEstudios> otrosEstudiosStream = manejadorObtenerOtrosEstudiosPorId
                .ejecutar(idUsuario);
        return new ObjetoRespuesta<>(HttpStatus.OK, otrosEstudiosStream);
    }

}
