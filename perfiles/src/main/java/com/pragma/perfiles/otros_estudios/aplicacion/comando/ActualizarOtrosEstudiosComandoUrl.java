package com.pragma.perfiles.otros_estudios.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarOtrosEstudiosComandoUrl {

    private String idUsuario;
    private LocalDate fechaExpiracion;
    private Double horas;
    private String etiquetas;
    private String titulo;
    private String anio;
    private String instituto;
    private String urlCertificado;
}
