package com.pragma.perfiles.otros_estudios.infraestructura.persistencia.builder;

import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioDetalleEntity;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioEntity;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad.OtrosEstudiosDetalleEntity;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad.OtrosEstudiosEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.*;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface OtrosEstudiosMapperOtrosEstudiosEntity extends ConvertidorBase<OtrosEstudios, OtrosEstudiosEntity> {
    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "idUsuario", target = "usuario.id")
    )
    OtrosEstudiosEntity leftToRight(OtrosEstudios otrosEstudios);


    @Named("rightToLeft")
    @Override
    @Mappings(
            @Mapping(source = "usuario.id", target = "idUsuario")
    )
    OtrosEstudios rightToLeft(OtrosEstudiosEntity estudiosEntity);

    @AfterMapping
    default void afterRightToLeft(@MappingTarget OtrosEstudios otrosEstudios, OtrosEstudiosEntity otrosEstudiosEntity) {
        if(otrosEstudiosEntity.getOtrosEstudiosDetalle()!=null) {
            List<OtrosEstudiosDetalleEntity> otrosEstudiosDetalle = otrosEstudiosEntity.getOtrosEstudiosDetalle().stream().filter(item ->
                    item.getIdioma().getId().equals(HttpRequestContextHolder.getIdioma())).collect(Collectors.toList());
            if(otrosEstudiosDetalle.isEmpty()) {
                otrosEstudiosDetalle.add(new OtrosEstudiosDetalleEntity());
            }
            otrosEstudios.setEtiquetas(otrosEstudiosDetalle.get(0).getEtiquetas());
            otrosEstudios.setInstituto(otrosEstudiosDetalle.get(0).getInstituto());
            otrosEstudios.setTitulo(otrosEstudiosDetalle.get(0).getTitulo());
        }
    }

}
