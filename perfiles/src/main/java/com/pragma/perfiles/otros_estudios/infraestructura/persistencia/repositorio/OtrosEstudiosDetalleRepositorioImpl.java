package com.pragma.perfiles.otros_estudios.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudiosDetalle;
import com.pragma.perfiles.otros_estudios.dominio.repositorio.OtrosEstudiosDetalleRepositorio;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.builder.OtrosEstudiosDetalleMapperOtrosEstudiosDetalleEntity;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.dao.OtrosEstudiosDetalleDao;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad.OtrosEstudiosDetalleEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class OtrosEstudiosDetalleRepositorioImpl extends CrudRepositorioBaseImpl<String, OtrosEstudiosDetalle, OtrosEstudiosDetalleEntity> implements OtrosEstudiosDetalleRepositorio {

    private final OtrosEstudiosDetalleDao otrosEstudiosDetalleDao;

    private final OtrosEstudiosDetalleMapperOtrosEstudiosDetalleEntity otrosEstudiosDetalleMapperOtrosEstudiosDetalleEntity;

    @Override
    public OtrosEstudiosDetalleDao obtenerRepositorio(){ return otrosEstudiosDetalleDao; }

    @Override
    protected OtrosEstudiosDetalleMapperOtrosEstudiosDetalleEntity obtenerConversionBase(){
        return otrosEstudiosDetalleMapperOtrosEstudiosDetalleEntity;
    }

    public OtrosEstudiosDetalle findByOtrosEstudiosIdAndIdiomaId(String otrosEstudiosId, String idiomaId) {
        return obtenerConversionBase().rightToLeft(otrosEstudiosDetalleDao.findByOtrosEstudiosIdAndIdiomaId(otrosEstudiosId,idiomaId));
    }

}
