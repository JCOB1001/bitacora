package com.pragma.perfiles.otros_estudios.infraestructura.persistencia.dao;

import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad.OtrosEstudiosDetalleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OtrosEstudiosDetalleDao extends JpaRepository<OtrosEstudiosDetalleEntity, String> {

    OtrosEstudiosDetalleEntity findByOtrosEstudiosIdAndIdiomaId(String otrosEstudiosId, String idiomaId);
}
