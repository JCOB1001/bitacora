package com.pragma.perfiles.otros_estudios.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@Setter
@ToString
public class OtrosEstudios extends Modelo<String> {

    private String idUsuario;
    private LocalDate fechaExpiracion;
    private Double horas;
    private String etiquetas;
    private String titulo;
    private String anio;
    private String instituto;
    private String urlCertificado;

    public OtrosEstudios(){
        super(null);
    }

    public OtrosEstudios(String id,
                         String idUsuario,
                         LocalDate fechaExpiracion,
                         Double horas,
                         String etiquetas,
                         String titulo,
                         String anio,
                         String instituto,
                         String urlCertificado){
        super(id);
        this.idUsuario = idUsuario;
        this.fechaExpiracion = fechaExpiracion;
        this.horas = horas;
        this.etiquetas = etiquetas;
        this.titulo = titulo;
        this.anio = anio;
        this.instituto = instituto;
        this.urlCertificado = urlCertificado;
    }
}
