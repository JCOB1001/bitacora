package com.pragma.perfiles.otros_estudios.aplicacion.manejador;

import com.pragma.perfiles.barra_progreso.aplicacion.manejadores.ManejadorCalcularValorBarraProgreso;
import com.pragma.perfiles.comun.infraestructura.error.EstudioNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarArchivoComando;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import com.pragma.perfiles.otros_estudios.infraestructura.configuracion.TextosComunes;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarOtrosEstudios {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase;
    private final ManejadorCalcularValorBarraProgreso manejadorCalcularValorBarraProgreso;

    public ObjetoRespuesta<String> ejecutar(String idEstudio,String idUsuario){
        OtrosEstudios estudios=otrosEstudiosCrudUseCase.obtenerPorId(idEstudio);

        if(estudios==null){
            throw  new EstudioNoEncontrado("No se encontro ningun estudio con el ID:"+idEstudio);
        }
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(idUsuario);

        if (usuario==null){
            throw new UsuarioNoEncontrado("No se encontro el usuario con el ID: "+idUsuario);
        }

        if(!estudios.getIdUsuario().equals(idUsuario)){
            throw new EstudioNoEncontrado("El id del usaurio en los estudios no coinciden con los registros en la bd");
        }
        if(estudios.getUrlCertificado()!=null){
            if(estudios.getUrlCertificado().isBlank() || estudios.getUrlCertificado().isEmpty()){
                estudios.setUrlCertificado(null);
            }else {
                String[] res=estudios.getUrlCertificado().split("/");
                if(res.length<5){
                    estudios.setUrlCertificado(null);
                }
            }
        }
        if (estudios.getUrlCertificado() != null ) {
            GuardarArchivoComando elimianarArchico = GuardarArchivoComando.builder().nombreArchivo(getNombreFromUrl(estudios.getUrlCertificado()))
                    .carpeta(TextosComunes.CARPETA_GUARDAR_CERTTIFICADO).build();

            otrosEstudiosCrudUseCase.eliminarCertificado(elimianarArchico);
        }

        otrosEstudiosCrudUseCase.eliminarPorId(idEstudio);

        double valorPorcentajeTotal = manejadorCalcularValorBarraProgreso.obtenerBarraProgreso(idUsuario);
        usuario.setBarraProgreso(valorPorcentajeTotal);
        usuarioCrudUseCase.actualizar(usuario);

        return new ObjetoRespuesta<>("Success");
    }

    public String getNombreFromUrl(String url){
        url = url.trim();
        String res=url.split("/")[5];

        return res;
    }
}
