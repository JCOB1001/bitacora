package com.pragma.perfiles.otros_estudios.infraestructura.clienteFeing;

import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarArchivoComando;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "FeignFilesApi", url = "${feign.modulo.filesApi}/file")
public interface FeignFilesApi {

    @PostMapping
    ResponseEntity<String> fileUpload(@RequestBody GuardarArchivoComando file);

    @DeleteMapping
    ResponseEntity deleteFile(@RequestBody GuardarArchivoComando file);
}
