package com.pragma.perfiles.otros_estudios.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OtrosEstudiosDetalle extends Modelo<String> {

   private String etiquetas;
   private String titulo;
   private String idOtrosEstudios;
   private String instituto;
   private String idIdioma;

    public OtrosEstudiosDetalle(String id, String etiquetas, String titulo, String idOtrosEstudios, String instituto, String idIdioma) {
        super(id);
        this.etiquetas = etiquetas;
        this.titulo = titulo;
        this.idOtrosEstudios = idOtrosEstudios;
        this.instituto = instituto;
        this.idIdioma = idIdioma;
    }
}
