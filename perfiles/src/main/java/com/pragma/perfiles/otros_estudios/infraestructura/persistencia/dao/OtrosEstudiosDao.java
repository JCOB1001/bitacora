package com.pragma.perfiles.otros_estudios.infraestructura.persistencia.dao;

import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad.OtrosEstudiosEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OtrosEstudiosDao extends JpaRepository<OtrosEstudiosEntity, String> {

    Iterable<OtrosEstudiosEntity> findByUsuarioId(String idUsuario);

}
