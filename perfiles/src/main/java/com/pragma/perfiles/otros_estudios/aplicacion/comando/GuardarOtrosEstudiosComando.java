package com.pragma.perfiles.otros_estudios.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuardarOtrosEstudiosComando {

    private String idUsuario;
    private LocalDate fechaExpiracion;
    private Double horas;
    private String etiquetas;
    private String titulo;
    private String anio;
    private String instituto;
    private String archivo;
    private String extension;

}
