package com.pragma.perfiles.otros_estudios.infraestructura.endpoint;

import com.pragma.perfiles.otros_estudios.aplicacion.manejador.ManejadorObtenerOtrosEstudiosListarTodo;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("info-academica-otro")
@RequiredArgsConstructor
@Validated
public class EndpointObtenerOtrosEstudiosListarTodo {

    private final ManejadorObtenerOtrosEstudiosListarTodo
            manejadorObtenerOtrosEstudiosListarTodo;

    @GetMapping
    public ObjetoRespuesta<Stream<OtrosEstudios>>  ejecutar(){
        Stream<OtrosEstudios> otrosEstudiosStream = manejadorObtenerOtrosEstudiosListarTodo
                .ejecutar();
        return new ObjetoRespuesta<>(HttpStatus.OK, otrosEstudiosStream);
    }

}
