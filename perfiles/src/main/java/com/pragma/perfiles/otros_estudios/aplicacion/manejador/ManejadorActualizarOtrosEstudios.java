package com.pragma.perfiles.otros_estudios.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.configuracion.util.HttpRequestContextHolder;
import com.pragma.perfiles.comun.infraestructura.error.OtrosEstudiosNoEncontrado;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioDetalleCrudUseCase;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.ActualizarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarArchivoComando;
import com.pragma.perfiles.otros_estudios.aplicacion.fabrica.FabricaOtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudiosDetalle;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosCrudUseCase;
import com.pragma.perfiles.otros_estudios.dominio.useCase.OtrosEstudiosDetalleCrudUseCase;
import com.pragma.perfiles.otros_estudios.infraestructura.configuracion.TextosComunes;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.UUID;

@RequiredArgsConstructor
public class ManejadorActualizarOtrosEstudios {

    private final OtrosEstudiosCrudUseCase otrosEstudiosCrudUseCase;
    private final FabricaOtrosEstudios fabricaOtrosEstudios;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final OtrosEstudiosDetalleCrudUseCase otrosEstudiosDetalleCrudUseCase;

    public ObjetoRespuesta<OtrosEstudios> ejecutarActualizarOtrosEstudios(ActualizarOtrosEstudiosComando actualizarOtrosEstudiosComando,
                                                                   String idInfoOtro){
        String idIdioma = HttpRequestContextHolder.getIdioma();
        OtrosEstudios otrosEstudios = otrosEstudiosCrudUseCase.obtenerPorId(idInfoOtro);
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(actualizarOtrosEstudiosComando.getIdUsuario());
        OtrosEstudiosDetalle otrosEstudiosDetalle = otrosEstudiosDetalleCrudUseCase.findByOtrosEstudiosIdAndIdiomaId(idInfoOtro,idIdioma);

        if (otrosEstudios == null){
            throw new OtrosEstudiosNoEncontrado("Los estudios no fueron encontrados");
        }

        if (usuario == null){
            throw new UsuarioNoEncontrado("El usuario no existe");
        }

        if(!usuario.getId().equals(actualizarOtrosEstudiosComando.getIdUsuario())){
            throw new OtrosEstudiosNoEncontrado("El ID del usuario enviado no coinside con el id enviado de otros estudios");
        }
        if(otrosEstudios.getUrlCertificado()!=null){
            if(otrosEstudios.getUrlCertificado().isBlank() || otrosEstudios.getUrlCertificado().isEmpty()){
                otrosEstudios.setUrlCertificado(null);
            }else {
                String[] res=otrosEstudios.getUrlCertificado().split("/");
                if(res.length<5){
                    otrosEstudios.setUrlCertificado(null);
                }
            }
        }

        if (otrosEstudios.getUrlCertificado() != null) {
            GuardarArchivoComando eliminarArchivoComando = GuardarArchivoComando.builder().nombreArchivo(getNombreFromUrl(otrosEstudios.getUrlCertificado()))
                    .carpeta(TextosComunes.CARPETA_GUARDAR_CERTTIFICADO).build();

            otrosEstudiosCrudUseCase.eliminarCertificado(eliminarArchivoComando);
        }

        GuardarArchivoComando guardarArchivoComando= GuardarArchivoComando.builder().
                nombreArchivo(UUID.randomUUID().toString()+actualizarOtrosEstudiosComando.getExtension())
                .file(actualizarOtrosEstudiosComando.getArchivo())
                .carpeta(TextosComunes.CARPETA_GUARDAR_CERTTIFICADO).build();

        String urlFormateada=formatUrl(otrosEstudiosCrudUseCase.guardarCertificado(guardarArchivoComando));

        OtrosEstudios otrosEstudiosNuevos = fabricaOtrosEstudios.actualizarOtrosEstudios(actualizarOtrosEstudiosComando);

        otrosEstudiosNuevos.setId(idInfoOtro);
        otrosEstudiosNuevos.setUrlCertificado(urlFormateada);

        otrosEstudiosCrudUseCase.actualizar(otrosEstudiosNuevos);
        if(otrosEstudiosDetalle==null){
            otrosEstudiosDetalle=new OtrosEstudiosDetalle(null,otrosEstudiosNuevos.getEtiquetas(),otrosEstudiosNuevos.getTitulo(),otrosEstudiosNuevos.getId(),otrosEstudiosNuevos.getInstituto(),idIdioma);
            otrosEstudiosDetalleCrudUseCase.guardar(otrosEstudiosDetalle);
        }else{
            otrosEstudiosDetalle.setEtiquetas(otrosEstudiosNuevos.getEtiquetas());
            otrosEstudiosDetalle.setInstituto(otrosEstudiosNuevos.getInstituto());
            otrosEstudiosDetalle.setTitulo(otrosEstudiosNuevos.getTitulo());
            otrosEstudiosDetalleCrudUseCase.actualizar(otrosEstudiosDetalle);
        }

        return new ObjetoRespuesta<OtrosEstudios>(HttpStatus.OK, otrosEstudiosNuevos);
    }

    public String getNombreFromUrl(String url){
        String res=url.split("/")[5];
        return res;
    }

    public String formatUrl(String url){

        String res[]=url.replace("{","").replace("}","").replace("\"","").split("/");
        String respuesta="https://";

        for (int i=2;i<res.length;i++ ){
            respuesta+=res[i];
            if(i<res.length-1){
                respuesta+="/";}
        }
        return respuesta;
    }
}
