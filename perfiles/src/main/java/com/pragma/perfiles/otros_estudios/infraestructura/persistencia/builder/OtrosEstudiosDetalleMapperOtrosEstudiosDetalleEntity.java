package com.pragma.perfiles.otros_estudios.infraestructura.persistencia.builder;

import com.pragma.perfiles.estudio.dominio.modelo.EstudioDetalle;
import com.pragma.perfiles.estudio.infraestrctura.persistencia.entidad.EstudioDetalleEntity;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudiosDetalle;
import com.pragma.perfiles.otros_estudios.infraestructura.persistencia.entidad.OtrosEstudiosDetalleEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface OtrosEstudiosDetalleMapperOtrosEstudiosDetalleEntity extends ConvertidorBase<OtrosEstudiosDetalle, OtrosEstudiosDetalleEntity> {

    @Named("leftToRight")
    @Override
    @Mappings({
            @Mapping(source = "idOtrosEstudios", target = "otrosEstudios.id"),
            @Mapping(source = "idIdioma", target = "idioma.id")
    })
    OtrosEstudiosDetalleEntity leftToRight(OtrosEstudiosDetalle otrosEstudiosDetalle);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "otrosEstudios.id", target = "idOtrosEstudios"),
            @Mapping(source = "idioma.id", target = "idIdioma")
    })
    OtrosEstudiosDetalle rightToLeft(OtrosEstudiosDetalleEntity otrosEstudiosDetalleEntity);
}
