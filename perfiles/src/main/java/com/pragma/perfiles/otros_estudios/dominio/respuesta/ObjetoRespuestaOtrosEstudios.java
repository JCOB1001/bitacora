package com.pragma.perfiles.otros_estudios.dominio.respuesta;

import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaOtrosEstudios extends ObjetoRespuesta {
    public ObjetoRespuestaOtrosEstudios(Stream<OtrosEstudios> dato) {
        super(dato);
    }

}
