package com.pragma.perfiles.otros_estudios.aplicacion.fabrica;

import com.pragma.perfiles.otros_estudios.aplicacion.comando.ActualizarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.ActualizarOtrosEstudiosComandoUrl;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.fabrica.impl.OtrosEstudiosMapperActualizarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.fabrica.impl.OtrosEstudiosMapperActualizarOtrosEstudiosComandoUrl;
import com.pragma.perfiles.otros_estudios.aplicacion.fabrica.impl.OtrosEstudiosMapperOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaOtrosEstudios {

    private final OtrosEstudiosMapperOtrosEstudiosComando otrosEstudiosMapperOtrosEstudiosComando;
    private final OtrosEstudiosMapperActualizarOtrosEstudiosComando otrosEstudiosMapperActualizarOtrosEstudiosComando;
    private final OtrosEstudiosMapperActualizarOtrosEstudiosComandoUrl otrosEstudiosMapperActualizarOtrosEstudiosComandoUrl;

    public OtrosEstudios guardarOtrosEstudios(GuardarOtrosEstudiosComando guardarOtrosEstudiosComando){
        return otrosEstudiosMapperOtrosEstudiosComando.rightToLeft(guardarOtrosEstudiosComando);
    }

    public OtrosEstudios actualizarOtrosEstudios(ActualizarOtrosEstudiosComando actualizarOtrosEstudiosComando){
        return otrosEstudiosMapperActualizarOtrosEstudiosComando.rightToLeft(actualizarOtrosEstudiosComando);
    }

    public OtrosEstudios actualizarOtrosEstudiosUrl(ActualizarOtrosEstudiosComandoUrl actualizarOtrosEstudiosComandoUrl){
        return otrosEstudiosMapperActualizarOtrosEstudiosComandoUrl.rightToLeft(actualizarOtrosEstudiosComandoUrl);
    }


}
