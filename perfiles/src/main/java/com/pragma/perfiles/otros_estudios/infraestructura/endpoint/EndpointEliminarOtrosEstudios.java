package com.pragma.perfiles.otros_estudios.infraestructura.endpoint;

import com.pragma.perfiles.otros_estudios.aplicacion.manejador.ManejadorEliminarOtrosEstudios;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("info-academica-otro")
@RequiredArgsConstructor
@Validated
public class EndpointEliminarOtrosEstudios {

    private final ManejadorEliminarOtrosEstudios manejadorEliminarOtrosEstudios;

    @DeleteMapping("/{idEstudio}/{idUsuario}")
    public ObjetoRespuesta<String> eliminarOtrosEstudios(@PathVariable String idEstudio, @PathVariable String idUsuario){

        return manejadorEliminarOtrosEstudios.ejecutar(idEstudio,idUsuario);
    }
}
