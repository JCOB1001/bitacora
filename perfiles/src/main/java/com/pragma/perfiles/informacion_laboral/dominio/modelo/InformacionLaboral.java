package com.pragma.perfiles.informacion_laboral.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InformacionLaboral extends Modelo<String> {

    private String usuarioId;
    private String tipoContrato;
    private LocalDate fechaInicioContratoActual;
    private String referido;
    private String cliente;
    private String proyecto;
    private LocalDate fechaInicioRelacion;
    private EstadoPeriodoPrueba estadoPeriodoPrueba;
    private String evaluadorPeriodoPrueba;

    public InformacionLaboral(String id, String usuarioId, String tipoContrato, LocalDate fechaInicioContratoActual, String referido, String cliente, String proyecto, LocalDate fechaInicioRelacion, EstadoPeriodoPrueba estadoPeriodoPrueba, String evaluadorPeriodoPrueba) {
        super(id);
        this.usuarioId = usuarioId;
        this.tipoContrato = tipoContrato;
        this.fechaInicioContratoActual = fechaInicioContratoActual;
        this.referido = referido;
        this.cliente = cliente;
        this.proyecto = proyecto;
        this.fechaInicioRelacion = fechaInicioRelacion;
        this.estadoPeriodoPrueba = estadoPeriodoPrueba;
        this.evaluadorPeriodoPrueba = evaluadorPeriodoPrueba;
    }

    public InformacionLaboral() {
        super(null);
    }
}
