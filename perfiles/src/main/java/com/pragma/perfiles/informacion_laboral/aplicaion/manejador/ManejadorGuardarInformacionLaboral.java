package com.pragma.perfiles.informacion_laboral.aplicaion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.informacion_laboral.aplicaion.comando.GuardarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.aplicaion.fabrica.FabricaInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@RequiredArgsConstructor
public class ManejadorGuardarInformacionLaboral {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaInformacionLaboral fabricaInformacionLaboral;
    private final InformacionLaboralCrudUseCase informacionLaboralCrudUseCase;

    public ObjetoRespuesta<InformacionLaboral> ejecutar(GuardarInformacionLaboralComando guardarInformacionLaboralComando) {
        Usuario usuario= usuarioCrudUseCase.obtenerPorId(guardarInformacionLaboralComando.getUsuarioId());

        if(usuario==null){
            throw new UsuarioNoEncontrado("No se encontro el usuario con el ID: "+guardarInformacionLaboralComando.getUsuarioId());
        }

        InformacionLaboral informacionLaboral = fabricaInformacionLaboral.guardarInformacionLaboral(guardarInformacionLaboralComando);
        InformacionLaboral nuevaInformacionLaboral =  informacionLaboralCrudUseCase.guardar(informacionLaboral);

        return new ObjetoRespuesta<InformacionLaboral>(HttpStatus.OK, nuevaInformacionLaboral);
    }
}
