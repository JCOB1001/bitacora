package com.pragma.perfiles.informacion_laboral.infraestructura.configuracion;

import com.pragma.perfiles.informacion_laboral.aplicaion.fabrica.FabricaInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.aplicaion.fabrica.impl.InformacionLaboralMapperActualizarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.aplicaion.fabrica.impl.InformacionLaboralMapperGuardarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.aplicaion.manejador.ManejadorActualizarInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.aplicaion.manejador.ManejadorEliminarInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.aplicaion.manejador.ManejadorGuardarInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.aplicaion.manejador.ManejadorObtenerInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.repositorio.InformacionLaboralRepositorio;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationInformacionLaboral {

    @Bean
    public ManejadorGuardarInformacionLaboral manejadorGuardarInformacionLaboral(UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                 FabricaInformacionLaboral fabricaInformacionLaboral,
                                                                                 InformacionLaboralCrudUseCase informacionLaboralCrudUseCase){
        return new ManejadorGuardarInformacionLaboral(usuarioCrudUseCase, fabricaInformacionLaboral, informacionLaboralCrudUseCase);
    }

    @Bean
    public FabricaInformacionLaboral fabricaInformacionLaboral(
            InformacionLaboralMapperGuardarInformacionLaboralComando informacionLaboralMapperGuardarInformacionLaboralComando,
            InformacionLaboralMapperActualizarInformacionLaboralComando informacionLaboralMapperActualizarInformacionLaboralComando){
        return new FabricaInformacionLaboral(informacionLaboralMapperGuardarInformacionLaboralComando, informacionLaboralMapperActualizarInformacionLaboralComando);
    }

    @Bean
    public InformacionLaboralCrudUseCase informacionLaboralCrudUseCase(InformacionLaboralRepositorio informacionLaboralRepositorio){
        return new InformacionLaboralCrudUseCase(informacionLaboralRepositorio);
    }

    @Bean
    public ManejadorObtenerInformacionLaboral manejadorObtenerInformacionLaboral(InformacionLaboralCrudUseCase informacionLaboralCrudUseCase,
                                                                                 UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorObtenerInformacionLaboral(informacionLaboralCrudUseCase, usuarioCrudUseCase);
    }

    @Bean
    public ManejadorActualizarInformacionLaboral manejadorActualizarInformacionLaboral(UsuarioCrudUseCase usuarioCrudUseCase,
                                                                                    FabricaInformacionLaboral fabricaInformacionLaboral,
                                                                                    InformacionLaboralCrudUseCase informacionLaboralCrudUseCase){
        return new ManejadorActualizarInformacionLaboral(usuarioCrudUseCase, fabricaInformacionLaboral, informacionLaboralCrudUseCase);
    }

    @Bean
    public ManejadorEliminarInformacionLaboral manejadorEliminarInformacionLaboral(InformacionLaboralCrudUseCase informacionLaboralCrudUseCase){
        return new ManejadorEliminarInformacionLaboral(informacionLaboralCrudUseCase);
    }
}
