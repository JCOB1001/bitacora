package com.pragma.perfiles.informacion_laboral.infraestructura.persistencia.builder;

import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.infraestructura.persistencia.entidad.InformacionLaboralEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface InformacionLaboralMapperInformacionLaboralEntity extends ConvertidorBase<InformacionLaboral, InformacionLaboralEntity> {
    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "usuarioId", target = "usuario.id")
    )
    InformacionLaboralEntity leftToRight(InformacionLaboral informacionLaboral);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "usuario.id", target = "usuarioId")
    })
    InformacionLaboral rightToLeft(InformacionLaboralEntity informacionLaboralEntity);

}

