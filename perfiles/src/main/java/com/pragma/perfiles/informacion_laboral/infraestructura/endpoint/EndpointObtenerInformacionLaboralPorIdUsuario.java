package com.pragma.perfiles.informacion_laboral.infraestructura.endpoint;

import com.pragma.perfiles.informacion_laboral.aplicaion.manejador.ManejadorObtenerInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("informacion-laboral")
@RequiredArgsConstructor
@Valid
public class EndpointObtenerInformacionLaboralPorIdUsuario {

    private final ManejadorObtenerInformacionLaboral manejadorObtenerInformacionLaboral;

    @GetMapping
    public ObjetoRespuesta<Stream<InformacionLaboral>> obtenerInformacionLaboral(@NotNull @RequestParam("userId") String userId) {
        return manejadorObtenerInformacionLaboral.ejecutar(userId);
    }
}
