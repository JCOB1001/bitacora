package com.pragma.perfiles.informacion_laboral.infraestructura.persistencia.repositorio;

import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.repositorio.InformacionLaboralRepositorio;
import com.pragma.perfiles.informacion_laboral.infraestructura.persistencia.builder.InformacionLaboralMapperInformacionLaboralEntity;
import com.pragma.perfiles.informacion_laboral.infraestructura.persistencia.dao.InformacionLaboralDao;
import com.pragma.perfiles.informacion_laboral.infraestructura.persistencia.entidad.InformacionLaboralEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class InformacionLaboralRepositorioImpl extends CrudRepositorioBaseImpl<String, InformacionLaboral, InformacionLaboralEntity> implements InformacionLaboralRepositorio {
    private final InformacionLaboralDao informacionLaboralDao;
    private final InformacionLaboralMapperInformacionLaboralEntity informacionLaboralMapperInformacionLaboralEntity;

    @Override
    protected InformacionLaboralDao obtenerRepositorio() {
        return informacionLaboralDao;
    }

    @Override
    protected InformacionLaboralMapperInformacionLaboralEntity obtenerConversionBase() {
        return informacionLaboralMapperInformacionLaboralEntity;
    }

    @Override
    public Stream<InformacionLaboral> findByUsuarioId(String usuarioId) {
        return informacionLaboralMapperInformacionLaboralEntity.rightToLeft(
                informacionLaboralDao.findByUsuarioId(usuarioId)
        );
    }


    @Override
    public InformacionLaboral findById(String id) {
        return informacionLaboralDao.findById(id).map(informacionLaboral -> informacionLaboralMapperInformacionLaboralEntity.rightToLeft(informacionLaboral))
                .orElse(null);
    }
}
