package com.pragma.perfiles.informacion_laboral.aplicaion.fabrica.impl;

import com.pragma.perfiles.informacion_familiar.aplicacion.comando.ActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_laboral.aplicaion.comando.ActualizarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.aplicaion.comando.GuardarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InformacionLaboralMapperActualizarInformacionLaboralComando extends ConvertidorBase<InformacionLaboral, ActualizarInformacionLaboralComando> {
}
