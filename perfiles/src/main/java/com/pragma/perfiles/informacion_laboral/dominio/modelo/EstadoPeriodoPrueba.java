package com.pragma.perfiles.informacion_laboral.dominio.modelo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;
import com.pragma.perfiles.comun.infraestructura.error.EnumNoEncontrado;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGenero;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum EstadoPeriodoPrueba {

    PENDIENTE("PENDIENTE"),
    PROGRAMADO("PROGRAMADO"),
    REALIZADO("REALIZADO");

    private String estadoPeriodoPrueba;

    @JsonValue
    public String getValue() {
        return getEstadoPeriodoPrueba();
    }

    @JsonCreator
    public static EstadoPeriodoPrueba fromValueEstadoPeridoPrueba(String value) {
        if (value != null && value.isEmpty()) {
            return null;
        }
        for (EstadoPeriodoPrueba p : values()) {
            if (p.getValue().equals(value)) {
                return p;
            }
        }
        return PENDIENTE;
    }

}
