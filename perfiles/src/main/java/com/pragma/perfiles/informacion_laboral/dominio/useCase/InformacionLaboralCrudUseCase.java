package com.pragma.perfiles.informacion_laboral.dominio.useCase;

import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.repositorio.InformacionLaboralRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class InformacionLaboralCrudUseCase  extends CrudUseCaseCommon<InformacionLaboralRepositorio, InformacionLaboral, String> {

    private final InformacionLaboralRepositorio informacionLaboralRepositorio;

    public InformacionLaboralCrudUseCase(InformacionLaboralRepositorio informacionLaboralRepositorio){
        super(informacionLaboralRepositorio);
        this.informacionLaboralRepositorio = informacionLaboralRepositorio;
    }

    public InformacionLaboral findById(String id){
        return informacionLaboralRepositorio.findById(id);
    }

    public Stream<InformacionLaboral> findByUsuarioId(String usuarioId){
        return informacionLaboralRepositorio.findByUsuarioId(usuarioId);
    }
}
