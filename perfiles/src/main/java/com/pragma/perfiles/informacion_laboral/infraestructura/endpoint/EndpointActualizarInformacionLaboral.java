package com.pragma.perfiles.informacion_laboral.infraestructura.endpoint;

import com.pragma.perfiles.informacion_laboral.aplicaion.comando.ActualizarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.aplicaion.manejador.ManejadorActualizarInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("informacion-laboral")
@RequiredArgsConstructor
@Valid
public class EndpointActualizarInformacionLaboral {

    private final ManejadorActualizarInformacionLaboral manejadorActualizarInformacionLaboral;

    @PutMapping("/{idInformacionLaboral}")
    public ObjetoRespuesta<InformacionLaboral> actualizarInformacionLaboral(@NotNull @PathVariable String idInformacionLaboral,
                                                                            @NotNull @RequestBody ActualizarInformacionLaboralComando actualizarInformacionLaboralComando) {
        return manejadorActualizarInformacionLaboral.ejecutar(idInformacionLaboral, actualizarInformacionLaboralComando);
    }
}
