package com.pragma.perfiles.informacion_laboral.dominio.repositorio;

import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;

public interface InformacionLaboralRepositorio extends CrudRepositorioBase<String, InformacionLaboral> {

    Stream<InformacionLaboral> findByUsuarioId(String usuarioId);
    InformacionLaboral findById(String id);
}
