package com.pragma.perfiles.informacion_laboral.infraestructura.persistencia.entidad;


import com.pragma.perfiles.informacion_laboral.dominio.modelo.EstadoPeriodoPrueba;
import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "informacion_laboral")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InformacionLaboralEntity implements IdEntidad<String> {

    public interface ATRIBUTOS extends IdEntidad.Atributos {
        String ID = "id";
        String USUARIO = "usuario";
        String TIPO_CONTRATO = "tipoContrato";
        String FECHA_INICIO_CONTRATO_ACTUAL = "fechaInicioContratoActual";
        String REFERIDO = "referido";
        String CLIENTE = "cliente";
        String PROYECTO = "proyecto";
        String FECHA_INICIO_RELACION = "fechaInicioRelacion";
        String ESTADO_PERIODO_PRUEBA = "estadoPeriodoPrueba";
        String EVALUADOR_PERIODO_PRUEBA = "evaluadorPeriodoPrueba";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_informacion_laboral_usuario_usuario_id"))
    private UsuarioEntity usuario;

    @Column(name = "tipo_contrato")
    private String tipoContrato;

    @Column(name = "fecha_inicio_contrato_actual")
    private LocalDate fechaInicioContratoActual;

    @Column(name = "referido")
    private String referido;

    @Column(name = "cliente")
    private String cliente;

    @Column(name = "proyecto")
    private String proyecto;

    @Column(name = "fecha_inicio_relacion")
    private LocalDate fechaInicioRelacion;

    @Column(name = "estado_periodo_prueba")
    @Enumerated(value = EnumType.STRING)
    private EstadoPeriodoPrueba estadoPeriodoPrueba;

    @Column(name = "evaluador_periodo_prueba")
    private String evaluadorPeriodoPrueba;

}
