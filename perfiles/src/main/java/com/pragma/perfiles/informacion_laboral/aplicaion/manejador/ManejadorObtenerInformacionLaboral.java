package com.pragma.perfiles.informacion_laboral.aplicaion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerInformacionLaboral {

    private final InformacionLaboralCrudUseCase informacionLaboralCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    public ObjetoRespuesta<Stream<InformacionLaboral>> ejecutar(String userId) {

        Usuario usuario = usuarioCrudUseCase.obtenerPorId(userId);
        if(usuario == null){
            throw new UsuarioNoEncontrado("Este usuario no existe en la base de datos");
        }

        Stream<InformacionLaboral> informacionLaboral = informacionLaboralCrudUseCase.findByUsuarioId(usuario.getId());
        return new ObjetoRespuesta<>(HttpStatus.OK, informacionLaboral);
    }
}
