package com.pragma.perfiles.informacion_laboral.infraestructura.endpoint;

import com.pragma.perfiles.informacion_laboral.aplicaion.manejador.ManejadorEliminarInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("informacion-laboral")
@RequiredArgsConstructor
@Valid
public class EndpointEliminarInformacionLaboral {

    private final ManejadorEliminarInformacionLaboral manejadorEliminarInformacionLaboral;

    @DeleteMapping("/{idInformacionLaboral}")
    public ObjetoRespuesta<InformacionLaboral> eliminarInformacionLaboral(@NotNull @PathVariable String idInformacionLaboral) {
        return manejadorEliminarInformacionLaboral.ejecutar(idInformacionLaboral);
    }
}
