package com.pragma.perfiles.informacion_laboral.infraestructura.endpoint;

import com.pragma.perfiles.informacion_laboral.aplicaion.comando.GuardarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.aplicaion.manejador.ManejadorGuardarInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("informacion-laboral")
@RequiredArgsConstructor
@Valid
public class EndpointGuardarInformacionLaboral {

    private final ManejadorGuardarInformacionLaboral manejadorGuardarInformacionLaboral;

    @PostMapping
    public ObjetoRespuesta<InformacionLaboral> guardarInformacionLaboral( @NotNull @RequestBody GuardarInformacionLaboralComando guardarInformacionLaboralComando) {
        return manejadorGuardarInformacionLaboral.ejecutar(guardarInformacionLaboralComando);
    }
}
