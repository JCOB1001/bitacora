package com.pragma.perfiles.informacion_laboral.infraestructura.persistencia.dao;

import com.pragma.perfiles.informacion_laboral.infraestructura.persistencia.entidad.InformacionLaboralEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InformacionLaboralDao extends JpaRepository<InformacionLaboralEntity, String> {

    Iterable<InformacionLaboralEntity> findByUsuarioId(String usuarioId);
}
