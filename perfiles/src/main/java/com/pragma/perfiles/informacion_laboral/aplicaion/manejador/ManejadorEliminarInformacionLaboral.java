package com.pragma.perfiles.informacion_laboral.aplicaion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarInformacionLaboral {

    private final InformacionLaboralCrudUseCase informacionLaboralCrudUseCase;

    public ObjetoRespuesta<InformacionLaboral> ejecutar(String idInformacionLaboral){
        InformacionLaboral informacionLaboral = informacionLaboralCrudUseCase.obtenerPorId(idInformacionLaboral);

        if(informacionLaboral == null){
            throw new ExcepcionesInformacionLaboral("No existe esta información Laboral");
        }

        informacionLaboralCrudUseCase.eliminarPorId(idInformacionLaboral);
        return new ObjetoRespuesta<InformacionLaboral>(informacionLaboral);
    }
}
