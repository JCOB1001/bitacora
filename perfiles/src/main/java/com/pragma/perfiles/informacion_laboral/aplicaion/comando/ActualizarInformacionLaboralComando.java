package com.pragma.perfiles.informacion_laboral.aplicaion.comando;

import com.pragma.perfiles.informacion_laboral.dominio.modelo.EstadoPeriodoPrueba;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarInformacionLaboralComando {

    private String usuarioId;
    private String tipoContrato;
    private LocalDate fechaInicioContratoActual;
    private String referido;
    private String cliente;
    private String proyecto;
    private LocalDate fechaInicioRelacion;
    private EstadoPeriodoPrueba estadoPeriodoPrueba;
    private String evaluadorPeriodoPrueba;
}
