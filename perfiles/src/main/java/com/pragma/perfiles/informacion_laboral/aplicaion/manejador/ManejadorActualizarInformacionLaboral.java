package com.pragma.perfiles.informacion_laboral.aplicaion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionLaboral;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.informacion_laboral.aplicaion.comando.ActualizarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.aplicaion.fabrica.FabricaInformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.perfiles.informacion_laboral.dominio.useCase.InformacionLaboralCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class ManejadorActualizarInformacionLaboral {

    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaInformacionLaboral fabricaInformacionLaboral;
    private final InformacionLaboralCrudUseCase informacionLaboralCrudUseCase;

    public ObjetoRespuesta<InformacionLaboral> ejecutar(String idInformacionLaboral, ActualizarInformacionLaboralComando actualizarInformacionLaboralComando) {
        InformacionLaboral informacionLaboral = informacionLaboralCrudUseCase.obtenerPorId(idInformacionLaboral);
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(actualizarInformacionLaboralComando.getUsuarioId());

        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+actualizarInformacionLaboralComando.getUsuarioId()+" no existe");
        }

        if(informacionLaboral == null){
            throw new ExcepcionesInformacionLaboral("La informaacion laboral con el ID: "+idInformacionLaboral+" no existe");
        }


            InformacionLaboral informacionLaboralNuevo = fabricaInformacionLaboral.actualizarInformacionLaboral(actualizarInformacionLaboralComando);
            informacionLaboralNuevo.setId(idInformacionLaboral);
            informacionLaboralCrudUseCase.actualizar(informacionLaboralNuevo);
            informacionLaboral = informacionLaboralNuevo;

        return new ObjetoRespuesta<InformacionLaboral>(informacionLaboral);
    }
}
