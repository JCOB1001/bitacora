package com.pragma.perfiles.informacion_laboral.aplicaion.fabrica.impl;

import com.pragma.perfiles.informacion_laboral.aplicaion.comando.GuardarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InformacionLaboralMapperGuardarInformacionLaboralComando extends ConvertidorBase<InformacionLaboral, GuardarInformacionLaboralComando> {
}
