package com.pragma.perfiles.informacion_nomina.infraestructura.persistencia.entidad;

import com.pragma.perfiles.perfil.infraestructura.persistencia.entidad.UsuarioEntity;
import com.pragma.vacaciones.common.persistencia.IdEntidad;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "informacion_nomina")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class InformacionNominaEntity implements IdEntidad<String> {
    public interface ATRIBUTOS extends IdEntidad.Atributos {
        String CARGO_NOMINA= "cargoNomina";
        String SALARIO_BASICO_ORDINARIO = "salarioBasicoOrdinario";
        String SALARIO_BASICO_INTEGRAL = "salarioBasicoIntegral";
        String NO_CONSTITUTIVOS_SALARIO = "noConstitutivosSalario";
        String MONEDA = "moneda";
        String USUARIO = "usuario";
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_informacion_nomina_usuario_usuario_id"))
    private UsuarioEntity usuario;

    @Column(name = "cargo_nomina")
    private String cargoNomina;

    @Column(name = "salario_basico_ordinario")
    private Double salarioBasicoOrdinario;

    @Column(name = "salario_basico_integral")
    private Double salarioBasicoIntegral;

    @Column(name = "no_constitutivos_salario")
    private Double noConstitutivosSalario;

    @Column(name = "moneda")
    private String moneda;
}
