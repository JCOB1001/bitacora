package com.pragma.perfiles.informacion_nomina.infraestructura.endpoint;

import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorEliminarInformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_nomina.aplicacion.manejador.ManejadorEliminarInformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("informacion-nomina")
@RequiredArgsConstructor
@Validated
public class EndPointEliminarInformacionNomina {

    private final ManejadorEliminarInformacionNomina manejadorEliminarInformacionNomina;

    @DeleteMapping("/{idInformacionNomina}/{idUsuario}")
    public ObjetoRespuesta<InformacionNomina> ejecutarEliminarInformacionNomina(@PathVariable String idInformacionNomina, @PathVariable String idUsuario){
        ObjetoRespuesta<InformacionNomina> informacionNominaObjetoRespuesta = manejadorEliminarInformacionNomina.ejecutar(idInformacionNomina, idUsuario);
        return new ObjetoRespuesta<InformacionNomina>(HttpStatus.OK, informacionNominaObjetoRespuesta.getDato());
    }
}
