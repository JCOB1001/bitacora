package com.pragma.perfiles.informacion_nomina.dominio.repositorio;

import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.vacaciones.common.servicio.CrudRepositorioBase;

import java.util.stream.Stream;

public interface InformacionNominaRepositorio extends CrudRepositorioBase<String,InformacionNomina> {
    Stream<InformacionNomina> findByUsuarioId(String usuarioId);
}
