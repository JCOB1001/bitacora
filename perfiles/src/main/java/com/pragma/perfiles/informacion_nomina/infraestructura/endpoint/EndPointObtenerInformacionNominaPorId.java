package com.pragma.perfiles.informacion_nomina.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;
import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionNomina;
import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorObtenerInformacionFamiliarPorId;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_nomina.aplicacion.manejador.ManejadorObtenerInformacionNominaPorId;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("informacion-nomina")
@RequiredArgsConstructor
@Validated
public class EndPointObtenerInformacionNominaPorId {
    private final ManejadorObtenerInformacionNominaPorId manejadorObtenerInformacionNominaPorId;

    @GetMapping("/{idInformacionNomina}")
    public ObjetoRespuesta<InformacionNomina> ejecutarObtenerInformacionNominaPorId(@NotNull @PathVariable String idInformacionNomina){
        ObjetoRespuesta<InformacionNomina> informacionNominaObjetoRespuesta = manejadorObtenerInformacionNominaPorId.ejecutar(idInformacionNomina);
        if(informacionNominaObjetoRespuesta.getDato() == null){
            throw new ExcepcionesInformacionNomina("No existe esta informacion nomina");
        }
        return new ObjetoRespuesta<InformacionNomina>(HttpStatus.OK, informacionNominaObjetoRespuesta.getDato());
    }
}
