package com.pragma.perfiles.informacion_nomina.dominio.useCase;

import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.repositorio.InformacionNominaRepositorio;
import com.pragma.vacaciones.common.useCase.CrudUseCaseCommon;

import java.util.stream.Stream;

public class InformacionNominaCrudUseCase extends CrudUseCaseCommon<InformacionNominaRepositorio, InformacionNomina, String> {

    private final InformacionNominaRepositorio informacionNominaRepositorio;

    public InformacionNominaCrudUseCase(InformacionNominaRepositorio informacionNominaRepositorio) {
        super(informacionNominaRepositorio);
        this.informacionNominaRepositorio=informacionNominaRepositorio;
    }

    public Stream<InformacionNomina> findByUsuarioId(String usuarioId){
        return informacionNominaRepositorio.findByUsuarioId(usuarioId);
    }
}
