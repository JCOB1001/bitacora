package com.pragma.perfiles.informacion_nomina.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;
import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionNomina;
import com.pragma.perfiles.informacion_familiar.aplicacion.comando.ActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_nomina.aplicacion.comando.ActualizarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.manejador.ManejadorActualizarInformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("informacion-nomina")
@RequiredArgsConstructor
@Validated
public class EndPointActualizarInformacionNomina {

    private final ManejadorActualizarInformacionNomina manejadorActualizarInformacionNomina;

    @PutMapping("/{idInformacionNomina}")
    public ObjetoRespuesta<InformacionNomina> ejecutarGuardarInformacionNomina(@NotNull @PathVariable String idInformacionNomina,
                                                                                 @NotNull @RequestBody ActualizarInformacionNominaComando
                                                                                           actualizarInformacionNominaComando){
        ObjetoRespuesta<InformacionNomina> informacionNominaObjetoRespuesta = manejadorActualizarInformacionNomina.ejecutar(idInformacionNomina,
                actualizarInformacionNominaComando);
        if(informacionNominaObjetoRespuesta.getDato() == null){
            throw new ExcepcionesInformacionNomina("No existe esta Informacion Nomina");
        }
        return new ObjetoRespuesta<InformacionNomina>(HttpStatus.OK, informacionNominaObjetoRespuesta.getDato());
    }
}
