package com.pragma.perfiles.informacion_nomina.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_nomina.aplicacion.comando.ActualizarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.FabricaInformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorActualizarInformacionNomina {

    private final InformacionNominaCrudUseCase informacionNominaCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;
    private final FabricaInformacionNomina fabricaInformacionNomina;

    public ObjetoRespuesta<InformacionNomina> ejecutar(String idInformacionNomina , ActualizarInformacionNominaComando actualizarInformacionNominaComando){
        InformacionNomina informacionNomina = informacionNominaCrudUseCase.obtenerPorId(idInformacionNomina);
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(actualizarInformacionNominaComando.getIdUsuario());

        if(usuario==null){
            throw new UsuarioNoEncontrado("El usuario con el ID: "+actualizarInformacionNominaComando.getIdUsuario()+" no existe");
        }

        if(informacionNomina != null){
            InformacionNomina informacionNominaNuevo = fabricaInformacionNomina.actualizarInformacionNomina(actualizarInformacionNominaComando);
            informacionNominaNuevo.setId(idInformacionNomina);
            informacionNominaCrudUseCase.actualizar(informacionNominaNuevo);
            informacionNomina = informacionNominaNuevo;
        }

        return new ObjetoRespuesta<InformacionNomina>(informacionNomina);
    }
}
