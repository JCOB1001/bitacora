package com.pragma.perfiles.informacion_nomina.infraestructura.persistencia.dao;


import com.pragma.perfiles.informacion_nomina.infraestructura.persistencia.entidad.InformacionNominaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InformacionNominaDao extends JpaRepository<InformacionNominaEntity,String> {
     Iterable<InformacionNominaEntity> findByUsuarioId(String usuarioId);
}
