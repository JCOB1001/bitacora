package com.pragma.perfiles.informacion_nomina.infraestructura.endpoint;

import com.pragma.perfiles.informacion_familiar.aplicacion.comando.GuardarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorGuardarInformacionFamiliar;
import com.pragma.perfiles.informacion_nomina.aplicacion.comando.GuardarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.manejador.ManejadorGuardarInformacionNomina;
import com.pragma.vacaciones.common.respuesta.RespuestaBase;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("informacion-nomina")
@RequiredArgsConstructor
@Validated
public class EndPointGuardarInformacionNomina {

    private final ManejadorGuardarInformacionNomina manejadorGuardarInformacionNomina;

    @PostMapping
    public RespuestaBase ejecutarGuardarInformacionNomina(@NotNull @RequestBody GuardarInformacionNominaComando guardarInformacionNominaComando){
        manejadorGuardarInformacionNomina.ejecutar(guardarInformacionNominaComando);
        return RespuestaBase.ok("Guardado Exitosamente");
    }
}
