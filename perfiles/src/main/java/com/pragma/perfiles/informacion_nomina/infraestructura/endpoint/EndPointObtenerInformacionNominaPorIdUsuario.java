package com.pragma.perfiles.informacion_nomina.infraestructura.endpoint;

import com.pragma.perfiles.informacion_familiar.aplicacion.manejador.ManejadorObtenerInformacionFamiliarPorIdUsuario;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_nomina.aplicacion.manejador.ManejadorObtenerInformacionNominaPorIdUsuario;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@RestController
@RequestMapping("informacion-nomina")
@RequiredArgsConstructor
@Validated
public class EndPointObtenerInformacionNominaPorIdUsuario {
    private final ManejadorObtenerInformacionNominaPorIdUsuario manejadorObtenerInformacionNominaPorIdUsuario;

    @GetMapping("/idUsuario/{idUsuario}")
    public ObjetoRespuesta<Stream<InformacionNomina>> obtenerInformacionNominaListPorIdUsuario(@NotNull @PathVariable String idUsuario){
        Stream<InformacionNomina> informacionNominaStream = manejadorObtenerInformacionNominaPorIdUsuario.ejecutar(idUsuario);
        return new ObjetoRespuesta<>(HttpStatus.OK, informacionNominaStream);
    }
}
