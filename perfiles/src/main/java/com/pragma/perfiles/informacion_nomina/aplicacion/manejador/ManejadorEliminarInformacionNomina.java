package com.pragma.perfiles.informacion_nomina.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionNomina;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorEliminarInformacionNomina {
    private final InformacionNominaCrudUseCase informacionNominaCrudUseCase;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public ObjetoRespuesta<InformacionNomina> ejecutar(String idInformacionNomina, String idUsuario){
        InformacionNomina informacionNomina = informacionNominaCrudUseCase.obtenerPorId(idInformacionNomina);
        Usuario usuario=usuarioCrudUseCase.obtenerPorId(idUsuario);
        if(usuario==null){
            throw  new UsuarioNoEncontrado("El usuario con el ID: "+idUsuario+" no existe");
        }

        if(informacionNomina == null){
            throw new ExcepcionesInformacionNomina("No existe esta información de nómina");
        }

        informacionNominaCrudUseCase.eliminarPorId(idInformacionNomina);
        return new ObjetoRespuesta<InformacionNomina>(informacionNomina);
    }

}
