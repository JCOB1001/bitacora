package com.pragma.perfiles.informacion_nomina.infraestructura.persistencia.builder;

import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.infraestructura.persistencia.entidad.InformacionNominaEntity;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface InformacionNominaMapperInformacionNominaEntity extends ConvertidorBase<InformacionNomina, InformacionNominaEntity> {
    @Named("leftToRight")
    @Override
    @Mappings(
            @Mapping(source = "idUsuario", target = "usuario.id")
    )
    InformacionNominaEntity leftToRight(InformacionNomina estudio);

    @Named("rightToLeft")
    @Override
    @Mappings({
            @Mapping(source = "usuario.id", target = "idUsuario")
    })
    InformacionNomina rightToLeft(InformacionNominaEntity estudioEntity);


}
