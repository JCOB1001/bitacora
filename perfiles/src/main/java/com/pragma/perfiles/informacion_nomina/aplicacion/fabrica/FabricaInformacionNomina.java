package com.pragma.perfiles.informacion_nomina.aplicacion.fabrica;


import com.pragma.perfiles.informacion_familiar.aplicacion.fabrica.impl.InformacionFamiliarMapperActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.comando.ActualizarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.comando.GuardarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.impl.InformacionNominaMapperActualizarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.impl.InformacionNominaMapperGuardarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FabricaInformacionNomina {

    private final InformacionNominaMapperGuardarInformacionNominaComando informacionNominaMapperGuardarInformacionNominaComando;
    private final InformacionNominaMapperActualizarInformacionNominaComando informacionNominaMapperActualizarInformacionNominaComando;

    public InformacionNomina guardarInformacionNomina (GuardarInformacionNominaComando guardarInformacionNominaComando){
        return informacionNominaMapperGuardarInformacionNominaComando.rightToLeft(guardarInformacionNominaComando);
    }

    public InformacionNomina actualizarInformacionNomina (ActualizarInformacionNominaComando actualizarInformacionNominaComando){
        return informacionNominaMapperActualizarInformacionNominaComando.rightToLeft(actualizarInformacionNominaComando);
    }
}
