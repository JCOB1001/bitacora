package com.pragma.perfiles.informacion_nomina.dominio.respuesta;

import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;

import java.util.stream.Stream;

public class ObjetoRespuestaInformacionNomina extends ObjetoRespuesta {
    public ObjetoRespuestaInformacionNomina(Stream<InformacionNomina> dato){
        super(dato);
    }
}
