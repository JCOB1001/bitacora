package com.pragma.perfiles.informacion_nomina.aplicacion.manejador;

import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerInformacionNominaPorIdUsuario {
    private final InformacionNominaCrudUseCase informacionNominaCrudUseCase;

    public Stream<InformacionNomina> ejecutar(String idUsuario){
        Stream<InformacionNomina> informacionNominaStream = informacionNominaCrudUseCase.findByUsuarioId(idUsuario);
        return informacionNominaStream;
    }
}
