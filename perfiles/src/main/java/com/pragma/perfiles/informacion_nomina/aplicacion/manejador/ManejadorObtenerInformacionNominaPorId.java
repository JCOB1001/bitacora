package com.pragma.perfiles.informacion_nomina.aplicacion.manejador;

import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerInformacionNominaPorId {
    private final InformacionNominaCrudUseCase informacionNominaCrudUseCase;

    public ObjetoRespuesta<InformacionNomina> ejecutar(String idInformacionNomina){
        InformacionNomina informacionNomina = informacionNominaCrudUseCase.obtenerPorId(idInformacionNomina);
        return new ObjetoRespuesta<InformacionNomina>(informacionNomina);

    }
}
