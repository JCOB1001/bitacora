package com.pragma.perfiles.informacion_nomina.aplicacion.manejador;

import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.informacion_nomina.aplicacion.comando.GuardarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.FabricaInformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ManejadorGuardarInformacionNomina {
    private final InformacionNominaCrudUseCase informacionNominaCrudUseCase;
    private final FabricaInformacionNomina fabricaInformacionNomina;
    private final UsuarioCrudUseCase usuarioCrudUseCase;

    public void ejecutar(GuardarInformacionNominaComando guardarInformacionNominaComando){
        Usuario usuario= usuarioCrudUseCase.obtenerPorId(guardarInformacionNominaComando.getIdUsuario());

        if(usuario==null){
            throw new UsuarioNoEncontrado("No se encontro el usuario con el ID: "+guardarInformacionNominaComando.getIdUsuario());
        }
        InformacionNomina informacionNomina = fabricaInformacionNomina.guardarInformacionNomina(guardarInformacionNominaComando);
        InformacionNomina informacionNominaNuevo = informacionNominaCrudUseCase.guardar(informacionNomina);

    }
}
