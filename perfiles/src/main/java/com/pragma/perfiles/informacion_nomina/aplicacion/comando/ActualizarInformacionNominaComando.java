package com.pragma.perfiles.informacion_nomina.aplicacion.comando;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarInformacionNominaComando {
    private String idUsuario;
    private String cargoNomina;
    private Double salarioBasicoOrdinario;
    private Double salarioBasicoIntegral;
    private Double noConstitutivosSalario;
    private String moneda;
}
