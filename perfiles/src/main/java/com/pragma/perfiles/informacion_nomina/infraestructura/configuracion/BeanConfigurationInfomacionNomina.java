package com.pragma.perfiles.informacion_nomina.infraestructura.configuracion;

import com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.FabricaInformacionNomina;
import com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.impl.InformacionNominaMapperActualizarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.impl.InformacionNominaMapperGuardarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.manejador.*;
import com.pragma.perfiles.informacion_nomina.dominio.repositorio.InformacionNominaRepositorio;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfigurationInfomacionNomina {

    @Bean
    public ManejadorGuardarInformacionNomina manejadorGuardarInformacionNomina(InformacionNominaCrudUseCase informacionNominaCrudUseCase,
                                                                               FabricaInformacionNomina fabricaInformacionNomina,
                                                                               UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorGuardarInformacionNomina(informacionNominaCrudUseCase, fabricaInformacionNomina,
                usuarioCrudUseCase);
    }

    @Bean
    public ManejadorActualizarInformacionNomina manejadorActualizarInformacionNomina(InformacionNominaCrudUseCase informacionNominaCrudUseCase,
                                                                                     FabricaInformacionNomina fabricaInformacionNomina,
                                                                                       UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorActualizarInformacionNomina(informacionNominaCrudUseCase, usuarioCrudUseCase,
                fabricaInformacionNomina);
    }

    @Bean
    public ManejadorEliminarInformacionNomina manejadorEliminarInformacionNomina(InformacionNominaCrudUseCase informacionNominaCrudUseCase,
                                                                                   UsuarioCrudUseCase usuarioCrudUseCase){
        return new ManejadorEliminarInformacionNomina(informacionNominaCrudUseCase,usuarioCrudUseCase);
    }

    @Bean
    public ManejadorObtenerInformacionNominaPorId manejadorObtenerInformacionNominaPorId(InformacionNominaCrudUseCase informacionNominaCrudUseCase){
        return new ManejadorObtenerInformacionNominaPorId(informacionNominaCrudUseCase);
    }

    @Bean
    public ManejadorObtenerInformacionNominaList manejadorObtenerInformacionNominaList(InformacionNominaCrudUseCase informacionNominaCrudUseCase){
        return new ManejadorObtenerInformacionNominaList(informacionNominaCrudUseCase);
    }

    @Bean
    public InformacionNominaCrudUseCase informacionNominaCrudUseCase(InformacionNominaRepositorio informacionNominaRepositorio){
        return new InformacionNominaCrudUseCase(informacionNominaRepositorio);
    }

    @Bean
    public ManejadorObtenerInformacionNominaPorIdUsuario manejadorObtenerInformacionNominaPorIdUsuario(InformacionNominaCrudUseCase informacionNominaCrudUseCase){
        return new ManejadorObtenerInformacionNominaPorIdUsuario(informacionNominaCrudUseCase);
    }

    @Bean
    public FabricaInformacionNomina fabricaInformacionNomina(
            InformacionNominaMapperGuardarInformacionNominaComando informacionNominaMapperGuardarInformacionNominaComando,
            InformacionNominaMapperActualizarInformacionNominaComando informacionNominaMapperActualizarInformacionNominaComando){
        return new FabricaInformacionNomina(informacionNominaMapperGuardarInformacionNominaComando, informacionNominaMapperActualizarInformacionNominaComando);
    }
}
