package com.pragma.perfiles.informacion_nomina.dominio.modelo;

import com.pragma.vacaciones.common.persistencia.Modelo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InformacionNomina extends Modelo<String> {
    private String idUsuario;
    private String cargoNomina;
    private Double salarioBasicoOrdinario;
    private Double salarioBasicoIntegral;
    private Double noConstitutivosSalario;
    private String moneda;

    public InformacionNomina() {
        super(null);
    }

    public InformacionNomina(String id, String idUsuario, String cargoNomina, Double salarioBasicoOrdinario, Double salarioBasicoIntegral, Double noConstitutivosSalario, String moneda) {
        super(id);
        this.idUsuario = idUsuario;
        this.cargoNomina = cargoNomina;
        this.salarioBasicoOrdinario = salarioBasicoOrdinario;
        this.salarioBasicoIntegral = salarioBasicoIntegral;
        this.noConstitutivosSalario = noConstitutivosSalario;
        this.moneda = moneda;
    }
}
