package com.pragma.perfiles.informacion_nomina.infraestructura.persistencia.repositorio;


import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.repositorio.InformacionNominaRepositorio;
import com.pragma.perfiles.informacion_nomina.infraestructura.persistencia.builder.InformacionNominaMapperInformacionNominaEntity;
import com.pragma.perfiles.informacion_nomina.infraestructura.persistencia.dao.InformacionNominaDao;
import com.pragma.perfiles.informacion_nomina.infraestructura.persistencia.entidad.InformacionNominaEntity;
import com.pragma.vacaciones.common.servicio.impl.CrudRepositorioBaseImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
@RequiredArgsConstructor
public class InformacionNominaRepositorioImpl extends CrudRepositorioBaseImpl<String, InformacionNomina, InformacionNominaEntity> implements InformacionNominaRepositorio {

    private final InformacionNominaDao informacionNominaDao;
    private final InformacionNominaMapperInformacionNominaEntity informacionNominaMapperInformacionNominaEntity;

    @Override
    protected InformacionNominaDao obtenerRepositorio() {
        return informacionNominaDao;
    }

    @Override
    protected InformacionNominaMapperInformacionNominaEntity obtenerConversionBase(){
        return informacionNominaMapperInformacionNominaEntity;
    }

    @Override
    public Stream<InformacionNomina> findByUsuarioId(String usuarioId) {
        return informacionNominaMapperInformacionNominaEntity
                .rightToLeft(informacionNominaDao.findByUsuarioId(usuarioId));
    }
}
