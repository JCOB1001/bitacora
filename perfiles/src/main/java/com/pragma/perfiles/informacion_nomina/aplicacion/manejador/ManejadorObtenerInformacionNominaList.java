package com.pragma.perfiles.informacion_nomina.aplicacion.manejador;

import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.informacion_nomina.dominio.useCase.InformacionNominaCrudUseCase;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
public class ManejadorObtenerInformacionNominaList {
    private final InformacionNominaCrudUseCase informacionNominaCrudUseCase;

    public ObjetoRespuesta<Stream<InformacionNomina>> ejecutar(){
        Stream<InformacionNomina> informacionNominaStream = informacionNominaCrudUseCase.obtenerTodos();
        return new ObjetoRespuesta<Stream<InformacionNomina>>(informacionNominaStream);
    }
}
