package com.pragma.perfiles.informacion_nomina.infraestructura.endpoint;

import com.pragma.perfiles.comun.infraestructura.error.ExcepcionesInformacionFamiliar;
import com.pragma.perfiles.informacion_nomina.aplicacion.manejador.ManejadorObtenerInformacionNominaList;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RestController
@RequestMapping("informacion-nomina")
@RequiredArgsConstructor
@Validated
public class EndPointObtenerInformacionNominaList {

    private final ManejadorObtenerInformacionNominaList manejadorObtenerInformacionNominaList;

    @GetMapping
    public ObjetoRespuesta<Stream<InformacionNomina>> ejecutarObtenerInformacionNominaList(){
        ObjetoRespuesta<Stream<InformacionNomina>> informacionNominaList = manejadorObtenerInformacionNominaList.ejecutar();
        if(informacionNominaList.getDato() == null){
            throw new ExcepcionesInformacionFamiliar("No existen registro de información nomina");
        }
        return new ObjetoRespuesta<Stream<InformacionNomina>>(HttpStatus.OK, informacionNominaList.getDato());
    }
}
