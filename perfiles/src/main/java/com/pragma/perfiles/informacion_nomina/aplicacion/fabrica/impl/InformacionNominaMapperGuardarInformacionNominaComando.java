package com.pragma.perfiles.informacion_nomina.aplicacion.fabrica.impl;

import com.pragma.perfiles.informacion_nomina.aplicacion.comando.GuardarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.vacaciones.common.mapper.ConvertidorBase;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface InformacionNominaMapperGuardarInformacionNominaComando extends ConvertidorBase<InformacionNomina, GuardarInformacionNominaComando> {
}
