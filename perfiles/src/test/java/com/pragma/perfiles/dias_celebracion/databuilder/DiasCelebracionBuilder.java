package com.pragma.perfiles.dias_celebracion.databuilder;

import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasCelebracion;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class DiasCelebracionBuilder {

    private final String ID_DIA = "123";
    private final String NOMBRE_DIA = "123";

    private String id;
    private String nombre;

    public DiasCelebracionBuilder(){
        this.id = ID_DIA;
        this.nombre = NOMBRE_DIA;
    }

    public Stream<DiasCelebracion> createObjetoRespuestaDiaCelebracion() {

        List<DiasCelebracion> diasCelebracionList = new ArrayList<>();
        DiasCelebracion diasCelebracion = new DiasCelebracion();
        diasCelebracion.setId(id);
        diasCelebracion.setNombre(nombre);
        diasCelebracionList.add(diasCelebracion);
        return diasCelebracionList.stream();
    }
}
