package com.pragma.perfiles.dias_celebracion.databuilder;

import com.pragma.perfiles.dias_celebracion.aplicacion.comando.GuardarUsuarioDiasNoCelebrablesComando;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasCelebracion;
import com.pragma.perfiles.dias_celebracion.dominio.modelo.DiasNoCelebrablesUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.EstadoProfesion;
import com.pragma.perfiles.perfil.dominio.modelo.Fotografia;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstadoCivil;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGenero;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGrupoSanguineo;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioTallaCamisa;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class DiasNoCelebrablesBuilder {

    private static final String ID_DIA = "123";
    private static final String NOMBRE_DIA = "NAVIDAD";

    public static final String CORREO_EMPRESARIAL = "cristianc.munozm@pragma.com.co";
    public static final String IDENTIFICACION = "123";
    public static final String NOMBRES = "PEDRO";
    public static final String APELLIDOS = "PEREZ";
    public static final String ID_VICEPRESIDENCIA = "1";
    public static final String CLIENTE = "PRUEBA";
    public static final LocalDate FECHA_INGRESO = LocalDate.of(2021, 7, 15);
    public static final double DIAS_GANADOS = 1.0;
    public static final int DIAS_TOMADOS = 1;
    public static final String GOOGLE_ID = "";
    public static final String CORREO_PERSONAL = "";
    public static final String TELEFONO_FIJO = "";
    public static final String TELEFONO_CELULAR = "";
    public static final String ID_NACIONALIDAD = "";
    public static final String ID_LUGAR_NACIMIENTO = "";
    public static final String ID_LUGAR_RECIDENCIA = "";
    public static final LocalDate FECHA_NACIMIENTO = LocalDate.of(1995, 7, 15);
    public static final String DIRECCION = "";
    public static final String NOMBRE_CONTACTO_EMERGENCIA = "";
    public static final String PARENTESCO_CONTACTO_EMERGENCIA = "";
    public static final String TELEFONO_CONTACTO_EMERGENCIA = "";
    public static final String TARJETA_PROFESIONAL = "";
    public static final String PERFIL_PROFESIONAL = "";
    public static final String CONOCIMIENTOS_TECNICOS = "";
    public static final UsuarioTallaCamisa TALLA_CAMISA = UsuarioTallaCamisa.L;
    public static final UsuarioEstadoCivil ESTADO_CIVIL = UsuarioEstadoCivil.CASADO_A;
    public static final UsuarioGenero GENERO = UsuarioGenero.MASCULINO;
    public static final UsuarioGrupoSanguineo GRUPO_SANGUINEO = UsuarioGrupoSanguineo.A_NEGATIVO;
    public static final Long ID_TIPODOCUMENTO = 1L;
    public static final Long ID_PROFESION = 1L;
    public static final LocalDate FECHA_CREACION = LocalDate.of(2021, 7, 15);
    public static final LocalDate FECHA_ULTIMA_ACTUALIZACION = LocalDate.of(2021, 7, 15);
    public static final String CARGO = "Desarrollador";
    public static final List<Long> CAPACIDAD = new ArrayList<>();
    public static final List<Fotografia> FOTOGRAFIAS = new ArrayList<>();
    public static final boolean ACEPTACION_TERMINOS = false;
    public static final double BARRA_PROGRESO = 0.9678;
    public static final String NOMBRE_PREFERENCIA = "Santiago";
    public static final String PASAPORTE = "AB2022A458S";
    public static final int ESTRATO = 1;
    public static final String SEMESTRE = "1";
    public static final EstadoProfesion ESTADO_PROFESION = EstadoProfesion.ESTUDIANDO;
    public static final String TIPO_CONTRATO_ID = "1";
    public static final LocalDate FECHA_INICIO_CONTRATO = LocalDate.of(2021, 7, 15);
    public static final String EMPRESA = "Pragma";
    public static final String CONTRASENIA_COMPUTADOR = "Induccion2022+";

    public DiasNoCelebrablesUsuario buildObjetoDiasNoCelebrablesUsuario() {
        DiasNoCelebrablesUsuario diasNoCelebrablesUsuario = new DiasNoCelebrablesUsuario();
        diasNoCelebrablesUsuario.setId(ID_DIA);
        diasNoCelebrablesUsuario.setUsuarioId(ID_DIA);
        diasNoCelebrablesUsuario.setDiaCelebracionId(ID_DIA);
        return diasNoCelebrablesUsuario;
    }


    public List<DiasNoCelebrablesUsuario> buildListObjetoDiasNoCelebrablesUsuario() {
        ArrayList<DiasNoCelebrablesUsuario> diasNoCelebrablesList = new ArrayList<>();
        diasNoCelebrablesList.add(buildObjetoDiasNoCelebrablesUsuario());
        diasNoCelebrablesList.add(buildObjetoDiasNoCelebrablesUsuario());
        return diasNoCelebrablesList;
    }


    public Usuario buildUsuario(String id) {
        return new Usuario(id,
                CORREO_EMPRESARIAL,
                IDENTIFICACION,
                NOMBRES,
                APELLIDOS,
                CLIENTE,
                FECHA_INGRESO,
                DIAS_GANADOS,
                DIAS_TOMADOS,
                GOOGLE_ID,
                CORREO_PERSONAL,
                TELEFONO_FIJO,
                TELEFONO_CELULAR,
                ID_NACIONALIDAD,
                ID_LUGAR_NACIMIENTO,
                ID_LUGAR_RECIDENCIA,
                FECHA_NACIMIENTO,
                DIRECCION,
                NOMBRE_CONTACTO_EMERGENCIA,
                PARENTESCO_CONTACTO_EMERGENCIA,
                TELEFONO_CONTACTO_EMERGENCIA,
                TARJETA_PROFESIONAL,
                PERFIL_PROFESIONAL,
                CONOCIMIENTOS_TECNICOS,
                TALLA_CAMISA,
                ESTADO_CIVIL,
                GENERO,
                GRUPO_SANGUINEO,
                ID_TIPODOCUMENTO,
                ID_PROFESION,
                FECHA_CREACION,
                FECHA_ULTIMA_ACTUALIZACION,
                CARGO,
                ID_VICEPRESIDENCIA,
                CAPACIDAD,
                FOTOGRAFIAS,
                ACEPTACION_TERMINOS,
                BARRA_PROGRESO,
                NOMBRE_PREFERENCIA,
                PASAPORTE,
                ESTRATO,
                SEMESTRE,
                ESTADO_PROFESION,
                TIPO_CONTRATO_ID,
                FECHA_INICIO_CONTRATO,
                EMPRESA,
                CONTRASENIA_COMPUTADOR
        );
    }

    public DiasCelebracion createObjetoRespuestaDiasCelebracion() {
        DiasCelebracion dia = new DiasCelebracion();
        dia.setId(ID_DIA);
        dia.setNombre(NOMBRE_DIA);
        return dia;
    }

    public GuardarUsuarioDiasNoCelebrablesComando createGuardarUsuarioDiasNoCelebrablesComando(String usuarioId, String diaCelebracionId) {
        return new GuardarUsuarioDiasNoCelebrablesComando(usuarioId, diaCelebracionId);
    }

}
