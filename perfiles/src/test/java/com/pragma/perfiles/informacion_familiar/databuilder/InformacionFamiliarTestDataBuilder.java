package com.pragma.perfiles.informacion_familiar.databuilder;

import com.pragma.perfiles.informacion_familiar.aplicacion.comando.ActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.aplicacion.comando.GuardarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.Genero;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;

import java.time.LocalDate;

public class InformacionFamiliarTestDataBuilder {

    private static final String ID = "4";
    private static final String NOMBRES = "Diego";
    private static final String APELLIDOS = "Lopez";
    private static final Genero GENERO = Genero.MASCULINO;
    private static final LocalDate FECHA_NACIMIENTO = LocalDate.now();
    private static final String ID_USUARIO = "01234567892";
    private static final String ID_USUARIO_INEXISTENTE = "12";
    private static final Parentesco PARENTESCO = new Parentesco(2L, "Padre");
    private static final boolean DEPENDIENTE = true;

    private String id;
    private String nombres;
    private String apellidos;
    private Genero genero;
    private LocalDate fechaNacimiento;
    private String idUsuario;
    private Parentesco parentesco;
    private boolean dependiente;

    public InformacionFamiliarTestDataBuilder() {
        this.id = ID;
        this.nombres = NOMBRES;
        this.apellidos = APELLIDOS;
        this.genero = GENERO;
        this.fechaNacimiento = FECHA_NACIMIENTO;
        this.idUsuario = ID_USUARIO;
        this.parentesco = PARENTESCO;
        this.dependiente = DEPENDIENTE;
    }

    public InformacionFamiliar buildInformacionFamiliar() {
        return new InformacionFamiliar(id, nombres, apellidos, fechaNacimiento, parentesco, genero, idUsuario, dependiente);
    }

    public GuardarInformacionFamiliarComando buildGuardarInformacionFamiliarComando() {
        return new GuardarInformacionFamiliarComando(nombres, apellidos, genero, fechaNacimiento, idUsuario, parentesco.getId(), dependiente);
    }

    public GuardarInformacionFamiliarComando buildGuardarInformacionFamiliarComandoIdUsuarioNoExistente() {
        this.idUsuario = ID_USUARIO_INEXISTENTE;
        return new GuardarInformacionFamiliarComando(nombres, apellidos, genero, fechaNacimiento, idUsuario, parentesco.getId(), dependiente);
    }

    public ActualizarInformacionFamiliarComando buildActualizarInformacionFamiliarComando() {
        return new ActualizarInformacionFamiliarComando(nombres, apellidos, genero, fechaNacimiento, idUsuario, parentesco.getId(), dependiente);
    }

    public ActualizarInformacionFamiliarComando buildActualizarInformacionFamiliarComandoIdUsuarioNoExistente() {
        this.idUsuario = ID_USUARIO_INEXISTENTE;
        return new ActualizarInformacionFamiliarComando(nombres, apellidos, genero, fechaNacimiento, idUsuario, parentesco.getId(), dependiente);
    }

}
