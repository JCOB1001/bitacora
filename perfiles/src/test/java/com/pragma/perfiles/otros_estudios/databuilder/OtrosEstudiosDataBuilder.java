package com.pragma.perfiles.otros_estudios.databuilder;

import com.pragma.perfiles.otros_estudios.aplicacion.comando.ActualizarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarOtrosEstudiosComando;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;

import java.time.LocalDate;
import java.util.List;

public class OtrosEstudiosDataBuilder {

    private final String ID = "1111";
    private final String ID_USUARIO = "111839089495784854974";
    private final LocalDate FECHA_EXPIRACION = LocalDate.of(2020, 07, 15);
    private final Double HORAS = Double.valueOf(20);
    private final String ETIQUETAS = "Back,Agilismo,Aws";
    private final String TITULO = "Ingeniero de Sistemas";
    private final String ANIO = "2020";
    private final String INSTITUTO = "Uni piloto";
    private final String URL_CERTIFICADO = "http:...";
    private final String ARCHIVO="dsfsdfdsfsdfsdfsdffgfhg";
    private final String EXTENSION=".png";


    private String id;
    private String idUsuario;
    private LocalDate fechaExpiracion;
    private Double horas;
    private String etiquetas;
    private String titulo;
    private String anio;
    private String instituto;
    private String urlCertificado;
    private String archivo;
    private String extension;

    public OtrosEstudiosDataBuilder(){
        this.id = ID;
        this.idUsuario = ID_USUARIO;
        this.fechaExpiracion = FECHA_EXPIRACION;
        this.horas = HORAS;
        this.etiquetas = ETIQUETAS;
        this.titulo = TITULO;
        this.anio = ANIO;
        this.instituto = INSTITUTO;
        this.urlCertificado = URL_CERTIFICADO;
        this.archivo=ARCHIVO;
        this.extension=EXTENSION;
    }

    public OtrosEstudios buildOtrosEstudios(){
        return new OtrosEstudios(
                this.id,
                this.idUsuario,
                this.fechaExpiracion,
                this.horas,
                this.etiquetas,
                this.titulo,
                this.anio,
                this.instituto,
                this.urlCertificado
        );
    }

    public GuardarOtrosEstudiosComando buildGuardarOtrosEstudiosComando(){
        return new GuardarOtrosEstudiosComando(
                this.idUsuario,
                this.fechaExpiracion,
                this.horas,
                this.etiquetas,
                this.titulo,
                this.anio,
                this.instituto,
                this.archivo,
                this.extension
        );
    }

    public GuardarOtrosEstudiosComando buildGuardarOtrosEstudiosComandoUsuarioNoExistente(){
        return new GuardarOtrosEstudiosComando(
                "11111111-000000",
                this.fechaExpiracion,
                this.horas,
                this.etiquetas,
                this.titulo,
                this.anio,
                this.instituto,
                this.archivo,
                this.extension
        );
    }

    public ActualizarOtrosEstudiosComando buildActualizarOtrosEstudiosComando(){
        return new ActualizarOtrosEstudiosComando(
                this.idUsuario,
                this.fechaExpiracion,
                this.horas,
                "Back,Front,Agilismo,AWS,DevOps",
                "Ingeniero Electronico",
                this.anio,
                this.instituto,
                this.archivo,
                this.extension
        );
    }

    public ActualizarOtrosEstudiosComando buildActualizarOtrosEstudiosComandoUsuarioNoExistente(){
        return new ActualizarOtrosEstudiosComando(
                "11111111-000000",
                this.fechaExpiracion,
                this.horas,
                "Back,Front,Agilismo,AWS,DevOps",
                "Ingeniero Electronico",
                this.anio,
                this.instituto,
                this.archivo,
                this.extension
        );
    }



}
