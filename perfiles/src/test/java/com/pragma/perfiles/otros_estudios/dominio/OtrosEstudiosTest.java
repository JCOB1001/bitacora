package com.pragma.perfiles.otros_estudios.dominio;

import com.pragma.perfiles.otros_estudios.databuilder.OtrosEstudiosDataBuilder;
import com.pragma.perfiles.otros_estudios.dominio.modelo.OtrosEstudios;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class OtrosEstudiosTest {

    private final String ID = "1111";
    private final String ID_USUARIO = "111839089495784854974";
    private final LocalDate FECHA_EXPIRACION = LocalDate.of(2020, 07, 15);
    private final Double HORAS = Double.valueOf(20);
    private final String ETIQUETAS = "Back,Agilismo,Aws";
    private final String TITULO = "Ingeniero de Sistemas";
    private final String ANIO = "2020";
    private final String INSTITUTO = "Uni piloto";
    private final String URL_CERTIFICADO = "http:...";

    @Test
    public void testCrearModeloOtrosEstudios(){
        OtrosEstudiosDataBuilder otrosEstudiosDataBuilder = new OtrosEstudiosDataBuilder();
        OtrosEstudios otrosEstudios = otrosEstudiosDataBuilder.buildOtrosEstudios();
        assertEquals(ID, otrosEstudios.getId());
        assertEquals(ID_USUARIO, otrosEstudios.getIdUsuario());
        assertEquals(FECHA_EXPIRACION, otrosEstudios.getFechaExpiracion());
        assertEquals(HORAS, otrosEstudios.getHoras());
        assertEquals(ETIQUETAS, otrosEstudios.getEtiquetas());
        assertEquals(TITULO, otrosEstudios.getTitulo());
        assertEquals(ANIO, otrosEstudios.getAnio());
        assertEquals(INSTITUTO, otrosEstudios.getInstituto());
        assertEquals(URL_CERTIFICADO, otrosEstudios.getUrlCertificado());
    }
    
}
