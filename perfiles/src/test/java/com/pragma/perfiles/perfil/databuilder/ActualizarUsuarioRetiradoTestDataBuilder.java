package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioRetiradoComando;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;

import java.time.LocalDate;

public class ActualizarUsuarioRetiradoTestDataBuilder {

    public static final String NOMBRE_TIPO_IDENTIFICACION = "Cédula de ciudadanía";
    public static final Long ID_TIPO_IDENTIFICACION = 55L;
    public static final String IDENTIFICACION = "1090521478";


    private String tipoIdentificacion;
    private Long idTipoIdentificacion;
    private String identificacion;

    public ActualizarUsuarioRetiradoTestDataBuilder(){
        this.tipoIdentificacion = NOMBRE_TIPO_IDENTIFICACION;
        this.idTipoIdentificacion = ID_TIPO_IDENTIFICACION;
        this.identificacion = IDENTIFICACION;

    }

    public ActualizarUsuarioRetiradoComando buildGuardarInfoContratoNominaRetiradoComando(){
        return new ActualizarUsuarioRetiradoComando(
                this.tipoIdentificacion,
                this.identificacion
        );
    }

    public Long getIdTipoIdentificacion() {
        return idTipoIdentificacion;
    }

    public HistoricoDeCambios buildhistoricoDeCambios(){
        return new HistoricoDeCambios("", "", LocalDate.now(), "");
    }
}
