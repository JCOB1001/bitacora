package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.informacion_familiar.aplicacion.comando.ActualizarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.Genero;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Data
public class ActualizarInformacionFamiliarComandoTestDataBuilder {

    public static final String ID_USUARIO_NO_EXISTENTE="01234567891234";
    public static final long ID_PARENTESCO_NO_EXISTENTE=45;
    private String nombres;
    private String apellidos;
    private Genero genero;
    private LocalDate fechaNacimiento;
    private String idUsuario;
    private Long idParentesco;
    private boolean dependiente;

    public ActualizarInformacionFamiliarComandoTestDataBuilder(){
        this.nombres="Eduard Jose";
        this.apellidos="Test test";
        this.fechaNacimiento=LocalDate.now();
        this.genero=Genero.MASCULINO;
        this.idUsuario="01234567891";
        this.idParentesco=4L;
        this.dependiente=true;
    }

    public ActualizarInformacionFamiliarComando buildActualizarInformacionFamiliarComando(){

        return new ActualizarInformacionFamiliarComando(nombres,apellidos,genero,fechaNacimiento,idUsuario,idParentesco,dependiente);
    }

    public ActualizarInformacionFamiliarComando buildActualizarInformacionFamiliarComandoConError(){

        return new ActualizarInformacionFamiliarComando(nombres,apellidos,Genero.MASCULINO,fechaNacimiento,ID_USUARIO_NO_EXISTENTE,ID_PARENTESCO_NO_EXISTENTE,dependiente);
    }
    public ActualizarInformacionFamiliarComando buildActualizarInformacionFamiliarComandoCoUsuarioNoExistente(){

        return new ActualizarInformacionFamiliarComando(nombres,apellidos,Genero.MASCULINO,fechaNacimiento,ID_USUARIO_NO_EXISTENTE,idParentesco,dependiente);
    }

    public ActualizarInformacionFamiliarComando buildActualizarInformacionFamiliarComandoConIdParentescoNoExistente(){

        return new ActualizarInformacionFamiliarComando(nombres,apellidos,Genero.MASCULINO,fechaNacimiento,idUsuario,ID_PARENTESCO_NO_EXISTENTE,dependiente);
    }
}
