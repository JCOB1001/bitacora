package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComando;
import com.pragma.perfiles.perfil.dominio.modelo.*;

import java.time.LocalDate;

public class ActualizarUsuarioComandoTestDataBuilder {

    public static final String NOMBRES = "PEDRO";
    public static final String GOOGLE_ID = "google_id";
    public static final String IDENTIFICACION = "123";
    public static final String APELLIDOS = "PEREZ";
    public static final String CORREO_PERSONAL = "cristianc.munozm@correo.com.co";
    public static final String TELEFONO_FIJO = "7432733";
    public static final String TELEFONO_CELULAR = "3103162780";
    public static final String ID_NACIONALIDAD = "1";
    public static final String ID_LUGAR_NACIMIENTO = "1";
    public static final String ID_LUGAR_RESIDENCIA = "1";
    public static final LocalDate FECHA_NACIMIENTO = LocalDate.of(1990, 07, 15);
    public static final String DIRECCION = "Barrio Jorge Eliecer Gaitan";
    public static final String NOMBRE_CONTACTO_EMERGENCIA = "Carlos";
    public static final String PARENTESCO_CONTACTO_EMERGENCIA = "Padre";
    public static final String TELEFONO_CONTACTO_EMERGENCIA = "3125878996";
    public static final UsuarioTallaCamisa TALLA_CAMISA = UsuarioTallaCamisa.M;
    public static final UsuarioEstadoCivil ESTADO_CIVIL = UsuarioEstadoCivil.CASADO_A;
    public static final UsuarioGenero GENERO = UsuarioGenero.MASCULINO;
    public static final UsuarioGrupoSanguineo GRUPO_SANGUINEO= UsuarioGrupoSanguineo.O_POSITIVO;
    public static final UsuarioEstado ESTADO = UsuarioEstado.ACTIVO;
    public static final Long ID_TIPO_DOCUMENTO = 1L;

    private String nombres;
    private String googleId;
    private String identificacion;
    private String apellidos;
    private String correoPersonal;
    private String telefonoFijo;
    private String telefonoCelular;
    private String idNacionalidad;
    private String idLugarNacimiento;
    private String idLugarRecidencia;
    private LocalDate fechaNacimiento;
    private String direccion;
    private String nombreContactoEmergencia;
    private String parentescoContactoEmergencia;
    private String telefonoContactoEmergencia;
    private UsuarioTallaCamisa tallaCamisa;
    private UsuarioEstadoCivil estadoCivil;
    private UsuarioGenero genero;
    private UsuarioGrupoSanguineo grupoSanguineo;
    private UsuarioEstado estado;
    private Long idTipoDocumento;

    public ActualizarUsuarioComandoTestDataBuilder() {
        this.nombres = NOMBRES;
        this.googleId = GOOGLE_ID;
        this.identificacion = IDENTIFICACION;
        this.apellidos = APELLIDOS;
        this.correoPersonal = CORREO_PERSONAL;
        this.telefonoFijo = TELEFONO_FIJO;
        this.telefonoCelular = TELEFONO_CELULAR;
        this.idNacionalidad = ID_NACIONALIDAD;
        this.idLugarNacimiento = ID_LUGAR_NACIMIENTO;
        this.idLugarRecidencia = ID_LUGAR_RESIDENCIA;
        this.fechaNacimiento = FECHA_NACIMIENTO;
        this.direccion = DIRECCION;
        this.nombreContactoEmergencia = NOMBRE_CONTACTO_EMERGENCIA;
        this.parentescoContactoEmergencia = PARENTESCO_CONTACTO_EMERGENCIA;
        this.telefonoContactoEmergencia = TELEFONO_CONTACTO_EMERGENCIA;
        this.tallaCamisa = TALLA_CAMISA;
        this.estadoCivil = ESTADO_CIVIL;
        this.genero = GENERO;
        this.grupoSanguineo = GRUPO_SANGUINEO;
        this.estado = ESTADO;
        this.idTipoDocumento = ID_TIPO_DOCUMENTO;
    }

    public ActualizarUsuarioComando buildActualizarUsuarioComando() {
        ActualizarUsuarioComando actualizarUsuarioComando = new ActualizarUsuarioComando();

        actualizarUsuarioComando.setNombres(this.nombres);
        actualizarUsuarioComando.setGoogleId(this.googleId);
        actualizarUsuarioComando.setIdentificacion(this.identificacion);
        actualizarUsuarioComando.setApellidos(this.apellidos);
        actualizarUsuarioComando.setCorreoPersonal(this.correoPersonal);
        actualizarUsuarioComando.setTelefonoCelular(this.telefonoFijo);
        actualizarUsuarioComando.setTelefonoCelular(this.telefonoCelular);
        actualizarUsuarioComando.setIdNacionalidad(this.idNacionalidad);
        actualizarUsuarioComando.setIdLugarNacimiento(this.idLugarNacimiento);
        actualizarUsuarioComando.setIdLugarRecidencia(this.idLugarRecidencia);
        actualizarUsuarioComando.setFechaNacimiento(this.fechaNacimiento);
        actualizarUsuarioComando.setDireccion(this.direccion);
        actualizarUsuarioComando.setNombreContactoEmergencia(this.nombreContactoEmergencia);
        actualizarUsuarioComando.setParentescoContactoEmergencia(this.parentescoContactoEmergencia);
        actualizarUsuarioComando.setTelefonoContactoEmergencia(this.telefonoContactoEmergencia);
        actualizarUsuarioComando.setTallaCamisa(this.tallaCamisa);
        actualizarUsuarioComando.setEstadoCivil(this.estadoCivil);
        actualizarUsuarioComando.setGenero(this.genero);
        actualizarUsuarioComando.setGrupoSanguineo(this.grupoSanguineo);
        actualizarUsuarioComando.setIdTipoDocumento(this.idTipoDocumento);

        return actualizarUsuarioComando;
    }

}
