package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarDiasVacacionesComando;

public class ActualizarDiasVacacionesComandoTest {

    private double DIAS_GANADOS = 50;
    private int DIAS_TOMADOS = 10;

    private double diasGanados;
    private int diasTomados;

    public ActualizarDiasVacacionesComandoTest(){
        this.diasGanados = DIAS_GANADOS;
        this.diasTomados = DIAS_TOMADOS;
    }

    public ActualizarDiasVacacionesComando build(){
        return new ActualizarDiasVacacionesComando(
                this.diasGanados,
                this.diasTomados
        );
    }

}
