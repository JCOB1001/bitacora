package com.pragma.perfiles.perfil.infraestructura;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.pragma.perfiles.perfil.aplicacion.comando.GuardarUsuarioNuevoComando;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorGuardarUsuario;
import com.pragma.perfiles.perfil.databuilder.*;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioDatosHojaDeVidaComando;
import com.pragma.perfiles.perfil.dominio.modelo.*;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ControladorUsuarioPragmaDexTest {

    public static final String RUTA_CONTROLADOR = "/usuarios";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String AUTHORIZATION_HEADER_TOKEN = "eyJraWQiOiJlcERtUHA2TUdnVVNtWFN6RktoV2VjcVRLQ0NJUWVHT0t3ZDl" +
            "TOWdkV3RzPSIsImFsZyI6IlJTMjU2In0.eyJhdF9oYXNoIjoiWFEwZnJuT2NMdkFndnd1ZjJSWXk5QSIsInN1YiI6ImNlODczOWI0LWMwZT" +
            "UtNGU1OS04NWEzLTVlNTBjMGY4NGY1OCIsImNvZ25pdG86Z3JvdXBzIjpbInVzLWVhc3QtMV9Ud3N5d2ZySURfR29vZ2xlIiwiZmFjdHVyY" +
            "WNpb25fYWRtaW5pc3RyYWRvciJdLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0" +
            "LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xX1R3c3l3ZnJJRCIsImNvZ25pdG86dXNlcm5hbWUiOiJHb29nbGVfMTExODM5MDg5NDk1Nzg" +
            "0ODU0OTc0IiwiYXVkIjoiNTlydGkwMTQ5c2xoaXZjdnQzZ2YxcTE1MmMiLCJpZGVudGl0aWVzIjpbeyJ1c2VySWQiOiIxMTE4MzkwODk0OT" +
            "U3ODQ4NTQ5NzQiLCJwcm92aWRlck5hbWUiOiJHb29nbGUiLCJwcm92aWRlclR5cGUiOiJHb29nbGUiLCJpc3N1ZXIiOm51bGwsInByaW1hc" +
            "nkiOiJ0cnVlIiwiZGF0ZUNyZWF0ZWQiOiIxNjE3ODkwMTM1MTMwIn1dLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYyNTc3MDM2" +
            "NSwibmFtZSI6Ilt7XCJtZXRhZGF0YVwiOntcInByaW1hcnlcIjp0cnVlLFwic291cmNlXCI6e1widHlwZVwiOlwiUFJPRklMRVwiLFwiaWR" +
            "cIjpcIjExMTgzOTA4OTQ5NTc4NDg1NDk3NFwifX0sXCJkaXNwbGF5TmFtZVwiOlwiQ3Jpc3RpYW4gQ2FtaWxvIE11w7HDs3ogQXJhbmdvXC" +
            "IsXCJmYW1pbHlOYW1lXCI6XCJNdcOxw7N6IEFyYW5nb1wiLFwiZ2l2ZW5OYW1lXCI6XCJDcmlzdGlhbiBDYW1pbG9cIixcImRpc3BsYXlOY" +
            "W1lTGFzdEZpcnN0XCI6XCJNdcOxw7N6IEFyYW5nbywgQ3Jpc3RpYW4gQ2FtaWxvXCIsXCJ1bnN0cnVjdHVyZWROYW1lXCI6XCJDcmlzdGlh" +
            "biBDYW1pbG8gTXXDscOzeiBBcmFuZ29cIn0se1wibWV0YWRhdGFcIjp7XCJzb3VyY2VcIjp7XCJ0eXBlXCI6XCJET01BSU5fUFJPRklMRVw" +
            "iLFwiaWRcIjpcIjExMTgzOTA4OTQ5NTc4NDg1NDk3NFwifX0sXCJkaXNwbGF5TmFtZVwiOlwiQ3Jpc3RpYW4gQ2FtaWxvIE11w7HDs3ogQX" +
            "JhbmdvXCIsXCJmYW1pbHlOYW1lXCI6XCJNdcOxw7N6IEFyYW5nb1wiLFwiZ2l2ZW5OYW1lXCI6XCJDcmlzdGlhbiBDYW1pbG9cIixcImRpc" +
            "3BsYXlOYW1lTGFzdEZpcnN0XCI6XCJDcmlzdGlhbiBDYW1pbG8gTXXDscOzeiBBcmFuZ29cIixcInVuc3RydWN0dXJlZE5hbWVcIjpcIkNy" +
            "aXN0aWFuIENhbWlsbyBNdcOxw7N6IEFyYW5nb1wifV0iLCJleHAiOjE2MjcwNzE3NTcsImlhdCI6MTYyNzA2ODE1NywiZW1haWwiOiJjcml" +
            "zdGlhbmMubXVub3ptQHByYWdtYS5jb20uY28ifQ.fVqTl_ge5d_yWZLJaQgzWOyLHEYHuUxd8dEjXD4_NM__YbpV85Ey16LBlfPBrzqHeG7" +
            "CiqE5WJgm9jDUT4GufsYFNY9VWrtpnrQisJ_cZV8Pa2lWpBbui-IGdJshd1Bvw2ys3pYjLQ8ADWUUWVjN2kjHHGaj7DkKutH-AJV4mqHtnC" +
            "Oeb-RMtAWL3G-S27loHtuTL1IEkBj455S6L8-gH3UbG_ucA9-wTm2Nadp6fU9zDfn79lNWqMF3lpGGkcfSnjfkqaaYWhI46FOLYI01iq_JZ" +
            "o3NSZXgB70pNRBuTGkchmA45ePbnGQ0-n5eqoqVm7fhyq-qeIVjVGR4B2bBjg";
    public static final String CORREO = "cristianc.munozm@pragma.com.co";
    private static String idUsuario;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ManejadorGuardarUsuario manejadorGuardarUsuario;

    @MockBean
    private UsuarioCrudUseCase usuarioCrudUseCase;

    @Test
    public void testOrden01EjecutarGuardarUsuarioNuevo() throws Exception {
        GuardarUsuarioNuevoComando guardarUsuarioNuevoComando = new GuardarUsuarioComandoTestDataBuilder()
                .buildGuardarUsuarioNuevoComando();
        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR)
                .build();
        mvc.perform(MockMvcRequestBuilders
                        .post(uri.toUriString())
                        .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                        .content(objectMapper.writeValueAsString(guardarUsuarioNuevoComando))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testOrden02EjecutarObtenerUsuarioFiltroCorreo() throws Exception {

        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR + "/correo?correo="+CORREO)
                .build();

        Mockito.when(usuarioCrudUseCase.findByCorreoEmpresarial(Mockito.any())).thenReturn(obtenerUsuario());

        final ResultActions result = mvc.perform(MockMvcRequestBuilders
                        .get(uri.toUriString())
                        .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON));
        result
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());

        MvcResult mvcResult = result.andReturn();

        String contentAsString = mvcResult.getResponse().getContentAsString();

        idUsuario = JsonPath.read(contentAsString, "$.dato.id");
        System.out.println(idUsuario);
    }

    @Test
    public void testOrden03EjecutarObtenerUsuarioPorId() throws Exception {
        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR + "/{idUsuario}")
                .build();

        Mockito.when(usuarioCrudUseCase.obtenerPorId(Mockito.any())).thenReturn(obtenerUsuario());

        mvc.perform(MockMvcRequestBuilders
                        .get(uri.toUriString(), idUsuario)
                        .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testOrden04EjecutarObtenerUsuariosFiltro() throws Exception {
        LinkedMultiValueMap<String, String> linkedMultiValueMapObtenerUsuariosFiltroComando = new ObtenerUsuariosFiltroComandoTestDataBuilder()
                .buildObtenerUsuariosFiltroComando();
        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR)
                .build();
        mvc.perform(MockMvcRequestBuilders
                        .get(uri.toUriString())
                        .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                        .params(linkedMultiValueMapObtenerUsuariosFiltroComando)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testOrden05EjecutarObtenerUsuario() throws Exception {
        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR + "/token")
                .build();
        mvc.perform(MockMvcRequestBuilders
                        .get(uri.toUriString())
                        .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testOrden06EjecutarActualizarUsuario() throws Exception {
        ActualizarUsuarioComando actualizarUsuarioComando = new ActualizarUsuarioComandoTestDataBuilder()
                .buildActualizarUsuarioComando();

        Mockito.when(usuarioCrudUseCase.obtenerPorId(Mockito.any())).thenReturn(obtenerUsuario());

        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR + "/" + idUsuario)
                .build();
        mvc.perform(MockMvcRequestBuilders
                .put(uri.toUriString(), idUsuario)
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(actualizarUsuarioComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testOrden07EjecutarAgregarDiasTomadosUsuario() throws Exception {
        String diasTomados = "1";
        LinkedMultiValueMap<String, String> linkedMultiValueMapObtenerUsuarioFiltroCorreoComando = new ObtenerUsuarioFiltroCorreoComandoTestDataBuilder()
                .buildObtenerUsuarioFiltroCorreoComando();
        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR + "/agregardiastomados/{diasTomados}")
                .build();
        mvc.perform(MockMvcRequestBuilders
                .put(uri.toUriString(), diasTomados)
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .params(linkedMultiValueMapObtenerUsuarioFiltroCorreoComando)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testOrden09EjecutarEliminarUsuarioPorId() throws Exception {
        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR + "/" + idUsuario)
                .build();

        Mockito.when(usuarioCrudUseCase.obtenerPorId(Mockito.any())).thenReturn(obtenerUsuario());
        mvc.perform(MockMvcRequestBuilders
                .delete(uri.toUriString(), idUsuario)
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testOrden08EjecutarActualizarUsuarioDatosHojaDeVida() throws Exception {
        ActualizarUsuarioDatosHojaDeVidaComando actualizarUsuarioDatosHojaDeVidaComando = new ActualizarUsuarioDatosHojaDeVidaComandoTestDataBuilder()
                .buildActualizarUsuarioDatosHojaDeVidaComando();

        Mockito.when(usuarioCrudUseCase.obtenerPorId(Mockito.any())).thenReturn(obtenerUsuario());

        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR + "/hoja-de-vida")
                .build();
        mvc.perform(MockMvcRequestBuilders
                        .put(uri.toUriString())
                        .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                        .content(objectMapper.writeValueAsString(actualizarUsuarioDatosHojaDeVidaComando))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    private Usuario obtenerUsuario() {
        return new UsuarioTestDataBuilder().buildUsuario();
    }
}
