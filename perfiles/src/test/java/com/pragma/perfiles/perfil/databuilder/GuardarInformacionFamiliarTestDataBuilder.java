package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.informacion_familiar.aplicacion.comando.GuardarInformacionFamiliarComando;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.Genero;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
public class GuardarInformacionFamiliarTestDataBuilder {

    private String nombres;
    private String apellidos;
    private Genero genero;
    private LocalDate fechaNacimiento;
    private String idUsuario;
    private Long idParentesco;
    private boolean dependiente;

    public GuardarInformacionFamiliarTestDataBuilder(){
        this.nombres="Eduard Jose";
        this.apellidos="Test Guardado";
        this.fechaNacimiento=LocalDate.now();
        this.genero=Genero.MASCULINO;
        this.idUsuario="9876554";
        this.idParentesco=4L;
        this.dependiente=false;
    }

    public GuardarInformacionFamiliarComando builGuardarInformacionFamiliarComando(){
        return new GuardarInformacionFamiliarComando(nombres,apellidos,genero,fechaNacimiento,idUsuario,idParentesco,dependiente);
    }

    public GuardarInformacionFamiliarComando builGuardarInformacionFamiliarComandoConUsuarioNoexistente(){
        return new GuardarInformacionFamiliarComando(nombres,apellidos,genero,fechaNacimiento,"01234567891234",idParentesco,dependiente);
    }
}
