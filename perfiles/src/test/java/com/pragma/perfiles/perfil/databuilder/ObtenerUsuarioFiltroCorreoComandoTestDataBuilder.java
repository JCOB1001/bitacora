package com.pragma.perfiles.perfil.databuilder;

    import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuarioFiltroCorreoComando;
    import lombok.Getter;
import org.springframework.util.LinkedMultiValueMap;

@Getter
public class ObtenerUsuarioFiltroCorreoComandoTestDataBuilder {

    public static final String CORREO = "juan.arrieta@pragma.com.co";

    private String correoEmpresarial;

    public ObtenerUsuarioFiltroCorreoComandoTestDataBuilder() {
        this.correoEmpresarial = CORREO;
    }

    public LinkedMultiValueMap<String, String> buildObtenerUsuarioFiltroCorreoComando() {
        LinkedMultiValueMap<String, String> linkedMultiValueMapObtenerUsuariosFiltroCorreoComando = new LinkedMultiValueMap<>();
        linkedMultiValueMapObtenerUsuariosFiltroCorreoComando.add("correoEmpresarial", String.valueOf(this.correoEmpresarial));
        return linkedMultiValueMapObtenerUsuariosFiltroCorreoComando;
    }

    public ObtenerUsuarioFiltroCorreoComando buildObtenerUsuarioFiltroCorreoComandoIdentificacion() {
        return new ObtenerUsuarioFiltroCorreoComando(this.correoEmpresarial);
    }

}
