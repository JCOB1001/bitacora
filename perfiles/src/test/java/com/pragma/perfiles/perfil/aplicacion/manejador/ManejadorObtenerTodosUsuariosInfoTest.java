package com.pragma.perfiles.perfil.aplicacion.manejador;

import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuariosFiltroComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioInfoConsulta;
import com.pragma.perfiles.perfil.aplicacion.fabrica.FabricaUsuario;
import com.pragma.perfiles.perfil.dominio.filtro.FiltroUsuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioInfo;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import com.pragma.perfiles.perfil.infraestructura.clientefeign.FeignTipoDocumento;
import com.pragma.perfiles.pragmadata.dominio.modelo.TipoDocumento;
import com.pragma.vacaciones.common.respuesta.ObjetoRespuesta;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ManejadorObtenerTodosUsuariosInfoTest {

    @InjectMocks
    ManejadorObtenerTodosUsuariosInfo manejadorObtenerTodosUsuariosInfo;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    UsuarioCrudUseCase usuarioCrudUseCase;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    FeignTipoDocumento feignTipoDocumento;
    @Mock
    FabricaUsuario fabricaUsuario;

    private ObtenerUsuariosFiltroComando obtenerUsuariosFiltroComando;
    private UsuarioInfoConsulta usuarioInfoConsulta;
    private FiltroUsuario filtroUsuario;
    private TipoDocumento tipoDocumento;
    private UsuarioInfo usuarioInfo;
    private ObjetoRespuesta objetoRespuesta;

    @BeforeEach
    void setUp() {
        obtenerUsuariosFiltroComando = new ObtenerUsuariosFiltroComando(0,0,"123456",true,"stefanie","perez");
        usuarioInfoConsulta = new UsuarioInfoConsulta("stefanie","perez","cedula","123456","abc123","correo@gmail.com", LocalDate.now(),"prestacion",false);
        filtroUsuario = new FiltroUsuario(0,0,"123456",true,"stefanie","perez");
        tipoDocumento = new TipoDocumento(1L, "cedula","cc");
        usuarioInfo = new UsuarioInfo("stefanie","perez",1,"123456","abc123","correo@gmail.com",LocalDate.now(),"prestacion",false);
        objetoRespuesta = new ObjetoRespuesta<>(List.of(tipoDocumento));
    }

    @Test
    void ejecutar() {
        when(fabricaUsuario.cargarFiltroUsuario(obtenerUsuariosFiltroComando)).thenReturn(filtroUsuario);
        when(feignTipoDocumento.obtenerTipoDocumentos().getDato()).thenReturn(List.of(tipoDocumento));
        when(usuarioCrudUseCase.obtenerInformacionTodosUsuarios(filtroUsuario).getDato()).thenReturn(List.of(usuarioInfo));

        Assertions.assertEquals(List.of(usuarioInfoConsulta),manejadorObtenerTodosUsuariosInfo.ejecutar(obtenerUsuariosFiltroComando));

        verify(fabricaUsuario).cargarFiltroUsuario(obtenerUsuariosFiltroComando);
    }
}