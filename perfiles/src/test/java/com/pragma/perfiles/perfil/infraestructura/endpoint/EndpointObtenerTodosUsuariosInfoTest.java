package com.pragma.perfiles.perfil.infraestructura.endpoint;

import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuariosFiltroComando;
import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioInfoConsulta;
import com.pragma.perfiles.perfil.aplicacion.manejador.ManejadorObtenerTodosUsuariosInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EndpointObtenerTodosUsuariosInfoTest {

    @InjectMocks
    EndpointObtenerTodosUsuariosInfo endpointObtenerTodosUsuariosInfo;

    @Mock
    ManejadorObtenerTodosUsuariosInfo manejadorObtenerTodosUsuariosInfo;

    private ObtenerUsuariosFiltroComando obtenerUsuariosFiltroComando;
    private UsuarioInfoConsulta usuarioInfoConsulta;
    @BeforeEach
    void setUp() {
        obtenerUsuariosFiltroComando = new ObtenerUsuariosFiltroComando(0,0,"123456",true,"stefanie","perez");
        usuarioInfoConsulta = new UsuarioInfoConsulta("stefanie","perez","1","123456","abc123","correo@gmail.com", LocalDate.now(),"prestacion",false);
    }

    @Test
    void obtenerTodosUsuariosInfo() {
        when(manejadorObtenerTodosUsuariosInfo.ejecutar(obtenerUsuariosFiltroComando)).thenReturn(List.of(usuarioInfoConsulta));
        Assertions.assertEquals(List.of(usuarioInfoConsulta),endpointObtenerTodosUsuariosInfo.obtenerTodosUsuariosInfo(obtenerUsuariosFiltroComando));
        verify(manejadorObtenerTodosUsuariosInfo).ejecutar(obtenerUsuariosFiltroComando);
    }
}