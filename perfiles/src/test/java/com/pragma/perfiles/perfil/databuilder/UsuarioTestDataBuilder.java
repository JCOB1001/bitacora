package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.perfil.dominio.modelo.EstadoProfesion;
import com.pragma.perfiles.perfil.dominio.modelo.Fotografia;
import com.pragma.perfiles.perfil.dominio.modelo.Usuario;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstadoCivil;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGenero;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGrupoSanguineo;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioTallaCamisa;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UsuarioTestDataBuilder {

    public static final String CORREO_EMPRESARIAL = "cristianc.munozm@pragma.com.co";
    public static final String IDENTIFICACION = "123";
    public static final String NOMBRES = "PEDRO";
    public static final String APELLIDOS = "PEREZ";
    public static final String ID_VICEPRESIDENCIA = "1";
    public static final String CLIENTE = "PRUEBA";
    public static final LocalDate FECHA_INGRESO = LocalDate.of(2021, 07, 15);
    public static final double DIAS_GANADOS = 1.0;
    public static final int DIAS_TOMADOS = 1;
    public static final String GOOGLE_ID = "";
    public static final String CORREO_PERSONAL = "";
    public static final String TELEFONO_FIJO = "";
    public static final String TELEFONO_CELULAR = "";
    public static final String ID_NACIONALIDAD = "";
    public static final String ID_LUGAR_NACIMIENTO = "";
    public static final String ID_LUGAR_RECIDENCIA = "";
    public static final LocalDate FECHA_NACIMIENTO = LocalDate.of(1995, 07, 15);
    public static final String DIRECCION = "";
    public static final String NOMBRE_CONTACTO_EMERGENCIA = "";
    public static final String PARENTESCO_CONTACTO_EMERGENCIA = "";
    public static final String TELEFONO_CONTACTO_EMERGENCIA = "";
    public static final String TARJETA_PROFESIONAL = "";
    public static final String PERFIL_PROFESIONAL = "";
    public static final String CONOCIMIENTOS_TECNICOS = "";
    public static final UsuarioTallaCamisa TALLA_CAMISA = UsuarioTallaCamisa.L;
    public static final UsuarioEstadoCivil ESTADO_CIVIL = UsuarioEstadoCivil.CASADO_A;
    public static final UsuarioGenero GENERO = UsuarioGenero.MASCULINO;
    public static final UsuarioGrupoSanguineo GRUPO_SANGUINEO = UsuarioGrupoSanguineo.A_NEGATIVO;
    public static final Long ID_TIPODOCUMENTO = 1L;
    public static final Long ID_PROFESION = 1L;
    public static final LocalDate FECHA_CREACION = LocalDate.of(2021, 07, 15);
    public static final LocalDate FECHA_ULTIMA_ACTUALIZACION = LocalDate.of(2021, 07, 15);
    public static final String CARGO = "Desarrollador";
    public static final List<Long> CAPACIDAD = new ArrayList<>();
    public static final List<Fotografia> FOTOGRAFIAS = new ArrayList<>();
    public static final boolean ACEPTACION_TERMINOS = false;
    public static final double BARRA_PROGRESO=0.9678;
    public static final String NOMBRE_PREFERENCIA = "Santiago";
    public static final String PASAPORTE = "AB2022A458S";
    public static final int ESTRATO = 1;
    public static final String SEMESTRE = "1";
    public static final EstadoProfesion ESTADO_PROFESION = EstadoProfesion.ESTUDIANDO;
    public static final String TIPO_CONTRATO_ID = "1";
    public static final LocalDate FECHA_INICIO_CONTRATO = LocalDate.of(2021, 7, 15);
    public static final String EMPRESA = "Pragma";
    public static final String CONTRASENIA_COMPUTADOR = "Induccion2022+";
    private String correoEmpresarial;
    private String identificacion;
    private String nombres;
    private String apellidos;
    private String cliente;
    private LocalDate fechaIngreso;
    private double diasGanados;
    private int diasTomados;
    private String googleId;
    private String correoPersonal;
    private String telefonoFijo;
    private String telefonoCelular;
    private String idNacionalidad;
    private String idLugarNacimiento;
    private String idLugarRecidencia;
    private LocalDate fechaNacimiento;
    private String direccion;
    private String nombreContactoEmergencia;
    private String parentescoContactoEmergencia;
    private String telefonoContactoEmergencia;
    private String tarjetaProfesional;
    private String perfilProfesional;
    private String conocimientosTecnicos;
    private UsuarioTallaCamisa tallaCamisa;
    private UsuarioEstadoCivil estadoCivil;
    private UsuarioGenero genero;
    private UsuarioGrupoSanguineo grupoSanguineo;
    private Long idTipoDocumento;
    private Long idProfesion;
    private LocalDate fechaCreacion;
    private LocalDate fechaUltimaActualizacion;
    private String cargo;
    private String idVicepresidencia;
    private List<Long> capacidad;
    private List<Fotografia> fotografias;
    private boolean aceptacionTerminos;
    private double barraProgreso;
    private String nombrePreferencia;
    private String pasaporte;
    private int estrato;
    private String semestre;
    private EstadoProfesion estadoProfesion;
    private String tipoContratoId;
    private LocalDate fechaInicioContrato;
    private String empresa;
    private String contraseniaComputador;

    public UsuarioTestDataBuilder() {
        this.correoEmpresarial = CORREO_EMPRESARIAL;
        this.identificacion = IDENTIFICACION;
        this.nombres = NOMBRES;
        this.apellidos = APELLIDOS;
        this.cliente = CLIENTE;
        this.fechaIngreso = FECHA_INGRESO;
        this.diasGanados = DIAS_GANADOS;
        this.diasTomados = DIAS_TOMADOS;
        this.googleId = GOOGLE_ID;
        this.correoPersonal = CORREO_PERSONAL;
        this.telefonoFijo = TELEFONO_FIJO;
        this.telefonoCelular = TELEFONO_CELULAR;
        this.idNacionalidad = ID_NACIONALIDAD;
        this.idLugarNacimiento = ID_LUGAR_NACIMIENTO;
        this.idLugarRecidencia = ID_LUGAR_RECIDENCIA;
        this.fechaNacimiento = FECHA_NACIMIENTO;
        this.direccion = DIRECCION;
        this.nombreContactoEmergencia = NOMBRE_CONTACTO_EMERGENCIA;
        this.parentescoContactoEmergencia = PARENTESCO_CONTACTO_EMERGENCIA;
        this.telefonoContactoEmergencia = TELEFONO_CONTACTO_EMERGENCIA;
        this.tarjetaProfesional = TARJETA_PROFESIONAL;
        this.perfilProfesional = PERFIL_PROFESIONAL;
        this.conocimientosTecnicos = CONOCIMIENTOS_TECNICOS;
        this.tallaCamisa = TALLA_CAMISA;
        this.estadoCivil = ESTADO_CIVIL;
        this.genero = GENERO;
        this.grupoSanguineo = GRUPO_SANGUINEO;
        this.idTipoDocumento = ID_TIPODOCUMENTO;
        this.idProfesion = ID_PROFESION;
        this.fechaCreacion = FECHA_CREACION;
        this.fechaUltimaActualizacion = FECHA_ULTIMA_ACTUALIZACION;
        this.cargo = CARGO;
        this.idVicepresidencia = ID_VICEPRESIDENCIA;
        this.capacidad = CAPACIDAD;
        this.fotografias = FOTOGRAFIAS;
        this.aceptacionTerminos=ACEPTACION_TERMINOS;
        this.barraProgreso=BARRA_PROGRESO;
        this.nombrePreferencia= NOMBRE_PREFERENCIA;
        this.pasaporte = PASAPORTE;
        this.estrato = ESTRATO;
        this.semestre = SEMESTRE;
        this.estadoProfesion = ESTADO_PROFESION;
        this.tipoContratoId = TIPO_CONTRATO_ID;
        this.fechaInicioContrato = FECHA_INICIO_CONTRATO;
        this.empresa = EMPRESA;
        this.contraseniaComputador = CONTRASENIA_COMPUTADOR;
    }


    public Usuario buildUsuario() {
        return new Usuario("1",
                this.correoEmpresarial,
                this.identificacion,
                this.nombres,
                this.apellidos,
                this.cliente,
                this.fechaIngreso,
                this.diasGanados,
                this.diasTomados,
                this.googleId,
                this.correoPersonal,
                this.telefonoFijo,
                this.telefonoCelular,
                this.idNacionalidad,
                this.idLugarNacimiento,
                this.idLugarRecidencia,
                this.fechaNacimiento,
                this.direccion,
                this.nombreContactoEmergencia,
                this.parentescoContactoEmergencia,
                this.telefonoContactoEmergencia,
                this.tarjetaProfesional,
                this.perfilProfesional,
                this.conocimientosTecnicos,
                this.tallaCamisa,
                this.estadoCivil,
                this.genero,
                this.grupoSanguineo,
                this.idTipoDocumento,
                this.idProfesion,
                this.fechaCreacion,
                this.fechaUltimaActualizacion,
                this.cargo,
                this.idVicepresidencia,
                this.capacidad,
                this.fotografias,
                this.aceptacionTerminos,
                this.barraProgreso,
                this.nombrePreferencia,
                this.pasaporte,
                this.estrato,
                this.semestre,
                this.estadoProfesion,
                this.tipoContratoId,
                this.fechaInicioContrato,
                this.empresa,
                this.contraseniaComputador);
    }

}
