package com.pragma.perfiles.perfil.dominio;

import com.pragma.perfiles.perfil.databuilder.UsuarioTestDataBuilder;
import com.pragma.perfiles.perfil.dominio.modelo.*;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class UsuarioPragmaDexTest {

    public static final String ID = "1";
    public static final String CORREO_EMPRESARIAL = "cristianc.munozm@pragma.com.co";
    public static final String IDENTIFICACION = "123";
    public static final String NOMBRES = "PEDRO";
    public static final String APELLIDOS = "PEREZ";
    public static final String ID_VICEPRESIDENCIA = "1";
    public static final String CLIENTE = "PRUEBA";
    public static final LocalDate FECHA_INGRESO = LocalDate.of(2021, 07, 15);
    public static final double DIAS_GANADOS = 1.0;
    public static final int DIAS_TOMADOS = 1;
    public static final String GOOGLE_ID = "";
    public static final String CORREO_PERSONAL = "";
    public static final String TELEFONO_FIJO = "";
    public static final String TELEFONO_CELULAR = "";
    public static final String ID_NACIONALIDAD = "";
    public static final String ID_LUGAR_NACIMIENTO = "";
    public static final String ID_LUGAR_RECIDENCIA = "";
    public static final LocalDate FECHA_NACIMIENTO = LocalDate.of(1995, 07, 15);
    public static final String DIRECCION = "";
    public static final String NOMBRE_CONTACTO_EMERGENCIA = "";
    public static final String PARENTESCO_CONTACTO_EMERGENCIA = "";
    public static final String TELEFONO_CONTACTO_EMERGENCIA = "";
    public static final String TARJETA_PROFESIONAL = "";
    public static final String PERFIL_PROFESIONAL = "";
    public static final String CONOCIMIENTOS_TECNICOS = "";
    public static final UsuarioTallaCamisa TALLA_CAMISA = UsuarioTallaCamisa.L;
    public static final UsuarioEstadoCivil ESTADO_CIVIL = UsuarioEstadoCivil.CASADO_A;
    public static final UsuarioGenero GENERO = UsuarioGenero.MASCULINO;
    public static final UsuarioGrupoSanguineo GRUPO_SANGUINEO = UsuarioGrupoSanguineo.A_NEGATIVO;
    public static final UsuarioEstado ESTADO = UsuarioEstado.ACTIVO;
    public static final Long ID_TIPODOCUMENTO = 1L;
    public static final Long ID_PROFESION = 1L;
    public static final LocalDate FECHA_CREACION = LocalDate.of(2021, 07, 15);
    public static final LocalDate FECHA_ULTIMA_ACTUALIZACION = LocalDate.now();
    public static final String CARGO = "Desarrollador";
    public static final List<Long> CAPACIDAD = new ArrayList<>();
    public static final List<Fotografia> FOTOGRAFIAS = new ArrayList<>();

    public static final double delta = 0.01;

    @Test
    public void testCrearModeloUsuario() {
        UsuarioTestDataBuilder usuarioTestDataBuilder = new UsuarioTestDataBuilder();
        Usuario usuario = usuarioTestDataBuilder.buildUsuario();
        assertEquals(ID, usuario.getId());
        assertEquals(CORREO_EMPRESARIAL, usuario.getCorreoEmpresarial());
        assertEquals(IDENTIFICACION, usuario.getIdentificacion());
        assertEquals(NOMBRES, usuario.getNombres());
        assertEquals(APELLIDOS, usuario.getApellidos());
        assertEquals(ID_VICEPRESIDENCIA, usuario.getIdVicepresidencia());
        assertEquals(CLIENTE, usuario.getCliente());
        assertEquals(FECHA_INGRESO, usuario.getFechaIngreso());
        assertEquals(DIAS_GANADOS, usuario.getDiasGanados(), delta);
        assertEquals(DIAS_TOMADOS, usuario.getDiasTomados());
        assertEquals(GOOGLE_ID, usuario.getGoogleId());
        assertEquals(CORREO_PERSONAL, usuario.getCorreoPersonal());
        assertEquals(TELEFONO_FIJO, usuario.getTelefonoFijo());
        assertEquals(TELEFONO_CELULAR, usuario.getTelefonoCelular());
        assertEquals(ID_NACIONALIDAD, usuario.getIdNacionalidad());
        assertEquals(ID_LUGAR_NACIMIENTO, usuario.getIdLugarNacimiento());
        assertEquals(ID_LUGAR_RECIDENCIA, usuario.getIdLugarRecidencia());
        assertEquals(FECHA_NACIMIENTO, usuario.getFechaNacimiento());
        assertEquals(DIRECCION, usuario.getDireccion());
        assertEquals(NOMBRE_CONTACTO_EMERGENCIA, usuario.getNombreContactoEmergencia());
        assertEquals(PARENTESCO_CONTACTO_EMERGENCIA, usuario.getParentescoContactoEmergencia());
        assertEquals(TELEFONO_CONTACTO_EMERGENCIA, usuario.getTelefonoContactoEmergencia());
        assertEquals(TARJETA_PROFESIONAL, usuario.getTarjetaProfesional());
        assertEquals(PERFIL_PROFESIONAL, usuario.getPerfilProfesional());
        assertEquals(CONOCIMIENTOS_TECNICOS, usuario.getConocimientosTecnicos());
        assertEquals(TALLA_CAMISA, usuario.getTallaCamisa());
        assertEquals(ESTADO_CIVIL, usuario.getEstadoCivil());
        assertEquals(GENERO, usuario.getGenero());
        assertEquals(GRUPO_SANGUINEO, usuario.getGrupoSanguineo());
        assertEquals(ESTADO, usuario.getEstado());
        assertEquals(ID_TIPODOCUMENTO, usuario.getIdTipoDocumento());
        assertEquals(ID_PROFESION, usuario.getIdProfesion());
        assertEquals(FECHA_ULTIMA_ACTUALIZACION, usuario.getFechaUltimaActualizacion());
        assertEquals(CARGO, usuario.getCargo());
        assertEquals(CAPACIDAD, usuario.getCapacidad());
        assertEquals(FOTOGRAFIAS, usuario.getFotografias());
    }

}
