package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.perfil.aplicacion.consulta.UsuarioPorTokenIdentificacionConsulta;

public class UsuarioPorTokenIdentificacionConsultaTestDataBuilder {

    private static final String ID = "93234567894";
    private static final String NOMBRES = "Karen Brigid";
    private static final String APELLIDOS = "Beltran Vera";
    private static final String CORREOEMPRESARIAL = "karen.beltran@pragma.com.co";
    private static final Long IDTIPODOCUMENTO = 1L;
    private static final String IDENTIFICACION = "123456";

    private String id;
    private String nombres;
    private String apellidos;
    private String correoEmpresarial;
    private Long idTipoDocumento;
    private String identificacion;

    public UsuarioPorTokenIdentificacionConsultaTestDataBuilder() {
        this.id = ID;
        this.nombres = NOMBRES;
        this.apellidos = APELLIDOS;
        this.correoEmpresarial = CORREOEMPRESARIAL;
        this.idTipoDocumento = IDTIPODOCUMENTO;
        this.identificacion = IDENTIFICACION;
    }

    public UsuarioPorTokenIdentificacionConsulta buildUsuarioPorTokenIdentificacionConsulta(){
        UsuarioPorTokenIdentificacionConsulta usuarioPorTokenIdentificacionConsulta = new UsuarioPorTokenIdentificacionConsulta();
        usuarioPorTokenIdentificacionConsulta.setId(this.id);
        usuarioPorTokenIdentificacionConsulta.setNombres(this.nombres);
        usuarioPorTokenIdentificacionConsulta.setApellidos(this.apellidos);
        usuarioPorTokenIdentificacionConsulta.setCorreoEmpresarial(this.correoEmpresarial);
        usuarioPorTokenIdentificacionConsulta.setIdTipoDocumento(this.idTipoDocumento);
        usuarioPorTokenIdentificacionConsulta.setIdentificacion(this.identificacion);
        return usuarioPorTokenIdentificacionConsulta;
    }
}
