package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioDatosHojaDeVidaComando;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class ActualizarUsuarioDatosHojaDeVidaComandoTestDataBuilder {

    public static final String ID_USUARIO = "111839089495784854974";
    public static final String GOOGLE_ID = "googleId";
    public static final Long ID_PROFESION = 5L;
    public static final String PERFIL_PROFESIONAL = "Programador junior con 2 años de experiencia.";
    public static final String TARJETA_PROFESIONAL = "987654321";
    public static final String CONOCIMIENTOS_TECNICOS = "Java - C++ - JavaScript";
    public static final String CARGO = "Auxiliar de desarrollo";
    public static final String ID_VICEPRECIDENCIA = "1";
    public static final LocalDate FECHA_INGRESO = LocalDate.of(2021, 06, 8);
    public static final List<Long> CAPACIDAD = Arrays.asList(1L,2L,3L);

    private String googleId;
    private String idUsuario;
    private Long idProfesion;
    private String perfilProfesional;
    private String tarjetaProfesional;
    private String conocimientosTecnicos;
    private String cargo;
    private String idVicepresidencia;
    private LocalDate fechaIngreso;
    private List<Long> capacidad;

    public ActualizarUsuarioDatosHojaDeVidaComandoTestDataBuilder(){
        this.googleId = GOOGLE_ID;
        this.idUsuario = ID_USUARIO;
        this.idProfesion = ID_PROFESION;
        this.perfilProfesional = PERFIL_PROFESIONAL;
        this.tarjetaProfesional = TARJETA_PROFESIONAL;
        this.conocimientosTecnicos = CONOCIMIENTOS_TECNICOS;
        this.cargo = CARGO;
        this.idVicepresidencia = ID_VICEPRECIDENCIA;
        this.fechaIngreso = FECHA_INGRESO;
        this.capacidad = CAPACIDAD;
    }

    public ActualizarUsuarioDatosHojaDeVidaComando buildActualizarUsuarioDatosHojaDeVidaComando(){
        ActualizarUsuarioDatosHojaDeVidaComando actualizarUsuarioDatosHojaDeVidaComando = new ActualizarUsuarioDatosHojaDeVidaComando();

        actualizarUsuarioDatosHojaDeVidaComando.setGoogleId(this.googleId);
        actualizarUsuarioDatosHojaDeVidaComando.setIdUsuario(this.idUsuario);
        actualizarUsuarioDatosHojaDeVidaComando.setIdProfesion(this.idProfesion);
        actualizarUsuarioDatosHojaDeVidaComando.setPerfilProfesional(this.perfilProfesional);
        actualizarUsuarioDatosHojaDeVidaComando.setTarjetaProfesional(this.tarjetaProfesional);
        actualizarUsuarioDatosHojaDeVidaComando.setConocimientosTecnicos(this.conocimientosTecnicos);
        actualizarUsuarioDatosHojaDeVidaComando.setCargo(this.cargo);
        actualizarUsuarioDatosHojaDeVidaComando.setIdVicepresidencia(this.idVicepresidencia);
        actualizarUsuarioDatosHojaDeVidaComando.setFechaIngreso(this.fechaIngreso);
        actualizarUsuarioDatosHojaDeVidaComando.setCapacidad(this.capacidad);

        return actualizarUsuarioDatosHojaDeVidaComando;
    }
}
