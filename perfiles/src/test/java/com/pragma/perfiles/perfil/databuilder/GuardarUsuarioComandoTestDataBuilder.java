package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.perfil.aplicacion.comando.GuardarUsuarioComando;
import com.pragma.perfiles.perfil.aplicacion.comando.GuardarUsuarioNuevoComando;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

public class GuardarUsuarioComandoTestDataBuilder {

    public static final String GOOGLE_ID = "1234prueba1234";
    public static final String NOMBRES = "Prueba";
    public static final String APELLIDOS = "Probados";
    public static final String CORREO = "elpruebas.munozm@pragma.com.co";
    public static final int DIAS_GANADOS = 1;
    public static final int DIAS_TOMADOS = 1;

    String googleId;
    String nombres;
    String apellidos;
    private String correo;
    private int diasGanados;
    private int diasTomados;

    public GuardarUsuarioComandoTestDataBuilder() {
        this.googleId = GOOGLE_ID;
        this.nombres = NOMBRES;
        this.apellidos = APELLIDOS;
        this.correo = CORREO;
        this.diasGanados = DIAS_GANADOS;
        this.diasTomados = DIAS_TOMADOS;
    }

    public GuardarUsuarioNuevoComando buildGuardarUsuarioNuevoComando() {
        GuardarUsuarioNuevoComando guardarUsuarioNuevoComando = new GuardarUsuarioNuevoComando();
        guardarUsuarioNuevoComando.setGoogleId(this.googleId);
        guardarUsuarioNuevoComando.setNombres(this.nombres);
        guardarUsuarioNuevoComando.setApellidos(this.apellidos);
        guardarUsuarioNuevoComando.setCorreoEmpresarial(this.correo);
        guardarUsuarioNuevoComando.setDiasGanados(this.diasGanados);
        guardarUsuarioNuevoComando.setDiasTomados(this.diasTomados);
        return guardarUsuarioNuevoComando;
    }

}
