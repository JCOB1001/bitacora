package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuarioFiltroCorreoComando;
import com.pragma.perfiles.perfil.aplicacion.comando.ObtenerUsuariosFiltroComando;
import lombok.Data;

@Data
public class ObtenerUsuarioPorCorreoComandoTestDataBuilder {

    private String correoEmpresarial;
    private String correoEmpresarialNoExistente;

    public ObtenerUsuarioPorCorreoComandoTestDataBuilder(){
        this.correoEmpresarial="pablo.munozm@pragma.com.co";
        this.correoEmpresarialNoExistente="correonoexistente@pragma.com.co";
    }

    public ObtenerUsuarioFiltroCorreoComando builObtenerdUsuarioFiltroCorreoComandoExistentes(){
        return new ObtenerUsuarioFiltroCorreoComando(this.correoEmpresarial);
    }

    public ObtenerUsuarioFiltroCorreoComando builObtenerdUsuarioFiltroCorreoComandoNoExistentes(){
        return new ObtenerUsuarioFiltroCorreoComando(this.correoEmpresarialNoExistente);
    }
}
