package com.pragma.perfiles.perfil.infraestructura;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.Genero;
import com.pragma.perfiles.informacion_familiar.dominio.modelo.InformacionFamiliar;
import com.pragma.perfiles.informacion_familiar.dominio.useCase.InformacionFamiliarCrudUseCase;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import com.pragma.perfiles.perfil.databuilder.ActualizarInformacionFamiliarComandoTestDataBuilder;
import com.pragma.perfiles.informacion_familiar.aplicacion.comando.ActualizarInformacionFamiliarComando;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
 class ControladorActualizarInformacionFamiliarTest {

    public static final String RUTA_CONTROLADOR = "/informacion-familiar";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String ID_INFORMACION_FAMILIAR_EXISTENTE="4";
    public static final String ID_INFORMACION_FAMILIAR_NO_EXISTENTE="5";
    public static final String AUTHORIZATION_HEADER_TOKEN = "eyJraWQiOiJlcERtUHA2TUdnVVNtWFN6RktoV2VjcVRLQ0NJUWVHT0t3ZDl" +
            "TOWdkV3RzPSIsImFsZyI6IlJTMjU2In0.eyJhdF9oYXNoIjoiWFEwZnJuT2NMdkFndnd1ZjJSWXk5QSIsInN1YiI6ImNlODczOWI0LWMwZT" +
            "UtNGU1OS04NWEzLTVlNTBjMGY4NGY1OCIsImNvZ25pdG86Z3JvdXBzIjpbInVzLWVhc3QtMV9Ud3N5d2ZySURfR29vZ2xlIiwiZmFjdHVyY" +
            "WNpb25fYWRtaW5pc3RyYWRvciJdLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0" +
            "LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xX1R3c3l3ZnJJRCIsImNvZ25pdG86dXNlcm5hbWUiOiJHb29nbGVfMTExODM5MDg5NDk1Nzg" +
            "0ODU0OTc0IiwiYXVkIjoiNTlydGkwMTQ5c2xoaXZjdnQzZ2YxcTE1MmMiLCJpZGVudGl0aWVzIjpbeyJ1c2VySWQiOiIxMTE4MzkwODk0OT" +
            "U3ODQ4NTQ5NzQiLCJwcm92aWRlck5hbWUiOiJHb29nbGUiLCJwcm92aWRlclR5cGUiOiJHb29nbGUiLCJpc3N1ZXIiOm51bGwsInByaW1hc" +
            "nkiOiJ0cnVlIiwiZGF0ZUNyZWF0ZWQiOiIxNjE3ODkwMTM1MTMwIn1dLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTYyNTc3MDM2" +
            "NSwibmFtZSI6Ilt7XCJtZXRhZGF0YVwiOntcInByaW1hcnlcIjp0cnVlLFwic291cmNlXCI6e1widHlwZVwiOlwiUFJPRklMRVwiLFwiaWR" +
            "cIjpcIjExMTgzOTA4OTQ5NTc4NDg1NDk3NFwifX0sXCJkaXNwbGF5TmFtZVwiOlwiQ3Jpc3RpYW4gQ2FtaWxvIE11w7HDs3ogQXJhbmdvXC" +
            "IsXCJmYW1pbHlOYW1lXCI6XCJNdcOxw7N6IEFyYW5nb1wiLFwiZ2l2ZW5OYW1lXCI6XCJDcmlzdGlhbiBDYW1pbG9cIixcImRpc3BsYXlOY" +
            "W1lTGFzdEZpcnN0XCI6XCJNdcOxw7N6IEFyYW5nbywgQ3Jpc3RpYW4gQ2FtaWxvXCIsXCJ1bnN0cnVjdHVyZWROYW1lXCI6XCJDcmlzdGlh" +
            "biBDYW1pbG8gTXXDscOzeiBBcmFuZ29cIn0se1wibWV0YWRhdGFcIjp7XCJzb3VyY2VcIjp7XCJ0eXBlXCI6XCJET01BSU5fUFJPRklMRVw" +
            "iLFwiaWRcIjpcIjExMTgzOTA4OTQ5NTc4NDg1NDk3NFwifX0sXCJkaXNwbGF5TmFtZVwiOlwiQ3Jpc3RpYW4gQ2FtaWxvIE11w7HDs3ogQX" +
            "JhbmdvXCIsXCJmYW1pbHlOYW1lXCI6XCJNdcOxw7N6IEFyYW5nb1wiLFwiZ2l2ZW5OYW1lXCI6XCJDcmlzdGlhbiBDYW1pbG9cIixcImRpc" +
            "3BsYXlOYW1lTGFzdEZpcnN0XCI6XCJDcmlzdGlhbiBDYW1pbG8gTXXDscOzeiBBcmFuZ29cIixcInVuc3RydWN0dXJlZE5hbWVcIjpcIkNy" +
            "aXN0aWFuIENhbWlsbyBNdcOxw7N6IEFyYW5nb1wifV0iLCJleHAiOjE2MjcwNzE3NTcsImlhdCI6MTYyNzA2ODE1NywiZW1haWwiOiJjcml" +
            "zdGlhbmMubXVub3ptQHByYWdtYS5jb20uY28ifQ.fVqTl_ge5d_yWZLJaQgzWOyLHEYHuUxd8dEjXD4_NM__YbpV85Ey16LBlfPBrzqHeG7" +
            "CiqE5WJgm9jDUT4GufsYFNY9VWrtpnrQisJ_cZV8Pa2lWpBbui-IGdJshd1Bvw2ys3pYjLQ8ADWUUWVjN2kjHHGaj7DkKutH-AJV4mqHtnC" +
            "Oeb-RMtAWL3G-S27loHtuTL1IEkBj455S6L8-gH3UbG_ucA9-wTm2Nadp6fU9zDfn79lNWqMF3lpGGkcfSnjfkqaaYWhI46FOLYI01iq_JZ" +
            "o3NSZXgB70pNRBuTGkchmA45ePbnGQ0-n5eqoqVm7fhyq-qeIVjVGR4B2bBjg";


    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private InformacionFamiliarCrudUseCase informacionFamiliarCrudUseCase;

    @Test
    void ActualizarInformacioFamiliarExistente() throws Exception {
        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR+"/{idInformacionFamiliar}")
                .build();

        ActualizarInformacionFamiliarComando informacionFamiliarComando=new ActualizarInformacionFamiliarComandoTestDataBuilder().buildActualizarInformacionFamiliarComando();

        Mockito.when(informacionFamiliarCrudUseCase.obtenerPorId(Mockito.any())).thenReturn(new InformacionFamiliar("12",
                "test", "test", LocalDate.of(1995,12,1), new Parentesco(123L, "test"), Genero.FEMENINO, "1243err",true));

        mvc.perform(MockMvcRequestBuilders
                .put(uri.toUriString(),ID_INFORMACION_FAMILIAR_EXISTENTE)
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(informacionFamiliarComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    void ActualizarInformacioFamiliarNoExistente() throws Exception {
        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR+"/{idInformacionFamiliar}")
                .build();

        ActualizarInformacionFamiliarComando informacionFamiliarComando=new ActualizarInformacionFamiliarComandoTestDataBuilder().buildActualizarInformacionFamiliarComando();

        mvc.perform(MockMvcRequestBuilders
                .put(uri.toUriString(),ID_INFORMACION_FAMILIAR_NO_EXISTENTE)
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(informacionFamiliarComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    void ActualizarInformacioFamiliarConError() throws Exception {
        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR+"/{idInformacionFamiliar}")
                .build();

        ActualizarInformacionFamiliarComando informacionFamiliarComando=new ActualizarInformacionFamiliarComandoTestDataBuilder().buildActualizarInformacionFamiliarComandoConError();

        mvc.perform(MockMvcRequestBuilders
                .put(uri.toUriString(),ID_INFORMACION_FAMILIAR_EXISTENTE)
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(informacionFamiliarComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    void ActualizarInformacioFamiliarConUsuarioNoexistente() throws Exception {
        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR+"/{idInformacionFamiliar}")
                .build();

        ActualizarInformacionFamiliarComando informacionFamiliarComando=new ActualizarInformacionFamiliarComandoTestDataBuilder()
                .buildActualizarInformacionFamiliarComandoCoUsuarioNoExistente();

        mvc.perform(MockMvcRequestBuilders
                .put(uri.toUriString(),ID_INFORMACION_FAMILIAR_EXISTENTE)
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(informacionFamiliarComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(MockMvcResultHandlers.print());


}


}
