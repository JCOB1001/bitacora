package com.pragma.perfiles.perfil.databuilder;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.LinkedMultiValueMap;

public class ObtenerUsuariosFiltroComandoTestDataBuilder {

    public static final int NUMERO_PAGINA = 0;
    public static final int TOTAL_ELEMENTOS = 1;
    public static final String IDENTIFICACION = "123";
    public static final Boolean FECHA_CREACION_ASCENDENTE = true;
    public static final String NOMBRES = "pedro";

    private String identificacion;
    private Boolean fechaCreacionAscendente;
    private String nombres;

    public ObtenerUsuariosFiltroComandoTestDataBuilder() {
        this.identificacion = IDENTIFICACION;
        this.fechaCreacionAscendente = FECHA_CREACION_ASCENDENTE;
        this.nombres = NOMBRES;
    }

    public LinkedMultiValueMap<String, String> buildObtenerUsuariosFiltroComando() {
        LinkedMultiValueMap<String, String> linkedMultiValueMapObtenerUsuariosFiltroComando = new LinkedMultiValueMap<>();
        linkedMultiValueMapObtenerUsuariosFiltroComando.add("numeroPagina", String.valueOf(NUMERO_PAGINA));
        linkedMultiValueMapObtenerUsuariosFiltroComando.add("totalElementos", String.valueOf(TOTAL_ELEMENTOS));
        linkedMultiValueMapObtenerUsuariosFiltroComando.add("identificacion", String.valueOf(this.identificacion));
        linkedMultiValueMapObtenerUsuariosFiltroComando.add("nombres", String.valueOf(this.nombres));
        linkedMultiValueMapObtenerUsuariosFiltroComando.add("fechaCreacionAscendente", String.valueOf(this.fechaCreacionAscendente));
        return linkedMultiValueMapObtenerUsuariosFiltroComando;
    }

}
