package com.pragma.perfiles.perfil.databuilder;

import com.pragma.perfiles.perfil.aplicacion.comando.ActualizarUsuarioComandoSistemaExterno;

public class ActualizarUsuarioSistemaExternoTestDataBuilder {

    public static final String TIPOIDENTIFICACION = "Cédula de ciudadanía";
    public static final String IDENTIFICACION = "233243546";
    public static final String NOMBRES = "Diego Maradona";
    public static final String APELLIDOS = "Molano Manquillo";
    public static final String AREA = "Talent Pool";
    public static final String CARGONOMINA = "Desarrollador de software";
    public static final String CHAPTER = "";
    public static final String TECNOLOGIAPRINCIPAL = "";
    public static final String CLASIFICACION = "";
    public static final String CORREO = "diego.jaramillo@pragma.com.co";
    public static final String SEXO = "Masculino";
    public static final String REFERIDO = "";
    public static final String TIPOCONTRATO = "término indefinido";
    public static final String FECHAINICIOCONTRATOACTUAL = "2021-11-16";
    public static final String FECHANACIMIENTO = "1990-11-10";
    public static final String SALARIOBASICOORDINARIO = "3000000";
    public static final String SALARIOBASICOINTEGRAL = "0";
    public static final String NOCONSTITUTIVOSSALARIO = "500000";
    public static final String MONEDA = "";
    public static final String PROFESION = "INGENIERÍA DE SISTEMAS";
    public static final String TARJETAPROFESIONAL = "19255-424056";
    public static final String PASAPORTE = "";
    public static final String FECHAINICIORELACION = "1970-01-01";
    public static final String ESTADOPERIODOPRUEBA = "";
    public static final String EVALUADORPERIODOPRUEBA = "";

    private String tipoIdentificacion;
    private String identificacion;
    private String nombres;
    private String apellidos;
    private String area;
    private String cargoNomina;
    private String chapter;
    private String tecnologiaPrincipal;
    private String clasificacion;
    private String correo;
    private String sexo;
    private String referido;
    private String tipoContrato;
    private String fechaInicioContratoActual;
    private String fechaNacimiento;
    private String salarioBasicoOrdinario;
    private String salarioBasicoIntegral;
    private String noConstitutivosSalario;
    private String moneda;
    private String profesion;
    private String tarjetaProfesional;
    private String pasaporte;
    private String fechaInicioRelacion;
    private String estadoPeriodoPrueba;
    private String evaluadorPeriodoPrueba;

    public ActualizarUsuarioSistemaExternoTestDataBuilder() {
        this.tipoIdentificacion = TIPOIDENTIFICACION;
        this.identificacion = IDENTIFICACION;
        this.nombres = NOMBRES;
        this.apellidos = APELLIDOS;
        this.area = AREA;
        this.cargoNomina = CARGONOMINA;
        this.chapter = CHAPTER;
        this.tecnologiaPrincipal = TECNOLOGIAPRINCIPAL;
        this.clasificacion = CLASIFICACION;
        this.correo = CORREO;
        this.sexo = SEXO;
        this.referido = REFERIDO;
        this.tipoContrato = TIPOCONTRATO;
        this.fechaInicioContratoActual = FECHAINICIOCONTRATOACTUAL;
        this.fechaNacimiento = FECHANACIMIENTO;
        this.salarioBasicoOrdinario = SALARIOBASICOORDINARIO;
        this.salarioBasicoIntegral = SALARIOBASICOINTEGRAL;
        this.noConstitutivosSalario = NOCONSTITUTIVOSSALARIO;
        this.moneda = MONEDA;
        this.profesion = PROFESION;
        this.tarjetaProfesional = TARJETAPROFESIONAL;
        this.pasaporte = PASAPORTE;
        this.fechaInicioRelacion = FECHAINICIORELACION;
        this.estadoPeriodoPrueba = ESTADOPERIODOPRUEBA;
        this.evaluadorPeriodoPrueba = EVALUADORPERIODOPRUEBA;
    }

    public ActualizarUsuarioComandoSistemaExterno buildActualizarUsuarioComandoSistemaExternoComando() {

        ActualizarUsuarioComandoSistemaExterno actualizarUsuarioComando = new ActualizarUsuarioComandoSistemaExterno();

        actualizarUsuarioComando.setTipoIdentificacion(this.tipoIdentificacion);
        actualizarUsuarioComando.setIdentificacion(this.identificacion);
        actualizarUsuarioComando.setNombres(this.nombres);
        actualizarUsuarioComando.setApellidos(this.apellidos);
        actualizarUsuarioComando.setArea(this.area);
        actualizarUsuarioComando.setCargoNomina(this.cargoNomina);
        actualizarUsuarioComando.setChapter(this.chapter);
        actualizarUsuarioComando.setTecnologiaPrincipal(this.tecnologiaPrincipal);
        actualizarUsuarioComando.setClasificacion(this.clasificacion);
        actualizarUsuarioComando.setCorreo(this.correo);
        actualizarUsuarioComando.setSexo(this.sexo);
        actualizarUsuarioComando.setReferido(this.referido);
        actualizarUsuarioComando.setTipoContrato(this.tipoContrato);
        actualizarUsuarioComando.setFechaInicioContratoActual(this.fechaInicioContratoActual);
        actualizarUsuarioComando.setFechaNacimiento(this.fechaNacimiento);
        actualizarUsuarioComando.setSalarioBasicoOrdinario(this.salarioBasicoOrdinario);
        actualizarUsuarioComando.setSalarioBasicoIntegral(this.salarioBasicoIntegral);
        actualizarUsuarioComando.setNoConstitutivosSalario(this.noConstitutivosSalario);
        actualizarUsuarioComando.setMoneda(this.moneda);
        actualizarUsuarioComando.setProfesion(this.profesion);
        actualizarUsuarioComando.setTarjetaProfesional(this.tarjetaProfesional);
        actualizarUsuarioComando.setPasaporte(this.pasaporte);
        actualizarUsuarioComando.setFechaInicioRelacion(this.fechaInicioRelacion);
        actualizarUsuarioComando.setEstadoPeriodoPrueba(this.estadoPeriodoPrueba);
        actualizarUsuarioComando.setEvaluadorPeriodoPrueba(this.evaluadorPeriodoPrueba);

        return actualizarUsuarioComando;
    }
}
