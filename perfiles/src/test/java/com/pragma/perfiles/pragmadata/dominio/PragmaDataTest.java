package com.pragma.perfiles.pragmadata.dominio;

import com.pragma.perfiles.perfil.dominio.modelo.EstadoProfesion;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstadoCivil;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGrupoSanguineo;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioTallaCamisa;
import com.pragma.perfiles.pragmadata.databuilder.PragmaDataBuilder;
import com.pragma.perfiles.pragmadata.dominio.modelo.PragmaData;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class PragmaDataTest {

    private final String AVATAR_PATH = "/image";
    private final String NOMBRES="Elena";
    private final String APELLIDOS="Mendez Soria";
    private final String NUMERO_DOCUMENTO= "1017224987";
    private final String PRAGMA_EMAIL= "SA@pragma.com.co";
    private final String TIPO_DOCUMENTO = "CC";
    private final String ROL = "ADMIN";
    private final String PROFESION = "INGENIERO";
    private final String DIRECCION = "CRA 20 ";
    private final String CORREO_PERSONAL = "SA@hotmail.com";
    private final String CELULAR = "2345689";
    private final String TELEFONO_FIJO = "32435676";
    private final UsuarioEstadoCivil ESTADO_CIVIL= UsuarioEstadoCivil.CASADO_A;
    private final String PERFIL_PROFESIONAL = "Especialista en búsquedas avanzadas en la web";
    private final String NACIONALIDAD = "Sueco";
    private final String PAIS_RESIDENCIA = "Colombia";
    private final String DEPARTAMENTO_RESIDENCIA = "Antioquia";
    private final String CIUDAD_RESIDENCIA = "Turbo";
    private final String PAIS_NACIMIENTO = "Colombia";
    private final String DEPARTAMENTO_NACIMIENTO = "Antioquia";
    private final String CIUDAD_NACIMIENTO = "Turbo";
    private final LocalDate FECHA_NACIMIENTO = LocalDate.now();
    private final UsuarioGrupoSanguineo SANGRE_RH = UsuarioGrupoSanguineo.AB_NEGATIVO;
    private final String VICEPRESIDENCIA = "Tecnología";
    private final UsuarioTallaCamisa TALLA_CAMISA = UsuarioTallaCamisa.XL;
    private final LocalDate ULTIMA_ACTUALIZACION = LocalDate.now();
    private final String CONTACTO_EMERGENCIA = "Estefania Cárdenas";
    private final String NUMERO_EMERGENCIA = "3143435355";
    private final String PARENTESCO_EMERGENCIA = "Abuela";
    private final String ID_USUARIO = "abc-254sfh";
    private final EstadoProfesion ESTADO_PROFESION = EstadoProfesion.ESTUDIANDO;
    private final String SEMESTRE = "1";
    private final String TARJETA_PROFESIONAL = "Y-grados 2022-1";
    private final LocalDate FECHA_INGRESO = LocalDate.now();
    private final UsuarioEstado ESTADO = UsuarioEstado.INACTIVO;

    private String EMPRESA = "Pragma";
    @Test
    void testCrearPragmaData(){
        PragmaDataBuilder pragmaDataBuilder = new PragmaDataBuilder();
        PragmaData pragmaData = pragmaDataBuilder.crearPragmaData();
        assertEquals(pragmaData.getCorreoEmpresarial(), PRAGMA_EMAIL);
        assertEquals(pragmaData.getApellidos(), APELLIDOS);
        assertEquals(pragmaData.getTelefonoCelular(), CELULAR);
        assertEquals(pragmaData.getCiudadNacimiento(),CIUDAD_NACIMIENTO);
        assertEquals(pragmaData.getCiudadResidencia(),CIUDAD_RESIDENCIA);
        assertEquals(pragmaData.getNombreContactoEmergencia(), CONTACTO_EMERGENCIA);
        assertEquals(pragmaData.getCorreoPersonal(),CORREO_PERSONAL);
        assertEquals(pragmaData.getDepartamentoNacimiento(),DEPARTAMENTO_NACIMIENTO);
        assertEquals(pragmaData.getDepartamentoResidencia(), DEPARTAMENTO_RESIDENCIA);
        assertEquals(pragmaData.getDireccion(),DIRECCION);
        assertEquals(pragmaData.getEstadoCivil() ,ESTADO_CIVIL);
        assertEquals(pragmaData.getId() ,ID_USUARIO);
        assertEquals(pragmaData.getImagenPerfil() ,AVATAR_PATH);
        assertEquals(pragmaData.getVicepresidencia(), VICEPRESIDENCIA);
        assertEquals(pragmaData.getFechaUltimaActualizacion(), ULTIMA_ACTUALIZACION);
        assertEquals(pragmaData.getGrupoSanguineo(), SANGRE_RH);
        assertEquals(pragmaData.getTallaCamisa(), TALLA_CAMISA);
        assertEquals(pragmaData.getCargo(), ROL);
        assertEquals(pragmaData.getPerfilProfesional(), PERFIL_PROFESIONAL);
        assertEquals(pragmaData.getPaisResidencia(), PAIS_RESIDENCIA);
        assertEquals(pragmaData.getPaisNacimiento(),PAIS_NACIMIENTO);
        assertEquals(pragmaData.getNombres(), NOMBRES);
        assertEquals(pragmaData.getTelefonoFijo(), TELEFONO_FIJO);
        assertEquals(pragmaData.getIdentificacion(), NUMERO_DOCUMENTO);
        assertEquals(pragmaData.getTipoDocumento(), TIPO_DOCUMENTO);
        assertEquals(pragmaData.getProfesion(), PROFESION);
        assertEquals(pragmaData.getNacionalidad(), NACIONALIDAD);
        assertEquals(pragmaData.getParentescoContactoEmergencia(),PARENTESCO_EMERGENCIA);
        assertEquals(pragmaData.getTelefonoContactoEmergencia(), NUMERO_EMERGENCIA);
        assertEquals(pragmaData.getFechaNacimiento(), FECHA_NACIMIENTO);
        assertEquals(pragmaData.getEstadoProfesion(), ESTADO_PROFESION);
        assertEquals(pragmaData.getSemestre(), SEMESTRE);
        assertEquals(pragmaData.getTarjetaProfesional(), TARJETA_PROFESIONAL);
        assertEquals(pragmaData.getFechaIngreso(), FECHA_INGRESO);
        assertEquals(pragmaData.getEstado(), ESTADO);
        assertEquals(pragmaData.getEmpresa(),EMPRESA);
    }
}
