package com.pragma.perfiles.pragmadata.databuilder;

import com.pragma.perfiles.perfil.dominio.modelo.EstadoProfesion;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstado;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioEstadoCivil;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioGrupoSanguineo;
import com.pragma.perfiles.perfil.dominio.modelo.UsuarioTallaCamisa;
import com.pragma.perfiles.pragmadata.dominio.modelo.PragmaData;

import java.time.LocalDate;

public class PragmaDataBuilder {

    private final String AVATAR_PATH = "/image";
    private final String NOMBRES="Elena";
    private final String APELLIDOS="Mendez Soria";
    private final String NUMERO_DOCUMENTO= "1017224987";
    private final String PRAGMA_EMAIL= "SA@pragma.com.co";
    private final String TIPO_DOCUMENTO = "CC";
    private final String ROL = "ADMIN";
    private final String PROFESION = "INGENIERO";
    private final String DIRECCION = "CRA 20 ";
    private final String CORREO_PERSONAL = "SA@hotmail.com";
    private final String CELULAR = "2345689";
    private final String TELEFONO_FIJO = "32435676";
    private final UsuarioEstadoCivil ESTADO_CIVIL= UsuarioEstadoCivil.CASADO_A;
    private final String PERFIL_PROFESIONAL = "Especialista en búsquedas avanzadas en la web";
    private final String NACIONALIDAD = "Sueco";
    private final String PAIS_RESIDENCIA = "Colombia";
    private final String DEPARTAMENTO_RESIDENCIA = "Antioquia";
    private final String CIUDAD_RESIDENCIA = "Turbo";
    private final String PAIS_NACIMIENTO = "Colombia";
    private final String DEPARTAMENTO_NACIMIENTO = "Antioquia";
    private final String CIUDAD_NACIMIENTO = "Turbo";
    private final LocalDate FECHA_NACIMIENTO = LocalDate.now();
    private final UsuarioGrupoSanguineo SANGRE_RH = UsuarioGrupoSanguineo.AB_NEGATIVO;
    private final String VICEPRESIDENCIA = "Tecnología";
    private final UsuarioTallaCamisa TALLA_CAMISA = UsuarioTallaCamisa.XL;
    private final LocalDate ULTIMA_ACTUALIZACION = LocalDate.now();
    private final String CONTACTO_EMERGENCIA = "Estefania Cárdenas";
    private final String NUMERO_EMERGENCIA = "3143435355";
    private final String PARENTESCO_EMERGENCIA = "Abuela";
    private final String ID_USUARIO = "abc-254sfh";
    private final EstadoProfesion ESTADO_PROFESION = EstadoProfesion.ESTUDIANDO;
    private final String SEMESTRE = "1";
    private final String TARJETA_PROFESIONAL = "Y-grados 2022-1";
    private final int ESTRATO =1;
    private final String DIAS_NO_CELEBRABLES = "dias de la madre/dia del padre";
    private final String FAMILIARES = null;
    private final LocalDate FECHA_INGRESO = LocalDate.now();
    private final String IDIOMAS = null;
    private final UsuarioEstado ESTADO = UsuarioEstado.INACTIVO;
    private String EMPRESA = "Pragma";

    private String imagenPerfil;
    private String numeroDocumento;
    private String correoPragma;
    private String nombres;
    private String apellidos;
    private String tipoDocumento;
    private String rol;
    private String profesion;
    private String direccion;
    private String correoPersonal;
    private String celular;
    private String telefonoFijo;
    private UsuarioEstadoCivil estadoCivil;
    private String perfilProfesional;
    private String nacionalidad;
    private String paisResidencia;
    private String departamentoResidencia;
    private String ciudadResidencia;
    private String paisNacimiento;
    private String departamentoNacimiento;
    private String ciudadNacimiento;
    private LocalDate fechaNacimiento;
    private UsuarioGrupoSanguineo sangreRH;
    private String vicepresidencia;
    private UsuarioTallaCamisa tallaCamisa;
    private LocalDate ultimaActualizacion;
    private String contactoEmergencia;
    private String numeroEmergencia;
    private String parentescoEmergencia;
    private String idUsuario;
    private EstadoProfesion estadoProfesion;
    private String semestre;
    private String tarjetaProfesional;
    private int estrato;
    private String diasNoCelebrables;
    private String familiares;
    private LocalDate fechaIngreso;
    private String idiomas;
    private UsuarioEstado estado;

    private String empresa;

    public  PragmaDataBuilder() {
        this.imagenPerfil = AVATAR_PATH;
        this.numeroDocumento = NUMERO_DOCUMENTO;
        this.correoPragma = PRAGMA_EMAIL;
        this.nombres = NOMBRES;
        this.apellidos = APELLIDOS;
        this.tipoDocumento = TIPO_DOCUMENTO;
        this.rol = ROL;
        this.profesion = PROFESION;
        this.direccion = DIRECCION;
        this.correoPersonal = CORREO_PERSONAL;
        this.celular = CELULAR;
        this.telefonoFijo = TELEFONO_FIJO;
        this.estadoCivil = ESTADO_CIVIL;
        this.perfilProfesional = PERFIL_PROFESIONAL;
        this.nacionalidad = NACIONALIDAD;
        this.paisResidencia = PAIS_RESIDENCIA;
        this.departamentoResidencia = DEPARTAMENTO_RESIDENCIA;
        this.ciudadResidencia = CIUDAD_RESIDENCIA;
        this.paisNacimiento = PAIS_NACIMIENTO;
        this.departamentoNacimiento = DEPARTAMENTO_NACIMIENTO;
        this.ciudadNacimiento = CIUDAD_NACIMIENTO;
        this.fechaNacimiento = FECHA_NACIMIENTO;
        this.sangreRH = SANGRE_RH;
        this.vicepresidencia = VICEPRESIDENCIA;
        this.tallaCamisa = TALLA_CAMISA;
        this.ultimaActualizacion = ULTIMA_ACTUALIZACION;
        this.contactoEmergencia = CONTACTO_EMERGENCIA;
        this.numeroEmergencia = NUMERO_EMERGENCIA;
        this.parentescoEmergencia = PARENTESCO_EMERGENCIA;
        this.idUsuario = ID_USUARIO;
        this.estadoProfesion = ESTADO_PROFESION;
        this.semestre = SEMESTRE;
        this.tarjetaProfesional = TARJETA_PROFESIONAL;
        this.estrato = ESTRATO;
        this.diasNoCelebrables = DIAS_NO_CELEBRABLES;
        this.familiares = FAMILIARES;
        this.fechaIngreso = FECHA_INGRESO;
        this.idiomas = IDIOMAS;
        this.estado = ESTADO;
        this.empresa = EMPRESA;
    }

    public PragmaData crearPragmaData(){
        return new PragmaData(imagenPerfil, numeroDocumento, correoPragma,
                nombres, apellidos, tipoDocumento, rol, profesion, direccion,
                correoPersonal, celular, telefonoFijo, estadoCivil, perfilProfesional,
                nacionalidad, paisResidencia, departamentoResidencia, ciudadResidencia,
                paisNacimiento, departamentoNacimiento, ciudadNacimiento, fechaNacimiento,
                sangreRH, vicepresidencia, tallaCamisa, ultimaActualizacion, contactoEmergencia, numeroEmergencia,
                parentescoEmergencia, ID_USUARIO, estadoProfesion, semestre, tarjetaProfesional, estrato, diasNoCelebrables, familiares, fechaIngreso, idiomas,estado, empresa);
    }
}
