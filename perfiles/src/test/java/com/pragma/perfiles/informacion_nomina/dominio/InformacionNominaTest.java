package com.pragma.perfiles.informacion_nomina.dominio;


import com.pragma.perfiles.informacion_nomina.databuilder.InformacionNominaDataBuilder;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InformacionNominaTest {

    private final String ID="1";
    private final String ID_USUARIO="098644";
    private final String CARGO_NOMINA="AAA";
    private final double SALARIO_BASICO_ORDINARIO=500.0;
    private final double SALARIO_BASICO_INTEGRAL=600.0;
    private final double NO_CONSTITUTIVOS_SALARIO=700.0;
    private final String MONEDA="SOLES";

    public static final double delta = 0.01;

    @Test
    public void testCrearModeloInformacionNomina(){
        InformacionNominaDataBuilder informacionNominaDataBuilder = new InformacionNominaDataBuilder();
        InformacionNomina informacionNomina = informacionNominaDataBuilder.buildInformacionNomina();
        assertEquals(ID, informacionNomina.getId());
        assertEquals(ID_USUARIO, informacionNomina.getIdUsuario());
        assertEquals(CARGO_NOMINA, informacionNomina.getCargoNomina());
        assertEquals(SALARIO_BASICO_ORDINARIO, informacionNomina.getSalarioBasicoOrdinario(),delta);
        assertEquals(SALARIO_BASICO_INTEGRAL, informacionNomina.getSalarioBasicoIntegral(),delta);
        assertEquals(NO_CONSTITUTIVOS_SALARIO, informacionNomina.getNoConstitutivosSalario(),delta);
        assertEquals(MONEDA, informacionNomina.getMoneda());

    }
}
