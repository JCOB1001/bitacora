package com.pragma.perfiles.informacion_nomina.databuilder;

import com.pragma.perfiles.informacion_nomina.aplicacion.comando.ActualizarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.aplicacion.comando.GuardarInformacionNominaComando;
import com.pragma.perfiles.informacion_nomina.dominio.modelo.InformacionNomina;
import com.pragma.perfiles.otros_estudios.aplicacion.comando.GuardarOtrosEstudiosComando;

public class InformacionNominaDataBuilder {
    private final String ID="1";
    private final String ID_USUARIO="098644";
    private final String CARGO_NOMINA="AAA";
    private final double SALARIO_BASICO_ORDINARIO=500.0;
    private final double SALARIO_BASICO_INTEGRAL=600.0;
    private final double NO_CONSTITUTIVOS_SALARIO=700.0;
    private final String MONEDA="SOLES";

    private  String id;
    private  String idUsuario;
    private  String cargoNomina;
    private  double salarioBasicoOrdinario;
    private  double salarioBasicoIntegral;
    private  double noConstitutivosSalario;
    private  String moneda;

    public InformacionNominaDataBuilder() {
        this.id = ID;
        this.idUsuario = ID_USUARIO;
        this.cargoNomina = CARGO_NOMINA;
        this.salarioBasicoOrdinario = SALARIO_BASICO_ORDINARIO;
        this.salarioBasicoIntegral = SALARIO_BASICO_INTEGRAL;
        this.noConstitutivosSalario = NO_CONSTITUTIVOS_SALARIO;
        this.moneda = MONEDA;
    }

    public InformacionNomina buildInformacionNomina(){
        return new InformacionNomina(
                this.id,
                this.idUsuario,
                this.cargoNomina,
                this.salarioBasicoOrdinario,
                this.salarioBasicoIntegral,
                this.noConstitutivosSalario,
                this.moneda
        );
    }
    public GuardarInformacionNominaComando buildGuardarInformacionNominaComando(){
        return new GuardarInformacionNominaComando(
                this.idUsuario,
                this.cargoNomina,
                this.salarioBasicoOrdinario,
                this.salarioBasicoIntegral,
                this.noConstitutivosSalario,
                this.moneda
        );
    }

    public GuardarInformacionNominaComando buildGuardarInformacionNominaComandoNoExistente(){
        return new GuardarInformacionNominaComando(
                "5",
                this.cargoNomina,
                this.salarioBasicoOrdinario,
                this.salarioBasicoIntegral,
                this.noConstitutivosSalario,
                this.moneda
        );
    }

    public ActualizarInformacionNominaComando buildActualizarInformacionNominaComando(){
        return new ActualizarInformacionNominaComando(
                this.idUsuario,
                this.cargoNomina,
                10000.0,
                6000.0,
                this.noConstitutivosSalario,
                this.moneda
        );
    }

    public ActualizarInformacionNominaComando buildActualizarInformacionNominaComandoNoExistente(){
        return new ActualizarInformacionNominaComando(
                "5",
                this.cargoNomina,
                10000.0,
                6000.0,
                this.noConstitutivosSalario,
                this.moneda
        );
    }

}
