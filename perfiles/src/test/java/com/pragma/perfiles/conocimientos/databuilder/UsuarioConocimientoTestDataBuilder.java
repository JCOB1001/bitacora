package com.pragma.perfiles.conocimientos.databuilder;

import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;

import javax.persistence.Column;

public class UsuarioConocimientoTestDataBuilder {

    public static final String ID_USUARIO = "9876554";
    public static final String NOMBRE_SKILL = "Angular";
    public static final String NUEVO_NOMBRE_SKILL = "Python";
    public static final int PUNTAJE = 3;
    public static final int INDICE = 0;

    private String idUsuario;
    private String nombreSkill;
    private String nuevoNombreSkill;
    private int puntaje;
    private int indice;


    public UsuarioConocimientoTestDataBuilder() {
        this.nombreSkill = NOMBRE_SKILL;
        this.nuevoNombreSkill = NUEVO_NOMBRE_SKILL;
        this.puntaje = PUNTAJE;
        this.idUsuario = ID_USUARIO;
        this.indice = INDICE;
    }

    public UsuarioConocimiento builUsuarioConocimiento() {
        return new UsuarioConocimiento(
                "1",
                this.idUsuario,
                this.nombreSkill,
                this.puntaje,
                this.indice
        );
    }

    public GuardarConocimientoComando buildGuardarConocimientoComando() {
        return new GuardarConocimientoComando(this.nuevoNombreSkill);
    }

    public ActualizarUsuarioConocimientoComando buildActualizarUsuarioConocimientoComandoExistente() {
        return new ActualizarUsuarioConocimientoComando(
                this.idUsuario,
                this.nombreSkill,
                this.puntaje
        );
    }

    public ActualizarUsuarioConocimientoComando buildActualizarUsuarioConocimientoComandoNoExistente() {
        return new ActualizarUsuarioConocimientoComando(
                this.idUsuario,
                this.nombreSkill,
                this.puntaje
        );
    }

    public ActualizarUsuarioConocimientoComando buildActualizarUsuarioConocimientoComandoIdUsuarioNoCoincide() {
        return new ActualizarUsuarioConocimientoComando(
                "987655411",
                this.nombreSkill,
                this.puntaje
        );
    }

    public GuardarUsuarioConocimientoComando buildGuardarUsuarioConocimientoComando() {
        return new GuardarUsuarioConocimientoComando(
                this.nombreSkill,
                this.puntaje,
                "01234567891"
        );
    }
    public GuardarUsuarioConocimientoComando buildGuardarUsuarioConocimientoComandoCuadoNoExisteUsuario() {
        return new GuardarUsuarioConocimientoComando(
                this.nombreSkill,
                this.puntaje,
                "01234567891123123123"
        );
    }

    public GuardarUsuarioConocimientoComando buildGuardarUsuarioConocimientoComandoCuandoExisteConocimiento() {
        return new GuardarUsuarioConocimientoComando(
                this.nombreSkill,
                this.puntaje,
                "9876554"
        );
    }


}
