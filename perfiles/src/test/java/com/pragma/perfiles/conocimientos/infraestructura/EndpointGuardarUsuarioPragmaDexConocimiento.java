package com.pragma.perfiles.conocimientos.infraestructura;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.databuilder.UsuarioConocimientoTestDataBuilder;
import com.pragma.perfiles.perfil.databuilder.UsuarioTestDataBuilder;
import com.pragma.perfiles.perfil.dominio.modelo.*;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT)
 class EndpointGuardarUsuarioPragmaDexConocimiento {

    public static final String RUTA_CONTROLADOR = "/conocimientos";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String AUTHORIZATION_HEADER_TOKEN = "eyJraWQiOiJlcERtUHA2TUdnVVNtWFN6RktoV2VjcVRLQ0NJUWVHT0t3ZDlTOWdkV3RzPSIsImFsZyI6IlJTMjU2In0" +
            ".eyJhdF9oYXNoIjoidzl4NnNnNE82ajkyZWp5OXhJeEdoZyIsInN1YiI6IjFmNGM3ZDg2LWNjOGQtNDc0ZS1iNWMyLTg2ZjgxZDI5MDdhOSIsImNvZ25pdG86Z3JvdXBzIjpbInVzLWV" +
            "hc3QtMV9Ud3N5d2ZySURfR29vZ2xlIl0sImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tXC91cy1l" +
            "YXN0LTFfVHdzeXdmcklEIiwiY29nbml0bzp1c2VybmFtZSI6Ikdvb2dsZV8xMTMxMjc2MTEyMjgyMDk3MjkyNTMiLCJhdWQiOiI1OXJ0aTAxNDlzbGhpdmN2dDNnZjFxMTUyYyIsImlkZW" +
            "50aXRpZXMiOlt7InVzZXJJZCI6IjExMzEyNzYxMTIyODIwOTcyOTI1MyIsInByb3ZpZGVyTmFtZSI6Ikdvb2dsZSIsInByb3ZpZGVyVHlwZSI6Ikdvb2dsZSIsImlzc3VlciI6bnVsbCwicH" +
            "JpbWFyeSI6InRydWUiLCJkYXRlQ3JlYXRlZCI6IjE2MjI1NTg0MjI2NjMifV0sInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjI5NzMyNzQ0LCJuYW1lIjoiW3tcIm1ldGFkYXRhXC" +
            "I6e1wicHJpbWFyeVwiOnRydWUsXCJzb3VyY2VcIjp7XCJ0eXBlXCI6XCJQUk9GSUxFXCIsXCJpZFwiOlwiMTEzMTI3NjExMjI4MjA5NzI5MjUzXCJ9fSxcImRpc3BsYXlOYW1lXCI6XCJHaW" +
            "xpYmVydCBBZG9uaXMgTW9yZW5vIEJlY2VycmFcIixcImZhbWlseU5hbWVcIjpcIk1vcmVubyBCZWNlcnJhXCIsXCJnaXZlbk5hbWVcIjpcIkdpbGliZXJ0IEFkb25pc1wiLFwiZGlzcGxheU" +
            "5hbWVMYXN0Rmlyc3RcIjpcIk1vcmVubyBCZWNlcnJhLCBHaWxpYmVydCBBZG9uaXNcIixcInVuc3RydWN0dXJlZE5hbWVcIjpcIkdpbGliZXJ0IEFkb25pcyBNb3Jlbm8gQmVjZXJyYVwifS" +
            "x7XCJtZXRhZGF0YVwiOntcInNvdXJjZVwiOntcInR5cGVcIjpcIkRPTUFJTl9QUk9GSUxFXCIsXCJpZFwiOlwiMTEzMTI3NjExMjI4MjA5NzI5MjUzXCJ9fSxcImRpc3BsYXlOYW1lXCI6XC" +
            "JHaWxpYmVydCBBZG9uaXMgTW9yZW5vIEJlY2VycmFcIixcImZhbWlseU5hbWVcIjpcIk1vcmVubyBCZWNlcnJhXCIsXCJnaXZlbk5hbWVcIjpcIkdpbGliZXJ0IEFkb25pc1wiLFwiZGlzcGxh" +
            "eU5hbWVMYXN0Rmlyc3RcIjpcIkdpbGliZXJ0IEFkb25pcyBNb3Jlbm8gQmVjZXJyYVwiLFwidW5zdHJ1Y3R1cmVkTmFtZVwiOlwiR2lsaWJlcnQgQWRvbmlzIE1vcmVubyBCZWNlcnJhXCJ9XS" +
            "IsImV4cCI6MTYzMDUyMzA4NCwiaWF0IjoxNjMwNTE5NDg0LCJlbWFpbCI6ImdpbGliZXJ0Lm1vcmVub0BwcmFnbWEuY29tLmNvIn0.m7stnjdwDorBBVy8keXQN2DMmtQ4nmQ505_erja5vneV" +
            "O4ixL_sXgDjTRiYkCqQmpNM670EwTT1eONmlE4sfTUFixzTBb4PmBSuQMNiBn40ClRR47DaUJid3l_-HaYZgVtnpdFFCNqs5ArTcm04mPDLB9yQdrIb2BR3CHnV0qivJpr2kxX22GdjSacOzzn" +
            "rGprZYxc01TGfPkSLhEy7rF9Uw7cBU1qV8PeK9zuSYuYYey3AMx1z9tqKpnizH5u6VuT7K2EmcGVNfvuVbYmA_T6-WaehLezpSl6RrHX6dzireZphhlzLABaATEznfdNaJhrJsypag2DrOO6R5UmIHTA";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UsuarioCrudUseCase usuarioCrudUseCase;

    @Test
    @Order(1)
    void tesGuardarUsuarioConocimiento()throws Exception{

        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR )
                .build();
        GuardarUsuarioConocimientoComando guardarUsuarioConocimientoComando =new UsuarioConocimientoTestDataBuilder().buildGuardarUsuarioConocimientoComando();

        Mockito.when(usuarioCrudUseCase.obtenerPorId(Mockito.any())).thenReturn(obtenerUsuario());

        mvc.perform(MockMvcRequestBuilders
                .post(uri.toUriString())
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(guardarUsuarioConocimientoComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dato.indice", Matchers.is(1)))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    @Order(2)
    void tesGuardarUsuarioConocimientoCuandoYaExisteConocimiento()throws Exception{

        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR )
                .build();
        GuardarUsuarioConocimientoComando guardarUsuarioConocimientoComando =new UsuarioConocimientoTestDataBuilder().buildGuardarUsuarioConocimientoComandoCuandoExisteConocimiento();

        Mockito.when(usuarioCrudUseCase.obtenerPorId(Mockito.any())).thenReturn(obtenerUsuario());

        mvc.perform(MockMvcRequestBuilders
                .post(uri.toUriString())
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(guardarUsuarioConocimientoComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    @Order(3)
    void tesGuardarUsuarioConocimientoCuandoNoExisteUsuario()throws Exception{

        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR )
                .build();
        GuardarUsuarioConocimientoComando guardarUsuarioConocimientoComando =new UsuarioConocimientoTestDataBuilder().buildGuardarUsuarioConocimientoComandoCuadoNoExisteUsuario();

        mvc.perform(MockMvcRequestBuilders
                .post(uri.toUriString())
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(guardarUsuarioConocimientoComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(MockMvcResultHandlers.print());
    }

    private Usuario obtenerUsuario() {
        return new UsuarioTestDataBuilder().buildUsuario();
    }
}
