package com.pragma.perfiles.conocimientos.databuilder;

import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarIndicesUsuarioConocimientosComando;
import com.pragma.perfiles.conocimientos.dominio.modelo.UsuarioConocimiento;

import java.util.Arrays;
import java.util.List;

public class IndexUsuarioConocimientoTestDataBuilder {

    public static final String ID_USUARIO = "9876554";
    public static final String NOMBRE_SKILL = "Angular";
    public static final int PUNTAJE = 3;
    public static final int INDICE = 0;

    private String idUsuario;
    private String nombreSkill;
    private List<String> skills;
    private int puntaje;
    private int indice;


    public IndexUsuarioConocimientoTestDataBuilder() {
        this.nombreSkill = NOMBRE_SKILL;
        this.puntaje = PUNTAJE;
        this.idUsuario = ID_USUARIO;
        this.indice = INDICE;
        this.skills = Arrays.asList(NOMBRE_SKILL);
    }

    public UsuarioConocimiento builUsuarioConocimiento() {
        return new UsuarioConocimiento(
                "1",
                this.idUsuario,
                this.nombreSkill,
                this.puntaje,
                this.indice
        );
    }

    public ActualizarIndicesUsuarioConocimientosComando buildActualizarIndicesUsuarioConocimientosComando() {
        return new ActualizarIndicesUsuarioConocimientosComando(
            this.idUsuario,
            this.skills
        );
    }

    public ActualizarIndicesUsuarioConocimientosComando buildActualizarIndicesUsuarioConocimientosComandoUsuarioInexistente() {
        return new ActualizarIndicesUsuarioConocimientosComando(
            "1",
            this.skills
        );
    }

    public ActualizarIndicesUsuarioConocimientosComando buildActualizarIndicesUsuarioConocimientosComandoConocimientoInexistente() {
        return new ActualizarIndicesUsuarioConocimientosComando(
            this.idUsuario,
            Arrays.asList()
        );
    }
}
