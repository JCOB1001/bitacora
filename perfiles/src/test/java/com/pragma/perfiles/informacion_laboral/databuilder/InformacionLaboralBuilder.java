package com.pragma.perfiles.informacion_laboral.databuilder;

import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.comando.GuardarExperienciaLaboralPragmaComando;
import com.pragma.perfiles.informacion_laboral.aplicaion.comando.ActualizarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.aplicaion.comando.GuardarInformacionLaboralComando;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.EstadoPeriodoPrueba;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;

import java.time.LocalDate;

public class InformacionLaboralBuilder {

    private final String USUARIO_ID = "2345rfdfgh654w";
    private final String TIPO_CONTRATO = "indefinido";
    private final LocalDate FECHA_INICIO_CONTRATO_ACTUAL = LocalDate.of(2020, 07, 12);
    private final String REFERIDO = "test";
    private final String CLIENTE = "test";
    private final String PROYECTO = "test";
    private final LocalDate FECHA_INICIO_RELACION = LocalDate.of(2020, 07, 12);
    private final EstadoPeriodoPrueba ESTADO_PERIODO_PRUEBA = EstadoPeriodoPrueba.REALIZADO;
    private final String EVALUADOR_PERIODO_PRUEBA = "test";

    private String usuarioId;
    private String tipoContrato;
    private LocalDate fechaInicioContratoActual;
    private String referido;
    private String cliente;
    private String proyecto;
    private LocalDate fechaInicioRelacion;
    private EstadoPeriodoPrueba estadoPeriodoPrueba;
    private String evaluadorPeriodoPrueba;

    public InformacionLaboralBuilder() {
        this.usuarioId = USUARIO_ID;
        this.tipoContrato = TIPO_CONTRATO;
        this.fechaInicioContratoActual = FECHA_INICIO_CONTRATO_ACTUAL;
        this.referido = REFERIDO;
        this.cliente = CLIENTE;
        this.proyecto = PROYECTO;
        this.fechaInicioRelacion = FECHA_INICIO_RELACION;
        this.estadoPeriodoPrueba = ESTADO_PERIODO_PRUEBA;
        this.evaluadorPeriodoPrueba = EVALUADOR_PERIODO_PRUEBA;
    }

    public InformacionLaboral crearInformacionLaboral() {
        return new InformacionLaboral("e45fghu6reewe", usuarioId, tipoContrato, fechaInicioContratoActual,
                referido,
                cliente,
                proyecto,
                fechaInicioRelacion,
                estadoPeriodoPrueba,
                evaluadorPeriodoPrueba);
    }

    public GuardarInformacionLaboralComando buildGuardarInformacionLaboralComando(){
        return new GuardarInformacionLaboralComando("098644", "fijo",
                LocalDate.of(2020, 07, 12), "test", "test", "test",
                LocalDate.of(2020, 07, 12), null, "test");
    }

    public GuardarInformacionLaboralComando buildNoGuardarInformacionLaboralCuandoNoExisteUsuarioComando(){
        return new GuardarInformacionLaboralComando("fvghrte4567890", "fijo",
                LocalDate.of(2020, 07, 12), "test", "test", "test",
                LocalDate.of(2020, 07, 12), null, "test");
    }

    public ActualizarInformacionLaboralComando buildActualizarInformacionLaboralComando(){
        return new ActualizarInformacionLaboralComando("098644", "fijo",
                LocalDate.of(2020, 07, 12), "pepito", "pepito", "pepito",
                LocalDate.of(2020, 07, 12), null, "pepito");
    }

    public ActualizarInformacionLaboralComando buildNoActualizarInformacionLaboralComandoCuandoNoExiste(){
        return new ActualizarInformacionLaboralComando("fghuji86754", "fijo",
                LocalDate.of(2020, 07, 12), "pepito", "pepito", "pepito",
                LocalDate.of(2020, 07, 12), null, "pepito");
    }
}
