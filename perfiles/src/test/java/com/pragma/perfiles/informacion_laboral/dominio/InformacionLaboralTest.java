package com.pragma.perfiles.informacion_laboral.dominio;

import com.pragma.perfiles.informacion_laboral.databuilder.InformacionLaboralBuilder;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.EstadoPeriodoPrueba;
import com.pragma.perfiles.informacion_laboral.dominio.modelo.InformacionLaboral;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class InformacionLaboralTest {

    private final String USUARIO_ID = "2345rfdfgh654w";
    private final String TIPO_CONTRATO = "indefinido";
    private final LocalDate FECHA_INICIO_CONTRATO_ACTUAL = LocalDate.of(2020, 07, 12);
    private final String REFERIDO = "test";
    private final String CLIENTE = "test";
    private final String PROYECTO = "test";
    private final LocalDate FECHA_INICIO_RELACION = LocalDate.of(2020, 07, 12);
    private final EstadoPeriodoPrueba ESTADO_PERIODO_PRUEBA = EstadoPeriodoPrueba.REALIZADO;
    private final String EVALUADOR_PERIODO_PRUEBA = "test";

    @Test
    void  crearInformacionLaboralTest() {
        InformacionLaboralBuilder informacionLaboralBuilder = new InformacionLaboralBuilder();
        InformacionLaboral informacionLaboral = informacionLaboralBuilder.crearInformacionLaboral();
        assertEquals(informacionLaboral.getUsuarioId(), USUARIO_ID);
        assertEquals(informacionLaboral.getTipoContrato(), TIPO_CONTRATO);
        assertEquals(informacionLaboral.getFechaInicioContratoActual(), FECHA_INICIO_CONTRATO_ACTUAL);
        assertEquals(informacionLaboral.getReferido(), REFERIDO);
        assertEquals(informacionLaboral.getCliente(), CLIENTE);
        assertEquals(informacionLaboral.getProyecto(), PROYECTO);
        assertEquals(informacionLaboral.getFechaInicioRelacion(), FECHA_INICIO_RELACION);
        assertEquals(informacionLaboral.getEstadoPeriodoPrueba(), ESTADO_PERIODO_PRUEBA);
        assertEquals(informacionLaboral.getEvaluadorPeriodoPrueba(), EVALUADOR_PERIODO_PRUEBA);

    }
}
