package com.pragma.perfiles.pragmadex.databuilder;

import com.pragma.perfiles.pragmadex.dominio.modelo.*;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PragmadexDataBuilder {

    private final String NOMBRE_CORREO = "Santiago";
    private final Boolean ADMIN = true;
    private final int MIN_PORCENTAJE = 0;
    private final int MAX_PORCENTAJE = 100;
    private final String SKILL = "Java";
    private final int RATE = 5;
    private final boolean PERSONAL_INFO= true;
    private final boolean LABORAL_INFO= true;
    private final boolean ACADEMIC_INFO= true;
    private final double PROGRESS_BAR = 100.0;
    private final String MESSAGE= "OK";
    private final int STATUS= HttpStatus.OK.value();
    private final int REGISTROS_POR_PAGINA= 8;
    private final Long TOTAL_REGISTROS= Long.valueOf(100);
    private final boolean ULTIMA_PAGINA= true;
    private final List<UsuarioPragmaDex> BODY = new ArrayList<>();

    private final String CORPORTE_EMAIL= "SA@pragma.com.co";
    private final String USERNAME = "Santiago";
    private final String LASTNAME = "Alvarez";
    private final String USER_ID = "111839089495784854974";
    private final List<HabilidadesPragmaDex> SKILLS= new ArrayList<>();
    private final String NUMBER_ID= "1017224987";
    private final List<String> CAPABILITIES= Arrays.asList("Salto");
    private final String AVATAR_PATH = "/image";
    private final String USER_ROLE = "ADMIN";
    private final boolean IS_ACTIVE = true;
    private final InfoEstado INFO_STATUS = new InfoEstado(PERSONAL_INFO, LABORAL_INFO, ACADEMIC_INFO, PROGRESS_BAR);



    private String nombreOrCorreo;
    private boolean admin;
    private int minPorcentaje;
    private int maxPorcentaje;

    private String skillName;
    private int rate;

    private boolean personalInfo;
    private boolean laboralInfo;
    private boolean academicInfo;
    private double progressBar;


    private String message;
    private int status;
    private int registrosPorPagina;
    private Long totalRegistros;
    private boolean ultimaPagina;
    private List<UsuarioPragmaDex> body;

    private String corporateEmail;
    private String userName;
    private String lastName;
    private String userId;
    private List<HabilidadesPragmaDex> skills;
    private String numberId;//cedula
    private List<String> capabilities;
    private String avatarPath;
    private String userRole;
    private boolean isActive;
    private InfoEstado infoStatus;

    public PragmadexDataBuilder() {
        this.nombreOrCorreo = NOMBRE_CORREO;
        this.admin = ADMIN;
        this.minPorcentaje = MIN_PORCENTAJE;
        this.maxPorcentaje = MAX_PORCENTAJE;
        this.skillName = SKILL;
        this.rate = RATE;
        this.personalInfo = PERSONAL_INFO;
        this.laboralInfo = LABORAL_INFO;
        this.academicInfo = ACADEMIC_INFO;
        this.progressBar = PROGRESS_BAR;
        this.message = MESSAGE;
        this.status = STATUS;
        this.registrosPorPagina = REGISTROS_POR_PAGINA;
        this.totalRegistros = TOTAL_REGISTROS;
        this.ultimaPagina = ULTIMA_PAGINA;
        this.body = BODY;
        this.corporateEmail = CORPORTE_EMAIL;
        this.userName = USERNAME;
        this.lastName = LASTNAME;
        this.userId = USER_ID;
        this.skills = SKILLS;
        this.numberId = NUMBER_ID;
        this.capabilities = CAPABILITIES;
        this.avatarPath = AVATAR_PATH;
        this.userRole = USER_ROLE;
        this.isActive = IS_ACTIVE;
        this.infoStatus = INFO_STATUS;
    }

    public InfoEstado buildInfoEstado(){
        return new InfoEstado(personalInfo,
                laboralInfo,
                academicInfo,
                progressBar);
    }
    public Pragmadex buildPragmadex(){
        return new Pragmadex(body);
    }
    public PragmaDexPaginado buildPragmaDexPaginado(){
        return new PragmaDexPaginado(message,
                status, registrosPorPagina,
                totalRegistros,
                ultimaPagina,
                body);
    }
    public UsuarioPragmaDex buildUsuarioPragmaDex (){
        return new UsuarioPragmaDex(corporateEmail,
                userName,
                lastName,
                userId,
                skills,
                numberId,
                capabilities,
                avatarPath,
                userRole,
                isActive,
                infoStatus);
    }
}
