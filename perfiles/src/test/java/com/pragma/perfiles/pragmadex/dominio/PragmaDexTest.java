package com.pragma.perfiles.pragmadex.dominio;

//import com.pragma.perfiles.pragmadex.databuilder.PragmadexDataBuilder;
import com.pragma.perfiles.pragmadex.dominio.modelo.*;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

class PragmaDexTest {

    private final String NOMBRE_CORREO = "Santiago";
    private final Boolean ADMIN = true;
    private final int MIN_PORCENTAJE = 0;
    private final int MAX_PORCENTAJE = 100;
    private final String SKILL = "Java";
    private final int RATE = 5;
    private final boolean PERSONAL_INFO= true;
    private final boolean LABORAL_INFO= true;
    private final boolean ACADEMIC_INFO= true;
    private final double PROGRESS_BAR = 100.0;
    private final String MESSAGE= "OK";
    private final int STATUS= HttpStatus.OK.value();
    private final int REGISTROS_POR_PAGINA= 8;
    private final Long TOTAL_REGISTROS= Long.valueOf(100);
    private final boolean ULTIMA_PAGINA= true;
    private final List<UsuarioPragmaDex> BODY = new ArrayList<>();

    private final String CORPORTE_EMAIL= "SA@pragma.com.co";
    private final String USERNAME = "Santiago";
    private final String LASTNAME = "Alvarez";
    private final String USER_ID = "111839089495784854974";
    private final List<HabilidadesPragmaDex> SKILLS= new ArrayList<>();
    private final String NUMBER_ID= "1017224987";
    private final List<String> CAPABILITIES= Arrays.asList("Salto");
    private final String AVATAR_PATH = "/image";
    private final String USER_ROLE = "ADMIN";
    private final boolean IS_ACTIVE = true;
    private final InfoEstado INFO_STATUS = new InfoEstado(PERSONAL_INFO, LABORAL_INFO, ACADEMIC_INFO, PROGRESS_BAR);

    @Test
    void testCrearModelosPragmaDex(){
        /*PragmadexDataBuilder builder = new PragmadexDataBuilder();
        PragmaDexPaginado pragmaDexPaginado= builder.buildPragmaDexPaginado();
        InfoEstado infoEstado = builder.buildInfoEstado();
        UsuarioPragmaDex usuarioPragmaDex = builder.buildUsuarioPragmaDex();
        Pragmadex pragmadex = builder.buildPragmadex();
        assertEquals(pragmaDexPaginado.getMessage(), MESSAGE);
        assertEquals(pragmaDexPaginado.isUltimaPagina(), ULTIMA_PAGINA);
        assertEquals(pragmaDexPaginado.getBody(), BODY);
        assertEquals(pragmaDexPaginado.getRegistrosPorPagina(), REGISTROS_POR_PAGINA);
        assertEquals(pragmaDexPaginado.getStatus(), STATUS);
        assertEquals(pragmaDexPaginado.getTotalRegistros(), TOTAL_REGISTROS);
        assertEquals(infoEstado.getProgressBar(), PROGRESS_BAR, 0.1);
        assertEquals(infoEstado.isAcademicInfo(), ACADEMIC_INFO);
        assertEquals(infoEstado.isLaboralInfo(), LABORAL_INFO);
        assertEquals(infoEstado.isPersonalInfo(), PERSONAL_INFO);
        assertEquals(usuarioPragmaDex.getAvatarPath(), AVATAR_PATH);
        assertEquals(usuarioPragmaDex.getCapabilities(), CAPABILITIES);
        assertEquals(usuarioPragmaDex.getCorporateEmail(), CORPORTE_EMAIL);
        assertEquals(usuarioPragmaDex.getIsActive(), IS_ACTIVE);
        assertEquals(usuarioPragmaDex.getLastName(), LASTNAME);
        assertEquals(usuarioPragmaDex.getNumberId(), NUMBER_ID);
        assertEquals(usuarioPragmaDex.getSkills(), SKILLS);
        assertEquals(usuarioPragmaDex.getUserName(), USERNAME);
        assertEquals(usuarioPragmaDex.getUserRole(), USER_ROLE);
        assertNull(pragmadex.getMessage());
        assertEquals(pragmadex.getBody(), BODY);*/
    }

}
