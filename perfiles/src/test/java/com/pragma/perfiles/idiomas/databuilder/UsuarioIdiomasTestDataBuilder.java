package com.pragma.perfiles.idiomas.databuilder;

import com.pragma.perfiles.conocimientos.aplicacion.comando.ActualizarUsuarioConocimientoComando;
import com.pragma.perfiles.conocimientos.aplicacion.comando.GuardarUsuarioConocimientoComando;
import com.pragma.perfiles.idiomas.aplicacion.comando.ActualizarUsuarioIdiomasComando;
import com.pragma.perfiles.idiomas.aplicacion.comando.GuardarUsuarioIdiomasComando;

public class UsuarioIdiomasTestDataBuilder {

        public static final String ID_USUARIO = "9876554";
        public static final String ID_IDIOMA = "3";
        public static final String NUEVO_IDIOMA_ = "3";
        public static final int NIVEL = 3;

        private String idUsuario;
        private String idIdioma;
        private int nivel;

        public UsuarioIdiomasTestDataBuilder() {
            this.idIdioma = ID_IDIOMA;
            this.idUsuario = ID_USUARIO;
            this.nivel = NIVEL;
        }


    public GuardarUsuarioIdiomasComando buildGuardarUsuarioIdiomasComando() {
        return new GuardarUsuarioIdiomasComando(
                "111839089495784854975", this.idIdioma, this.nivel, true
                , "test", "A1", "file", "jpg"
        );
    }

    public GuardarUsuarioIdiomasComando buildGuardarUsuarioIdiomasComandoCuandoYaExisteIdioma() {
        return new GuardarUsuarioIdiomasComando(
                this.idUsuario, "3",4
                , true, "", "A1", "s", "s"
        );
    }

   public ActualizarUsuarioIdiomasComando buildActualizarUsuarioIdiomasCuandoIdiomaNoExiste() {
        return new ActualizarUsuarioIdiomasComando(
                this.idUsuario, "8",4

        );
    }

    public ActualizarUsuarioIdiomasComando buildActualizarUsuarioIdiomasComando() {
        return new ActualizarUsuarioIdiomasComando(
                this.idUsuario, this.idIdioma, 5
          );
       }

    //public ActualizarUsuarioIdiomasComando buildActualizarUsuarioIdiomasComandoIdUsuarioNoCoincide() {
    //    return new ActualizarUsuarioIdiomasComando(
    //            this.idUsuario, "3",4

   //         );
   //     }

}





