package com.pragma.perfiles.contrato_historico.dominio;

import com.pragma.perfiles.contrato_historico.databuilder.ContratoHistoricoDataBuilder;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;

import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class ContratoHistoricoTest {
    private final String ID="1";
    private final String IDUSUARIO="9876554";
    private final LocalDate FECHAINICIO =  LocalDate.parse("2020-12-09");
    private final LocalDate FECHAFIN =  LocalDate.parse("2021-02-12");
    private final String IDTIPOCONTRATO="1";
    private final ContratoHistoricoEstado ESTADO=ContratoHistoricoEstado.INACTIVO;

    @Test
    public void testCrearModeloContratoHistorico(){
        ContratoHistoricoDataBuilder contratoHistoricoDataBuilder = new ContratoHistoricoDataBuilder();
        ContratoHistorico contratoHistorico = contratoHistoricoDataBuilder.buildContratoHistorico();

        assertEquals(ID, contratoHistorico.getId());
        assertEquals(IDUSUARIO, contratoHistorico.getIdUsuario());
        assertEquals(FECHAINICIO, contratoHistorico.getFechaInicio());
        assertEquals(FECHAFIN, contratoHistorico.getFechaFin());
        assertEquals(IDTIPOCONTRATO, contratoHistorico.getIdTipoContrato());
        assertEquals(ESTADO, contratoHistorico.getEstado());

    }
}
