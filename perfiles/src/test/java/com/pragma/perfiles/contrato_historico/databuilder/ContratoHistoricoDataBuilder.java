package com.pragma.perfiles.contrato_historico.databuilder;

import com.pragma.perfiles.contrato_historico.aplicacion.comando.ActualizarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.aplicacion.comando.GuardarContratoHistoricoComando;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistorico;
import com.pragma.perfiles.contrato_historico.dominio.modelo.ContratoHistoricoEstado;


import java.time.LocalDate;


public class ContratoHistoricoDataBuilder {
    private final String ID="1";
    private final String IDUSUARIO="9876554";
    private final LocalDate FECHAINICIO =  LocalDate.parse("2020-12-09");
    private final LocalDate FECHAFIN =  LocalDate.parse("2021-02-12");
    private final String IDTIPOCONTRATO="1";

    private final ContratoHistoricoEstado ESTADO=ContratoHistoricoEstado.INACTIVO;


    private String id;
    private String idUsuario;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
    private String idTipoContrato;
    private ContratoHistoricoEstado estado;

    public ContratoHistoricoDataBuilder(){
        this.idUsuario=IDUSUARIO;
        this.fechaInicio = FECHAINICIO;
        this.fechaFin = FECHAFIN;
        this.idTipoContrato = IDTIPOCONTRATO;
        this.estado = ESTADO;
    }

    public ContratoHistorico buildContratoHistorico(){
        return new ContratoHistorico(
                "1",
                this.IDUSUARIO,
                this.FECHAINICIO,
                this.FECHAFIN,
                this.IDTIPOCONTRATO,
                this.ESTADO
        );
    }

    public GuardarContratoHistoricoComando buildGuardarContratoHistoricoComando() {
        return new GuardarContratoHistoricoComando(
                this.idUsuario,
                this.fechaInicio,
                this.fechaFin,
                this.idTipoContrato,
                this.estado
        );
    }

    public GuardarContratoHistoricoComando buildGuardarContratoHistoricoComandoCuandoEsNull() {
        return new GuardarContratoHistoricoComando(
                null,null,null,null,null
        );
    }

    public ActualizarContratoHistoricoComando buildActualizarContratoHistoricoComando(){
        return new ActualizarContratoHistoricoComando(

                this.idUsuario,
                this.fechaInicio,
                this.fechaFin,
                this.idTipoContrato,
                this.estado
        );
    }

    public ActualizarContratoHistoricoComando buildActualizarContratoHistoricoComandoCuandoEsNull(){
        return new ActualizarContratoHistoricoComando(
                null,null,null,null,null
        );
    }
}
