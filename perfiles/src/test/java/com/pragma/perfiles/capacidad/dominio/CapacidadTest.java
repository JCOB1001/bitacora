package com.pragma.perfiles.capacidad.dominio;

import com.pragma.perfiles.capacidad.databuilder.CapacidadTestDataBuilder;
import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CapacidadTest {

    public static final Long ID = 6L;
    public static final String NOMBRE = "testCapacidad";

    @Test
    public void testCrearModeloCapacidad(){
        CapacidadTestDataBuilder capacidadTestDataBuilder = new CapacidadTestDataBuilder();
        Capacidad capacidad = capacidadTestDataBuilder.capacidadBuilder();
        assertEquals(ID, capacidad.getId());
        assertEquals(NOMBRE, capacidad.getNombre());
    }



}
