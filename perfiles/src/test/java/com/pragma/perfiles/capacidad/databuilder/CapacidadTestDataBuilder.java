package com.pragma.perfiles.capacidad.databuilder;

import com.pragma.perfiles.capacidad.dominio.modelo.Capacidad;

public class CapacidadTestDataBuilder {

    public static final String NOMBRE = "testCapacidad";

    private String nombre;

    public CapacidadTestDataBuilder(){
        this.nombre = NOMBRE;
    }

    public Capacidad capacidadBuilder(){
        return new Capacidad(6L, this.nombre);
    }

}
