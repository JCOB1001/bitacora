package com.pragma.perfiles.capacidad.databuilder;

import com.pragma.perfiles.capacidad.aplicacion.comando.GuardarCapacidadComando;

public class GuardarCapacidadComandoTestDataBuilder {

    public static final String NOMBRE = "Devops";

    private String nombre;

    public GuardarCapacidadComandoTestDataBuilder(){
        this.nombre = NOMBRE;
    }

    public GuardarCapacidadComando buildGuardarCapacidadComando(){
        return new GuardarCapacidadComando(this.nombre);
    }

}
