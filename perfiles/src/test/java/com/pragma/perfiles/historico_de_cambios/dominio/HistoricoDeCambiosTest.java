package com.pragma.perfiles.historico_de_cambios.dominio;


import com.pragma.perfiles.historico_de_cambios.databuilder.HistoricoDeCambiosTestDataBuilder;
import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;


public class HistoricoDeCambiosTest {
    public static final String ID = "3123";
    public static final String ID_USUARIO = "2";
    public static final String ID_TIPO_CAMBIO = "23";
    public static final LocalDate FECHA_HITO = LocalDate.of(2021,05,21);
    public static final String DETALLE = "3";


    @Test
    public void testCrearModeloHistoricoDeCambios(){
        HistoricoDeCambiosTestDataBuilder historicoDeCambiosTestDataBuilder = new HistoricoDeCambiosTestDataBuilder();
        HistoricoDeCambios historicoDeCambios = historicoDeCambiosTestDataBuilder.buildHistoricoDeCambios();
        Assert.assertEquals(ID, historicoDeCambios.getId());
        Assert.assertEquals(ID_USUARIO, historicoDeCambios.getIdUsuario());
        Assert.assertEquals(ID_TIPO_CAMBIO, historicoDeCambios.getIdTipoCambio());
        Assert.assertEquals(FECHA_HITO, historicoDeCambios.getFechaHito());
        Assert.assertEquals(DETALLE, historicoDeCambios.getDetalle());


    }
}
