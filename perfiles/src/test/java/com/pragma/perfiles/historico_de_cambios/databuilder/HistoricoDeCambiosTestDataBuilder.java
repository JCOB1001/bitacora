package com.pragma.perfiles.historico_de_cambios.databuilder;

import com.pragma.perfiles.historico_de_cambios.dominio.modelo.HistoricoDeCambios;
import java.time.LocalDate;

public class HistoricoDeCambiosTestDataBuilder {

    public static final String ID_USUARIO = "2";
    public static final String ID_TIPO_CAMBIO = "23";
    public static final LocalDate FECHA_HITO = LocalDate.of(2021,05,21);
    public static final String DETALLE = "3";


    private String idUsuario;
    private String idTipoCambio;
    private LocalDate fechaHito;
    private String detalle;

    public HistoricoDeCambiosTestDataBuilder(){
        this.idUsuario = ID_USUARIO;
        this.idTipoCambio = ID_TIPO_CAMBIO;
        this.fechaHito = FECHA_HITO;
        this.detalle = DETALLE;

    }

    public HistoricoDeCambios buildHistoricoDeCambios(){
        return new HistoricoDeCambios(
                this.idUsuario,
                this.idTipoCambio,
                this.fechaHito,
                this.detalle
        );

    }
}
