package com.pragma.perfiles.parentesco.databuilder;

import com.pragma.perfiles.capacidad.aplicacion.comando.GuardarCapacidadComando;
import com.pragma.perfiles.parentesco.aplicacion.comando.GuardarParentescoComando;

public class GuardarParentescoComandoTestDataBuilder{

        public static final String NOMBRE = "Devops";

        private String nombre;

        public GuardarParentescoComandoTestDataBuilder(){
            this.nombre = NOMBRE;
        }

        public GuardarParentescoComando buildGuardarParentescoComando(){
            return new GuardarParentescoComando(this.nombre);
        }

}



