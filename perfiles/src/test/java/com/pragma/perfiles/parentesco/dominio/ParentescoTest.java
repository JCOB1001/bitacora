package com.pragma.perfiles.parentesco.dominio;

import com.pragma.perfiles.parentesco.databuilder.ParentescoTestDataBuilder;
import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParentescoTest {

    public static final Long ID = 6L;
    public static final String NOMBRE = "testParentesco";

    @Test
    public void testCrearModeloParentesco(){

        ParentescoTestDataBuilder parentescoTestDataBuilder = new ParentescoTestDataBuilder();
        Parentesco parentesco = parentescoTestDataBuilder.parentescoBuilder();
        assertEquals(ID, parentesco.getId());
        assertEquals(NOMBRE, parentesco.getNombre());

    }

}
