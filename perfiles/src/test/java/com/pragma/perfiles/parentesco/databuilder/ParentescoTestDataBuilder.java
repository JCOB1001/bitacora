package com.pragma.perfiles.parentesco.databuilder;


import com.pragma.perfiles.parentesco.dominio.modelo.Parentesco;

public class ParentescoTestDataBuilder {
    public static final String NOMBRE="testParentesco";

    private String nombre;

    public ParentescoTestDataBuilder(){
        this.nombre = NOMBRE;
    }

    public Parentesco parentescoBuilder(){
        return new Parentesco(6L, this.nombre);
    }


}

