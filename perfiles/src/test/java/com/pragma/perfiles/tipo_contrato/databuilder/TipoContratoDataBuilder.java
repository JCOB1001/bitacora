package com.pragma.perfiles.tipo_contrato.databuilder;

import com.pragma.perfiles.informacion_nomina.aplicacion.comando.ActualizarInformacionNominaComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.comando.ActualizarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.aplicacion.comando.GuardarTipoContratoComando;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;

public class TipoContratoDataBuilder {
    private final String ID="4";
    private final String DESCRIPCION="OOOOO";
    private final boolean VACACIONES=true;

    private String id;
    private String descripcion;
    private boolean vacaciones;

    public TipoContratoDataBuilder(){
        this.id=ID;
        this.descripcion=DESCRIPCION;
        this.vacaciones=VACACIONES;
    }

    public TipoContrato buildTipoContrato(){
        return new TipoContrato(
                this.ID,
                this.DESCRIPCION,this.VACACIONES);
    }

    public GuardarTipoContratoComando buildGuardarTipoContratoComando() {
        return new GuardarTipoContratoComando(
                this.descripcion,
                this.vacaciones
        );
    }

    public GuardarTipoContratoComando buildGuardarTipoContratoComandoCuandoEsNull() {
        return new GuardarTipoContratoComando(
                null,
                false
        );
    }

    public ActualizarTipoContratoComando buildActualizarTipoContratoComando(){
        return new ActualizarTipoContratoComando(
                this.descripcion,
                this.vacaciones
        );
    }

    public ActualizarTipoContratoComando buildActualizarTipoContratoComandoCuandoEsNull(){
        return new ActualizarTipoContratoComando(
                null,
                false
        );
    }
}
