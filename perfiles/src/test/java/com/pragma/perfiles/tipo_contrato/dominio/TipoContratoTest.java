package com.pragma.perfiles.tipo_contrato.dominio;
import com.pragma.perfiles.tipo_contrato.databuilder.TipoContratoDataBuilder;
import com.pragma.perfiles.tipo_contrato.dominio.modelo.TipoContrato;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TipoContratoTest {
    private final String ID="4";
    private final String DESCRIPCION="OOOOO";

    public static final double delta = 0.01;

    @Test
    public void testCrearModeloTipoContrato(){
        TipoContratoDataBuilder tipoContratoDataBuilder = new TipoContratoDataBuilder();
        TipoContrato tipoContrato = tipoContratoDataBuilder.buildTipoContrato();

        assertEquals(ID, tipoContrato.getId());
        assertEquals(DESCRIPCION, tipoContrato.getDescripcion());
    }
}
