package com.pragma.perfiles.barra_progreso.databuilder;

import com.pragma.perfiles.barra_progreso.dominio.modelo.PesoValoresInformacionUsuario;

public class PesoValoresInfoUsuarioDatabuilder {

    public static final String ID = "1";
    public static final String NOMBRE_CAMPO = "Nombre";
    public static final double VALOR = 3.125;

    private  String id;
    private  String nombreCampo;
    private double valor;

    public PesoValoresInfoUsuarioDatabuilder(){
        this.id=ID;
        this.nombreCampo=NOMBRE_CAMPO;
        this.valor=VALOR;
    }

    public PesoValoresInformacionUsuario buildInformacion(){
        return new PesoValoresInformacionUsuario(id,nombreCampo,valor);
    }
}
