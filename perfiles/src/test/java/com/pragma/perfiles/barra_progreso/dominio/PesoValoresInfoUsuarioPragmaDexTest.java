package com.pragma.perfiles.barra_progreso.dominio;

import com.pragma.perfiles.barra_progreso.databuilder.PesoValoresInfoUsuarioDatabuilder;
import com.pragma.perfiles.barra_progreso.dominio.modelo.PesoValoresInformacionUsuario;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PesoValoresInfoUsuarioPragmaDexTest {
    public static final String ID = "1";
    public static final String NOMBRE_CAMPO = "Nombre";
    public static final double VALOR = 3.125;

    @Test
    public void testCrearModeloPesoValores(){
        PesoValoresInfoUsuarioDatabuilder pesoValoresInfoUsuarioDatabuilder = new PesoValoresInfoUsuarioDatabuilder();
        PesoValoresInformacionUsuario pesoValoresInformacionUsuario = pesoValoresInfoUsuarioDatabuilder.buildInformacion();
        assertEquals(ID, pesoValoresInformacionUsuario.getId());
        assertEquals(NOMBRE_CAMPO,pesoValoresInformacionUsuario.getNombreCampo());
        assertEquals(VALOR,pesoValoresInformacionUsuario.getValor(),0);

    }
}
