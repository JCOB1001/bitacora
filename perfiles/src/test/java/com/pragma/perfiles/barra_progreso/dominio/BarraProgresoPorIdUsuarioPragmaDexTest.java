package com.pragma.perfiles.barra_progreso.dominio;

import com.pragma.perfiles.barra_progreso.databuilder.BarraProgresoPorIdUsuarioDataBuilder;
import com.pragma.perfiles.barra_progreso.dominio.modelo.InfoEstado;
import com.pragma.perfiles.barra_progreso.dominio.modelo.Response;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class BarraProgresoPorIdUsuarioPragmaDexTest {
    public static final String MENSAJE="Ok";
    public static final int STATUS=200;
    public static final InfoEstado BODY = new InfoEstado(true,true,true,90);

    @Test
    public  void testCrearResponse(){
        BarraProgresoPorIdUsuarioDataBuilder barraProgresoPorIdUsuarioDataBuilder= new BarraProgresoPorIdUsuarioDataBuilder();
        Response response=barraProgresoPorIdUsuarioDataBuilder.buildResponse();
        assertEquals(MENSAJE,response.getMensaje());
        assertEquals(STATUS,response.getStatus());
        assertEquals(BODY.isPersonalInfo(), response.getBody().isPersonalInfo());
        assertEquals(BODY.isAcademicInfo(), response.getBody().isAcademicInfo());
        assertEquals(BODY.isLaboralInfo(), response.getBody().isLaboralInfo());
        assertEquals(BODY.getProgressBar(), response.getBody().getProgressBar(),0);
    }
}
