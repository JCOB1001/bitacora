package com.pragma.perfiles.barra_progreso.databuilder;

import com.pragma.perfiles.barra_progreso.dominio.modelo.InfoEstado;
import com.pragma.perfiles.barra_progreso.dominio.modelo.Response;

public class BarraProgresoPorIdUsuarioDataBuilder {

    public static final String MENSAJE="Ok";
    public static final int STATUS=200;
    public static final InfoEstado BODY = new InfoEstado(true,true,true,90);

    private String mensaje;
    private int status;
    private InfoEstado body;

    public BarraProgresoPorIdUsuarioDataBuilder(){
        this.mensaje=MENSAJE;
        this.status=STATUS;
        this.body=BODY;
    }

    public Response buildResponse(){
        Response response=new Response();
        response.setMensaje(this.mensaje);
        response.setStatus(this.status);
        response.setBody(this.body);
        return response;
    }
}
