package com.pragma.perfiles.estudio.databuilder;

import com.pragma.perfiles.estudio.aplicacion.comando.ActualizarEstudioComando;
import com.pragma.perfiles.estudio.aplicacion.comando.GuardarEstudioComando;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.modelo.Tipo;

public class EstudioTestDataBuilder {

    public static final Tipo NIVEL = Tipo.PROFESIONAL;
    public static final String TITULO_OBTENIDO = "Ingeniero";
    public static final String ID_USUARIO = "9876554";
    public static final String ANIO_FINALIZACION = "2022";
    public static final String INSTITUCION = "UFPS";


    private Tipo nivel;
    private String tituloObtenido;
    private String idUsuario;
    private String anioFinalizacion;
    private String institucion;


    private GuardarEstudioComando guardarEstudioComando;

    public EstudioTestDataBuilder() {
        this.nivel = NIVEL;
        this.institucion = INSTITUCION;
        this.idUsuario = ID_USUARIO;
        this.tituloObtenido = TITULO_OBTENIDO;
        this.anioFinalizacion = ANIO_FINALIZACION;

    }

    public GuardarEstudioComando buildEstudio() {
        return new GuardarEstudioComando(
                this.nivel,
                this.tituloObtenido,
                this.idUsuario,
                this.anioFinalizacion, this.institucion


        );
    }

    public Estudio builEstudio() {
        return new Estudio(
                "1",
                this.nivel,
                this.tituloObtenido,
                this.idUsuario,
                this.anioFinalizacion, this.institucion

        );
    }

    public ActualizarEstudioComando buildActualizarEstudioComandoExistente() {
        return new ActualizarEstudioComando(
                this.nivel,
                this.tituloObtenido,
                "9876554",
                this.anioFinalizacion,
                this.institucion


        );
    }

    public ActualizarEstudioComando buildActualizarEstudioComandoNoExistente() {
        return new ActualizarEstudioComando(
                this.nivel,
                this.tituloObtenido,
                this.idUsuario,
                this.anioFinalizacion,
                this.institucion


        );
    }

    public ActualizarEstudioComando buildActualizarEstudioComandoIdUsuarioNoCoincide() {
        return new ActualizarEstudioComando(
                this.nivel,
                this.tituloObtenido,
                "987655411",
                this.anioFinalizacion,
                this.institucion


        );
    }

    public GuardarEstudioComando builEstudioConError() {
        guardarEstudioComando = buildEstudio();
        guardarEstudioComando.setIdUsuario("1");
        return guardarEstudioComando;
    }


}
