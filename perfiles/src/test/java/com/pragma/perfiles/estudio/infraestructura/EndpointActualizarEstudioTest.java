package com.pragma.perfiles.estudio.infraestructura;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.perfiles.estudio.aplicacion.comando.ActualizarEstudioComando;
import com.pragma.perfiles.estudio.databuilder.EstudioTestDataBuilder;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.modelo.Tipo;
import com.pragma.perfiles.estudio.dominio.useCase.EstudioCrudUseCase;
import com.pragma.perfiles.perfil.databuilder.UsuarioTestDataBuilder;
import com.pragma.perfiles.perfil.dominio.modelo.*;
import com.pragma.perfiles.perfil.dominio.useCase.UsuarioCrudUseCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT)

public class EndpointActualizarEstudioTest {
    public static final String RUTA_CONTROLADOR = "/info-academica-estudio/{idInformacionAcademicaEstudio}";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String ID_INFORMACION_ESTUDIO_EXISTENTE="3";
    public static final String ID_INFORMACION_ESTUDIO_NO_EXISTENTE="5";
    public static final String AUTHORIZATION_HEADER_TOKEN = "eyJraWQiOiJlcERtUHA2TUdnVVNtWFN6RktoV2VjcVRLQ0NJUWVHT0t3ZDlTOWdkV3RzPSIsImFsZyI6IlJTMjU2In0" +
            ".eyJhdF9oYXNoIjoidzl4NnNnNE82ajkyZWp5OXhJeEdoZyIsInN1YiI6IjFmNGM3ZDg2LWNjOGQtNDc0ZS1iNWMyLTg2ZjgxZDI5MDdhOSIsImNvZ25pdG86Z3JvdXBzIjpbInVzLWV" +
            "hc3QtMV9Ud3N5d2ZySURfR29vZ2xlIl0sImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tXC91cy1l" +
            "YXN0LTFfVHdzeXdmcklEIiwiY29nbml0bzp1c2VybmFtZSI6Ikdvb2dsZV8xMTMxMjc2MTEyMjgyMDk3MjkyNTMiLCJhdWQiOiI1OXJ0aTAxNDlzbGhpdmN2dDNnZjFxMTUyYyIsImlkZW" +
            "50aXRpZXMiOlt7InVzZXJJZCI6IjExMzEyNzYxMTIyODIwOTcyOTI1MyIsInByb3ZpZGVyTmFtZSI6Ikdvb2dsZSIsInByb3ZpZGVyVHlwZSI6Ikdvb2dsZSIsImlzc3VlciI6bnVsbCwicH" +
            "JpbWFyeSI6InRydWUiLCJkYXRlQ3JlYXRlZCI6IjE2MjI1NTg0MjI2NjMifV0sInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjI5NzMyNzQ0LCJuYW1lIjoiW3tcIm1ldGFkYXRhXC" +
            "I6e1wicHJpbWFyeVwiOnRydWUsXCJzb3VyY2VcIjp7XCJ0eXBlXCI6XCJQUk9GSUxFXCIsXCJpZFwiOlwiMTEzMTI3NjExMjI4MjA5NzI5MjUzXCJ9fSxcImRpc3BsYXlOYW1lXCI6XCJHaW" +
            "xpYmVydCBBZG9uaXMgTW9yZW5vIEJlY2VycmFcIixcImZhbWlseU5hbWVcIjpcIk1vcmVubyBCZWNlcnJhXCIsXCJnaXZlbk5hbWVcIjpcIkdpbGliZXJ0IEFkb25pc1wiLFwiZGlzcGxheU" +
            "5hbWVMYXN0Rmlyc3RcIjpcIk1vcmVubyBCZWNlcnJhLCBHaWxpYmVydCBBZG9uaXNcIixcInVuc3RydWN0dXJlZE5hbWVcIjpcIkdpbGliZXJ0IEFkb25pcyBNb3Jlbm8gQmVjZXJyYVwifS" +
            "x7XCJtZXRhZGF0YVwiOntcInNvdXJjZVwiOntcInR5cGVcIjpcIkRPTUFJTl9QUk9GSUxFXCIsXCJpZFwiOlwiMTEzMTI3NjExMjI4MjA5NzI5MjUzXCJ9fSxcImRpc3BsYXlOYW1lXCI6XC" +
            "JHaWxpYmVydCBBZG9uaXMgTW9yZW5vIEJlY2VycmFcIixcImZhbWlseU5hbWVcIjpcIk1vcmVubyBCZWNlcnJhXCIsXCJnaXZlbk5hbWVcIjpcIkdpbGliZXJ0IEFkb25pc1wiLFwiZGlzcGxh" +
            "eU5hbWVMYXN0Rmlyc3RcIjpcIkdpbGliZXJ0IEFkb25pcyBNb3Jlbm8gQmVjZXJyYVwiLFwidW5zdHJ1Y3R1cmVkTmFtZVwiOlwiR2lsaWJlcnQgQWRvbmlzIE1vcmVubyBCZWNlcnJhXCJ9XS" +
            "IsImV4cCI6MTYzMDUyMzA4NCwiaWF0IjoxNjMwNTE5NDg0LCJlbWFpbCI6ImdpbGliZXJ0Lm1vcmVub0BwcmFnbWEuY29tLmNvIn0.m7stnjdwDorBBVy8keXQN2DMmtQ4nmQ505_erja5vneV" +
            "O4ixL_sXgDjTRiYkCqQmpNM670EwTT1eONmlE4sfTUFixzTBb4PmBSuQMNiBn40ClRR47DaUJid3l_-HaYZgVtnpdFFCNqs5ArTcm04mPDLB9yQdrIb2BR3CHnV0qivJpr2kxX22GdjSacOzzn" +
            "rGprZYxc01TGfPkSLhEy7rF9Uw7cBU1qV8PeK9zuSYuYYey3AMx1z9tqKpnizH5u6VuT7K2EmcGVNfvuVbYmA_T6-WaehLezpSl6RrHX6dzireZphhlzLABaATEznfdNaJhrJsypag2DrOO6R5UmIHTA";


    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private EstudioCrudUseCase estudioCrudUseCase;

    @MockBean
    private UsuarioCrudUseCase usuarioCrudUseCase;

    @Test
    public void testActualizarEstudioCuandoExiste()throws Exception{

        Estudio estudio = new Estudio(ID_INFORMACION_ESTUDIO_EXISTENTE, Tipo.PROFESIONAL, "Tecnologo en Análisis y Desarrollo de Sistemas de Información",
                "9876554", "2019", "ITM");

        Mockito.when(estudioCrudUseCase.obtenerPorId(Mockito.any())).thenReturn(estudio);
        Mockito.when(usuarioCrudUseCase.obtenerPorId(Mockito.any())).thenReturn(obtenerUsuario());

        UriComponents uri = UriComponentsBuilder
                .fromPath(RUTA_CONTROLADOR )
                .build();
        ActualizarEstudioComando actualizarEstudioComando =new EstudioTestDataBuilder().buildActualizarEstudioComandoExistente();

        mvc.perform(MockMvcRequestBuilders
                .put(uri.toUriString(),ID_INFORMACION_ESTUDIO_EXISTENTE)
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(actualizarEstudioComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    private Usuario obtenerUsuario() {
        return new UsuarioTestDataBuilder().buildUsuario();
    }

}
