package com.pragma.perfiles.estudio.dominio;

import com.pragma.perfiles.estudio.databuilder.EstudioTestDataBuilder;
import com.pragma.perfiles.estudio.dominio.modelo.Estudio;
import com.pragma.perfiles.estudio.dominio.modelo.Tipo;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EstudioTest {
    public static final String ID = "1";
    public static final Tipo nivel =Tipo.PROFESIONAL;
    public static final String institucion ="UFPS";
    public static final String idUsuario="9876554";
    public static final String tituloObtenido="Ingeniero";
    public static final String anioFinalizacion ="2022";

    @Test
    public void testCrearModeloEstudio(){
        EstudioTestDataBuilder estudioTestDataBuilder=new EstudioTestDataBuilder();
        Estudio estudio =  estudioTestDataBuilder.builEstudio();
        assertEquals(ID,estudio.getId());
        assertEquals(nivel.getTipo(),estudio.getNivel().getTipo());
        assertEquals(institucion,estudio.getInstitucion());
        assertEquals(idUsuario,estudio.getIdUsuario());
        assertEquals(tituloObtenido,estudio.getTituloObtenido());
        assertEquals(anioFinalizacion, estudio.getAnioFinalizacion());

    }

}
