package com.pragma.perfiles.experiencia_laboral_externa.dominio;

import com.pragma.perfiles.experiencia_laboral_externa.databuilder.ExperienciaLaboralExternaTestDataBuilder;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Period;
import static org.junit.Assert.assertEquals;

public class ExperienciaLaboralExternaTest {

    public static final String ID = "1L";
    public static final String ID_USUARIO = "67e0ea4b-cad2-41cd-97be-5fbdd961a11b"; //id de leonardo.lotero@pragma.com.co
    public static final String EMPRESA = "Developers S.A";
    public static final String ROL = "Desarrollador Backend";
    public static final LocalDate FECHA_DE_INGRESO = LocalDate.of(2020, 07, 15);
    public static final LocalDate FECHA_DE_FINALIZACION = LocalDate.of(2021, 07, 15);
    public static final long TIEMPO_DE_EXPERIENCIA = Period.between(FECHA_DE_INGRESO, FECHA_DE_FINALIZACION).getMonths();
    public static final String LOGROS = "Desarrollar varias aplicaciones y trabajar con diversas herramientas y lenguajes";
    public static final String HERRAMIENTAS_UTILIZADAS =  "JAVA, SPRINGBOOT, ARQUITECTURA LIMPIA, CLEAN CODE, MICROSERVICIOS, ANGULAR, TYPESCRIPT, NODEJS";

    @Test
    public void testCrearModeloExperienciaLaboralExterna(){
        ExperienciaLaboralExternaTestDataBuilder experienciaLaboralExternaTestDataBuilder = new ExperienciaLaboralExternaTestDataBuilder();
        ExperienciaLaboralExterna experienciaLaboralExterna = experienciaLaboralExternaTestDataBuilder.buildExperienciaLaboralExterna();
        assertEquals(ID, experienciaLaboralExterna.getId());
        assertEquals(ID_USUARIO, experienciaLaboralExterna.getIdUsuario());
        assertEquals(EMPRESA, experienciaLaboralExterna.getEmpresa());
        assertEquals(ROL, experienciaLaboralExterna.getRol());
        assertEquals(FECHA_DE_INGRESO, experienciaLaboralExterna.getFechaDeIngreso());
        assertEquals(FECHA_DE_FINALIZACION, experienciaLaboralExterna.getFechaDeFinalizacion());
        assertEquals(TIEMPO_DE_EXPERIENCIA, experienciaLaboralExterna.getTiempoDeExperiencia());
        assertEquals(LOGROS, experienciaLaboralExterna.getLogros());
        assertEquals(HERRAMIENTAS_UTILIZADAS, experienciaLaboralExterna.getHerramientasUtilizadas());
    }

}
