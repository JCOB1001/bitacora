package com.pragma.perfiles.experiencia_laboral_externa.databuilder;

import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.comando.GuardarExperienciaLaboralExternaComando;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;

import java.time.LocalDate;
import java.time.Period;

public class GuardarExperienciaLaboralExternaComandoTestDataBuilder {
    public static final String ID_USUARIO = "012345678910"; //
    public static final String ID_USUARIO_NO_EXISTE = "0000000"; //
    public static final String EMPRESA = "Developers S.A";
    public static final String ROL = "Desarrollador Backend";
    public static final LocalDate FECHA_DE_INGRESO = LocalDate.of(2020, 7, 15);
    public static final LocalDate FECHA_DE_FINALIZACION = LocalDate.of(2021, 7, 15);
    public static final long TIEMPO_DE_EXPERIENCIA = Period.between(FECHA_DE_INGRESO, FECHA_DE_FINALIZACION).getMonths();
    public static final String LOGROS = "Desarrollar varias aplicaciones y trabajar con diversas herramientas y lenguajes";
    public static final String HERRAMIENTAS_UTILIZADAS =  "JAVA, SPRINGBOOT, ARQUITECTURA LIMPIA, CLEAN CODE, MICROSERVICIOS, ANGULAR, TYPESCRIPT, NODEJS";

    private String idUsuario;
    private String idUsuarioNoExiste;
    private String empresa;
    private String rol;
    private LocalDate fechaDeIngreso;
    private LocalDate fechaDeFinalizacion;
    private long tiempoDeExperiencia;
    private String logros;
    private String herramientasUtilizadas;

    public GuardarExperienciaLaboralExternaComandoTestDataBuilder(){
        this.idUsuario = ID_USUARIO;
        this.idUsuarioNoExiste = ID_USUARIO_NO_EXISTE;
        this.empresa = EMPRESA;
        this.rol = ROL;
        this.fechaDeIngreso = FECHA_DE_INGRESO;
        this.fechaDeFinalizacion = FECHA_DE_FINALIZACION;
        this.tiempoDeExperiencia = TIEMPO_DE_EXPERIENCIA;
        this.logros = LOGROS;
        this.herramientasUtilizadas = HERRAMIENTAS_UTILIZADAS;
    }

    public GuardarExperienciaLaboralExternaComando buildGuardarExperienciaLaboralExternaComando(){

        GuardarExperienciaLaboralExternaComando guardarExperienciaLaboralExternaComando = new GuardarExperienciaLaboralExternaComando();
        guardarExperienciaLaboralExternaComando.setIdUsuario(this.idUsuario);
        guardarExperienciaLaboralExternaComando.setEmpresa(this.empresa);
        guardarExperienciaLaboralExternaComando.setRol(this.rol);
        guardarExperienciaLaboralExternaComando.setFechaDeIngreso(this.fechaDeIngreso);
        guardarExperienciaLaboralExternaComando.setFechaDeFinalizacion(this.fechaDeFinalizacion);
        guardarExperienciaLaboralExternaComando.setTiempoDeExperiencia(this.tiempoDeExperiencia);
        guardarExperienciaLaboralExternaComando.setLogros(this.logros);
        guardarExperienciaLaboralExternaComando.setHerramientasUtilizadas(this.herramientasUtilizadas);
        return guardarExperienciaLaboralExternaComando;
    }
    public GuardarExperienciaLaboralExternaComando buildGuardarExperienciaLaboralExternaComandoNoExiste(){

        GuardarExperienciaLaboralExternaComando guardarExperienciaLaboralExternaComando = new GuardarExperienciaLaboralExternaComando();
        guardarExperienciaLaboralExternaComando.setIdUsuario(this.idUsuarioNoExiste);
        guardarExperienciaLaboralExternaComando.setEmpresa(this.empresa);
        guardarExperienciaLaboralExternaComando.setRol(this.rol);
        guardarExperienciaLaboralExternaComando.setFechaDeIngreso(this.fechaDeIngreso);
        guardarExperienciaLaboralExternaComando.setFechaDeFinalizacion(this.fechaDeFinalizacion);
        guardarExperienciaLaboralExternaComando.setTiempoDeExperiencia(this.tiempoDeExperiencia);
        guardarExperienciaLaboralExternaComando.setLogros(this.logros);
        guardarExperienciaLaboralExternaComando.setHerramientasUtilizadas(this.herramientasUtilizadas);
        return guardarExperienciaLaboralExternaComando;
    }
}
