package com.pragma.perfiles.experiencia_laboral_externa.databuilder;

import com.pragma.perfiles.experiencia_laboral_externa.aplicacion.comando.ActualizarExperienciaLaboralExternaComando;
import com.pragma.perfiles.experiencia_laboral_externa.dominio.modelo.ExperienciaLaboralExterna;

import java.time.LocalDate;
import java.time.Period;

public class ExperienciaLaboralExternaTestDataBuilder {

    public static final String ID_USUARIO = "67e0ea4b-cad2-41cd-97be-5fbdd961a11b"; //id de leonardo.lotero@pragma.com.co
    public static final String EMPRESA = "Developers S.A";
    public static final String ROL = "Desarrollador Backend";
    public static final LocalDate FECHA_DE_INGRESO = LocalDate.of(2020, 07, 15);
    public static final LocalDate FECHA_DE_FINALIZACION = LocalDate.of(2021, 07, 15);
    public static final long TIEMPO_DE_EXPERIENCIA = Period.between(FECHA_DE_INGRESO, FECHA_DE_FINALIZACION).getMonths();
    public static final String LOGROS = "Desarrollar varias aplicaciones y trabajar con diversas herramientas y lenguajes";
    public static final String HERRAMIENTAS_UTILIZADAS =  "JAVA, SPRINGBOOT, ARQUITECTURA LIMPIA, CLEAN CODE, MICROSERVICIOS, ANGULAR, TYPESCRIPT, NODEJS";

    private String idUsuario;
    private String empresa;
    private String rol;
    private LocalDate fechaDeIngreso;
    private LocalDate fechaDeFinalizacion;
    private Long tiempoDeExperiencia;
    private String logros;
    private String herramientasUtilizadas;

    public ExperienciaLaboralExternaTestDataBuilder(){
        this.idUsuario = ID_USUARIO;
        this.empresa = EMPRESA;
        this.rol = ROL;
        this.fechaDeIngreso = FECHA_DE_INGRESO;
        this.fechaDeFinalizacion = FECHA_DE_FINALIZACION;
        this.tiempoDeExperiencia = TIEMPO_DE_EXPERIENCIA;
        this.logros = LOGROS;
        this.herramientasUtilizadas = HERRAMIENTAS_UTILIZADAS;
    }

    public ExperienciaLaboralExterna buildExperienciaLaboralExterna(){
        return new ExperienciaLaboralExterna("1L",
                this.idUsuario,
                this.empresa,
                this.rol,
                this.fechaDeIngreso,
                this.fechaDeFinalizacion,
                this.tiempoDeExperiencia,
                this.logros,
                this.herramientasUtilizadas);
    }

    public ActualizarExperienciaLaboralExternaComando buildActualizarExperienciaLaboralExternaComandoExistente(){
        return new ActualizarExperienciaLaboralExternaComando("9876554",this.empresa,this.rol,this.fechaDeIngreso,this.fechaDeFinalizacion,this.tiempoDeExperiencia,this.logros, this.herramientasUtilizadas);
    }

    public ActualizarExperienciaLaboralExternaComando buildActualizarExperienciaLaboralExternaComandoNoExistente(){
        return new ActualizarExperienciaLaboralExternaComando(this.idUsuario,this.empresa,this.rol,this.fechaDeIngreso,this.fechaDeFinalizacion,this.tiempoDeExperiencia,this.logros, this.herramientasUtilizadas);
    }

    public ActualizarExperienciaLaboralExternaComando buildActualizarExperienciaLaboralExternaComandoIdUsuarioNoCoinciden(){
        return new ActualizarExperienciaLaboralExternaComando("012345678911",this.empresa,this.rol,this.fechaDeIngreso,this.fechaDeFinalizacion,this.tiempoDeExperiencia,this.logros, this.herramientasUtilizadas);
    }
}
