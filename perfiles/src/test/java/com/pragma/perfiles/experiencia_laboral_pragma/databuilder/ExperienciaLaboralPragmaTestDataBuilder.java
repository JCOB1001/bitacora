package com.pragma.perfiles.experiencia_laboral_pragma.databuilder;

import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.comando.GuardarExperienciaLaboralPragmaComando;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.comando.ActualizarExperienciaLaboralPragmaComando;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;

import java.time.LocalDate;
import java.time.Period;

public class ExperienciaLaboralPragmaTestDataBuilder {

    public static final String ID_USUARIO = "67e0ea4b-cad2-41cd-97be-5fbdd961a11b"; //id de leonardo.lotero@pragma.com.co
    public static final String PROYECTO = "Ecosistemas";
    public static final String ROL = "Desarrollador Backend";
    public static final LocalDate FECHA_DE_INGRESO = LocalDate.of(2020, 07, 15);
    public static final LocalDate FECHA_DE_FINALIZACION = LocalDate.of(2021, 07, 15);
    public static final long TIEMPO_DE_EXPERIENCIA = Period.between(FECHA_DE_INGRESO, FECHA_DE_FINALIZACION).getMonths();
    public static final String LOGROS = "MEJORAR LA NAVEGACION ENTRE APLICACION Y SENTAR LAS BASES DEL FUTURO DEL ECOSISTEMA DIGITAL EN TORNO A LOS USUARIOS";
    public static final String HERRAMIENTAS_UTILIZADAS = "JAVA, SPRINGBOOT, ARQUITECTURA LIMPIA, CLEAN CODE, MICROSERVICIOS, ANGULAR, TYPESCRIPT, NODEJS";

    private String idUsuario;
    private String proyecto;
    private String rol;
    private LocalDate fechaDeIngreso;
    private LocalDate fechaDeFinalizacion;
    private long tiempoDeExperiencia;
    private String logros;
    private String herramientasUtilizadas;

    public ExperienciaLaboralPragmaTestDataBuilder(){
        this.idUsuario = ID_USUARIO;
        this.proyecto = PROYECTO;
        this.rol = ROL;
        this.fechaDeIngreso = FECHA_DE_INGRESO;
        this.fechaDeFinalizacion = FECHA_DE_FINALIZACION;
        this.tiempoDeExperiencia = TIEMPO_DE_EXPERIENCIA;
        this.logros = LOGROS;
        this.herramientasUtilizadas = HERRAMIENTAS_UTILIZADAS;
    }

    public ExperienciaLaboralPragma buildExperienciaLaboralPragma(){
        return new ExperienciaLaboralPragma("1",
                this.proyecto,
                this.rol,
                this.fechaDeIngreso,
                this.fechaDeFinalizacion,
                this.tiempoDeExperiencia,
                this.logros,
                this.herramientasUtilizadas,
                this.idUsuario);
    }

    public GuardarExperienciaLaboralPragmaComando buildGuardarExperienciaLaboralPragmaComandoExistente(){
        return new GuardarExperienciaLaboralPragmaComando("01234567891",this.proyecto,this.rol,this.fechaDeIngreso,
                this.fechaDeFinalizacion,this.tiempoDeExperiencia,this.logros, this.herramientasUtilizadas);
    }

    public GuardarExperienciaLaboralPragmaComando buildGuardarExperienciaLaboralPragmaComandoNoExistente(){
        return new GuardarExperienciaLaboralPragmaComando("0000",this.proyecto,this.rol,this.fechaDeIngreso,
                this.fechaDeFinalizacion,this.tiempoDeExperiencia,this.logros,this.herramientasUtilizadas);
    }


    public GuardarExperienciaLaboralPragmaComando buildActualizarExperienciaLaboralExternaComandoNoExistente(){
        return new GuardarExperienciaLaboralPragmaComando(this.idUsuario,this.proyecto,this.rol,this.fechaDeIngreso,this.fechaDeFinalizacion,this.tiempoDeExperiencia,this.logros,this.herramientasUtilizadas);


    }

    public ActualizarExperienciaLaboralPragmaComando buildActualizarExperienciaLaboralPragmaComandoExistente(){
        return new ActualizarExperienciaLaboralPragmaComando(
                this.proyecto,
                this.rol,
                this.fechaDeIngreso,
                this.fechaDeFinalizacion,
                this.tiempoDeExperiencia,
                this.logros,
                this.herramientasUtilizadas,
                "01234567891");
    }

    public ActualizarExperienciaLaboralPragmaComando buildActualizarExperienciaLaboralPragmaComandoNoExistente(){
        return new ActualizarExperienciaLaboralPragmaComando(
                this.proyecto,
                this.rol,
                this.fechaDeIngreso,
                this.fechaDeFinalizacion,
                this.tiempoDeExperiencia,
                this.logros,
                this.herramientasUtilizadas,
                this.idUsuario);
    }

    public ActualizarExperienciaLaboralPragmaComando buildActualizarExperienciaLaboralPragmaComandoIdUsuarioNoCoinciden(){
        return new ActualizarExperienciaLaboralPragmaComando(
                this.proyecto,
                this.rol,
                this.fechaDeIngreso,
                this.fechaDeFinalizacion,
                this.tiempoDeExperiencia,
                this.logros,
                this.herramientasUtilizadas,
                "012345678911");
    }
}
