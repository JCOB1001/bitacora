package com.pragma.perfiles.experiencia_laboral_pragma.infraestructura;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.perfiles.comun.infraestructura.error.UsuarioNoEncontrado;
import com.pragma.perfiles.experiencia_laboral_pragma.aplicacion.comando.GuardarExperienciaLaboralPragmaComando;
import com.pragma.perfiles.experiencia_laboral_pragma.databuilder.ExperienciaLaboralPragmaTestDataBuilder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EndpointGuardarExperienciaLaboralPragmaTest {

    public static final String RUTA_CONTROLADOR = "/info-laboral-pragma";
    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String AUTHORIZATION_HEADER_TOKEN = "eyJraWQiOiJlcERtUHA2TUdnVVNtWFN6RktoV2VjcVRLQ0NJUWVHT0t3ZDlTOWdkV3RzPSIsImFsZyI6IlJTMjU2In0.eyJhdF9oYXNoIjoiQldGZ29jYzd3RDdybS1ETnM2Q3NJUSIsInN1YiI6IjFmNGM3ZDg2LWNjOGQtNDc0ZS1iNWMyLTg2ZjgxZDI5MDdhOSIsImNvZ25pdG86Z3JvdXBzIjpbInVzLWVhc3QtMV9Ud3N5d2ZySURfR29vZ2xlIl0sImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tXC91cy1lYXN0LTFfVHdzeXdmcklEIiwiY29nbml0bzp1c2VybmFtZSI6Ikdvb2dsZV8xMTMxMjc2MTEyMjgyMDk3MjkyNTMiLCJhdWQiOiI1OXJ0aTAxNDlzbGhpdmN2dDNnZjFxMTUyYyIsImlkZW50aXRpZXMiOlt7InVzZXJJZCI6IjExMzEyNzYxMTIyODIwOTcyOTI1MyIsInByb3ZpZGVyTmFtZSI6Ikdvb2dsZSIsInByb3ZpZGVyVHlwZSI6Ikdvb2dsZSIsImlzc3VlciI6bnVsbCwicHJpbWFyeSI6InRydWUiLCJkYXRlQ3JlYXRlZCI6IjE2MjI1NTg0MjI2NjMifV0sInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjI5NzMyNzQ0LCJuYW1lIjoiW3tcIm1ldGFkYXRhXCI6e1wicHJpbWFyeVwiOnRydWUsXCJzb3VyY2VcIjp7XCJ0eXBlXCI6XCJQUk9GSUxFXCIsXCJpZFwiOlwiMTEzMTI3NjExMjI4MjA5NzI5MjUzXCJ9fSxcImRpc3BsYXlOYW1lXCI6XCJHaWxpYmVydCBBZG9uaXMgTW9yZW5vIEJlY2VycmFcIixcImZhbWlseU5hbWVcIjpcIk1vcmVubyBCZWNlcnJhXCIsXCJnaXZlbk5hbWVcIjpcIkdpbGliZXJ0IEFkb25pc1wiLFwiZGlzcGxheU5hbWVMYXN0Rmlyc3RcIjpcIk1vcmVubyBCZWNlcnJhLCBHaWxpYmVydCBBZG9uaXNcIixcInVuc3RydWN0dXJlZE5hbWVcIjpcIkdpbGliZXJ0IEFkb25pcyBNb3Jlbm8gQmVjZXJyYVwifSx7XCJtZXRhZGF0YVwiOntcInNvdXJjZVwiOntcInR5cGVcIjpcIkRPTUFJTl9QUk9GSUxFXCIsXCJpZFwiOlwiMTEzMTI3NjExMjI4MjA5NzI5MjUzXCJ9fSxcImRpc3BsYXlOYW1lXCI6XCJHaWxpYmVydCBBZG9uaXMgTW9yZW5vIEJlY2VycmFcIixcImZhbWlseU5hbWVcIjpcIk1vcmVubyBCZWNlcnJhXCIsXCJnaXZlbk5hbWVcIjpcIkdpbGliZXJ0IEFkb25pc1wiLFwiZGlzcGxheU5hbWVMYXN0Rmlyc3RcIjpcIkdpbGliZXJ0IEFkb25pcyBNb3Jlbm8gQmVjZXJyYVwiLFwidW5zdHJ1Y3R1cmVkTmFtZVwiOlwiR2lsaWJlcnQgQWRvbmlzIE1vcmVubyBCZWNlcnJhXCJ9XSIsImV4cCI6MTYzMDA5OTk3NiwiaWF0IjoxNjMwMDk2Mzc2LCJlbWFpbCI6ImdpbGliZXJ0Lm1vcmVub0BwcmFnbWEuY29tLmNvIn0.TQkS9ILFO3SEPVUSewywCGGfMbt1yl7dMBdmZF-BpHvdkmOQpAfpiyuH-z37L06Do5KGyhx0egvbB9XZxiI4u9Zx3StPjrtKTvTpxfF_PY4Aai3cCHqFO8CB3WCizIDGWGZ1ITm7n0J7zjcR7jtmk723e7RJaEMxFQ2BMpe7x_H1gMW402HuhZVHQwp-0cMRJxFpGGBcHkwaF6I4Vttnwb8VCUQfzCGIV4BAKsuT0TmWI_11T4exl-vXuhAcHkm1AgQcfr3O77bZzl8Py3KKMQNmUCdr3mw-vbBDvL__fzTyhbz4jL3Ug7a1mRP2OYFuc5nuE7M8ue3SHFj9BMo-rw";
    public static final String ID_INFO_EXISTENTE = "3"; //ID DE PRUEBA DE pablo.munozm@pragma.com.co

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testNoGuardarExperienciaLaboralPragmaCuandoNoExisteUsuario() throws Exception {
        UriComponents uri = UriComponentsBuilder.fromPath(RUTA_CONTROLADOR).build();
        GuardarExperienciaLaboralPragmaComando guardarExperienciaLaboralPragmaComando = new ExperienciaLaboralPragmaTestDataBuilder()
                .buildGuardarExperienciaLaboralPragmaComandoNoExistente();
        guardarExperienciaLaboralPragmaComando.setIdUsuario(ID_INFO_EXISTENTE);
        mvc.perform(MockMvcRequestBuilders
                .post(uri.toUriString())
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(guardarExperienciaLaboralPragmaComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void testGuardarExperienciaLaboralPragmaCuandoExisteUsuario() throws Exception {
        UriComponents uri = UriComponentsBuilder.fromPath(RUTA_CONTROLADOR ).build();
        GuardarExperienciaLaboralPragmaComando guardarExperienciaLaboralPragmaComando = new ExperienciaLaboralPragmaTestDataBuilder()
                .buildGuardarExperienciaLaboralPragmaComandoExistente();
        mvc.perform(MockMvcRequestBuilders
                .post(uri.toUriString())
                .header(AUTHORIZATION_HEADER, AUTHORIZATION_HEADER_TOKEN)
                .content(objectMapper.writeValueAsString(guardarExperienciaLaboralPragmaComando))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

}
