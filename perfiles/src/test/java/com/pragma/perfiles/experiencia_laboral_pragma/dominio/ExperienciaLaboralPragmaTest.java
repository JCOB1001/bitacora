package com.pragma.perfiles.experiencia_laboral_pragma.dominio;

import com.pragma.perfiles.experiencia_laboral_pragma.databuilder.ExperienciaLaboralPragmaTestDataBuilder;
import com.pragma.perfiles.experiencia_laboral_pragma.dominio.modelo.ExperienciaLaboralPragma;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Period;

import static org.junit.Assert.assertEquals;

public class ExperienciaLaboralPragmaTest {

    public static final String ID = "1";
    public static final String ID_USUARIO = "67e0ea4b-cad2-41cd-97be-5fbdd961a11b"; //id de leonardo.lotero@pragma.com.co
    public static final String PROYECTO = "Ecosistemas";
    public static final String ROL = "Desarrollador Backend";
    public static final LocalDate FECHA_DE_INGRESO = LocalDate.of(2020, 07, 15);
    public static final LocalDate FECHA_DE_FINALIZACION = LocalDate.of(2021, 07, 15);
    public static final long TIEMPO_DE_EXPERIENCIA = Period.between(FECHA_DE_INGRESO, FECHA_DE_FINALIZACION).getMonths();
    public static final String LOGROS = "MEJORAR LA NAVEGACION ENTRE APLICACION Y SENTAR LAS BASES DEL FUTURO DEL ECOSISTEMA DIGITAL EN TORNO A LOS USUARIOS";
    public static final String HERRAMIENTAS_UTILIZADAS = "JAVA, SPRINGBOOT, ARQUITECTURA LIMPIA, CLEAN CODE, MICROSERVICIOS, ANGULAR, TYPESCRIPT, NODEJS";

    @Test
    public void testCrearModeloExperienciaLaboralExterna(){
        ExperienciaLaboralPragmaTestDataBuilder experienciaLaboralPragmaTestDataBuilder = new ExperienciaLaboralPragmaTestDataBuilder();
        ExperienciaLaboralPragma experienciaLaboralPragma = experienciaLaboralPragmaTestDataBuilder.buildExperienciaLaboralPragma();
        assertEquals(ID, experienciaLaboralPragma.getId());
        assertEquals(ID_USUARIO, experienciaLaboralPragma.getIdUsuario());
        assertEquals(PROYECTO, experienciaLaboralPragma.getProyecto());
        assertEquals(ROL, experienciaLaboralPragma.getRol());
        assertEquals(FECHA_DE_INGRESO, experienciaLaboralPragma.getFechaDeIngreso());
        assertEquals(FECHA_DE_FINALIZACION, experienciaLaboralPragma.getFechaDeFinalizacion());
        assertEquals(TIEMPO_DE_EXPERIENCIA, experienciaLaboralPragma.getTiempoDeExperiencia());
        assertEquals(LOGROS, experienciaLaboralPragma.getLogros());
        assertEquals(HERRAMIENTAS_UTILIZADAS, experienciaLaboralPragma.getHerramientasUtilizadas());
    }

}
